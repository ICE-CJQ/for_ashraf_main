package com.internal.service.somruinternal.controller;

import static org.springframework.data.domain.ExampleMatcher.matching;
import com.internal.service.somruinternal.model.SaleLink;
import com.internal.service.somruinternal.repository.SaleLinkRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

@RestController
@RequestMapping("/SaleLink")
public class SaleLinkControl {
    @Autowired
    SaleLinkRepository saleLinkRepo;
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/all")
    public List<SaleLink> getAllSaleLinks() {
        return saleLinkRepo.findAll();
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/add")
    public SaleLink createSaleLinks(@Valid @RequestBody SaleLink saleLinks) {
        return saleLinkRepo.save(saleLinks);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/get/{id}")
    public ResponseEntity<SaleLink> getSaleLinkById(@PathVariable(value = "id") Long saleLinkId) {
    	SaleLink saleLink = saleLinkRepo.findOne(saleLinkId);
        if(saleLink == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(saleLink);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getLinksById/{type}/{id}")
    public List<SaleLink> getKitLinkByKitId(@PathVariable(value = "type") String type,@PathVariable(value = "id") Long saleLinkId) {
    	Example<SaleLink> example = Example.of(new SaleLink((long)-1, saleLinkId, type, "", (long)-1, "", "", (double) -1, "",(long)-1, (double)-1, "", "", null), matching(). //
				withIgnorePaths("dbid", "itemType", "itemId", "cat", "name", "price","size", "itemDiscount", "itemQuan", "footnote", "editby", "modify"));
    	return saleLinkRepo.findAll(example);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<SaleLink> updateSaleLink(@PathVariable(value = "id") Long  saleLinkId, 
                                           @Valid @RequestBody SaleLink saleLinkDetail) {
    	SaleLink saleLink= saleLinkRepo.findOne(saleLinkId);
        if(saleLink == null) {
            return ResponseEntity.notFound().build();
        }
        
       saleLink.setDbid(saleLinkDetail.getDbid());
       saleLink.setLinkId(saleLinkDetail.getLinkId());
       saleLink.setLinkType(saleLinkDetail.getLinkType());
       saleLink.setItemType(saleLinkDetail.getItemType());
       saleLink.setItemId(saleLinkDetail.getItemId());
       saleLink.setCat(saleLinkDetail.getCat());
       saleLink.setName(saleLinkDetail.getName());
       saleLink.setPrice(saleLinkDetail.getPrice());
       saleLink.setSize(saleLinkDetail.getSize());
       saleLink.setItemDiscount(saleLinkDetail.getItemDiscount());
       saleLink.setItemQuan(saleLinkDetail.getItemQuan());
       saleLink.setFootnote(saleLinkDetail.getFootnote());
       saleLink.setEditby(saleLinkDetail.getEditby());
       saleLink.setModify(saleLinkDetail.getModify());
       
       SaleLink UpdatesaleLink = saleLinkRepo.save(saleLink);
       return ResponseEntity.ok(UpdatesaleLink);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<SaleLink> deleteSaleLink(@PathVariable(value = "id") Long saleLinkId) {
    	SaleLink saleLink = saleLinkRepo.findOne(saleLinkId);
        if(saleLink == null) {
            return ResponseEntity.notFound().build();
        }

        saleLinkRepo.delete(saleLink);
        return ResponseEntity.ok().build();
    }
}
