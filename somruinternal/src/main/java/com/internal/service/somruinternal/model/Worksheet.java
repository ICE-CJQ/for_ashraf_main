package com.internal.service.somruinternal.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "worksheetlist")
@EntityListeners(AuditingEntityListener.class)
public class Worksheet {

  public Worksheet(){super();}

  public Worksheet(int assayNumber, String scientist, Date date){
    super();
    this.assayNumber=assayNumber;
    this.scientist=scientist;
    this.date=date;
  }
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long worksheetID;

  private long assayNumber;
  @Column(nullable = false)

  private String scientist;
  @Column(nullable = false)

  private Date date;
  @Column(nullable = false)

  private int coatingBuffer;
  @Column(nullable = false)

  private long coatingBufferLotNumber;
  @Column(nullable = false)

  private int coatingMaterial1;
  @Column(nullable = false)

  private long coatingMaterial1LotNumber;
  @Column(nullable = false)

  private int ulCoatingMaterial;
  @Column(nullable = false)

  private int ulCoatingBuffer;
  @Column(nullable = false)

  private int ulCoatingMaterial_ulCoatingBuffer;
  @Column(nullable = false)

  private int lockingLotNum;
  @Column(nullable = false)
  private int washingLotNUm;
  @Column(nullable = false)
  private int superblockLotNum;
  @Column(nullable = false)
  private int ulCLone1;
  @Column(nullable = false)
  private int ulCLone2;
  @Column(nullable = false)
  private int ulDiluent;
  @Column(nullable = false)
  private int diluentLotNumber;
  @Column(nullable = false)
  private int innovatorLotNumber;
  @Column(nullable = false)
  private int chemicalInput1;
  @Column(nullable = false)
  private int chemicalInput2;
  @Column(nullable = false)
  private int chemicalInput3;
  @Column(nullable = false)
  private int chemicalInput4;
  @Column(nullable = false)
  private int chemicalInput5;
  @Column(nullable = false)
  private int chemicalInput6;
  @Column(nullable = false)
  private int chemicalInput7;
  @Column(nullable = false)
  private int concentrationLotNumber;
  @Column(nullable = false)
  private int ulBiosimilar;
  @Column(nullable = false)
  private int ulBiosimilar2;
  @Column(nullable = false)
  private int ulSuperblock;
  @Column(nullable = false)
  private int ulBiosimilar3;
  @Column(nullable = false)
  private int ulBioSimilar4;
  @Column(nullable = false)
  private int ulBioSimilar5;
  @Column(nullable = false)
  private int ulBioSimilar6;
  @Column(nullable = false)
  private int chemicalInput8;
  @Column(nullable = false)
  private int chemicalInput9;
  @Column(nullable = false)
  private int chemicalInput10;
  @Column(nullable = false)
  private int chemicalInput11;
  @Column(nullable = false)
  private int chemicalInput12;
  @Column(nullable = false)
  private int chemicalInput13;
  @Column(nullable = false)
  private int chemicalInput14;
  @Column(nullable = false)
  private int chemicalInput15;
  @Column(nullable = false)

  public long getAssayNumber() {
    return assayNumber;
  }
  public void setAssayNumber(long assayNumber) {
    this.assayNumber = assayNumber;
  }

  public String getScientist() { return scientist; }
  public void setScientist(String scientist) {
    this.scientist = scientist;
  }

  public Date getDate() {return date;}
  public void setDate(Date date){this.date=date;}

  public long getCoatingBuffer(){return coatingBuffer;}

  public long getCoatingBufferLotNumber() {
    return coatingBufferLotNumber;
  }

  public long getCoatingMaterial1() {
    return coatingMaterial1;
  }

  public long getCoatingMaterial1LotNumber() {
    return coatingMaterial1LotNumber;
  }

  public long getWorksheetID() {
    return worksheetID;
  }

  public void setWorksheetID(long worksheetID) {
    this.worksheetID = worksheetID;
  }

  public long getUlCoatingBuffer() {
    return ulCoatingBuffer;
  }

  public long getUlCoatingMaterial() {
    return ulCoatingMaterial;
  }

  public long getUlCoatingMaterial_ulCoatingBuffer() {
    return ulCoatingMaterial_ulCoatingBuffer;
  }

  public void setCoatingBuffer(int coatingBuffer) {
    this.coatingBuffer = coatingBuffer;
  }

  public void setCoatingBufferLotNumber(long coatingBufferLotNumber) {
    this.coatingBufferLotNumber = coatingBufferLotNumber;
  }

  public void setCoatingMaterial1(int coatingMaterial1) {
    this.coatingMaterial1 = coatingMaterial1;
  }

  public void setCoatingMaterial1LotNumber(long coatingMaterial1LotNumber) {
    this.coatingMaterial1LotNumber = coatingMaterial1LotNumber;
  }

  public void setUlCoatingBuffer(int ulCoatingBuffer) {
    this.ulCoatingBuffer = ulCoatingBuffer;
  }

  public void setUlCoatingMaterial(int ulCoatingMaterial) {
    this.ulCoatingMaterial = ulCoatingMaterial;
  }

  public void setUlCoatingMaterial_ulCoatingBuffer(int ulCoatingMaterial_ulCoatingBuffer) {
    this.ulCoatingMaterial_ulCoatingBuffer = ulCoatingMaterial_ulCoatingBuffer;
  }

  public int getChemicalInput1() {
    return chemicalInput1;
  }

  public int getChemicalInput2() {
    return chemicalInput2;
  }

  public int getChemicalInput3() {
    return chemicalInput3;
  }

  public int getChemicalInput4() {
    return chemicalInput4;
  }

  public int getChemicalInput5() {
    return chemicalInput5;
  }

  public int getChemicalInput6() {
    return chemicalInput6;
  }

  public int getChemicalInput7() {
    return chemicalInput7;
  }

  public int getChemicalInput8() {
    return chemicalInput8;
  }

  public int getChemicalInput9() {
    return chemicalInput9;
  }

  public void setChemicalInput9(int chemicalInput9) {
    this.chemicalInput9 = chemicalInput9;
  }

  public int getChemicalInput10() {
    return chemicalInput10;
  }

  public int getChemicalInput11() {
    return chemicalInput11;
  }

  public int getChemicalInput12() {
    return chemicalInput12;
  }

  public int getChemicalInput13() {
    return chemicalInput13;
  }

  public int getChemicalInput14() {
    return chemicalInput14;
  }

  public int getChemicalInput15() {
    return chemicalInput15;
  }

  public int getConcentrationLotNumber() {
    return concentrationLotNumber;
  }

  public int getDiluentLotNumber() {
    return diluentLotNumber;
  }

  public int getInnovatorLotNumber() {
    return innovatorLotNumber;
  }

  public int getLockingLotNum() {
    return lockingLotNum;
  }

  public int getUlBiosimilar3() {
    return ulBiosimilar3;
  }

  public int getUlBioSimilar4() {
    return ulBioSimilar4;
  }

  public int getSuperblockLotNum() {
    return superblockLotNum;
  }

  public int getUlBioSimilar5() {
    return ulBioSimilar5;
  }

  public int getUlBiosimilar2() {
    return ulBiosimilar2;
  }

  public int getUlCLone1() {
    return ulCLone1;
  }

  public int getUlCLone2() {
    return ulCLone2;
  }

  public int getUlBioSimilar6() {
    return ulBioSimilar6;
  }

  public void setChemicalInput1(int chemicalInput1) {
    this.chemicalInput1 = chemicalInput1;
  }

  public int getUlBiosimilar() {
    return ulBiosimilar;
  }

  public int getUlDiluent() {
    return ulDiluent;
  }

  public int getUlSuperblock() {
    return ulSuperblock;
  }

  public int getWashingLotNUm() {
    return washingLotNUm;
  }

  public void setChemicalInput2(int chemicalInput2) {
    this.chemicalInput2 = chemicalInput2;
  }

  public void setChemicalInput3(int chemicalInput3) {
    this.chemicalInput3 = chemicalInput3;
  }

  public void setChemicalInput4(int chemicalInput4) {
    this.chemicalInput4 = chemicalInput4;
  }

  public void setChemicalInput5(int chemicalInput5) {
    this.chemicalInput5 = chemicalInput5;
  }

  public void setChemicalInput6(int chemicalInput6) {
    this.chemicalInput6 = chemicalInput6;
  }

  public void setChemicalInput7(int chemicalInput7) {
    this.chemicalInput7 = chemicalInput7;
  }

  public void setChemicalInput8(int chemicalInput8) {
    this.chemicalInput8 = chemicalInput8;
  }

  public void setWashingLotNUm(int washingLotNUm) {
    this.washingLotNUm = washingLotNUm;
  }

  public void setChemicalInput10(int chemicalInput10) {
    this.chemicalInput10 = chemicalInput10;
  }

  public void setChemicalInput11(int chemicalInput11) {
    this.chemicalInput11 = chemicalInput11;
  }

  public void setChemicalInput12(int chemicalInput12) {
    this.chemicalInput12 = chemicalInput12;
  }

  public void setChemicalInput13(int chemicalInput13) {
    this.chemicalInput13 = chemicalInput13;
  }

  public void setChemicalInput14(int chemicalInput14) {
    this.chemicalInput14 = chemicalInput14;
  }

  public void setChemicalInput15(int chemicalInput15) {
    this.chemicalInput15 = chemicalInput15;
  }

  public void setConcentrationLotNumber(int concentrationLotNumber) {
    this.concentrationLotNumber = concentrationLotNumber;
  }

  public void setDiluentLotNumber(int diluentLotNumber) {
    this.diluentLotNumber = diluentLotNumber;
  }

  public void setInnovatorLotNumber(int innovatorLotNumber) {
    this.innovatorLotNumber = innovatorLotNumber;
  }

  public void setLockingLotNum(int lockingLotNum) {
    this.lockingLotNum = lockingLotNum;
  }

  public void setSuperblockLotNum(int superblockLotNum) {
    this.superblockLotNum = superblockLotNum;
  }

  public void setUlBiosimilar(int ulBiosimilar) {
    this.ulBiosimilar = ulBiosimilar;
  }

  public void setUlBiosimilar2(int ulBiosimilar2) {
    this.ulBiosimilar2 = ulBiosimilar2;
  }

  public void setUlBiosimilar3(int ulBiosimilar3) {
    this.ulBiosimilar3 = ulBiosimilar3;
  }

  public void setUlBioSimilar4(int ulBioSimilar4) {
    this.ulBioSimilar4 = ulBioSimilar4;
  }

  public void setUlBioSimilar5(int ulBioSimilar5) {
    this.ulBioSimilar5 = ulBioSimilar5;
  }

  public void setUlBioSimilar6(int ulBioSimilar6) {
    this.ulBioSimilar6 = ulBioSimilar6;
  }

  public void setUlCLone1(int ulCLone1) {
    this.ulCLone1 = ulCLone1;
  }

  public void setUlCLone2(int ulCLone2) {
    this.ulCLone2 = ulCLone2;
  }

  public void setUlDiluent(int ulDiluent) {
    this.ulDiluent = ulDiluent;
  }

  public void setUlSuperblock(int ulSuperblock) {
    this.ulSuperblock = ulSuperblock;
  }
}
