package com.internal.service.somruinternal.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.internal.service.somruinternal.model.Kit;
import com.internal.service.somruinternal.model.KitComponent;
import com.internal.service.somruinternal.repository.KitRepository;

@RestController
@RequestMapping("/kit")
public class KitControl {
	
	@Autowired
	KitRepository kitRepo;
	
    @GetMapping("/all")
	public List<Kit> getAllKits() {
        return kitRepo.findAll();
    }
    
    @GetMapping("/count")
	public int getCount() {
        return kitRepo.findAll().size();
    }
    
    @GetMapping("/allByPage/{page}")
	public List<Kit> getAllKits(@PathVariable(value = "page") int page) {
        return kitRepo.findAll(new PageRequest(page, 10, Sort.Direction.DESC, "dbid")).getContent();
    }
    
    @GetMapping("/searchKit/{searchKey}/{page}")
	public List<Kit> searchKit(@PathVariable(value = "searchKey") String searchKey, @PathVariable(value = "page") int page) {
    	searchKey=searchKey.replaceAll("slash", "/");
    	searchKey=searchKey.replaceAll("whitespace", " ");
    	searchKey=searchKey.replaceAll("pong", "#");
    	searchKey=searchKey.replaceAll("percent", "%");
    	searchKey=searchKey.replaceAll("questionmark", "?");
        return kitRepo.searchKit(searchKey, new PageRequest(page, 10));
    }
    
    @GetMapping("/searchByCat/{searchKey}/{page}")
	public List<Kit> searchByCat(@PathVariable(value = "searchKey") String searchKey, @PathVariable(value = "page") int page) {
    	searchKey=searchKey.replaceAll("slash", "/");
    	searchKey=searchKey.replaceAll("whitespace", " ");
    	searchKey=searchKey.replaceAll("pong", "#");
    	searchKey=searchKey.replaceAll("percent", "%");
    	searchKey=searchKey.replaceAll("questionmark", "?");
    	return kitRepo.searchByCat(searchKey, new PageRequest(page, 10));
    }
    
    @GetMapping("/searchByName/{searchKey}/{page}")
	public List<Kit> searchByName(@PathVariable(value = "searchKey") String searchKey, @PathVariable(value = "page") int page) {
    	searchKey=searchKey.replaceAll("slash", "/");
    	searchKey=searchKey.replaceAll("whitespace", " ");
    	searchKey=searchKey.replaceAll("pong", "#");
    	searchKey=searchKey.replaceAll("percent", "%");
    	searchKey=searchKey.replaceAll("questionmark", "?");
        return kitRepo.searchByName(searchKey, new PageRequest(page, 10));
    }
    
    @GetMapping("/searchKitCount/{searchKey}/")
    public Long searchKitCount(@PathVariable(value="searchKey") String searchKey){
    	searchKey=searchKey.replaceAll("slash", "/");
    	searchKey=searchKey.replaceAll("whitespace", " ");
    	searchKey=searchKey.replaceAll("pong", "#");
    	searchKey=searchKey.replaceAll("percent", "%");
    	searchKey=searchKey.replaceAll("questionmark", "?");
    	return kitRepo.searchKitCount(searchKey);
    }
    
    @GetMapping("/searchByCatCount/{searchKey}/")
    public Long searchByCatCount(@PathVariable(value="searchKey") String searchKey){
    	searchKey=searchKey.replaceAll("slash", "/");
    	searchKey=searchKey.replaceAll("whitespace", " ");
    	searchKey=searchKey.replaceAll("pong", "#");
    	searchKey=searchKey.replaceAll("percent", "%");
    	searchKey=searchKey.replaceAll("questionmark", "?");
    	return kitRepo.searchByCatCount(searchKey);
    }
    
    @GetMapping("/searchByNameCount/{searchKey}/")
    public Long searchByNameCount(@PathVariable(value="searchKey") String searchKey){
    	searchKey=searchKey.replaceAll("slash", "/");
    	searchKey=searchKey.replaceAll("whitespace", " ");
    	searchKey=searchKey.replaceAll("pong", "#");
    	searchKey=searchKey.replaceAll("percent", "%");
    	searchKey=searchKey.replaceAll("questionmark", "?");
    	return kitRepo.searchByNameCount(searchKey);
    }
    
    
    @GetMapping("/searchSingleKitByCat/{cat}")
	public Kit searchSingleKitByCat(@PathVariable(value = "cat")String cat) {
        return kitRepo.searchSingleKitByCat(cat);
    }
    

    
    @PostMapping("/add")
    public Kit createKit(@Valid @RequestBody Kit kit) {
        return kitRepo.save(kit);
    }
    
    @GetMapping("/get/{id}")
    public ResponseEntity<Kit> getKitById(@PathVariable(value = "id") Long KitId) {
    	Kit kit = kitRepo.findOne(KitId);
        if(kit == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(kit);
    }
    
    @PutMapping("/update/{id}")
    public ResponseEntity<Kit> updateKit(@PathVariable(value = "id") Long  KitId, 
                                           @Valid @RequestBody Kit kitDetail) {
    	Kit kit= kitRepo.findOne(KitId);
        if(kit == null) {
            return ResponseEntity.notFound().build();
        }
        
        kit.setDbid(kitDetail.getDbid());
        kit.setCat(kitDetail.getCat());
        kit.setName(kitDetail.getName());
        kit.setDescription(kitDetail.getDescription());
        kit.setClientspecific(kitDetail.getClientspecific());
        kit.setMolecular(kitDetail.getMolecular());
        kit.setBiosimilar(kitDetail.getBiosimilar());
        kit.setMethod(kitDetail.getMethod());
        kit.setSize(kitDetail.getSize());
        kit.setStatus(kitDetail.getStatus());
        kit.setPrice(kitDetail.getPrice()); 
        kit.setEnteredby(kitDetail.getEnteredby());
        kit.setReviewedby(kitDetail.getReviewedby());
        kit.setReviewcomment(kitDetail.getReviewcomment());
        kit.setApprovedby(kitDetail.getApprovedby());
        kit.setApprovecomment(kitDetail.getApprovecomment());       
        kit.setLastmodify(kitDetail.getLastmodify());
        kit.setEditstatus(kitDetail.getEditstatus());
        kit.setComment(kitDetail.getComment());
        kit.setReviewtime(kitDetail.getReviewtime());
        kit.setApprovetime(kitDetail.getApprovetime());
        
        Kit UpdateKit = kitRepo.save(kit);
        return ResponseEntity.ok(UpdateKit);
    }
    
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Kit> deleteKit(@PathVariable(value = "id") Long KitId) {
    	Kit kit = kitRepo.findOne(KitId);
        if(kit == null) {
            return ResponseEntity.notFound().build();
        }

        kitRepo.delete(kit);
        return ResponseEntity.ok().build();
    }
}
