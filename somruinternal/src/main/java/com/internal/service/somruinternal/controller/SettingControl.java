package com.internal.service.somruinternal.controller;

import static org.springframework.data.domain.ExampleMatcher.matching;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.internal.service.somruinternal.model.SaleLink;
import com.internal.service.somruinternal.model.Setting;
import com.internal.service.somruinternal.repository.SettingRepository;

//@CrossOrigin(origins = "http://localhost:4200")
//@CrossOrigin(origins = "http://192.168.0.46:4200")
@RestController
@RequestMapping("/setting")
public class SettingControl {
	@Autowired
	SettingRepository settingRepo;
    
    @GetMapping("/all")
    public List<Setting> getAllsettings() {
        return settingRepo.findAll();
    }
    
    @PostMapping("/add")
    public Setting createsetting(@Valid @RequestBody Setting setting) {
		return settingRepo.save(setting);
    }
    
    @GetMapping("/get/{id}")
    public ResponseEntity<Setting> getSettingById(@PathVariable(value = "id") Long settingId) {
    	Setting setting = settingRepo.findOne(settingId);
        if(setting == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(setting);
    }
    
    @GetMapping("/getByPage/{page}")
    public List<Setting> getSettingByPage(@PathVariable(value = "page") String settingPage) {
    	Example<Setting> example = Example.of(new Setting(-1, settingPage, "", "", "", "", "", null), matching(). //
				withIgnorePaths("dbid", "type", "value", "pending", "requestDel", "editby", "modify"));
    	return settingRepo.findAll(example);
    }
    
    @GetMapping("/getByPageAndType/{page}/{type}")
    public Setting getSettingByPageAndType(@PathVariable(value = "page") String settingPage, @PathVariable(value = "type") String type) {
    	Example<Setting> example = Example.of(new Setting(-1, settingPage, type, "", "", "", "", null), matching(). //
				withIgnorePaths("dbid", "value", "pending", "requestDel", "editby", "modify"));
    	return settingRepo.findOne(example);
    }
    
    @PutMapping("/update/{id}")
    public ResponseEntity<Setting> updatesetting(@PathVariable(value = "id") Long  settingId, 
                                           @Valid @RequestBody Setting settingDetail) {
    	Setting setting= settingRepo.findOne(settingId);
        if(setting == null) {
            return ResponseEntity.notFound().build();
        }
        
        setting.setDbid(settingDetail.getDbid());
        setting.setType(settingDetail.getType());
        setting.setPage(settingDetail.getPage());
        setting.setValue(settingDetail.getValue());
        setting.setPending(settingDetail.getPending());
        setting.setRequestDel(settingDetail.getRequestDel());
        setting.setEditby(settingDetail.getEditby());
        setting.setModify(settingDetail.getModify());        
 
        Setting Updatesetting = settingRepo.save(setting);
        return ResponseEntity.ok(Updatesetting);
    }
    
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Setting> deletesetting(@PathVariable(value = "id") Long settingId) {
    	Setting setting = settingRepo.findOne(settingId);
        if(setting == null) {
            return ResponseEntity.notFound().build();
        }

        settingRepo.delete(setting);
        return ResponseEntity.ok().build();
    }
}
