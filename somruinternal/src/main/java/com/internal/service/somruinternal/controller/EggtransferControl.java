package com.internal.service.somruinternal.controller;

import static org.springframework.data.domain.ExampleMatcher.matching;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.internal.service.somruinternal.model.Egg;
import com.internal.service.somruinternal.model.Eggtransfer;
import com.internal.service.somruinternal.repository.EggtransferRepository;

@RestController
@RequestMapping("/eggtransfer")
public class EggtransferControl {
	
	@Autowired
	EggtransferRepository eggtransferRepository;

	
    @GetMapping("/all")
	public List<Eggtransfer> getAlleggtransfers() {
        return eggtransferRepository.findAll();
    }
    
    @GetMapping("/count")
	public int getCount() {
        return eggtransferRepository.findAll().size();
    }
    
    @GetMapping("/get/{id}")
    public ResponseEntity<Eggtransfer> getEggtransferByDbid(@PathVariable(value = "id") Long eggtransferdbid) {
    	Eggtransfer eggtransfer = eggtransferRepository.findOne(eggtransferdbid);
        if(eggtransfer == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(eggtransfer);
    }
    
    @GetMapping("/getEggtransfersByChickenId/{id}")
    public List<Eggtransfer> getEggtransfersByChickenId(@PathVariable(value = "id") String chickenid) {
    	Example<Eggtransfer> example = Example.of(new Eggtransfer((long)-1, chickenid, "", "", "", "", "", "", new Date()), matching(). //
				withIgnorePaths("dbid", "eggdbid", "action", "egguseamount", "destinationtable", "destinationdbid", "editby", "modify"));
    	return eggtransferRepository.findAll(example);
    }   
    
    @PostMapping("/add")
    public Eggtransfer createEggtransfer(@Valid @RequestBody Eggtransfer eggtransfer) {
        return eggtransferRepository.save(eggtransfer);
    }
    
    @PutMapping("/update/{id}")
    public ResponseEntity<Eggtransfer> updateEggtransfer(@PathVariable(value = "id") Long  eggtransferdbid, 
                                           @Valid @RequestBody Eggtransfer eggtransferDetail) {
    	Eggtransfer eggtransfer= eggtransferRepository.findOne(eggtransferdbid);
        if(eggtransfer == null) {
            return ResponseEntity.notFound().build();
        }
        
        eggtransfer.setDbid(eggtransferDetail.getDbid());
        eggtransfer.setChickenid(eggtransferDetail.getChickenid());
        eggtransfer.setEggdbid(eggtransferDetail.getEggdbid());
        eggtransfer.setAction(eggtransferDetail.getAction());
        eggtransfer.setEgguseamount(eggtransferDetail.getEgguseamount());
        eggtransfer.setDestinationtable(eggtransferDetail.getDestinationtable());
        eggtransfer.setDestinationdbid(eggtransferDetail.getDestinationdbid());
        eggtransfer.setEditby(eggtransferDetail.getEditby());
        eggtransfer.setModify(eggtransferDetail.getModify());

        
        Eggtransfer UpdateEggtransfer = eggtransferRepository.save(eggtransfer);
        return ResponseEntity.ok(UpdateEggtransfer);
    }
    
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Eggtransfer> deleteEggtransfer(@PathVariable(value = "id") Long Eggtransferdbid) {
    	Eggtransfer chicken = eggtransferRepository.findOne(Eggtransferdbid);
        if(chicken == null) {
            return ResponseEntity.notFound().build();
        }

        eggtransferRepository.delete(chicken);
        return ResponseEntity.ok().build();
    }
}
