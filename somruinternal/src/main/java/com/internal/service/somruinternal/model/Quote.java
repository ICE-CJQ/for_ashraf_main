package com.internal.service.somruinternal.model;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

import java.util.Date;

@Entity
@Table(name = "Quote")
@EntityListeners(AuditingEntityListener.class)
public class Quote {
	public Quote(long dbid, String currency, double currencyrate, String quoteNum, long clientid, long saleRepid, Date createDate, Date expireDate,
			String paymentType, long revision, Date revisionDate, double shippingfee, double handlefee, double tax, String taxrate, boolean complete, String note, String editby, Date modify) {
		super();
		this.dbid = dbid;
		this.currency = currency;
		this.currencyrate = currencyrate;
		this.quoteNum = quoteNum;
		this.clientid = clientid;
		this.saleRepid = saleRepid;
		this.createDate = createDate;
		this.expireDate = expireDate;
		this.paymentType = paymentType;
		this.revision = revision;
		this.revisionDate = revisionDate;
		this.shippingfee=shippingfee;
		this.handlefee=handlefee;
		this.tax=tax;
		this.taxrate=taxrate;
		this.complete = complete;
		this.note =  note;
		this.editby = editby;
		this.modify =  modify;
	}
	
	public Quote() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long dbid;
	@Column(nullable = false)
	private String currency;
	@Column(nullable = false)
	private double currencyrate;
	@Column(nullable = false)
	private String quoteNum;
	@Column(nullable = false)
	private long clientid;
	@Column(nullable = false)
	private long saleRepid;
	@Column(nullable = false)
	private Date createDate;
	@Column(nullable = false)
	private Date expireDate;
	@NotBlank
	private String paymentType;
	@Column(nullable = false)
	private long revision;
	@Column(nullable = false)
	private Date revisionDate;
	
	@Column(nullable = false)
	private Double shippingfee;
	@Column(nullable = false)
	private Double handlefee;
	@Column(nullable = false)
	private Double tax;
	@Column(nullable = false)
	private String taxrate;
	@Column(nullable = false)
	private boolean complete;
	@Column(nullable = true,length=5000)
	private String note;
	@Column(nullable = true)
	private String editby;
	@Column(nullable = true)
	private Date modify;
	
	public long getClientid() {
		return clientid;
	}
	public void setClientid(long clientid) {
		this.clientid = clientid;
	}
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}



	public double getCurrencyrate() {
		return currencyrate;
	}

	public void setCurrencyrate(double currencyrate) {
		this.currencyrate = currencyrate;
	}

	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public long getRevision() {
		return revision;
	}
	public void setRevision(long revision) {
		this.revision = revision;
	}

	public String getQuoteNum() {
		return quoteNum;
	}

	public void setQuoteNum(String quoteNum) {
		this.quoteNum = quoteNum;
	}

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	public long getDbid() {
		return dbid;
	}

	public void setDbid(long dbid) {
		this.dbid = dbid;
	}

	public Date getRevisionDate() {
		return revisionDate;
	}

	public void setRevisionDate(Date revisionDate) {
		this.revisionDate = revisionDate;
	}

	public long getSaleRepid() {
		return saleRepid;
	}

	public void setSaleRepid(long saleRepid) {
		this.saleRepid = saleRepid;
	}

	public Double getShippingfee() {
		return shippingfee;
	}

	public void setShippingfee(Double shippingfee) {
		this.shippingfee = shippingfee;
	}

	public Double getHandlefee() {
		return handlefee;
	}

	public void setHandlefee(Double handlefee) {
		this.handlefee = handlefee;
	}

	public Double getTax() {
		return tax;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}

	public String getTaxrate() {
		return taxrate;
	}

	public void setTaxrate(String taxrate) {
		this.taxrate = taxrate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getEditby() {
		return editby;
	}

	public void setEditby(String editby) {
		this.editby = editby;
	}

	public Date getModify() {
		return modify;
	}

	public void setModify(Date modify) {
		this.modify = modify;
	}

	

}
