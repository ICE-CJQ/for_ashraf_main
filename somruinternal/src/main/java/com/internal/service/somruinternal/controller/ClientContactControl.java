package com.internal.service.somruinternal.controller;

import static org.springframework.data.domain.ExampleMatcher.matching;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.internal.service.somruinternal.model.ClientContact;
import com.internal.service.somruinternal.repository.ClientContactRepository;

@RestController
@RequestMapping("/clientcontact")
public class ClientContactControl {
	@Autowired
    ClientContactRepository clientRepo;
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/all")
    public List<ClientContact> getAllclients() {
        return clientRepo.findAll();
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/add")
    public ClientContact createclient(@Valid @RequestBody ClientContact client) {
        return clientRepo.save(client);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/get/{id}")
    public ResponseEntity<ClientContact> getclientById(@PathVariable(value = "id") Long clientId) {
    	ClientContact client = clientRepo.findOne(clientId);
        if(client == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(client);
    }
    
    @GetMapping("/getContactByCompany/{id}")
    public List<ClientContact> getContactByCompany(@PathVariable(value = "id") Long compId) {
    	Example<ClientContact> example = Example.of(new ClientContact((long)-1, compId, "", "", "", "", "", "","",null), matching(). //
				withIgnorePaths("dbid", "name", "role", "email", "phone", "ext", "fax","editby", "modify"));
    	return clientRepo.findAll(example);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<ClientContact> updateclient(@PathVariable(value = "id") Long  clientId, 
                                           @Valid @RequestBody ClientContact clientDetail) {
    	ClientContact client= clientRepo.findOne(clientId);
        if(client == null) {
            return ResponseEntity.notFound().build();
        }
        
        client.setDbid(clientDetail.getDbid());
        client.setCompanyId(clientDetail.getCompanyId());
        client.setName(clientDetail.getName());
        client.setRole(clientDetail.getRole());
        client.setEmail(clientDetail.getEmail());
        client.setPhone(clientDetail.getPhone());
        client.setEditby(clientDetail.getEditby());
        client.setModify(clientDetail.getModify());

        ClientContact Updateclient = clientRepo.save(client);
        return ResponseEntity.ok(Updateclient);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ClientContact> deleteclient(@PathVariable(value = "id") Long clientId) {
    	ClientContact client = clientRepo.findOne(clientId);
        if(client == null) {
            return ResponseEntity.notFound().build();
        }

        clientRepo.delete(client);
        return ResponseEntity.ok().build();
    }
}
