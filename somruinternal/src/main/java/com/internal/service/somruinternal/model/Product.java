package com.internal.service.somruinternal.model;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

import java.util.Date;

@Entity
@Table(name = "Product")
@EntityListeners(AuditingEntityListener.class)
public class Product {
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Product(long dbid, String name, String type, String description, String cat, String oldcat, String unit, String host, String clonality, String isotype, String immunogen,  String purification, String buffer, String specificity, String reconstitution, String storage, String unitsize, String unitprice, String comment, String packsize,
			String status,String enteredby, String reviewedby, String approvedby, Date entertime, Date reviewtime, Date approvetime, String reviewcomment, String approvecomment) {
		super();
		this.dbid = dbid;
		this.name=name;
		this.type=type;
		this.description=description;
		this.cat=cat;
		this.oldcat=oldcat;
		this.unit=unit;
		this.host=host;
		this.clonality=clonality;
		this.isotype=isotype;
		this.immunogen=immunogen;
		this.purification=purification;
		this.buffer=buffer;
		this.specificity=specificity;
		this.reconstitution=reconstitution;
		this.storage=storage;
		this.unitsize=unitsize;
		this.unitprice=unitprice;
		this.comment=comment;
		this.packsize=packsize;
		this.status=status;
		this.enteredby=enteredby;
		this.reviewedby=reviewedby;
		this.approvedby=approvedby;
		this.entertime=entertime;
		this.reviewtime=reviewtime;
		this.approvetime=approvetime;
		this.reviewcomment=reviewcomment;
		this.approvecomment=approvecomment;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	@Column(nullable = true)
	private String name;
	@Column(nullable = true)
	private String type;	
	@Column(nullable = true)
	private String description;
	@Column(nullable = true)
	private String cat;
	@Column(nullable = true)
	private String oldcat;	
	@Column(nullable = true)
	private String unit;
	@Column(nullable = true)
	private String host;
	@Column(nullable = true)
	private String clonality;
	@Column(nullable = true)
	private String isotype;
	@Column(nullable = true)
	private String immunogen;
	@Column(nullable = true)
	private String purification;
	@Column(nullable = true)
	private String buffer;
	@Column(nullable = true)
	private String specificity;
	@Column(nullable = true)
	private String reconstitution;
	@Column(nullable = true, length=2000)
	private String storage;
	@Column(nullable = true)
	private String unitsize;
	@Column(nullable = true)
	private String unitprice;
	@Column(nullable = true, length=2000)
	private String comment;
	@Column(nullable = true)
	private String packsize;
	

	@Column(nullable = true)
	private String status;
	@Column(nullable = true)
	private String enteredby;	
	@Column(nullable = true)
	private String reviewedby;
	@Column(nullable = true)
	private String approvedby;
	@Column(nullable = true)
	private Date entertime;
	@Column(nullable = true)
	private Date reviewtime;
	@Column(nullable = true)
	private Date approvetime;
	@Column(nullable = true)
	private String reviewcomment;
	@Column(nullable = true)
	private String approvecomment;
	
	public long getDbid() {
		return dbid;
	}
	public void setDbid(long dbid) {
		this.dbid = dbid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCat() {
		return cat;
	}
	public void setCat(String cat) {
		this.cat = cat;
	}
	public String getOldcat() {
		return oldcat;
	}
	public void setOldcat(String oldcat) {
		this.oldcat = oldcat;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getClonality() {
		return clonality;
	}
	public void setClonality(String clonality) {
		this.clonality = clonality;
	}
	public String getIsotype() {
		return isotype;
	}
	public void setIsotype(String isotype) {
		this.isotype = isotype;
	}
	public String getImmunogen() {
		return immunogen;
	}
	public void setImmunogen(String immunogen) {
		this.immunogen = immunogen;
	}
	public String getPurification() {
		return purification;
	}
	public void setPurification(String purification) {
		this.purification = purification;
	}
	public String getBuffer() {
		return buffer;
	}
	public void setBuffer(String buffer) {
		this.buffer = buffer;
	}
	public String getSpecificity() {
		return specificity;
	}
	public void setSpecificity(String specificity) {
		this.specificity = specificity;
	}
	
	public String getReconstitution() {
		return reconstitution;
	}
	public void setReconstitution(String reconstitution) {
		this.reconstitution = reconstitution;
	}
	public String getStorage() {
		return storage;
	}
	public void setStorage(String storage) {
		this.storage = storage;
	}
	public String getUnitsize() {
		return unitsize;
	}
	public void setUnitsize(String unitsize) {
		this.unitsize = unitsize;
	}
	public String getUnitprice() {
		return unitprice;
	}
	public void setUnitprice(String unitprice) {
		this.unitprice = unitprice;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getPacksize() {
		return packsize;
	}
	public void setPacksize(String packsize) {
		this.packsize = packsize;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEnteredby() {
		return enteredby;
	}
	public void setEnteredby(String enteredby) {
		this.enteredby = enteredby;
	}
	public String getReviewedby() {
		return reviewedby;
	}
	public void setReviewedby(String reviewedby) {
		this.reviewedby = reviewedby;
	}
	public String getApprovedby() {
		return approvedby;
	}
	public void setApprovedby(String approvedby) {
		this.approvedby = approvedby;
	}
	public Date getEntertime() {
		return entertime;
	}
	public void setEntertime(Date entertime) {
		this.entertime = entertime;
	}
	public Date getReviewtime() {
		return reviewtime;
	}
	public void setReviewtime(Date reviewtime) {
		this.reviewtime = reviewtime;
	}
	public Date getApprovetime() {
		return approvetime;
	}
	public void setApprovetime(Date approvetime) {
		this.approvetime = approvetime;
	}
	public String getReviewcomment() {
		return reviewcomment;
	}
	public void setReviewcomment(String reviewcomment) {
		this.reviewcomment = reviewcomment;
	}
	public String getApprovecomment() {
		return approvecomment;
	}
	public void setApprovecomment(String approvecomment) {
		this.approvecomment = approvecomment;
	}
	


}
