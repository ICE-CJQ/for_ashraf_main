package com.internal.service.somruinternal.controller;

import com.internal.service.somruinternal.model.Itemdetail;
import com.internal.service.somruinternal.utils.sendEmailUtil;
import com.internal.service.somruinternal.model.Orderitem;
import com.internal.service.somruinternal.objects.emailDetail;

import com.internal.service.somruinternal.repository.OrderitemRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

import java.util.List;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.file.Files;

@RestController
@RequestMapping("/orderitem")
public class OrderitemControl {
    @Autowired
    OrderitemRepository OrderitemRepo;
    @Autowired
    public JavaMailSender javaMailSender;
	@Autowired
	sendEmailUtil sendemailutil;
    
//    @PutMapping("/noeta/")
//	public void noEtaEmail(@Valid @RequestBody emailDetail emaildetail) throws MessagingException {
//    	sendemailutil.sendEmail(sendEmailUtil.NO_ETA, null, null, emaildetail);
//    }
	
    @PutMapping("/sendemail/")
	public void sendEmail(@Valid @RequestBody emailDetail emaildetail) throws MessagingException {
    	if(emaildetail.getReason().equals("no eta")) {
    		sendemailutil.sendEmail(sendEmailUtil.NO_ETA, null, null, emaildetail);
    	}
    	else if(emaildetail.getReason().equals("not arrive")) {
    		sendemailutil.sendEmail(sendEmailUtil.NOT_ARRIVE, null, null, emaildetail);
    	}
    	else if(emaildetail.getReason().equals("not approve")){
    		sendemailutil.sendEmail(sendEmailUtil.UN_APPROVE, null, null, emaildetail);
    	}
    	else {
    		sendemailutil.sendEmail(sendEmailUtil.REQUEST_RESET_PASSWORD, null, null, emaildetail);
    	}
    	
    }
    
//    @PutMapping("/approvenotice/")
//	public void approveNotice(@Valid @RequestBody emailDetail emaildetail) throws MessagingException {
//    	sendemailutil.sendEmail(sendEmailUtil.UN_APPROVE, null, null, emaildetail);
//    }
    
    @GetMapping("/all")
    public List<Orderitem> getAllOrder() {
        return OrderitemRepo.findAll();
    }
    
    @GetMapping("/all/{status}")
    public List<Orderitem> getAllByStatus(@PathVariable(value = "status") String status) {
        return OrderitemRepo.loadOrderByStatus(status);
    }
    
    @GetMapping("/count/{status}")
    public int getCount(@PathVariable(value = "status") String status) {
    	return OrderitemRepo.loadOrderByStatus(status).size();
    }
    
    @GetMapping("/orderWStatus/{status}/{page}")
    public List<Orderitem> getOrdersWStatusByPage(@PathVariable(value = "status") String status, @PathVariable(value = "page") int page) {   	
    	if(status == "Requested"){
        	return OrderitemRepo.loadRequestedOrder(status,new PageRequest(page, 10, Sort.Direction.DESC, "dbid"));
        }
        else if(status == "Approved"){
        	return OrderitemRepo.loadApprovedOrder(status,new PageRequest(page, 10, Sort.Direction.DESC, "dbid"));
        }
        else if(status == "Ordered"){
        	return OrderitemRepo.loadOrderedOrder(status,new PageRequest(page, 10, Sort.Direction.DESC, "dbid"));
        }
        
    	return OrderitemRepo.loadReceivedOrder(status,new PageRequest(page, 10, Sort.Direction.DESC, "dbid"));
    }
    
    @GetMapping("/urgentOrderWStatus/{status}/{page}")
    public List<Orderitem> getUrgentOrdersWStatusByPage(@PathVariable(value = "status") String status, @PathVariable(value = "page") int page) {
    	if(status == "Requested"){
        	return OrderitemRepo.loadUrgentRequestedOrder(status,new PageRequest(page, 10));
        }
        else if(status == "Approved"){
        	return OrderitemRepo.loadUrgentApprovedOrder(status,new PageRequest(page, 10));
        }
        else if(status == "Ordered"){
        	return OrderitemRepo.loadUrgentOrderedOrder(status,new PageRequest(page, 10));
        }
        
    	return OrderitemRepo.loadUrgentReceivedOrder(status,new PageRequest(page, 10));
    }
    
    @GetMapping("/urgentOrderCount/{status}/")
    public Long getUrgentOrdersWStatusByPage(@PathVariable(value = "status") String status) {
    	if(status == "Requested"){
        	return OrderitemRepo.loadUrgentRequestedOrderCount(status);
        }
        else if(status == "Approved"){
        	return OrderitemRepo.loadUrgentApprovedOrderCount(status);
        }
        else if(status == "Ordered"){
        	return OrderitemRepo.loadUrgentOrderedOrderCount(status);
        }
        
    	return OrderitemRepo.loadUrgentReceivedOrderCount(status);
    }
    
    @GetMapping("/search/{status}/{searchKey}/{page}")
    public List<Orderitem> searchOrderItem(@PathVariable(value = "status") String status,@PathVariable(value = "searchKey") String searchKey, @PathVariable(value = "page") int page) {
    	searchKey=searchKey.replaceAll("slash", "/");
    	searchKey=searchKey.replaceAll("whitespace", " ");
    	searchKey=searchKey.replaceAll("pong", "#");
    	searchKey=searchKey.replaceAll("percent", "%");
    	searchKey=searchKey.replaceAll("questionmark", "?");
    	if(status == "Requested"){
        	return OrderitemRepo.searchRequestedOrder(status,searchKey, new PageRequest(page, 10));
        }
        else if(status == "Approved"){
        	return OrderitemRepo.searchApprovedOrder(status,searchKey,new PageRequest(page, 10));
        }
        else if(status == "Ordered"){
        	return OrderitemRepo.searchOrderedOrder(status,searchKey,new PageRequest(page, 10));
        }
        
    	return OrderitemRepo.searchReceivedOrder(status,searchKey,new PageRequest(page, 10));
    }
    
    @GetMapping("/search/{status}/{searchKey}/")
    public Long searchOrderItemCount(@PathVariable(value = "status") String status,@PathVariable(value = "searchKey") String searchKey) {
    	searchKey=searchKey.replaceAll("slash", "/");
    	searchKey=searchKey.replaceAll("whitespace", " ");
    	searchKey=searchKey.replaceAll("pong", "#");
    	searchKey=searchKey.replaceAll("percent", "%");
    	searchKey=searchKey.replaceAll("questionmark", "?");
    	if(status == "Requested"){
        	return OrderitemRepo.searchRequestedOrderCount(status,searchKey);
        }
        else if(status == "Approved"){
        	return OrderitemRepo.searchApprovedOrderCount(status,searchKey);
        }
        else if(status == "Ordered"){
        	return OrderitemRepo.searchOrderedOrderCount(status,searchKey);
        }
        
    	return OrderitemRepo.searchReceivedOrderCount(status,searchKey);
    }
    
    @PostMapping("/add")
    public Orderitem createDrug(@Valid @RequestBody Orderitem orderitem) {
    	return OrderitemRepo.save(orderitem);
    }
    
    @GetMapping("/get/{id}")
    public ResponseEntity<Orderitem> getDrugById(@PathVariable(value = "id") Long orderitemId) {
    	Orderitem orderitem = OrderitemRepo.findOne(orderitemId);
        if(orderitem == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(orderitem);
    }
    
    @PutMapping("/update/{id}")
    public ResponseEntity<Orderitem> updateDrug(@PathVariable(value = "id") Long  orderitemId, 
                                           @Valid @RequestBody Orderitem orderitemDetail) {
        Orderitem orderitem= OrderitemRepo.findOne(orderitemId);
        if(orderitem == null) {
            return ResponseEntity.notFound().build();
        }
        
        orderitem.setDbid(orderitemDetail.getDbid());
        orderitem.setCat(orderitemDetail.getCat());
        orderitem.setCategory(orderitemDetail.getCategory());
        orderitem.setSuppliercat(orderitemDetail.getSuppliercat());
        orderitem.setName(orderitemDetail.getName());
        orderitem.setType(orderitemDetail.getType());
        orderitem.setSupplier(orderitemDetail.getSupplier());
        orderitem.setManufacturer(orderitemDetail.getManufacturer());
        orderitem.setAmount(orderitemDetail.getAmount());   
        orderitem.setUnreceiveamount(orderitemDetail.getUnreceiveamount());   
        orderitem.setUnit(orderitemDetail.getUnit()); 
        orderitem.setUnitsize(orderitemDetail.getUnitsize()); 
        orderitem.setUnitprice(orderitemDetail.getUnitprice());
        orderitem.setGrantid(orderitemDetail.getGrantid());
        orderitem.setRequestperson(orderitemDetail.getRequestperson());
        orderitem.setApproveperson(orderitemDetail.getApproveperson());
        orderitem.setOrderperson(orderitemDetail.getOrderperson());
        orderitem.setStatus(orderitemDetail.getStatus()); 
        orderitem.setRequesttime(orderitemDetail.getRequesttime());
        orderitem.setApprovetime(orderitemDetail.getApprovetime());
        orderitem.setOrdertime(orderitemDetail.getOrdertime());
        orderitem.setReceivetime(orderitemDetail.getReceivetime());
        orderitem.setEta(orderitemDetail.getEta());
        orderitem.setEmailsendtime(orderitemDetail.getEmailsendtime());
        orderitem.setReceiveperson(orderitemDetail.getReceiveperson());
        orderitem.setUrgent(orderitemDetail.isUrgent());
        orderitem.setComment(orderitemDetail.getComment());
        orderitem.setReserve(orderitemDetail.getReserve());
        orderitem.setExist(orderitemDetail.getExist());
        
        Orderitem UpdateEquip = OrderitemRepo.save(orderitem);
        return ResponseEntity.ok(UpdateEquip);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Orderitem> deleteDrug(@PathVariable(value = "id") Long orderitemId) {
    	Orderitem orderitem = OrderitemRepo.findOne(orderitemId);
        if(orderitem == null) {
            return ResponseEntity.notFound().build();
        }

        OrderitemRepo.delete(orderitem);
        return ResponseEntity.ok().build();
    }
}
