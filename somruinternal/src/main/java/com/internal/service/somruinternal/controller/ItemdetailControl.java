package com.internal.service.somruinternal.controller;

import static org.springframework.data.domain.ExampleMatcher.matching;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.internal.service.somruinternal.model.Itemdetail;
import com.internal.service.somruinternal.repository.ItemdetailRepository;


@RestController
@RequestMapping("/Itemdetail")
public class ItemdetailControl {
	
	//private final Path rootLocation = Paths.get("C:/Users/succe/Desktop/Springboot0.1.2/src/main/resources");
	private final Path rootLocation = Paths.get("C:/Users/succe/Documents/internal-software/somruinternal/src/main/resources");
	//private final Path rootLocation = Paths.get("/var/www/upload/");
	
	@Autowired
	private ItemdetailRepository ItemdetailRepo;
	private ResourceLoader resourceLoader;
	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;
	
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private ServletContext servletContext;
    
	  @PutMapping("/uploadfile/{itemdetaildbid}")
	  public ResponseEntity<Itemdetail> handleFileUpload(@RequestParam("file") MultipartFile file, @PathVariable(value = "itemdetaildbid") Long  itemdetaildbid) {
	    String message = "";
	    
		    try {
			      Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()));
			    } catch (Exception e) {throw new RuntimeException(file.getOriginalFilename() +this.rootLocation+"FAIL!");}
	     
	      message = "You successfully uploaded " + file.getOriginalFilename() + "!";
	      
	      String location = rootLocation + "\\"+file.getOriginalFilename();
	    	Itemdetail itemdetail= ItemdetailRepo.findOne(itemdetaildbid);
	        if(itemdetail == null) {
	            return ResponseEntity.notFound().build();
	        }
	               
	        itemdetail.setConfile(location);
	        
	        Itemdetail UpdateItemdetail = ItemdetailRepo.save(itemdetail);
	        return ResponseEntity.ok(UpdateItemdetail);
	        
//	    } catch (Exception e) {
//	      message = "FAIL to upload " + file.getOriginalFilename() + "!";
//	      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
//	    }
	  }
	  
	  @PutMapping("/uploadrffile/{itemdetaildbid}")
	  public ResponseEntity<Itemdetail> handlerfFileUpload(@RequestParam("file") MultipartFile file, @PathVariable(value = "itemdetaildbid") Long  itemdetaildbid) {
	    String message = "";
		    try {
			      Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()));

			    } catch (Exception e) {throw new RuntimeException(file.getOriginalFilename() +this.rootLocation+"FAIL!");}
	     
	      message = "You successfully uploaded " + file.getOriginalFilename() + "!";
	      
	      String location = rootLocation + "\\"+file.getOriginalFilename();
	    	Itemdetail itemdetail= ItemdetailRepo.findOne(itemdetaildbid);
	        if(itemdetail == null) {
	            return ResponseEntity.notFound().build();
	        }
	        
	        itemdetail.setRffile(location);
	        
	        Itemdetail UpdateItemdetail = ItemdetailRepo.save(itemdetail);
	        return ResponseEntity.ok(UpdateItemdetail);
	  }
	  
	    @GetMapping("/getfile/{itemdetaildbid}")
	    	public ResponseEntity<Resource> getFilebyPath(@PathVariable(value = "itemdetaildbid") Long itemdetaildbid) throws IOException {
	    	Itemdetail itemdetail = ItemdetailRepo.findOne(itemdetaildbid);	

			File file = new File(itemdetail.getConfile());
			
	    	String fileName= file.getName();
			String filePath=itemdetail.getConfile();

			try {			
				file = ResourceUtils.getFile(filePath);
				FileSystemResource res = new FileSystemResource(file);
			    
		        return ResponseEntity.ok()
		        		.header(HttpHeaders.CONTENT_DISPOSITION,
		                        "attachment; filename=\"" + file.getName() + "\"")
		                .contentLength(file.length())
		                .contentType(MediaType.APPLICATION_PDF)
//		                .contentType(mediaType)
		                .body(res);
		        
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				System.out.println("no file");
				e.printStackTrace();
				//return null;
				return ResponseEntity.notFound().build();
			}

	    }
	    
	    @GetMapping("/getrffile/{itemdetaildbid}")
	    	public ResponseEntity<Resource> getrfFilebyPath(@PathVariable(value = "itemdetaildbid") Long itemdetaildbid) throws IOException {
	    	Itemdetail itemdetail = ItemdetailRepo.findOne(itemdetaildbid);	
			File file = new File(itemdetail.getRffile());
	    	String fileName= file.getName();
			String filePath=itemdetail.getRffile();
			try {
				file = ResourceUtils.getFile(filePath);
				FileSystemResource res = new FileSystemResource(file);
		        return ResponseEntity.ok()
		        		.header(HttpHeaders.CONTENT_DISPOSITION,
		                        "attachment; filename=\"" + file.getName() + "\"")
		                .contentLength(file.length())
		                .contentType(MediaType.APPLICATION_PDF)
		                .body(res);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return ResponseEntity.notFound().build();
			}
	    }
	    
	  
	
//	@CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/all")
	public List<Itemdetail> getAllItemDetails() {
        return ItemdetailRepo.findAll(new Sort(Sort.Direction.ASC, "dbid"));
    }

    @PostMapping("/add")
    public Itemdetail createItemDetail(@Valid @RequestBody Itemdetail itemdetail) {
        return ItemdetailRepo.save(itemdetail);
    }
    
    @GetMapping("/searchItemdetailsByItemid/{id}")
	public List<Itemdetail> loadDetailsWItemId(@PathVariable(value = "id") Long id) {
        return ItemdetailRepo.loadDetailsWItemId(id);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getItemDetailByItemDbid/{id}")
    public List<Itemdetail> getItemDetailByItemId(@PathVariable(value = "id") Long itemdbid) {
    	Example<Itemdetail> example = Example.of(new Itemdetail((long)-1, itemdbid, "", "", "", "", "", "", new Date(), new Date(), new Date(), "", "", "", "", "",
    			"","","","",new Date(),"","","","", "","","","","",
    			"", "", "", "", "", "", "", "", "", "", "", "", "", "","", "", new Date(), "", "", "","","","","","","", new Date(),""), matching(). //
				withIgnorePaths("dbid",  "itemcategory", "itemname", "itemtype", "supplier", "parentlotnumber", "lotnumber", "receivedate", "expiredate", "retestdate", "batchnumber", "storetemperature", "purity", "rfstatus", "rffile",
						"conjugatechemistry","biotintobiomoleculeratio","descriptionpreparation","conjugateprepperson","conjugateprepdate","moleculeweight","bindingactivity","conjugateincorporationratio","constatus","confile","bca","runnumber","biomoleculeinfo","conjugateinfo",
						"location","sublocation", "amount", "inuse", "concentration", "concentrationunit", 
			            "species","clonality", "host", "conjugate","iggdepletion", "purification","volume","volumeunit", "weight", "weightunit", "apcolumnprepdate","usagecolumn","columnnumber", "comment", "reserve","projectnumber","receiveperson","unit","unitsize","recon", "modifydate", "modifyperson"));
    	return ItemdetailRepo.findAll(example);
    }
    
    
    @GetMapping("/get/{id}")
    public ResponseEntity<Itemdetail> getDrugById(@PathVariable(value = "id") Long itemdetailId) {
    	Itemdetail itemdetail = ItemdetailRepo.findOne(itemdetailId);
        if(itemdetail == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(itemdetail);
    }
    
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<Itemdetail> updateItemDetail(@PathVariable(value = "id") Long  itemdetailid, 
                                           @Valid @RequestBody Itemdetail itemdetailinformation) {
    	Itemdetail itemdetail= ItemdetailRepo.findOne(itemdetailid);
        if(itemdetail == null) {
            return ResponseEntity.notFound().build();
        }
        
        itemdetail.setDbid(itemdetailinformation.getDbid());
        itemdetail.setItemdbid(itemdetailinformation.getItemdbid());
        itemdetail.setItemcategory(itemdetailinformation.getItemcategory()); 
        itemdetail.setItemname(itemdetailinformation.getItemname()); 
        itemdetail.setItemtype(itemdetailinformation.getItemtype()); 
        itemdetail.setSupplier(itemdetailinformation.getSupplier()); 
        itemdetail.setParentlotnumber(itemdetailinformation.getParentlotnumber());  
        itemdetail.setLotnumber(itemdetailinformation.getLotnumber());   
        itemdetail.setReceivedate(itemdetailinformation.getReceivedate());                        
        itemdetail.setExpiredate(itemdetailinformation.getExpiredate());
        
        itemdetail.setRetestdate(itemdetailinformation.getRetestdate());
        itemdetail.setBatchnumber(itemdetailinformation.getBatchnumber());
        itemdetail.setStoretemperature(itemdetailinformation.getStoretemperature());
        itemdetail.setPurity(itemdetailinformation.getPurity());
        itemdetail.setRfstatus(itemdetailinformation.getRfstatus());
        itemdetail.setRffile(itemdetailinformation.getRffile());
        
        itemdetail.setConjugatechemistry(itemdetailinformation.getConjugatechemistry());
        itemdetail.setBiotintobiomoleculeratio(itemdetailinformation.getBiotintobiomoleculeratio());
        itemdetail.setDescriptionpreparation(itemdetailinformation.getDescriptionpreparation());
        itemdetail.setConjugateprepperson(itemdetailinformation.getConjugateprepperson());
        itemdetail.setConjugateprepdate(itemdetailinformation.getConjugateprepdate());
        itemdetail.setMoleculeweight(itemdetailinformation.getMoleculeweight());
        itemdetail.setBindingactivity(itemdetailinformation.getBindingactivity());
        itemdetail.setConjugateincorporationratio(itemdetailinformation.getConjugateincorporationratio());
        itemdetail.setConstatus(itemdetailinformation.getConstatus());
        itemdetail.setConfile(itemdetailinformation.getConfile());
        itemdetail.setBca(itemdetailinformation.getBca());
        itemdetail.setRunnumber(itemdetailinformation.getRunnumber());
        itemdetail.setBiomoleculeinfo(itemdetailinformation.getBiomoleculeinfo());
        itemdetail.setConjugateinfo(itemdetailinformation.getConjugateinfo());
        
        itemdetail.setLocation(itemdetailinformation.getLocation());
        itemdetail.setSublocation(itemdetailinformation.getSublocation());
        itemdetail.setAmount(itemdetailinformation.getAmount());
        itemdetail.setInuse(itemdetailinformation.getInuse());
        itemdetail.setConcentration(itemdetailinformation.getConcentration());
        itemdetail.setConcentrationunit(itemdetailinformation.getConcentrationunit());
        itemdetail.setSpecies(itemdetailinformation.getSpecies());
        itemdetail.setClonality(itemdetailinformation.getClonality());
        itemdetail.setHost(itemdetailinformation.getHost());
        itemdetail.setConjugate(itemdetailinformation.getConjugate());
        itemdetail.setIggdepletion(itemdetailinformation.getIggdepletion());
        itemdetail.setPurification(itemdetailinformation.getPurification());
        itemdetail.setVolume(itemdetailinformation.getVolume());
        itemdetail.setVolumeunit(itemdetailinformation.getVolumeunit());
        itemdetail.setWeight(itemdetailinformation.getWeight());
        itemdetail.setWeightunit(itemdetailinformation.getWeightunit());
        itemdetail.setApcolumnprepdate(itemdetailinformation.getApcolumnprepdate());
        itemdetail.setUsagecolumn(itemdetailinformation.getUsagecolumn());
        itemdetail.setColumnnumber(itemdetailinformation.getColumnnumber());
        itemdetail.setComment(itemdetailinformation.getComment());
        itemdetail.setReserve(itemdetailinformation.getReserve());
        itemdetail.setProjectnumber(itemdetailinformation.getProjectnumber());
        itemdetail.setReceiveperson(itemdetailinformation.getReceiveperson());
        itemdetail.setUnit(itemdetailinformation.getUnit());
        itemdetail.setUnitsize(itemdetailinformation.getUnitsize());
        itemdetail.setRecon(itemdetailinformation.getRecon());
        itemdetail.setModifydate(itemdetailinformation.getModifydate());
        itemdetail.setModifyperson(itemdetailinformation.getModifyperson());
        
        Itemdetail UpdateKitLink = ItemdetailRepo.save(itemdetail);
        return ResponseEntity.ok(UpdateKitLink);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Itemdetail> deleteItemDetail(@PathVariable(value = "id") Long itemdetailid) {
    	Itemdetail itemdetail = ItemdetailRepo.findOne(itemdetailid);
        if(itemdetail == null) {
            return ResponseEntity.notFound().build();
        }

        ItemdetailRepo.delete(itemdetail);
        return ResponseEntity.ok().build();
    }
}
