package com.internal.service.somruinternal.repository;
import java.util.List;

import com.internal.service.somruinternal.model.Invoice;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long>{
	@Query(value="Select invoice from Invoice invoice Where CONVERT(invoice.invoiceNum, CHAR(50))  LIKE %:searchKey%")
	public List<Invoice> searchByNum(@Param(value="searchKey") String searchKey, Pageable pageable);
}
