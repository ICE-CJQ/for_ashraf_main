package com.internal.service.somruinternal.repository;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.internal.service.somruinternal.model.ClientCompany;

@Repository
public interface ClientCompanyRepository  extends JpaRepository<ClientCompany, Long>{
	@Query(value = "SELECT DISTINCT company FROM ClientCompany company where "
			+ "company.company LIKE %:searchkey% OR "
			+ "company.address LIKE %:searchkey% OR "
			+ "company.address2 LIKE %:searchkey% OR "
			+ "company.baddress LIKE %:searchkey% OR "
			+ "company.baddress2 LIKE %:searchkey% OR "
			+ "company.city LIKE %:searchkey% OR "
			+ "company.country LIKE %:searchkey% OR "
			+ "company.province LIKE %:searchkey% OR "
			+ "company.postalCode LIKE %:searchkey% OR "
			+ "company.bcity LIKE %:searchkey% OR "
			+ "company.bcountry LIKE %:searchkey% OR "
			+ "company.bprovince LIKE %:searchkey% OR "
			+ "company.bpostalCode LIKE %:searchkey%" +" ORDER By company.dbid DESC" ) 
    public List<ClientCompany> searchClient(@Param("searchkey") String searchkey, Pageable pageable);
//	
//	@Query(value = "SELECT COUNT(DISTINCT company) FROM ClientCompany company where "
//			+ "company.company LIKE %:searchkey% OR "
//			+ "company.address LIKE %:searchkey% OR "
//			+ "company.address2 LIKE %:searchkey% OR "
//			+ "company.baddress LIKE %:searchkey% OR "
//			+ "company.baddress2 LIKE %:searchkey% OR "
//			+ "company.city LIKE %:searchkey% OR "
//			+ "company.country LIKE %:searchkey% OR "
//			+ "company.province LIKE %:searchkey% OR "
//			+ "company.postalCode LIKE %:searchkey% OR "
//			+ "company.bcity LIKE %:searchkey% OR "
//			+ "company.bcountry LIKE %:searchkey% OR "
//			+ "company.bprovince LIKE %:searchkey% OR "
//			+ "company.bpostalCode LIKE %:searchkey%" ) 
//    public Long searchClientCount(@Param("searchkey") String searchkey);
	
	@Query(value = "SELECT DISTINCT company FROM ClientCompany company where company.company LIKE %:searchkey% ORDER By company.dbid DESC")
    public List<ClientCompany> searchByName(@Param("searchkey") String searchkey, Pageable pageable);
	
//	@Query(value = "SELECT COUNT(DISTINCT company) FROM ClientCompany company where company.company LIKE %:searchkey%")
//    public Long searchByNameCount(@Param("searchkey") String searchkey);
	
	@Query(value = "SELECT company FROM ClientCompany company where "
			+ "company.address LIKE %:searchkey% OR "
			+ "company.address2 LIKE %:searchkey% OR "
			+ "company.city LIKE %:searchkey% OR "
			+ "company.country LIKE %:searchkey% OR "
			+ "company.province LIKE %:searchkey% OR "
			+ "company.postalCode LIKE %:searchkey% "+" ORDER By company.dbid DESC") 
    public List<ClientCompany> searchByShipAddress(@Param("searchkey") String searchkey, Pageable pageable);
	
//	@Query(value = "SELECT COUNT(DISTINCT company) FROM ClientCompany company where "
//			+ "company.address LIKE %:searchkey% OR "
//			+ "company.address2 LIKE %:searchkey% OR "
//			+ "company.city LIKE %:searchkey% OR "
//			+ "company.country LIKE %:searchkey% OR "
//			+ "company.province LIKE %:searchkey% OR "
//			+ "company.postalCode LIKE %:searchkey% ") 
//    public Long searchByShipAddressCount(@Param("searchkey") String searchkey);
	
	@Query(value = "SELECT DISTINCT company FROM ClientCompany company where "
			+ "company.address2 LIKE %:searchkey% OR "
			+ "company.baddress LIKE %:searchkey% OR "
			+ "company.baddress2 LIKE %:searchkey% OR "
			+ "company.bcity LIKE %:searchkey% OR "
			+ "company.bcountry LIKE %:searchkey% OR "
			+ "company.bprovince LIKE %:searchkey% OR "
			+ "company.bpostalCode LIKE %:searchkey%"+" ORDER By company.dbid DESC") 
    public List<ClientCompany> searchByBillAddress(@Param("searchkey") String searchkey, Pageable pageable);
	
//	@Query(value = "SELECT COUNT(DISTINCT company) company FROM ClientCompany company where "
//			+ "company.address2 LIKE %:searchkey% OR "
//			+ "company.baddress LIKE %:searchkey% OR "
//			+ "company.baddress2 LIKE %:searchkey% OR "
//			+ "company.bcity LIKE %:searchkey% OR "
//			+ "company.bcountry LIKE %:searchkey% OR "
//			+ "company.bprovince LIKE %:searchkey% OR "
//			+ "company.bpostalCode LIKE %:searchkey%") 
//    public Long searchByBillAddressCount(@Param("searchkey") String searchkey);
	
	@Query(value = "SELECT DISTINCT company FROM ClientCompany company, ClientContact contacts where "
			+ "contacts.companyId = company.dbid AND"
			+ "(contacts.name LIKE %:searchkey% OR "
			+ "contacts.role LIKE %:searchkey% OR "
			+ "contacts.email LIKE %:searchkey% OR "
			+ "contacts.phone LIKE %:searchkey% OR "
			+ "contacts.ext LIKE %:searchkey% OR "
			+ "contacts.fax LIKE %:searchkey%)"+" ORDER By company.dbid DESC") 
    public List<ClientCompany> searchByContact(@Param("searchkey") String searchkey, Pageable pageable);
	
//	@Query(value = "SELECT COUNT(DISTINCT company) FROM ClientCompany company, ClientContact contacts where "
//			+ "contacts.companyId = company.dbid AND"
//			+ "(contacts.name LIKE %:searchkey% OR "
//			+ "contacts.role LIKE %:searchkey% OR "
//			+ "contacts.email LIKE %:searchkey% OR "
//			+ "contacts.phone LIKE %:searchkey% OR "
//			+ "contacts.ext LIKE %:searchkey% OR "
//			+ "contacts.fax LIKE %:searchkey%)") 
//    public Long searchByContactCount(@Param("searchkey") String searchkey);
}
