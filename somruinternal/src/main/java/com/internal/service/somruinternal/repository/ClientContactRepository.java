package com.internal.service.somruinternal.repository;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.internal.service.somruinternal.model.ClientCompany;
import com.internal.service.somruinternal.model.ClientContact;

@Repository
public interface ClientContactRepository  extends JpaRepository<ClientContact, Long>{
	@Query(value = "SELECT contact FROM ClientContact contact where contact.companyId = :companyId") 
    public List<ClientContact> loadContactWithComId(@Param("companyId") long companyId);
}
