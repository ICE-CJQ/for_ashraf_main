package com.internal.service.somruinternal.controller;
import java.util.Date;
import static org.springframework.data.domain.ExampleMatcher.matching;
import com.internal.service.somruinternal.model.Application;
import com.internal.service.somruinternal.model.Ingredient;
import com.internal.service.somruinternal.repository.ApplicationRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

@RestController
@RequestMapping("/application")
public class ApplicationControl {
    @Autowired
    ApplicationRepository applicationRepo;
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/all")
    public List<Application> getAllApplications() {
        return applicationRepo.findAll();
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/add")
    public Application createApplications(@Valid @RequestBody Application applications) {
        return applicationRepo.save(applications);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/get/{id}")
    public ResponseEntity<Application> getApplicationById(@PathVariable(value = "id") Long applicationId) {
    	Application application = applicationRepo.findOne(applicationId);
        if(application == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(application);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getApplicationsByProductdbid/{id}")
    public List<Application> getApplicationByProductDbid(@PathVariable(value = "id") Long productdbid) {
    	Example<Application> example = Example.of(new Application((long)-1, productdbid, "", "",  "", "","",  new Date()), matching(). //
				withIgnorePaths("dbid", "purpose", "recommenconcentration", "comment", "note", "editby","modify"));
    	return applicationRepo.findAll(example);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<Application> updateApplication(@PathVariable(value = "id") Long  applicationId, 
                                           @Valid @RequestBody Application applicationDetail) {
    	Application application= applicationRepo.findOne(applicationId);
        if(application == null) {
            return ResponseEntity.notFound().build();
        }
        
        application.setDbid(applicationDetail.getDbid());
        application.setProductdbid(applicationDetail.getProductdbid());
        application.setPurpose(applicationDetail.getPurpose());
        application.setRecommenconcentration(applicationDetail.getRecommenconcentration());
        application.setComment(applicationDetail.getComment());
        application.setNote(applicationDetail.getNote());
        application.setEditby(applicationDetail.getEditby());
        application.setModify(applicationDetail.getModify());
        
       Application Updateapplication = applicationRepo.save(application);
       return ResponseEntity.ok(Updateapplication);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Application> deleteApplication(@PathVariable(value = "id") Long applicationId) {
    	Application application = applicationRepo.findOne(applicationId);
        if(application == null) {
            return ResponseEntity.notFound().build();
        }

        applicationRepo.delete(application);
        return ResponseEntity.ok().build();
    }
}
