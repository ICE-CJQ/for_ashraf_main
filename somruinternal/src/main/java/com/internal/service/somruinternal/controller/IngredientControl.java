package com.internal.service.somruinternal.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import static org.springframework.data.domain.ExampleMatcher.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.internal.service.somruinternal.model.Ingredient;
import com.internal.service.somruinternal.model.KitComponent;
import com.internal.service.somruinternal.repository.IngredientRepository;

@RestController
@RequestMapping("/ingredient")
public class IngredientControl {
	
	@Autowired
	private IngredientRepository ingreRepo;
	
//	@CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/all")
	public List<Ingredient> getAllIngredients() {
        return ingreRepo.findAll();
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/add")
    public Ingredient createIngredient(@Valid @RequestBody Ingredient ingredient) {
        return ingreRepo.save(ingredient);
    }
    
    @GetMapping("/searchIngredientsByRecipeid/{id}")
	public List<Ingredient> searchIngredientsByRecipeid(@PathVariable(value = "id") Long id) {
        return ingreRepo.searchIngredientsByRecipeid(id);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/get/{id}")
    public ResponseEntity<Ingredient> getIngredientById(@PathVariable(value = "id") Long ingredientid) {
    	Ingredient ingredient = ingreRepo.findOne(ingredientid);
        if(ingredient == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(ingredient);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getByRecipeid/{id}")
    public List<Ingredient> findAllByRecipeid(@PathVariable(value = "id") Long recipeid){
    	Example<Ingredient> example = Example.of(new Ingredient((long)-1, recipeid, "", "", "", "", "", "", "","","","","", null), matching(). //
				withIgnorePaths("dbid", "itemname","recipename", "recipesomruid", "unitquan", "unitmeasuretype",  "reqquan", "requnit", "vendor","cat", "suppliercat", "editby", "modify"));
    	return ingreRepo.findAll(example);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<Ingredient> updateIngredient(@PathVariable(value = "id") Long  ingredientid, 
                                           @Valid @RequestBody Ingredient ingredientDetail) {
    	Ingredient ingredient= ingreRepo.findOne(ingredientid);
        if(ingredient == null) {
            return ResponseEntity.notFound().build();
        }
        
        ingredient.setDbid(ingredientDetail.getDbid());
        ingredient.setRecipeid(ingredientDetail.getRecipeid());
        ingredient.setItemname(ingredientDetail.getItemname());
        ingredient.setRecipename(ingredientDetail.getRecipename());
        ingredient.setRecipesomruid(ingredientDetail.getRecipesomruid());
        ingredient.setUnitquan(ingredientDetail.getUnitquan());
        ingredient.setUnitmeasuretype(ingredientDetail.getUnitmeasuretype());
        ingredient.setReqquan(ingredientDetail.getReqquan());
        ingredient.setRequnit(ingredientDetail.getRequnit());
        ingredient.setVendor(ingredientDetail.getVendor());
        ingredient.setCat(ingredientDetail.getCat());
        ingredient.setSuppliercat(ingredientDetail.getSuppliercat());
        ingredient.setEditby(ingredientDetail.getEditby());
        ingredient.setModify(ingredientDetail.getModify());
        
        Ingredient Updateingredient = ingreRepo.save(ingredient);
        return ResponseEntity.ok(Updateingredient);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Ingredient> deleteIngredient(@PathVariable(value = "id") Long ingredientid) {
    	Ingredient ingredient = ingreRepo.findOne(ingredientid);
        if(ingredient == null) {
            return ResponseEntity.notFound().build();
        }

        ingreRepo.delete(ingredient);
        return ResponseEntity.ok().build();
    }
}
