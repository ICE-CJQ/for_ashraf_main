package com.internal.service.somruinternal.model;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "Ingredient")
public class Ingredient {
	public Ingredient(Long dbid, Long recipeid, String itemname, String recipename, String recipesomruid, String unitquan, String unitmeasuretype, String reqquan, String requnit, String vendor, String cat, String suppliercat, String editby, Date modify) {
		super();
		this.dbid = dbid;
		this.recipeid = recipeid;
		this.itemname = itemname;
		this.recipename = recipename;
		this.recipesomruid = recipesomruid;
		this.unitquan = unitquan;
		this.unitmeasuretype = unitmeasuretype;
		this.reqquan = reqquan;
		this.requnit = requnit;		
		this.vendor = vendor;
		this.cat=cat;
		this.suppliercat=suppliercat;
		this.editby = editby;
		this.modify =  modify;
	}
	
	public Ingredient(){
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	
	@Column(nullable = true)
	private long recipeid;
	
	@Column(nullable = true)
	private String itemname;
	
	@Column(nullable = true)
	private String recipename;
	
	@Column(nullable = true)
	private String recipesomruid;
	
	@Column(nullable = true)
	private String unitquan;
	
	@Column(nullable = true)
	private String unitmeasuretype;
	
	@Column(nullable = true)
	private String reqquan;
	
	@Column(nullable = true)
	private String requnit;
	
	@Column(nullable = true)
	private String vendor;
	
	@Column(nullable = true)
	private String cat;
	
	@Column(nullable = true)
	private String suppliercat;
	
	@Column(nullable = true)
	private String editby;
	
	@Column(nullable = true)
	private Date modify;
	
	
	public long getDbid() {
		return dbid;
	}

	public void setDbid(long dbid) {
		this.dbid = dbid;
	}

	public long getRecipeid() {
		return recipeid;
	}

	public void setRecipeid(long recipeid) {
		this.recipeid = recipeid;
	}

	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	public String getRecipename() {
		return recipename;
	}

	public void setRecipename(String recipename) {
		this.recipename = recipename;
	}

	public String getRecipesomruid() {
		return recipesomruid;
	}

	public void setRecipesomruid(String recipesomruid) {
		this.recipesomruid = recipesomruid;
	}

	public String getUnitquan() {
		return unitquan;
	}

	public void setUnitquan(String unitquan) {
		this.unitquan = unitquan;
	}

	public String getUnitmeasuretype() {
		return unitmeasuretype;
	}

	public void setUnitmeasuretype(String unitmeasuretype) {
		this.unitmeasuretype = unitmeasuretype;
	}

	public String getReqquan() {
		return reqquan;
	}

	public void setReqquan(String reqquan) {
		this.reqquan = reqquan;
	}

	public String getRequnit() {
		return requnit;
	}

	public void setRequnit(String requnit) {
		this.requnit = requnit;
	}
	
	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	
	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}
	
	public String getSuppliercat() {
		return suppliercat;
	}

	public void setSuppliercat(String suppliercat) {
		this.suppliercat = suppliercat;
	}

	public String getEditby() {
		return editby;
	}

	public void setEditby(String editby) {
		this.editby = editby;
	}

	public Date getModify() {
		return modify;
	}

	public void setModify(Date modify) {
		this.modify = modify;
	}

	@Override
	public String toString() {
		return "Ingredient [dbid=" + dbid + ", recipeid=" + recipeid+ ", itemname=" + itemname + ", recipename=" + recipename + ", recipesomruid=" + recipesomruid + ", unitquan=" + unitquan + ", unitmeasuretype=" + unitmeasuretype + ",reqquan=" + reqquan + ", requnit=" + requnit + ", vendor=" + vendor + ", cat=" + cat + ", suppliercat=" + suppliercat + "]";
	}
}
