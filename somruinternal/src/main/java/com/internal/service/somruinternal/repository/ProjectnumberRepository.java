package com.internal.service.somruinternal.repository;
import com.internal.service.somruinternal.model.Projectnumber;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectnumberRepository extends JpaRepository<Projectnumber, Long>{

}
