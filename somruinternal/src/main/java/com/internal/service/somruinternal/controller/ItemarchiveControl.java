package com.internal.service.somruinternal.controller;

import static org.springframework.data.domain.ExampleMatcher.matching;

import com.internal.service.somruinternal.model.Itemarchive;
import com.internal.service.somruinternal.model.Itemdetailarchive;
import com.internal.service.somruinternal.repository.ItemarchiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/itemarchive")
public class ItemarchiveControl {

    @Autowired
    ItemarchiveRepository itemarchiverepo;   
 
    
    @GetMapping("/all")
    public List<Itemarchive> getAllDrugs() {
        return itemarchiverepo.findAll();
    }
    
  
    @PostMapping("/add")
    public Itemarchive createDrug(@Valid @RequestBody Itemarchive itemarchive) {
    	return itemarchiverepo.save(itemarchive);
    }
    

    
    @GetMapping("/get/{id}")
    public ResponseEntity<Itemarchive> getDrugById(@PathVariable(value = "id") Long itemarchiveId) {
    	Itemarchive itemarchive = itemarchiverepo.findOne(itemarchiveId);
        if(itemarchive == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(itemarchive);
    }
    

    @GetMapping("/getItemarchiveByItemdbid/{id}")
    public List<Itemarchive> getItemarchiveByItemdbid(@PathVariable(value = "id") Long itemdbid) {
    	Example<Itemarchive> example = Example.of(new Itemarchive((long)-1, "", new Date(), "", itemdbid, "", "", "","","", true, "",
    			"","","","","","","","",new Date(),"","" ), matching(). //
				withIgnorePaths("itemarchivedbid",  "action", "createtime", "createperson", "category", "cat","suppliercat", "name", "type","active",
						"clientspecific", "supplier", "manufacturer", "unit","unitprice","unitsize","quantitythreshold",
						"supplylink","manufacturerlink","lastmodify","modifyperson", "comment"));
    	return itemarchiverepo.findAll(example);
    }
    
    
    
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Itemarchive> deleteDrug(@PathVariable(value = "id") Long itemarchiveId) {
    	Itemarchive itemarchive = itemarchiverepo.findOne(itemarchiveId);
        if(itemarchive == null) {
            return ResponseEntity.notFound().build();
        }


        itemarchiverepo.delete(itemarchive);
        return ResponseEntity.ok().build();
    }
}
