package com.internal.service.somruinternal.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.internal.service.somruinternal.model.Ingredient;
import com.internal.service.somruinternal.model.KitComponent;

@Repository
public interface IngredientRepository  extends JpaRepository<Ingredient, Long>{
	
	@Query(value="Select ingredients From Ingredient ingredients Where ingredients.recipeid = :id")
	public List<Ingredient> searchIngredientsByRecipeid(@Param(value="id") Long id);
	
	@Query(value="Delete From Ingredient ingre Where ingre.recipeid = :id")
	public void removeIngreWRecipeId(@Param(value="id") Long id);
}
