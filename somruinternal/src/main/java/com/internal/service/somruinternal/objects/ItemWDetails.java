package com.internal.service.somruinternal.objects;

import java.util.List;

import com.internal.service.somruinternal.model.Inventory;
import com.internal.service.somruinternal.model.Itemdetail;

public class ItemWDetails {
	public ItemWDetails(Inventory item, List<Itemdetail> details) {
		super();
		this.item = item;
		this.details = details;
	}
	
	public Inventory item;
	public List<Itemdetail> details;
	
	public Inventory getItem() {
		return item;
	}
	public void setItem(Inventory item) {
		this.item = item;
	}
	public List<Itemdetail> getDetails() {
		return details;
	}
	public void setDetails(List<Itemdetail> details) {
		this.details = details;
	}
}
