package com.internal.service.somruinternal.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.internal.service.somruinternal.model.Kit;
import com.internal.service.somruinternal.model.Recipe;
import com.internal.service.somruinternal.repository.RecipeRepository;

@RestController
@RequestMapping("/recipe")
public class RecipeControl {
	
	@Autowired
	private RecipeRepository recipeRepo;
	
    @GetMapping("/all")
	public List<Recipe> getAllRecipes() {
        return recipeRepo.findAll();
    } 
    
    @GetMapping("/count")
    public int getCount(){
    	return recipeRepo.findAll().size();
    }
    
    @GetMapping("/searchSingleRecipeByName/{name}/")
	public Recipe searchSingleRecipeByName(@PathVariable(value = "name") String name) {
    	name=name.replaceAll("slash", "/");
    	name=name.replaceAll("whitespace", " ");
    	name=name.replaceAll("pong", "#");
    	name=name.replaceAll("percent", "%");
    	name=name.replaceAll("questionmark", "?");
        return recipeRepo.searchSingleRecipeByName(name);
    }
    
    @GetMapping("/searchSingleRecipeBySId/{somruid}/")
	public Recipe searchSingleRecipeBySId(@PathVariable(value = "somruid") String somruid) {
        return recipeRepo.searchSingleRecipeBySId(somruid);
    }
    
    @GetMapping("/searchSingleRecipeByDId/{dbid:.+}")
	public Recipe searchSingleRecipeByDId(@PathVariable(value = "dbid") Long dbid) {
        return recipeRepo.searchSingleRecipeByDId(dbid);
    }
    
    @GetMapping("/all/{page}")
    public List<Recipe> getRecipeByPage(@PathVariable(value="page" ) int page){
    	return recipeRepo.findAll(new PageRequest(page, 10, Sort.Direction.DESC, "dbid")).getContent();
    }
    
    @GetMapping("/search/{searchKey}/{page}")
    public List<Recipe> searchRecipe(@PathVariable(value="searchKey") String searchKey, @PathVariable(value="page" ) int page){
    	searchKey=searchKey.replaceAll("slash", "/");
    	searchKey=searchKey.replaceAll("whitespace", " ");
    	searchKey=searchKey.replaceAll("pong", "#");
    	searchKey=searchKey.replaceAll("percent", "%");
    	searchKey=searchKey.replaceAll("questionmark", "?");
    	return recipeRepo.searchRecipe(searchKey, new PageRequest(page, 10));
    }
       
    @GetMapping("/searchId/{searchKey}/{page}")
    public List<Recipe> searchById(@PathVariable(value="searchKey") String searchKey, @PathVariable(value="page" ) int page){
    	searchKey=searchKey.replaceAll("slash", "/");
    	searchKey=searchKey.replaceAll("whitespace", " ");
    	searchKey=searchKey.replaceAll("pong", "#");
    	searchKey=searchKey.replaceAll("percent", "%");
    	searchKey=searchKey.replaceAll("questionmark", "?");
    	return recipeRepo.searchById(searchKey, new PageRequest(page, 10));
    }
    
    @GetMapping("/searchName/{searchKey}/{page}")
    public List<Recipe> searchByName(@PathVariable(value="searchKey") String searchKey, @PathVariable(value="page" ) int page){
    	searchKey=searchKey.replaceAll("slash", "/");
    	searchKey=searchKey.replaceAll("whitespace", " ");
    	searchKey=searchKey.replaceAll("pong", "#");
    	searchKey=searchKey.replaceAll("percent", "%");
    	searchKey=searchKey.replaceAll("questionmark", "?");
    	return recipeRepo.searchByName(searchKey, new PageRequest(page, 10));
    }
    
    @GetMapping("/searchCount/{searchKey}/")
    public Long searchRecipenCount(@PathVariable(value="searchKey") String searchKey){
    	searchKey=searchKey.replaceAll("slash", "/");
    	searchKey=searchKey.replaceAll("whitespace", " ");
    	searchKey=searchKey.replaceAll("pong", "#");
    	searchKey=searchKey.replaceAll("percent", "%");
    	searchKey=searchKey.replaceAll("questionmark", "?");
    	return recipeRepo.searchRecipenCount(searchKey);
    }
    
    @GetMapping("/searchIdCount/{searchKey}/")
    public Long searchByIdCount(@PathVariable(value="searchKey") String searchKey){
    	searchKey=searchKey.replaceAll("slash", "/");
    	searchKey=searchKey.replaceAll("whitespace", " ");
    	searchKey=searchKey.replaceAll("pong", "#");
    	searchKey=searchKey.replaceAll("percent", "%");
    	searchKey=searchKey.replaceAll("questionmark", "?");
    	return recipeRepo.searchByIdCount(searchKey);
    }
    
    @GetMapping("/searchNameCount/{searchKey}/")
    public Long searchByNameCount(@PathVariable(value="searchKey") String searchKey){
    	searchKey=searchKey.replaceAll("slash", "/");
    	searchKey=searchKey.replaceAll("whitespace", " ");
    	searchKey=searchKey.replaceAll("pong", "#");
    	searchKey=searchKey.replaceAll("percent", "%");
    	searchKey=searchKey.replaceAll("questionmark", "?");
    	return recipeRepo.searchByNameCount(searchKey);
    }
    
    
    
    @PostMapping("/add")
    public Recipe createRecipe(@Valid @RequestBody Recipe recipe) {
        return recipeRepo.save(recipe);
    }	
    
    @GetMapping("/get/{id}")
    public ResponseEntity<Recipe> getRecipeById(@PathVariable(value = "id") Long recipeid) {
    	Recipe recipe = recipeRepo.findOne(recipeid);
        if(recipe == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(recipe);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<Recipe> updateRecipe(@PathVariable(value = "id") Long  Recipeid, 
                                           @Valid @RequestBody Recipe recipeDetail) {
    	Recipe recipe= recipeRepo.findOne(Recipeid);
        if(recipe == null) {
            return ResponseEntity.notFound().build();
        }
        
        recipe.setDbid(recipeDetail.getDbid());
        recipe.setSomruid(recipeDetail.getSomruid());
        recipe.setName(recipeDetail.getName());
        recipe.setVolumn(recipeDetail.getVolumn());  
        recipe.setUnit(recipeDetail.getUnit());         
        recipe.setDescription(recipeDetail.getDescription());
        recipe.setDirection(recipeDetail.getDirection());
        recipe.setEnteredby(recipeDetail.getEnteredby());
        recipe.setReviewedby(recipeDetail.getReviewedby());
        recipe.setApprovedby(recipeDetail.getApprovedby());
        recipe.setEntertime(recipeDetail.getEntertime());
        recipe.setReviewtime(recipeDetail.getReviewtime());
        recipe.setApprovetime(recipeDetail.getApprovetime());                  
        recipe.setReviewcomment(recipeDetail.getReviewcomment());
        recipe.setApprovecomment(recipeDetail.getApprovecomment());
        recipe.setEditstatus(recipeDetail.getEditstatus());
        
        Recipe UpdateRecipe = recipeRepo.save(recipe);
        return ResponseEntity.ok(UpdateRecipe);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Recipe> deleteRecipe(@PathVariable(value = "id") Long recipeid) {
    	Recipe recipe = recipeRepo.findOne(recipeid);
        if(recipe == null) {
            return ResponseEntity.notFound().build();
        }

        recipeRepo.delete(recipe);
        return ResponseEntity.ok().build();
    }
}
