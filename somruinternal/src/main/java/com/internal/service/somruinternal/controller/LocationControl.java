package com.internal.service.somruinternal.controller;

import com.internal.service.somruinternal.model.Location;
import com.internal.service.somruinternal.repository.LocationRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

@RestController
@RequestMapping("/location")
public class LocationControl {
    @Autowired
    LocationRepository locationRepo;
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/all")
    public List<Location> getAllLocations() {
        return locationRepo.findAll();
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/add")
    public Location createLocation(@Valid @RequestBody Location location) {
    	return locationRepo.save(location);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/get/{id}")
    public ResponseEntity<Location> getLocationById(@PathVariable(value = "id") Long locationId) {
    	Location location = locationRepo.findOne(locationId);
        if(location == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(location);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<Location> updateLocation(@PathVariable(value = "id") Long  locationId, 
                                           @Valid @RequestBody Location locationDetail) {
    	Location location= locationRepo.findOne(locationId);
        if(location == null) {
            return ResponseEntity.notFound().build();
        }
        
        location.setDbid(locationDetail.getDbid());
        location.setLocationname(locationDetail.getLocationname());

        Location UpdateLocation = locationRepo.save(location);
        return ResponseEntity.ok(UpdateLocation);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Location> deleteLocation(@PathVariable(value = "id") Long locationId) {
    	Location location = locationRepo.findOne(locationId);
        if(location == null) {
            return ResponseEntity.notFound().build();
        }

        locationRepo.delete(location);
        return ResponseEntity.ok().build();
    }
}
