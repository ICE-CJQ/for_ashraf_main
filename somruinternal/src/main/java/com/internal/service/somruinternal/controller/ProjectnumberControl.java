package com.internal.service.somruinternal.controller;
import com.internal.service.somruinternal.model.Projectnumber;
import com.internal.service.somruinternal.repository.ProjectnumberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/projectnumber")
public class ProjectnumberControl {
    @Autowired
    ProjectnumberRepository projectnumberRepo;
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/all")
    public List<Projectnumber> getAllProjectnumbers() {
        return projectnumberRepo.findAll();
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/add")
    public Projectnumber createProjectnumber(@Valid @RequestBody Projectnumber projectnumber) {
    	return projectnumberRepo.save(projectnumber);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/get/{id}")
    public ResponseEntity<Projectnumber> getProjectnumberById(@PathVariable(value = "id") Long projectnumberId) {
    	Projectnumber projectnumber = projectnumberRepo.findOne(projectnumberId);
        if(projectnumber == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(projectnumber);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<Projectnumber> updateProjectnumber(@PathVariable(value = "id") Long  projectnumberId, 
                                           @Valid @RequestBody Projectnumber projectnumberDetail) {
    	Projectnumber projectnumber= projectnumberRepo.findOne(projectnumberId);
        if(projectnumber == null) {
            return ResponseEntity.notFound().build();
        }
        
        projectnumber.setDbid(projectnumberDetail.getDbid());
        projectnumber.setProjectnumber(projectnumberDetail.getProjectnumber());

        Projectnumber UpdateProjectnumber = projectnumberRepo.save(projectnumber);
        return ResponseEntity.ok(UpdateProjectnumber);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Projectnumber> deleteLocation(@PathVariable(value = "id") Long projectnumberId) {
    	Projectnumber projectnumber = projectnumberRepo.findOne(projectnumberId);
        if(projectnumber == null) {
            return ResponseEntity.notFound().build();
        }

        projectnumberRepo.delete(projectnumber);
        return ResponseEntity.ok().build();
    }
}
