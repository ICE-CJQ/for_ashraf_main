package com.internal.service.somruinternal.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.internal.service.somruinternal.model.Setting;

@Repository
public interface SettingRepository  extends JpaRepository<Setting, Long>{

}
