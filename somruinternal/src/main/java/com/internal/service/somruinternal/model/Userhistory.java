package com.internal.service.somruinternal.model;
import org.hibernate.validator.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Userhistory")
@EntityListeners(AuditingEntityListener.class)
public class Userhistory {
	public Userhistory() {
		super();
	}
	
	public Userhistory(long dbid, Date time, String username, String component, String action, String description) {
		super();
		this.dbid = dbid;
		this.time = time;
		this.username = username;
		this.component = component;
		this.action = action;
		this.description = description;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	@Column(nullable = true)
	private Date time;
	@Column(nullable = true)
	private String username;
	@Column(nullable = true)
	private String component;
	@Column(nullable = true)
	private String action;
	@Column(nullable = true,length=5000)
	private String description;
	
	public long getDbid() {
		return dbid;
	}

	public void setDbid(long dbid) {
		this.dbid = dbid;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	

}