package com.internal.service.somruinternal.controller;

import static org.springframework.data.domain.ExampleMatcher.matching;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.internal.service.somruinternal.model.Chicken;
import com.internal.service.somruinternal.model.Injection;
import com.internal.service.somruinternal.model.Inventory;
import com.internal.service.somruinternal.model.Kit;
import com.internal.service.somruinternal.objects.ChickenWEggs;
import com.internal.service.somruinternal.objects.ItemWDetails;
import com.internal.service.somruinternal.model.Egg;
import com.internal.service.somruinternal.repository.ChickenRepository;
import com.internal.service.somruinternal.repository.EggRepository;

@RestController
@RequestMapping("/chicken")
public class ChickenControl {
	
	@Autowired
	ChickenRepository chickenRepo;
    @Autowired
    EggRepository eggRepo;
	
    @GetMapping("/all")
	public List<Chicken> getAllChickens() {
        return chickenRepo.findAll();
    }
    
    @GetMapping("/count")
	public int getCount() {
        return chickenRepo.findAll().size();
    }
    
    @GetMapping("/allByPage/{page}")
	public List<Chicken> getAllChickens(@PathVariable(value = "page") int page) {
        return chickenRepo.findAll(new PageRequest(page, 10, Sort.Direction.ASC, "dbid")).getContent();
    }
    
    @GetMapping("/allWEggsByPage/{page}")
    public List<ChickenWEggs> getAllChickenWEgg(@PathVariable(value="page") int page) {
    	
    	List<Chicken> chicken = chickenRepo.findAll(new PageRequest(page, 10, Sort.Direction.DESC, "sequence".split("-")[0])).getContent();

    	List<ChickenWEggs> result = new ArrayList();
    	int i = 0;
    	for(i = 0; i < chicken.size(); i++){
    		String chickenid = chicken.get(i).getChickenid();
    		result.add(new ChickenWEggs(chicken.get(i), eggRepo.loadEggsWChickenId(chickenid)));
    	}
    	
    	
    	return result;
    }
    
    @GetMapping("/get/{dbid}")
    public ResponseEntity<Chicken> getChickenByDbid(@PathVariable(value = "dbid") Long dbid) {
    	Chicken chicken = chickenRepo.findOne(dbid);
        if(chicken == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(chicken);
    }
    
    @GetMapping("/getChickenByChickenId/{id}")
    public Chicken getChickenByChickenId(@PathVariable(value = "id") String chickenid) {
    	Example<Chicken> example = Example.of(new Chicken((long)-1, chickenid, new Date(), "", "", "", "", "", "", "", new Date(), "", new Date(), (double)1.1, ""), matching(). //
				withIgnorePaths("dbid", "dateofbirth", "immunogen", "projectname", "chickenstatus", "totalegg", "eggused", "eggdiscard", "latesttiter", "titerdate", "editby","modify","sequence", "immunstatus"));
    	return chickenRepo.findOne(example);
    }
    
    @GetMapping("/getChickenByImmunogen/{immunogen}")
    public List<Chicken> getChickenByImmunogen(@PathVariable(value = "immunogen") String immunogen) {
    	Example<Chicken> example = Example.of(new Chicken((long)-1, "", new Date(), immunogen, "", "", "", "", "", "", new Date(), "", new Date(), (double)1.1, ""), matching(). //
				withIgnorePaths("dbid", "chickenid", "dateofbirth",  "projectname", "chickenstatus", "totalegg", "eggused", "eggdiscard", "latesttiter", "titerdate", "editby","modify","sequence", "immunstatus"));
    	return chickenRepo.findAll(example);
    }
   
    
    @GetMapping("/searchChickenById/{searchKey}/{page}")
	public List<ChickenWEggs> searchChickenById(@PathVariable(value = "searchKey") String searchKey, @PathVariable(value = "page") int page) {
    	List<Chicken> chicken = chickenRepo.searchChickenByChickenid(searchKey, new PageRequest(page, 10));

        List<ChickenWEggs> result = new ArrayList();
    	int i = 0;
    	for(i = 0; i < chicken.size(); i++){
    		String chickenid = chicken.get(i).getChickenid();
    		result.add(new ChickenWEggs(chicken.get(i), eggRepo.loadEggsWChickenId(chickenid)));
    	}
    	return result;
    }
    
    @GetMapping("/searchChickenByIdCount/{searchkey}/")
    public Long searchChickenByIdCount(@PathVariable(value="searchkey") String searchkey)  {
    	return chickenRepo.searchChickenCount(searchkey);
    }
    
    
    @PostMapping("/add")
    public Chicken createChicken(@Valid @RequestBody Chicken chicken) {
        return chickenRepo.save(chicken);
    }
    
   
    @PutMapping("/update/{id}")
    public ResponseEntity<Chicken> updateChicken(@PathVariable(value = "id") Long  Chickendbid, 
                                           @Valid @RequestBody Chicken chickenDetail) {
    	Chicken chicken= chickenRepo.findOne(Chickendbid);
        if(chicken == null) {
            return ResponseEntity.notFound().build();
        }
        
        chicken.setDbid(chickenDetail.getDbid());
        chicken.setChickenid(chickenDetail.getChickenid());
        chicken.setDateofbirth(chickenDetail.getDateofbirth());
        chicken.setImmunogen(chickenDetail.getImmunogen());
        chicken.setProjectname(chickenDetail.getProjectname());
        chicken.setChickenstatus(chickenDetail.getChickenstatus());
        chicken.setTotalegg(chickenDetail.getTotalegg());
        chicken.setEggused(chickenDetail.getEggused());
        chicken.setEggdiscard(chickenDetail.getEggdiscard());
        chicken.setLatesttiter(chickenDetail.getLatesttiter());
        chicken.setTiterdate(chickenDetail.getTiterdate());
        chicken.setEditby(chickenDetail.getEditby());
        chicken.setModify(chickenDetail.getModify());
        chicken.setSequence(chickenDetail.getSequence());
        chicken.setImmunstatus(chickenDetail.getImmunstatus());
        
        Chicken UpdateChicken = chickenRepo.save(chicken);
        return ResponseEntity.ok(UpdateChicken);
    }
    
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Chicken> deleteChicken(@PathVariable(value = "id") Long Chickendbid) {
    	Chicken chicken = chickenRepo.findOne(Chickendbid);
        if(chicken == null) {
            return ResponseEntity.notFound().build();
        }

        chickenRepo.delete(chicken);
        return ResponseEntity.ok().build();
    }
}
