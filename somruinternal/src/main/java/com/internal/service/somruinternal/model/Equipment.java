package com.internal.service.somruinternal.model;
import org.hibernate.validator.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Equipment")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"lastmodify"}, 
        allowGetters = true)
public class Equipment {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	@NotBlank
	private String id;
	@NotBlank
	private String serial;
	@Column(nullable = true)
	private String name;
	@Column(nullable = true)
	private String manufacture;
	@Column(nullable = true)
	private String model;
	@Column(nullable = true)
	private String currentLocation;
	@Column(nullable = true)
	private String comment;
	@Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date lastmodify;
	
	public Equipment(String id, String serial, String name, String manufacture, String model, String currentLocation, String comment, Date lastmodify){
		super();
		this.id = id;
		this.serial = serial;
		this.name = name;
		this.manufacture = manufacture;
		this.model = model;
		this.currentLocation = currentLocation;
		this.comment = comment;
		this.lastmodify = lastmodify;
	}
	
	public Equipment(){
		super();
	}
	
	public Long getDBId(){
		return this.dbid;
	}
	
	public String getId(){
		return this.id;
	}
	
	public String getSerial(){
		return this.serial;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getManufactor(){
		return this.manufacture;
	}
	
	public String getModel(){
		return this.model;
	}
	
	public String getCurrentLocation(){
		return this.currentLocation;
	}
	
	public String getComment(){
		return this.comment;
	}
	
	public Date getLastModify(){
		return this.lastmodify;
	}
	
	public void setDBId(Long dbid){
		this.dbid = dbid;
	}
	
	public void setId(String id){
		this.id = id;
	}
	
	public void setSerial(String serial){
		this.serial = serial;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setManufactor(String manufactor){
		this.manufacture = manufactor;
	}
	
	public void setModel(String model){
		this.model = model;
	}
	
	public void setCurrentLocation(String currentLocation){
		this.currentLocation = currentLocation;
	}
	
	public void setComment(String comment){
		this.comment = comment;
	}
	
	public void setLastModify(Date lastModify){
		this.lastmodify = lastModify;
	}
	
	public String toString(){
		return "Serial: "+this.serial+"\tName: "+this.name+"\tModel: "+this.model+"\tManufactor: "+this.manufacture+"\tcurrent Location: "+this.currentLocation+"\tComment: "+this.comment+"\tLast Modfiy: "+this.lastmodify;
	}
}
