package com.internal.service.somruinternal.controller;

import static org.springframework.data.domain.ExampleMatcher.matching;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.internal.service.somruinternal.model.Injection;
import com.internal.service.somruinternal.repository.InjectionRepository;

@RestController
@RequestMapping("/injection")
public class InjectionControl {
	
	@Autowired
	private InjectionRepository injectionRepo;
	
//	@CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/all")
	public List<Injection> getAllInjections() {
        return injectionRepo.findAll();
    }

    @PostMapping("/add")
    public Injection createInjection(@Valid @RequestBody Injection injection) {
        return injectionRepo.save(injection);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getInjectionByChickenId/{id}")
    public List<Injection> getInjectionByChickenId(@PathVariable(value = "id") String chickenid) {
    	Example<Injection> example = Example.of(new Injection((long)-1, chickenid, "", null, "", "","","","", "", null), matching(). //
				withIgnorePaths("dbid", "adjuvant", "drugamount", "injectdate", "daysafterimmune", "unit", "bloodtiter","complete", "editby", "modify"));
    	return injectionRepo.findAll(example);
    }
    
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<Injection> updateInjection(@PathVariable(value = "id") Long  injectiondbid, 
                                           @Valid @RequestBody Injection injectionDetail) {
    	Injection injection= injectionRepo.findOne(injectiondbid);
        if(injection == null) {
            return ResponseEntity.notFound().build();
        }
        
        injection.setDbid(injectionDetail.getDbid());
        injection.setChickenid(injectionDetail.getChickenid());
        injection.setAdjuvant(injectionDetail.getAdjuvant());
        injection.setInjectdate(injectionDetail.getInjectdate()); 
        injection.setDaysafterimmune(injectionDetail.getDaysafterimmune());
        injection.setDrugamount(injectionDetail.getDrugamount());  
        injection.setUnit(injectionDetail.getUnit()); 
        injection.setBloodtiter(injectionDetail.getBloodtiter()); 
        injection.setComplete(injectionDetail.getComplete()); 
        injection.setEditby(injectionDetail.getEditby()); 
        injection.setModify(injectionDetail.getModify());

        Injection UpdateInjection = injectionRepo.save(injection);
        return ResponseEntity.ok(UpdateInjection);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Injection> deleteInjection(@PathVariable(value = "id") Long injectionid) {
    	Injection injection = injectionRepo.findOne(injectionid);
        if(injection == null) {
            return ResponseEntity.notFound().build();
        }

        injectionRepo.delete(injection);
        return ResponseEntity.ok().build();
    }
}
