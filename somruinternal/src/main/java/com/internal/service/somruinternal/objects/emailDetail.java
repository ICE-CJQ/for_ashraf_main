package com.internal.service.somruinternal.objects;

import java.util.List;
import com.internal.service.somruinternal.model.Orderitem;

public class emailDetail {
    public String reason;
    public List<String> emailAddressList;
    public String subject;
    public String emailContent;
    
	public emailDetail(){
		super();
	}
	
	public emailDetail(String reason, List<String> emailAddressList, String subject, String emailContent) {
		super();
		this.reason = reason;
		this.emailAddressList = emailAddressList;
		this.subject=subject;
		this.emailContent = emailContent;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public List<String> getEmailAddressList() {
		return emailAddressList;
	}

	public void setEmailAddressList(List<String> emailAddressList) {
		this.emailAddressList = emailAddressList;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getEmailContent() {
		return emailContent;
	}

	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}
	


    
    

	


}
