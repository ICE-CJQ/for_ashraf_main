package com.internal.service.somruinternal.repository;
import com.internal.service.somruinternal.model.SaleLink;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaleLinkRepository extends JpaRepository<SaleLink, Long>{

}
