package com.internal.service.somruinternal.controller;

import static org.springframework.data.domain.ExampleMatcher.matching;

import com.internal.service.somruinternal.model.Ingredient;
import com.internal.service.somruinternal.model.Inventory;
import com.internal.service.somruinternal.model.Itemdetail;
import com.internal.service.somruinternal.model.KitComponent;
import com.internal.service.somruinternal.model.User;
import com.internal.service.somruinternal.objects.ItemWDetails;
import com.internal.service.somruinternal.repository.InventoryRepository;
import com.internal.service.somruinternal.repository.ItemdetailRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/invent")
public class InventoryControl {

    @Autowired
    InventoryRepository InventoryRepo;
    @Autowired
    ItemdetailRepository itemDetailRepo;
    
    @GetMapping("/all")
    public List<Inventory> getAllDrugs() {
        return InventoryRepo.findAll();
    }
    
    @GetMapping("/count")
    public long count() {
    	return InventoryRepo.searchByActiveCount();
//        return InventoryRepo.findAll().size();
    }
    
    @GetMapping("/allWDetails/{page}")
    public List<ItemWDetails> getAllDrugsWDetail(@PathVariable(value="page") int page) {
//    	List<Inventory> inven = InventoryRepo.findAll(new PageRequest(page, 10, Sort.Direction.DESC, "dbid")).getContent();
    	
    	List<Inventory> inven = InventoryRepo.searchByActive(new PageRequest(page, 10));
    	
        List<ItemWDetails> result = new ArrayList();
    	int i = 0;
    	for(i = 0; i < inven.size(); i++){
			long dbid = inven.get(i).getDbid();
			result.add(new ItemWDetails(inven.get(i), itemDetailRepo.loadDetailsWItemId(dbid)));
    	}
    	return result;
    }
    
//    @GetMapping("/getConjugateByItemName/{name}")
//    public Item getConjugateByItemName(@PathVariable(value = "name") Long itemname) {
//    	Example<Item> example = Example.of(new Item((long)-1, itemdbid, "", "", "", "", "", "", new Date(), new Date(), new Date(), "", "", "", "",     "","","","",new Date(),"","","","",        "", "", "", "", "", "", "", "", "", "", "", "", "", "","", "", new Date(), "", "", "","","","","","","", new Date(),""), matching(). //
//				withIgnorePaths("dbid",  "itemcategory", "itemname", "itemtype", "supplier", "parentlotnumber", "lotnumber", "receivedate", "expiredate", "retestdate", "batchnumber", "storetemperature", "purity", "rfstatus", 
//						"conjugatechemistry","biotintobiomoleculeratio","descriptionpreparation","conjugateprepperson","conjugateprepdate","moleculeweight","bindingactivity","conjugateincorporationratio","constatus",
//						"location","sublocation", "amount", "inuse", "concentration", "concentrationunit", 
//			            "species","clonality", "host", "conjugate","iggdepletion", "purification","volume","volumeunit", "weight", "weightunit", "apcolumnprepdate","usagecolumn","columnnumber", "comment", "reserve","projectnumber","receiveperson","unit","unitsize","recon", "modifydate", "modifyperson"));
//    	return InventoryRepo.findOne(example);
//    }
    
    @GetMapping("/searchWPage/{searchkey}/{page}")
    public List<ItemWDetails> search(@PathVariable(value="searchkey") String searchkey ,@PathVariable(value="page") int page) {
    	searchkey=searchkey.replaceAll("slash", "/");
    	searchkey=searchkey.replaceAll("whitespace", " ");
    	searchkey=searchkey.replaceAll("pong", "#");
    	searchkey=searchkey.replaceAll("percent", "%");
    	searchkey=searchkey.replaceAll("questionmark", "?");
    	List<Inventory> inven = InventoryRepo.searchInven(searchkey, new PageRequest(page, 10));

        List<ItemWDetails> result = new ArrayList();
    	int i = 0;
    	for(i = 0; i < inven.size(); i++){
    		long dbid = inven.get(i).getDbid();
    		result.add(new ItemWDetails(inven.get(i), itemDetailRepo.loadDetailsWItemId(dbid)));
    	}
    	return result;
    }
       
    
//    @GetMapping("/searchByNameWPage/{searchkey}/{page}")
//    @RequestMapping  (value = "/ex/bars", params = { "id", "page" }, method = GET)
//    @ResponseBody
//    public List<ItemWDetails> searchByName(@RequestParam(value="searchkey") String searchkey ,@RequestParam(value="page") int page) {
//    	searchkey=searchkey.replaceAll("slash", "/");
//    	searchkey=searchkey.replaceAll("whitespace", " ");
//    	searchkey=searchkey.replaceAll("pong", "#");
//    	searchkey=searchkey.replaceAll("percent", "%");
//    	searchkey=searchkey.replaceAll("questionmark", "?");
//    	List<Inventory> inven = InventoryRepo.searchByName(searchkey, new PageRequest(page, 10));
//    	
//        List<ItemWDetails> result = new ArrayList();
//    	int i = 0;
//    	for(i = 0; i < inven.size(); i++){
//    		long dbid = inven.get(i).getDbid();
//    		result.add(new ItemWDetails(inven.get(i), itemDetailRepo.loadDetailsWItemId(dbid)));
//    	}
//    	return result;
//    }
    
    
    @GetMapping("/searchByNameWPage/{searchkey}/{page}")
    public List<ItemWDetails> searchByName(@PathVariable(value="searchkey") String searchkey ,@PathVariable(value="page") int page) {
    	searchkey=searchkey.replaceAll("slash", "/");
    	searchkey=searchkey.replaceAll("whitespace", " ");
    	searchkey=searchkey.replaceAll("pong", "#");
    	searchkey=searchkey.replaceAll("percent", "%");
    	searchkey=searchkey.replaceAll("questionmark", "?");
    	List<Inventory> inven = InventoryRepo.searchByName(searchkey, new PageRequest(page, 10));
    	
        List<ItemWDetails> result = new ArrayList();
    	int i = 0;
    	for(i = 0; i < inven.size(); i++){
    		long dbid = inven.get(i).getDbid();
    		result.add(new ItemWDetails(inven.get(i), itemDetailRepo.loadDetailsWItemId(dbid)));
    	}
    	return result;
    }
    
    @GetMapping("/searchBySupplierorManufacturerWPage/{searchkey}/{page}")
    public List<ItemWDetails> searchBySupplierorManufacturerCount(@PathVariable(value="searchkey") String searchkey ,@PathVariable(value="page") int page) {
    	searchkey=searchkey.replaceAll("slash", "/");
    	searchkey=searchkey.replaceAll("whitespace", " ");
    	searchkey=searchkey.replaceAll("pong", "#");
    	searchkey=searchkey.replaceAll("percent", "%");
    	searchkey=searchkey.replaceAll("questionmark", "?");
    	List<Inventory> inven = InventoryRepo.searchBySupplierorManufacturer(searchkey, new PageRequest(page, 10));
    	
        List<ItemWDetails> result = new ArrayList();
    	int i = 0;
    	for(i = 0; i < inven.size(); i++){
    		long dbid = inven.get(i).getDbid();
    		result.add(new ItemWDetails(inven.get(i), itemDetailRepo.loadDetailsWItemId(dbid)));
    	}
    	return result;
    }
    
    @GetMapping("/searchByCatWPage/{searchkey}/{page}")
    public List<ItemWDetails> searchByCat(@PathVariable(value="searchkey") String searchkey ,@PathVariable(value="page") int page) {
    	searchkey=searchkey.replaceAll("slash", "/");
    	searchkey=searchkey.replaceAll("whitespace", " ");
    	searchkey=searchkey.replaceAll("pong", "#");
    	searchkey=searchkey.replaceAll("percent", "%");
    	searchkey=searchkey.replaceAll("questionmark", "?");
    	List<Inventory> inven = InventoryRepo.searchByCat(searchkey, new PageRequest(page, 10));
        List<ItemWDetails> result = new ArrayList();
    	int i = 0;
    	for(i = 0; i < inven.size(); i++){
    		long dbid = inven.get(i).getDbid();
    		result.add(new ItemWDetails(inven.get(i), itemDetailRepo.loadDetailsWItemId(dbid)));
    	}
    	return result;
    }
    
    @GetMapping("/searchByLocWPage/{searchkey}/{page}")
    public List<ItemWDetails> searchByLocation(@PathVariable(value="searchkey") String searchkey ,@PathVariable(value="page") int page) {
    	searchkey=searchkey.replaceAll("slash", "/");
    	searchkey=searchkey.replaceAll("whitespace", " ");
    	searchkey=searchkey.replaceAll("pong", "#");
    	searchkey=searchkey.replaceAll("percent", "%");
    	searchkey=searchkey.replaceAll("questionmark", "?");
    	List<Inventory> inven = InventoryRepo.searchByLocaton(searchkey, new PageRequest(page, 10));
        List<ItemWDetails> result = new ArrayList();
    	int i = 0;
    	for(i = 0; i < inven.size(); i++){
    		long dbid = inven.get(i).getDbid();
    		result.add(new ItemWDetails(inven.get(i), itemDetailRepo.loadDetailsWItemId(dbid)));
    	}
    	return result;
    } 
    
    @GetMapping("/searchBySubLocWPage/{searchkey}/{page}")
    public List<ItemWDetails> searchBySubLocation(@PathVariable(value="searchkey") String searchkey ,@PathVariable(value="page") int page) {
    	searchkey=searchkey.replaceAll("slash", "/");
    	searchkey=searchkey.replaceAll("whitespace", " ");
    	searchkey=searchkey.replaceAll("pong", "#");
    	searchkey=searchkey.replaceAll("percent", "%");
    	searchkey=searchkey.replaceAll("questionmark", "?");
    	List<Inventory> inven = InventoryRepo.searchBySubLocaton(searchkey, new PageRequest(page, 10));
        List<ItemWDetails> result = new ArrayList();
    	int i = 0;
    	for(i = 0; i < inven.size(); i++){
    		long dbid = inven.get(i).getDbid();
    		result.add(new ItemWDetails(inven.get(i), itemDetailRepo.loadDetailsWItemId(dbid)));
    	}
    	return result;
    }
    
    
    
    @GetMapping("/searchAllCount/{searchkey}/")
    public Long searchCount(@PathVariable(value="searchkey") String searchkey)  {
    	searchkey=searchkey.replaceAll("slash", "/");
    	searchkey=searchkey.replaceAll("whitespace", " ");
    	searchkey=searchkey.replaceAll("pong", "#");
    	searchkey=searchkey.replaceAll("percent", "%");
    	searchkey=searchkey.replaceAll("questionmark", "?");
    	return InventoryRepo.searchInvenCount(searchkey);
    }
    
    @GetMapping("/searchByNameCount/{searchkey}/")
    public Long searchByNameCount(@PathVariable(value="searchkey") String searchkey)  {
    	searchkey=searchkey.replaceAll("slash", "/");
    	searchkey=searchkey.replaceAll("whitespace", " ");
    	searchkey=searchkey.replaceAll("pong", "#");
    	searchkey=searchkey.replaceAll("percent", "%");
    	searchkey=searchkey.replaceAll("questionmark", "?");
    	return InventoryRepo.searchByNameCount(searchkey);
    }
    
    @GetMapping("/searchBySupplierorManufacturerCount/{searchkey}/")
    public Long searchBySupplierorManufacturerCount(@PathVariable(value="searchkey") String searchkey)  {
    	searchkey=searchkey.replaceAll("slash", "/");
    	searchkey=searchkey.replaceAll("whitespace", " ");
    	searchkey=searchkey.replaceAll("pong", "#");
    	searchkey=searchkey.replaceAll("percent", "%");
    	searchkey=searchkey.replaceAll("questionmark", "?");
    	return InventoryRepo.searchBySupplierorManufacturerCount(searchkey);
    }
       
    @GetMapping("/searchByCatCount/{searchkey}/")
    public Long searchByCatCount(@PathVariable(value="searchkey") String searchkey)  {
    	searchkey=searchkey.replaceAll("slash", "/");
    	searchkey=searchkey.replaceAll("whitespace", " ");
    	searchkey=searchkey.replaceAll("pong", "#");
    	searchkey=searchkey.replaceAll("percent", "%");
    	searchkey=searchkey.replaceAll("questionmark", "?");
    	return InventoryRepo.searchByCatCount(searchkey);
    }
    
    @GetMapping("/searchByLocatonCount/{searchkey}/")
    public Long searchByLocatonCount(@PathVariable(value="searchkey") String searchkey)  {
    	searchkey=searchkey.replaceAll("slash", "/");
    	searchkey=searchkey.replaceAll("whitespace", " ");
    	searchkey=searchkey.replaceAll("pong", "#");
    	searchkey=searchkey.replaceAll("percent", "%");
    	searchkey=searchkey.replaceAll("questionmark", "?");
    	return InventoryRepo.searchByLocatonCount(searchkey);
    }
       
    @GetMapping("/searchBySubLocatonCount/{searchkey}/")
    public Long searchBySubLocatonCount(@PathVariable(value="searchkey") String searchkey)  {
    	searchkey=searchkey.replaceAll("slash", "/");
    	searchkey=searchkey.replaceAll("whitespace", " ");
    	searchkey=searchkey.replaceAll("pong", "#");
    	searchkey=searchkey.replaceAll("percent", "%");
    	searchkey=searchkey.replaceAll("questionmark", "?");
    	return InventoryRepo.searchBySubLocatonCount(searchkey);
    }
    
    @GetMapping("/searchSingleInventoryByCat/{cat}")
	public Inventory searchSingleInventoryByCat(@PathVariable(value = "cat") String cat) {
    	cat=cat.replaceAll("slash", "/");
    	cat=cat.replaceAll("whitespace", " ");
    	cat=cat.replaceAll("pong", "#");
    	cat=cat.replaceAll("percent", "%");
    	cat=cat.replaceAll("questionmark", "?");
        return InventoryRepo.searchSingleInventoryByCat(cat);
    }
    
    @GetMapping("/searchSingleInventoryByName/{name}/")
	public Inventory searchSingleInventoryByName(@PathVariable(value = "name") String name) {
    	name=name.replaceAll("slash", "/");
    	name=name.replaceAll("whitespace", " ");
    	name=name.replaceAll("pong", "#");
    	name=name.replaceAll("percent", "%");
    	name=name.replaceAll("questionmark", "?");
        return InventoryRepo.searchSingleInventoryByName(name);
    }
    
    @PostMapping("/add")
    public Inventory createDrug(@Valid @RequestBody Inventory item) {
    	return InventoryRepo.save(item);
    }
    
    @PostMapping("/checkExist")
    public Inventory checkExist(@Valid @RequestBody Inventory item) {
    	Example<Inventory> example = Example.of(new Inventory((long)-1,  item.getCategory(), item.getCat(), "", item.getName(), "", true, "", "","","","","","","", "", new Date(), "", ""), matching(). //
				withIgnorePaths("dbid", "category", "suppliercat", "type", "active", "clientspecific", "supplier", "manufacturer", "unit", "unitprice", "unitsize", "quantitythreshold", "supplylink", "manufacturerlink", "lastmodify", "modifyperson", "comment"));
    	return InventoryRepo.findOne(example);
    }
    
    @GetMapping("/get/{id}")
    public ResponseEntity<Inventory> getDrugById(@PathVariable(value = "id") Long drugId) {
    	Inventory drug = InventoryRepo.findOne(drugId);
        if(drug == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(drug);
    }
    
    @PutMapping("/update/{id}")
    public ResponseEntity<Inventory> updateDrug(@PathVariable(value = "id") Long  drugId, 
                                           @Valid @RequestBody Inventory drugDetail) {
    	Inventory drug= InventoryRepo.findOne(drugId);
        if(drug == null) {
            return ResponseEntity.notFound().build();
        }
        
        drug.setDbid(drugDetail.getDbid());
        drug.setCategory(drugDetail.getCategory());
        drug.setCat(drugDetail.getCat());
        drug.setSuppliercat(drugDetail.getSuppliercat());
        drug.setName(drugDetail.getName());
        drug.setType(drugDetail.getType());
        drug.setActive(drugDetail.isActive());
        drug.setClientspecific(drugDetail.getClientspecific());
        drug.setSupplier(drugDetail.getSupplier());
        drug.setManufacturer(drugDetail.getManufacturer()); 
        drug.setUnit(drugDetail.getUnit()); 
        drug.setUnitprice(drugDetail.getUnitprice()); 
        drug.setUnitsize(drugDetail.getUnitsize()); 
        drug.setQuantitythreshold(drugDetail.getQuantitythreshold());        
        drug.setSupplylink(drugDetail.getSupplylink());
        drug.setManufacturerlink(drugDetail.getManufacturerlink());
        drug.setLastmodify(drugDetail.getLastmodify());
        drug.setModifyperson(drugDetail.getModifyperson());
        drug.setComment(drugDetail.getComment());

        
        Inventory UpdateEquip = InventoryRepo.save(drug);
        return ResponseEntity.ok(UpdateEquip);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Inventory> deleteDrug(@PathVariable(value = "id") Long drugId) {
    	Inventory drug = InventoryRepo.findOne(drugId);
        if(drug == null) {
            return ResponseEntity.notFound().build();
        }


        InventoryRepo.delete(drug);
        return ResponseEntity.ok().build();
    }
}
