package com.internal.service.somruinternal.controller;

import com.internal.service.somruinternal.model.Invoice;
import com.internal.service.somruinternal.repository.InvoiceRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

@RestController
@RequestMapping("/invoice")
public class InvoiceControl {
    @Autowired
    InvoiceRepository invoiceRepo;
    
    @GetMapping("/all")
    public List<Invoice> getAllInvoices() {
        return invoiceRepo.findAll();
    }
    
    @GetMapping("/count")
    public int count(){
    	return invoiceRepo.findAll().size();
    }
    
    @GetMapping("/latest")
    public Invoice getLastest(){
    	return invoiceRepo.findAll(new Sort(Sort.Direction.DESC, "dbid")).get(0);
    }
    
    @GetMapping("/all/{page}")
    public List<Invoice> getInvoicesByPage(@PathVariable(value = "page") int page) {
        return invoiceRepo.findAll(new PageRequest(page,10, Sort.Direction.DESC, "invoiceNum")).getContent();
    }
    
    @GetMapping("/search/{searchKey}/{page}")
    public List<Invoice> search(@PathVariable(value = "searchKey") String searchKey ,@PathVariable(value = "page") int page) {
        return invoiceRepo.searchByNum(searchKey, new PageRequest(page,10));
    }
    	
//    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/add")
    public Invoice createInvoice(@Valid @RequestBody Invoice Invoice) {
        return invoiceRepo.save(Invoice);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/get/{id}")
    public ResponseEntity<Invoice> getInvoiceById(@PathVariable(value = "id") Long InvoiceId) {
    	Invoice Invoice = invoiceRepo.findOne(InvoiceId);
        if(Invoice == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(Invoice);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<Invoice> updateInvoice(@PathVariable(value = "id") Long  InvoiceId, 
                                           @Valid @RequestBody Invoice InvoiceDetail) {
    	Invoice invoice= invoiceRepo.findOne(InvoiceId);
        if(invoice == null) {
            return ResponseEntity.notFound().build();
        }
       
       invoice.setDbid(InvoiceDetail.getDbid());
       invoice.setCurrency(InvoiceDetail.getCurrency());
       invoice.setCurrencyrate(InvoiceDetail.getCurrencyrate());
       invoice.setInvoiceNum(InvoiceDetail.getInvoiceNum());
       invoice.setDateCreate(InvoiceDetail.getDateCreate());
       invoice.setQuoteId(InvoiceDetail.getQuoteId());
       invoice.setClientId(InvoiceDetail.getClientId());       
       invoice.setRevision(InvoiceDetail.getRevision());
       invoice.setRevisionDate(InvoiceDetail.getRevisionDate());
       invoice.setBillAttn(InvoiceDetail.getBillAttn());
       invoice.setBillAddress(InvoiceDetail.getBillAddress());
       invoice.setShipAttn(InvoiceDetail.getShipAttn());
       invoice.setShipAddress(InvoiceDetail.getShipAddress());
       invoice.setPoNum(InvoiceDetail.getPoNum());
       invoice.setRequisitioner(InvoiceDetail.getRequisitioner());
       invoice.setCourier(InvoiceDetail.getCourier());
       invoice.setPaymentTerm(InvoiceDetail.getPaymentTerm());
       invoice.setShippingTerm(InvoiceDetail.getShippingTerm());
       invoice.setShippingfee(InvoiceDetail.getShippingfee());
       invoice.setHandleFee(InvoiceDetail.getHandleFee());
       invoice.setTax(InvoiceDetail.getTax());
       invoice.setProform(InvoiceDetail.getProform());
       invoice.setType(InvoiceDetail.getType());
       invoice.setNote(InvoiceDetail.getNote());
       invoice.setEditby(InvoiceDetail.getEditby());
       invoice.setModify(InvoiceDetail.getModify());
       
       Invoice UpdateInvoice = invoiceRepo.save(invoice);
       return ResponseEntity.ok(UpdateInvoice);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Invoice> deleteInvoice(@PathVariable(value = "id") Long InvoiceId) {
    	Invoice Invoice = invoiceRepo.findOne(InvoiceId);
        if(Invoice == null) {
            return ResponseEntity.notFound().build();
        }

        invoiceRepo.delete(Invoice);
        return ResponseEntity.ok().build();
    }
}
