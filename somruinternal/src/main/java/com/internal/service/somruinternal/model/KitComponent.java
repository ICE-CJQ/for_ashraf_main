package com.internal.service.somruinternal.model;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "kit_components")
public class KitComponent {

	public KitComponent() {
		super();
		// TODO Auto-generated constructor stub
	}

	public KitComponent(Long dbid, Long kitid, String component,
			String componentid, String reagent, String recipename, String recipeid, String amount, String unit, String iskit, String partnumber, String packaging, String vialid, String vialsize, String vialdescription, String editby, Date modify) {
		super();
		this.dbid = dbid;
		this.kitid = kitid;
		//this.recipeId=recipeId;		
		this.component=component;
		//this.componentType = componentType;
		this.componentid = componentid;
		this.reagent = reagent;
		this.recipename=recipename;
		this.recipeid=recipeid;
		this.amount = amount;
		this.unit=unit;
		this.iskit=iskit;
		this.partnumber=partnumber;
		this.packaging=packaging;
		this.vialid=vialid;
		this.vialsize=vialsize;
		this.vialdescription=vialdescription;
		this.editby = editby;
		this.modify =  modify;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	
	@Column(nullable = false)
	private long kitid;
	
	@Column(nullable = true)
	private String component;	
	
	@Column(nullable = false)
	private String componentid;

	@Column(nullable = false)
	private String reagent;
	
	@Column(nullable = false)
	private String recipename;
	
	@Column(nullable = false)
	private String recipeid;
	
	@Column(nullable = false)
	private String amount;
	
	@Column(nullable = false)
	private String unit;
	
	@Column(nullable = false)
	private String iskit;
	
	@Column(nullable = true)
	private String partnumber;
	
	@Column(nullable = false)
	private String packaging;
	
	@Column(nullable = false)
	private String vialid;	

	@Column(nullable = false)
	private String vialsize;
	
	@Column(nullable = false)
	private String vialdescription;
	
	@Column(nullable = true)
	private String editby;
	
	@Column(nullable = true)
	private Date modify;
	
	public long getDbid() {
		return dbid;
	}

	public void setDbid(long dbid) {
		this.dbid = dbid;
	}

	public long getKitid() {
		return kitid;
	}

	public void setKitid(long kitid) {
		this.kitid = kitid;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public String getComponentid() {
		return componentid;
	}

	public void setComponentid(String componentid) {
		this.componentid = componentid;
	}

	public String getReagent() {
		return reagent;
	}

	public void setReagent(String reagent) {
		this.reagent = reagent;
	}

	public String getRecipename() {
		return recipename;
	}

	public void setRecipename(String recipename) {
		this.recipename = recipename;
	}
	
	public String getRecipeid() {
		return recipeid;
	}

	public void setRecipeid(String recipeid) {
		this.recipeid = recipeid;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public String getIskit() {
		return iskit;
	}

	public void setIskit(String iskit) {
		this.iskit = iskit;
	}

	public String getPartnumber() {
		return partnumber;
	}

	public void setPartnumber(String partnumber) {
		this.partnumber = partnumber;
	}

	public String getPackaging() {
		return packaging;
	}

	public void setPackaging(String packaging) {
		this.packaging = packaging;
	}

	public String getVialid() {
		return vialid;
	}

	public void setVialid(String vialid) {
		this.vialid = vialid;
	}
	
	public String getVialsize() {
		return vialsize;
	}

	public void setVialsize(String vialsize) {
		this.vialsize = vialsize;
	}

	public String getVialdescription() {
		return vialdescription;
	}

	public void setVialdescription(String vialdescription) {
		this.vialdescription = vialdescription;
	}
	
	public String getEditby() {
		return editby;
	}

	public void setEditby(String editby) {
		this.editby = editby;
	}

	public Date getModify() {
		return modify;
	}

	public void setModify(Date modify) {
		this.modify = modify;
	}

	@Override
	public String toString() {
		return "KitComponent [dbid=" + dbid + ", kitid=" + kitid
			 + ", component=" + component + ", componentid="
				+ componentid + ", reagent=" + reagent
				+ ", recipename=" + recipename + ", recipeid=" + recipeid + ", amount=" + amount + ", unit=" + unit +", iskit=" + iskit +", partnumber=" + partnumber +", packaging=" + packaging +", vialid=" + vialid +", vialsize=" + vialsize + ", vialdescription=" + vialdescription + ", editby=" + editby + ", modify=" + modify + "]";
	}
}
