package com.internal.service.somruinternal.objects;

import java.util.List;

import com.internal.service.somruinternal.model.ClientCompany;
import com.internal.service.somruinternal.model.ClientContact;

public class CompanyWContacts {
	
	public CompanyWContacts(ClientCompany company, List<ClientContact> contacts) {
		super();
		this.company = company;
		this.contacts = contacts;
	}
	
	public ClientCompany company;
	public List<ClientContact> contacts;
	
	public ClientCompany getCompany() {
		return company;
	}
	public void setCompany(ClientCompany company) {
		this.company = company;
	}
	public List<ClientContact> getContacts() {
		return contacts;
	}
	public void setContacts(List<ClientContact> contacts) {
		this.contacts = contacts;
	}
}
