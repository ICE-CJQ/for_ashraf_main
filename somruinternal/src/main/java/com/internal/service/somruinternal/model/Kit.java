package com.internal.service.somruinternal.model;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Kit")
@EntityListeners(AuditingEntityListener.class)
public class Kit {

	public Kit() {
		super();
	}

	public Kit(Long dbid, String name, String cat, String description, String clientspecific, String molecular, String biosimilar, String method, String size, String status, String price, String enteredby, String reviewedby, String reviewcomment, String approvedby, String approvecomment,
			Date lastmodify, String editstatus, String comment, Date reviewtime, Date approvetime) {
		super();
		this.dbid = dbid;
		this.cat = cat;
		this.name = name;
		this.description = description;
		this.clientspecific = clientspecific;
		this.molecular = molecular;
		this.biosimilar = biosimilar;
		this.method = method;
		this.size=size;
		this.status = status;		
		this.price = price;
		this.enteredby = enteredby;
		this.reviewedby = reviewedby;
		this.approvedby = approvedby;
		
		this.reviewtime = reviewtime;
		this.approvetime = approvetime;
		
		this.reviewcomment = reviewcomment;
		this.approvecomment = approvecomment;
		this.lastmodify = lastmodify;
		this.editstatus = editstatus;
		this.comment = comment;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	
	@Column(nullable = false)
	private String cat;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = true)
	private String description;
	
	@Column(nullable = true)
	private String clientspecific;
	
	@Column(nullable = true)
	private String molecular;
	
	@Column(nullable = true)
	private String biosimilar;
	
	@Column(nullable = true)
	private String method;
	
	@Column(nullable = true)
	private String size;
	
	@Column(nullable = true)
	private String status;
		
	@Column(nullable = true)
	private String price;
	
	@Column(nullable = true)
	private String enteredby;
	
	@Column(nullable = true)
	private String reviewedby;
	
	@Column(nullable = true)
	private String approvedby;
	
	@Column(nullable = true)
	private Date reviewtime;
	
	@Column(nullable = true)
	private Date approvetime;	
	
	@Column(nullable = true)
	private String reviewcomment;

	
	@Column(nullable = true)
	private String approvecomment;
		
	@Column(nullable = true)
	private Date lastmodify;
	
	@Column(nullable = true)
	private String editstatus;
	
	@Column(nullable = true,length=5000)
	private String comment;
	
	public long getDbid() {
		return dbid;
	}

	public void setDbid(long dbid) {
		this.dbid = dbid;
	}
	
	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getClientspecific() {
		return clientspecific;
	}

	public void setClientspecific(String clientspecific) {
		this.clientspecific = clientspecific;
	}

	public String getMolecular() {
		return molecular;
	}

	public void setMolecular(String molecular) {
		this.molecular = molecular;
	}

	public String getBiosimilar() {
		return biosimilar;
	}

	public void setBiosimilar(String biosimilar) {
		this.biosimilar = biosimilar;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
	
	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}	

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	public String getEnteredby() {
		return enteredby;
	}

	public void setEnteredby(String enteredby) {
		this.enteredby = enteredby;
	}

	public String getReviewedby() {
		return reviewedby;
	}

	public void setReviewedby(String reviewedby) {
		this.reviewedby = reviewedby;
	}
	
	public String getReviewcomment() {
		return reviewcomment;
	}

	public void setReviewcomment(String reviewcomment) {
		this.reviewcomment = reviewcomment;
	}

	public String getApprovedby() {
		return approvedby;
	}

	public void setApprovedby(String approvedby) {
		this.approvedby = approvedby;
	}
	
	public String getApprovecomment() {
		return approvecomment;
	}

	public void setApprovecomment(String approvecomment) {
		this.approvecomment = approvecomment;
	}

	public Date getLastmodify() {
		return lastmodify;
	}

	public void setLastmodify(Date lastmodify) {
		this.lastmodify = lastmodify;
	}
		
	public String getEditstatus() {
		return editstatus;
	}

	public void setEditstatus(String editstatus) {
		this.editstatus = editstatus;
	}
	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getReviewtime() {
		return reviewtime;
	}

	public void setReviewtime(Date reviewtime) {
		this.reviewtime = reviewtime;
	}

	public Date getApprovetime() {
		return approvetime;
	}

	public void setApprovetime(Date approvetime) {
		this.approvetime = approvetime;
	}

	@Override
	public String toString() {
		return "Kit [dbid=" + dbid + ", cat=" + cat + ",name=" + name + ", description=" + description + ", clientspecific=" + clientspecific + ", molecular=" + molecular + ", biosimilar=" + biosimilar + ", method=" + method + ", size=" + size + ", status=" + status + ", price=" + price + ", enteredby=" + enteredby + ", reviewedby=" + reviewedby + ", reviewcomment=" + reviewcomment + ", approvedby=" + approvedby + ", approvecomment=" + approvecomment + ", reviewtime=" + reviewtime + ", approvetime=" + approvetime + ", lastmodify=" + lastmodify + ", editstatus=" + editstatus + ", comment=" + comment + "]";
	}

}
