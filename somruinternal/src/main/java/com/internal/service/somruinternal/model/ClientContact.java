package com.internal.service.somruinternal.model;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ClientContact")
@EntityListeners(AuditingEntityListener.class)
public class ClientContact {

	public ClientContact(long dbid, long companyId, String name, String role,
			String email, String phone, String ext, String fax, String editby, Date modify) {
		super();
		this.dbid = dbid;
		this.companyId = companyId;
		this.name = name;
		this.role = role;
		this.email = email;
		this.phone = phone;
		this.ext = ext;
		this.fax = fax;
		this.editby = editby;
		this.modify =  modify;
	}
	public ClientContact() {
		super();
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	@Column(nullable = false)
	private long companyId;
	@NotBlank
	private String name;
	@Column(nullable = true)
	private String role;
	@Column(nullable = true)
	private String email;
	@Column(nullable = true)
	private String phone;
	@Column(nullable = true)
	private String ext;
	@Column(nullable = true)
	private String fax;
	@Column(nullable = true)
	private String editby;
	@Column(nullable = true)
	private Date modify;

	
	public long getDbid() {
		return dbid;
	}
	public void setDbid(long dbid) {
		this.dbid = dbid;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEditby() {
		return editby;
	}
	public void setEditby(String editby) {
		this.editby = editby;
	}
	public Date getModify() {
		return modify;
	}
	public void setModify(Date modify) {
		this.modify = modify;
	}
	
}
