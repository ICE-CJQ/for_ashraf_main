package com.internal.service.somruinternal.model;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

import java.util.Date;

@Entity
@Table(name = "Application")
@EntityListeners(AuditingEntityListener.class)
public class Application {
	public Application(long dbid, long productdbid, String purpose, String recommenconcentration, String comment, String note, String editby, Date modif) {
		super();
		this.dbid = dbid;
		this.productdbid = productdbid;
		this.purpose = purpose;
		this.recommenconcentration = recommenconcentration;
		this.comment = comment;
		this.note = note;
		this.editby = editby;
		this.modify =  modify;
	}
	public Application() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	@Column(nullable = true)
	private long productdbid;
	@Column(nullable = true)
	private String purpose;
	@Column(nullable = true)
	private String recommenconcentration;
	@Column(nullable = true)
	private String comment;
	@Column(nullable = true,length=5000)
	private String note;
	@Column(nullable = true)
	private String editby;
	
	@Column(nullable = true)
	private Date modify;
	
	public long getDbid() {
		return dbid;
	}
	public void setDbid(long dbid) {
		this.dbid = dbid;
	}
	public long getProductdbid() {
		return productdbid;
	}
	public void setProductdbid(long productdbid) {
		this.productdbid = productdbid;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public String getRecommenconcentration() {
		return recommenconcentration;
	}
	public void setRecommenconcentration(String recommenconcentration) {
		this.recommenconcentration = recommenconcentration;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getEditby() {
		return editby;
	}
	public void setEditby(String editby) {
		this.editby = editby;
	}
	public Date getModify() {
		return modify;
	}
	public void setModify(Date modify) {
		this.modify = modify;
	}
	
}
