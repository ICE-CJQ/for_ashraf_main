package com.internal.service.somruinternal.model;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "Setting")
@EntityListeners(AuditingEntityListener.class)
public class Setting {
	public Setting() {
		super();
	}
	
	public Setting(long dbid, String page, String type,  String value, String pending, String requestDel, String editby, Date modify) {
		super();
		this.page = page;
		this.dbid = dbid;
		this.type = type;
		this.value = value;
		this.pending = pending;
		this.requestDel = requestDel;
		this.editby = editby;
		this.modify =  modify;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	@NotBlank
	private String type;
	@NotBlank
	private String page;
	@NotBlank
	@Column(length=5000)
	private String value;
	@Column(nullable=false, length=5000)
	private String pending;
	@Column(nullable=false, length=5000)
	private String requestDel;
	@Column(nullable = true)
	private String editby;
	@Column(nullable = true)
	private Date modify;
	
	public long getDbid() {
		return dbid;
	}
	public void setDbid(long dbid) {
		this.dbid = dbid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public String getPending() {
		return pending;
	}

	public void setPending(String pending) {
		this.pending = pending;
	}
	
	public String getRequestDel() {
		return requestDel;
	}

	public void setRequestDel(String requestDel) {
		this.requestDel = requestDel;
	}

	public String getEditby() {
		return editby;
	}

	public void setEditby(String editby) {
		this.editby = editby;
	}

	public Date getModify() {
		return modify;
	}

	public void setModify(Date modify) {
		this.modify = modify;
	}
	
}
