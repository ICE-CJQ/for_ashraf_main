package com.internal.service.somruinternal.model;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "DrugList")
@EntityListeners(AuditingEntityListener.class)
public class DrugList {
	public DrugList() {
		super();
	}
	
	public DrugList(long dbid, long num,String name) {
		super();
		this.dbid = dbid;
		this.name = name;
		this.num = num;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	@NotBlank
	private String name;
	@Column(nullable = false)
	private long num;
	
	public long getDbid() {
		return dbid;
	}
	public void setDbid(long dbid) {
		this.dbid = dbid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public long getNum() {
		return num;
	}

	public void setNum(long num) {
		this.num = num;
	}	
}
