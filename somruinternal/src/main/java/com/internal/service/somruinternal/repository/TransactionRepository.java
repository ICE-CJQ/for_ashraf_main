package com.internal.service.somruinternal.repository;
import java.util.List;

import com.internal.service.somruinternal.model.Transaction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long>{
	@Query(value="Select trans From Transaction trans Where trans.tableName LIKE :table")
	public List<Transaction> loadTransByTable(@Param(value="table") String table);
}
 