package com.internal.service.somruinternal.controller;

import static org.springframework.data.domain.ExampleMatcher.matching;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.TemplateEngine;

import com.internal.service.somruinternal.model.Kit;
import com.internal.service.somruinternal.model.User;
import com.internal.service.somruinternal.repository.LogInRepository;
import com.internal.service.somruinternal.utils.JwtTokenUtil;
import com.internal.service.somruinternal.utils.sendEmailUtil;
import io.jsonwebtoken.Claims;

import java.util.List;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.sql.Date;

@RestController
@RequestMapping("/login")
public class LogInControl {
	@Autowired
    LogInRepository loginRepo;
	@Autowired
	JwtTokenUtil tokenutil;
	@Autowired
	sendEmailUtil sendemailutil;
	@Autowired
    public JavaMailSender javamailsender;
	@Autowired	
	private MailSender mailsender;
	@Autowired
	private TemplateEngine templateengine;
	@Autowired
	MailProperties  mailproperties;
	
    @GetMapping("/all")
	public List<User> getAllUsers() {
        return loginRepo.findAll(new Sort(Sort.Direction.ASC, "dbid"));
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/add")
    public User register(@Valid @RequestBody User user) {
        return loginRepo.save(user);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/authenticate")
    public User login(@Valid @RequestBody User user){
    	User userInDb = loginRepo.searchSingleUserByEmail(user.getEmail());
    	if(userInDb==null) {
    		return null;
    	}

		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		
		if(passwordEncoder.matches(user.getPassword(), userInDb.getPassword())) {
			return userInDb;
		}
		else {
			userInDb.setPasword(null);
			return userInDb;
		}
        
	
        
//    	Example<User> example = Example.of(new User((long)-1, user.getName(), user.getRole(), user.getEmail(), encodedPassword), matching(). //
//				withIgnorePaths("dbid", "role", "name"));
//    	List<User> list = loginRepo.findAll(example);
//    	
//    	if(list.size() == 0) return ResponseEntity.notFound().build();
//    	
//    	return ResponseEntity.ok(list.get(0));
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/get/{id}")
    public ResponseEntity<User> getUserInfo(@PathVariable(value = "id") Long userID) {
    	User user = loginRepo.findOne(userID);
        if(user == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(user);
    }
    
//  @CrossOrigin(origins = "http://localhost:4200")
  @GetMapping("/requestResetPassword/{email}")
  public boolean requestResetPassword(@PathVariable(value = "email") String email) {
	  	boolean result = false;
	    User user = loginRepo.searchSingleUserByEmail(email);
	    if (user == null) {
	        return result;
	    }
	    else {
	    	String userEmail = user.getEmail();
	    	//set token
		    String token = tokenutil.generateToken(user);

		    //send email
		    try {
				sendemailutil.sendEmail(sendEmailUtil.REQUEST_RESET_PASSWORD, userEmail, token, null);
			} catch (MessagingException e) {
				e.printStackTrace();
				return result;
			}
		    
	    	result = true;
	    	return result;
	    }
  }

	  @GetMapping("/verifytoken/{token}")
//	  @RequestMapping(value="/verifytoken/{token}", method=RequestMethod.GET, produces="text/plain")
//	  @ResponseBody
	  public HashMap<String, Object> verifyToken(@PathVariable(value = "token") String token) {
//	  public String verifyToken(@PathVariable(value = "token") String token) {
		  String result = "The reset password link is invalid. This can happen if the link is malformed, expired or has already been used.";
		  Claims claims = tokenutil.decodeToken(token);
		  HashMap<String, Object> rtn = new LinkedHashMap<String, Object>();
		  if(claims != null) {
			    String userId = claims.get("userid").toString();
			    String usePassword = claims.get("userpassword").toString();
			    
			    //get user by userId
			    User user = loginRepo.findOne(Long.parseLong(userId));
			    
			    if(user == null) {
			    	result = "User dose not exist.";
			    }
			    else {
			    	if(usePassword.equals(user.getPassword()) ) {
			    		result =  userId;
			    	}
			    }
		  }

		  rtn.put("result",result);
		  return rtn;

	  }
    
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<User> updateuser(@PathVariable(value = "id") Long  userId, 
                                           @Valid @RequestBody User userDetail) {
    	User user= loginRepo.findOne(userId);
        if(user == null) {
            return ResponseEntity.notFound().build();
        }
        
        user.setDbid(userDetail.getDbid());
        user.setName(userDetail.getName());
        user.setRole(userDetail.getRole());
        user.setPasword(userDetail.getPassword());
        user.setEmail(userDetail.getEmail());
        user.setInvalidlogincount(userDetail.getInvalidlogincount());
        user.setActive(userDetail.isActive());

        User Updateuser = loginRepo.save(user);
        return ResponseEntity.ok(Updateuser);
    }
    
    @PutMapping("/updatepassword/{id}/{password}")
    public ResponseEntity<User> updateuserPassword(@PathVariable(value = "id") Long  userId, @PathVariable(value = "password") String  password) {
    	User user= loginRepo.findOne(userId);
        if(user == null) {
            return ResponseEntity.notFound().build();
        }
        
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword=passwordEncoder.encode(password);
        user.setPasword(encodedPassword);

        User Updateuser = loginRepo.save(user);
        return ResponseEntity.ok(Updateuser);
    }
    
    @PutMapping("/updatepinvalidcount/{id}/{count}")
    public ResponseEntity<User> updateuserInvalidCount(@PathVariable(value = "id") Long  userId, @PathVariable(value = "count") Long  count) {
    	User user= loginRepo.findOne(userId);
        if(user == null) {
            return ResponseEntity.notFound().build();
        }
        user.setInvalidlogincount(count);;

        User Updateuser = loginRepo.save(user);
        return ResponseEntity.ok(Updateuser);
    }
    
    @PutMapping("/updateactive/{id}/{status}")
    public ResponseEntity<User> updateuserActive(@PathVariable(value = "id") Long  userId, @PathVariable(value = "status") boolean  status) {
    	User user= loginRepo.findOne(userId);
        if(user == null) {
            return ResponseEntity.notFound().build();
        }
        user.setActive(status);;;

        User Updateuser = loginRepo.save(user);
        return ResponseEntity.ok(Updateuser);
    }
    
    
    

    
    
    
}
