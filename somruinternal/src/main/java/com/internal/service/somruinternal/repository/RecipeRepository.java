package com.internal.service.somruinternal.repository;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.internal.service.somruinternal.model.Kit;
import com.internal.service.somruinternal.model.Recipe;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long>{
	@Query(value="Select DISTINCT recipe from Recipe recipe Where recipe.somruid LIKE %:searchKey% OR recipe.name LIKE %:searchKey% ORDER By recipe.dbid DESC")
	public List<Recipe> searchRecipe(@Param(value="searchKey") String searckKey, Pageable pageable);
	
	@Query(value="Select recipe from Recipe recipe Where recipe.somruid LIKE %:searchKey% ORDER By recipe.dbid DESC")
	public List<Recipe> searchById(@Param(value="searchKey") String searckKey, Pageable pageable);
	
	@Query(value="Select recipe from Recipe recipe Where recipe.name LIKE %:searchKey% ORDER By recipe.dbid DESC")
	public List<Recipe> searchByName(@Param(value="searchKey") String searckKey, Pageable pageable);
	
	
	@Query(value="Select COUNT(DISTINCT recipe) from Recipe recipe Where recipe.somruid LIKE %:searchKey% OR recipe.name LIKE %:searchKey%")
	public Long searchRecipenCount(@Param("searchKey") String searchKey);
	
	@Query(value="Select COUNT(recipe) from Recipe recipe Where recipe.somruid LIKE %:searchKey%")
	public Long searchByIdCount(@Param("searchKey") String searchKey);
	
	@Query(value="Select COUNT(recipe) from Recipe recipe Where recipe.name LIKE %:searchKey%")
	public Long searchByNameCount(@Param("searchKey") String searchKey);
	
	
	
	
	@Query(value="Select recipe From Recipe recipe Where recipe.name = :name")
	public Recipe searchSingleRecipeByName(@Param(value="name") String name);
	
	@Query(value="Select recipe From Recipe recipe Where recipe.somruid = :somruid")
	public Recipe searchSingleRecipeBySId(@Param(value="somruid") String somruid);
	
	@Query(value="Select recipe From Recipe recipe Where recipe.dbid = :dbid")
	public Recipe searchSingleRecipeByDId(@Param(value="dbid") Long dbid);
	
	
}
