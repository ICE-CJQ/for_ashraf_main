package com.internal.service.somruinternal.model;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

import java.util.Date;

@Entity
@Table(name = "Transaction")
public class Transaction {
	public Transaction(long dbid, Date date, String tableName, String action,
			long id) {
		super();
		this.dbid = dbid;
		this.date = date;
		this.tableName = tableName;
		this.action = action;
		this.id = id;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	@Column(nullable = false)
	private Date date;
	@Column(nullable = false)
	private String tableName;
	@Column(nullable = false)
	private String action;
	@Column(nullable= false)
	private long id;
	public long getDbid() {
		return dbid;
	}
	public void setDbid(long dbid) {
		this.dbid = dbid;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getTable() {
		return tableName;
	}
	public void setTable(String table) {
		this.tableName = table;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
