package com.internal.service.somruinternal.repository;
import com.internal.service.somruinternal.model.Equipment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EquipmentRepository extends JpaRepository<Equipment, Long>{

}
