package com.internal.service.somruinternal.model;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

import java.util.Date;

@Entity
@Table(name = "Invoice")
@EntityListeners(AuditingEntityListener.class)
public class Invoice {
	public Invoice() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Invoice(long dbid, String currency, double currencyrate, long invoiceNum, Date dateCreate, long quoteId, long clientId, long revision, Date revisionDate,
			String billAttn, String billAddress, String shipAttn,
			String shipAddress, String poNum, String requisitioner,
			String courier, String paymentTerm, String shippingTerm, double shippingfee,
			double handleFee, double tax, String proform, String type, String note, String editby, Date modify) {
		super();
		this.dbid = dbid;
		this.currency = currency;
		this.currencyrate = currencyrate;
		this.invoiceNum = invoiceNum;
		this.dateCreate = dateCreate;
		this.quoteId = quoteId;
		this.clientId = clientId;
		this.revision = revision;
		this.revisionDate = revisionDate;
		this.billAttn = billAttn;
		this.billAddress = billAddress;
		this.shipAttn = shipAttn;
		this.shipAddress = shipAddress;
		this.poNum = poNum;
		this.requisitioner = requisitioner;
		this.courier = courier;
		this.paymentTerm = paymentTerm;
		this.shippingTerm = shippingTerm;
		this.shippingfee = shippingfee;
		this.handleFee = handleFee;
		this.tax = tax;
		this.proform = proform;
		this.type = type;
		this.note =  note;
		this.editby = editby;
		this.modify =  modify;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	@Column(nullable = false)
	private String currency;
	@Column(nullable = false)
	private double currencyrate;
	@Column(nullable = false)
	private long invoiceNum;
	@Column(nullable = false)
	private Date dateCreate;
	@Column(nullable = true)
	private long quoteId;
	@Column(nullable = true)
	private long clientId;
	@Column(nullable = false)
	private long revision;
	@Column(nullable = false)
	private Date revisionDate;
	@NotBlank
	private String billAttn;
	@NotBlank
	private String billAddress;
	@NotBlank
	private String shipAttn;
	@NotBlank
	private String shipAddress;
	@NotBlank
	private String poNum;
	@NotBlank
	private String requisitioner;
	@NotBlank
	private String courier;
	@NotBlank
	private String paymentTerm;
	@NotBlank
	private String shippingTerm;
	@Column(nullable = true)
	private Double shippingfee;
	@Column(nullable = true)
	private Double handleFee;
	@Column(nullable = true)
	private Double tax;
	@Column(nullable = true)
	private String proform;
	@Column(nullable = true)
	private String type;
	@Column(nullable = true,length=5000)
	private String note;
	@Column(nullable = true)
	private String editby;
	@Column(nullable = true)
	private Date modify;
	
	public long getDbid() {
		return dbid;
	}
	public void setDbid(long dbid) {
		this.dbid = dbid;
	}
	
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public double getCurrencyrate() {
		return currencyrate;
	}
	public void setCurrencyrate(double currencyrate) {
		this.currencyrate = currencyrate;
	}
	public long getInvoiceNum() {
		return invoiceNum;
	}
	public void setInvoiceNum(long invoiceNum) {
		this.invoiceNum = invoiceNum;
	}
	public Date getDateCreate() {
		return dateCreate;
	}
	public void setDateCreate(Date dateCreate) {
		this.dateCreate = dateCreate;
	}
	public long getQuoteId() {
		return quoteId;
	}
	public void setQuoteId(long quoteId) {
		this.quoteId = quoteId;
	}
	
	public long getClientId() {
		return clientId;
	}
	public void setClientId(long clientId) {
		this.clientId = clientId;
	}
	public String getBillAttn() {
		return billAttn;
	}
	public void setBillAttn(String billAttn) {
		this.billAttn = billAttn;
	}
	public String getBillAddress() {
		return billAddress;
	}
	public void setBillAddress(String billAddress) {
		this.billAddress = billAddress;
	}
	public String getShipAttn() {
		return shipAttn;
	}
	public void setShipAttn(String shipAttn) {
		this.shipAttn = shipAttn;
	}
	public String getShipAddress() {
		return shipAddress;
	}
	public void setShipAddress(String shipAddress) {
		this.shipAddress = shipAddress;
	}
	public String getPoNum() {
		return poNum;
	}
	public void setPoNum(String poNum) {
		this.poNum = poNum;
	}
	public String getRequisitioner() {
		return requisitioner;
	}
	public void setRequisitioner(String requisitioner) {
		this.requisitioner = requisitioner;
	}
	public String getCourier() {
		return courier;
	}
	public void setCourier(String courier) {
		this.courier = courier;
	}
	public String getPaymentTerm() {
		return paymentTerm;
	}
	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}
	public String getShippingTerm() {
		return shippingTerm;
	}
	public void setShippingTerm(String shippingTerm) {
		this.shippingTerm = shippingTerm;
	}
	
	public Double getShippingfee() {
		return shippingfee;
	}
	public void setShippingfee(Double shippingfee) {
		this.shippingfee = shippingfee;
	}
	public Double getHandleFee() {
		return handleFee;
	}
	public void setHandleFee(Double handleFee) {
		this.handleFee = handleFee;
	}
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public long getRevision() {
		return revision;
	}
	public void setRevision(long revision) {
		this.revision = revision;
	}
	public Date getRevisionDate() {
		return revisionDate;
	}
	public void setRevisionDate(Date revisionDate) {
		this.revisionDate = revisionDate;
	}
	public String getProform() {
		return proform;
	}
	public void setProform(String proform) {
		this.proform = proform;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getEditby() {
		return editby;
	}
	public void setEditby(String editby) {
		this.editby = editby;
	}
	public Date getModify() {
		return modify;
	}
	public void setModify(Date modify) {
		this.modify = modify;
	}
	
}
