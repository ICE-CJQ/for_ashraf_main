package com.internal.service.somruinternal.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "projectnumber")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"lastmodify"}, 
        allowGetters = true)
public class Projectnumber {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;

	@Column(nullable = true)
	private String projectnumber;
	

	public Projectnumber(
		Long dbid,
		String projectnumber){
		super();
		this.dbid = dbid;
		this.projectnumber = projectnumber;
	}
	
	public Projectnumber(){
		super();
	}

	public long getDbid() {
		return dbid;
	}

	public void setDbid(long dbid) {
		this.dbid = dbid;
	}

	public String getProjectnumber() {
		return projectnumber;
	}

	public void setProjectnumber(String projectnumber) {
		this.projectnumber = projectnumber;
	}


}
