package com.internal.service.somruinternal.controller;

import com.internal.service.somruinternal.model.Product;
import com.internal.service.somruinternal.model.Recipe;
import com.internal.service.somruinternal.repository.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductControl {
    @Autowired
    ProductRepository productRepo;
    
    @GetMapping("/all")
    public List<Product> getAllProducts() {
        return productRepo.findAll();
    }
    
    @GetMapping("/count")
    public int count(){
    	return productRepo.findAll().size();
    }
    
//    @GetMapping("/latest")
//    public Product getLastest(){
//    	return productRepo.findAll(new Sort(Sort.Direction.DESC, "dbid")).get(0);
//    }
    
    @GetMapping("/all/{page}")
    public List<Product> getProductsByPage(@PathVariable(value = "page") int page) {
        return productRepo.findAll(new PageRequest(page,10, Sort.Direction.DESC, "dbid")).getContent();
    }
    
//    @GetMapping("/search/{searchKey}/{page}")
//    public List<Product> search(@PathVariable(value = "searchKey") String searchKey ,@PathVariable(value = "page") int page) {
//        return productRepo.searchByNum(searchKey, new PageRequest(page,10));
//    }
    	
//    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/add")
    public Product createProduct(@Valid @RequestBody Product Product) {
        return productRepo.save(Product);
    }
    
    @GetMapping("/searchName/{searchKey}/{page}")
    public List<Product> searchByName(@PathVariable(value="searchKey") String searchKey, @PathVariable(value="page" ) int page){
    	searchKey=searchKey.replaceAll("slash", "/");
    	searchKey=searchKey.replaceAll("whitespace", " ");
    	searchKey=searchKey.replaceAll("pong", "#");
    	searchKey=searchKey.replaceAll("percent", "%");
    	searchKey=searchKey.replaceAll("questionmark", "?");
    	return productRepo.searchProductByName(searchKey, new PageRequest(page, 10));
    }
    
    @GetMapping("/searchNameCount/{searchKey}/")
    public Long searchByNameCount(@PathVariable(value="searchKey") String searchKey){
    	searchKey=searchKey.replaceAll("slash", "/");
    	searchKey=searchKey.replaceAll("whitespace", " ");
    	searchKey=searchKey.replaceAll("pong", "#");
    	searchKey=searchKey.replaceAll("percent", "%");
    	searchKey=searchKey.replaceAll("questionmark", "?");
    	return productRepo.searchProductByNameCount(searchKey);
    }
    
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/get/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable(value = "id") Long ProductId) {
    	Product Product = productRepo.findOne(ProductId);
        if(Product == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(Product);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable(value = "id") Long  ProductId, 
                                           @Valid @RequestBody Product ProductDetail) {
    	Product product= productRepo.findOne(ProductId);
        if(product == null) {
            return ResponseEntity.notFound().build();
        }
       
        product.setDbid(ProductDetail.getDbid());
        product.setName(ProductDetail.getName());
        product.setType(ProductDetail.getType());
        product.setDescription(ProductDetail.getDescription());
        product.setCat(ProductDetail.getCat());
        product.setOldcat(ProductDetail.getOldcat());
        product.setUnit(ProductDetail.getUnit());
        product.setHost(ProductDetail.getHost());
        product.setClonality(ProductDetail.getClonality());        
        product.setIsotype(ProductDetail.getIsotype());        
        product.setImmunogen(ProductDetail.getImmunogen());
        product.setPurification(ProductDetail.getPurification());
        product.setBuffer(ProductDetail.getBuffer());
        product.setSpecificity(ProductDetail.getSpecificity());
        product.setReconstitution(ProductDetail.getReconstitution());
        product.setStorage(ProductDetail.getStorage());
        product.setUnitsize(ProductDetail.getUnitsize());
        product.setUnitprice(ProductDetail.getUnitprice());
        product.setPacksize(ProductDetail.getPacksize());
        product.setComment(ProductDetail.getComment());                        
        product.setStatus(ProductDetail.getStatus());
        product.setEnteredby(ProductDetail.getEnteredby());
        product.setReviewedby(ProductDetail.getReviewedby());
        product.setApprovedby(ProductDetail.getApprovedby());     
        product.setEntertime(ProductDetail.getEntertime());        
        product.setReviewtime(ProductDetail.getReviewtime());        
        product.setApprovetime(ProductDetail.getApprovetime());        
        product.setReviewcomment(ProductDetail.getReviewcomment());
        product.setApprovecomment(ProductDetail.getApprovecomment());
                                     
       Product UpdateProduct = productRepo.save(product);
       return ResponseEntity.ok(UpdateProduct);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Product> deleteProduct(@PathVariable(value = "id") Long ProductId) {
    	Product Product = productRepo.findOne(ProductId);
        if(Product == null) {
            return ResponseEntity.notFound().build();
        }

        productRepo.delete(Product);
        return ResponseEntity.ok().build();
    }
}
