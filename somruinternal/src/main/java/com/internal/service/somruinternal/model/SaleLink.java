package com.internal.service.somruinternal.model;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

import java.util.Date;

@Entity
@Table(name = "SaleLink")
@EntityListeners(AuditingEntityListener.class)
public class SaleLink {
	public SaleLink(long dbid, long linkId, String linkType, String itemType, long itemId, String cat, String name, double price, String size, double itemDiscount,
			double itemQuan, String footnote, String editby, Date modify) {
		super();
		this.dbid = dbid;
		this.linkId = linkId;
		this.linkType = linkType;
		this.itemType = itemType;
		this.itemId = itemId;
		this.cat = cat;
		this.name = name;
		this.price = price;
		this.size= size;
		this.itemDiscount = itemDiscount;
		this.itemQuan = itemQuan;
		this.footnote = footnote;
		this.editby = editby;
		this.modify =  modify;
	}
	public SaleLink() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	@Column(nullable = false)
	private long linkId;
	@NotBlank
	private String linkType;
	@NotBlank
	private String itemType;
	@Column(nullable = true)
	private long itemId;
	@NotBlank
	private String cat;
	@NotBlank
	private String name;
	@Column(nullable = true)
	private double price;
	@NotBlank
	private String size;
	@Column(nullable = true)
	private double itemDiscount;
	@Column(nullable = true)
	private double itemQuan;
	@Column(nullable = true,length=5000)
	private String footnote;
	@Column(nullable = true)
	private String editby;
	@Column(nullable = true)
	private Date modify;
	
	public long getDbid() {
		return dbid;
	}
	public void setDbid(long dbid) {
		this.dbid = dbid;
	}
	public long getLinkId() {
		return linkId;
	}
	public void setLinkId(long linkId) {
		this.linkId = linkId;
	}
	public long getItemId() {
		return itemId;
	}
	public void setItemId(long itemId) {
		this.itemId = itemId;
	}
	public double getItemDiscount() {
		return itemDiscount;
	}
	public void setItemDiscount(double itemDiscount) {
		this.itemDiscount = itemDiscount;
	}
	public double getItemQuan() {
		return itemQuan;
	}
	public void setItemQuan(double itemQuan) {
		this.itemQuan = itemQuan;
	}
	public String getCat() {
		return cat;
	}
	public void setCat(String cat) {
		this.cat = cat;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getFootnote() {
		return footnote;
	}
	public void setFootnote(String footnote) {
		this.footnote = footnote;
	}
	public String getLinkType() {
		return linkType;
	}
	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getEditby() {
		return editby;
	}
	public void setEditby(String editby) {
		this.editby = editby;
	}
	public Date getModify() {
		return modify;
	}
	public void setModify(Date modify) {
		this.modify = modify;
	}
	
}
