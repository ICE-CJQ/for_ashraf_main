package com.internal.service.somruinternal.repository;
import com.internal.service.somruinternal.model.Application;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long>{
	@Query(value="Select applications From Application applications Where applications.productdbid = :id")
	public List<Application> searchApplicationsByProdictid(@Param(value="id") Long id);
	
	@Query(value="Delete From Application application Where application.productdbid = :id")
	public void removeApplicationWProductId(@Param(value="id") Long id);

}
