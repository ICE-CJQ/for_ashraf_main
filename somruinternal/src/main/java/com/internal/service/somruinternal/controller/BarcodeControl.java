package com.internal.service.somruinternal.controller;
import static org.springframework.data.domain.ExampleMatcher.matching;
import com.internal.service.somruinternal.model.Barcode;
import com.internal.service.somruinternal.model.Invoice;
import com.internal.service.somruinternal.model.Itemdetail;
import com.internal.service.somruinternal.repository.BarcodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/barcode")
public class BarcodeControl {

    @Autowired
    BarcodeRepository BarcodeRepo;
    
    @GetMapping("/all")
    public List<Barcode> getAllDrugs() {
        return BarcodeRepo.findAll();
    }
    
    @GetMapping("/count")
    public int count() {
        return BarcodeRepo.findAll().size();
    }
    
    @PostMapping("/add")
    public Barcode createDrug(@Valid @RequestBody Barcode barcode) {
    	return BarcodeRepo.save(barcode);
    }
    
    @GetMapping("/latest")
    public Barcode getLastest(){
    	return BarcodeRepo.findAll(new Sort(Sort.Direction.DESC, "dbid")).get(0);
    }
    
    @GetMapping("/get/{id}")
    public ResponseEntity<Barcode> getDrugById(@PathVariable(value = "id") Long drugId) {
    	Barcode barcode = BarcodeRepo.findOne(drugId);
        if(barcode == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(barcode);
    }
    
    @GetMapping("/getBarcodesByItemdetailDbid/{id}")
    public List<Barcode> getBarcodesByItemdetailDbid(@PathVariable(value = "id") Long itemdetaildbid) {
    	Example<Barcode> example = Example.of(new Barcode((long)-1, itemdetaildbid, "", "", "", new Date()), matching(). //
				withIgnorePaths("dbid",  "barcode", "usestatus", "editby", "modify"));
    	return BarcodeRepo.findAll(example);
    }
    
    @GetMapping("/getBarcodesByBarcode/{barcode}")
    public Barcode getBarcodesByBarcode(@PathVariable(value = "barcode") String barcode) {
    	Example<Barcode> example = Example.of(new Barcode((long)-1, (long)-1, barcode, "", "", new Date()), matching(). //
				withIgnorePaths("dbid",  "itemdetaildbid", "usestatus", "editby", "modify"));
    	return BarcodeRepo.findOne(example);
    }
    
    @PutMapping("/update/{id}")
    public ResponseEntity<Barcode> updateDrug(@PathVariable(value = "id") Long  barcodeId, 
                                           @Valid @RequestBody Barcode barcodeDetail) {
    	Barcode barcode= BarcodeRepo.findOne(barcodeId);
        if(barcode == null) {
            return ResponseEntity.notFound().build();
        }
        
        barcode.setDbid(barcodeDetail.getDbid());
        barcode.setItemdetaildbid(barcodeDetail.getItemdetaildbid());
        barcode.setBarcode(barcodeDetail.getBarcode());
        barcode.setUsestatus(barcodeDetail.getUsestatus());
        barcode.setEditby(barcodeDetail.getEditby());
        barcode.setModify(barcodeDetail.getModify());
        
        Barcode UpdateBarcode = BarcodeRepo.save(barcode);
        return ResponseEntity.ok(UpdateBarcode);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Barcode> deleteDrug(@PathVariable(value = "id") Long drugId) {
    	Barcode drug = BarcodeRepo.findOne(drugId);
        if(drug == null) {
            return ResponseEntity.notFound().build();
        }


        BarcodeRepo.delete(drug);
        return ResponseEntity.ok().build();
    }
}
