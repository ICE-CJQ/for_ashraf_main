package com.internal.service.somruinternal.repository;
import java.util.List;

import com.internal.service.somruinternal.model.Orderitem;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderitemRepository extends JpaRepository<Orderitem, Long>{
	@Query(value="Select orderitem From Orderitem orderitem Where orderitem.status LIKE :status")
	public List<Orderitem> loadOrderByStatus(@Param(value="status") String status);
	
	@Query(value="Select orderitem From Orderitem orderitem Where orderitem.status LIKE :status ORDER BY orderitem.requesttime DESC")
	public List<Orderitem> loadRequestedOrder(@Param(value="status") String status, Pageable pageable);
	
	@Query(value="Select orderitem From Orderitem orderitem Where orderitem.status LIKE :status ORDER BY orderitem.approvetime DESC")
	public List<Orderitem> loadApprovedOrder(@Param(value="status") String status, Pageable pageable);
	
	@Query(value="Select orderitem From Orderitem orderitem Where orderitem.status LIKE :status ORDER BY orderitem.ordertime DESC")
	public List<Orderitem> loadOrderedOrder(@Param(value="status") String status, Pageable pageable);
	
	@Query(value="Select orderitem From Orderitem orderitem Where orderitem.status LIKE :status ORDER BY orderitem.receivetime DESC")
	public List<Orderitem> loadReceivedOrder(@Param(value="status") String status, Pageable pageable);
	

	@Query(value="Select orderitem From Orderitem orderitem Where orderitem.status LIKE :status and orderitem.urgent= TRUE ORDER BY orderitem.requesttime DESC")
	public List<Orderitem> loadUrgentRequestedOrder(@Param(value="status") String status, Pageable pageable);
	
	@Query(value="Select orderitem From Orderitem orderitem Where orderitem.status LIKE :status and orderitem.urgent= TRUE ORDER BY orderitem.approvetime DESC")
	public List<Orderitem> loadUrgentApprovedOrder(@Param(value="status") String status,Pageable pageable);
	
	@Query(value="Select orderitem From Orderitem orderitem Where orderitem.status LIKE :status and orderitem.urgent= TRUE ORDER BY orderitem.ordertime DESC")
	public List<Orderitem> loadUrgentOrderedOrder(@Param(value="status") String status,Pageable pageable);
	
	@Query(value="Select orderitem From Orderitem orderitem Where orderitem.status LIKE :status and orderitem.urgent= TRUE ORDER BY orderitem.receivetime DESC")
	public List<Orderitem> loadUrgentReceivedOrder(@Param(value="status") String status,Pageable pageable);
	
	
	@Query(value="Select COUNT(DISTINCT orderitem) From Orderitem orderitem Where orderitem.status LIKE :status and orderitem.urgent= TRUE ORDER BY orderitem.requesttime DESC")
	public Long loadUrgentRequestedOrderCount(@Param(value="status") String status);
	
	@Query(value="Select COUNT(DISTINCT orderitem) From Orderitem orderitem Where orderitem.status LIKE :status and orderitem.urgent= TRUE ORDER BY orderitem.approvetime DESC")
	public Long loadUrgentApprovedOrderCount(@Param(value="status") String status);
	
	@Query(value="Select COUNT(DISTINCT orderitem) From Orderitem orderitem Where orderitem.status LIKE :status and orderitem.urgent= TRUE ORDER BY orderitem.ordertime DESC")
	public  Long loadUrgentOrderedOrderCount(@Param(value="status") String status);
	
	@Query(value="Select COUNT(DISTINCT orderitem) From Orderitem orderitem Where orderitem.status LIKE :status and orderitem.urgent= TRUE ORDER BY orderitem.receivetime DESC")
	public Long loadUrgentReceivedOrderCount(@Param(value="status") String status);
	
	
	
	
	
	
	@Query(value="Select DISTINCT orderitem From Orderitem orderitem Where "
			+ "orderitem.status LIKE :status AND "
			+ "(orderitem.name LIKE %:searchKey% OR orderitem.cat LIKE %:searchKey% OR orderitem.suppliercat LIKE %:searchKey% OR orderitem.supplier LIKE %:searchKey% OR orderitem.manufacturer LIKE %:searchKey% OR orderitem.requestperson LIKE %:searchKey%) "
			+ "ORDER BY orderitem.requesttime DESC")
	public List<Orderitem> searchRequestedOrder(@Param(value="status") String status, @Param(value="searchKey") String searchKey, Pageable pageable);

	@Query(value="Select DISTINCT orderitem From Orderitem orderitem Where "
			+ "orderitem.status LIKE :status AND "
			+ "(orderitem.name LIKE %:searchKey% OR orderitem.cat LIKE %:searchKey%  OR orderitem.suppliercat LIKE %:searchKey% OR orderitem.supplier LIKE %:searchKey%  OR orderitem.manufacturer LIKE %:searchKey% OR orderitem.requestperson LIKE %:searchKey%) "
			+ "ORDER BY orderitem.approvetime DESC")
	public List<Orderitem> searchApprovedOrder(@Param(value="status") String status, @Param(value="searchKey") String searchKey, Pageable pageable);
	
	@Query(value="Select DISTINCT orderitem From Orderitem orderitem Where "
			+ "orderitem.status LIKE :status AND "
			+ "(orderitem.name LIKE %:searchKey% OR orderitem.cat LIKE %:searchKey% OR orderitem.suppliercat LIKE %:searchKey% OR orderitem.supplier LIKE %:searchKey%  OR orderitem.manufacturer LIKE %:searchKey% OR orderitem.requestperson LIKE %:searchKey%) "
			+ "ORDER BY orderitem.ordertime DESC")
	public List<Orderitem> searchOrderedOrder(@Param(value="status") String status, @Param(value="searchKey") String searchKey, Pageable pageable);
	
	@Query(value="Select DISTINCT orderitem From Orderitem orderitem Where "
			+ "orderitem.status LIKE :status AND "
			+ "(orderitem.name LIKE %:searchKey% OR orderitem.cat LIKE %:searchKey% OR orderitem.suppliercat LIKE %:searchKey% OR orderitem.supplier LIKE %:searchKey%  OR orderitem.manufacturer LIKE %:searchKey% OR orderitem.requestperson LIKE %:searchKey%) "
			+ "ORDER BY orderitem.receivetime DESC")
	public List<Orderitem> searchReceivedOrder(@Param(value="status") String status, @Param(value="searchKey") String searchKey, Pageable pageable);
	
	
	
	
	
	@Query(value="Select COUNT(DISTINCT orderitem) From Orderitem orderitem Where "
			+ "orderitem.status LIKE :status AND "
			+ "(orderitem.name LIKE %:searchKey% OR orderitem.cat LIKE %:searchKey% OR orderitem.suppliercat LIKE %:searchKey% OR orderitem.supplier LIKE %:searchKey% OR orderitem.requestperson LIKE %:searchKey%) "
			+ "ORDER BY orderitem.requesttime DESC")
	public Long searchRequestedOrderCount(@Param(value="status") String status, @Param(value="searchKey") String searchKey);
	
	@Query(value="Select COUNT(DISTINCT orderitem) From Orderitem orderitem Where "
			+ "orderitem.status LIKE :status AND "
			+ "(orderitem.name LIKE %:searchKey% OR orderitem.cat LIKE %:searchKey% OR orderitem.suppliercat LIKE %:searchKey% OR orderitem.supplier LIKE %:searchKey% OR orderitem.requestperson LIKE %:searchKey%) "
			+ "ORDER BY orderitem.approvetime DESC")
	public Long searchApprovedOrderCount(@Param(value="status") String status, @Param(value="searchKey") String searchKey);
	
	@Query(value="Select COUNT(DISTINCT orderitem) From Orderitem orderitem Where "
			+ "orderitem.status LIKE :status AND "
			+ "(orderitem.name LIKE %:searchKey% OR orderitem.cat LIKE %:searchKey% OR orderitem.suppliercat LIKE %:searchKey% OR orderitem.supplier LIKE %:searchKey% OR orderitem.requestperson LIKE %:searchKey%) "
			+ "ORDER BY orderitem.ordertime DESC")
	public Long searchOrderedOrderCount(@Param(value="status") String status, @Param(value="searchKey") String searchKey);
	
	@Query(value="Select COUNT(DISTINCT orderitem) From Orderitem orderitem Where "
			+ "orderitem.status LIKE :status AND "
			+ "(orderitem.name LIKE %:searchKey% OR orderitem.cat LIKE %:searchKey%  OR orderitem.suppliercat LIKE %:searchKey% OR orderitem.supplier LIKE %:searchKey% OR orderitem.requestperson LIKE %:searchKey%) "
			+ "ORDER BY orderitem.receivetime DESC")
	public Long searchReceivedOrderCount(@Param(value="status") String status, @Param(value="searchKey") String searchKey);
	
	
	
//	@Query(value="Select order From Orderitem order Where order.status LIKE :status AND (order.name LIKE %:searchKey%)")
//	public List<Orderitem> searchOrderByNameWStatus(@Param(value="searchKey") String searchKey, @Param(value="status") String status, Pageable pageable);
//
//	@Query(value="Select order From Orderitem order Where order.status LIKE :status AND (order.name LIKE %:searchKey%)")
//	public List<Orderitem> searchOrderByCatWStatus(@Param(value="searchKey") String searchKey, @Param(value="status") String status, Pageable pageable);
//
//	@Query(value="Select order From Orderitem order Where order.status LIKE :status AND (order.name LIKE %:searchKey%)")
//	public List<Orderitem> searchOrderByDateWStatus(@Param(value="searchKey") String searchKey, @Param(value="status") String status, Pageable pageable);
//
//	@Query(value="Select order From Orderitem order Where order.status LIKE :status AND (order.name LIKE %:searchKey%)")
//	public List<Orderitem> searchOrderBySupplierWStatus(@Param(value="searchKey") String searchKey, @Param(value="status") String status, Pageable pageable);
//
//	@Query(value="Select orderitem From Orderitem orderitem Where orderitem.status LIKE :status ORDER BY orderitem.urgent DESC, orderitem.requesttime DESC")
//	public List<Orderitem> loadUrgentRequestedOrder(@Param(value="status") String status, Pageable pageable);
}
