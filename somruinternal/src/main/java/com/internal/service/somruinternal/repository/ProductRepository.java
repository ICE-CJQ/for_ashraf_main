package com.internal.service.somruinternal.repository;
import java.util.List;

import com.internal.service.somruinternal.model.Inventory;
import com.internal.service.somruinternal.model.Product;
import com.internal.service.somruinternal.model.Recipe;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{
//	@Query(value="Select DISTINCT product from Product product Where product.name LIKE %:searchKey% OR recipe.name LIKE %:searchKey% ORDER By recipe.dbid DESC")
//	public List<Recipe> searchProductByAll(@Param(value="searchKey") String searckKey, Pageable pageable);
		
	@Query(value="Select product from Product product Where product.name LIKE %:searchKey% OR product.cat LIKE %:searchKey% ORDER By product.dbid DESC")
	public List<Product> searchProductByName(@Param(value="searchKey") String searckKey, Pageable pageable);
	
	
//	@Query(value="Select COUNT(DISTINCT recipe) from Recipe recipe Where recipe.somruid LIKE %:searchKey% OR recipe.name LIKE %:searchKey%")
//	public Long searchProductByAllCount(@Param("searchKey") String searchKey);
	
	@Query(value="Select COUNT(product) from Product product Where product.name LIKE %:searchKey% OR product.cat LIKE %:searchKey%")
	public Long searchProductByNameCount(@Param("searchKey") String searchKey);
	
	
	
	
	
	
	
	
}
