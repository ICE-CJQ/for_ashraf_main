package com.internal.service.somruinternal.repository;
import com.internal.service.somruinternal.model.Sublocation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SublocationRepository extends JpaRepository<Sublocation, Long>{

}
