package com.internal.service.somruinternal.repository;
import com.internal.service.somruinternal.model.Concentrationunit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConcentrationunitRepository extends JpaRepository<Concentrationunit, Long>{

}
