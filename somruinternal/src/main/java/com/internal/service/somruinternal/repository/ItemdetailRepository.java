package com.internal.service.somruinternal.repository;
import java.util.List;

import com.internal.service.somruinternal.model.ClientContact;
import com.internal.service.somruinternal.model.Inventory;
import com.internal.service.somruinternal.model.Itemdetail;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemdetailRepository extends JpaRepository<Itemdetail, Long>{
	@Query(value="Select detail From Itemdetail detail Where detail.itemdbid = :id AND (detail.amount !='0' OR  detail.inuse !='0') ")
//	@Query(value="Select detail From Itemdetail detail Where detail.itemdbid = :id")
	public List<Itemdetail> loadDetailsWItemId(@Param("id") long id);
	
	@Query(value="Delete From Itemdetail detail Where detail.itemdbid = :id")
	public void removeDetailWItemId(@Param("id") long id);
}
