package com.internal.service.somruinternal.controller;

import com.internal.service.somruinternal.model.Equipment;
import com.internal.service.somruinternal.repository.EquipmentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

@RestController
@RequestMapping("/equipment")
public class EquipmentControl {
    @Autowired
    EquipmentRepository equipRepo;
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/all")
    public List<Equipment> getAllEquips() {
        return equipRepo.findAll();
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/add")
    public Equipment createEquip(@Valid @RequestBody Equipment equip) {
        return equipRepo.save(equip);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/get/{id}")
    public ResponseEntity<Equipment> getEquipById(@PathVariable(value = "id") Long equipId) {
    	Equipment equip = equipRepo.findOne(equipId);
        if(equip == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(equip);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<Equipment> updateEquip(@PathVariable(value = "id") Long  equipId, 
                                           @Valid @RequestBody Equipment equipDetail) {
    	Equipment equip= equipRepo.findOne(equipId);
        if(equip == null) {
            return ResponseEntity.notFound().build();
        }
        
        equip.setId(equipDetail.getId());
        equip.setName(equipDetail.getName());
        equip.setSerial(equipDetail.getSerial());
        equip.setModel(equipDetail.getModel());
        equip.setManufactor(equipDetail.getManufactor());
        equip.setCurrentLocation(equipDetail.getCurrentLocation());
        equip.setComment(equipDetail.getComment());

        Equipment UpdateEquip = equipRepo.save(equip);
        return ResponseEntity.ok(UpdateEquip);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Equipment> deleteEquip(@PathVariable(value = "id") Long equipId) {
    	Equipment equip = equipRepo.findOne(equipId);
        if(equip == null) {
            return ResponseEntity.notFound().build();
        }

        equipRepo.delete(equip);
        return ResponseEntity.ok().build();
    }
}
