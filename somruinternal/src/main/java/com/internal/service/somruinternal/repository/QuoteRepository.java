package com.internal.service.somruinternal.repository;
import java.util.List;

import com.internal.service.somruinternal.model.Quote;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface QuoteRepository extends JpaRepository<Quote, Long>{
	@Query(value="Select quote from Quote quote Where quote.quoteNum LIKE %:searchKey% Order By "
			+ "case when quote.expireDate <= CURRENT_DATE then 1 else 0 end ASC,"
			+ "quote.complete ASC,"
			+ "case when quote.expireDate > CURRENT_DATE then 1 else 0 end")
	public List<Quote> searchByNum(@Param(value="searchKey") String searchKey, Pageable pageable);
	
//	@Query(value="Select quote from Quote quote Order By "
//			+ "case when quote.expireDate <= CURRENT_DATE then 1 else 0 end ASC,"
//			+ "quote.complete ASC,"
//			+ "case when quote.expireDate > CURRENT_DATE then 1 else 0 end")
//	public List<Quote> quoteByPage(Pageable pageable);
	
	
	@Query(value="Select quote from Quote quote Order By quote.quoteNum DESC")
	public List<Quote> quoteByPage(Pageable pageable);
	
	
}
