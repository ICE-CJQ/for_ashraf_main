package com.internal.service.somruinternal.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.internal.service.somruinternal.model.Kit;
import com.internal.service.somruinternal.model.KitComponent;

@Repository
public interface KitComponentRepository  extends JpaRepository<KitComponent, Long>{
	
	@Query(value="Select kitcomponent From KitComponent kitcomponent Where kitcomponent.kitid = :id")
	public List<KitComponent> searchKitComponentsByKitid(@Param(value="id") Long id);
	
	@Query(value="Delete From KitComponent kitcomp Where kitcomp.kitid = :id")
	public void removeCompWKitId(@Param(value="id") Long id);
	
	
}
