package com.internal.service.somruinternal.model;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

import java.util.Date;

@Entity
@Table(name = "Recipe")
@EntityListeners(AuditingEntityListener.class)
//@JsonIgnoreProperties(value = {"lastmodify"}, 
        //allowGetters = true)
public class Recipe {
	public Recipe() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Recipe(long dbid, String somruid, String name, String volumn, String unit, String description, String direction, String enteredby, String reviewedby, String approvedby, Date entertime, Date reviewtime, Date approvetime,String reviewcomment, String approvecomment, String editstatus) {
		super();
		this.dbid = dbid;
		this.somruid=somruid;
		this.name = name;
		this.volumn = volumn;
		this.unit = unit;
		this.description = description;
		this.direction = direction;
		this.enteredby = enteredby;
		this.reviewedby = reviewedby;
		this.approvedby = approvedby;
		this.entertime = entertime;
		this.reviewtime = reviewtime;
		this.approvetime = approvetime;
		this.reviewcomment = reviewcomment;
		this.approvecomment=approvecomment;
		this.editstatus=editstatus;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	
	@Column(nullable = true)
	private String somruid;
	
	@NotBlank
	private String name;
	
	@Column(nullable = true)
	private String volumn;
	
	@Column(nullable = true)
	private String unit;
	
	@Column(nullable = true)
	private String description;
	
	@Column(nullable = true)
	private String direction;
	
	@Column(nullable = true)
	private String enteredby;
	
	@Column(nullable = true)
	private String reviewedby;
	
	@Column(nullable = true)
	private String approvedby;
	
	@Column(nullable = true)
	private Date entertime;
	
	@Column(nullable = true)
	private Date reviewtime;
	
	@Column(nullable = true)
	private Date approvetime;
	
	@Column(nullable = true,length=5000)
	private String reviewcomment;
	
	@Column(nullable = true,length=5000)
	private String approvecomment;

	@Column(nullable = true)
	private String editstatus;

	public long getDbid() {
		return dbid;
	}

	public void setDbid(long dbid) {
		this.dbid = dbid;
	}

	public String getSomruid() {
		return somruid;
	}

	public void setSomruid(String somruid) {
		this.somruid = somruid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getVolumn() {
		return volumn;
	}

	public void setVolumn(String volumn) {
		this.volumn = volumn;
	}
	
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getEnteredby() {
		return enteredby;
	}

	public void setEnteredby(String enteredby) {
		this.enteredby = enteredby;
	}

	public String getReviewedby() {
		return reviewedby;
	}

	public void setReviewedby(String reviewedby) {
		this.reviewedby = reviewedby;
	}

	public String getReviewcomment() {
		return reviewcomment;
	}

	public void setReviewcomment(String reviewcomment) {
		this.reviewcomment = reviewcomment;
	}

	public String getApprovedby() {
		return approvedby;
	}

	public void setApprovedby(String approvedby) {
		this.approvedby = approvedby;
	}

	public String getApprovecomment() {
		return approvecomment;
	}

	public void setApprovecomment(String approvecomment) {
		this.approvecomment = approvecomment;
	}

	public String getEditstatus() {
		return editstatus;
	}

	public void setEditstatus(String editstatus) {
		this.editstatus = editstatus;
	}
	
	public Date getEntertime() {
		return entertime;
	}

	public void setEntertime(Date entertime) {
		this.entertime = entertime;
	}

	public Date getReviewtime() {
		return reviewtime;
	}

	public void setReviewtime(Date reviewtime) {
		this.reviewtime = reviewtime;
	}

	public Date getApprovetime() {
		return approvetime;
	}

	public void setApprovetime(Date approvetime) {
		this.approvetime = approvetime;
	}

	@Override
	public String toString() {
		return "Recipe [dbid=" + dbid + ", somruid=" + somruid+ ", name=" + name + ", volumn=" + volumn + ", unit=" + unit + ", description="
				+ description + ", direction=" + direction +",  enteredby=" + enteredby + ", reviewedby=" + reviewedby + ", approvedby=" + approvedby + ", entertime=" + entertime + ", reviewtime=" + reviewtime + ", approvetime=" + approvetime + ", reviewcomment=" + reviewcomment + ",  approvecomment=" + approvecomment +", editstatus=" + editstatus +"]";
	}
}
