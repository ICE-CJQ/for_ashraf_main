package com.internal.service.somruinternal.controller;

import static org.springframework.data.domain.ExampleMatcher.matching;

import com.internal.service.somruinternal.model.Application;
import com.internal.service.somruinternal.model.Itemarchive;
import com.internal.service.somruinternal.model.Itemdetail;
import com.internal.service.somruinternal.model.Itemdetailarchive;
import com.internal.service.somruinternal.repository.ItemdetailarchiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/itemdetailarchive")
public class ItemdetailarchiveControl {

    @Autowired
    ItemdetailarchiveRepository itemdetailarchiverepo;   
 
    
    @GetMapping("/all")
    public List<Itemdetailarchive> getAllDrugs() {
        return itemdetailarchiverepo.findAll();
    }
    
  
    @PostMapping("/add")
    public Itemdetailarchive createDrug(@Valid @RequestBody Itemdetailarchive itemdetailarchive) {
    	return itemdetailarchiverepo.save(itemdetailarchive);
    }
    

    
    @GetMapping("/get/{id}")
    public ResponseEntity<Itemdetailarchive> getDrugById(@PathVariable(value = "id") Long drugId) {
    	Itemdetailarchive itemdetailarchive = itemdetailarchiverepo.findOne(drugId);
        if(itemdetailarchive == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(itemdetailarchive);
    }
    
   
    @GetMapping("/getItemdetailarchiveByItemdetaildbid/{id}")
    public List<Itemdetailarchive> getItemdetailarchiveByItemdetaildbid(@PathVariable(value = "id") Long itemdetaildbid) {
    	Example<Itemdetailarchive> example = Example.of(new Itemdetailarchive((long)-1, "", new Date(), "", itemdetaildbid, 
    			(long)-1, "", "","","", "", new Date(), new Date(),"","",
    			"","","","","","","","","","","","","","",
    			new Date(),"","", "","","","","","","", new Date(),"" ), matching(). //
				withIgnorePaths("Itemdetailarchivedbid",  "action", "createtime", "createperson", "itemdbid", "itemname", "itemtype", "supplier", "parentlotnumber",
				"lotnumber","receivedate","expiredate","location","sublocation","amount","inuse","concentration","concentrationunit",
				"species","clonality","host","conjugate", "iggdepletion", "purification","volume","volumeunit",
				"weight","weightunit","apcolumnprepdate","usagecolumn","columnnumber","comment","reserve",
				"projectnumber", "receiveperson","unit","unitsize","recon","modifydate","modifyperson"));
    	return itemdetailarchiverepo.findAll(example);
    }
    
    
//    @PutMapping("/update/{id}")
//    public ResponseEntity<Itemarchive> updateDrug(@PathVariable(value = "id") Long  drugId, 
//                                           @Valid @RequestBody Itemarchive drugDetail) {
//    	Itemarchive drug= itemarchiverepo.findOne(drugId);
//        if(drug == null) {
//            return ResponseEntity.notFound().build();
//        }
//        
//        drug.setDbid(drugDetail.getDbid());
//        drug.setCat(drugDetail.getCat());
//        drug.setSuppliercat(drugDetail.getSuppliercat());
//        drug.setName(drugDetail.getName());
//        drug.setType(drugDetail.getType());
//        drug.setActive(drugDetail.isActive());
//        drug.setClientspecific(drugDetail.getClientspecific());
//        drug.setSupplier(drugDetail.getSupplier());
//        drug.setManufacturer(drugDetail.getManufacturer()); 
//        drug.setUnit(drugDetail.getUnit()); 
//        drug.setUnitprice(drugDetail.getUnitprice()); 
//        drug.setUnitsize(drugDetail.getUnitsize()); 
//        drug.setQuantitythreshold(drugDetail.getQuantitythreshold());        
//        drug.setSupplylink(drugDetail.getSupplylink());
//        drug.setManufacturerlink(drugDetail.getManufacturerlink());
//        drug.setLastmodify(drugDetail.getLastmodify());
//        drug.setModifyperson(drugDetail.getModifyperson());
//        drug.setComment(drugDetail.getComment());
//
//        
//        Itemarchive UpdateEquip = itemarchiverepo.save(drug);
//        return ResponseEntity.ok(UpdateEquip);
//    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Itemarchive> deleteDrug(@PathVariable(value = "id") Long drugId) {
    	Itemdetailarchive drug = itemdetailarchiverepo.findOne(drugId);
        if(drug == null) {
            return ResponseEntity.notFound().build();
        }


        itemdetailarchiverepo.delete(drug);
        return ResponseEntity.ok().build();
    }
}
