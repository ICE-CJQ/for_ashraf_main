package com.internal.service.somruinternal.model;

import javax.persistence.*;
import java.util.Date;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@Entity
@Table(name = "item_details")
public class Itemdetail {

	public Itemdetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Itemdetail(Long dbid, Long itemdbid,  String itemcategory,String itemname, String itemtype, String supplier, String parentlotnumber,
			String lotnumber, Date receivedate, Date expiredate,Date retestdate, String batchnumber, String storetemperature, String purity, String rfstatus,  String rffile,
			
			String conjugatechemistry, String biotintobiomoleculeratio, String descriptionpreparation,
			String conjugateprepperson, Date conjugateprepdate, String moleculeweight,
			String bindingactivity, String conjugateincorporationratio, String constatus, String confile,
			String bca, String runnumber,String biomoleculeinfo,String conjugateinfo,
			String location, String sublocation, String amount, String inuse, String concentration, String concentrationunit, String species, String clonality, String host, String conjugate, String iggdepletion, String purification, String volume, String volumeunit, String weight, String weightunit, Date apcolumnprepdate, String usagecolumn, String columnnumber, 
			String comment, String reserve, String projectnumber, String receiveperson, String unit, String unitsize, String recon, Date modifydate, String modifyperson) {
		super();
		this.dbid = dbid;
		this.itemdbid = itemdbid;
		this.itemcategory=itemcategory;
		this.itemname = itemname;
		this.itemtype = itemtype;
		this.supplier = supplier;
		this.parentlotnumber=parentlotnumber;
		this.lotnumber = lotnumber;
		this.receivedate = receivedate;
		this.expiredate=expiredate;
		
		this.retestdate=retestdate;
		this.batchnumber=batchnumber;
		this.storetemperature=storetemperature;
		this.purity=purity;
		this.rfstatus=rfstatus;
		this.rffile=rffile;
		
		this.conjugatechemistry=conjugatechemistry;
		this.biotintobiomoleculeratio=biotintobiomoleculeratio;
		this.descriptionpreparation=descriptionpreparation;
		this.conjugateprepperson=conjugateprepperson;
		this.conjugateprepdate=conjugateprepdate;
		this.moleculeweight=moleculeweight;
		this.bindingactivity=bindingactivity;
		this.conjugateincorporationratio=conjugateincorporationratio;
		this.constatus=constatus;
		this.confile=confile;
		this.bca=bca;
		this.runnumber=runnumber;
		this.biomoleculeinfo=biomoleculeinfo;
		this.conjugateinfo=conjugateinfo;
		
		this.location=location;
		this.sublocation = sublocation;
		this.amount=amount;
		this.inuse =  inuse;
		this.concentration = concentration;
		this.concentrationunit = concentrationunit;
		this.species =species;
		this.clonality =clonality; 
		this.host =host; 
		this.conjugate =conjugate;
		this.iggdepletion =iggdepletion;
		this.purification =purification;
		this.volume =volume;
		this.volumeunit =volumeunit;
		this.weight =weight;
		this.weightunit =weightunit;
		this.apcolumnprepdate =apcolumnprepdate;
		this.usagecolumn=usagecolumn;
		this.columnnumber=columnnumber;
		this.comment=comment;
		this.reserve=reserve;
		this.projectnumber=projectnumber;
		this.receiveperson=receiveperson;
		this.unit=unit;
		this.unitsize=unitsize;
		this.recon=recon;
		this.modifydate=modifydate;
		this.modifyperson=modifyperson;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	
	@Column(nullable = false)
	private long itemdbid;
	
	@Column(nullable = true)
	private String itemcategory;	
	
	@Column(nullable = true)
	private String itemname;	
	
	@Column(nullable = true)
	private String itemtype;	
	
	@Column(nullable = true)
	private String supplier;	
	
	@Column(nullable = true)
	private String parentlotnumber;	
	
	@Column(nullable = false)
	private String lotnumber;

	@Column(nullable = true)
	private Date receivedate;
	
	@Column(nullable = true)
	private Date expiredate;
	
	@Column(nullable = true)
	private Date retestdate;
	
	@Column(nullable = true)
	private String batchnumber;
	
	@Column(nullable = true)
	private String storetemperature;
	
	@Column(nullable = true)
	private String purity;
	
	@Column(nullable = true)
	private String rfstatus;
	
	@Column(nullable = true)
	private String rffile;
	
	@Column(nullable = true)
	private String conjugatechemistry;
	@Column(nullable = true)
	private String biotintobiomoleculeratio;
	@Column(nullable = true,length=300)
	private String descriptionpreparation;
	@Column(nullable = true)
	private String conjugateprepperson;
	@Column(nullable = true)
	private Date conjugateprepdate;
	@Column(nullable = true)
	private String moleculeweight;
	@Column(nullable = true)
	private String bindingactivity;
	@Column(nullable = true)
	private String conjugateincorporationratio;
	@Column(nullable = true)
	private String constatus;
	@Column(nullable = true)
	private String confile;
	@Column(nullable = true)
	private String bca;
	@Column(nullable = true)
	private String runnumber;
	@Column(nullable = true)
	private String biomoleculeinfo;
	@Column(nullable = true)
	private String conjugateinfo;
	

	@Column(nullable = true)
	private String location;
	
	@Column(nullable = false)
	private String sublocation;
	
	@Column(nullable = false, columnDefinition = "INT(11) UNSIGNED default '0'") 
	private String amount;
	
	@Column(nullable = false, columnDefinition = "INT(11) UNSIGNED default '0'")
	private String inuse;
	
	@Column(nullable = false)
	private String concentration;
	
	@Column(nullable = false)
	private String concentrationunit;
	
	@Column(nullable = true)
	private String species;
	
	@Column(nullable = true)
	private String clonality;
	
	@Column(nullable = true)
	private String host;
	
	@Column(nullable = true)
	private String conjugate;
	
	@Column(nullable = true)
	private String iggdepletion;
	
	@Column(nullable = true)
	private String purification;
	
	@Column(nullable = true)
	private String volume;
	
	@Column(nullable = true)
	private String volumeunit;
	
	@Column(nullable = true)
	private String weight;
	
	@Column(nullable = true)
	private String weightunit;
	
	@Column(nullable = true)
	private Date apcolumnprepdate;
	
	@Column(nullable = true)
	private String usagecolumn;
	
	@Column(nullable = true)
	private String columnnumber;
	
	@Column(nullable = false,length=5000)
	private String comment;	
	
	@Column(nullable = true)
	private String reserve;
	
	@Column(nullable = true)
	private String projectnumber;
	
	@Column(nullable = true)
	private String receiveperson;
	
	@Column(nullable = true)
	private String unit;
	
	@Column(nullable = true)
	private String unitsize;
	
	@Column(nullable = true)
	private String recon;
	
	@Column(nullable = true)
	private Date modifydate;
	
	@Column(nullable = true)
	private String modifyperson;
	
	public long getDbid() {
		return dbid;
	}

	public void setDbid(long dbid) {
		this.dbid = dbid;
	}

	public long getItemdbid() {
		return itemdbid;
	}

	public void setItemdbid(long itemdbid) {
		this.itemdbid = itemdbid;
	}

	public String getItemcategory() {
		return itemcategory;
	}

	public void setItemcategory(String itemcategory) {
		this.itemcategory = itemcategory;
	}

	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	public String getItemtype() {
		return itemtype;
	}

	public void setItemtype(String itemtype) {
		this.itemtype = itemtype;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getParentlotnumber() {
		return parentlotnumber;
	}

	public void setParentlotnumber(String parentlotnumber) {
		this.parentlotnumber = parentlotnumber;
	}

	public String getLotnumber() {
		return lotnumber;
	}

	public void setLotnumber(String lotnumber) {
		this.lotnumber = lotnumber;
	}

	public Date getReceivedate() {
		return receivedate;
	}

	public void setReceivedate(Date receivedate) {
		this.receivedate = receivedate;
	}

	public Date getExpiredate() {
		return expiredate;
	}

	public void setExpiredate(Date expiredate) {
		this.expiredate = expiredate;
	}

	public Date getRetestdate() {
		return retestdate;
	}

	public void setRetestdate(Date retestdate) {
		this.retestdate = retestdate;
	}



	public String getBatchnumber() {
		return batchnumber;
	}

	public void setBatchnumber(String batchnumber) {
		this.batchnumber = batchnumber;
	}

	public String getStoretemperature() {
		return storetemperature;
	}

	public void setStoretemperature(String storetemperature) {
		this.storetemperature = storetemperature;
	}

	public String getPurity() {
		return purity;
	}

	public void setPurity(String purity) {
		this.purity = purity;
	}

	public String getRfstatus() {
		return rfstatus;
	}

	public void setRfstatus(String rfstatus) {
		this.rfstatus = rfstatus;
	}

	
	
	public String getRffile() {
		return rffile;
	}

	public void setRffile(String rffile) {
		this.rffile = rffile;
	}

	public String getConjugatechemistry() {
		return conjugatechemistry;
	}

	public void setConjugatechemistry(String conjugatechemistry) {
		this.conjugatechemistry = conjugatechemistry;
	}

	public String getBiotintobiomoleculeratio() {
		return biotintobiomoleculeratio;
	}

	public void setBiotintobiomoleculeratio(String biotintobiomoleculeratio) {
		this.biotintobiomoleculeratio = biotintobiomoleculeratio;
	}

	public String getDescriptionpreparation() {
		return descriptionpreparation;
	}

	public void setDescriptionpreparation(String descriptionpreparation) {
		this.descriptionpreparation = descriptionpreparation;
	}

	public String getConjugateprepperson() {
		return conjugateprepperson;
	}

	public void setConjugateprepperson(String conjugateprepperson) {
		this.conjugateprepperson = conjugateprepperson;
	}

	public Date getConjugateprepdate() {
		return conjugateprepdate;
	}

	public void setConjugateprepdate(Date conjugateprepdate) {
		this.conjugateprepdate = conjugateprepdate;
	}

	public String getMoleculeweight() {
		return moleculeweight;
	}

	public void setMoleculeweight(String moleculeweight) {
		this.moleculeweight = moleculeweight;
	}

	public String getBindingactivity() {
		return bindingactivity;
	}

	public void setBindingactivity(String bindingactivity) {
		this.bindingactivity = bindingactivity;
	}

	public String getConjugateincorporationratio() {
		return conjugateincorporationratio;
	}

	public void setConjugateincorporationratio(String conjugateincorporationratio) {
		this.conjugateincorporationratio = conjugateincorporationratio;
	}

	public String getConstatus() {
		return constatus;
	}

	public void setConstatus(String constatus) {
		this.constatus = constatus;
	}

	public String getConfile() {
		return confile;
	}

	public void setConfile(String confile) {
		this.confile = confile;
	}

	public String getBca() {
		return bca;
	}

	public void setBca(String bca) {
		this.bca = bca;
	}



	public String getRunnumber() {
		return runnumber;
	}

	public void setRunnumber(String runnumber) {
		this.runnumber = runnumber;
	}

	public String getBiomoleculeinfo() {
		return biomoleculeinfo;
	}

	public void setBiomoleculeinfo(String biomoleculeinfo) {
		this.biomoleculeinfo = biomoleculeinfo;
	}

	public String getConjugateinfo() {
		return conjugateinfo;
	}

	public void setConjugateinfo(String conjugateinfo) {
		this.conjugateinfo = conjugateinfo;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getSublocation() {
		return sublocation;
	}

	public void setSublocation(String sublocation) {
		this.sublocation = sublocation;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getInuse() {
		return inuse;
	}

	public void setInuse(String inuse) {
		this.inuse = inuse;
	}

	public String getConcentration() {
		return concentration;
	}

	public void setConcentration(String concentration) {
		this.concentration = concentration;
	}

	public String getConcentrationunit() {
		return concentrationunit;
	}

	public void setConcentrationunit(String concentrationunit) {
		this.concentrationunit = concentrationunit;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getClonality() {
		return clonality;
	}

	public void setClonality(String clonality) {
		this.clonality = clonality;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getConjugate() {
		return conjugate;
	}

	public void setConjugate(String conjugate) {
		this.conjugate = conjugate;
	}

	public String getIggdepletion() {
		return iggdepletion;
	}

	public void setIggdepletion(String iggdepletion) {
		this.iggdepletion = iggdepletion;
	}

	public String getPurification() {
		return purification;
	}

	public void setPurification(String purification) {
		this.purification = purification;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getVolumeunit() {
		return volumeunit;
	}

	public void setVolumeunit(String volumeunit) {
		this.volumeunit = volumeunit;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getWeightunit() {
		return weightunit;
	}

	public void setWeightunit(String weightunit) {
		this.weightunit = weightunit;
	}

	public Date getApcolumnprepdate() {
		return apcolumnprepdate;
	}

	public void setApcolumnprepdate(Date apcolumnprepdate) {
		this.apcolumnprepdate = apcolumnprepdate;
	}

	public String getUsagecolumn() {
		return usagecolumn;
	}

	public void setUsagecolumn(String usagecolumn) {
		this.usagecolumn = usagecolumn;
	}

	public String getColumnnumber() {
		return columnnumber;
	}

	public void setColumnnumber(String columnnumber) {
		this.columnnumber = columnnumber;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getReserve() {
		return reserve;
	}

	public void setReserve(String reserve) {
		this.reserve = reserve;
	}

	public String getProjectnumber() {
		return projectnumber;
	}

	public void setProjectnumber(String projectnumber) {
		this.projectnumber = projectnumber;
	}

	public String getReceiveperson() {
		return receiveperson;
	}

	public void setReceiveperson(String receiveperson) {
		this.receiveperson = receiveperson;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getUnitsize() {
		return unitsize;
	}

	public void setUnitsize(String unitsize) {
		this.unitsize = unitsize;
	}

	public String getRecon() {
		return recon;
	}

	public void setRecon(String recon) {
		this.recon = recon;
	}

	public Date getModifydate() {
		return modifydate;
	}

	public void setModifydate(Date modifydate) {
		this.modifydate = modifydate;
	}

	public String getModifyperson() {
		return modifyperson;
	}

	public void setModifyperson(String modifyperson) {
		this.modifyperson = modifyperson;
	}

	@Override
	public String toString() {
		return "Itemdetail [dbid=" + dbid + ", "
				+ "itemdbid=" + itemdbid+ ", "
				+ "itemname=" + itemname + ", "
				+ "itemtype=" + itemtype + ", "
				+ "supplier=" + supplier + ", "
				+ "parentlotnumber=" + parentlotnumber + ", "
				+ "lotnumber="+ lotnumber + ", "
				+ "receivedate=" + receivedate + ", "
				+ "expiredate=" + expiredate + ", "
				+ "resetdate=" + retestdate + ", "
				
				+ "batchnumber=" + batchnumber + ", "
				+ "storetemperature=" + storetemperature + ", "
				+ "purity=" + purity + ", "
				+ "rfstatus=" + rfstatus + ", "
				
				+ "conjugatechemistry=" + conjugatechemistry + ", "
				+ "biotintobiomoleculeratio=" + biotintobiomoleculeratio + ", "
				+ "descriptionpreparation=" + descriptionpreparation + ", "
				+ "conjugateprepperson=" + conjugateprepperson + ", "
				+ "conjugateprepdate=" + conjugateprepdate + ", "
				+ "moleculeweight=" + moleculeweight + ", "
				+ "bindingactivity=" + bindingactivity + ", "
				+ "conjugateincorporationratio=" + conjugateincorporationratio + ", "
				+ "constatus=" + constatus + ", "
				+ "confile=" + confile + ", "
				+ "bca=" + bca + ", "
				+ "runnumber=" + runnumber + ", "
				+ "biomoleculeinfo=" + biomoleculeinfo + ", "
				+ "conjugateinfo=" + conjugateinfo + ", "
				

				
				+ "location=" + location + ", "
				+ "sublocation=" + sublocation + ", "
				+ "amount=" + amount + ", "
				+ "concentration=" + concentration +", "
				+ "concentrationunit=" + concentrationunit +", "
                + "species=" + species +", "
                + "clonality=" + clonality +", " 
                + "host=" + host +", "                 
                + "conjugate=" + conjugate +", "                 
                + "iggdepletion=" + iggdepletion +", "                
                + "purification=" + purification +", "                
                + "volume=" + volume +", "   
                + "volumeunit=" + volumeunit +", " 
                + "weight=" + weight +", " 
                + "weightunit=" + weightunit +", " 
                + "usagecolumn=" + usagecolumn +", "    
                + "columnnumber=" + columnnumber +", " 
                + "reserve=" + reserve +", " 
                + "projectnumber=" + projectnumber +", " 
                + "receiveperson=" + receiveperson +", " 
                + "unit=" + unit +", " 
                + "unitsize=" + unitsize +", " 
                + "recon=" + recon +", " 
                + "modifydate=" + modifydate +", " 
				+ "modifyperson=" + modifyperson +"]";
		
	}
}
