package com.internal.service.somruinternal.controller;

import com.internal.service.somruinternal.model.Sublocation;
import com.internal.service.somruinternal.repository.SublocationRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.data.domain.ExampleMatcher.matching;

import java.util.List;

@RestController
@RequestMapping("/sublocation")
public class SublocationControl {
    @Autowired
    SublocationRepository sublocationRepo;
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/all")
    public List<Sublocation> getAllSublocations() {
        return sublocationRepo.findAll();
    }
    
//  @CrossOrigin(origins = "http://localhost:4200")
  @GetMapping("/getSublocationByLocationId/{id}")
  public List<Sublocation> getSubocationByLocationId(@PathVariable(value = "id") Long locationid) {
  	Example<Sublocation> example = Example.of(new Sublocation((long)-1, locationid, " "), matching(). //
				withIgnorePaths("dbid", "sublocationname"));
  	return sublocationRepo.findAll(example);
  }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/add")
    public Sublocation createSublocation(@Valid @RequestBody Sublocation sublocation) {
    	return sublocationRepo.save(sublocation);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/get/{id}")
    public ResponseEntity<Sublocation> getSublocationById(@PathVariable(value = "id") Long sublocationId) {
    	Sublocation sublocation = sublocationRepo.findOne(sublocationId);
        if(sublocation == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(sublocation);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<Sublocation> updateSublocation(@PathVariable(value = "id") Long  sublocationId, 
                                           @Valid @RequestBody Sublocation sublocationDetail) {
    	Sublocation sublocation= sublocationRepo.findOne(sublocationId);
        if(sublocation == null) {
            return ResponseEntity.notFound().build();
        }
        
        sublocation.setDbid(sublocationDetail.getDbid());
        sublocation.setLocationid(sublocationDetail.getLocationid());
        sublocation.setSublocationname(sublocationDetail.getSublocationname());
        
        Sublocation UpdateEquip = sublocationRepo.save(sublocation);
        return ResponseEntity.ok(UpdateEquip);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Sublocation> deleteSublocation(@PathVariable(value = "id") Long sublocationId) {
    	Sublocation sublocation = sublocationRepo.findOne(sublocationId);
        if(sublocation == null) {
            return ResponseEntity.notFound().build();
        }

        sublocationRepo.delete(sublocation);
        return ResponseEntity.ok().build();
    }
}
