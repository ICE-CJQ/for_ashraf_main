package com.internal.service.somruinternal.controller;

import com.internal.service.somruinternal.model.Transaction;
import com.internal.service.somruinternal.repository.ClientCompanyRepository;
import com.internal.service.somruinternal.repository.ClientContactRepository;
import com.internal.service.somruinternal.repository.IngredientRepository;
import com.internal.service.somruinternal.repository.InventoryRepository;
import com.internal.service.somruinternal.repository.ItemdetailRepository;
import com.internal.service.somruinternal.repository.KitComponentRepository;
import com.internal.service.somruinternal.repository.KitRepository;
import com.internal.service.somruinternal.repository.OrderitemRepository;
import com.internal.service.somruinternal.repository.RecipeRepository;
import com.internal.service.somruinternal.repository.TransactionRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

@RestController
@RequestMapping("/transation")
public class TransactionControl {
    @Autowired
    TransactionRepository transRepo;
    
    InventoryRepository invenRepo;
    ItemdetailRepository itemDetailRepo;
    KitRepository kitRepo;
    KitComponentRepository kitCompRepo; 
    RecipeRepository recipeRepo;
    IngredientRepository ingreRepo;
    OrderitemRepository orderRepo;
    ClientCompanyRepository compRepo;
    ClientContactRepository contactRepo;
    
    @GetMapping("/all/{table}")
    public List<Transaction> getAllEquips(@PathVariable(value ="table") String table) {
        return transRepo.loadTransByTable(table);
    }
    
    @PostMapping("/add")
    public Transaction addTransaction(@Valid @RequestBody Transaction trans) {
        return transRepo.save(trans);
    }
    
    @DeleteMapping("/reverse/{id}")
    public ResponseEntity<Transaction> ReverseTrans(@PathVariable(value = "id") Long transId) {
    	Transaction trans = transRepo.findOne(transId);
        if(trans == null) {
            return ResponseEntity.notFound().build();
        }
        
        if(trans.getTable() == "item"){
        	if(trans.getAction() == "ADD"){
    			Long id = trans.getId();
    			if(id != null){
        			invenRepo.delete(id);
        			itemDetailRepo.removeDetailWItemId(id);
    			}
        	}
        	else if(trans.getAction() == "UPDATE"){}
        	else if(trans.getAction() == "DEL"){}
        }
        if(trans.getTable() == "Kit"){
        	if(trans.getAction() == "ADD"){
        		Long id = trans.getId();
    			if(id != null){
        			kitRepo.delete(id);
        			kitCompRepo.removeCompWKitId(id);
    			}
        	}
        	else if(trans.getAction() == "UPDATE"){}
        	else if(trans.getAction() == "DEL"){}
        }
        if(trans.getTable() == "Recipe"){
        	if(trans.getAction() == "ADD"){
        		if(trans.getAction() == "ADD"){
        			Long id = trans.getId();
        			if(id != null){
            			recipeRepo.delete(id);
            			ingreRepo.removeIngreWRecipeId(id);
        			}
            	}
        	}
        	else if(trans.getAction() == "UPDATE"){}
        	else if(trans.getAction() == "DEL"){}
        }
        if(trans.getTable() == "orderitem"){
        	if(trans.getAction() == "ADD"){
        		if(trans.getAction() == "ADD"){
        			Long id = trans.getId();
        			if(id != null){
            			orderRepo.delete(id);
        			}
            	}
        	}
        	else if(trans.getAction() == "UPDATE"){}
        	else if(trans.getAction() == "DEL"){}
        }
        
        transRepo.delete(trans);
        return ResponseEntity.ok().body(trans);
    }
}
