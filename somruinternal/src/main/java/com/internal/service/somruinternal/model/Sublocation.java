package com.internal.service.somruinternal.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "sublocation")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"lastmodify"}, 
        allowGetters = true)
public class Sublocation {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	
	@Column(nullable = false)
	private long locationid;
		
	@Column(nullable = true)
	private String sublocationname;

	public Sublocation(
		Long dbid,
		Long locationid,
		String sublocationname){
		super();
		this.dbid = dbid;
		this.locationid=locationid;
		this.sublocationname = sublocationname;
	}
	
	public Sublocation(){
		super();
	}

	public long getDbid() {
		return dbid;
	}

	public void setDbid(long dbid) {
		this.dbid = dbid;
	}

	public long getLocationid() {
		return locationid;
	}

	public void setLocationid(long locationid) {
		this.locationid = locationid;
	}

	public String getSublocationname() {
		return sublocationname;
	}

	public void setSublocationname(String sublocationname) {
		this.sublocationname = sublocationname;
	}	
}
