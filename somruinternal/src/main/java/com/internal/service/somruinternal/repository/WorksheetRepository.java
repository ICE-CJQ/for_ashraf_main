package com.internal.service.somruinternal.repository;

import com.internal.service.somruinternal.model.Worksheet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorksheetRepository extends JpaRepository<Worksheet, Long> {
}
