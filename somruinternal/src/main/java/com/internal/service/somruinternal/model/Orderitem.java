package com.internal.service.somruinternal.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "orderitem")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"lastmodify"}, 
        allowGetters = true)
public class Orderitem {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	
	@Column(nullable = true)
	private String cat;
	
	@Column(nullable = true)
	private String category;
	
	@Column(nullable = true)
	private String suppliercat;
	
	@Column(nullable = true)
	private String name;
	
	@Column(nullable = true)
	private String type;
	
	@Column(nullable = true)
	private String supplier;
	
	@Column(nullable = true)
	private String manufacturer;
	
	@Column(nullable = true)
	private String amount;
	
	@Column(nullable = true)
	private String unreceiveamount;
	
	@Column(nullable = true)
	private String unit;
	
	@Column(nullable = true)
	private String unitsize;
	
	@Column(nullable = true)
	private String unitprice;
	
	@Column(nullable = true)
	private String grantid;
	
	@Column(nullable = true)
	private String requestperson;
	
	@Column(nullable = true)
	private String approveperson;
	
	@Column(nullable = true)
	private String orderperson;
	
	@Column(nullable = true)
	private String receiveperson;
	
	@Column(nullable = true)
	private String status;

	@Column(nullable = true)
	private Date requesttime;

	@Column(nullable = true)
	private Date approvetime;
	
	@Column(nullable = true)
	private Date ordertime;

	@Column(nullable = true)
	private Date receivetime;
	
	@Column(nullable = true)
	private Date eta;
	
	@Column(nullable = true)
	private Date emailsendtime;
	
	@Column(nullable = true)
	private boolean urgent;

	@Column(nullable = true,length=5000)
	private String comment;
	
	@Column(nullable = true)
	private String reserve;
	
	@Column(nullable = true)
	private String exist;
	
	public Orderitem(
		Long dbid,
		String cat,
		String category,
		String suppliercat,
		String name,
		String type,
		String supplier,
		String manufacturer,
		String amount,
		String unreceiveamount,
		String unit,
		String unitsize,
		String unitprice,
		String grantid,	
		String requestperson,
		String approveperson,
		String orderperson,
		String status,
		Date requesttime,
		Date approvetime,
		Date ordertime,
		Date receivetime,
		Date eta,
		Date emailsendtime,
		String receiveperson,
		boolean urgent,
		String comment,
		String reserve,
		String exist){
		super();
		this.dbid = dbid;
		this.cat=cat;
		this.category=category;
		this.suppliercat=suppliercat;
		this.name = name;
		this.type = type;
		this.supplier = supplier;
		this.manufacturer = manufacturer;
		this.amount=amount;
		this.unreceiveamount=unreceiveamount;
		this.unit=unit;
		this.unitsize = unitsize;
		this.unitprice = unitprice;
		this.grantid=grantid;
		this.requestperson=requestperson;
		this.approveperson=approveperson;
		this.orderperson=orderperson;
		this.status=status;
		this.requesttime = requesttime;
		this.approvetime = approvetime;
		this.ordertime = ordertime;
		this.receivetime = receivetime;
		this.eta = eta;
		this.emailsendtime = emailsendtime;
		this.receiveperson = receiveperson;
		this.urgent = urgent;
		this.comment = comment;
		this.reserve = reserve;
		this.exist = exist;
	}
	
	public Orderitem(){
		super();
	}

	public long getDbid() {
		return dbid;
	}

	public void setDbid(long dbid) {
		this.dbid = dbid;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSuppliercat() {
		return suppliercat;
	}

	public void setSuppliercat(String suppliercat) {
		this.suppliercat = suppliercat;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getUnreceiveamount() {
		return unreceiveamount;
	}

	public void setUnreceiveamount(String unreceiveamount) {
		this.unreceiveamount = unreceiveamount;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getUnitsize() {
		return unitsize;
	}

	public void setUnitsize(String unitsize) {
		this.unitsize = unitsize;
	}
	
	public String getUnitprice() {
		return unitprice;
	}

	public void setUnitprice(String unitprice) {
		this.unitprice = unitprice;
	}

	public String getGrantid() {
		return grantid;
	}

	public void setGrantid(String grantid) {
		this.grantid = grantid;
	}
	

	public String getRequestperson() {
		return requestperson;
	}

	public void setRequestperson(String requestperson) {
		this.requestperson = requestperson;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getRequesttime() {
		return requesttime;
	}

	public void setRequesttime(Date requesttime) {
		this.requesttime = requesttime;
	}

	public Date getApprovetime() {
		return approvetime;
	}

	public void setApprovetime(Date approvetime) {
		this.approvetime = approvetime;
	}

	public Date getOrdertime() {
		return ordertime;
	}

	public void setOrdertime(Date ordertime) {
		this.ordertime = ordertime;
	}

	public Date getReceivetime() {
		return receivetime;
	}

	public void setReceivetime(Date receivetime) {
		this.receivetime = receivetime;
	}

	public Date getEta() {
		return eta;
	}

	public void setEta(Date eta) {
		this.eta = eta;
	}

	public Date getEmailsendtime() {
		return emailsendtime;
	}

	public void setEmailsendtime(Date emailsendtime) {
		this.emailsendtime = emailsendtime;
	}

	public String getReceiveperson() {
		return receiveperson;
	}

	public void setReceiveperson(String receiveperson) {
		this.receiveperson = receiveperson;
	}

	public boolean isUrgent() {
		return urgent;
	}

	public void setUrgent(boolean urgent) {
		this.urgent = urgent;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getReserve() {
		return reserve;
	}

	public void setReserve(String reserve) {
		this.reserve = reserve;
	}

	public String getExist() {
		return exist;
	}

	public void setExist(String exist) {
		this.exist = exist;
	}

	public String getApproveperson() {
		return approveperson;
	}

	public void setApproveperson(String approveperson) {
		this.approveperson = approveperson;
	}

	public String getOrderperson() {
		return orderperson;
	}

	public void setOrderperson(String orderperson) {
		this.orderperson = orderperson;
	}
	

}
