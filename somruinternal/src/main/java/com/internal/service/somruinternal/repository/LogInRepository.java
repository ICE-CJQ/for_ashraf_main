package com.internal.service.somruinternal.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.internal.service.somruinternal.model.Inventory;
import com.internal.service.somruinternal.model.User;

@Repository
public interface LogInRepository  extends JpaRepository<User, Long>{
	
	@Query(value="Select distinct user From User user Where user.email LIKE :email")
	public User searchSingleUserByEmail(@Param(value="email") String email);

}
