package com.internal.service.somruinternal.repository;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.internal.service.somruinternal.model.Inventory;
import com.internal.service.somruinternal.model.Userhistory;

@Repository
public interface UserhistoryRepository  extends JpaRepository<Userhistory, Long>{

	@Query(value="Select userhistory From Userhistory userhistory Where Date(userhistory.time) BETWEEN CURDATE()-7  AND  CURDATE()")
	public List<Userhistory> loadWeekUserHistory(Pageable pageable);
	

	@Query(value="Select userhistory From Userhistory userhistory Where Date(userhistory.time) BETWEEN CURDATE() -30 AND  CURDATE()")
	public List<Userhistory> loadMonthUserHistory(Pageable pageable);
	
	@Query(value="Select userhistory From Userhistory userhistory Where Date(userhistory.time) BETWEEN CURDATE()-180 AND  CURDATE()")
	public List<Userhistory> load6MonthsUserHistory(Pageable pageable);
	
	@Query(value="Select COUNT(DISTINCT userhistory) From Userhistory userhistory Where Date(userhistory.time) BETWEEN CURDATE()-7  AND  CURDATE()")
	public Long loadWeekUserHistoryCount();
	
	@Query(value="Select COUNT(DISTINCT userhistory) From Userhistory userhistory Where Date(userhistory.time) BETWEEN CURDATE()-30 AND  CURDATE()")
	public Long loadMonthUserHistoryCount();
	
	@Query(value="Select COUNT(DISTINCT userhistory) From Userhistory userhistory Where Date(userhistory.time) BETWEEN CURDATE()-180 AND  CURDATE()")
	public  Long load6MonthsUserHistoryCount();
	
	
	@Query(value="Select userhistory From Userhistory userhistory Where Date(userhistory.time) BETWEEN CURDATE()-7  AND  CURDATE() AND userhistory.username LIKE %:name%")
	public List<Userhistory> loadWeekUserHistoryByName(@Param("name") String name, Pageable pageable);
	
	@Query(value="Select userhistory From Userhistory userhistory Where Date(userhistory.time) BETWEEN CURDATE() -30 AND  CURDATE() AND userhistory.username LIKE %:name%")
	public List<Userhistory> loadMonthUserHistoryByName(@Param("name") String name, Pageable pageable);
	
	@Query(value="Select userhistory From Userhistory userhistory Where Date(userhistory.time) BETWEEN CURDATE()-180 AND  CURDATE() AND userhistory.username LIKE %:name%")
	public List<Userhistory> load6MonthsUserHistoryByName(@Param("name") String name, Pageable pageable);
	
	@Query(value="Select COUNT(DISTINCT userhistory) From Userhistory userhistory Where Date(userhistory.time) BETWEEN CURDATE()-7  AND  CURDATE() AND userhistory.username LIKE %:name%")
	public Long loadWeekUserHistoryCountByName(@Param("name") String name);
	
	@Query(value="Select COUNT(DISTINCT userhistory) From Userhistory userhistory Where Date(userhistory.time) BETWEEN CURDATE()-30 AND  CURDATE() AND userhistory.username LIKE %:name%")
	public Long loadMonthUserHistoryCountByName(@Param("name") String name);
	
	@Query(value="Select COUNT(DISTINCT userhistory) From Userhistory userhistory Where Date(userhistory.time) BETWEEN CURDATE()-180 AND  CURDATE() AND userhistory.username LIKE %:name%")
	public  Long load6MonthsUserHistoryCountByName(@Param("name") String name);
	
	

	

	
}
