package com.internal.service.somruinternal.model;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "User")
@EntityListeners(AuditingEntityListener.class)
public class User {
	public User() {
		super();
	}
	
	public User(long dbid, String name, String role, String email, String password, long invalidlogincount, boolean active) {
		super();
		this.dbid = dbid;
		this.name = name;
		this.role = role;
		this.email = email;
		this.password = password;
		this.invalidlogincount = invalidlogincount;
		this.active = active;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	@NotBlank
	private String name;
	@NotBlank
	private String password;
	@NotBlank
	private String role;
	@NotBlank
	private String email;
	private long invalidlogincount;
	private boolean active;
	
	
	public long getDbid() {
		return dbid;
	}
	public void setDbid(long dbid) {
		this.dbid = dbid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPasword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public long getInvalidlogincount() {
		return invalidlogincount;
	}

	public void setInvalidlogincount(long invalidlogincount) {
		this.invalidlogincount = invalidlogincount;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}


	
}
