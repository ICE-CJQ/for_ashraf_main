package com.internal.service.somruinternal.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.internal.service.somruinternal.model.DrugList;
import com.internal.service.somruinternal.repository.DrugListRepository;

@RestController
@RequestMapping("/drugList")
public class DrugListControl {
	@Autowired
	DrugListRepository drugListRepo;
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/all")
    public List<DrugList> getAlldrugLists() {
        return drugListRepo.findAll();
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/add")
    public DrugList createdrugList(@Valid @RequestBody DrugList drugList) {
    	List<DrugList> list = new ArrayList<>();
    	list = drugListRepo.findAll();
    	
    	list.sort(new SortbyNum());
    	if(list.get(list.size() -1).getNum()+1 != drugList.getNum()){
    		long num = list.get(list.size() -1).getNum()+1;
    		drugList.setNum(num);
    	}
    	
		return drugListRepo.save(drugList);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/get/{id}")
    public ResponseEntity<DrugList> getdrugListById(@PathVariable(value = "id") Long drugListId) {
    	DrugList drugList = drugListRepo.findOne(drugListId);
        if(drugList == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(drugList);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<DrugList> updatedrugList(@PathVariable(value = "id") Long  drugListId, 
                                           @Valid @RequestBody DrugList drugListDetail) {
    	DrugList drugList= drugListRepo.findOne(drugListId);
        if(drugList == null) {
            return ResponseEntity.notFound().build();
        }
        
        drugList.setDbid(drugListDetail.getDbid());
        drugList.setName(drugListDetail.getName());
        drugList.setNum(drugListDetail.getNum());
        DrugList UpdatedrugList = drugListRepo.save(drugList);
        return ResponseEntity.ok(UpdatedrugList);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<DrugList> deletedrugList(@PathVariable(value = "id") Long drugListId) {
    	DrugList drugList = drugListRepo.findOne(drugListId);
        if(drugList == null) {
            return ResponseEntity.notFound().build();
        }

        drugListRepo.delete(drugList);
        return ResponseEntity.ok().build();
    }
}

class SortbyNum implements Comparator<DrugList>
{
    // Used for sorting in ascending order of
    // roll number
    public int compare(DrugList a, DrugList b)
    {
        return (int)(a.getNum() - b.getNum());
    }
}
