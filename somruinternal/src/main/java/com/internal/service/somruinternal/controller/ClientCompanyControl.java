package com.internal.service.somruinternal.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.internal.service.somruinternal.model.ClientCompany;
import com.internal.service.somruinternal.objects.CompanyWContacts;
import com.internal.service.somruinternal.repository.ClientCompanyRepository;
import com.internal.service.somruinternal.repository.ClientContactRepository;


@RestController
@RequestMapping("/clientcompany")
public class ClientCompanyControl {
	@Autowired
    ClientCompanyRepository clientRepo;
	@Autowired
    ClientContactRepository contactRepo;
    
    @GetMapping("/all")
    public List<ClientCompany> getAllclients() {
        return clientRepo.findAll();
    }
    
    @GetMapping("/all/{page}")
    public List<ClientCompany> getClientsByPage(@PathVariable(value = "page") int page) {
        return clientRepo.findAll(new PageRequest(page, 10, Sort.Direction.ASC, "company")).getContent();
    }
    
    @GetMapping("/count")
    public int getClientCount() {
        return clientRepo.findAll().size();
    }
    
	@GetMapping("/allwithContact/{page}")
	public List<CompanyWContacts> getAllClientsWithContacts(@PathVariable(value = "page") int page) {
	    List<ClientCompany> company = clientRepo.findAll(new PageRequest(page, 10, Sort.Direction.DESC, "dbid")).getContent();
		List<CompanyWContacts> result = new ArrayList();
	    int i = 0;
	    for(i = 0; i < company.size(); i++){
	    	long companyId = company.get(i).getDbid();
	    	CompanyWContacts record = new CompanyWContacts(company.get(i), contactRepo.loadContactWithComId(companyId));
	    	result.add(record);
	    }
	    return result;
	}
	
	@GetMapping("/searchClient/{keyword}/{page}")
	public List<CompanyWContacts> search(@PathVariable(value = "keyword") String keyword, @PathVariable(value = "page") int page){
		List<ClientCompany> company = clientRepo.searchClient(keyword, new PageRequest(page, 10));
		List<CompanyWContacts> result = new ArrayList();
	    int i = 0;
	    for(i = 0; i < company.size(); i++){
	    	long companyId = company.get(i).getDbid();
	    	CompanyWContacts record = new CompanyWContacts(company.get(i), contactRepo.loadContactWithComId(companyId));
	    	result.add(record);
	    }
	    return result;
	}
	
//	@GetMapping("/searchClientCount/{keyword}/")
//	public Long searchClientCount(@PathVariable(value = "keyword") String keyword){
//	    return clientRepo.searchClientCount(keyword);
//	}
	
	@GetMapping("/searchClientWName/{keyword}/{page}")
	public List<CompanyWContacts> searchByName(@PathVariable(value = "keyword") String keyword, @PathVariable(value = "page") int page){
		List<ClientCompany> company = clientRepo.searchByName(keyword, new PageRequest(page, 10));
		List<CompanyWContacts> result = new ArrayList();
	    int i = 0;
	    for(i = 0; i < company.size(); i++){
	    	long companyId = company.get(i).getDbid();
	    	CompanyWContacts record = new CompanyWContacts(company.get(i), contactRepo.loadContactWithComId(companyId));
	    	result.add(record);
	    }
	    return result;
	}
	
	@GetMapping("/searchClientWShipAd/{keyword}/{page}")
	public List<CompanyWContacts> searchByShipAddress(@PathVariable(value = "keyword") String keyword, @PathVariable(value = "page") int page){
		List<ClientCompany> company = clientRepo.searchByShipAddress(keyword, new PageRequest(page, 10));
		List<CompanyWContacts> result = new ArrayList();
	    int i = 0;
	    for(i = 0; i < company.size(); i++){
	    	long companyId = company.get(i).getDbid();
	    	CompanyWContacts record = new CompanyWContacts(company.get(i), contactRepo.loadContactWithComId(companyId));
	    	result.add(record);
	    }
	    return result;
	}
	
	@GetMapping("/searchClientWBillAd/{keyword}/{page}")
	public List<CompanyWContacts> searchByBillAddress(@PathVariable(value = "keyword") String keyword, @PathVariable(value = "page") int page){
		List<ClientCompany> company = clientRepo.searchByBillAddress(keyword, new PageRequest(page, 10));
		List<CompanyWContacts> result = new ArrayList();
	    int i = 0;
	    for(i = 0; i < company.size(); i++){
	    	long companyId = company.get(i).getDbid();
	    	CompanyWContacts record = new CompanyWContacts(company.get(i), contactRepo.loadContactWithComId(companyId));
	    	result.add(record);
	    }
	    return result;
	}
	
	@GetMapping("/searchClientWContacts/{keyword}/{page}")
	public List<CompanyWContacts> searchByContact(@PathVariable(value = "keyword") String keyword, @PathVariable(value = "page") int page){
		List<ClientCompany> company = clientRepo.searchByContact(keyword, new PageRequest(page, 10));
		List<CompanyWContacts> result = new ArrayList();
	    int i = 0;
	    for(i = 0; i < company.size(); i++){
	    	long companyId = company.get(i).getDbid();
	    	CompanyWContacts record = new CompanyWContacts(company.get(i), contactRepo.loadContactWithComId(companyId));
	    	result.add(record);
	    }
	    return result;
	}
    
    @PostMapping("/add")
    public ClientCompany createclient(@Valid @RequestBody ClientCompany client) {
        return clientRepo.save(client);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/get/{id}")
    public ResponseEntity<ClientCompany> getclientById(@PathVariable(value = "id") Long clientId) {
    	ClientCompany client = clientRepo.findOne(clientId);
        if(client == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(client);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<ClientCompany> updateclient(@PathVariable(value = "id") Long  clientId, 
                                           @Valid @RequestBody ClientCompany clientDetail) {
    	ClientCompany client= clientRepo.findOne(clientId);
        if(client == null) {
            return ResponseEntity.notFound().build();
        }
        
        client.setDbid(clientDetail.getDbid());
        client.setCompany(clientDetail.getCompany());
        client.setAddress(clientDetail.getAddress());
        client.setAddress2(clientDetail.getAddress2());
        client.setCity(clientDetail.getCity());
        client.setProvince(clientDetail.getProvince());
        client.setCountry(clientDetail.getCountry());
        client.setPostalCode(clientDetail.getPostalCode());
        client.setBaddress(clientDetail.getBaddress());
        client.setBaddress2(clientDetail.getBaddress2());
        client.setBcity(clientDetail.getBcity());
        client.setBprovince(clientDetail.getBprovince());
        client.setBcountry(clientDetail.getBcountry());
        client.setBpostalCode(clientDetail.getBpostalCode());
        client.setEditby(clientDetail.getEditby());
        client.setModify(clientDetail.getModify());
        

        ClientCompany Updateclient = clientRepo.save(client);
        return ResponseEntity.ok(Updateclient);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ClientCompany> deleteclient(@PathVariable(value = "id") Long clientId) {
    	ClientCompany client = clientRepo.findOne(clientId);
        if(client == null) {
            return ResponseEntity.notFound().build();
        }

        clientRepo.delete(client);
        return ResponseEntity.ok().build();
    }
}
