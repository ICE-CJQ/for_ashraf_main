package com.internal.service.somruinternal.utils;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.mail.MailProperties;

import com.internal.service.somruinternal.model.User;
import com.internal.service.somruinternal.objects.emailDetail;

import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import java.util.List;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import com.internal.service.somruinternal.model.Itemdetail;

@Component
public class sendEmailUtil {
	@Autowired
    public JavaMailSender javamailsender;
	@Autowired	
	private MailSender mailsender;
	@Autowired
	private TemplateEngine templateengine;
	@Autowired
	MailProperties  mailproperties;
	
	public static final String NO_ETA = "No eta";
	public static final String NOT_ARRIVE = "Not arrive";
	public static final String UN_APPROVE = "Not approve";
	public static final String REQUEST_RESET_PASSWORD = "Request reset password";
	


public void sendEmail(String emailType, String userEmail, String information,  emailDetail emaildetail) throws MessagingException {
	switch(emailType) {
	  case "No eta":
			SimpleMailMessage noetamsg = new SimpleMailMessage();
			String noetauseremails = "";
			String noetasubject;
			String noetacontent;
	        for(int i=0;i<emaildetail.getEmailAddressList().size();i++) {
	        	noetauseremails = noetauseremails + "" + emaildetail.getEmailAddressList().get(i);
	        }
	        noetasubject = emaildetail.getSubject();
	        noetacontent = emaildetail.getEmailContent();
	        noetamsg.setTo(noetauseremails);
	        noetamsg.setSubject(noetasubject);
	        noetamsg.setText(noetacontent);
	        javamailsender.send(noetamsg);
	    break;
	  case "Not arrive":
			SimpleMailMessage noarrivemsg = new SimpleMailMessage();
			String noarriveuseremails = "";
			String noarrivesubject;
			String noarrivecontent;
	        for(int i=0;i<emaildetail.getEmailAddressList().size();i++) {
	        	noarriveuseremails = noarriveuseremails + "" + emaildetail.getEmailAddressList().get(i);
	        }
	        noarrivesubject = emaildetail.getSubject();
	        noarrivecontent = emaildetail.getEmailContent();
	        noarrivemsg.setTo(noarriveuseremails);
	        noarrivemsg.setSubject(noarrivesubject);
	        noarrivemsg.setText(noarrivecontent);
	        javamailsender.send(noarrivemsg);
	    break;
	  case "Not approve":
			SimpleMailMessage noapprovemsg = new SimpleMailMessage();
			String noapprovesubject;
			String noapprovecontent;
			noapprovesubject = emaildetail.getSubject();
			noapprovecontent = emaildetail.getEmailContent();		
	        noapprovemsg.setTo(emaildetail.getEmailAddressList().get(0));
	        noapprovemsg.setCc(emaildetail.getEmailAddressList().get(1));    
	        noapprovemsg.setSubject(noapprovesubject);
	        noapprovemsg.setText(noapprovecontent);
	        javamailsender.send(noapprovemsg);
	    break;
	  case "Request reset password":
		  System.out.println(userEmail);
			String resetpasswordsubject;
			String resetpasswordcontent;
			resetpasswordsubject = "Reset your password for Intellilab";
			resetpasswordcontent = 
  		"<p>Follow the link below to reset your password for the account <a href=\"mailto:"+userEmail+"\" target=\"_blank\">"+userEmail+"</a></p>"+
  		"<a href=\"http://localhost:4200/resetpassword/"+information+"\""+">Reset Password</a>";
		    MimeMessage mail = javamailsender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mail, true);
			helper.setTo(userEmail);
			helper.setSubject(resetpasswordsubject);
			helper.setText(resetpasswordcontent, true);
			javamailsender.send(mail);
	  default:
	    // code block
	}
	
	

}





}

