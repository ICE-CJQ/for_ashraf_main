package com.internal.service.somruinternal.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "concentrationunit")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"lastmodify"}, 
        allowGetters = true)
public class Concentrationunit {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;

	@Column(nullable = true)
	private String concentrationunit;
	

	public Concentrationunit(
		Long dbid,
		String concentrationunit){
		super();
		this.dbid = dbid;
		this.concentrationunit = concentrationunit;
	}
	
	public Concentrationunit(){
		super();
	}

	public long getDbid() {
		return dbid;
	}

	public void setDbid(long dbid) {
		this.dbid = dbid;
	}

	public String getConcentrationunit() {
		return concentrationunit;
	}

	public void setConcentrationunit(String concentrationunit) {
		this.concentrationunit = concentrationunit;
	}

}
