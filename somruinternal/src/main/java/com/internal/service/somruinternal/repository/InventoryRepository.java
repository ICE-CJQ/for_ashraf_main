package com.internal.service.somruinternal.repository;
import java.util.List;

import com.internal.service.somruinternal.model.Inventory;
import com.internal.service.somruinternal.model.Itemdetail;
import com.internal.service.somruinternal.model.Kit;
import com.internal.service.somruinternal.model.KitComponent;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryRepository extends JpaRepository<Inventory, Long>{
	@Query(value="Select DISTINCT inven From Inventory inven, Itemdetail detail Where inven.active=true AND (inven.cat LIKE %:searchkey%  OR inven.suppliercat LIKE %:searchkey% OR inven.name LIKE %:searchkey% OR inven.supplier LIKE %:searchkey% OR inven.manufacturer LIKE %:searchkey% OR (detail.itemdbid = inven.dbid AND detail.location LIKE %:searchkey%) OR (detail.itemdbid = inven.dbid AND detail.sublocation LIKE %:searchkey%) ) ORDER By inven.dbid DESC")
	public List<Inventory> searchInven(@Param("searchkey") String searchkey, Pageable pageable);
	
	@Query(value="Select COUNT(DISTINCT inven) From Inventory inven, Itemdetail detail Where inven.active=true AND (inven.cat LIKE %:searchkey% OR inven.suppliercat LIKE %:searchkey% OR inven.name LIKE %:searchkey%  OR inven.supplier LIKE %:searchkey% OR inven.manufacturer LIKE %:searchkey% OR (detail.itemdbid = inven.dbid AND detail.location LIKE %:searchkey%) OR (detail.itemdbid = inven.dbid AND detail.sublocation LIKE %:searchkey%)) ")
	public Long searchInvenCount(@Param("searchkey") String searchkey);
	
	
	@Query(value="Select DISTINCT inven From Inventory inven Where inven.active=true AND inven.name LIKE %:searchkey% ORDER BY inven.dbid DESC")
	public List<Inventory> searchByName(@Param("searchkey") String searchkey, Pageable pageable);
	
	@Query(value="Select COUNT(DISTINCT inven.dbid) From Inventory inven Where inven.active=true AND inven.name LIKE %:searchkey% ")
	public Long searchByNameCount(@Param("searchkey") String searchkey);
	
	
	
	
	@Query(value="Select DISTINCT inven From Inventory inven Where inven.active=true ORDER BY inven.dbid DESC")
	public List<Inventory> searchByActive(Pageable pageable);
	
	@Query(value="Select COUNT(DISTINCT inven.dbid) From Inventory inven Where inven.active=true")
	public Long searchByActiveCount();
	
	
	
	@Query(value="Select DISTINCT inven From Inventory inven Where inven.active=true AND (inven.supplier LIKE %:searchkey% OR inven.manufacturer LIKE %:searchkey%)  ORDER BY inven.dbid DESC")
	public List<Inventory> searchBySupplierorManufacturer(@Param("searchkey") String searchkey, Pageable pageable);
	
	@Query(value="Select COUNT(DISTINCT inven.dbid) From Inventory inven Where inven.active=true AND (inven.supplier LIKE %:searchkey% OR inven.manufacturer LIKE %:searchkey%)")
	public Long searchBySupplierorManufacturerCount(@Param("searchkey") String searchkey);
	
	
	
	@Query(value="Select DISTINCT inven From Inventory inven Where inven.active=true AND (inven.cat LIKE %:searchkey% OR inven.suppliercat LIKE %:searchkey%)  ORDER BY inven.dbid DESC")
	public List<Inventory> searchByCat(@Param("searchkey") String searchkey, Pageable pageable);
	
	@Query(value="Select COUNT(DISTINCT inven.dbid) From Inventory inven Where inven.active=true AND (inven.cat LIKE %:searchkey%  OR inven.suppliercat LIKE %:searchkey% )")
	public Long searchByCatCount(@Param("searchkey") String searchkey);
	
	
	
	
	@Query(value="Select DISTINCT inven From Inventory inven, Itemdetail detail Where inven.active=true AND (detail.itemdbid = inven.dbid AND detail.location LIKE %:searchkey%) ORDER BY inven.dbid DESC")
	public List<Inventory> searchByLocaton(@Param("searchkey") String searchkey, Pageable pageable);
	
	@Query(value="Select COUNT(DISTINCT inven.dbid) From Inventory inven, Itemdetail detail Where inven.active=true AND (detail.itemdbid = inven.dbid AND detail.location LIKE %:searchkey% ) ")
	public Long searchByLocatonCount(@Param("searchkey") String searchkey);
	
	
	
	
	@Query(value="Select DISTINCT inven From Inventory inven, Itemdetail detail Where inven.active=true AND (detail.itemdbid = inven.dbid AND detail.sublocation LIKE %:searchkey%)  ORDER BY inven.dbid DESC")
	public List<Inventory> searchBySubLocaton(@Param("searchkey") String searchkey, Pageable pageable);
	
	@Query(value="Select COUNT(DISTINCT inven.dbid) From Inventory inven, Itemdetail detail Where inven.active=true AND (detail.itemdbid = inven.dbid AND detail.sublocation LIKE %:searchkey%)")
	public Long searchBySubLocatonCount(@Param("searchkey") String searchkey);
	
	
	

	@Query(value="Select DISTINCT inven from Inventory inven Where inven.active=true Order By inven.dbid DESC")
	public List<Inventory> loadReverse(Pageable pageable);
	
	@Query(value="Select DISTINCT inven From Inventory inven Where inven.active=true AND inven.cat = :cat")
	public Inventory searchSingleInventoryByCat(@Param(value="cat") String cat);
	
	@Query(value="Select DISTINCT inven From Inventory inven Where inven.active=true AND inven.name LIKE :name")
	public Inventory searchSingleInventoryByName(@Param(value="name") String name);
	
	
	
	
//	@Query(value="SELECT item.*, archive.* FROM item item, itemarchive archive WHERE item.dbid=archive.dbid AND item.dbid=dbid;")
//	public List<Inventory> searchInventoryandArchive(@Param("searchkey") Long dbid);
}
