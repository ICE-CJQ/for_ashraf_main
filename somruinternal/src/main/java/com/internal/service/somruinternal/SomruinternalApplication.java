package com.internal.service.somruinternal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableJpaAuditing
@EnableAutoConfiguration(exclude = {ErrorMvcAutoConfiguration.class})
public class SomruinternalApplication {

	public static void main(String[] args) {
		SpringApplication.run(SomruinternalApplication.class, args);
	}
	
	@Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("http://localhost:4200").allowedMethods("GET", "POST","PUT", "DELETE");
                //registry.addMapping("/**").allowedOrigins("http://192.168.0.2:4200").allowedMethods("GET", "POST","PUT", "DELETE");
                //registry.addMapping("/**").allowedOrigins("http://192.168.0.31:4200").allowedMethods("GET", "POST","PUT", "DELETE");
            }
        };
    }
	
	
    @Bean
    public RestTemplate getRestTemplate() {
       return new RestTemplate();
    }

}
