package com.internal.service.somruinternal.utils;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Value;
import com.internal.service.somruinternal.model.User;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
@Component
public class JwtTokenUtil implements Serializable {
private static final long serialVersionUID = -2550185165626007488L;
//public static final long JWT_TOKEN_VALIDITY = 1000*60*60;//1 hours
public static final long JWT_TOKEN_VALIDITY = 3600;//1 hour
public static final String JWT_TOKEN_SECRET = "jf9i4jgu83nfl0jfu57ejf7";
private static final String Object = null;
//public final String PASSWORD_RESET_TEXTBODY = 
//"<p>Follow the link below to reset your password for the account <a href=\"mailto:chen.fei@somrubioscience.com\" target=\"_blank\">chen.fei@somrubioscience.com</a></p>"+
//"<a href=\"http://localhost:4200/resetPassword/"+token+"/>Reset Password</a>";

		


//@Value("${jwt.secret}")
//private String secret;
//retrieve username from jwt token
public String getUsernameFromToken(String token) {
return getClaimFromToken(token, Claims::getSubject);
}
//retrieve expiration date from jwt token
public Date getExpirationDateFromToken(String token) {
return getClaimFromToken(token, Claims::getExpiration);
}
public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
final Claims claims = getAllClaimsFromToken(token);
return claimsResolver.apply(claims);
}
    //for retrieveing any information from token we will need the secret key
private Claims getAllClaimsFromToken(String token) {
return Jwts.parser().setSigningKey(JWT_TOKEN_SECRET).parseClaimsJws(token).getBody();
}
//check if the token has expired
private Boolean isTokenExpired(String token) {
final Date expiration = getExpirationDateFromToken(token);
return expiration.before(new Date());
}

public String generateToken(User user) {
	return Jwts.builder()
			.setSubject(user.getName())
			.claim("userid", user.getDbid())
			.claim("userpassword", user.getPassword())
			.setIssuedAt(new Date(System.currentTimeMillis()))
			.setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
			.signWith(SignatureAlgorithm.HS512, JWT_TOKEN_SECRET).compact();
}


//generate token for user
//public String generateToken(User user) {
//Map<String, Object> claims = new HashMap<>();
//return doGenerateToken(claims, user.getName());
//}
//while creating the token -
//1. Define  claims of the token, like Issuer, Expiration, Subject, and the ID
//2. Sign the JWT using the HS512 algorithm and secret key.
//3. According to JWS Compact Serialization(https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.1)
//   compaction of the JWT to a URL-safe string 
//private String doGenerateToken(Map<String, Object> claims, String subject) {
//return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
//.setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
//.signWith(SignatureAlgorithm.HS512, secret).compact();
//}
public Claims decodeToken(String token) {
    try {
    	Claims claims =Jwts.parser().setSigningKey(JWT_TOKEN_SECRET).parseClaimsJws(token).getBody();
    	System.out.println(getExpirationDateFromToken(token));
    	return claims;
    }catch (SignatureException ex){
    	System.out.println("Invalid JWT Signature");
    	return null;
    }catch (MalformedJwtException ex){
    	System.out.println("Invalid JWT token");
    	return null;
    }catch (ExpiredJwtException ex){
    	System.out.println("Expired JWT token");
    	return null;
    }catch (UnsupportedJwtException ex){
    	System.out.println("Unsupported JWT exception");
    	return null;
    }catch (IllegalArgumentException ex){
    	System.out.println("Jwt claims string is empty");
    	return null;
    }
}

//public Claims decodeToken(String token) {
//    	Claims claims =Jwts.parser().setSigningKey(JWT_TOKEN_SECRET).parseClaimsJws(token).getBody();
//    	return claims;
//
//}






}

