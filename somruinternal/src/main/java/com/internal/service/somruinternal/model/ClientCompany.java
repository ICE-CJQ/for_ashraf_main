package com.internal.service.somruinternal.model;
import org.hibernate.validator.constraints.NotBlank;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "ClientCompany")
@EntityListeners(AuditingEntityListener.class)
public class ClientCompany {
	
	public ClientCompany(long dbid, String company, String address,
			String address2, String city, String province, String country,
			String postalCode, String baddress, String baddress2, String bcity,
			String bprovince, String bcountry, String bpostalCode, String editby, Date modify) {
		super();
		this.dbid = dbid;
		this.company = company;
		this.address = address;
		this.address2 = address2;
		this.city = city;
		this.province = province;
		this.country = country;
		this.postalCode = postalCode;
		this.baddress = baddress;
		this.baddress2 = baddress2;
		this.bcity = bcity;
		this.bprovince = bprovince;
		this.bcountry = bcountry;
		this.bpostalCode = bpostalCode;
		this.editby = editby;
		this.modify = modify;
		
	}
	public ClientCompany() {
		super();
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 	
	private long dbid;
	@NotBlank
	private String company;
	
	@NotBlank
	private String address;
	@Column(nullable = true)
	private String address2;
	@NotBlank
	private String city;
	@Column(nullable = true)
	private String province;
	@NotBlank
	private String country;
	@Column(nullable = true)
	private String postalCode;
	@Column(nullable = true)
	private String baddress;
	@Column(nullable = true)
	private String baddress2;
	@Column(nullable = true)
	private String bcity;
	@Column(nullable = true)
	private String bprovince;
	@Column(nullable = true)
	private String bcountry;
	@Column(nullable = true)
	private String bpostalCode;
	@Column(nullable = true)
	private String editby;
	@Column(nullable = true)
	private Date modify;
	
	
	public long getDbid() {
		return dbid;
	}
	public void setDbid(long dbid) {
		this.dbid = dbid;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getBaddress() {
		return baddress;
	}
	public void setBaddress(String baddress) {
		this.baddress = baddress;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getBcity() {
		return bcity;
	}
	public void setBcity(String bcity) {
		this.bcity = bcity;
	}
	public String getBcountry() {
		return bcountry;
	}
	public void setBcountry(String bcountry) {
		this.bcountry = bcountry;
	}
	public String getBpostalCode() {
		return bpostalCode;
	}
	public void setBpostalCode(String bpostalCode) {
		this.bpostalCode = bpostalCode;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getBaddress2() {
		return baddress2;
	}
	public void setBaddress2(String baddress2) {
		this.baddress2 = baddress2;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getBprovince() {
		return bprovince;
	}
	public void setBprovince(String bprovince) {
		this.bprovince = bprovince;
	}
	public String getEditby() {
		return editby;
	}
	public void setEditby(String editby) {
		this.editby = editby;
	}
	public Date getModify() {
		return modify;
	}
	public void setModify(Date modify) {
		this.modify = modify;
	}
	
}
