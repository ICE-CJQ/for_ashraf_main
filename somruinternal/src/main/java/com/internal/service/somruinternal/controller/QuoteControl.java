package com.internal.service.somruinternal.controller;

import static org.springframework.data.domain.ExampleMatcher.matching;

import com.internal.service.somruinternal.model.Invoice;
import com.internal.service.somruinternal.model.Quote;
import com.internal.service.somruinternal.repository.QuoteRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/Quote")
public class QuoteControl {
    @Autowired
    QuoteRepository quoteRepo;
    
    @Autowired
    private RestTemplate restTemplate;
    
    @GetMapping("/currency")
    public Object getAllCurrencies() {
    	
    	String url = "https://api.exchangeratesapi.io/latest?base=USD";
    	return restTemplate.getForObject(url, Object.class);
    }
    
    @GetMapping("/all")
    public List<Quote> getAllQuotes() {
        return quoteRepo.findAll();
    }
    
    @GetMapping("/count")
    public int count(){
    	return quoteRepo.findAll().size();
    }
    
    @GetMapping("/latest")
    public Quote getLastest(){
    	return quoteRepo.findAll(new Sort(Sort.Direction.DESC, "dbid")).get(0);
    }
    
//    @GetMapping("/all/{page}")
//    public List<Quote> getQuotesByPage(@PathVariable(value = "page") int page) {
//        return quoteRepo.quoteByPage(new PageRequest(page,10, Sort.Direction.DESC, "quoteNum"));
//    }
    
    @GetMapping("/all/{page}")
    public List<Quote> getQuotesByPage(@PathVariable(value = "page") int page) {
    	return quoteRepo.quoteByPage(new PageRequest(page,10));
//    	return quoteRepo.quoteByPage(new PageRequest(page,10, Sort.Direction.DESC, "quoteNum"));
    }
    
    
    @GetMapping("/search/{searchKey}/{page}")
    public List<Quote> search(@PathVariable(value = "searchKey") String searchKey ,@PathVariable(value = "page") int page) {
        return quoteRepo.searchByNum(searchKey, new PageRequest(page,10));
    }
    
    @PostMapping("/add")
    public Quote createQuote(@Valid @RequestBody Quote Quote) {
        return quoteRepo.save(Quote);
    }
    
    @GetMapping("/getCompletedQuote")
    public List<Quote> getComplete() {
    	Example<Quote> example = Example.of(new Quote((long)-1, "", (double)-1, "", (long)-1, (long)-1, new Date(), new Date(), "", (long)-1,new Date(), (double)1.1, (double)1.1, (double)1.1, "", true, "", "", null), matching(). //
				withIgnorePaths("dbid", "currency", "currencyrate", "quoteNum", "clientid", "saleRepid", "createDate", "expireDate", "paymentType", "revision","revisionDate", "shippingfee", "handlefee", "tax", "taxrate", "note", "editby", "modify" ));
    	return quoteRepo.findAll(example);
    }
    
    @GetMapping("/get/{id}")
    public ResponseEntity<Quote> getQuoteById(@PathVariable(value = "id") Long QuoteId) {
    	Quote Quote = quoteRepo.findOne(QuoteId);
        if(Quote == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(Quote);
    }
    
    @PutMapping("/update/{id}")
    public ResponseEntity<Quote> updateQuote(@PathVariable(value = "id") Long  QuoteId, 
                                           @Valid @RequestBody Quote QuoteDetail) {
    	Quote quote= quoteRepo.findOne(QuoteId);
        if(quote == null) {
            return ResponseEntity.notFound().build();
        }
       
       quote.setDbid(QuoteDetail.getDbid());
       quote.setCurrency(QuoteDetail.getCurrency());
       quote.setCurrencyrate(QuoteDetail.getCurrencyrate());
       quote.setQuoteNum(QuoteDetail.getQuoteNum());
       quote.setClientid(QuoteDetail.getClientid());
       quote.setSaleRepid(QuoteDetail.getSaleRepid());
       quote.setCreateDate(QuoteDetail.getCreateDate());
       quote.setExpireDate(QuoteDetail.getExpireDate());
       quote.setPaymentType(QuoteDetail.getPaymentType());
       quote.setRevision(QuoteDetail.getRevision());
       quote.setRevisionDate(QuoteDetail.getRevisionDate());
       quote.setShippingfee(QuoteDetail.getShippingfee());
       quote.setHandlefee(QuoteDetail.getHandlefee());
       quote.setTax(QuoteDetail.getTax());
       quote.setTaxrate(QuoteDetail.getTaxrate());
       quote.setComplete(QuoteDetail.isComplete());
       quote.setNote(QuoteDetail.getNote());
       
       Quote UpdateQuote = quoteRepo.save(quote);
       return ResponseEntity.ok(UpdateQuote);
    }
    
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Quote> deleteQuote(@PathVariable(value = "id") Long QuoteId) {
    	Quote Quote = quoteRepo.findOne(QuoteId);
        if(Quote == null) {
            return ResponseEntity.notFound().build();
        }

        quoteRepo.delete(Quote);
        return ResponseEntity.ok().build();
    }
}
