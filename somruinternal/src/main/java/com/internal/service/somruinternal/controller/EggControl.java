package com.internal.service.somruinternal.controller;

import static org.springframework.data.domain.ExampleMatcher.matching;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.internal.service.somruinternal.model.Chicken;
import com.internal.service.somruinternal.model.Egg;
import com.internal.service.somruinternal.model.Injection;
import com.internal.service.somruinternal.model.Inventory;
import com.internal.service.somruinternal.repository.EggRepository;

@RestController
@RequestMapping("/egg")
public class EggControl {
	
	@Autowired
	private EggRepository eggRepo;
	
//	@CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/all")
	public List<Egg> getAllEggs() {
        return eggRepo.findAll();
    }
    
    @PostMapping("/add")
    public Egg createEgg(@Valid @RequestBody Egg egg) {
        return eggRepo.save(egg);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getIEggByChickenId/{id}")
    public List<Egg> getEggByChickenId(@PathVariable(value = "id") String chickenid) {
    	Example<Egg> example = Example.of(new Egg((long)-1, chickenid, "", "", null, null, "", null, null, "","","",null, "", "","",null), matching(). //
				withIgnorePaths("dbid", "immunogen", "eggpart", "collectiondate", "firstinjectdate", "daysafterimmune", "firstlayeggdate", "lastlayeggdate", "amount", "location", "sublocation", "frozendate", "addsucrose", "titer", "editby", "modify"));
    	return eggRepo.findAll(example);
    }
    
    @GetMapping("/get/{id}")
    public ResponseEntity<Egg> getEggBydbid(@PathVariable(value = "id") Long Eggdbid) {
    	Egg egg = eggRepo.findOne(Eggdbid);
        if( egg== null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(egg);
    }
   

    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<Egg> updateEgg(@PathVariable(value = "id") Long eggdbid,
                                           @Valid @RequestBody Egg eggDetail) {
    	Egg egg= eggRepo.findOne(eggdbid);
        if(egg == null) {
            return ResponseEntity.notFound().build();
        }
        
        egg.setDbid(eggDetail.getDbid());
        egg.setChickenid(eggDetail.getChickenid());
        egg.setImmunogen(eggDetail.getImmunogen());
        egg.setEggpart(eggDetail.getEggpart());
        egg.setCollectiondate(eggDetail.getCollectiondate());
        egg.setFirstinjectdate(eggDetail.getFirstinjectdate());
        egg.setDaysafterimmune(eggDetail.getDaysafterimmune());
        egg.setFirstlayeggdate(eggDetail.getFirstlayeggdate()); 
        egg.setLastlayeggdate(eggDetail.getLastlayeggdate()); 
        egg.setAmount(eggDetail.getAmount());   
        egg.setLocation(eggDetail.getLocation());
        egg.setSublocation(eggDetail.getSublocation());
        egg.setFrozendate(eggDetail.getFrozendate());
        egg.setAddsucrose(eggDetail.getAddsucrose());
        egg.setTiter(eggDetail.getTiter());
        egg.setEditby(eggDetail.getEditby());
        egg.setModify(eggDetail.getModify());

        Egg UpdateEgg = eggRepo.save(egg);
        return ResponseEntity.ok(UpdateEgg);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Egg> deleteEgg(@PathVariable(value = "id") Long eggdbid) {
    	Egg egg = eggRepo.findOne(eggdbid);
        if(egg == null) {
            return ResponseEntity.notFound().build();
        }

        eggRepo.delete(egg);
        return ResponseEntity.ok().build();
    }
}
