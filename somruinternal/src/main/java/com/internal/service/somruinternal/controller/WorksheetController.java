package com.internal.service.somruinternal.controller;

import com.internal.service.somruinternal.model.Worksheet;
import com.internal.service.somruinternal.repository.WorksheetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/worksheet")
public class WorksheetController {

  @Autowired
  private WorksheetRepository worksheetRepository;

  @PostMapping("/add")
  public Worksheet addWorksheet(@Valid @RequestBody Worksheet worksheet){
    return worksheetRepository.save(worksheet);
  }

  @GetMapping("/all")
  public List<Worksheet> getAllWorksheet() {
    return worksheetRepository.findAll();
  }


  @GetMapping("/get/{worksheetID}")
  public ResponseEntity<Worksheet> getWorksheetById(@PathVariable(value = "worksheetID") Long worksheetID) {
    Worksheet worksheet = worksheetRepository.findOne(worksheetID);
    if(worksheet == null) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok().body(worksheet);
  }

}
