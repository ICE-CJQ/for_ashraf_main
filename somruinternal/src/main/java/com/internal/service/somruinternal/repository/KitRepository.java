package com.internal.service.somruinternal.repository;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.internal.service.somruinternal.model.Kit;

@Repository
public interface KitRepository  extends JpaRepository<Kit, Long>{
	@Query(value="Select DISTINCT kit From Kit kit Where kit.cat LIKE %:searchKey% OR kit.name LIKE %:searchKey% ORDER By kit.dbid DESC")
	public List<Kit> searchKit(@Param(value="searchKey") String searchKey, Pageable pageable);
	
	@Query(value="Select kit From Kit kit Where kit.cat LIKE %:searchKey% ORDER By kit.dbid DESC")
	public List<Kit> searchByCat(@Param(value="searchKey") String searchKey, Pageable pageable);
	
	@Query(value="Select kit From Kit kit Where kit.name LIKE %:searchKey% ORDER By kit.dbid DESC")
	public List<Kit> searchByName(@Param(value="searchKey") String searchKey, Pageable pageable);
	
	@Query(value="Select COUNT(DISTINCT kit) From Kit kit Where kit.cat LIKE %:searchKey% OR kit.name LIKE %:searchKey%")
	public Long searchKitCount(@Param("searchKey") String searchKey);
	
	@Query(value="Select COUNT(DISTINCT kit) From Kit kit Where kit.cat LIKE %:searchKey%")
	public Long searchByCatCount(@Param("searchKey") String searchKey);
	
	@Query(value="Select COUNT(kit) From Kit kit Where kit.name LIKE %:searchKey%")
	public Long searchByNameCount(@Param("searchKey") String searchKey);
	
	
	@Query(value="Select kit From Kit kit Where kit.cat = :cat")
	public Kit searchSingleKitByCat(@Param(value="cat") String cat);
}
