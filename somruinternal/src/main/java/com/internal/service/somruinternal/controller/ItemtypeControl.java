package com.internal.service.somruinternal.controller;

import com.internal.service.somruinternal.model.Itemtype;
import com.internal.service.somruinternal.repository.ItemtypeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

@RestController
@RequestMapping("/itemtype")
public class ItemtypeControl {
    @Autowired
    ItemtypeRepository itemtype;
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/all")
    public List<Itemtype> getAllItemtypes() {
        return itemtype.findAll();
    }
}
