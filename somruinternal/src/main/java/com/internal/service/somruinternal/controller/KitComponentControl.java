package com.internal.service.somruinternal.controller;

import static org.springframework.data.domain.ExampleMatcher.matching;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;

import static org.springframework.data.domain.ExampleMatcher.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.internal.service.somruinternal.model.Ingredient;
import com.internal.service.somruinternal.model.Kit;
import com.internal.service.somruinternal.model.KitComponent;
import com.internal.service.somruinternal.repository.KitComponentRepository;

@RestController
@RequestMapping("/KitComponent")
public class KitComponentControl {
	
	@Autowired
	private KitComponentRepository kitComponentRepo;
	
//	@CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/all")
	public List<KitComponent> getAllKitLinks() {
        return kitComponentRepo.findAll();
    }

    @PostMapping("/add")
    public KitComponent createKitLink(@Valid @RequestBody KitComponent kitLink) {
        return kitComponentRepo.save(kitLink);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getKitLinkByKitId/{id}")
    public List<KitComponent> getKitLinkByKitId(@PathVariable(value = "id") Long kitid) {
    	Example<KitComponent> example = Example.of(new KitComponent((long)-1, kitid, " ", "", " ", " ", "", "", " ", "", "", "", "", " ", " ", "", null), matching(). //
				withIgnorePaths("dbid",  "component", "componentid", "reagent", "recipename", "recipeid",  "amount", "unit", "iskit", "partnumber", "packaging", "vialid","vialsize","vialdescription", "editby", "modify"));
    	return kitComponentRepo.findAll(example);
    }
    
    @GetMapping("/searchKitComponentsByKitid/{id}")
	public List<KitComponent> searchKitComponentsByKitid(@PathVariable(value = "id") Long id) {
        return kitComponentRepo.searchKitComponentsByKitid(id);
    }
    
    
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<KitComponent> updateKitLink(@PathVariable(value = "id") Long  kitLinkid, 
                                           @Valid @RequestBody KitComponent kitLinkDetail) {
    	KitComponent kitLink= kitComponentRepo.findOne(kitLinkid);
        if(kitLink == null) {
            return ResponseEntity.notFound().build();
        }
        
        kitLink.setDbid(kitLinkDetail.getDbid());
        kitLink.setKitid(kitLinkDetail.getKitid());
        kitLink.setComponent(kitLinkDetail.getComponent());
        //kitLink.setComponentType(kitLinkDetail.getComponentType());   
        kitLink.setComponentid(kitLinkDetail.getComponentid());   
        kitLink.setReagent(kitLinkDetail.getReagent());
        kitLink.setRecipename(kitLinkDetail.getRecipename());
        kitLink.setRecipeid(kitLinkDetail.getRecipeid());
        kitLink.setAmount(kitLinkDetail.getAmount());
        kitLink.setUnit(kitLinkDetail.getUnit());
        kitLink.setIskit(kitLinkDetail.getIskit());
        kitLink.setPartnumber(kitLinkDetail.getPartnumber());
        kitLink.setPackaging(kitLinkDetail.getPackaging());
        kitLink.setVialid(kitLinkDetail.getVialid());
        kitLink.setVialsize(kitLinkDetail.getVialsize());
        kitLink.setVialdescription(kitLinkDetail.getVialdescription());
        kitLink.setEditby(kitLinkDetail.getEditby());
        kitLink.setModify(kitLinkDetail.getModify());

        KitComponent UpdateKitLink = kitComponentRepo.save(kitLink);
        return ResponseEntity.ok(UpdateKitLink);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<KitComponent> deleteKitLink(@PathVariable(value = "id") Long kitLinkid) {
    	KitComponent kitLink = kitComponentRepo.findOne(kitLinkid);
        if(kitLink == null) {
            return ResponseEntity.notFound().build();
        }

        kitComponentRepo.delete(kitLink);
        return ResponseEntity.ok().build();
    }
}
