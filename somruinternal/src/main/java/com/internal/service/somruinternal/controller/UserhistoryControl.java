package com.internal.service.somruinternal.controller;
import static org.springframework.data.domain.ExampleMatcher.matching;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.internal.service.somruinternal.model.Inventory;
import com.internal.service.somruinternal.model.Userhistory;
import com.internal.service.somruinternal.objects.ItemWDetails;
import com.internal.service.somruinternal.repository.UserhistoryRepository;

@RestController
@RequestMapping("/userhistory")
public class UserhistoryControl {
	
	@Autowired
	private UserhistoryRepository userhistoryRepo;
	
//	@CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/all")
	public List<Userhistory> getAllUserhistories() {
        return userhistoryRepo.findAll();
    }

    @PostMapping("/add")
    public Userhistory createUserhistory(@Valid @RequestBody Userhistory userhistory) {
        return userhistoryRepo.save(userhistory);
    }
    
    @GetMapping("/count/{period}")
    public Long getPeriodCount(@PathVariable(value = "period") String period) {
    	period=period.trim();
    	if(period.contains("1 Week") ) {
    		return userhistoryRepo.loadWeekUserHistoryCount();
    	}
    	else if(period.contains("1 Month")) {
    		return userhistoryRepo.loadMonthUserHistoryCount();
    	}
    	else {
    		return userhistoryRepo.load6MonthsUserHistoryCount();
    	}
    }
    
    @GetMapping("/searchByNameCount/{name}/{period}")
    public Long getPeriodCountByName(@PathVariable(value = "period") String period, @PathVariable(value = "name") String name) {
//    	name=name.replaceAll("slash", "/");
//    	name=name.replaceAll("whitespace", " ");
//    	name=name.replaceAll("pong", "#");
//    	name=name.replaceAll("percent", "%");
//    	name=name.replaceAll("questionmark", "?");
    	period=period.trim();
    	if(period.contains("1 Week") ) {
    		return userhistoryRepo.loadWeekUserHistoryCountByName(name);
    	}
    	else if(period.contains("1 Month")) {
    		return userhistoryRepo.loadMonthUserHistoryCountByName(name);
    	}
    	else {
    		return userhistoryRepo.load6MonthsUserHistoryCountByName(name);
    	}
    }
    
    @GetMapping("/Period/{period}/{page}")
    public List<Userhistory> loadUserHistoryByPeriod(@PathVariable(value = "period") String period, @PathVariable(value = "page") int page) {   	
    	period=period.trim();
    	if(period.contains("1 Week")){
        	return userhistoryRepo.loadWeekUserHistory(new PageRequest(page, 10, Sort.Direction.DESC, "dbid"));
        }
        else if(period.contains("1 Month")){
        	return userhistoryRepo.loadMonthUserHistory(new PageRequest(page, 10, Sort.Direction.DESC, "dbid"));
        }
        else {
        	return userhistoryRepo.load6MonthsUserHistory(new PageRequest(page, 10, Sort.Direction.DESC, "dbid"));
        }
    }
    
    @GetMapping("/searchByNameWPage/{name}/{period}/{page}")
    public List<Userhistory> loadUserHistoryByPeriodByName(@PathVariable(value="name") String name ,@PathVariable(value="period")  String period, @PathVariable(value = "page") int page) {
//    	name=name.replaceAll("slash", "/");
//    	name=name.replaceAll("whitespace", " ");
//    	name=name.replaceAll("pong", "#");
//    	name=name.replaceAll("percent", "%");
//    	name=name.replaceAll("questionmark", "?");
    	period=period.trim();
    	if(period.contains("1 Week")){
        	return userhistoryRepo.loadWeekUserHistoryByName(name, new PageRequest(page, 10, Sort.Direction.DESC, "dbid"));
        }
        else if(period.contains("1 Month")){
        	return userhistoryRepo.loadMonthUserHistoryByName(name, new PageRequest(page, 10, Sort.Direction.DESC, "dbid"));
        }
        else {
        	return userhistoryRepo.load6MonthsUserHistoryByName(name, new PageRequest(page, 10, Sort.Direction.DESC, "dbid"));
        }
    	
    }
    
    
    @PutMapping("/update/{id}")
    public ResponseEntity<Userhistory> updateInjection(@PathVariable(value = "id") Long  userhistorydbid, 
                                           @Valid @RequestBody Userhistory userhistoryDetail) {
    	Userhistory userhistory= userhistoryRepo.findOne(userhistorydbid);
        if(userhistory == null) {
            return ResponseEntity.notFound().build();
        }
        
        userhistory.setDbid(userhistoryDetail.getDbid());
        userhistory.setTime(userhistoryDetail.getTime());
        userhistory.setUsername(userhistoryDetail.getUsername());
        userhistory.setComponent(userhistoryDetail.getComponent()); 
        userhistory.setAction(userhistoryDetail.getAction());  
        userhistory.setDescription(userhistoryDetail.getDescription()); 

        Userhistory UpdateUserhistory = userhistoryRepo.save(userhistory);
        return ResponseEntity.ok(UpdateUserhistory);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Userhistory> deleteUserhistory(@PathVariable(value = "id") Long userhistoryid) {
    	Userhistory userhistory = userhistoryRepo.findOne(userhistoryid);
        if(userhistory == null) {
            return ResponseEntity.notFound().build();
        }

        userhistoryRepo.delete(userhistory);
        return ResponseEntity.ok().build();
    }
}