package com.internal.service.somruinternal.controller;

import com.internal.service.somruinternal.model.Concentrationunit;
import com.internal.service.somruinternal.repository.ConcentrationunitRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

@RestController
@RequestMapping("/concentrationunit")
public class ConcentrationunitControl {
    @Autowired
    ConcentrationunitRepository concentrationunitRepo;
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/all")
    public List<Concentrationunit> getAllConcentrationunits() {
        return concentrationunitRepo.findAll();
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/add")
    public Concentrationunit createConcentrationunit(@Valid @RequestBody Concentrationunit concentrationunit) {
    	return concentrationunitRepo.save(concentrationunit);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/get/{id}")
    public ResponseEntity<Concentrationunit> getConcentrationunitById(@PathVariable(value = "id") Long concentrationunitId) {
    	Concentrationunit concentrationunit = concentrationunitRepo.findOne(concentrationunitId);
        if(concentrationunit == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(concentrationunit);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/update/{id}")
    public ResponseEntity<Concentrationunit> updateConcentrationunit(@PathVariable(value = "id") Long  concentrationunitId, 
                                           @Valid @RequestBody Concentrationunit concentrationunitDetail) {
    	Concentrationunit concentrationunit= concentrationunitRepo.findOne(concentrationunitId);
        if(concentrationunit == null) {
            return ResponseEntity.notFound().build();
        }
        
        concentrationunit.setDbid(concentrationunitDetail.getDbid());
        concentrationunit.setConcentrationunit(concentrationunitDetail.getConcentrationunit());

        Concentrationunit UpdateEquip = concentrationunitRepo.save(concentrationunit);
        return ResponseEntity.ok(UpdateEquip);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Concentrationunit> deleteLocation(@PathVariable(value = "id") Long concentrationunitId) {
    	Concentrationunit concentrationunit = concentrationunitRepo.findOne(concentrationunitId);
        if(concentrationunit == null) {
            return ResponseEntity.notFound().build();
        }

        concentrationunitRepo.delete(concentrationunit);
        return ResponseEntity.ok().build();
    }
}
