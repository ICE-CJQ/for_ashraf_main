import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TemplateMainComponent } from "./page/template-main/template-main.component";
import { TemplateOneComponent } from "./page/template-one/template-one.component";
import { TemplateTwoComponent } from "./page/template-two/template-two.component";
import { TemplateThreeComponent } from "./page/template-three/template-three.component";
import { TemplateFourComponent } from "./page/template-four/template-four.component";
import { TemplateListComponent } from "./page/template-list/template-list.component";
import { TemplateReviewListComponent } from "./page/template-review-list/template-review-list.component";
import { TemplateOneEditComponent } from "./page/template-one-edit/template-one-edit.component";

const routes: Routes = [
  {
    path: "template-main",
    pathMatch: "full",
    component: TemplateMainComponent
  },
  {
    path: "template-main/template-review-list",
    pathMatch: "full",
    component: TemplateReviewListComponent
  },
  {
    path: "template-main/template-review-list/template-one-edit/:worksheetID",
    pathMatch: "full",
    component: TemplateOneEditComponent
  },
  {
    path: "template-main/template-list",
    pathMatch: "full",
    component: TemplateListComponent
  },
  {
    path: "template-main/template-1",
    pathMatch: "full",
    component: TemplateOneComponent
  },
  //   {
  //     path: "template-main/template-2",
  //     pathMatch: "full",
  //     component: TemplateTwoComponent
  //   },
  {
    path: "template-main/template-3",
    pathMatch: "full",
    component: TemplateThreeComponent
  },
  {
    path: "template-main/template-4",
    pathMatch: "full",
    component: TemplateFourComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
export const routingTemplates = [
  TemplateMainComponent,
  TemplateOneComponent,
  TemplateTwoComponent,
  TemplateThreeComponent,
  TemplateFourComponent,
  TemplateListComponent,
  TemplateReviewListComponent,
  TemplateOneEditComponent
];
