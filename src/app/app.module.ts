import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { routerConfig } from "./router.config";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, FormBuilder, ReactiveFormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { RouterModule } from "@angular/router";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { MainComponent } from "./main/main.component";
import { DashboardComponent } from "./page/dashboard/dashboard.component";
import { InventoryComponent } from "./page/inventory/inventory.component";
import { KitComponent } from "./page/kit/kit.component";
import { OrderShipmentComponent } from "./page/order-shipment/order-shipment.component";
import { MemberComponent } from "./page/member/member.component";
import { AdminComponent } from "./page/admin/admin.component";
import { EquipmentComponent } from "./page/equipment/equipment.component";
import { ItemDisplayComponent } from "./display-forms/item-display/item-display.component";
import { EquipsDisplayComponent } from "./display-forms/equips-display/equips-display.component";
import { KitDisplayComponent } from "./display-forms/kit-display/kit-display.component";
import { RecipeDisplayComponent } from "./display-forms/recipe-display/recipe-display.component";
import { SopComponent } from "./page/sop/sop.component";
import { EgginventoryComponent } from "./page/egginventory/egginventory.component";
import { OrderitemComponent } from "./page/orderitem/orderitem.component";
import { OrderitemDisplayComponent } from "./display-forms/orderitem-display/orderitem-display.component";
import { ClientComponent } from "./page/client/client.component";
import { SaleComponent } from "./page/sale/sale.component";
import { ClientDisplayComponent } from "./display-forms/client-display/client-display.component";
import { QuoteDisplayComponent } from "./display-forms/quote-display/quote-display.component";
import { MyDatePickerModule } from "mydatepicker";
import { InvoiceDisplayComponent } from "./display-forms/invoice-display/invoice-display.component";
import { AuthGuard } from "./shared/services/AuthGuard";
import { AuthenticationService } from "./shared/services/Authenticate.Service";
import { CatalogComponent } from "./master-catalog/catalog/catalog.component";
import { CatalogSettingComponent } from "./master-catalog/catalog-setting/catalog-setting.component";
import { CatalogSearchComponent } from "./master-catalog/catalog-search/catalog-search.component";
import { CheckOutComponent } from "./components/check-out/check-out.component";
import { ReceiveitemDisplayComponent } from "./display-forms/receiveitem-display/receiveitem-display.component";
import { ConfigComponent } from "./page/config/config.component";
import { ChickenDisplayComponent } from "./display-forms/chicken-display/chicken-display.component";
import { EggDisplayComponent } from "./display-forms/egg-display/egg-display.component";
import { ClickOutsideModule } from "ng-click-outside";
import { BuildkitDisplayComponent } from "./display-forms/buildkit-display/buildkit-display.component";
import { ProductDisplayComponent } from "./display-forms/product-display/product-display.component";
import { ProductComponent } from "./page/product/product.component";
import { ChartsModule } from "ng2-charts";
import { HttpClientModule } from "@angular/common/http";
import { UserhistoryComponent } from "./page/userhistory/userhistory.component";
import { NgxBarcodeModule } from "ngx-barcode";
import { NgxPrintModule } from "ngx-print";
import { ReconstitutionComponent } from "./components/reconstitution/reconstitution.component";
import { PdfViewerModule } from "ng2-pdf-viewer";
import { InventoryComponent as DashInventoryComponent } from "./page/dashboard/dashboard-subcomponents/inventory/inventory.component";
// tslint:disable-next-line:max-line-length
import { ImmunizationComponent as DashImmunizationComponent } from "./page/dashboard/dashboard-subcomponents/immunization/immunization.component";
import { ResetpasswordComponent } from "./resetpassword/resetpassword.component";
import { AppRoutingModule, routingTemplates } from "./template-routing.module";
import { TimerComponent } from "./page/timer/timer.component";
import { MaterialModule } from "./material/material.module";
import { CounterComponent } from "./page/counter/counter.component";
import { AutoCompleteService } from "./shared/services/auto-complete.service";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    DashboardComponent,
    InventoryComponent,
    KitComponent,
    OrderShipmentComponent,
    MemberComponent,
    AdminComponent,
    EquipmentComponent,
    ItemDisplayComponent,
    EquipsDisplayComponent,
    KitDisplayComponent,
    RecipeDisplayComponent,
    SopComponent,
    EgginventoryComponent,
    OrderitemComponent,
    OrderitemDisplayComponent,
    ClientComponent,
    SaleComponent,
    ClientDisplayComponent,
    QuoteDisplayComponent,
    InvoiceDisplayComponent,
    CatalogComponent,
    CatalogSettingComponent,
    CatalogSearchComponent,
    CheckOutComponent,
    ReceiveitemDisplayComponent,
    ConfigComponent,
    ChickenDisplayComponent,
    EggDisplayComponent,
    BuildkitDisplayComponent,
    ProductDisplayComponent,
    ProductComponent,
    UserhistoryComponent,
    ReconstitutionComponent,
    DashInventoryComponent,
    DashImmunizationComponent,
    ResetpasswordComponent,
    routingTemplates,
    TimerComponent,
    CounterComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routerConfig),
    NgbModule.forRoot(),
    MyDatePickerModule,
    ClickOutsideModule,
    ChartsModule,
    HttpClientModule,
    NgxBarcodeModule,
    NgxPrintModule,
    PdfViewerModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MaterialModule
  ],
  providers: [
    FormBuilder,
    AuthGuard,
    AuthenticationService,
    AutoCompleteService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
