import { Component, Directive, HostListener } from '@angular/core';
import { ResponsiveService } from 'app/shared/services/responsive.service';
import { emailDetail } from 'app/shared/objects/OrderItem';
import { OrderitemService } from 'app/shared/services/Orderitem.Service';
import { interval } from 'rxjs';
import { SettingService } from 'app/shared/services/Setting.Service';


@Directive({selector: '[windowevent]'})
export class windowevent {
  // not much going on here as well...
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [ResponsiveService, OrderitemService, SettingService],
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // public static URL:string = 'http://localhost:8080';
  //public static URL:string = 'http://192.168.0.2:8080';
  //public static URL:string = 'http://192.168.0.31:8080';

  constructor(private responsiveService:ResponsiveService, private orderitemService: OrderitemService, 
    private setting: SettingService){
    // The method below does not seem to do anything
  
    window.onbeforeunload = function(e) {
      if(localStorage.getItem('isR') == 'n'){
        sessionStorage.clear();
        localStorage.clear();
      }
    };

  }

  ngOnInit(){
    this.responsiveService.getTabletStatus().subscribe(isTablet =>{
      // if(isTablet){
      //   console.log('Tablet device detected')
      // }
      // else{
      //   console.log('Tablet device did not detect')
      // }
    });
    this.checkReceiveTime();
    this.onResize();    
  }

  onResize(){
    this.responsiveService.checkWidth();
  }

  checkReceiveTime(){
    //excute every 1 day
    interval(86400000).subscribe(val => {
      this.setting.getSettingByPageAndType('orderItem','checktime').subscribe(checktime=>{
        let today = new Date();
        if(today.getTime() >= (new Date (checktime.value).getTime()- 1000*60*60*24 )){
          checktime.value = new Date().toString(); 
          this.setting.updateSetting(checktime);
          //get all ordered items
          this.orderitemService.loadAllByStatus('Ordered').subscribe(allOrderitems=>{
            let noEtaemailContent = '';
            let notArriveemailContent='';
            allOrderitems.forEach(orderitem=>{
              if(orderitem.eta == undefined){
                noEtaemailContent=noEtaemailContent+orderitem.name+', '+orderitem.cat+', requested by '+orderitem.requestperson+' on '+new Date(orderitem.requesttime).toDateString()+'\n'
              }
              if(orderitem.eta != undefined && today > orderitem.eta && today > orderitem.emailsendtime){
                notArriveemailContent=notArriveemailContent+orderitem.name+', '+orderitem.cat+', requested by '+orderitem.requestperson+' on '+new Date(orderitem.requesttime).toDateString()+', ETA is: '+new Date(orderitem.eta).toDateString()+'.'+'\n'
              }
            });
            if(noEtaemailContent.trim()!=''){
              noEtaemailContent=noEtaemailContent+'have no eta.'
              let emaildetail = new emailDetail('no eta',['cfei@upei.ca'], 'Order Items no eta notification', noEtaemailContent);
              this.orderitemService.sendEmail(emaildetail)
              .catch(error=>{
                console.log(error);
              })
            }

            if(notArriveemailContent.trim()!=''){
              notArriveemailContent=notArriveemailContent+'are not arrive.'
              let emaildetail = new emailDetail('not arrive',['cfei@upei.ca'], 'Order Item not arrived notification', notArriveemailContent);
              this.orderitemService.sendEmail(emaildetail)
              .catch(error=>{
                console.log(error);
              })
            }
          });





        }
      })
    });
  }








}
