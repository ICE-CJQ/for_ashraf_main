import { Component, OnInit, Input } from '@angular/core';
import { SettingService } from '../../shared/services/Setting.Service';
import { ConfigValue, Setting } from '../../shared/objects/Setting';

@Component({
  providers: [SettingService],
  selector: 'app-catalog-setting',
  templateUrl: './catalog-setting.component.html',
  styleUrls: ['./catalog-setting.component.css']
})
export class CatalogSettingComponent implements OnInit {
  @Input() enableEdit: boolean;

  catSetting: Setting[];
  type: ConfigValue[];
  cloneLevel: ConfigValue[];
  clients: ConfigValue[];
  species: ConfigValue[];
  nextSequence: number;
  constructor(private setting: SettingService) { }

  ngOnInit() {
    this.setting.getSettingByPage('catalog').subscribe(config => {
      let nextCat = config.find(x => x.type == 'CatPointer').value;
      if(nextCat == undefined) nextCat = 100+'';
      this.nextSequence = Number(nextCat);
      this.type = ConfigValue.toConfigArray(config.find(x => x.type == 'catType').value);
      if (this.type == []) this.type = Setting.getKitType()
      this.cloneLevel = ConfigValue.toConfigArray(config.find(x => x.type == 'cloneType').value);
      if (this.cloneLevel == []) this.cloneLevel = Setting.getClone()
      this.clients = ConfigValue.toConfigArray(config.find(x => x.type == 'focusClient').value);
      if (this.clients == []) this.clients = Setting.getClientSpec()
      this.species = ConfigValue.toConfigArray(config.find(x => x.type == 'species').value);
      if (this.species == []) this.species = Setting.getSpecies()
    });
  }

  newConfigVal(valId: number, valName: string, valType: string) {
    if (isNaN(valId)) return;
    if (valName == undefined || valName.trim() == '') return;
    if (valType == undefined) return;

    let newVal = new ConfigValue(valId, valName);
    switch (valType) {
      case 'catType':
        if (this.hasDuplicate(newVal, this.type)) break;
        this.type.push(newVal)
        break;
      case 'cloneType':
        if (this.hasDuplicate(newVal, this.cloneLevel)) break;
        this.cloneLevel.push(newVal)
        break;
      case 'focusClient':
        if (this.hasDuplicate(newVal, this.clients)) break;
        this.clients.push(newVal)
        break;
      case 'species':
        if (this.hasDuplicate(newVal, this.species)) break;
        this.species.push(newVal)
        break;
    }
  }

  hasDuplicate(val: ConfigValue, list: ConfigValue[]): boolean {
    if (list == undefined || list.length < 1) return false;
    if (list.find(x => x.id == val.id) !== undefined) return true;
    if (list.find(x => x.name == val.name) !== undefined) return true;

    return false;
  }

  isOutOfBound(list: ConfigValue[], index: number): boolean {
    if (list == undefined || list.length < 1) return true;
    if (index < 0 || index >= list.length) return true;

    return false;
  }

  assignId(val: any, index: number, configType: string) {
    if (isNaN(val)) return;

    switch (configType) {
      case 'catType':
        if (!this.isOutOfBound(this.type, index)) break;
        this.type[index].id = val;
        break;
      case 'cloneType':
        if (!this.isOutOfBound(this.cloneLevel, index)) break;
        this.cloneLevel[index].id = val;
        break;
      case 'focusClient':
        if (!this.isOutOfBound(this.clients, index)) break;
        this.clients[index].id = val;
        break;
      case 'species':
        if (!this.isOutOfBound(this.species, index)) break;
        this.species[index].id = val;
        break;
    }
  }

  assignName(val: any, index: number, configType: string) {
    if (val == undefined || val.trim() == '') return;

    switch (configType) {
      case 'catType':
        if (!this.isOutOfBound(this.type, index)) break;
        this.type[index].name = val;
        break;
      case 'cloneType':
        if (!this.isOutOfBound(this.cloneLevel, index)) break;
        this.cloneLevel[index].name = val;
        break;
      case 'focusClient':
        if (!this.isOutOfBound(this.clients, index)) break;
        this.clients[index].name = val;
        break;
      case 'species':
        if (!this.isOutOfBound(this.species, index)) break;
        this.species[index].name = val;
        break;
    }
  }
}
