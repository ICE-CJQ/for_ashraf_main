import { Component, OnInit, Output, EventEmitter } from '@angular/core';
// import { DrugListService } from '../../shared/services/Drug.Catalog';
import { SettingService } from '../../shared/services/Setting.Service';
import { KitService } from '../../shared/services/Kit.Service';
import { InventoryService } from '../../shared/services/Inventory.Service';
import { item } from '../../shared/objects/item';
import { Kit } from '../../shared/objects/Kit';
import { Product } from '../../shared/objects/Product';
import { ProductService } from '../../shared/services/product.service';
import { ISubscription } from "rxjs/Subscription";
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  providers: [SettingService, KitService, InventoryService, ProductService],
  selector: 'app-catalog-search',
  templateUrl: './catalog-search.component.html',
  styleUrls: ['./catalog-search.component.css']
})
export class CatalogSearchComponent implements OnInit {
  @Output() viewkit = new EventEmitter();
  @Output() viewproduct = new EventEmitter();

  loadKitSubscription:ISubscription;
  loadInventorySubscription:ISubscription;
  loadProductSubscription:ISubscription;

  kitList: Kit[] = [];
  invent: item[] = [];
  allproducts: Product[]=[];
  searchResult: { id: number, type: string, name: string, cat: string, size: string, price: string }[];
  searchForm: FormGroup;
  searchKey:string;
  noInput:boolean=false;

  constructor(private setting: SettingService, private fb: FormBuilder, 
    private kitservice: KitService, private inventservice: InventoryService, private productservice: ProductService) {

    this.loadKitSubscription=this.kitservice.loadKit().subscribe(list => {
      this.kitList = list;
    })

    this.loadProductSubscription=this.productservice.loadProduct().subscribe(products =>{
      this.allproducts = products;
    })
  }

  ngOnInit() {
    this.searchForm=this.createSearchForm();
    this.noInput=false;
  }

  ngOnDestroy() {
    if(this.loadKitSubscription){this.loadKitSubscription.unsubscribe()};
    if(this.loadProductSubscription){this.loadProductSubscription.unsubscribe()};
  }

  createSearchForm(): FormGroup {
    return this.fb.group(
      {
        search: [ null, Validators.compose([Validators.required])]
     });
  }

  search() {
    if (this.searchForm.controls['search'].value == undefined || this.searchForm.controls['search'].value.trim() == '') { this.noInput=true;return; }
    let key = this.searchForm.controls['search'].value.trim();
    this.noInput=false;
    this.searchResult = [];
    this.kitList.forEach(k => {
      if (k.cat !== undefined && k.cat.toLocaleLowerCase().includes(key.toLocaleLowerCase())) {
        this.searchResult.push({ id: k.dbid, type: 'Kit', name: k.name, cat: k.cat, size: k.size, price: k.price + '' })
      }
      if (k.name !== undefined && k.name.toLocaleLowerCase().includes(key.toLocaleLowerCase())) {
        this.searchResult.push({ id: k.dbid, type: 'Kit', name: k.name, cat: k.cat, size: k.size, price: k.price + '' })
      }
    });

    this.allproducts.forEach(i => {
      if (i.cat !== undefined && i.cat.toLocaleLowerCase().includes(key.toLocaleLowerCase())) {
        this.searchResult.push({ id: i.dbid, type: 'Product', name: i.name, cat: i.cat, size: i.unitsize + '' + i.unit, price:i.unitprice+'' })
      }
      if (i.name !== undefined && i.name.toLocaleLowerCase().includes(key.toLocaleLowerCase())) {
        this.searchResult.push({ id: i.dbid, type: 'Product', name: i.name, cat: i.cat, size: i.unitsize + '' + i.unit, price:i.unitprice+'' })
      }
    });

  }

  getAll() {
    this.searchResult = [];
    this.kitList.forEach(k => {
      if (this.searchResult.find(x => x.cat == k.cat) == undefined) {
        this.searchResult.push({ id: k.dbid, type: 'Kit', name: k.name, cat: k.cat, size: k.size, price: k.price + '' })
      }
    });

    this.allproducts.forEach(i => {
      if (this.searchResult.find(x => x.cat == i.cat) == undefined) {
        this.searchResult.push({ id: i.dbid, type: 'Product', name: i.name, cat: i.cat, size: i.unitsize + '' + i.unit, price:i.unitprice+'' });
      }
    });

  }

  setViewKit(id: number) {
    if (id == undefined) return;
    let k = this.kitList.find(x => x.dbid == id);
    if (k == undefined) return;
    this.viewkit.emit(k);
  }

  // setView(id: number) {
  //   if (id == undefined) return;
  //   let i = this.invent.find(x => x.dbid == id);
  //   if (i == undefined) return;
  //   this.viewitem.emit(i);
  // }

  setViewProduct(id: number) {
    if (id == undefined) return;
    let i = this.allproducts.find(x => x.dbid == id);
    if (i == undefined) return;
    this.viewproduct.emit(i);
  }


}
