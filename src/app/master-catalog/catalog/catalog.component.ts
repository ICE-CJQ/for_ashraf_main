import { Component, OnInit } from '@angular/core';
// import { DrugListService } from '../../shared/services/Drug.Catalog';
// import { DrugList } from '../../shared/objects/DrugList';
import { SettingService } from '../../shared/services/Setting.Service';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { KitService } from '../../shared/services/Kit.Service';
import { ProductService } from '../../shared/services/product.service';
import { item } from '../../shared/objects/item';
import { Kit } from '../../shared/objects/Kit';
import { User } from 'app/shared/objects/User';
import { AuthenticationService } from '../../shared/services/Authenticate.Service'
import { Product } from '../../shared/objects/Product';
@Component({
  providers: [SettingService, KitService, ProductService],
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {

  currentuser: User;
  currentusername: string = '';
  currentuserrole: string = '';
  //type: ConfigValue[] //= [{ id: 6, name: 'ADA' }, { id: 7, name: 'PK' }, { id: 0, name: 'Antibody' }, { id: 3, name: 'Cell Line' }, { id: 4, name: 'Protein' }, { id: 5, name: 'Misc' }, { id: 8, name: 'Method Transfer' }, { id: 9, name: 'Charecterization kit' }, { id: 10, name: 'Biomaker' }]
  clients: ConfigValue[]// = [{ id: 5, name: 'Intas' }];
  cattype: ConfigValue[]=[];
  species: ConfigValue[]// = [{ id: 0, name: 'Human' }, { id: 1, name: 'Mouse' }, { id: 2, name: 'Rat' }, { id: 3, name: 'Monkey' }, { id: 4, name: 'Chicken' }, { id: 5, name: 'Donkey' }, { id: 6, name: 'Goat' }, { id: 7, name: 'Rabbit' }];
  selectClientName:string='';
  selectTypeName:string='';
  selectSpeciesName:string='';
  isNewDrug: boolean = true;
  isKit: boolean = true;
  isClientSpecific:string;
  hasSpecies:string;

  catalogchangevalid=true;

  catalog: string;
  editedcatalog:string;
  nextProductNum: Setting;
  nextKitNum: Setting;
  addMessage: string[]=[];
  kitList: Kit[];
  allproducts: Product[];
  existObject: boolean = false;
  message: string;

  constructor(private setting: SettingService,
    private kitservice: KitService, private authenticationservice: AuthenticationService, private productservice: ProductService) { }

  ngOnInit() {
    let getcurrentuser = this.authenticationservice.getCurrentUser()
    if (getcurrentuser !== undefined) {
      getcurrentuser.then(_ => {
        this.currentuser = User.fromJson(_);
        this.currentusername = this.currentuser.name;
        this.currentuserrole = this.currentuser.role;
      });
    }

    this.kitservice.loadKit().subscribe(list => {
      this.kitList = list;
      // console.log(this.kitList[0].cat.split('-'))
      // let catlist:number[]=[];
      // this.kitList.forEach(kit=>{
      //   let cats=kit.cat.split('-');
      //   catlist.push(Number(cats[2].substring(1,3)));
      //});

      // catlist.sort((a, b) => b - a);

      // console.log(catlist[0])
    })

    this.productservice.loadProduct().subscribe(products => {
      this.allproducts = products;
    });

    this.setting.getSettingByPage('catalog').subscribe(config => {
      this.nextProductNum =config.find(x => x.type == 'productPointer') ;


      this.nextKitNum =config.find(x => x.type == 'kitPointer') ;


      // this.type = ConfigValue.toConfigArray(config.find(x => x.type == 'catType').value);

      this.clients = ConfigValue.toConfigArray(config.find(x => x.type == 'focusClient').value);
      if (this.clients == []) this.clients = Setting.getClientSpec()
      this.cattype = ConfigValue.toConfigArray(config.find(x => x.type == 'catType').value);
      if (this.species == []) this.species = Setting.getSpecies()
      this.species = ConfigValue.toConfigArray(config.find(x => x.type == 'species').value);
      if (this.species == []) this.species = Setting.getSpecies()
      // this.cloneLevel = ConfigValue.toConfigArray(config.find(x => x.type == 'cloneType').value);
      // if (this.cloneLevel == []) this.cloneLevel = Setting.getClone()

    });

  }

  clear() {
    this.selectClientName='';
    this.selectSpeciesName='';
    this.selectTypeName='';
    this.catalog = '';
    this.isKit = true;
    this.hasSpecies=undefined;
    this.isClientSpecific=undefined;
    this.existObject = false;
    this.catalogchangevalid=true;
  }

  isValid(): boolean {
    if (this.isKit==undefined || this.isClientSpecific==undefined || this.hasSpecies==undefined) {
      return false;
    }

    if (this.selectTypeName == undefined || this.selectTypeName.trim() == '') {
      return false;
    }

    if (this.hasSpecies=='Yes' && (this.selectSpeciesName == undefined || this.selectSpeciesName.trim() == '')  ) {
      return false;
    }

    if (this.isClientSpecific=='Yes' && (this.selectClientName == undefined || this.selectClientName.trim() == '')  ) {
      return false;
    }

    if(this.catalogchangevalid==false){
      return false;
    }

    return true;
  }

  generateCatalog() {
    if (!this.isValid()) return;

    this.catalog = 'SB-0';

    if (this.isClientSpecific=='Yes' && this.selectClientName.trim()!='') {
      let index = this.clients.findIndex(x=>x.name.trim()==this.selectClientName.trim());
      this.catalog += '' + this.clients[index].id;
    }

    let typeindex = this.cattype.findIndex(x=>x.name.trim()==this.selectTypeName.trim());
    this.catalog += '' + this.cattype[typeindex].id;

    this.catalog += '-';

    if(this.hasSpecies=='Yes' && this.selectSpeciesName.trim()!=''){
      let speciesindex = this.species.findIndex(x=>x.name.trim()==this.selectSpeciesName.trim());
      this.catalog += '' + this.species[speciesindex].id;
    }
    else{
      this.catalog += '' + '0';
    }


    if(this.isKit){
      this.catalog += '' + this.nextKitNum.value;
      if (this.kitList.find(x => x.cat.trim() == this.catalog.trim()) !== undefined) {
      this.existObject = true;
      }
    }
    else{
      this.catalog += '' + this.nextProductNum.value;
      if (this.allproducts.find(x => x.cat.trim() == this.catalog.trim()) !== undefined) {
      this.existObject = true;
      }
    }

    this.editedcatalog=this.catalog;

  }

  addKitWithCat(name: string, price: number) {
    if (this.existObject) return;
    if (name == undefined || name.trim() == '' || isNaN(price)) {
      this.message = 'Please make sure below fields are not empty or blank!'
      return;
    }

    // if (this.kitList.find(x => x.name == name) !== undefined) {
    //   this.message = 'The item name is already exist!'
    //   return;
    // }

    let method='';

    if(this.selectTypeName.includes('ADA')){
      method='ADA';
    }
    else if(this.selectTypeName.includes('PK')){
      method='PK';
    }
    else if(this.selectTypeName.includes('Antibody')){
      method='Antibody';
    }
    else{
      method=this.selectTypeName;
    }

    let kit = new Kit(-1, this.editedcatalog, name, '', this.selectClientName ? this.selectClientName : '', '', '', method, '2 X 96', 'Qualified with innovator', price.toString(), this.currentuser.name, '', '', '', '', new Date(), null, null, 'Entered', '');
    this.kitservice.addKit(kit).then(_ => {
      this.nextKitNum.value = (Number(this.nextKitNum.value) + 1).toString();
      this.setting.updateSetting(this.nextKitNum);
      this.addMessage.push('Successly Added New Kit')
      setTimeout(_ => { this.addMessage = [] }, 7000);
    });
    this.clear();
  }

  checkcatalog(){
    if(!this.editedcatalog.startsWith(this.catalog)){
      this.catalogchangevalid=false;
    }
    else{
      this.catalogchangevalid=true;
    }
  }

  addProductWithCat(name: string) {

    if (this.existObject) return;
    if (name == undefined || name.trim() == '') {
      this.message = 'Please make sure below fields are not empty or blank!'
      return;
    }

    this.productservice.loadProduct().subscribe(products => {
      this.allproducts = products;
      // if (this.allproducts.find(x => x.name == name) !== undefined) {
      //   this.message = 'The product name is already exist!'
      //   return;
      // }


        let type='';

        if(this.selectTypeName.includes('ADA') || this.selectTypeName.includes('PK')){
          type='Associated kit component';
        }

        else{
          type=this.selectTypeName;
        }


        let newproduct=new Product(-1,name,type, '', this.editedcatalog, '', '', '', '', '', '',  '', '', '', '', '', '', '','','','',  this.currentuser.name, '', '', new Date(), null, null,'','');
        this.productservice.addProduct(newproduct).then(_ =>{
          this.addMessage.push('Successly Added a New Product!');
          // setTimeout(_ => { this.addMessage = undefined }, 7000);
          this.nextProductNum.value = (Number(this.nextProductNum.value) + 1).toString();
          this.setting.updateSetting(this.nextProductNum).then(_=>{
            setTimeout(_ => { this.addMessage = [] }, 7000);
            this.clear();
          });

        });

    })

  }


}


