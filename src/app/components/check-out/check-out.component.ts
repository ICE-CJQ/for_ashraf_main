import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { InventoryService } from '../../shared/services/Inventory.Service';
import { SettingService } from '../../shared/services/Setting.Service';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { item, itemdetail } from '../../shared/objects/item';
import { Location, Sublocation, Concentrationunit } from '../../shared/objects/SettingObjects';
import { ItemdetailService } from '../../shared/services/itemdetail.service';
import { toggleItem } from '../../page/inventory/inventory.component';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';

@Component({
  providers: [InventoryService, ItemdetailService, SettingService],
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.css']
})
export class CheckOutComponent implements OnInit {
  @Input() selectItems: toggleItem[] = [];
  @Output() update = new EventEmitter();
  @Output() cancel = new EventEmitter();

  invent: item[];
  itemdetails: itemdetail[];
  reconstitute: { m: item, d: itemdetail, o:string, e: { date: { year: number, month: number, day: number } } }[];
  //reconstitute: { d: itemdetail, e: { date: { year: number, month: number, day: number } } }[];
  checkoutAmountValid: boolean;
  showReconError: boolean;
  unit: ConfigValue[];
  volUnit: ConfigValue[];
  massUnit: ConfigValue[];
  otherUnit: ConfigValue[];
  conunit: Concentrationunit[];
  locations: Location[] = [];
  subLocList: Sublocation[] = [];
  sublocations: Sublocation[] = [];
  useInUse: boolean[];
  dateoption: IMyDpOptions = { dateFormat: 'mm/dd/yyyy' };

  constructor(private inventService: InventoryService, private settingservice: SettingService, private itemdetailservice: ItemdetailService) { }

  ngOnInit() {
    this.settingservice.getSettingByPage('general').subscribe(config => {
      this.unit = [];
      this.volUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'volUnit').value)
      this.volUnit.forEach(v => { this.unit.push(v) })
      this.massUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'massUnit').value)
      this.massUnit.forEach(m => { this.unit.push(m) })
      this.otherUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'otherUnit').value)
      this.otherUnit.forEach(o => { this.unit.push(o) })
      this.conunit = [];
      let con = ConfigValue.toConfigArray(config.find(x => x.type == 'conUnit').value)
      con.forEach(c => { this.conunit.push(new Concentrationunit(c.id, c.name)) });
      this.locations = [];
      ConfigValue.toConfigArray(config.find(x => x.type == 'location').value).forEach(l => {
        this.locations.push(new Location(l.id, l.name));
      });
      this.showsublocationoption(this.locations[0].locationname);
    });

    // this.locationservice.loadLocation().subscribe(locations => {
    //   this.locations = locations;
    // });
    this.inventService.loadInventory().subscribe(invent => {
      this.invent = invent
    });
    this.itemdetailservice.loadItemDetail().subscribe(itemdetails => {
      this.itemdetails = itemdetails.slice();
    })

  }

  ngOnChanges(change: SimpleChanges) {
    if (change.selectItems && this.selectItems !== undefined) {
      this.useInUse = [];
      this.selectItems.forEach((i, index) => { if (i !== undefined) this.useInUse[index] = undefined; });
    }
  }

  showsublocationoption(l: string) {
    this.sublocations = [];
    let target = this.locations.find(x => x.locationname == l);
    if (target == undefined) return;
    this.settingservice.getSettingByPageAndType('general', 'subLocation').subscribe(subLoc => {
      let subList = Setting.getSubLocation();
      if (subLoc !== undefined && subLoc.value !== undefined) subList = ConfigValue.toConfigArray(subLoc.value);
      subList.forEach(sl => {
        if (sl.id == target.dbid) {
          this.sublocations.push(new Sublocation(-1, sl.id.toString(), sl.name));
        }
      })
    })
    // this.locations.forEach(location => {
    //   if (location.locationname == l) {
    //     this.subLocList.forEach(sub=>{
    //       if(sub.locationid == location.dbid+''){
    //         this.sublocations.push(sub);
    //       }
    //     });
    //   }
    // });

  }

  newReconstitute(index: number) {
    if (index == undefined || index < 0 || this.selectItems[index] == undefined) return;
    let t = this.selectItems[index];//t is the itemdetail that is selected
    t.nonopencheckoutamount='1';
    this.validateCheckoutAmount(t.nonopencheckoutamount, index)
    t.userforrecon='Yes';
    
    //find the item of the itemdetail that selected
    let selecteditem = this.invent.find(x => x.dbid == t.masterId);
    //find the selected itemdetail
    let selecteditemdetail = this.itemdetails.find(x => x.dbid == t.detailId);
    let oldlotnumber= selecteditemdetail.lotnumber;

    if (selecteditemdetail == undefined || selecteditem == undefined) return;
    if (this.reconstitute == undefined) this.reconstitute = [];

    if (this.reconstitute.find(x => x.m.dbid == selecteditemdetail.dbid) == undefined) {
      let newitemdetail=itemdetail.newitemdetail(selecteditemdetail);
      newitemdetail.dbid = -1;
      newitemdetail.amount = '1';
      newitemdetail.inuse = '0';
      newitemdetail.recon = 'Yes';
      newitemdetail.receivedate = new Date();
      newitemdetail.projectnumber = '';
      newitemdetail.receiveperson = '';
      newitemdetail.reserve = 'No';
      let thirtydayslater = new Date().getTime() + (1000 * 60 * 60 * 24) * 30;
      newitemdetail.expiredate = new Date(thirtydayslater);
      let formatE = {
        date: {
          year: newitemdetail.expiredate.getFullYear(),
          month: newitemdetail.expiredate.getMonth() + 1,
          day: newitemdetail.expiredate.getDate()
        }
      };
      
      let i = this.reconstitute.push({ m: selecteditem, d: newitemdetail, o:oldlotnumber, e: formatE }) - 1;
      this.validateInput(i)
    }
  }

  //validate new reconstitute input
  validateInput(index: number) {
    if (index == undefined || index < 0 || this.reconstitute[index] == undefined) return;
    let newitem = this.reconstitute[index];
    if (newitem.d == undefined || newitem.e == undefined || newitem.m == undefined) return;
    let itemdetail = this.reconstitute[index].d;

    if (
      newitem.d.lotnumber.trim()== this.reconstitute[index].o.trim() ||
      newitem.d.unitsize == undefined || Number(newitem.d.unitsize) < 1 ||
      newitem.d.lotnumber == undefined || newitem.d.lotnumber.trim() == '' ||
      this.detailExist(newitem.d) !== undefined ||
      isNaN(Number(newitem.d.amount)) || Number(newitem.d.amount) < 1 ||
      isNaN(Number(newitem.d.concentration)) || Number(newitem.d.concentration) < 0 ||
      newitem.d.concentrationunit == undefined || newitem.d.concentrationunit.trim() == '' ||
      newitem.d.receivedate == undefined ||newitem.d.expiredate == undefined || newitem.d.receivedate >= newitem.d.expiredate) {
      this.showReconError = true;
    }
    else {
      this.showReconError = false;
    }
  }

  removeRecon(index: number) {
    if (index == undefined || index < 0 || this.reconstitute[index] == undefined) return;
    this.reconstitute.splice(index, 1);
    this.reconstitute.forEach((v, i) => { this.validateInput(i) })
    if (this.reconstitute.length == 0) this.showReconError = false;
  }

  // resetQty(index: number) {
  //   if (index == undefined || index < 0 || index >= this.selectItems.length) return;
  //   if (this.selectItems == undefined || this.selectItems[index] == undefined) return;

  //   this.useInUse[index] = !this.useInUse[index];
  //   if (this.useInUse[index]) {
  //     this.selectItems[index].remain = this.selectItems[index].itemQuan;
  //     this.selectItems[index].reduceValue = 0;
  //   }
  //   else {
  //     this.selectItems[index].inuseremain = this.selectItems[index].inuse;
  //     this.selectItems[index].inusereduce = 0;
  //   }

  //   this.amountalert = false;
  // }

  setunopen(index: number){
    if (index == undefined || index < 0 || index >= this.selectItems.length) return;
    if (this.selectItems == undefined || this.selectItems[index] == undefined) return;
    this.useInUse[index]=false;
    this.selectItems[index].inusecheckoutamount='';

  }
  setinuse(index: number){
    if (index == undefined || index < 0 || index >= this.selectItems.length) return;
    if (this.selectItems == undefined || this.selectItems[index] == undefined) return;
    this.useInUse[index]=true;
    this.selectItems[index].nonopencheckoutamount='';
  }

  updateChange() {   
    if (this.showReconError) {
      return;
    }

    if (this.reconstitute !== undefined && this.reconstitute.length > 0) {
      this.reconstitute.forEach((v, i) => { this.validateInput(i) });
    }

    this.selectItems.forEach((item, index) => {
      if (this.useInUse[index]==false) {
        if(item.userforrecon=='Yes'){
          item.itemQuan= (Number(item.itemQuan)  - Number(item.nonopencheckoutamount)).toString();
        }
        else{
          item.itemQuan= (Number(item.itemQuan)  - Number(item.nonopencheckoutamount)).toString();
          item.inuseQuan = (Number(item.inuseQuan) + Number(item.nonopencheckoutamount)).toString();
        }
      }
      if(this.useInUse[index]==true){
        item.inuseQuan = (Number(item.inuseQuan) - Number(item.inusecheckoutamount)).toString();
      }
    })
  


    this.update.emit({ selectItems: this.selectItems, recon: this.reconstitute });
  }

  cancelChange() {
    this.cancel.emit();
  }



  //check if this item detail is already exist
  detailExist(detail: itemdetail): itemdetail {
    return this.itemdetails.find(x =>
      x.lotnumber == detail.lotnumber &&
      x.receivedate == detail.receivedate &&
      x.expiredate == detail.expiredate &&
      x.location == detail.location &&
      x.sublocation == detail.sublocation &&
      x.amount == detail.amount &&
      x.inuse == detail.inuse &&
      x.concentration == detail.concentration &&
      x.concentrationunit == detail.concentrationunit && 
      x.unit == detail.unit && 
      x.unitsize == detail.unitsize &&
      x.projectnumber == detail.projectnumber &&
      x.recon == detail.recon &&
      x.receiveperson == detail.receiveperson
    );
  }

  //assign expire date to the new reconstitute itemdetail
  assignDate(index: number, date: IMyDateModel) {
    if (index == undefined || index < 0 || this.reconstitute[index] == undefined) return;
    if (date == undefined) {
      this.reconstitute[index].d.expiredate = undefined
      return;
    }
    this.showReconError = false;
    this.reconstitute[index].d.expiredate = new Date(date.formatted);
    if (this.reconstitute[index].d.expiredate <= this.reconstitute[index].d.receivedate)
      this.showReconError = true;
  }

  
  validateCheckoutAmount(checkoutAmount: string, itemlotIndex: number){
    if (itemlotIndex == undefined || itemlotIndex < 0 || itemlotIndex >= this.selectItems.length) return;
    if (this.selectItems == undefined || this.selectItems[itemlotIndex] == undefined) return;
    if(this.useInUse[itemlotIndex]==false){
      if(Number(checkoutAmount) > Number(this.selectItems[itemlotIndex].itemQuan)){
        this.selectItems[itemlotIndex].nonopencheckoutamount = '';
        this.checkoutAmountValid = false;
        return;
      }
      else{
        this.selectItems[itemlotIndex].nonopencheckoutamount = checkoutAmount;
        this.checkoutAmountValid = true;
      }
    }
    if(this.useInUse[itemlotIndex]==true){
      if(Number(checkoutAmount) > Number(this.selectItems[itemlotIndex].inuseQuan)){
        this.selectItems[itemlotIndex].inusecheckoutamount= '';
        this.checkoutAmountValid = false;
        return;
      }
      else{
        this.selectItems[itemlotIndex].inusecheckoutamount= checkoutAmount;
        this.checkoutAmountValid = true;
      }
    }

  }


}
