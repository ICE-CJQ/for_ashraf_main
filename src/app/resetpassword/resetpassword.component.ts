import { ActivatedRoute, Router, Params } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm, FormControl, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { AuthenticationService } from '../shared/services/Authenticate.Service';
import { User } from '../shared/objects/User';
import { UserhistoryService } from 'app/shared/services/userhistory.service';
import { Userhistory } from 'app/shared/objects/Userhistory';
import { ResponsiveService } from 'app/shared/services/responsive.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {

  resetPasswordForm: FormGroup;
  resetpassword: FormControl;
  confirmpassword: FormControl;
  showResetPass: boolean;
  userId: string;

  constructor(private fb: FormBuilder, private router: Router, private auth: AuthenticationService, private route: ActivatedRoute, private responsiveService:ResponsiveService) {
   }

  ngOnInit() {
    this.resetPasswordForm=this.createResetPasswordForm();

  //   this.resetpassword= new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(15), this.confirmPasswordCompare]),
  //   this.confirmpassword= new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(15), this.confirmPasswordCompare ]),
  //   this.resetPasswordForm = new FormGroup({
  //     resetpassword: this.resetpassword,
  //     confirmpassword: this.confirmpassword
  // });

  this.showResetPass = false;
    this.route.params.subscribe((param: Params) => {
      let token = param.token;
      this.auth.verifyResetPassToken(token).subscribe(result=> {
        let key = Object.keys(result)[0]
        this.userId = result[key];

        if(parseInt(this.userId)){
          this.showResetPass = true;
        }
        else{
          alert(result[key])
          this.router.navigate(['/login']);
        }
      })
    });
  }

  createResetPasswordForm(): FormGroup {
    return this.fb.group(
      {
        password: [ null, Validators.compose([
          // 1. Password Field is Required
          Validators.required,
          // 2. check whether the entered password has a number
          this.patternValidator(/\d/, { hasNumber: true }),
          // 3. check whether the entered password has upper case letter
          this.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
          // 4. check whether the entered password has a lower-case letter
          this.patternValidator(/[a-z]/, { hasSmallCase: true }),
          // 5. check whether the entered password has a special character
          this.patternValidator(/\W|_/g, { hasSpecialCharacters: true }),
         // 6. Has a minimum length of 8 characters
         Validators.maxLength(15),
          // 7. Has a minimum length of 8 characters
          Validators.minLength(8)])
        ],
        confirmPassword: [null, Validators.compose([Validators.required])]
     },
     {
        // check whether our password and confirm password match
        validator: this.passwordMatchValidator
     });
  }

  passwordMatchValidator(group: FormGroup) {
    const password: string = group.get('password').value; // get password from our password form control
    const confirmPassword: string = group.get('confirmPassword').value; // get password from our confirmPassword form control
    // compare is the password math
    if (password !== confirmPassword) {
      // if they don't match, set an error in our confirmPassword form control
      group.get('confirmPassword').setErrors({ NoPassswordMatch: true });
    }
  }

  patternValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        // if control is empty return no error
        return null;
      }
  
      // test the value of the control against the regexp supplied
      const valid = regex.test(control.value);
  
      // if true, return no error (no error), else return error passed in the second parameter
      return valid ? null : error;
    };
  }

  changePassword(){
    this.auth.updatePassword(parseInt(this.userId), this. resetPasswordForm.controls['password'].value).then(_=>{
      localStorage.setItem('user.dbid', this.userId)
      this.router.navigate(['./main']);
    })
  }

  backToLogin(){
    this.resetPasswordForm.reset();
    this.router.navigate(['/login']);
  }

  // resetPass() {
  //   let p = this.form.value.newpass;
  //   let vp = this.form.value.verifypass;
  //   if (this.errMessage !== '') { return };

  //   if (p.trim() == '' || vp.trim() == '') {
  //     this.errMessage = 'Please fill in the new password and verify password';
  //     return;
  //   }

  //   this.auth.resetPassWithCode(this.code, p).toPromise().then(_ => {
  //     this.countdown = 10;
  //     this.showResetPass = false;
  //     this.change = 'success';
  //     setTimeout(_ => {
  //       this.router.navigate(['/login']);
  //     }, 3000);
  //   }, err => {
  //     this.errMessage = err.message;
  //     setTimeout(_ => { this.errMessage = '' }, 2000)
  //   })
  // }

}




