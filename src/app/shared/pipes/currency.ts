import { Pipe, PipeTransform } from '@angular/core';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({name: 'currency'})
export class CurrencyPipe implements PipeTransform {
  transform(value: number): string {
    // let exp = parseFloat(exponent);
    let cur = ''+value;
    let formatcur = '';
    let cursplit = cur.split('.');
    if(cursplit.length == 1){
        formatcur = cursplit[0]+'.00';
    }
    else if (cursplit.length == 2){
        let cent = Number('0.'+cursplit[1]);
        cent = (Math.round(cent*100))/100;
        formatcur = (cursplit[0]+ cent)+'';
    }

    return formatcur;
  }
}