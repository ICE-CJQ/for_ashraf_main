import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Equipment } from '../objects/Equipment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Itemarchive, Itemdetailarchive } from 'app/shared/objects/archives';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';
@Injectable()
export class ArchiveService {
  itemarchiveurl: string = UrlService.URL + `/itemarchive/`;
  itemdetailarchiveurl: string = UrlService.URL + `/itemdetailarchive/`;
  constructor(private http: HttpClient) { }

  public getItemarchiveByItemdbid(id:number):  Observable<Itemarchive[]>{
    return this.http.get<Itemarchive[]>(this.itemarchiveurl + `getItemarchiveByItemdbid/${id}`);
}

public getItemdetailarchiveByItemdetaildbid(id:number):  Observable<Itemdetailarchive[]>{
  return this.http.get<Itemdetailarchive[]>(this.itemdetailarchiveurl + `getItemdetailarchiveByItemdetaildbid/${id}`);
}

public addItemarchive(i: Itemarchive): Promise<Object> {
  if (i == undefined) return;
  // let  headers = new  HttpHeaders();
  // headers.append('Content-Type', 'application/json');
  let  headers = new  HttpHeaders().set('Content-Type','application/json');
  return this.http.post(this.itemarchiveurl + 'add/', JSON.stringify(i), { headers }).toPromise();
}



}
