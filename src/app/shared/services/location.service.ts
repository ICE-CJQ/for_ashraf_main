import { Injectable } from '@angular/core';
import { Location } from '../objects/Location';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';
@Injectable()
export class LocationService {
  url: string = UrlService.URL + `/location/`;
  locations: Location[];

  constructor(private http: HttpClient) { }

  public loadLocation(): Observable<Location[]> {
    return this.http.get<Location[]>(this.url + 'all/');
  }
}
