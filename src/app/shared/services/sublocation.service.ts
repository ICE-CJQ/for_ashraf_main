import { Injectable } from '@angular/core';
import { Sublocation } from '../objects/Sublocation';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';
@Injectable()
export class SublocationService {
  url: string = UrlService.URL + `/sublocation/`;
  sublocations: Sublocation[];

  constructor(private http: HttpClient) { }

  public loadSublocationBylocationid(locationid:number): Observable<Sublocation[]> {
    return this.http.get<Sublocation[]>(this.url + `getSublocationByLocationId/${locationid}`);
} 

}
