import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Equipment } from '../objects/Equipment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';
@Injectable()
export class EquipmentService {
    url: string = UrlService.URL + `/equipment/`;
    equips: Equipment[] = [];

    constructor(private http: HttpClient) {
        // console.log('hello');
        // this.addEquipment(new Equipment("SOM001", "89404-462", "Inverted Microscope", "VWR", "NA", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM002", "XS02205", "SpectraMAX 340 pc", "Molecular Devices", "SpectraMAX 340 Gemini SX", "19 Innovation Way", "Luminesence", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM003", "2213", "SpectraMAX 340 pc", "Molecular Devices", "SpectraMAX 340 pc", "19 Innovation Way", "General use reader", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM004", "9111010051", "GloMax Navagator", "Promega", "GM2000", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM005", "401940", "Comfort Vacuum", "Inegra Biosciences", "Vacusafe Comfort Vacuum", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM006", "ESS3-125-320", "CO2 regulator", "", "", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM007", "NA", "Glass Desiccator", "Pyrex", "55/38", "19 Innovation Way", "desiccant and tray inside, hose attatched", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM008", "B458248731", "pH Meter", "Toledo", "SevenCompact S210", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM009", "3003944", "hot plate/stirrer", "Corning", "C320", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM010", "NA (scratched off)", "Stirrer", "Corning", "", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM011", "E150458", "Impulse heat sealer", "U Line", "H-161", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM012", "NA", "Heating gun", "Veritemp", "VT-750C", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM013", "17130348", "Tube Rotators", "VWR", "10136-084", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM014", "17130348", "Tube Rotators", "VWR", "10136-084", "19 Innovation Way", "Drawer of paperwork bench", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM015", "NA", "5 kg kitchen scale", "Starfrit", "93016", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM016", "1119030574", "41g/210g scale", "Mettler Toledo", "", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM017", "AS20130565751", "Mini Centrifuge", "My Fuge", "C-1008-G", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM018", "AS20130768878", "Mini Centrifuge", "My Fuge", "C-1008-G", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM019", "35498", "Centrifuge", "Eppendorf", "5810 R", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM020", "24782157", "Centra-8R Centrifuge", "", "IEC", "19 Innovation Way", "U-Hal Storage", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM021", "NA", "Amplatto Machine", "", "", "", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM022", "JXT10F 02", "High Performance", "Beckman Coultier", "Beckman Coultier/J-26 XPI", "19 Innovation Way", "Waiting for repair", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM023", "BI00378", "CO2 Incubator", "Nauire", "", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM024", "0997-0156", "Oven Incubator", "VWR", "2710", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM025", "190453", "Auto Strip Washer", "Bio-Tek Instruments", "ELX 50", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM026", "186278", "96 well Washer", "Bio-Tek Instruments", "ELX 405", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM027", "1882070307390", "Orbital Shaker", "Barnstead ", "4625", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM028", "1882071005906", "Orbital Shaker", "Barnstead", "4625", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM029", "2-397552", "Vortex Genie 2", "Scientific Industries", "G-560", "19 Innovation Way", "Muditha/Emily Bench", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM030", "2-397590", "Vortex Genie 2", "Scientific Industries", "G-560", "19 Innovation Way", "Kelly/Kim Bench", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM031", "511000006", "Liquid Handler", "TECAN", "Freedom Evo 100", "19 Innovation Way", "", (new Date).toString()));
        // this.addEquipment(new Equipment("SOM032", "2-164659", "Vortex Genie 2", "Fisher Scientific", "", "19 Innovation Way", "Jessi/Ying Bench", (new Date).toString()));
    }

    public loadEquipment(): Observable<Equipment[]> {
        return this.http.get<Equipment[]>(this.url + 'all/');
    }

    public addEquipment(e: Equipment): boolean {
        if (e == undefined) return false;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        this.http.post(this.url + 'add/', JSON.stringify(e), { headers }).toPromise().then(_ => {
            return true;
        }).catch(err => {
            return false;
        });
    }

    public updateEquipment( e: Equipment): Promise<Object> {
        if (e == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.put(this.url + 'update/' + (e.dbid) + '/', JSON.stringify(e), { headers }).toPromise();
        // return true;
    }

    public deleteEquipment(e: Equipment): Promise<Object> {
        if (e == undefined) return;
        return this.http.delete(this.url + 'delete/' + (e.dbid) + '/').toPromise();
    }
}