import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Equipment } from '../objects/Equipment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Kit, Kitcomponent } from 'app/shared/objects/Kit';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';

@Injectable()
export class KitService {
    url: string = UrlService.URL + `/kit/`;

    constructor(private http: HttpClient) {
    }

    public loadKit(): Observable<Kit[]> {
        return this.http.get<Kit[]>(this.url + 'all/');
    }

    public loadKitWPage(page: number): Observable<Kit[]> {
        return this.http.get<Kit[]>(this.url + 'allByPage/'+page);
    }

    public searchSingleKitByCat(cat: string): Observable<Kit> {
        if (cat == undefined) return;
        return this.http.get<Kit>(this.url + `searchSingleKitByCat/${cat}`);
    }

    public search(searchKey:string, page:number, searchArea?: string): Observable<Kit[]> {
        searchKey = searchKey.replace(/\//g, 'slash');
        searchKey = searchKey.replace(/\s/g, 'whitespace');
        searchKey = searchKey.replace(/#/g, 'pong');
        searchKey = searchKey.replace(/%/g, 'percent');
        searchKey = searchKey.replace(/\?/g, 'questionmark');
        if (searchArea == undefined || searchArea == '' || searchArea == 'All')
            return this.http.get<Kit[]>(this.url + `searchKit/${searchKey}/${page}`);
        else if (searchArea == 'Catalog')
            return this.http.get<Kit[]>(this.url + `searchByCat/${searchKey}/${page}`);
        else if (searchArea == 'Name')
            return this.http.get<Kit[]>(this.url + `searchByName/${searchKey}/${page}`);
    }

    public getSearchCount(search: string,searchArea:string): Observable<number> {
        //encode all white space and #
        search = search.replace(/\//g, 'slash');
        search = search.replace(/\s/g, 'whitespace');
        search = search.replace(/#/g, 'pong');
        search = search.replace(/%/g, 'percent');
        search = search.replace(/\?/g, 'questionmark');
        if(searchArea == undefined || searchArea == '' || searchArea == 'All'){
            return this.http.get<number>(this.url + 'searchKitCount/' + search + '/');
        }
        if(searchArea == 'Catalog'){
            return this.http.get<number>(this.url + 'searchByCatCount/' + search + '/');
        }
        if(searchArea == 'Name'){
            return this.http.get<number>(this.url + 'searchByNameCount/' + search + '/');
        }
    }

    public count(): Observable<number> {
        return this.http.get<number>(this.url + 'count/');
    }

    public addKit(k: Kit): Promise<Object> {
        if (k == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.post(this.url + 'add/', JSON.stringify(k), { headers }).toPromise();
    }

    public updateKitInfo(k: Kit): Promise<Object> {
        if (k == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.put(this.url + 'update/' + (k.dbid) + '/', JSON.stringify(k), { headers }).toPromise();
    }

    public deleteKit(k: Kit): Promise<Object> {
        if (k == undefined)  return; 
        return this.http.delete(this.url + 'delete/' + (k.dbid) + '/').toPromise()
    }


}