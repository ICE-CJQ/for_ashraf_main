import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Equipment } from '../objects/Equipment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Egg } from 'app/shared/objects/Chicken';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';

@Injectable()
export class EggService {
  url: string = UrlService.URL + `/egg/`;

  constructor(private http: HttpClient) {
  }

  public loadEgg(): Observable<Egg[]> {
      return this.http.get<Egg[]>(this.url + 'all/');
  }

  public getEggWithDbid(id: number): Observable<Egg> {
    return this.http.get<Egg>(this.url + 'get/' + id + '/');
}

  public loadEggBychickenid(chickenid:string): Observable<Egg[]> {
      return this.http.get<Egg[]>(this.url + `getIEggByChickenId/${chickenid}`);
  }

  public searchEggsByChickenid(chickenid:string): Observable<Egg[]> {
      return this.http.get<Egg[]>(this.url + `searchEggsByChickenId/${chickenid}`);
  }

  public addEgg(e: Egg): Promise<Object> {
      if (e == undefined) return;
    //   let  headers = new  HttpHeaders();
    //   headers.append('Content-Type', 'application/json');
      let  headers = new  HttpHeaders().set('Content-Type','application/json');
      return this.http.post(this.url + 'add/', JSON.stringify(e), { headers }).toPromise();
  }

  public updateEgg(eggs: Egg[]): Promise<Object> {
      if (eggs == undefined || eggs.length < 1) return;
    //   let  headers = new  HttpHeaders();
    //   headers.append('Content-Type', 'application/json');
      let  headers = new  HttpHeaders().set('Content-Type','application/json');
      eggs.forEach(egg => {
          if(egg.dbid == -1)
              return this.http.post(this.url + 'add/', JSON.stringify(egg), { headers }).toPromise();
          else 
              //console.log("I want to updte this kitlink");
              return this.http.post(this.url + 'update/' + (egg.dbid) + '/', JSON.stringify(egg),{ headers }).toPromise();
      });
  }

  public updateSingleEgg(e: Egg): Promise<Object> {
      if (e == undefined) return;
    //   let  headers = new  HttpHeaders();
    //   headers.append('Content-Type', 'application/json');
      let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.put(this.url + 'update/' + (e.dbid) + '/', JSON.stringify(e), { headers }).toPromise();
  }

  public deleteEgg(e: Egg): Promise<Object> {
      if (e == undefined) return;
          return this.http.delete(this.url + 'delete/' + (e.dbid) + '/').toPromise();
  }
}
