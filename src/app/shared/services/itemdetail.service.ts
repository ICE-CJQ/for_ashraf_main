import { Injectable } from '@angular/core';
import {HttpClient, HttpRequest, HttpEvent, HttpHeaders, HttpResponse, } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { itemdetail } from '../objects/item';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';

@Injectable()
export class ItemdetailService {
  url: string = UrlService.URL + `/Itemdetail/`;

  constructor(private http: HttpClient) {
  }

  public loadItemDetail(): Observable<itemdetail[]> {
      return this.http.get<itemdetail[]>(this.url + 'all/');
  }

  public searchItemdetailsByItemdbid(itemdbid:number): Observable<itemdetail[]> {
    return this.http.get<itemdetail[]>(this.url + `searchItemdetailsByItemid/${itemdbid}`);
}


  public loadItemDetailByItemDbid(itemdbid:number): Observable<itemdetail[]> {
      return this.http.get<itemdetail[]>(this.url + `getItemDetailByItemDbid/${itemdbid}`);
  }

  public getItemdetailWithId(id:number):  Observable<itemdetail>{
    return this.http.get<itemdetail>(this.url + 'get/' + id + '/');
}

  public addItemDetail(itemdetail: itemdetail): Promise<Object> {
      if (itemdetail == undefined) return;
      // let  headers = new  HttpHeaders();
      // headers.append('Content-Type', 'application/json');
      let  headers = new  HttpHeaders().set('Content-Type','application/json');
      return this.http.post(this.url + 'add/', JSON.stringify(itemdetail), { headers }).toPromise();
      //return this.http.post(this.url + 'add/', JSON.stringify(itemdetail), { headers });
/*
      headers.append('Content-Type', 'application/json');
      this.http.post(this.url + 'add/', JSON.stringify(k), { headers }).toPromise().then(_ => {
          return true;
      }).catch(err => {
          return false;
      });*/
  }

  public updateItemDetail(itemdetails: itemdetail[]): Promise<Object> {
      if (itemdetails == undefined || itemdetails.length < 1) return;
      // let  headers = new  HttpHeaders();
      // headers.append('Content-Type', 'application/json');
      let  headers = new  HttpHeaders().set('Content-Type','application/json');
      itemdetails.forEach(itemdetail => {
          if(itemdetail.dbid !== -1)
        //       return this.http.post(this.url + 'add/', JSON.stringify(itemdetail), { headers }).toPromise();
        //   else 
              //console.log("I want to updte this kitlink");
              return this.http.post(this.url + 'update/' + (itemdetail.dbid) + '/', JSON.stringify(itemdetail),{ headers }).toPromise();
      });
  }

  public updateSingleItemDetail(itemdetail: itemdetail): Promise<Object> {
      if (itemdetail == undefined) return;
      // let  headers = new  HttpHeaders();
      // headers.append('Content-Type', 'application/json');
      let  headers = new  HttpHeaders().set('Content-Type','application/json');
          return this.http.put(this.url + 'update/' + (itemdetail.dbid) + '/', JSON.stringify(itemdetail), { headers }).toPromise();
  }


  public deleteItemDetail(itemdetail: itemdetail): Promise<Object> {
      if (itemdetail == undefined) return;
          return this.http.delete(this.url + 'delete/' + (itemdetail.dbid) + '/').toPromise();
  }

  uploadFile(file: File, itemdbid:number): Promise<Object>{
      let formdata: FormData = new FormData();
 
      formdata.append('file', file);

    if (file == undefined) return;
    // let  headers = new  HttpHeaders();
    // headers.append('Content-Type', 'multipart/form-data');
    // return this.http.put(this.url + 'uploadfile/'+(itemdbid)+'/', formdata, { headers }).toPromise();
    return this.http.put(this.url + 'uploadfile/'+(itemdbid)+'/', formdata).toPromise();
  }

  uploadRfFile(file: File, itemdbid:number): Promise<Object>{
    let formdata: FormData = new FormData();
    formdata.append('file', file);
  if (file == undefined) return;
  return this.http.put(this.url + 'uploadrffile/'+(itemdbid)+'/', formdata).toPromise();
}
 
  // getFilepath(itemdetaildbid:number): Observable<any> {
  //   const httpOptions = {
  //     'responseType'  : 'arraybuffer' as 'json'
  //      //'responseType'  : 'blob' as 'json'        //This also worked
  //   };

  //   let headers = new HttpHeaders();
  //   headers = headers.append('Accept', 'application/pdf');


  //   const httpOptions = {
  //     responseType: 'blob' as 'json',
  //     headers: new HttpHeaders({
  //       'Accept': 'application/pdf'
    
  //     })
  //   };
  //   return this.http.get(this.url + 'getfile/'+itemdetaildbid+'/');
  //   return this.http.get(this.url + 'getfile/'+itemdetaildbid+'/');
  // }
  getFilepath(itemdetaildbid:number): Observable<any> {
    
    let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'MyApp-Application' : 'AppName', 'Accept': 'application/pdf' });
    let options = {
      headers,
      responseType: 'blob'
    }
    
    return this.http.get(this.url + 'getfile/'+itemdetaildbid+'/',  {headers, responseType:'blob'});
       //.map(this.extractContent);
        //.catch(this.getError);
    }

    getrfFilepath(itemdetaildbid:number): Observable<any> {
    
      let headers = new HttpHeaders({ 'Content-Type': 'application/json', 'MyApp-Application' : 'AppName', 'Accept': 'application/pdf' });
      //let options = new RequestOptions({ headers, responseType: ResponseContentType.Blob });
      
      return this.http.get(this.url + 'getrffile/'+itemdetaildbid+'/',  {headers, responseType:'blob'});
      }
}
