import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Worksheet} from '../objects/Worksheet';
import {UrlService} from './url.service';
import {Observable} from 'rxjs';

@Injectable()
export class WorksheetService {

  url: string = UrlService.URL + `/worksheet/`;
  constructor(private http: HttpClient) { }

  public addWorksheet(worksheet: Worksheet) {
    // tslint:disable-next-line:triple-equals
      if (worksheet == undefined) return;
      const  headers = new  HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(this.url + 'add/', JSON.stringify(worksheet), { headers }).toPromise();
  }

  public getAllWorksheet(): Observable<Worksheet[]> {
    return this.http.get<Worksheet[]>(this.url + 'all/');
  }

  public getWorksheetById(worksheetID: number): Observable<Worksheet> {
    return this.http.get<Worksheet>(this.url + `get/${worksheetID}`);
  }
}
