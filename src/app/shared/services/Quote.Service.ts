import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Quote} from '../objects/Quote';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';
@Injectable()
export class QuoteService {
    url: string = UrlService.URL + `/Quote/`;

    constructor(private http: HttpClient) {
    }

    public loadQuote(): Observable<Quote[]> {
        return this.http.get<Quote[]>(this.url + 'all/');
    }

    public count(): Observable<number> {
        return this.http.get<number>(this.url + 'count/');
    }

    public getLatest(): Observable<Quote>{
        return this.http.get<Quote>(this.url + 'latest/');
    }

    public search(key:string, page:number): Observable<Quote[]> {
        return this.http.get<Quote[]>(this.url + `search/${key}/${page}`);
    }

    public loadPage(page:number): Observable<Quote[]> {
        return this.http.get<Quote[]>(this.url + 'all/'+page);
    }

    public loadCompletedQuote(): Observable<Quote[]> {
        return this.http.get<Quote[]>(this.url + 'getCompletedQuote/');
    }

    public getQuoteById(id:number):Observable<Quote>{
        return this.http.get<Quote>(this.url + `get/${id}`);
    } 

    public addQuote(quote:Quote):Promise<Object>{
        if (quote == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.post(this.url + 'add/', JSON.stringify(quote), { headers }).toPromise();
    }

    public updateQuote(quote:Quote):Promise<Object>{
        if (quote == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.put(this.url + `update/${quote.dbid}`, JSON.stringify(quote), { headers }).toPromise();
    }

    public deleteQuote(quote:Quote): Promise<Object> {
        if (quote == undefined) return;
        return this.http.delete(this.url + 'delete/' + (quote.dbid) + '/').toPromise();
    }

    public getCurrencies(): Observable<any> {
        return this.http.get(this.url + 'currency');
    }
}