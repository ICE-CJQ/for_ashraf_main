import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { User } from "../objects/User";
// import { AppComponent } from "../../app.component";
import { UrlService } from 'app/shared/services/url.Service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { JsonPipe } from '@angular/common';

@Injectable()
export class AuthenticationService {
    url: string = UrlService.URL + `/login/`;
    user: User;

    constructor(private http: HttpClient) {
        // localStorage.setItem('stayLoggedIn', 'n');
    }

    public loadAllUsers(): Observable<User[]> {
        return this.http.get<User[]>(this.url + 'all/');
      }

    public requestResetPassword(email:string){
        return this.http.get<User>(this.url + `requestResetPassword/${email}/`);
    }

    public verifyResetPassToken(token:string): Observable<Object>{
        return this.http.get<Object>(this.url + `verifytoken/${token}/`);
    }

    getCurrentUser(): Promise<any> {
        if (!this.hasDbid()) {
            this.logout();
            return;
        }
        return this.getPromiseCacheUser();
    }

    getCurrentUserFromCache(): User {
        if (!this.hasDbid()) {
            this.logout();
            return;
        }
        if (this.user == undefined) {
            this.getPromiseCacheUser();
        }
        return this.user;
    }

    // Helper function for getCurrentUser() and getCurrentUserFromCache()
    // Fetch data from backend, save User, return Promise
    private getPromiseCacheUser = function() {
        let promise = this.http.get(this.url + `get/${sessionStorage.getItem('user.dbid')===null?localStorage.getItem('user.dbid'):sessionStorage.getItem('user.dbid')}`).toPromise();       
            promise.then(_=>{this.user = User.fromJson(_);});
            return promise;
    }

    // Checks to see if we can access user's dbid
    private hasDbid(): boolean {
        return localStorage.getItem('user.dbid') !== null || sessionStorage.getItem('user.dbid') !== null;
    }

    isLogin(): boolean {
        return this.hasDbid();
    }

    // login(user: User, isR: boolean) {

    //     if (user == undefined) return;
    //     let  headers = new  HttpHeaders().set('Content-Type','application/json');
    //    return this.http.post(this.url + 'authenticate', JSON.stringify(user), { headers}).toPromise().then(_ => {
    //         if (_!=null) {
    //             localStorage.setItem('user.dbid', _['dbid']);
    //             sessionStorage.setItem('user.dbid', _['dbid']);
    //             if (isR) {
    //                 localStorage.setItem('user.email', user.email);
    //                 localStorage.setItem('isR', 'y');
    //             }
    //             else {
    //                 localStorage.setItem('isR', 'n');
    //             }

    //             let stayLoggedIn = localStorage.getItem('stayLoggedIn');
    //             if (stayLoggedIn === 'y') {
    //                 localStorage.setItem('user.dbid', _['dbid']);
    //                 localStorage.setItem('user.role', _['role']);
    //             } 
    //             else {
    //                 sessionStorage.setItem('user.dbid', _['dbid']);
    //                 sessionStorage.setItem('user.role', _['role']);
    //             }
    //         }
    //     }).catch(err => {
    //         localStorage.clear();
    //         sessionStorage.clear();
    //     });
    // }

    login(user: User) {

        if (user == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');

        return new Promise((resolve, reject) => {
            this.http.post<User>(this.url + 'authenticate', JSON.stringify(user), { headers}).toPromise().then(user=>{
            if (user!=null) {
                if(user.password==null){
                    let count = user.invalidlogincount;
                    count= count+1;
                    this.updateInvalidCount(user.dbid, count);
                    if(count<3){
                        reject('Invalid email or password');
                    }
                    else{
                        this.updateActive(user.dbid, false);
                        reject('Your account is inactive, please contact administrator');
                    }
                    localStorage.clear();
                    sessionStorage.clear();
                    
                }
                else{
                    if(user.active==false){
                        reject('Your account is inactive, please contact administrator');
                        localStorage.clear();
                        sessionStorage.clear();
                    }
                    else{
                        resolve('login')
                        localStorage.setItem('user.dbid', user.dbid.toString());
                        sessionStorage.setItem('user.dbid', user.dbid.toString());
                        this.updateInvalidCount(user.dbid, 0);
                    }

                }

            }
            else{
                reject('Invalid email or password');
                localStorage.clear();
                sessionStorage.clear();
            }

            })
            .catch(err => {
            localStorage.clear();
            sessionStorage.clear();
        });


        });





        return this.http.post<User>(this.url + 'authenticate', JSON.stringify(user), { headers}).toPromise();

    //    return this.http.post<User>(this.url + 'authenticate', JSON.stringify(user), { headers}).toPromise()
    //    .then(user => {
    //         if (user!=null) {
    //             if(user.password==null){
    //                 window.alert("Wrong password");
    //                 let count = user.invalidlogincount;
    //                 count= count+1;
    //                 this.updateInvalidCount(user.dbid, count);
    //                 if(count==3){
    //                     this.updateActive(user.dbid, false);
    //                 }
    //             }
    //             else{
    //                 if(user.active==false){
    //                     window.alert("Your account is inactive");
    //                 }
    //                 else{
    //                     localStorage.setItem('user.dbid', user.dbid.toString());
    //                     sessionStorage.setItem('user.dbid', user.dbid.toString());
    //                 }

    //             }

    //         }
    //         else{
    //             window.alert("Wrong email");
    //         }
    //     })
    //     .catch(err => {
    //         localStorage.clear();
    //         sessionStorage.clear();
    //     });
    }

    updatePass(user: User) {
        if (user == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.put(this.url + 'updateuser/' + (user.dbid) + '/', JSON.stringify(user), { headers }).toPromise();
    }

    updatePassword(userid:number, password:string){
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.put(this.url + `updatepassword/${userid}/${password}`, null, { headers }).toPromise();
    }

    updateInvalidCount(userid:number, count:number){
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.put(this.url + `updatepinvalidcount/${userid}/${count}`, null, { headers }).toPromise();
    }

    updateActive(userid:number, status:boolean){
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.put(this.url + `updateactive/${userid}/${status}`, null, { headers }).toPromise();
    }

    logout() {
        // let rememberLogin = localStorage.getItem('isR');
        // let email = localStorage.getItem('user.email');

        this.user = undefined;
        localStorage.clear();
        sessionStorage.clear();

        // localStorage.setItem('isR', rememberLogin);
        // if (rememberLogin === 'y') {
        //     localStorage.setItem('user.email', email);
        // }
    }
}