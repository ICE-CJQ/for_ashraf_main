import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Quote, SaleLink } from '../objects/Quote';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';
@Injectable()
export class SaleLinkService {
    url: string = UrlService.URL + `/SaleLink/`;

    constructor(private http: HttpClient) {
    }

    public loadSaleLink(): Observable<SaleLink[]> {
        return this.http.get<SaleLink[]>(this.url + 'all/');
    }

    public loadAllLinkById(id:number, type:string): Observable<SaleLink[]> {
        return this.http.get<SaleLink[]>(this.url + `getLinksById/${type}/${id}`);
    }

    public getSaleLinkById(id:number):Observable<SaleLink >{
        return this.http.get<SaleLink>(this.url + `get/${id}`);
    }

    public addSaleLink(quoteLink:SaleLink):Promise<Object>{
        if (quoteLink == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.post(this.url + 'add/', JSON.stringify(quoteLink), { headers }).toPromise();
    }

    public updateSaleLink(quoteLink:SaleLink):Promise<Object>{
        if (quoteLink == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.put(this.url + `update/${quoteLink.dbid}`, JSON.stringify(quoteLink), { headers }).toPromise();
    }

    public deleteSaleLink(quoteLink:SaleLink): Promise<Object> {
        if (quoteLink == undefined) return;
        return this.http.delete(this.url + 'delete/' + (quoteLink.dbid) + '/').toPromise();
    }
}