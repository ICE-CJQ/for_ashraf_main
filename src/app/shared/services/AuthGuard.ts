import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthenticationService } from './Authenticate.Service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private auth: AuthenticationService, private router:Router) { };

    canActivate() {
        if (this.auth.isLogin()) {
            return true;
        } else {
            // window.alert("You don't have permission to view this page");
            this.router.navigate(['/login']);
            return false;
        }
    }
}