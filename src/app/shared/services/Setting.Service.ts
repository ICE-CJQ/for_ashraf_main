import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Setting, ConfigValue } from '../objects/Setting';
import { UrlService } from 'app/shared/services/url.Service';

@Injectable()
export class SettingService {
    url: string = UrlService.URL + `/setting/`;

    constructor(private http: HttpClient) { }

    public loadSetting(): Observable<Setting[]> {
        return this.http.get<Setting[]>(this.url + 'all/');
    }

    public getSettingByPage(page: string): Observable<Setting[]> {
        return this.http.get<Setting[]>(this.url + `getByPage/${page}`);
    }

    public getSettingByPageAndType(page: string, type: string): Observable<Setting> {
        return this.http.get<Setting>(this.url + `getByPageAndType/${page}/${type}`);
    }

    public addSetting(i: Setting): Promise<Object> {
        if (i == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.post(this.url + 'add/', JSON.stringify(i), { headers }).toPromise();
    }

    public updateSetting(i: Setting): Promise<Object> {
        if (i == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.put(this.url + 'update/' + (i.dbid), JSON.stringify(i), { headers }).toPromise();
    }

    public deleteSetting(i: Setting): Promise<Object> {
        if (i == undefined) return;
        return this.http.delete(this.url + 'delete/' + (i.dbid) + '/').toPromise();
    }
}