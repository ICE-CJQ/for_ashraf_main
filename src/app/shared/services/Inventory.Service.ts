
import {of as observableOf,  Observable } from 'rxjs';
import { item, ItemWDetails } from '../objects/item';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';


@Injectable()
export class InventoryService {
    url: string = UrlService.URL + `/invent/`;
    invent: item[];

    constructor(private http: HttpClient) { }

    public searchSingleInventoryByCat(cat: string): Observable<item> {
        return this.http.get<item>(this.url + `searchSingleInventoryByCat/${cat}`);
    }

    public searchSingleInventoryByName(name: string): Observable<item> {
        name = name.replace(/\//g, 'slash');
        name = name.replace(/\s/g, 'whitespace');
        name = name.replace(/#/g, 'pong');
        name = name.replace(/%/g, 'percent');
        name = name.replace(/\?/g, 'questionmark');
        return this.http.get<item>(this.url + `searchSingleInventoryByName/${name}/`);
    }

    public loadInventory(): Observable<item[]> {
        return this.http.get<item[]>(this.url + 'all/');
    }

    public loadInventoryWDetails(page: number): Observable<ItemWDetails[]> {
        return this.http.get<ItemWDetails[]>(this.url + 'allWDetails/' + page);
    }

    public searchInventory(search: string, page: number, searchArea:string): Observable<ItemWDetails[]> {
        //encode all white space and #
        search = search.replace(/\//g, 'slash');
        search = search.replace(/\s/g, 'whitespace');
        search = search.replace(/#/g, 'pong');
        search = search.replace(/%/g, 'percent');
        search = search.replace(/\?/g, 'questionmark');
        if(searchArea == undefined || searchArea == '' || searchArea == 'All'){
            return this.http.get<ItemWDetails[]>(this.url + 'searchWPage/' + search + '/' + page);
        }
        if(searchArea == 'cat'){
            return this.http.get<ItemWDetails[]>(this.url + 'searchByCatWPage/' + search + '/' + page);
        }
        if(searchArea == 'name'){
            return this.http.get<ItemWDetails[]>(this.url + 'searchByNameWPage/' + search + '/' + page);
        }
        if(searchArea == 'location'){
            return this.http.get<ItemWDetails[]>(this.url + 'searchByLocWPage/' + search + '/' + page);
        }
        if(searchArea == 'sublocation'){
            return this.http.get<ItemWDetails[]>(this.url + 'searchBySubLocWPage/' + search + '/' + page);
        }
        if(searchArea == 'suppliermanufacturer'){
            return this.http.get<ItemWDetails[]>(this.url + 'searchBySupplierorManufacturerWPage/' + search + '/' + page);
        }
    }
    
    public getSearchCount(search: string,searchArea:string): Observable<number> {
        //encode all white space and #
        search = search.replace(/\//g, 'slash');
        search = search.replace(/\s/g, 'whitespace');
        search = search.replace(/#/g, 'pong');
        search = search.replace(/%/g, 'percent');
        search = search.replace(/\?/g, 'questionmark');
        if(searchArea == undefined || searchArea == '' || searchArea == 'All'){
            return this.http.get<number>(this.url + 'searchAllCount/' + search + '/');
        }
        if(searchArea == 'cat'){
            return this.http.get<number>(this.url + 'searchByCatCount/' + search + '/');
        }
        if(searchArea == 'name'){
            return this.http.get<number>(this.url + 'searchByNameCount/' + search + '/');
        }
        if(searchArea == 'location'){
            return this.http.get<number>(this.url + 'searchByLocatonCount/' + search + '/');
        }
        if(searchArea == 'sublocation'){
            return this.http.get<number>(this.url + 'searchBySubLocatonCount/' + search + '/');
        }
        if(searchArea == 'suppliermanufacturer'){
            return this.http.get<number>(this.url + 'searchBySupplierorManufacturerCount/' + search + '/');
        }
    }

    public getCount(): Observable<number> {
        return this.http.get<number>(this.url + 'count/');
    }

    public getItemWithId(id: number): Observable<item> {
        return this.http.get<item>(this.url + 'get/' + id + '/');
    }

    // public exist(item: item): Promise<item> {
    //     if (item == undefined) return;
    //     let  headers = new  HttpHeaders();

    //     headers.append('Content-Type', 'application/json');
    //     return this.http.post(this.url + 'checkExist', JSON.stringify(item), { headers })
    //         .pipe(map(response => response.json()), catchError(error => {
    //             return observableOf(null);
    //         })).toPromise();
    // }

    public addItem(i: item): Promise<Object> {
        if (i == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.post(this.url + 'add/', JSON.stringify(i), { headers }).toPromise();
    }

    public updateItem(i: item): Promise<Object> {

        if (i == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.put(this.url + 'update/' + (i.dbid) + '/', JSON.stringify(i), { headers }).toPromise();
    }

    public deleteItem(i: item): Promise<Object> {
        if (i == undefined) return;
        return this.http.delete(this.url + 'delete/' + (i.dbid) + '/').toPromise();
    }
}