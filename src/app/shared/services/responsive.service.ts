import { Injectable } from '@angular/core';
import { Subject ,  BehaviorSubject ,  Observable } from "rxjs";

@Injectable()
export class ResponsiveService {
  private isMobile = new Subject();
  private isTablet = new Subject();
  public screenWidth: string;
  constructor() {this.checkWidth(); }

  // onMobileChange(status: boolean) {
  //   this.isMobile.next(status);
  // }

  // getMobileStatus(): Observable<any> {
  //   return this.isMobile.asObservable();
  // }

  onTabletChange(status: boolean) {
    this.isTablet.next(status);
  }

  getTabletStatus(): Observable<any> {
    return this.isTablet.asObservable();
  }

  public checkWidth() {
    var width = window.innerWidth;
    var height =  window.innerHeight;
    if (width <= 768) {
        this.screenWidth = 'sm';
        //this.onMobileChange(true);
        this.onTabletChange(false);
    } else if (width > 768 && width <= 1280) {
        this.screenWidth = 'md';
        //this.onMobileChange(false);
        this.onTabletChange(true);
    } else {
        this.screenWidth = 'lg';
        //this.onMobileChange(false);
        this.onTabletChange(false);
    }
  }

}
