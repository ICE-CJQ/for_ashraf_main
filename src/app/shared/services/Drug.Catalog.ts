import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { DrugList } from '../objects/DrugList';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';
@Injectable()
export class DrugListService { 
    url: string = UrlService.URL + `/drugList/`;

    constructor(private http: HttpClient) {
        let list = [new DrugList(-1, 'Exendin-4 (Exenatide®)', 11),
        new DrugList(-1, 'Etanercept (Enbrel®)', 12),
        new DrugList(-1, 'Glargine (Lantus®)', 13),
        new DrugList(-1, 'Glucagon (GlucaGen®)', 14),
        new DrugList(-1, 'Cetuximab (Erbitux®)', 15),
        new DrugList(-1, 'Pegylated Proteins', 16),
        new DrugList(-1, 'Alpha1-Antitrypsin (Prolastin®)', 17),
        new DrugList(-1, 'Infliximab (Remicade®)', 18),
        new DrugList(-1, 'Filgrastim (Neupogen®)', 19),
        new DrugList(-1, 'Erythropoietin (Epogen®)', 20),
        new DrugList(-1, 'Glucose', 21),
        new DrugList(-1, 'Rituximab (Rituxan®)', 22),
        new DrugList(-1, 'C1-inhibitor (Berinert®)', 23),
        new DrugList(-1, 'Adalimumab (Humira®)', 24),
        new DrugList(-1, 'mPEG', 25),
        new DrugList(-1, 'Human drug specific Isotyping ELISA kit', 26),
        new DrugList(-1, 'Human drug specific Sub-isotyping ELISA kit', 27),
        new DrugList(-1, 'Interferon beta-1a (Avonex® Rebif®)', 28),
        new DrugList(-1, 'Human Insulin', 29),
        new DrugList(-1, 'Recombinant Human Hyaluronidase (rhuPH20 ®))', 70),
        new DrugList(-1, 'Etanercept (Enbrel®) FREE', 31),
        new DrugList(-1, 'Omalizumab (Xolair®)', 32),
        new DrugList(-1, 'Abciximab (Reopro®)', 33),
        new DrugList(-1, 'Alemtuzumab (Campath®)', 34),
        new DrugList(-1, 'Trastuzumab (Herceptin®)', 35),
        new DrugList(-1, 'Basiliximab(Sumulect®)', 36),
        new DrugList(-1, 'Certolizumab pegot (Cimzia®)', 37),
        new DrugList(-1, 'Daclizumab (Zenpax®)', 38),
        new DrugList(-1, 'Eculizumab(Soliris®)', 39),
        new DrugList(-1, 'Efalizumab (Raptiva®)', 40),
        new DrugList(-1, 'Bevacizumab (Avastin®) ', 41),
        new DrugList(-1, 'Growth Hormone Releasing Hormone (Sermorelin®) ', 42),
        new DrugList(-1, 'PTH (1-34) – Teriparatide', 43),
        new DrugList(-1, 'Peg-Filgrastim (Neulasta®)', 44),
        new DrugList(-1, 'Gemtuzumab ozogamicin (Mylotarg®)', 45),
        new DrugList(-1, 'Ibritumomab tiuxetan (Zevalin®)', 46),
        new DrugList(-1, 'Muromonab-CD3 (Orthoclone OKT3)', 47),
        new DrugList(-1, 'Palivizumab (Synagis®)', 48),
        new DrugList(-1, 'Panitumumab(Vectibix®)', 49),
        new DrugList(-1, 'Ranibizumab(Lucentis®)', 50),
        new DrugList(-1, 'Tositumomab (Bexxar®)', 51),
        new DrugList(-1, 'Insulin Aspart (Novolog®)', 52),
        new DrugList(-1, 'Bridging Immunogenicity ELISA Development kit', 53),
        new DrugList(-1, 'Imiglucerase (Cerezyme®)', 54),
        new DrugList(-1, 'Rafiq will add', 55),
        new DrugList(-1, 'Factor Vlll', 56),
        new DrugList(-1, 'PEG Filgrastim detects PEG', 57),
        new DrugList(-1, 'Copaxone', 58),
        new DrugList(-1, 'Darbepoetin Alpha', 59),
        new DrugList(-1, 'Sample amplification (MISC/custom)', 60),
        new DrugList(-1, 'Rituximab (Rituxan®) Urine', 61),
        new DrugList(-1, 'Octreotide', 62),
        new DrugList(-1, 'Glargine (Lantus®) solution phase', 63),
        new DrugList(-1, 'Denosumab', 64),
        new DrugList(-1, 'Folicle Stimulating Hormone', 65),
        new DrugList(-1, 'Insulin (NEEDED FOR CUSTOM CELERION ANTIBODY)', 66),
        new DrugList(-1, 'CTC', 67),
        new DrugList(-1, 'Kadcyla (ado-trastuzumab emtansine)', 68),
        new DrugList(-1, 'PD-1', 69),
        new DrugList(-1, 'PDL-1', 30),
        new DrugList(-1, 'PTH (1-84) – Teriparatide', 71),
        new DrugList(-1, 'Ustekinumab (Stelara)', 72),
        new DrugList(-1, 'Abatacept (Orencia®)', 73),
        new DrugList(-1, 'Ofatumumab (Arzerra®, HuMax-CD20)', 74),
        new DrugList(-1, 'Ipilimumab (Yervoy®)', 75),
        new DrugList(-1, 'Nivolumab (Opdivo®)', 76),
        new DrugList(-1, 'Pembrolizumab (Keytruda®)', 77),
        new DrugList(-1, 'Pertuzumab (Perjeta®)', 78),
        new DrugList(-1, 'Vedolizumab (Entyvio®)', 80),
        new DrugList(-1, 'Terifrac', 81),
        new DrugList(-1, 'Secukinumab (Cosentyx)', 82),
        new DrugList(-1, 'Hepatitis B Immune Globulin (HepaGam B®)', 83),
        new DrugList(-1, 'IgY', 84),
        new DrugList(-1, 'Pelgraz', 85),
        new DrugList(-1, 'Folisurge', 86),
        new DrugList(-1, 'Bevatas', 87),
        new DrugList(-1, ' Sargramostim (Leukine)®', 88),
        new DrugList(-1, 'aflibercept (Eylea)®', 89),
        new DrugList(-1, 'IGF-1 ( Increlex)', 90),
        new DrugList(-1, 'razumab', 91),
        new DrugList(-1, '765IGF-MTX', 92),
        new DrugList(-1, 'Bevacizumab(Avastin®) VEGF', 93),
        new DrugList(-1, 'do not use ', 99),]

        // list.forEach(d=>{
        //     this.addDrugList(d);
        // })
    }

    public loadDrugList(): Observable<DrugList[]> {
        return this.http.get<DrugList[]> (this.url + 'all/');
    }

    public getDrugListById(id:number): Observable<DrugList>{
        return this.http.get<DrugList> (this.url + `get/${id}`);
    }

    public addDrugList(e: DrugList): Promise<Object> {
        if (e == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.post(this.url + 'add/', JSON.stringify(e), { headers }).toPromise()
    }

    public updateDrugList( e: DrugList): Promise<Object> {
        if (e == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.put(this.url + 'update/' + (e.dbid) + '/', JSON.stringify(e), { headers }).toPromise();
        // return true;
    }

    public deleteDrugList(e: DrugList): Promise<Object> {
        if (e == undefined) return;
        return this.http.delete(this.url + 'delete/' + (e.dbid) + '/').toPromise();
    }
}