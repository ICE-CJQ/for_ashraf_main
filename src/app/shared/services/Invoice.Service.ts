import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Invoice } from '../objects/Invoice';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';

@Injectable()
export class InvoiceService {
    url: string = UrlService.URL + `/invoice/`;

    constructor(private http: HttpClient) {
    }

    public loadInvoice(): Observable<Invoice[]> {
        return this.http.get<Invoice[]>(this.url + 'all/');
    }

    public count(): Observable<number> {
        return this.http.get<number>(this.url + 'count/');
    }

    public getLatest(): Observable<Invoice>{
        return this.http.get<Invoice>(this.url + 'latest/');
    }

    public search(key:string, page:number): Observable<Invoice[]> {
        return this.http.get<Invoice[]>(this.url + `search/${key}/${page}`);
    }
 
    public loadPage(page:number): Observable<Invoice[]> {
        return this.http.get<Invoice[]>(this.url + 'all/'+page);
    }

    public getInvoiceById(id:number):Observable<Invoice>{
        return this.http.get<Invoice>(this.url + `get/${id}`);
    } 

    public addInvoice(invoice:Invoice):Promise<Object>{
        if (invoice == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.post(this.url + 'add/', JSON.stringify(invoice), { headers }).toPromise();
    }

    public updateInvoice(invoice:Invoice):Promise<Object>{
        if (invoice == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.put(this.url + `update/${invoice.dbid}`, JSON.stringify(invoice), { headers }).toPromise();
    }

    public deleteInvoice(invoice:Invoice): Promise<Object> {
        if (invoice == undefined) return;
        return this.http.delete(this.url + 'delete/' + (invoice.dbid) + '/').toPromise();
    }

}