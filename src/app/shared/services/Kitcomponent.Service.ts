import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Equipment } from '../objects/Equipment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Kitcomponent } from 'app/shared/objects/Kit';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';

@Injectable()
export class KitcomponentService {
    url: string = UrlService.URL + `/KitComponent/`;

    constructor(private http: HttpClient) {
    }

    public loadKitLink(): Observable<Kitcomponent[]> {
        return this.http.get<Kitcomponent[]>(this.url + 'all/');
    }

    public loadKitLinkBykitid(kitid:number): Observable<Kitcomponent[]> {
        return this.http.get<Kitcomponent[]>(this.url + `getKitLinkByKitId/${kitid}`);
    }

    public searchKitComponentsByKitid(kitid:number): Observable<Kitcomponent[]> {
        return this.http.get<Kitcomponent[]>(this.url + `searchKitComponentsByKitid/${kitid}`);
    }

    public addKitLink(k: Kitcomponent): Promise<Object> {
        if (k == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.post(this.url + 'add/', JSON.stringify(k), { headers }).toPromise();
/*
        headers.append('Content-Type', 'application/json');
        this.http.post(this.url + 'add/', JSON.stringify(k), { headers }).toPromise().then(_ => {
            return true;
        }).catch(err => {
            return false;
        });*/
    }

    public updateKitLink(newLink: Kitcomponent[]): Promise<Object> {
        if (newLink == undefined || newLink.length < 1) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        newLink.forEach(link => {
            if(link.dbid == -1)
                return this.http.post(this.url + 'add/', JSON.stringify(link), { headers }).toPromise();
            else 
                //console.log("I want to updte this kitlink");
                return this.http.post(this.url + 'update/' + (link.dbid) + '/', JSON.stringify(link),{ headers }).toPromise();
        });
    }

    public updateSingleKitLink(link: Kitcomponent): Promise<Object> {
        if (link == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        //console.log("somebody using the updateSingleKitLink?");
        // link.forEach(link => {
            return this.http.put(this.url + 'update/' + (link.dbid) + '/', JSON.stringify(link), { headers }).toPromise();
        // });
    }


    public deleteKitLink(k: Kitcomponent): Promise<Object> {
        if (k == undefined) return;
            return this.http.delete(this.url + 'delete/' + (k.dbid) + '/').toPromise();
    }
}