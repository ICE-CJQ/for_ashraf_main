import { Injectable } from '@angular/core';
import { orderitem, emailDetail } from '../objects/OrderItem';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UrlService } from 'app/shared/services/url.Service';

@Injectable()
export class OrderitemService {
    url: string = UrlService.URL + `/orderitem/`;
    invent: orderitem[];

    constructor(private http: HttpClient) { }

    public loadOrderitem(): Observable<orderitem[]> {
        return this.http.get<orderitem[]>(this.url + 'all/');
    }

    public loadAllByStatus(status: string): Observable<orderitem[]> {
        return this.http.get<orderitem[]>(this.url + `all/${status}/`);
    }

    public loadOrderByStatus(status: string, page: number): Observable<orderitem[]> {
        return this.http.get<orderitem[]>(this.url + `orderWStatus/${status}/${page}`);
    }

    public loadUrgentOrderByStatus(status: string, page: number): Observable<orderitem[]> {
        return this.http.get<orderitem[]>(this.url + `urgentOrderWStatus/${status}/${page}`);
    }

    public loadUrgentOrderByStatusCount(status: string): Observable<number> {
        return this.http.get<number>(this.url + `urgentOrderCount/${status}/`);
    }

    public searchOrder(status:string, searchKey:string, page:number): Observable<orderitem[]>{
        searchKey = searchKey.replace(/\//g, 'slash');
        searchKey = searchKey.replace(/\s/g, 'whitespace');
        searchKey = searchKey.replace(/#/g, 'pong');
        searchKey = searchKey.replace(/%/g, 'percent');
        searchKey = searchKey.replace(/\?/g, 'questionmark');
        return this.http.get<orderitem[]>(this.url + `search/${status}/${searchKey}/${page}`);
    }

    public searchOrderCount(status:string, searchKey:string): Observable<number>{
        searchKey = searchKey.replace(/\//g, 'slash');
        searchKey = searchKey.replace(/\s/g, 'whitespace');
        searchKey = searchKey.replace(/#/g, 'pong');
        searchKey = searchKey.replace(/%/g, 'percent');
        searchKey = searchKey.replace(/\?/g, 'questionmark');
        return this.http.get<number>(this.url + `search/${status}/${searchKey}/`);
    }

    public getCount(status:string): Observable<number>{
        return this.http.get<number>(this.url + `count/${status}`);
    }

    public addOrderitem(i: orderitem): Observable<orderitem> {
        if (i == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.post<orderitem>(this.url + 'add/', JSON.stringify(i), { headers });
    }

    public updateOrderitem(i: orderitem): Promise<Object> {

        if (i == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        console.log(JSON.stringify(i));
        return this.http.put(this.url + 'update/' + (i.dbid) + '/', JSON.stringify(i), { headers }).toPromise();
    }

    public deleteOrderitem(i: orderitem): Promise<Object> {
        if (i == undefined) return;
        return this.http.delete(this.url + 'delete/' + (i.dbid) + '/').toPromise();
    }

    // public noEtaEmail(emaildetail:emailDetail): Promise<Object>{
    //     if (emaildetail == undefined) return;
    //     let  headers = new  HttpHeaders().set('Content-Type','application/json');
    //     return this.http.put(this.url + 'noeta/', JSON.stringify(emaildetail), { headers }).toPromise();
    //   }

    public sendEmail(emaildetail:emailDetail): Promise<Object>{
        if (emaildetail == undefined) return;
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.put(this.url + 'sendemail/', JSON.stringify(emaildetail), { headers }).toPromise();
      }

    //   public approveNotice(emaildetail:emailDetail): Promise<Object>{
    //     if (emaildetail == undefined) return;
    //     // let  headers = new  HttpHeaders();
    //     // headers.append('Content-Type', 'application/json');
    //     let  headers = new  HttpHeaders().set('Content-Type','application/json');
    //     return this.http.put(this.url + 'approvenotice/', JSON.stringify(emaildetail), { headers }).toPromise();
    //   }

}
