import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Product } from 'app/shared/objects/Product';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';
@Injectable()
export class ProductService {
  url: string = UrlService.URL + `/product/`;
  applicationUrl: string = UrlService.URL + `/applicaiton/`;

  constructor(private http: HttpClient) { }
  public loadProduct(): Observable<Product[]> {
    return this.http.get<Product[]>(this.url + 'all/');
}

public getCount(): Observable<number> {
    return this.http.get<number>(this.url + 'count/');
}

public loadProductByPage(page: number): Observable<Product[]> {
    return this.http.get<Product[]>(this.url + 'all/' + page);
}


public searchProduct(searchKey: string, page: number): Observable<Product[]> {
    searchKey = searchKey.replace(/\//g, 'slash');
    searchKey = searchKey.replace(/\s/g, 'whitespace');
    searchKey = searchKey.replace(/#/g, 'pong');
    searchKey = searchKey.replace(/%/g, 'percent');
    searchKey = searchKey.replace(/\?/g, 'questionmark');
    return this.http.get<Product[]>(this.url + 'searchName/' + searchKey + '/' + page);
}

public searchProductCount(search: string): Observable<number> {
    //encode all white space and #
    search = search.replace(/\//g, 'slash');
    search = search.replace(/\s/g, 'whitespace');
    search = search.replace(/#/g, 'pong');
    search = search.replace(/%/g, 'percent');
    search = search.replace(/\?/g, 'questionmark');
    return this.http.get<number>(this.url + 'searchNameCount/' + search + '/');
}

public addProduct(p: Product): Promise<Object> {
    if (p == undefined) return;
    // let  headers = new  HttpHeaders();
    // headers.append('Content-Type', 'application/json');
    let  headers = new  HttpHeaders().set('Content-Type','application/json');
    return this.http.post(this.url + 'add/', JSON.stringify(p), { headers }).toPromise();
}

public updateProduct(p: Product): Promise<Object> {
    if (p == undefined) return;
    //let  headers = new  HttpHeaders();
    let  headers = new  HttpHeaders().set('Content-Type','application/json');
    return this.http.put(this.url + 'update/' + (p.dbid) + '/', JSON.stringify(p), { headers }).toPromise();
}

public deleteProduct(p: Product): Promise<Object> {
    if (p == undefined) return;
    return this.http.delete(this.url + 'delete/' + (p.dbid) + '/').toPromise();
}

// public searchSingleRecipeByName(name:string): Observable<Product> {
//     name = name.replace(/\//g, 'slash');
//     name = name.replace(/\s/g, 'whitespace');
//     name = name.replace(/#/g, 'pong');
//     name = name.replace(/%/g, 'percent');
//     name = name.replace(/\?/g, 'questionmark');
//     return this.http.get(this.url + `searchSingleRecipeByName/${name}/`);
// }

// public searchSingleRecipeBySId(recipeid:string): Observable<Recipe> {
//     console.log("in recipe service, recipe id is "+recipeid);
//     return this.http.get(this.url + `searchSingleRecipeBySId/${recipeid}/`);
// }

// public searchSingleRecipeByDId(dbid:number): Observable<Recipe> {
//     console.log("in recipe service, recipe dbid is "+dbid);
//     return this.http.get(this.url + `searchSingleRecipeByDId/${dbid}`);
// }

// public searchRecipe(searchKey: string, page: number, searchArea: string): Observable<Product[]> {
//     searchKey = searchKey.replace(/\//g, 'slash');
//     searchKey = searchKey.replace(/\s/g, 'whitespace');
//     searchKey = searchKey.replace(/#/g, 'pong');
//     searchKey = searchKey.replace(/%/g, 'percent');
//     searchKey = searchKey.replace(/\?/g, 'questionmark');
//     if (searchArea == undefined || searchArea == '' || searchArea == 'All')
//         return this.http.get(this.url + 'search/' + searchKey + '/' + page);
//     if (searchArea == 'Recipeid')
//         return this.http.get(this.url + 'searchId/' + searchKey + '/' + page);
//     if (searchArea == 'Name'){
//         return this.http.get(this.url + 'searchName/' + searchKey + '/' + page);
//     }
// }

// public searchRecipeCount(search: string,searchArea:string): Observable<number> {
//     //encode all white space and #
//     search = search.replace(/\//g, 'slash');
//     search = search.replace(/\s/g, 'whitespace');
//     search = search.replace(/#/g, 'pong');
//     search = search.replace(/%/g, 'percent');
//     search = search.replace(/\?/g, 'questionmark');
//     if(searchArea == undefined || searchArea == '' || searchArea == 'All'){
//         return this.http.get(this.url + 'searchCount/' + search + '/');
//     }
//     if(searchArea == 'Recipeid'){
//         return this.http.get(this.url + 'searchIdCount/' + search + '/');
//     }
//     if(searchArea == 'Name'){
//         return this.http.get(this.url + 'searchNameeCount/' + search + '/');
//     }
// }

}
