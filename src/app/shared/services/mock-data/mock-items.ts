import { Item } from "./item";

export const ITEMS: Item[] = [
  {
    name: "Carb/Bi-Carb",
    suppliers: ["Somru", "A", "B", "C", "D", "E"],
    lotNumbers: ["#021", "#0212", "#0215"]
  },

  {
    name: "Anti-6XHIS",
    suppliers: ["R&D Systems", "MAB050", "A", "B", "C", "D", "E"],
    lotNumbers: ["#022", "#021234", "#022"]
  },

  {
    name: "Superblock",
    suppliers: ["ScyTek", "Cat#AAA999", "A", "B", "C", "D", "E"],
    lotNumbers: ["#023", "#02sdfsdf1", "#021sdf"]
  },

  {
    name: "1X PBST",
    suppliers: ["Roche", "A", "B", "C", "D", "E"],
    lotNumbers: ["#024", "#021ss", "#021sdf"]
  },

  {
    name: "Avastin",
    suppliers: ["INTAS", "A", "B", "C", "D", "E"],
    lotNumbers: ["#025", "#0sdfd1", "#02sdf"]
  },

  {
    name: "Bevatas",
    suppliers: ["NIST", "A", "B", "C", "D", "E"],
    lotNumbers: ["#026", "#021222", "#021sss"]
  }
];
