export interface Item {
  name: string;
  suppliers: string[];
  lotNumbers: string[];
}
