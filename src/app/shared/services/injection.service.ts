import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Equipment } from '../objects/Equipment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injection } from 'app/shared/objects/Chicken';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';
@Injectable()
export class InjectionService {
  url: string = UrlService.URL + `/injection/`;

  constructor(private http: HttpClient) {
  }

  public loadInjection(): Observable<Injection[]> {
      return this.http.get<Injection[]>(this.url + 'all/');
  }

  public loadInjectionBychickenid(chickenid:string): Observable<Injection[]> {
      return this.http.get<Injection[]>(this.url + `getInjectionByChickenId/${chickenid}`);
  }

  public searchInjectionsByChickenid(chickenid:string): Observable<Injection[]> {
      return this.http.get<Injection[]>(this.url + `searchInjectionByChickenId/${chickenid}`);
  }

  public addInjection(i: Injection): Promise<Object> {
      if (i == undefined) return;
    //   let  headers = new  HttpHeaders();
    //   headers.append('Content-Type', 'application/json');
      let  headers = new  HttpHeaders().set('Content-Type','application/json');
      return this.http.post(this.url + 'add/', JSON.stringify(i), { headers }).toPromise();
  }

  public updateInjection(injections: Injection[]): Promise<Object> {
      if (injections == undefined || injections.length < 1) return;
    //   let  headers = new  HttpHeaders();
    //   headers.append('Content-Type', 'application/json');
      let  headers = new  HttpHeaders().set('Content-Type','application/json');
      injections.forEach(injection => {
          if(injection.dbid == -1)
              return this.http.post(this.url + 'add/', JSON.stringify(injection), { headers }).toPromise();
          else 
              //console.log("I want to updte this kitlink");
              return this.http.post(this.url + 'update/' + (injection.dbid) + '/', JSON.stringify(injection),{ headers }).toPromise();
      });
  }

  public updateSingleInjection(i: Injection): Promise<Object> {
      if (i == undefined) return;
    //   let  headers = new  HttpHeaders();
    //   headers.append('Content-Type', 'application/json');
      let  headers = new  HttpHeaders().set('Content-Type','application/json');
      //console.log("somebody using the updateSingleKitLink?");
      // link.forEach(link => {
          return this.http.put(this.url + 'update/' + (i.dbid) + '/', JSON.stringify(i), { headers }).toPromise();
      // });
  }

  public deleteInjection(i: Injection): Promise<Object> {
      if (i == undefined) return;
          return this.http.delete(this.url + 'delete/' + (i.dbid) + '/').toPromise();
  }
}
