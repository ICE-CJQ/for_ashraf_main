import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Equipment } from '../objects/Equipment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Ingredient } from 'app/shared/objects/Recipe';

// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';
@Injectable()
export class IngredientService {
    url: string = UrlService.URL + `/ingredient/`;

    constructor(private http: HttpClient) {
    }

    public loadIngredient(): Observable<Ingredient[]> {
        return this.http.get<Ingredient[]>(this.url + 'all/');
    }

    public searchIngredientsByRecipeid(recipeid:number): Observable<Ingredient[]> {
        return this.http.get<Ingredient[]>(this.url + `searchIngredientsByRecipeid/${recipeid}`);
    }

    public getIngredientWithId(id:number):  Observable<Ingredient[]>{
        return this.http.get<Ingredient[]>(this.url + `getByRecipeid/${id}`);
    }

    public addIngredient(i: Ingredient): Promise<Object> {
        if (i == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.post(this.url + 'add/', JSON.stringify(i), { headers }).toPromise();
    }

    public updateIngredient(i: Ingredient): Promise<Object> {
        if (i == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.put(this.url + 'update/' + (i.dbid) + '/', JSON.stringify(i), { headers }).toPromise();
    }

    public deleteIngredient(i: Ingredient): Promise<Object> {
        if (i == undefined) return;
        return this.http.delete(this.url + 'delete/' + (i.dbid) + '/').toPromise();
    }
}