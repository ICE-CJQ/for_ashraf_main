import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Equipment } from "../objects/Equipment";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Application } from "app/shared/objects/Product";
// import { AppComponent } from '../../app.component';
import { UrlService } from "app/shared/services/url.Service";
@Injectable()
export class ApplicationService {
  url: string = UrlService.URL + `/application/`;

  constructor(private http: HttpClient) {}
  public loadApplication(): Observable<Application[]> {
    return this.http.get<Application[]>(this.url + "all/");
  }

  // public searchIngredientsByRecipeid(recipeid:number): Observable<Ingredient[]> {
  //     return this.http.get(this.url + `searchIngredientsByRecipeid/${recipeid}`);
  // }

  public getApplicationByProductId(id: number): Observable<Application[]> {
    return this.http.get<Application[]>(
      this.url + `getApplicationsByProductdbid/${id}`
    );
  }

  public addApplication(a: Application): Promise<Object> {
    if (a == undefined) return;
    // let  headers = new  HttpHeaders();
    // headers.append('Content-Type', 'application/json');
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    return this.http
      .post(this.url + "add/", JSON.stringify(a), { headers })
      .toPromise();
  }

  public updateApplication(a: Application): Promise<Object> {
    if (a == undefined) return;
    // let  headers = new  HttpHeaders();
    // headers.append('Content-Type', 'application/json');
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    return this.http
      .put(this.url + "update/" + a.dbid + "/", JSON.stringify(a), { headers })
      .toPromise();
  }

  public deleteApplication(a: Application): Promise<Object> {
    if (a == undefined) return;
    return this.http.delete(this.url + "delete/" + a.dbid + "/").toPromise();
  }
}
