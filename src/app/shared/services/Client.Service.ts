import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ClientCompany, ClientContact, CompanyWContacts } from '../objects/Client';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';
@Injectable()
export class ClientService { 
    url: string = UrlService.URL + `/clientcompany/`;
    curl: string = UrlService.URL + `/clientcontact/`;
    constructor(private http: HttpClient) {
    }

    public loadClient(): Observable<ClientCompany[]> {
        return this.http.get<ClientCompany[]>(this.url + 'all/');
    }

    //new
    public loadClientByPage(page:number): Observable<ClientCompany[]> {
        return this.http.get<ClientCompany[]>(this.url + 'all/'+page);
    }

    public loadClientWContactByPage(page:number): Observable<CompanyWContacts[]> {
        return this.http.get<CompanyWContacts[]>(this.url + 'allwithContact/'+page);
    }

    public searchByPage(key:string, page:number, searchArea:string): Observable<CompanyWContacts[]> {
        if(searchArea == undefined || searchArea == '' || searchArea == 'All')
        return this.http.get<CompanyWContacts[]>(this.url + 'searchClient/'+key+'/'+page);
        if(searchArea == 'Company')
        return this.http.get<CompanyWContacts[]>(this.url + 'searchClientWName/'+key+'/'+page);
        if(searchArea == 'Ship')
        return this.http.get<CompanyWContacts[]>(this.url + 'searchClientWShipAd/'+key+'/'+page);
        if(searchArea == 'Bill')
        return this.http.get<CompanyWContacts[]>(this.url + 'searchClientWBillAd/'+key+'/'+page);
        if(searchArea == 'Contacts')
        return this.http.get<CompanyWContacts[]>(this.url + 'searchClientWContacts/'+key+'/'+page);
    }

    public count(): Observable<number> {
        return this.http.get<number>(this.url + 'count/');
    }
//end new

    public loadContact(): Observable<ClientContact[]> {
        return this.http.get<ClientContact[]>(this.curl + 'all/');
    }

    public getClientById(id:number): Observable<ClientCompany>{
        return this.http.get<ClientCompany>(this.url + `get/${id}`);
    }

    public getContactById(id:number): Observable<ClientContact>{
        return this.http.get<ClientContact>(this.curl + `get/${id}`);
    }

    public getContactByCompany(id:number): Observable<ClientContact[]>{
        return this.http.get<ClientContact[]>(this.curl + `getContactByCompany/${id}`);
    }

    public addClient(e: ClientCompany): Promise<Object> {
        if (e == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.post(this.url + 'add/', JSON.stringify(e), { headers }).toPromise()
    }

    public addContact(e: ClientContact): Promise<Object> {
        if (e == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.post(this.curl + 'add/', JSON.stringify(e), { headers }).toPromise()
    }

    public updateClient( e: ClientCompany): Promise<Object> {
        if (e == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.put(this.url + 'update/' + (e.dbid) + '/', JSON.stringify(e), { headers }).toPromise();
        // return true;
    }

    public updateContact( e: ClientContact): Promise<Object> {
        if (e == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.put(this.curl + 'update/' + (e.dbid) + '/', JSON.stringify(e), { headers }).toPromise();
        // return true;
    }

    public deleteClient(e: ClientCompany): Promise<Object> {
        if (e == undefined) return;
        return this.http.delete(this.url + 'delete/' + (e.dbid) + '/').toPromise();
    }

    public deleteContact(e: ClientContact): Promise<Object> {
        if (e == undefined) return;
        return this.http.delete(this.curl + 'delete/' + (e.dbid) + '/').toPromise();
    }
}