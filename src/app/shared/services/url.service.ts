import { Injectable } from '@angular/core';

@Injectable()
export class UrlService {

  constructor() { }

  public static URL: string = 'http://localhost:8085';
  // public static URL:string = 'http://192.168.0.2:8080';
  // public static URL:string = 'http://192.168.0.31:8080';

}
