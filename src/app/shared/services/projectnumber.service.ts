import { Injectable } from '@angular/core';
import { Projectnumber } from '../objects/Projectnumber';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';
@Injectable()
export class ProjectnumberService {
  url: string = UrlService.URL + `/projectnumber/`;

  constructor(private http: HttpClient) { }

  public loadProjectnumber(): Observable<Projectnumber[]> {
    return this.http.get<Projectnumber[]>(this.url + 'all/');
  }
}
