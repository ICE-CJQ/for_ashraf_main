import { Injectable } from '@angular/core';
import { Itemtype } from '../objects/item';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';

@Injectable()
export class ItemtypeService {
  url: string = UrlService.URL + `/itemtype/`;
  itemtypes: Itemtype[];

  constructor(private http: HttpClient) { }
  public loadItemtype(): Observable<Itemtype[]> {
    return this.http.get<Itemtype[]>(this.url + 'all/');
  }
}

