import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';
import { Transaction } from '../objects/Transaction';

@Injectable()
export class TransactionService {
  url: string = UrlService.URL + `/transation/`;

  constructor(private http: HttpClient) { }

  public addTransaction(trans:Transaction){
    if (trans == undefined) return;
    // let  headers = new  HttpHeaders();
    // headers.append('Content-Type', 'application/json');
    let  headers = new  HttpHeaders().set('Content-Type','application/json');
    return this.http.post(this.url + 'add/', JSON.stringify(trans), { headers }).toPromise();
  }

  public reverseTransaction(id:number):Promise<Object>{
    return this.http.delete(this.url + 'reverse/'+id).toPromise();
  }
}
