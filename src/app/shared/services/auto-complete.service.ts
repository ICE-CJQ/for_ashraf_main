import { Injectable } from "@angular/core";
import { ITEMS } from "./mock-data/mock-items";
import { Item } from "./mock-data/item";
@Injectable({
  providedIn: "root"
})
export class AutoCompleteService {
  items: Item[] = ITEMS;

  initSearchListArr(num: number) {
    let tempArr = new Array(num);
    for (let i = 0; i < num; i++) {
      let temp: Item[] = [];
      tempArr[i] = temp;
    }

    return tempArr;
  }

  iniInputDataArr(num: number) {
    let tempArr = new Array(num);
    for (let i = 0; i < num; i++) {
      let temp = {
        name: "",
        suppliers: [],
        lotNumbers: []
      };

      // this.autoInputDataArr.push(temp);
      tempArr[i] = temp;
    }

    return tempArr;
  }

  //search all matched results based up user's input for an input field, and return the matching results as an
  // array, which will be assigned to a particular search list
  search(inputFieldId: number, value: string, searchListArr): void {
    const filterValue = value.toLowerCase();

    switch (inputFieldId) {
      case 0:
        searchListArr[0] = this.items.filter(item =>
          item.name.toLowerCase().includes(filterValue)
        );
        break;

      case 1:
        searchListArr[1] = this.items.filter(item =>
          item.name.toLowerCase().includes(filterValue)
        );
        break;
      case 2:
        searchListArr[2] = this.items.filter(item =>
          item.name.toLowerCase().includes(filterValue)
        );
        break;
    }
  }

  setInputValues(setId: number, searchTerm: string, autoInputDataArr) {
    // find the user clicked item name from the items array
    let temp = this.items.find(
      item =>
        item.name.toLocaleLowerCase() === searchTerm.trim().toLocaleLowerCase()
    );

    //pull item name, supplier and lot number out of the found item above
    autoInputDataArr[setId].name = temp.name;
    autoInputDataArr[setId].suppliers = temp.suppliers;
    autoInputDataArr[setId].lotNumbers = temp.lotNumbers;
  }

  //clear up an input field's search list after user choose an option from the list
  clearSearchListArr(setId, searchListArr) {
    searchListArr[setId] = [];
  }

  clearSupplierArr(setId, autoInputDataArr) {
    autoInputDataArr[setId].suppliers = [];
  }

  clearlotNumArr(setId, autoInputDataArr) {
    autoInputDataArr[setId].lotNumbers = [];
  }

  constructor() {}
}
