import { Injectable } from '@angular/core';
import { Concentrationunit } from '../objects/Concentrationunit';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';

@Injectable()
export class ConcentrationunitService {
  url: string = UrlService.URL + `/concentrationunit/`;
  concentrationunits: Concentrationunit[];
  constructor(private http: HttpClient) { }

  public loadConcentrationunit(): Observable<Concentrationunit[]> {
    return this.http.get<Concentrationunit[]>(this.url + 'all/');
  }

}




