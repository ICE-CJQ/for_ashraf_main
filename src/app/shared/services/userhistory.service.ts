import { Userhistory } from '../objects/Userhistory';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';


@Injectable()
export class UserhistoryService {
  url: string = UrlService.URL + `/userhistory/`;
  userhistories: Userhistory[];

  constructor(private http: HttpClient) { }

  public loadUserHistory(): Observable<Userhistory[]> {
      return this.http.get<Userhistory[]>(this.url + 'all/');
  }

  public loadUserHistoryByPeriod(period: string, page: number): Observable<Userhistory[]> {
      return this.http.get<Userhistory[]>(this.url + `Period/${period}/${page}`);
  }

  public getPeriodCount(period:string): Observable<number>{
      return this.http.get<number>(this.url + `count/${period}`);
  }

  public searchByNameAndPeriod(name: string, period:string, page: number): Observable<Userhistory[]> {
      //encode all white space and #
    //   name = name.replace(/\//g, 'slash');
    //   name = name.replace(/\s/g, 'whitespace');
    //   name = name.replace(/#/g, 'pong');
    //   name = name.replace(/%/g, 'percent');
    //   name = name.replace(/\?/g, 'questionmark');

    //   period = period.replace(/\s/g, 'whitespace');

    //   return this.http.get(this.url + 'searchByNameWPage/' + name + '/' + period + '/' +page);
      return this.http.get<Userhistory[]>(this.url + `searchByNameWPage/${name}/${period}/${page}`);
  }
  
  public getNameAndPeriodCount(name: string, period:string): Observable<number> {
      //encode all white space and #
    //   name = name.replace(/\//g, 'slash');
    //   name = name.replace(/\s/g, 'whitespace');
    //   name = name.replace(/#/g, 'pong');
    //   name = name.replace(/%/g, 'percent');
    //   name = name.replace(/\?/g, 'questionmark');
    //   period = period.replace(/\s/g, 'whitespace');
    //   return this.http.get(this.url + 'searchByNameCount/' + name + '/' + period + '/');
      return this.http.get<number>(this.url + `searchByNameCount/${name}/${period}`);
  }

  public addUserHistory(u: Userhistory): Promise<Object> {
      if (u == undefined) return;
      // let  headers = new  HttpHeaders();
      // headers.append('Content-Type', 'application/json');
      let  headers = new  HttpHeaders().set('Content-Type','application/json');
      return this.http.post(this.url + 'add/', JSON.stringify(u), { headers }).toPromise();
  }

}
