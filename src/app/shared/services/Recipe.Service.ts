import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Equipment } from '../objects/Equipment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Recipe } from 'app/shared/objects/Recipe';
// import { AppComponent } from '../../app.component';
import { UrlService } from 'app/shared/services/url.Service';
@Injectable()
export class RecipeService {
    url: string = UrlService.URL + `/recipe/`;
    ingredientUrl: string = UrlService.URL + `/ingredient/`;
    constructor(private http: HttpClient) {
    }

    public loadRecipe(): Observable<Recipe[]> {
        return this.http.get<Recipe[]>(this.url + 'all/');
    }

    public getCount(): Observable<number> {
        return this.http.get<number>(this.url + 'count/');
    }

    public loadRecipeByPage(page: number): Observable<Recipe[]> {
        return this.http.get<Recipe[]>(this.url + 'all/' + page);
    }

    public searchSingleRecipeByName(name:string): Observable<Recipe> {
        name = name.replace(/\//g, 'slash');
        name = name.replace(/\s/g, 'whitespace');
        name = name.replace(/#/g, 'pong');
        name = name.replace(/%/g, 'percent');
        name = name.replace(/\?/g, 'questionmark');
        return this.http.get<Recipe>(this.url + `searchSingleRecipeByName/${name}/`);
    }

    public searchSingleRecipeBySId(recipeid:string): Observable<Recipe> {
        return this.http.get<Recipe>(this.url + `searchSingleRecipeBySId/${recipeid}/`);
    }

    public searchSingleRecipeByDId(dbid:number): Observable<Recipe> {
        return this.http.get<Recipe>(this.url + `searchSingleRecipeByDId/${dbid}`);
    }

    public searchRecipe(searchKey: string, page: number, searchArea: string): Observable<Recipe[]> {
        searchKey = searchKey.replace(/\//g, 'slash');
        searchKey = searchKey.replace(/\s/g, 'whitespace');
        searchKey = searchKey.replace(/#/g, 'pong');
        searchKey = searchKey.replace(/%/g, 'percent');
        searchKey = searchKey.replace(/\?/g, 'questionmark');
        if (searchArea == undefined || searchArea == '' || searchArea == 'All')
            return this.http.get<Recipe[]>(this.url + 'search/' + searchKey + '/' + page);
        if (searchArea == 'Recipeid')
            return this.http.get<Recipe[]>(this.url + 'searchId/' + searchKey + '/' + page);
        if (searchArea == 'Name'){
            return this.http.get<Recipe[]>(this.url + 'searchName/' + searchKey + '/' + page);
        }
    }

    public searchRecipeCount(search: string,searchArea:string): Observable<number> {
        //encode all white space and #
        search = search.replace(/\//g, 'slash');
        search = search.replace(/\s/g, 'whitespace');
        search = search.replace(/#/g, 'pong');
        search = search.replace(/%/g, 'percent');
        search = search.replace(/\?/g, 'questionmark');
        if(searchArea == undefined || searchArea == '' || searchArea == 'All'){
            return this.http.get<number>(this.url + 'searchCount/' + search + '/');
        }
        if(searchArea == 'Recipeid'){
            return this.http.get<number>(this.url + 'searchIdCount/' + search + '/');
        }
        if(searchArea == 'Name'){
            return this.http.get<number>(this.url + 'searchNameCount/' + search + '/');
        }
    }

    public addRecipe(r: Recipe): Promise<Object> {
        if (r == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.post(this.url + 'add/', JSON.stringify(r), { headers }).toPromise();
    }

    public updateRecipe(r: Recipe): Promise<Object> {
        if (r == undefined) return;
        // let  headers = new  HttpHeaders();
        // headers.append('Content-Type', 'application/json');
        let  headers = new  HttpHeaders().set('Content-Type','application/json');
        return this.http.put(this.url + 'update/' + (r.dbid) + '/', JSON.stringify(r), { headers }).toPromise();
    }

    public deleteRecipe(r: Recipe): Promise<Object> {
        if (r == undefined) return;
        return this.http.delete(this.url + 'delete/' + (r.dbid) + '/').toPromise();
    }

    // public deleteRecipe(r: Recipe): Promise<Object> {
    //     if (r == undefined) return;
    //     this.http.delete(this.url + 'delete/' + (r.dbid) + '/').toPromise()
    //     .then(response=>{
            
    //     })
    //     .catch(err=>{
            
    //     });
    // }
}