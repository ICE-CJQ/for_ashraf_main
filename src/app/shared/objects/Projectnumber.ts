export class Projectnumber {
    constructor(
        public dbid: number,
        public projectnumber: string
    ) { }

    public static newProjectnumber(projectnumber: Projectnumber): Projectnumber {
        let p = new Projectnumber(projectnumber.dbid, projectnumber.projectnumber);
        return p;
    }
}