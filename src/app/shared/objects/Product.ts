export class Product {
    constructor(
        public dbid: number,
        public name: string,
        public type: string,
        public description: string,
        public cat:string,
        public oldcat:string,
        public unit: string,
        public host: string,
        public clonality: string,
        public isotype: string,
        public immunogen: string,
        public purification: string,
        public buffer: string,
        public specificity: string,
        public reconstitution: string,
        public storage: string,
        public unitsize:string,
        public unitprice:string,
        public comment:string,
        public packsize:string,
        public status:string,
        public enteredby:string,
        public reviewedby:string,
        public approvedby:string,
        public entertime:Date,
        public reviewtime:Date,
        public approvetime:Date,
        public reviewcomment:string,
        public approvecomment:string
    ) {}

    public static newProduct(p: Product): Product {
        let x = new Product(
            p.dbid,
            p.name,
            p.type,
            p.description,
            p.cat,
            p.oldcat,
            p.unit,
            p.host,
            p.clonality,
            p.isotype,
            p.immunogen,
            p.purification,
            p.buffer,
            p.specificity,
            p.reconstitution,
            p.storage,
            p.unitsize,
            p.unitprice,
            p.comment,
            p.packsize,
            p.status,
            p.enteredby,
            p.reviewedby,
            p.approvedby,
            p.entertime,
            p.reviewtime,
            p.approvetime,
            p.reviewcomment,
            p.approvecomment
        );
        return x;
    }
}

export class Application {
    constructor(
        public dbid: number,
        public productdbid: number,
        public purpose: string,
        public recommenconcentration: string,
        public comment: string,
        public note: string,
        public editby: string,
        public modify: Date
    ) { }

    public static newapplication(a: Application): Application {
        return new Application(
            a.dbid,
            a.productdbid,
            a.purpose,
            a.recommenconcentration,
            a.comment,
            a.note,
            a.editby,
            a.modify
        );
    }
}
