export class Equipment {
    constructor(
        public dbid: number,
        public id: string,
        public serial: string,
        public name: string,
        public manufactor: string,
        public model: string,
        public currentLocation: string,
        public comment: string,
        public lastModify: string,
    ) { }

    public static newEquipment(e: Equipment): Equipment {
        return new Equipment(e.dbid, e.id, e.serial, e.name, e.manufactor, e.model, e.currentLocation, e.comment, e.lastModify);
    }
}