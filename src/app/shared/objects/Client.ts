export class ClientCompany {
    constructor(
        public dbid: number,
        public company: string,
        public address: string,
        public address2: string,
        public city: string,
        public province: string,
        public country: string,
        public postalCode: string,
        public baddress: string,
        public baddress2: string,
        public bcity: string,
        public bprovince: string,
        public bcountry: string,
        public bpostalCode: string,
        public editby: string,
        public modify: Date
    ) { }

    public static newCompany(c: ClientCompany): ClientCompany {
        return new ClientCompany(c.dbid, c.company, c.address, c.address2, c.city, c.province, c.country, c.postalCode, c.baddress, c.baddress2, c.bcity, c.bprovince, c.bcountry, c.bpostalCode, c.editby, c.modify);
    }
}

export class ClientContact {
    constructor(
        public dbid: number,
        public companyId: number,
        public name: string,
        public role: string,
        public email: string,
        public phone: string,
        public ext: string,
        public fax: string,
        public editby: string,
        public modify: Date
    ) { }

    public static newContact(c: ClientContact): ClientContact {
        return new ClientContact(c.dbid, c.companyId, c.name, c.role, c.email, c.phone, c.ext, c.fax, c.editby, c.modify);
    }
}

export class CompanyWContacts {
    constructor(
        public company: ClientCompany,
        public contacts: ClientContact[]) { }
}