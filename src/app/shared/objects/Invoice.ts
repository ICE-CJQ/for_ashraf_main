
export class Invoice {
    constructor(
        public dbid: number,
        public invoiceNum: number,
        public dateCreate: Date,
        public quoteId: number,
        public clientId: number,
        public revision: number,
        public revisionDate: Date,
        public billAttn: string,
        public billAddress: string,
        public shipAttn: string,
        public shipAddress: string,
        public poNum: string,
        public requisitioner: string,
        public courier: string,
        public paymentTerm: string,
        public shippingTerm: string,
        public shippingfee: number,
        public handleFee: number,
        public tax: number,
        public proform:string,
        public type:string,
        public note: string,
        public editby: string,
        public modify: Date,
        public currency:string,
        public currencyrate:number
    ) { }

    public static newInvoice(i: Invoice): Invoice {
        return new Invoice(i.dbid, i.invoiceNum, i.dateCreate, i.quoteId, i.clientId, i.revision, i.revisionDate, i.billAttn, i.billAddress, i.shipAttn, i.shipAddress, i.poNum, i.requisitioner, i.courier, i.paymentTerm, i.shippingTerm, i.shippingfee,i.handleFee, i.tax, i.proform,i.type, i.note, i.editby, i.modify, i.currency, i.currencyrate);
    }
}