import { Ingredient, Recipe } from "./Recipe";

export class buildkit {
    public recipe: Recipe;
    public ingredients: Ingredient[];
    constructor(
    ) {
        this.recipe = null;
        this.ingredients = [];
    }

}

