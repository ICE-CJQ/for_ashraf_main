export class Kit {
    constructor(
        public dbid: number,
        public cat: string,
        public name: string,
        public description: string,
        public clientspecific: string,
        public molecular: string,
        public biosimilar: string,
        public method: string,
        public size: string,
        public status: string,
        public price: string,
        public enteredby: string,
        public reviewedby: string,
        public reviewcomment: string,
        public approvedby: string,
        public approvecomment: string,
        public lastmodify: Date,
        public reviewtime: Date,
        public approvetime: Date,
        public editstatus: string,
        public comment: string) { }

    public static newKit(k: Kit) {
        let nkit = new Kit(k.dbid, k.cat, k.name, k.description, k.clientspecific, k.molecular, k.biosimilar, k.method, k.size, k.status, k.price, k.enteredby, k.reviewedby, k.reviewcomment, k.approvedby, k.approvecomment, k.lastmodify, k.reviewtime, k.approvetime, k.editstatus, k.comment);
        return nkit;
    }

    public static parseFromJSON(kit: { dbid: number, cat: string, name: string, description: string, clientspecific: string, molecular: string, biosimilar: string, method: string, size: string, status: string, type: string, price: string, enteredby: string, reviewedby: string, reviewcomment: string, approvedby: string, approvecomment: string, lastmodify: Date, reviewtime: Date, approvetime: Date,editstatus: string, comment: string }) {
        return new Kit(kit.dbid, kit.cat, kit.name, kit.description, kit.molecular, kit.clientspecific, kit.biosimilar, kit.method, kit.size, kit.status, kit.price, kit.enteredby, kit.reviewedby, kit.reviewcomment, kit.approvedby, kit.approvecomment, kit.lastmodify, kit.reviewtime, kit.approvetime, kit.editstatus, kit.comment);
    }
}

export class Kitcomponent {
    constructor(
        public dbid: number, //database id
        public kitid: number, // corresponding  kit.dbid
        public component: string,
        public componentid: string, // item.dbid  or recipe.dbid
        public reagent: string, // item.name or recipe.name
        //reagentDbid
        public recipename: string,
        public recipeid: string,
        //recipeDbid
        public amount: string,//need amount
        public unit: string,//need unit
        public iskit: string,
        public partnumber: string,
        public packaging,
        public vialid: string,
        public vialsize: string,
        public vialdescription: string,
        public editby: string,
        public modify: Date
    ) { }
}