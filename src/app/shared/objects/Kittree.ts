export class Kittree {
    constructor(
        public root:recipenode,
    ) {
        this.root=null;
     }

}

export class recipenode{
    constructor(
        public kitcat:string,
        public componentname:string,
        public recipename:string,
        public itemname:string,
        public supplier:string,
        public itemcat:string,
        public partnumber:string,
        public requireamount:number,
        public requireunit:string,
        public stockamount:number,
        public stockunit:string,
        public enough:string,
        public selected:string,
        public children:recipenode[]
    ){}

    // public static newrecipenode(r: recipenode) {
    //     let nrnode = new recipenode(r.kitname, r.componentname, r.recipename,r.itemname, r.requireamount, r.requireunit,r.stockamount,r.stockunit, r.children);
    //     return nrnode;
    // }
}

