export class Concentrationunit {
    constructor(
        public dbid: number,
        public concentrationunit: string
    ) { }

    public static newConcentrationunit(concentrationunit: Concentrationunit): Concentrationunit {
        let c = new Concentrationunit(concentrationunit.dbid, concentrationunit.concentrationunit);
        return c;
    }
}