import { item } from 'app/shared/objects/item';

export class orderitem {
    constructor(
        public dbid: number,
        public cat: string,
        public category: string,
        public suppliercat: string,
        public name: string,
        public type: string,
        public supplier: string,
        public manufacturer: string,
        public amount: string,
        public unreceiveamount:string,
        public unit: string,
        public unitsize: string,
        public unitprice: string,
        public grantid: string,
        public requestperson: string,
        public approveperson: string,
        public orderperson: string,
        public receiveperson: string,
        public status: string,
        public requesttime: Date,
        public approvetime:Date,
        public ordertime: Date,
        public receivetime: Date,
        public eta: Date,
        public emailsendtime: Date,
        public urgent: boolean,
        public comment:string,
        public reserve:string,
        public exist:string

    ) { }

    public static neworderitem(r: orderitem) {
        let newordertime = new orderitem(r.dbid, r.cat, r.category, r.suppliercat, r.name, r.type, r.supplier, r.manufacturer, r.amount, r.unreceiveamount, r.unit, r.unitsize, r.unitprice, r.grantid, r.requestperson, r.approveperson, r.orderperson, r.receiveperson, r.status, r.requesttime, r.approvetime, r.ordertime, r.receivetime, r.eta, r.emailsendtime, r.urgent, r.comment, r.reserve, r.exist);
        return newordertime;
    }

    // Will copy all non-inhertied properties except for the ones listed in the switch statement
    public static fromItem(item: item): orderitem {
      let order = new orderitem(-1, '', '', '', '', '', '', '', '', '', '', '', ' ', '', '', '', '', '', '', new Date(), null, null, null, null, null, false, '', '','No');
      for (var property in order) {
        let ignoreProperty = false;
        switch (property) {
          case "dbid":
          case "amount":
          case "comment":
            ignoreProperty = true;
        }
        if (ignoreProperty) {continue;}
        if (order.hasOwnProperty(property)) { //ignore inherited properties too
          if (item.hasOwnProperty(property)) {
            order[property] = item[property];
          }
        }
      }
      return order;
    }
}

// export class emailDetail{
//   constructor(
//     public emailAddressList: string[],
//     public comment: string,
//     public orderitem: orderitem
// ) { }
// }

export class emailDetail{
  constructor(
    public reason,
    public emailAddressList: string[],
    public subject:string,
    public emailContent:string
) { }
}
