export class Setting {
    constructor(
        public dbid: number,
        public page: string,
        public type: string,
        public value: string,
        public pending: string,
        public requestDel: string
    ) { }



    public static getSetting(page: string): Setting[] {
        switch (page) {
            case 'general': return this.getGeneralSetting();
            case 'invent': return this.getInventorySetting();
            case 'orderItem': return this.getOrderItemSetting();
            case 'kit': return this.getKitSetting();
            case 'sale': return this.getSaleSetting();
            case 'catalog': return this.getCatSetting();
            case 'invoice': return this.getCatSetting();
            // case 'product': return this.getProductSetting();
            // case 'application': return this.getApplicationSetting();
        }
    }

    // public static getProductSetting():Setting[]{
    //     let type=this.getProductType();
    //     let safety=this.getProductSafety();

    //     return [new Setting(-1,'product','type', ConfigValue.toDBValueLine(type), '', ''),
    //     new Setting(-1,'product','safety', ConfigValue.toDBValueLine(safety), '', '')]
    // }

    // public static getApplicationSetting():Setting[]{
    //     // let note=this.getApplicationNote();

    //     return [new Setting(-1,'application','note', ConfigValue.toDBValueLine(note), '', '')]
    // }

    //------------------------------------page setting----------------------------------------    
    public static getGeneralSetting(): Setting[] {
        let locationtype = this.getLocation();
        let sublocation = this.getSubLocation();
        let volUnit = this.getVolumnUnit();
        let massUnit = this.getMassUnit();
        let otherUnit = this.getOtherUnit();
        let conunit = this.getConUnit();


        return [new Setting(-1, 'general', 'location', ConfigValue.toDBValueLine(locationtype), '', ''),
        new Setting(-1, 'general', 'subLocation', ConfigValue.toDBValueLine(sublocation), '', ''),
        new Setting(-1, 'general', 'volUnit', ConfigValue.toDBValueLine(volUnit), '', ''),
        new Setting(-1, 'general', 'massUnit', ConfigValue.toDBValueLine(massUnit), '', ''),
        new Setting(-1, 'general', 'otherUnit', ConfigValue.toDBValueLine(otherUnit), '', ''),
        new Setting(-1, 'general', 'conUnit', ConfigValue.toDBValueLine(conunit), '', '')]
    }

    public static getInventorySetting(): Setting[] {
        let itemType = this.getItemType();
        let purification = this.getPurification();
        let cloneLevel = this.getClone();
        let host = this.getHost();
        let species = this.getSpecies();

        return [new Setting(-1, 'invent', 'itemType', ConfigValue.toDBValueLine(itemType), '', ''),
        new Setting(-1, 'invent', 'cloneType', ConfigValue.toDBValueLine(cloneLevel), '', ''),
        new Setting(-1, 'invent', 'purification', ConfigValue.toDBValueLine(purification), '', ''),
        new Setting(-1, 'invent', 'host', ConfigValue.toDBValueLine(host), '', ''),
        new Setting(-1, 'invent', 'species', ConfigValue.toDBValueLine(species), '', '')]
    }

    public static getOrderItemSetting(): Setting[] {
        let project = this.getProject();

        return [new Setting(-1, 'orderItem', 'projectList', ConfigValue.toDBValueLine(project), '', '')]
    }

    public static getKitSetting(): Setting[] {
        let kitstatus = this.getKitStatus();
        let kitsize = this.getPackSize();
        let vialdescription = this.getVial();
        let method = this.getKitType();
        let clientSpec = this.getClientSpec();
        let cloneLevel = this.getClone();
        let host = this.getHost();
        let species = this.getSpecies();

        return [new Setting(-1, 'kit', 'kitStatus', ConfigValue.toDBValueLine(kitstatus), '', ''),
        new Setting(-1, 'kit', 'kitMethod', ConfigValue.toDBValueLine(method), '', ''),
        new Setting(-1, 'kit', 'clientSpec', ConfigValue.toDBValueLine(clientSpec), '', ''),
        new Setting(-1, 'kit', 'vialList', ConfigValue.toDBValueLine(vialdescription), '', ''),
        new Setting(-1, 'kit', 'packSize', ConfigValue.toDBValueLine(kitsize), '', '')]
    }

    public static getSaleSetting(): Setting[] {
        let paymentTerm = this.getPaymentTerm();
        let incoTerm = this.getIncoTerm();
        let taxrate = this.getTaxRate();

        return [new Setting(-1, 'sale', 'paymentTerms', ConfigValue.toDBValueLine(paymentTerm), '', ''),
        new Setting(-1, 'sale', 'incoTerm', ConfigValue.toDBValueLine(incoTerm), '', ''),
        new Setting(-1, 'sale', 'tax', ConfigValue.toDBValueLine(taxrate), '', '')]
    }

    public static getCatSetting(): Setting[] {
        let type = this.getKitType();
        let cloneLevel = this.getClone();
        let clients = this.getClientSpec();
        let species = this.getSpecies();
        return [new Setting(-1, 'catalog', 'catType', ConfigValue.toDBValueLine(type), '', ''),
        new Setting(-1, 'catalog', 'cloneType', ConfigValue.toDBValueLine(cloneLevel), '', ''),
        new Setting(-1, 'catalog', 'focusClient', ConfigValue.toDBValueLine(clients), '', ''),
        new Setting(-1, 'catalog', 'species', ConfigValue.toDBValueLine(species), '', '')]
    }

    // public static getInvoiceSetting(): Setting[] {
    //     let invoiceoption = this.getOptionType();
    //     return [new Setting(-1, 'invoice', 'invoiceoption', ConfigValue.toDBValueLine(invoiceoption),'', '')]
    // }



    //--------------------------------setting default values----------------------------------
    public static isIndexMatter(type: string): boolean {
        switch (type) {
            case 'location':
            case 'volUnit':
            case 'massUnit':
            case 'otherUnit':
            case 'conUnit':
            case 'purification':
            case 'projectList':
            case 'kitStatus':
            case 'vialList':
            case 'packSize':
            case 'paymentTerms':
            case 'incoTerm':
            case 'tax':
            case 'titer':
                return false;
            case 'subLocation':
            case 'itemType':
            case 'cloneType':
            case 'host':
            case 'species':
            case 'kitMethod':
            case 'catType':
            case 'clientSpec':
            case 'focusClient':
            case 'type':
                return true;
        }
    }

    public static getNameOfType(type: string): string {
        switch (type) {
            case 'location':
                return 'Location';
            case 'subLocation':
                return 'Sub Location';
            case 'volUnit':
                return 'Volumn Unit';
            case 'massUnit':
                return 'Mass Unit';
            case 'otherUnit':
                return 'Other Unit';
            case 'conUnit':
                return 'Concentration Unit';
            case 'itemType':
                return 'Inventory Item Type';
            case 'cloneType':
                return 'Clonality Type';
            case 'purification':
                return 'Purification Method';
            case 'host':
                return 'Animal Host';
            case 'species':
                return 'Animal Species';
            case 'projectList':
                return 'Project Number List';
            case 'kitStatus':
                return 'Kit Development Status';
            case 'kitMethod':
            case 'catType':
                return 'Kit type';
            case 'clientSpec':
            case 'focusClient':
                return 'Specific Clients Name';
            case 'vialList':
                return 'Storage Vial List';
            case 'packSize':
                return 'Kit Pack Size';
            case 'paymentTerms':
                return 'Payment Term';
            case 'incoTerm':
                return 'Inco Term';
            case 'tax':
                return 'Tax Rate';
            case 'protype':
                return 'Product Type';
            case 'safety':
                return 'Product Safety';
            case 'note':
                return 'Product Note';
            /*NEW*/ 
            case 'titer':
                return 'Titer';
            case 'productPointer':
                return 'Product Current Identifer';
            case 'kitPointer':
                return 'Kit Current Identifer';
        }
    }

    public static getSettingByType(type: string) {
        switch (type) {
            case 'location':
                return this.getLocation();
            case 'subLocation':
                return this.getSubLocation();
            case 'volUnit':
                return this.getVolumnUnit();
            case 'massUnit':
                return this.getMassUnit();
            case 'otherUnit':
                return this.getOtherUnit();
            case 'conUnit':
                return this.getConUnit();
            case 'itemType':
                return this.getItemType();
            case 'cloneType':
                return this.getClone();
            case 'purification':
                return this.getPurification();
            case 'host':
                return this.getHost();
            case 'species':
                return this.getSpecies();
            case 'projectList':
                return this.getProject();
            case 'kitStatus':
                return this.getKitStatus();
            case 'kitMethod':
            case 'catType':
                return this.getKitType();
            case 'clientSpec':
            case 'focusClient':
                return this.getClientSpec();
            case 'vialList':
                return this.getVial();
            case 'packSize':
                return this.getPackSize();
            case 'paymentTerms':
                return this.getPaymentTerm();
            case 'incoTerm':
                return this.getIncoTerm();
            case 'tax':
                return this.getTaxRate();
        }
    }

    public static getStartCat(): ConfigValue {
        return { id: 0, name: '100' };
    }

    public static getTaxRate(): ConfigValue[] {
        return [{ id: 1, name: 'AB - 5%' },
        { id: 2, name: 'BC - 12%' },
        { id: 3, name: 'MB - 13%' },
        { id: 4, name: 'NB - 15%' },
        { id: 4, name: 'NL - 15%' },
        { id: 5, name: 'NT - 5%' },
        { id: 6, name: 'NS - 15%' },
        { id: 7, name: 'NU - 5%' },
        { id: 8, name: 'ON - 13%' },
        { id: 9, name: 'PE - 15%' },
        { id: 10, name: 'QC - 14.975%' },
        { id: 11, name: 'SK - 11%' },
        { id: 12, name: 'YT - 5%' }]
    }

    public static getPaymentTerm(): ConfigValue[] {
        return [{ id: 1, name: 'Prepayment' },
        { id: 2, name: 'Net 30' }];
    }

    public static getIncoTerm(): ConfigValue[] {
        return [{ id: 1, name: 'EXW' },
        { id: 2, name: 'FCA' },
        { id: 3, name: 'FAS' },
        { id: 4, name: 'FOB' },
        { id: 5, name: 'CFR' },
        { id: 6, name: 'CIF' },
        { id: 7, name: 'CPT' },
        { id: 8, name: 'CIP' },
        { id: 9, name: 'DAT' },
        { id: 10, name: 'DAP' },
        { id: 11, name: 'DDP' }];
    }

    public static getPackSize(): ConfigValue[] {
        return [{ id: 0, name: "2 x 96 wells" },
        { id: 1, name: "5 x 96 wells" },
        { id: 2, name: "10 x 96 wells" }]
    }

    public static getPurification(): ConfigValue[] {
        return [{ id: 0, name: 'Affinity Purification' },
        { id: 1, name: 'Melon gel Purification' }];
    }

    public static getKitStatus(): ConfigValue[] {
        return [{ id: 0, name: 'Qualified with innovator' },
        { id: 1, name: 'Validated with innovator' },
        { id: 2, name: 'Development in progress' },
        { id: 3, name: 'Developed (kit ready but not qualified)' }];
    }

    public static getVial(): ConfigValue[] {
        return [{ id: 0, name: 'narrow mouth' },
        { id: 1, name: 'white bottle' },
        { id: 2, name: 'polypropylene' },
        { id: 3, name: 'wide mouth' },
        { id: 4, name: 're-labelled reagent' },
        { id: 5, name: 'N/A' }];
    }

    public static getVolumnUnit(): ConfigValue[] {
        return [{ id: 0, name: "µL" },
        { id: 1, name: "mL" },
        { id: 2, name: "L" }]
    }

    public static getMassUnit(): ConfigValue[] {
        return [{ id: 0, name: "pg" },
        { id: 1, name: "ng" },
        { id: 2, name: "µg" },
        { id: 3, name: "mg" },
        { id: 4, name: "g" },
        { id: 5, name: "kg" }]
    }

    public static getOtherUnit(): ConfigValue[] {
        return [{ id: 0, name: "plate(s)" },
        { id: 1, name: "unit(s)" },
        { id: 2, name: "case(s)" },
        { id: 3, name: "pack(s)" },
        { id: 4, name: "carton(s)" },
        { id: 5, name: "bag(s)" },
        { id: 6, name: "box(es)" },
        { id: 7, name: "prep(s)" },
        { id: 8, name: "each" },
        { id: 9, name: "vial(s)" },
        { id: 10, name: "bottle(s)" },
        { id: 11, name: "flask(s)" },
        { id: 12, name: "mmol" },
        { id: 13, name: "mol" },
        { id: 14, name: "oz" },
        { id: 15, name: "lb" },
        { id: 16, name: "fl oz" },
        { id: 19, name: "gal" }]
    }

    public static getConUnit(): ConfigValue[] {
        return [{ id: 0, name: 'pg/μL' },
        { id: 1, name: 'pg/mL' },
        { id: 2, name: 'pg/L' },
        { id: 3, name: 'ng/μL' },
        { id: 4, name: 'ng/mL' },
        { id: 5, name: 'ng/L' },
        { id: 6, name: 'μg/μL' },
        { id: 7, name: 'μg/mL' },
        { id: 8, name: 'μg/L' },
        { id: 9, name: 'mg/μL' },
        { id: 10, name: 'mg/mL' },
        { id: 11, name: 'mg/L' },
        { id: 12, name: 'g/μL' },
        { id: 13, name: 'g/mL' },
        { id: 14, name: 'g/L' }]
    }

    public static getProject(): ConfigValue[] {
        return [{ id: 0, name: 'A.General supplies' },
        { id: 1, name: 'B.Reagents to fill order' },
        { id: 2, name: 'C.Regents kit dev' },
        { id: 3, name: 'D.Reagents Antibody technology dev.' },
        { id: 4, name: 'E.Reagents for other R&D' },
        { id: 5, name: 'F.Intas' },
        { id: 6, name: 'G. AIF' }]
    }

    public static getLocation(): ConfigValue[] {
        return [{ id: 0, name: "-80 Freezer #1" },
        { id: 1, name: "-80 Freezer #2" },
        { id: 2, name: "-20 Freezer #1" },
        { id: 3, name: "-20 Freezer #2" },
        { id: 4, name: "-20 Freezer #3" },
        { id: 5, name: "-20 Freezer #4" },
        { id: 6, name: "-20 Freezer SOM046" },
        { id: 7, name: "Fridge #1" },
        { id: 8, name: "Fridge #2" },
        { id: 9, name: "Fridge #3" },
        { id: 10, name: "Fridge #4" },
        { id: 11, name: "Fridge #5" },
        { id: 12, name: "Fridge SOM088" },
        { id: 13, name: "Lab shelf" },
        { id: 14, name: "Fume Hood" },
        { id: 15, name: "Packing bench" },
        { id: 16, name: "Reagent shelf" },
        { id: 17, name: "Bench #1" },
        { id: 18, name: "Bench #2" },
        { id: 19, name: "Bench #3" },
        { id: 20, name: "Bench #4" },
        { id: 21, name: "Bench #5" },
        { id: 22, name: "Bench #6" },
        { id: 23, name: "Bench #7" },
        { id: 24, name: "Bench #8" },
        { id: 25, name: "Bench #9" },
        { id: 26, name: "Bench #10" }]
    }

    public static getSubLocation(): ConfigValue[] {
        return [{ id: 0, name: "R&S Systems Box" },
        { id: 0, name: "Abcam Box" },
        { id: 0, name: "AbD Serotec Box" },
        { id: 0, name: "Alpha Diagnostics Box" },
        { id: 0, name: "Amplatto Box" },
        { id: 0, name: "Cedarlane Box" },
        { id: 0, name: "Somru Conjugates Box" },
        { id: 0, name: "Drg INTM Box" },
        { id: 0, name: "Fitzgerald Box" },
        { id: 0, name: "Jackson Box" },
        { id: 0, name: "Miscellaneous Box" },
        { id: 0, name: "Nanocs Box" },
        { id: 0, name: "Peprotech Box" },
        { id: 0, name: "Prospec Box" },
        { id: 0, name: "Sino Biological Box" },
        { id: 0, name: "MabTech Box" },
        { id: 0, name: "Shelf 2" },
        { id: 0, name: "Purified Antibodies Box" },
        { id: 1, name: "Shelf 3" },
        { id: 2, name: "Door" },
        { id: 2, name: "R&S Systems Box" },
        { id: 2, name: "Abcam Box" },
        { id: 2, name: "AbD Serotec Box" },
        { id: 2, name: "Alpha Diagnostics Box" },
        { id: 2, name: "Amplatto Box" },
        { id: 2, name: "Cedarlane Box" },
        { id: 2, name: "Somru Conjugates Box" },
        { id: 2, name: "Drg INTM Box" },
        { id: 2, name: "Fitzgerald Box" },
        { id: 2, name: "Jackson Box" },
        { id: 2, name: "Miscellaneous Box" },
        { id: 2, name: "Nanocs Box" },
        { id: 2, name: "Peprotech Box" },
        { id: 2, name: "Prospec Box" },
        { id: 2, name: "Sino Biological Box" },
        { id: 2, name: "MabTech Box" },
        { id: 2, name: "Purified Antibodies Box" },
        { id: 3, name: "Shelf 3" },
        { id: 3, name: "Shelf 4" },
        { id: 4, name: "Shelf 5" },
        { id: 4, name: "Shelf 6" },
        { id: 5, name: "Shelf 7" },
        { id: 5, name: "Shelf 8" },
        { id: 6, name: "Shelf 9" },
        { id: 6, name: "Shelf 10" },
        { id: 7, name: "Door" },
        { id: 7, name: "R&S Systems Box" },
        { id: 7, name: "Abcam Box" },
        { id: 7, name: "AbD Serotec Box" },
        { id: 7, name: "Alpha Diagnostics Box" },
        { id: 7, name: "Amplatto Box" },
        { id: 7, name: "Cedarlane Box" },
        { id: 7, name: "Somru Conjugates Box" },
        { id: 7, name: "Drg INTM Box" },
        { id: 7, name: "Fitzgerald Box" },
        { id: 7, name: "Jackson Box" },
        { id: 7, name: "Miscellaneous Box" },
        { id: 7, name: "Nanocs Box" },
        { id: 7, name: "Peprotech Box" },
        { id: 7, name: "Prospec Box" },
        { id: 7, name: "Sino Biological Box" },
        { id: 7, name: "MabTech Box" },
        { id: 7, name: "General Box" },
        { id: 8, name: "Door" },
        { id: 8, name: "Veggie Drawer" },
        { id: 9, name: "Purified Antibodies Box" },
        { id: 9, name: "Precipitated hIgG Depleted only Box" },
        { id: 10, name: "Level 7" },
        { id: 10, name: "Level 8" },
        { id: 11, name: "Top" },
        { id: 11, name: "Middle" },
        { id: 12, name: "Middle" },
        { id: 12, name: "Top" },
        { id: 12, name: "Level 1" },
        { id: 12, name: "Level 2" },
        { id: 12, name: "Level 5" },
        { id: 12, name: "Level 6" }]


    }

    public static getItemType(): ConfigValue[] {
        return [{ id: 0, name: "Antibody" },
        { id: 1, name: "Anti-Sera" },
        { id: 2, name: "Buffer and Solution" },
        { id: 3, name: "Cell Line" },
        { id: 4, name: "Chemical" },
        { id: 5, name: "Consumable" },
        { id: 6, name: "Drug" },
        { id: 7, name: "Kit" },
        { id: 8, name: "Matrix" },
        { id: 9, name: "Protein and Enzyme" }];
    }

    public static getProductType(): ConfigValue[] {
        return [{ id: 0, name: "Antibody" },
        { id: 1, name: "Anti-Sera" },
        { id: 2, name: "Buffer and Solution" },
        { id: 3, name: "Cell Line" },
        { id: 4, name: "Chemical" },
        { id: 5, name: "Consumable" },
        { id: 6, name: "Drug" },
        { id: 7, name: "Kit" },
        { id: 8, name: "Matrix" },
        { id: 9, name: "Protein and Enzyme" },
        { id: 10, name: "Misc" },
        { id: 11, name: "Associated kit component" },
        { id: 12, name: "Software" },
        { id: 13, name: "Reagent" }];
    }

    public static getSpecies(): ConfigValue[] {
        return [{ id: 0, name: 'Human' },
        { id: 1, name: 'Mouse' },
        { id: 2, name: 'Rat' },
        { id: 3, name: 'Monkey' },
        { id: 4, name: 'Chicken' },
        { id: 5, name: 'Donkey' },
        { id: 6, name: 'Goat' },
        { id: 7, name: 'Rabbit' }];
    }

    public static getHost(): ConfigValue[] {
        return [{ id: 0, name: 'Chicken' },
        { id: 1, name: 'Rabbit' },
        { id: 2, name: 'Rat' },
        { id: 3, name: 'Goat' },
        { id: 4, name: 'Sheep' },
        { id: 5, name: 'Mouse' },
        { id: 6, name: 'Hamster' }];
    }

    public static getKitType(): ConfigValue[] {
        return [{ id: 6, name: 'ADA' },
        { id: 7, name: 'PK' },
        { id: 0, name: 'Antibody' },
        { id: 3, name: 'Cell Line' },
        { id: 4, name: 'Protein' },
        { id: 5, name: 'Misc' },
        { id: 8, name: 'Method Transfer' },
        { id: 9, name: 'Charecterization kit' },
        { id: 10, name: 'Biomaker' }];
    }

    public static getClone(): ConfigValue[] {
        return [{ id: 1, name: 'monoclonal' },
        { id: 2, name: 'polyclonal' }];
    }

    public static getClientSpec(): ConfigValue[] {
        return [{ id: 0, name: 'Celerion' },
        { id: 5, name: 'Intas' }];
    }
}

export class ConfigValue {
    constructor(
        public id: number,
        public name: string
    ) { }

    public static toConfigArray(valueLine: string): ConfigValue[] {
        let list = [];
        if (valueLine == undefined || valueLine.trim() == '') return [];

        let record = valueLine.split(';');
        record.forEach(v => {
            if (v !== undefined && v.trim() !== '') {
                let val = v.split('-');
                let id = val[0];
                let name = val[1];
                if (val.length > 2) {
                    name += '-';
                    for (let i = 2; i < val.length; i++) {
                        name += val[i];
                    }
                }
                list.push(new ConfigValue(Number(id), name));
            }
        })

        return list
    }

    public static toStringArray(valueLine: string): string[] {
        let list = [];
        if (valueLine == undefined || valueLine.trim() == '') return [];

        let record = valueLine.split(';');
        record.forEach(v => {
            if (v !== undefined && v.trim() !== '') {
                let val = v.split('-');
                let id = val[0];
                let name = val[1];
                if (val.length > 2) {
                    name += '-';
                    for (let i = 2; i < val.length; i++) {
                        name += val[i];
                    }
                }
                list.push(name);
            }
        })

        return list
    }

    public static toDBValueLine(valuelist: ConfigValue[]): string {
        let linevalue = '';

        if (valuelist == undefined || valuelist.length < 1) return "";

        valuelist.forEach(val => {
            linevalue += val.id + '-' + val.name + ';'
        });

        return linevalue;
    }
}