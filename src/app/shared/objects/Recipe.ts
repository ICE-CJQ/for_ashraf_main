
export class Recipe {

    constructor(
        public dbid: number,
        public somruid: string,
        public name: string,
        public volumn: string,
        public unit: string,
        public description: string,
        public direction: string,
        public enteredby: string,
        public reviewedby: string,
        public approvedby: string,
        public entertime: Date,
        public reviewtime: Date,
        public approvetime: Date,
        public reviewcomment: string,
        public approvecomment: string,
        public editstatus: string) {
    }

    public static newRecipe(r: Recipe) {
        let nrecipe = new Recipe(r.dbid, r.somruid, r.name, r.volumn, r.unit, r.description, r.direction, r.enteredby, r.reviewedby, r.approvedby, r.entertime, r.reviewtime, r.approvetime,  r.reviewcomment, r.approvecomment, r.editstatus);
        return nrecipe;
    }

    public static parseFromJSON(recipe: { dbid: number, somruid: string, name: string, volumn: string, unit: string, description: string, direction: string, enteredby: string, reviewedby: string, approvedby: string, entertime: Date, reviewtime: Date, approvetime: Date, reviewcomment: string, approvecomment: string, editstatus: string }) {
        return new Recipe(recipe.dbid, recipe.somruid, recipe.name, recipe.volumn, recipe.unit, recipe.description, recipe.direction, recipe.enteredby, recipe.reviewedby, recipe.approvedby, recipe.entertime, recipe.reviewtime, recipe.approvetime, recipe.reviewcomment,  recipe.approvecomment, recipe.editstatus);
    }
}

export class Ingredient {
    constructor(
        public dbid: number,
        public recipeid: number,
        public itemname: string,
        public recipename: string,
        public recipesomruid: string,
        public unitquan: string,
        public unitmeasuretype: string,
        public reqquan: string,
        public requnit: string,
        public vendor: string,
        public cat: string,
        public suppliercat: string,
        public editby: string,
        public modify: Date
    ) { }

    public static NewIngredient(i: Ingredient) {
        return new Ingredient(i.dbid, i.recipeid, i.itemname, i.recipename, i.recipesomruid, i.unitquan, i.unitmeasuretype, i.reqquan, i.requnit, i.vendor, i.cat, i.suppliercat, i.editby, i.modify);
    }
}
