export class Userhistory {
    constructor(
        public dbid: number,
        public time: Date,
        public username: string,
        public component: string,
        public action: string,
        public description: string
    ) { }

    public static newhistory(h: Userhistory): Userhistory {
        return new Userhistory(h.dbid, h.time, h.username, h.component, h.action, h.description);
    }
}