export class Sublocation {
    constructor(
        public dbid: number,
        public locationid: string,
        public sublocationname: string
    ) { }

    public static newSublocation(sublocation: Sublocation): Sublocation {
        let s = new Sublocation(sublocation.dbid, sublocation.locationid, sublocation.sublocationname);
        return s;
    }
}