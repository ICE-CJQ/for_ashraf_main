export class item {

    public amount: number = 0;
    public inuse: number = 0;
    constructor(
        public dbid: number,
        public cat: string,
        public name: string,
        public type: string,
        public active: boolean,
        public clientspecific: string,
        public supplier: string,
        public manufacturer: string,
        public unit: string,
        public unitsize: string,
        public unitprice: string,
        public quantitythreshold: string,
        public supplylink: string,
        public manufacturerlink: string,
        public lastmodify: string,
        public comment: string

    ) {
        this.amount = 0;
        this.inuse = 0;
    }

    public static newItem(i: item): item {
        let x = new item(
            i.dbid,
            i.cat,
            i.name,
            i.type,
            i.active,
            i.clientspecific,
            i.supplier,
            i.manufacturer,
            i.unit,
            i.unitsize,
            i.unitprice,
            i.quantitythreshold,
            i.supplylink,
            i.manufacturerlink,
            i.lastmodify,
            i.comment
        );
        x.amount = i.amount
        x.inuse = i.inuse;
        return x;
    }
}

export class itemdetail {
    constructor(
        public dbid: number,
        public itemdbid: number,
        public itemname: string,
        public itemtype:string,
        public supplier:string,
        public parentlotnumber: string,
        public lotnumber: string,
        public receivedate: Date,
        public expiredate: Date,
        public location: string,
        public sublocation: string,
        public amount: string,
        public inuse: string,
        public concentration: string,
        public concentrationunit: string,
        public species:string,
        public clonality:string,
        public host:string,
        public conjugate:string,
        public iggdepletion:string,
        public purification:string,
        public volume:string,
        public volumeunit:string,
        public weight:string,
        public weightunit:string,
        public apcolumnprepdate:Date,
        public usagecolumn:string,
        public comment: string
    ) { }

    public static newitemdetail(i: itemdetail): itemdetail {
        return new itemdetail(
            i.dbid,
            i.itemdbid,
            i.itemname,
            i.itemtype,
            i.supplier,
            i.parentlotnumber,
            i.lotnumber,
            i.receivedate,
            i.expiredate,
            i.location,
            i.sublocation,
            i.amount,
            i.inuse,
            i.concentration,
            i.concentrationunit,
            i.species,
            i.clonality,
            i.host,
            i.conjugate,
            i.iggdepletion,
            i.purification,
            i.volume,
            i.volumeunit,
            i.weight,
            i.weightunit,
            i.apcolumnprepdate,
            i.usagecolumn,
            i.comment
        );
    }
}

export class Itemtype {
    constructor(
        public dbid: number,
        public itemtype: string
    ) { }
}

export class Recipe {

    constructor(
        public dbid: number,
        public somruid: string,
        public name: string,
        public volumn: string,
        public unit: string,
        public description: string,
        public direction: string,
        public enteredby: string,
        public reviewedby: string,
        public reviewcomment: string,
        public approvedby: string,
        public approvecomment: string,
        public editstatus: string) {
    }

    public static newRecipe(r: Recipe) {
        let nrecipe = new Recipe(r.dbid, r.somruid, r.name, r.volumn, r.unit, r.description, r.direction, r.enteredby, r.reviewedby, r.reviewcomment, r.approvedby, r.approvecomment, r.editstatus);
        return nrecipe;
    }

    public static parseFromJSON(recipe: { dbid: number, somruid: string, name: string, volumn: string, unit: string, description: string, direction: string, enteredby: string, reviewedby: string, reviewcomment: string, approvedby: string, approvecomment: string, editstatus: string }) {
        return new Recipe(recipe.dbid, recipe.somruid, recipe.name, recipe.volumn, recipe.unit, recipe.description, recipe.direction, recipe.enteredby, recipe.reviewedby, recipe.reviewcomment, recipe.approvedby, recipe.approvecomment, recipe.editstatus);
    }
}

export class Ingredient {
    constructor(
        public dbid: number,
        public recipeid: number,
        public itemname: string,
        public unitquan: string,
        public unitmeasuretype: string,
        public reqquan: string,
        public requnit: string,
        public vendor: string,
        public cat: string
    ) { }

    public static NewIngredient(i: Ingredient) {
        return new Ingredient(i.dbid, i.recipeid, i.itemname, i.unitquan, i.unitmeasuretype, i.reqquan, i.requnit, i.vendor, i.cat);
    }
}

export class Kit {
    constructor(
        public dbid: number,
        public cat: string,
        public name: string,
        public description: string,
        public clientspecific: string,
        public molecular: string,
        public biosimilar: string,
        public method: string,
        public size: string,
        public status: string,
        public price: string,
        public enteredby: string,
        public reviewedby: string,
        public reviewcomment: string,
        public approvedby: string,
        public approvecomment: string,
        public lastmodify: string,
        public editstatus: string,
        public comment: string) { }

    public static newKit(k: Kit) {
        let nkit = new Kit(k.dbid, k.cat, k.name, k.description, k.clientspecific, k.molecular, k.biosimilar, k.method, k.size, k.status, k.price, k.enteredby, k.reviewedby, k.reviewcomment, k.approvedby, k.approvecomment, k.lastmodify, k.editstatus, k.comment);
        return nkit;
    }

    public static parseFromJSON(kit: { dbid: number, cat: string, name: string, description: string, clientspecific: string, molecular: string, biosimilar: string, method: string, size: string, status: string, type: string, price: string, enteredby: string, reviewedby: string, reviewcomment: string, approvedby: string, approvecomment: string, lastmodify: string, editstatus: string, comment: string }) {
        return new Kit(kit.dbid, kit.cat, kit.name, kit.description, kit.molecular, kit.clientspecific, kit.biosimilar, kit.method, kit.size, kit.status, kit.price, kit.enteredby, kit.reviewedby, kit.reviewcomment, kit.approvedby, kit.approvecomment, new Date(kit.lastmodify).toString(), kit.editstatus, kit.comment);
    }
}

export class Kitcomponent {
    constructor(
        public dbid: number, //database id
        public kitid: number, // corresponding  kit.dbid
        public component: string,
        public componentid: string, // item.dbid  or recipe.dbid
        public reagent: string, // item.name or recipe.name
        //reagentDbid
        public recipename: string,
        public recipeid: string,
        //recipeDbid
        public amount: string,//need amount
        public unit: string,//need unit
        public iskit: string,
        public partnumber: string,
        public packaging,
        public vialid: string,
        public vialsize: string,
        public vialdescription: string
    ) { }
}

export class orderitem {
    constructor(
        public dbid: number,
        public cat: string,
        public name: string,
        public type: string,
        public supplier: string,
        public manufacturer: string,
        public amount: string,
        public unreceiveamount:string,
        public unit: string,
        public unitsize: string,
        public grantid: string,
        public requestperson: string,
        public status: string,
        public requesttime: string,
        public ordertime: string,
        public receivetime: string,
        public urgent: boolean,
        public comment:string

    ) { }

    public static neworderitem(r: orderitem) {
        let newordertime = new orderitem(r.dbid, r.cat, r.name, r.type, r.supplier, r.manufacturer, r.amount, r.unreceiveamount, r.unit, r.unitsize, r.grantid, r.requestperson, r.status, r.requesttime, r.ordertime, r.receivetime, r.urgent, r.comment);
        return newordertime;
    }
}

export class requestorderitem {
    constructor(
        public dbid: number,
        public cat: string,
        public name: string,
        public vendor: string,
        public amount: string,
        public unit: string,
        public grantid: string,
        public requestperson: string,
        public status: string,
        public requesttime: string,
        public urgent: boolean

    ) { }

    public static newrequestorderitem(r: requestorderitem) {
        let newrequestorderitme = new requestorderitem(r.dbid, r.cat, r.name, r.vendor, r.amount, r.unit, r.grantid, r.requestperson, r.status, r.requesttime, r.urgent);
        return newrequestorderitme;
    }
}

export class confirmedorderitem {
    constructor(
        public dbid: number,
        public cat: string,
        public name: string,
        public vendor: string,
        public orderamount: string,
        public unit: string,
        public grantid: string,
        public requestperson: string,
        public status: string,
        public ordertime: string,
        public receiveamount: string,
        public receivetime: string,
        public urgent: boolean

    ) { }

    public static newconfirmedorderitem(r: confirmedorderitem) {
        let newconfirmedorderitem = new confirmedorderitem(r.dbid, r.cat, r.name, r.vendor, r.orderamount, r.unit, r.grantid, r.requestperson, r.status, r.ordertime, r.receiveamount, r.receivetime, r.urgent);
        return newconfirmedorderitem;
    }
}