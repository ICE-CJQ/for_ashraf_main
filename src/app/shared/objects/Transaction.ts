export class Transaction {
    constructor(
        public dbid: number,
        public date: Date,
        public tableName: string,
        public action: string,
        public ids: number
    ) {
    }
}