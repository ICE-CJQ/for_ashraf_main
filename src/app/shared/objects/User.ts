export class User{
    constructor(
        public dbid:number,
        public name:string,
        public role:string,
        public email:string,
        public password:string,
        public invalidlogincount:number,
        public active:boolean
    ){}

    public static fromJson(user:{dbid:number, name:string, role:string, email:string, password:string, invalidlogincount:number, active:boolean}){
        return new User(user.dbid, user.name, user.role, user.email, user.password, user.invalidlogincount, user.active);
    }
}