export class Location {
    constructor(
        public dbid: number,
        public locationname: string
    ) { }

    public static newLocation(location: Location): Location {
        let c = new Location(location.dbid, location.locationname);
        return c;
    }
}