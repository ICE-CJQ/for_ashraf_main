export class Quote{
    constructor(
        public dbid:number,
        public quoteNum:string,
        public clientid:number,
        public saleRepid:number,
        public createDate:Date,
        public expireDate:Date,
        public paymentType:string,
        public revision:number,
        public revisionDate:Date,
        public shippingfee: number,
        public handlefee: number,
        public tax: number,
        public taxrate: string,
        public complete:boolean,
        public note: string,
        public editby: string,
        public modify: Date,
        public currency:string,
        public currencyrate:number
    ){}

    public static newQuote(quote:Quote){
        // //console.log(quote);
        return new Quote(quote.dbid, quote.quoteNum, quote.clientid, quote.saleRepid, quote.createDate, quote.expireDate, quote.paymentType, quote.revision, quote.revisionDate, quote.shippingfee, quote.handlefee, quote.tax, quote.taxrate, quote.complete, quote.note, quote.editby, quote.modify, quote.currency, quote.currencyrate)
    }
}

export class SaleLink{
    constructor(
        public dbid:number,
        public linkId:number,
        public linkType:string,
        public itemType:string,
        public itemId:number,
        public cat:string,
        public name:string,
        public price:number,
        public size:string,
        public itemDiscount:number,
        public itemQuan:number,
        public footnote:string,
        public editby: string,
        public modify: Date
    ){}

    public static newLink(l:SaleLink){
        return new SaleLink(l.dbid, l.linkId, l.linkType, l.itemType, l.itemId, l.cat, l.name, l.price, l.size, l.itemDiscount, l.itemQuan, l.footnote, l.editby, l.modify)
    }
}

export class revisionLog{
    constructor(
        public dbid:number,
        public quoteId:number,
        public revision:number,
        public change:string
    ){}
}