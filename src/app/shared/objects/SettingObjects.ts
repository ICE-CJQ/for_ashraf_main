export class Projectnumber {
    constructor(
        public dbid: number,
        public projectnumber: string
    ) { }

    public static newProjectnumber(projectnumber: Projectnumber): Projectnumber {
        let p = new Projectnumber(projectnumber.dbid, projectnumber.projectnumber);
        return p;
    }
}

export class Location {
    constructor(
        public dbid: number,
        public locationname: string
    ) { }

    public static newLocation(location: Location): Location {
        let c = new Location(location.dbid, location.locationname);
        return c;
    }
}

export class Sublocation {
    constructor(
        public dbid: number,
        public locationid: string,
        public sublocationname: string
    ) { }

    public static newSublocation(sublocation: Sublocation): Sublocation {
        let s = new Sublocation(sublocation.dbid, sublocation.locationid, sublocation.sublocationname);
        return s;
    }
}

export class Concentrationunit {
    constructor(
        public dbid: number,
        public concentrationunit: string
    ) { }

    public static newConcentrationunit(concentrationunit: Concentrationunit): Concentrationunit {
        let c = new Concentrationunit(concentrationunit.dbid, concentrationunit.concentrationunit);
        return c;
    }
}