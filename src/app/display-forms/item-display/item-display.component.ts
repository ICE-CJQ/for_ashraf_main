import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { item, Itemtype, itemdetail } from '../../shared/objects/item';
import { Location, Sublocation, Concentrationunit } from '../../shared/objects/SettingObjects';
import { InventoryService } from '../../shared/services/Inventory.Service';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { ItemdetailService } from '../../shared/services/itemdetail.service';
import { SettingService } from '../../shared/services/Setting.Service';
import { IMyDpOptions } from 'mydatepicker';
import { User } from '../../shared/objects/User';
import { AuthenticationService } from '../../shared/services/Authenticate.Service';
import { UserhistoryService } from '../../shared/services/userhistory.service';
import { EggtransferService } from '../../shared/services/eggtransfer.service';
import { Userhistory } from '../../shared/objects/Userhistory';
import { Eggtransfer } from '../../shared/objects/eggtransfer';


@Component({
  providers: [InventoryService, SettingService, ItemdetailService, UserhistoryService, EggtransferService],
  selector: 'app-item-display',
  templateUrl: './item-display.component.html',
  styleUrls: ['./item-display.component.css']
})

export class ItemDisplayComponent implements OnInit, OnChanges {
  @Input() item: item
  @Input() enableEdit: boolean;
  @Input() isNew: boolean;
  @Input() eggtransfer: Eggtransfer;

  @Output() updateItem = new EventEmitter();
  @Output() exit = new EventEmitter();
  @Output() cancelitem = new EventEmitter();


  currency: ConfigValue[]// = ["AUD","BDT","CAD","CNY","EUR","GBP","JPY","KRW","THB","USD","VND"];
  locationtype: ConfigValue[];
  volUnit: ConfigValue[];
  massUnit: ConfigValue[];
  otherUnit: ConfigValue[];
  conUnit: ConfigValue[];
  unit: ConfigValue[];
  itemCategory: ConfigValue[];
  displayItem: item;
  displayItemDetail: itemdetail;
  customeItemType: string;
  customeUnitType: string;
  customeLocationType: string;

  items: item[];
  itemdetails: itemdetail[] = [];
  allitemdetails: itemdetail[] = [];

  inputwarning: string[] = [];
  hidewarning = true;
  catunique = true;
  nameunique = true;
  missunitsize = false;
  specialunits = ['case(s)', 'pack(s)', 'carton(s)', 'bag(s)', 'box(es)', 'unit(s)', 'prep(s)', 'each', 'bottle(s)', 'flask(s)'];
  //cat config
  useExistCat: boolean = false;
  type: ConfigValue[];
  cloneLevel: ConfigValue[];
  clients: ConfigValue[];
  species: ConfigValue[];
  selectType: ConfigValue;
  selectClone: ConfigValue;
  isClientSpecific: boolean = false;
  selectClient: ConfigValue;
  selectSpecies: ConfigValue;
  nextNum: Setting;
  catalog: string;
  islockCat: boolean;

  locations: Location[] = [];
  sublocations: Sublocation[] = [];
  itemtypes: Itemtype[];
  concentrationunits: Concentrationunit[];

  expiredateoptions: IMyDpOptions = { dateFormat: 'mm/dd/yyyy' };
  expiredate: { date: { year: number, month: number, day: number } };
  earlyexpiredatemessage: string;

  originalitemtype='';
  originalitemname='';
  originalitemsupplier='';
  currentuser: User;

  constructor(private inventService: InventoryService, private setting: SettingService, private itemdetailservice: ItemdetailService, private authenticationservice: AuthenticationService, private userhistoryservice: UserhistoryService, private eggtransferservice: EggtransferService) {}

  ngOnInit() {
    let getcurrentuser = this.authenticationservice.getCurrentUser()
    if (getcurrentuser !== undefined) {
      getcurrentuser.then(_ => {
        this.currentuser = User.fromJson(_);
      });
    }
    this.loadSetting()
    this.inventService.loadInventory().subscribe(items => { 
      this.items = items; 
    });
    // this.locationservice.loadLocation().subscribe(locations => { this.locations = locations; });
    // this.itemtypeserivce.loadItemtype().subscribe(itemtypes => { this.itemtypes = itemtypes; });
    // this.concentrationunitservice.loadConcentrationunit().subscribe(concentrationunits => { this.concentrationunits = concentrationunits; });
    // this.itemdetailservice.loadItemDetail().subscribe(itemdetails => {this.allitemdetails = itemdetails;});
  }

  ngOnChanges(change: SimpleChanges) {
    if (change.item && this.item !== undefined) {
      this.originalitemtype=this.item.type;
      this.originalitemname=this.item.name;
      this.originalitemsupplier=this.item.supplier;
      this.reset();
    }
  }

  loadSetting() {
    this.setting.getSettingByPage('catalog').subscribe(config => {
      this.type = ConfigValue.toConfigArray(config.find(x => x.type == 'catType').value);
      if (this.type == []) this.type = Setting.getKitType();
      this.cloneLevel = ConfigValue.toConfigArray(config.find(x => x.type == 'cloneType').value);
      if (this.cloneLevel == []) this.cloneLevel = Setting.getClone();
      this.clients = ConfigValue.toConfigArray(config.find(x => x.type == 'focusClient').value);
      if (this.clients == []) this.clients = Setting.getClientSpec();
      this.species = ConfigValue.toConfigArray(config.find(x => x.type == 'species').value);
      if (this.species == []) this.species = Setting.getSpecies();
      this.nextNum = config.find(x => x.type == 'catPointer')
    });

    this.setting.getSettingByPage('general').subscribe(config => {
      this.unit = [];
      this.volUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'volUnit').value)
      if (this.volUnit == []) this.volUnit = Setting.getVolumnUnit()
      this.volUnit.forEach(v => { this.unit.push(v) })
      this.massUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'massUnit').value)
      if (this.massUnit == []) this.massUnit = Setting.getMassUnit()
      this.massUnit.forEach(m => { this.unit.push(m) })
      this.otherUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'otherUnit').value)
      if (this.otherUnit == []) this.otherUnit = Setting.getOtherUnit()
      this.otherUnit.forEach(o => { this.unit.push(o) });
    });

    this.setting.getSettingByPage('invent').subscribe(config => {
      this.itemtypes = [];
      let itemt = ConfigValue.toConfigArray(config.find(x => x.type == 'itemType').value)
      if (itemt == []) itemt = Setting.getItemType();
      itemt.forEach(l => {
        this.itemtypes.push(new Itemtype(l.id, l.name));
      });
      this.itemCategory = ConfigValue.toConfigArray(config.find(x => x.type == 'category').value)
    });
  }

  assignAndCheck(type: string, value: any) {
    if (value == undefined) return;
    switch (type) {
      case 'type':
        if (value.trim() == '') break;
        this.selectType = this.type.find(x => x.id == value);
        break;
      case 'clone':
        if (value.trim() == '') break;
        this.selectClone = this.cloneLevel.find(x => x.id == value);
        break;
      case 'isCP':
        this.isClientSpecific = value == 'Yes';
        break;
      case 'client':
        if (value.trim() == '') break;
        this.selectClient = this.clients.find(x => x.id == value);
        break;
      case 'species':
        if (value.trim() == '') break;
        this.selectSpecies = this.species.find(x => x.id == value);
        break;
    }
    this.generateCatalog();
  }

  isValid(): boolean {
    if (this.selectType == undefined || this.selectSpecies == undefined) {
      return false;
    }

    if (this.selectType.name == 'Antibody' && this.selectClone == undefined) {
      return false;
    }

    if (this.isClientSpecific && this.selectClient == undefined) {
      return false;
    }

    return true;
  }

  generateCatalog() {
    if (!this.isValid()) return;

    this.catalog = 'SB-0';

    if (this.isClientSpecific) {
      this.catalog += '' + this.selectClient.id;
    }
    if (this.selectType.name == 'Antibody') {
      this.catalog += '' + this.selectClone.id;
    }
    else {
      this.catalog += '' + this.selectType.id;
    }
    this.catalog += '-';
    this.catalog += '' + this.selectSpecies.id;
    this.catalog += '' + this.nextNum.value;
    this.displayItem.type = this.selectType.name;
  }

  setCat() {
    this.displayItem.cat = this.catalog;
    this.islockCat = true;
  }

  setManufacturer(){
    if(this.displayItem.manufacturer.toUpperCase().includes('SOMRU')){
      this.displayItem.manufacturer='Somru BioScience Inc.'
    }
  }

  checkinput(){
    let missinformation = false;
    this.catunique = true;
    this.nameunique = true;
    this.hidewarning = true;
    this.inputwarning = [];

    // if (this.displayItem.cat == undefined || !this.displayItem.cat || this.displayItem.cat.trim() == '') {
    //   this.inputwarning.push('Item catalog number can not be empty.');
    //   missinformation = true;
    //   this.hidewarning = false;
    // }
    if (this.displayItem.name == undefined || !this.displayItem.name || this.displayItem.name.trim() == '') {
      this.inputwarning.push('Item name can not be empty.');
      missinformation = true;
      this.hidewarning = false;
    }

    if (  (this.displayItem.supplier == undefined || !this.displayItem.supplier || this.displayItem.supplier.trim() == '')  && 
    (this.displayItem.manufacturer == undefined || !this.displayItem.manufacturer || this.displayItem.manufacturer.trim() == '')  ) {
      this.inputwarning.push('Item must have either supplier or manufacturer.');
      missinformation = true;
      this.hidewarning = false;
    }

    
    if (  (this.displayItem.cat == undefined || !this.displayItem.cat || this.displayItem.cat.trim() == '')  && 
    (this.displayItem.suppliercat == undefined || !this.displayItem.suppliercat || this.displayItem.suppliercat.trim() == '')  ) {
      this.inputwarning.push('Item must have either supplier catalog # or manufacturer catalog #.');
      missinformation = true;
      this.hidewarning = false;
    }

    if (this.displayItem.unit == undefined || !this.displayItem.unit || this.displayItem.unit.trim() == '') {
      this.inputwarning.push('Item unit can not be empty.');
      missinformation = true;
      this.hidewarning = false;
    }

    if (this.displayItem.type == undefined || !this.displayItem.type || this.displayItem.type.trim() == '') {
      this.inputwarning.push('Item type can not be empty.');
      missinformation = true;
      this.hidewarning = false;
    }

    // if (this.displayItem.unitsize == undefined || !this.displayItem.unitsize || this.displayItem.unitsize.trim() == '') {
    //   this.inputwarning.push('Item unitsize can not be empty.');
    //   missinformation = true;
    //   this.hidewarning = false;
    // }


    if (this.specialunits.indexOf(this.displayItem.unit) != -1 && !this.displayItem.unitsize) {
      this.inputwarning.push('You must choose unit size for case/pack/carton/bag/box/unit/prep/each/bottle/flask!');
      missinformation = true;
      this.hidewarning = false;
    }


      let j;
      for (j = 0; j < this.items.length; j++) {

        if (this.items[j].cat == this.displayItem.cat && this.displayItem.cat.trim() != '' && this.items[j].dbid != this.displayItem.dbid) {
          this.catunique = false;
          break;
        }
      }

      //if cat suppliercat unit and unitsize are or same, then name should not be different
      let i;
      for (i = 0; i < this.items.length; i++) {
        if(this.items[i].dbid != this.displayItem.dbid  && 
          this.items[i].cat==this.displayItem.cat &&  
          this.items[i].suppliercat==this.displayItem.suppliercat && 
          this.items[i].unit==this.displayItem.unit &&  
          this.items[i].unitsize==this.displayItem.unitsize ) {
          this.nameunique = false;
          break;
        }
      }

      if (this.catunique == false) {
        setTimeout(() => this.catunique = true, 5000);
      }

      if (this.nameunique == false) {
        setTimeout(() => this.nameunique = true, 5000);
      }

      if (missinformation == true) {
        setTimeout(() => this.hidewarning = true, 5000);
      }

      if (this.catunique && this.nameunique && missinformation == false) {
        this.haschanges()
      }

  }

  haschanges(){
    let haschange=false;
    if(this.displayItem.category!=this.item.category){haschange=true;}
    if(this.displayItem.cat!=this.item.cat){haschange=true;}
    if(this.displayItem.suppliercat!=this.item.suppliercat){haschange=true;}
    if(this.displayItem.name!=this.item.name){haschange=true;}
    if(this.displayItem.type!=this.item.type){haschange=true;}
    if(this.displayItem.active!=this.item.active){haschange=true;}
    if(this.displayItem.clientspecific!=this.item.clientspecific){haschange=true;}
    if(this.displayItem.supplier!=this.item.supplier){haschange=true;}
    if(this.displayItem.manufacturer!=this.item.manufacturer){haschange=true;}


    if(this.displayItem.unit!=this.item.unit){haschange=true;}
    if(this.displayItem.unitsize!=this.item.unitsize){haschange=true;}
    if(this.displayItem.unitprice!=this.item.unitprice){haschange=true;}
    if(this.displayItem.quantitythreshold!=this.item.quantitythreshold){haschange=true;}
    if(this.displayItem.supplylink!=this.item.supplylink){haschange=true;}
    if(this.displayItem.manufacturerlink!=this.item.manufacturerlink){haschange=true;}
    if(this.displayItem.comment!=this.item.comment){haschange=true;}

    this.enableEdit=false;

    if(haschange==true){this.save();}
  }
 

  save() {
    this.displayItem.lastmodify=new Date();
    this.displayItem.modifyperson=this.currentuser.name;
    if(this.displayItem.dbid==-1){
      this.inventService.addItem(this.displayItem).then(_=>{
        this.displayItem.dbid=_['dbid'];
        this.inputwarning = [];
        this.item = this.displayItem;
        if(this.eggtransfer){
          this.eggtransfer.destinationdbid=this.displayItem.dbid.toString();
          this.eggtransferservice.addEggtransfer(this.eggtransfer);
        }

        this.updateItem.emit({ titem: this.item});
        this.enableEdit = false;
        this.isNew = false;
        this.reset();
        let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Inventory', 'Create New Item', 'Item dbid: '+this.displayItem.dbid+', Item name: '+this.displayItem.name+', Item manufacturer cat#: '+this.displayItem.cat)
        this.userhistoryservice.addUserHistory(userhistory);
      })
    }
    else{
      this.inventService.updateItem(this.displayItem).then(()=>{
        let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Inventory', 'Update Item', 'Item dbid: '+this.displayItem.dbid+', Item name: '+this.displayItem.name+', Item manufacturer cat#: '+this.displayItem.cat)
        this.userhistoryservice.addUserHistory(userhistory);
        if(this.displayItem.type!=this.originalitemtype || this.displayItem.name!=this.originalitemname || this.displayItem.supplier!=this.originalitemsupplier){
          this.itemdetailservice.loadItemDetailByItemDbid(this.displayItem.dbid).subscribe(itemdetails=>{
            if(itemdetails.length>0){
              let updateitemtypes=[];
              itemdetails.forEach(itemdetail=>{
                itemdetail.itemtype=this.displayItem.type;
                itemdetail.itemname=this.displayItem.name;
                itemdetail.supplier=this.displayItem.supplier;
                updateitemtypes.push(this.itemdetailservice.updateSingleItemDetail(itemdetail));
              });
              Promise.all(updateitemtypes).then(result=>{
                this.inputwarning = [];
                this.item = this.displayItem;
                this.updateItem.emit({ titem: this.item,itemdetail:itemdetails});
                this.enableEdit = false;
                this.isNew = false;
                this.reset();
              });
            }
            else{
              this.inputwarning = [];
              this.item = this.displayItem;
              this.updateItem.emit({ titem: this.item});
              this.enableEdit = false;
              this.isNew = false;
              this.reset();
            }
          });
        }
        else{
            this.inputwarning = [];
            this.item = this.displayItem;
            this.updateItem.emit({ titem: this.item});
            this.enableEdit = false;
            this.isNew = false;
            this.reset();
        }
      })
    }

  }

  /* checkNum(val, tag){
    if(isNaN(val)) val = 0;
    val = Math.abs(val);

    solution 1
    switch(tag){
      case 'unitsize':
      this.displayitem.unitsize = val;
      break;
      case 'quantity':
      this.displayitem.quantity = val;
    }

    solution 2
    if(tag == 'unitsize') this.displayitem.unitsize = val;
  }
  */

  reset() {
    this.displayItem = item.newItem(this.item);
    // let e = new Date(this.item.expiredate);
    // this.expiredate = { date: { year: e.getFullYear(), month: e.getMonth() + 1, day: e.getDate() } };
    // this.isNew ? '' : this.enableEdit = false;
    this.customeItemType = '';
    this.customeUnitType = '';
    this.customeLocationType = '';
    if (this.isNew) {
      this.useExistCat = false;
    }
  }

  cancelItem(){
    this.cancelitem.emit();
  }


  // changeexpiredate(event) {
  //   let d = event.formatted;
  //   if (d == undefined) return;
  //   if (new Date(d) < new Date()) {
  //     this.earlyexpiredatemessage = "Please make sure the expire date is not earlier than today";
  //     setTimeout(_ => { this.earlyexpiredatemessage = undefined }, 5000);
  //     this.displayItem.expiredate = new Date();
  //   }
  //   else{
  //     this.displayItem.expiredate = new Date(d);
  //   }
  // }







}
