import { async, inject, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import {ChickenDisplayComponent} from './chicken-display.component'
import { Chicken, Injection } from '../../shared/objects/Chicken';
import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter,  ViewChild, ElementRef, SimpleChange  } from '@angular/core';
import { InventoryService } from 'app/shared/services/Inventory.Service';
import { ChickenService } from '../../shared/services/chicken.Service';
import { SettingService } from '../../shared/services/Setting.Service';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { User } from 'app/shared/objects/User';
import { AuthenticationService } from '../../shared/services/Authenticate.Service'
import { InjectionService } from '../../shared/services/injection.service';
import { EggService } from '../../shared/services/egg.service';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/of';
import 'zone.js/dist/async-test';
import 'zone.js/dist/fake-async-test';


fdescribe('Chicken-Display Component Integration Test',()=>{
    //Arrange
    let chickencomponent: ChickenDisplayComponent;
    let fixture: ComponentFixture<ChickenDisplayComponent>;
    let inventoryService: InventoryService;
    let chickenService: ChickenService;
    let injectionService: InjectionService; 
    let setting: SettingService ;
    let eggService: EggService;
    let authenticationservice: AuthenticationService;




    beforeEach(()=>{
        TestBed.configureTestingModule({
            imports: [ FormsModule, MyDatePickerModule, HttpClientModule, HttpClientTestingModule, HttpModule, NgbModule.forRoot()],
            declarations:[ChickenDisplayComponent],
            providers:[ChickenService, EggService, InjectionService, InventoryService, SettingService, AuthenticationService]
        });

        fixture =  TestBed.createComponent(ChickenDisplayComponent);
        chickencomponent=fixture.componentInstance;
        injectionService = TestBed.get(InjectionService); 
        inventoryService = TestBed.get(InventoryService);
        chickenService = TestBed.get(ChickenService);
        eggService = TestBed.get(EggService);
        setting = TestBed.get(SettingService);
        authenticationservice = TestBed.get(AuthenticationService);
        //fixture.detectChanges();
        //chickencomponent=new ChickenDisplayComponent(new InventoryService(),new ChickenService(),new InjectionService(),new SettingService(),new EggService(),new AuthenticationService(),new NgbModal())
        chickencomponent.currentuser={dbid:1, name:'Chen Fei', role:'Senior Manager', email:'chen.fei@somrubioscience.com', password:'123456'}
        //chickencomponent.chicken=new Chicken(6, '11-6', new Date('Jul/20/2018'), 'PLGF', 'AIF', 'Active', '18', '0', '0', '1:1000', 'Princess', new Date('Feb/11/2019'), 11.6, 'Active')
        //chickencomponent.ngOnInit();
        //let changes: SimpleChanges = {chicken: new SimpleChange(null, chickencomponent.chicken, false)};
        //chickencomponent.ngOnChanges(changes);
        //fixture.detectChanges();
    });


    // it('just for test, output should be negative if input is smaller than 0', ()=>{
    //     let result = chickencomponent.justfortest(-2)
    //     expect(result).toBe(-1);
    // })

    it('Add a New Chicken with Injections', (done)=>{
        chickencomponent.displayChicken= new Chicken(-1, '11-7', new Date(), 'TestImmunogen', 'AIF', 'Active', '0', '0', '0', null, 'Chen Fei', new Date(), 11.7, 'Active');
        let inject=new Injection(-1, '11-7', 'Freund’s Adjuvant, Complete', null, '400', 'µg','','Not Complete');
        chickencomponent.updatedInjections.push(inject);
        inject=new Injection(-1, '11-7', 'Freund’s Adjuvant, Incomplete', null, '200', 'µg','','Not Complete');
        chickencomponent.updatedInjections.push(inject);
        inject=new Injection(-1, '11-7', 'Freund’s Adjuvant, Incomplete', null, '200', 'µg','','Not Complete');
        chickencomponent.updatedInjections.push(inject);
        inject=new Injection(-1, '11-7', 'Freund’s Adjuvant, Incomplete', null, '200', 'µg','','Not Complete');
        chickencomponent.updatedInjections.push(inject);
        inject=new Injection(-1, '11-7', 'Freund’s Adjuvant, Incomplete', null, '200', 'µg','','Not Complete');
        chickencomponent.updatedInjections.push(inject);
        //let changes: SimpleChanges = {chicken: new SimpleChange(null, chickencomponent.chicken, false)};
        //chickencomponent.changes = {chicken: new SimpleChange(null, chickencomponent.chicken, false)};
        // chickencomponent.ngOnChanges(changes);
        // setTimeout(()=>{
            console.log('in test, displaychicken is')
            console.log(chickencomponent.displayChicken)
            
            // chickencomponent.updatedInjections.forEach(injection=>{
            //     injection.injectdate=new Date();
            // });
            //method 1 to test
            chickencomponent.updateChicken.subscribe(()=>{
                chickenService.getChickenByChickenId(chickencomponent.displayChicken.chickenid).subscribe(chicken=>{
                    console.log('the new added chicken is')
                    console.log(chicken)
                    expect(chicken.chickenid).toBe('11-7');
                    expect(chicken.immunogen).toBe('TestImmunogen');
                    expect(chicken.projectname).toBe('AIF');
                    expect(chicken.chickenstatus).toBe('Active');
                    expect(chicken.totalegg).toBe('0');
                    expect(chicken.eggused).toBe('0');
                    expect(chicken.eggdiscard).toBe('0');
                    expect(chicken.editby).toBe('Chen Fei');
                    expect(chicken.sequence).toBe(11.7);
                    expect(chicken.immunstatus).toBe('Active');     
                    injectionService.loadInjectionBychickenid(chickencomponent.displayChicken.chickenid).subscribe(injections=>{
                        expect(injections.length).toBe(5);
                        done(); 
                    })
                     
                              
                })
            })
            chickencomponent.save();   

            //method 2 to test
            //chickencomponent.save();   
            // setTimeout(()=>{
            //     chickenService.getChickenByChickenId(chickencomponent.displayChicken.chickenid).subscribe(chicken=>{
            //         console.log('the new added chicken is')
            //         console.log(chicken)
            //         //expect(updateChickenSpy).toHaveBeenCalled();
            //         expect(chicken.chickenid).toBe('11-8');
            //         expect(chicken.immunogen).toBe('TestImmunogen');
            //         expect(chicken.projectname).toBe('AIF');
            //         expect(chicken.chickenstatus).toBe('Active');
            //         expect(chicken.totalegg).toBe('0');
            //         expect(chicken.eggused).toBe('0');
            //         expect(chicken.eggdiscard).toBe('0');
            //         expect(chicken.editby).toBe('Chen Fei');
            //         expect(chicken.sequence).toBe(11.7);
            //         expect(chicken.immunstatus).toBe('Active');                 
            //     })
            // }, 3000);

            //method 3 to test
            //let updateChickenSpy = spyOn( chickencomponent.updateChicken, "emit").and.callThrough();
            //chickencomponent.save(); 
                        //     chickenService.getChickenByChickenId(chickencomponent.displayChicken.chickenid).subscribe(chicken=>{
            //         console.log('the new added chicken is')
            //         console.log(chicken)
            //         //expect(updateChickenSpy).toHaveBeenCalled();
            //         expect(chicken.chickenid).toBe('11-8');
            //         expect(chicken.immunogen).toBe('TestImmunogen');
            //         expect(chicken.projectname).toBe('AIF');
            //         expect(chicken.chickenstatus).toBe('Active');
            //         expect(chicken.totalegg).toBe('0');
            //         expect(chicken.eggused).toBe('0');
            //         expect(chicken.eggdiscard).toBe('0');
            //         expect(chicken.editby).toBe('Chen Fei');
            //         expect(chicken.sequence).toBe(11.7);
            //         expect(chicken.immunstatus).toBe('Active');                 
            //     })
                   
    })

    it('Edit an existing Chicken and Injections', (done)=>{
        chickenService.getChickenByChickenId('11-7').subscribe(chicken=>{
            chickencomponent.displayChicken=chicken;
            chickencomponent.displayChicken.immunogen='SecondTestImmunogen'
            injectionService.loadInjectionBychickenid('11-7').subscribe(injections=>{
                chickencomponent.updatedInjections=injections;
                chickencomponent.removeinjection(4);
                chickencomponent.removeinjection(3);
                chickencomponent.updatedInjections[0].complete='Complete';

                chickencomponent.updateChicken.subscribe(()=>{
                chickenService.getChickenByChickenId('11-7').subscribe(chicken=>{
                    expect(chicken.immunogen).toBe('SecondTestImmunogen');   
                    injectionService.loadInjectionBychickenid('11-7').subscribe(injections=>{
                        expect(injections.length).toBe(3);
                         expect(injections[0].complete).toBe('Complete');
                        done(); 
                    })
                     
                              
                })
            })
            chickencomponent.save();   

            })

        })









                   
    })

    // it('Add New Injection, Injections length should be 1', ()=>{
    //     //chickencomponent.displayChicken=new Chicken(6, '11-6', new Date('Jul/20/2018'), 'PLGF', 'AIF', 'Active', '18', '0', '0', '1:1000', 'Princess', new Date('Feb/11/2019'), 11.6, 'Active')
    //     chickencomponent.addNewInjection()
    //     expect(chickencomponent.updatedInjections.length).toBe(1);
    // })

    //     it('Remove an Injection, Injections length should be 0', ()=>{
    //     //chickencomponent.displayChicken=new Chicken(6, '11-6', new Date('Jul/20/2018'), 'PLGF', 'AIF', 'Active', '18', '0', '0', '1:1000', 'Princess', new Date('Feb/11/2019'), 11.6, 'Active')
    //     chickencomponent.removeinjection(0)
    //     expect(chickencomponent.updatedInjections.length).toBe(0);
    // })


    // it('It should complete the first injection of chicken 11-6', ()=>{
    //     setTimeout(() => {
    //         console.log('in test, injections are')
    //         console.log(chickencomponent.updatedInjections)
    //         chickencomponent.completeimmune(chickencomponent.updatedInjections[0], chickencomponent.chicken);
            
    //         setTimeout(()=>{
    //             console.log('in test after change, injections are')
    //             console.log(chickencomponent.updatedInjections);
    //             expect(chickencomponent.updatedInjections[0].complete).toBe('Complete');
    //         })
    //       });
    // })

    // it( 'Change Chicken 11-6 first injection to Complete', fakeAsync( () => {
    //     tick(5000);
    //         console.log('injections are')
    //         console.log(chickencomponent.updatedInjections)
    //         chickencomponent.completeimmune(chickencomponent.updatedInjections[0], chickencomponent.chicken);
    //     //chickencomponent.completeimmune(chickencomponent.updatedInjections[0], chickencomponent.chicken);
    //     tick(1000); // fast forward 10 milliseconds considering that emit would take 10 milliseconds 

    //     expect(chickencomponent.updatedInjections[0].complete).toBe('Complete');
    // } ) );

    // it( 'Change Chicken 11-6 first injection to Complete',  () => {
    //     fixture.detectChanges();
    //         chickencomponent.completeimmune(chickencomponent.updatedInjections[0], chickencomponent.chicken); 
    //         expect(chickencomponent.updatedInjections[0].complete).toBe('Complete');
    // } ) ;


    



})
