import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { item } from '../../shared/objects/item';
import { Recipe, Ingredient } from 'app/shared/objects/Recipe';
import { InventoryService } from '../../shared/services/Inventory.Service';
import { RecipeService } from '../../shared/services/Recipe.Service';
import { IngredientService } from '../../shared/services/Ingredient.Service';
import { User } from 'app/shared/objects/User';
import { AuthenticationService } from '../../shared/services/Authenticate.Service';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { SettingService } from '../../shared/services/Setting.Service';
import { ItemdetailService } from '../../shared/services/itemdetail.service';
import { UserhistoryService } from '../../shared/services/userhistory.service';
import { Userhistory } from '../../shared/objects/Userhistory';

@Component({
  providers: [InventoryService, RecipeService, IngredientService, SettingService, ItemdetailService, UserhistoryService],
  selector: 'app-recipe-display',
  templateUrl: './recipe-display.component.html',
  styleUrls: ['./recipe-display.component.css']
})
export class RecipeDisplayComponent implements OnInit, OnChanges {
  @Input() recipe: Recipe;
  @Input() enableEdit: boolean;
  @Input() isNew: boolean;

  @Output() updateRecipe = new EventEmitter();
  @Output() cancel = new EventEmitter();


  unitList: string[];
  volumeunits:string[];
  displayRecipe: Recipe;
  invent: item[];
  recipes: Recipe[];
  ingredient: Ingredient[];
  ingredients: Ingredient[];
  onfocusIndex: number;
  filterList: { index: number, dbid: number, cat: string, name: string }[];
  itemfilterList: { index: number, dbid: number, name: string, amount: number, unit: string, supplier: string, cat: string, suppliercat:string }[] = [];
  recipeFilterList: { index: number, dbid: number, recipename: string, recipeid:string }[];
  updatedsopingredients: Ingredient[] = [];

  inputwarning: string[] = [];
  hidewarning = true;
  finditem = true;
  findrecipe = true;
  idunique = true;
  nameunique = true;

  currentuser: User;
  currentusername: string = '';
  currentuserrole: string = '';

  reviewjectcomment: boolean = true;
  approvejectcomment: boolean = true;

  useExistCat: boolean = false;
  islockCat: boolean;
  currency: ConfigValue[]// = ["AUD","BDT","CAD","CNY","EUR","GBP","JPY","KRW","THB","USD","VND"];
  locationtype: ConfigValue[];
  unit: ConfigValue[];
  volUnit: ConfigValue[];
  massUnit: ConfigValue[];
  otherUnit: ConfigValue[];
  displayItem: item;
  customeItemType: string;
  customeUnitType: string;
  customeLocationType: string;

  constructor(private inventService: InventoryService, private recipeService: RecipeService, private ingredientService: IngredientService,
    private authenticationservice: AuthenticationService, private setting: SettingService, private idetailservice: ItemdetailService, private userhistoryservice: UserhistoryService) {
  }

  ngOnInit() {
    let getcurrentuser = this.authenticationservice.getCurrentUser()
    if (getcurrentuser !== undefined) {
      getcurrentuser.then(_ => {
        this.currentuser = User.fromJson(_);
        this.currentusername = this.currentuser.name;
        this.currentuserrole = this.currentuser.role;
      });
    }

    this.loadSetting();
    this.inventService.loadInventory().subscribe(invent => {
      this.invent = invent;
    });
    this.ingredientService.loadIngredient().subscribe(ingredients => {
      this.ingredients = ingredients;
    });

    this.recipeService.loadRecipe().subscribe(recipes => { this.recipes = recipes; });

  }

  ngOnChanges(change: SimpleChanges) {
    if (change.recipe && this.recipe !== undefined) {
      this.ingredientService.getIngredientWithId(this.recipe.dbid).subscribe(ingredient => {
        this.updatedsopingredients = ingredient;
      });
      this.reset()
    }
  }

  loadSetting() {
    this.setting.getSettingByPage('general').subscribe(config => {
      this.locationtype = ConfigValue.toConfigArray(config.find(x => x.type == 'location').value)
      if (this.locationtype == []) this.locationtype = Setting.getLocation();
      this.unit = [];
      this.volumeunits = [];
      this.volUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'volUnit').value)
      this.volUnit.forEach(v => { this.unit.push(v) })
      this.volUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'volUnit').value)
      this.volUnit.forEach(v => { this.unit.push(v); this.volumeunits.push(v.name) })
      this.massUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'massUnit').value)
      this.massUnit.forEach(m => { this.unit.push(m) })
      this.otherUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'otherUnit').value)
      this.otherUnit.forEach(o => { this.unit.push(o) })
    });
  }

  addNewIngredient() {
    this.updatedsopingredients.push(new Ingredient(-1, this.displayRecipe.dbid, '', '', '', '', '', '', this.unit[0].name, '', '','', this.currentuser.name, new Date()));
    this.enableEdit = true;
  }

  getFilter(ingredient: Ingredient, key: string) {
    if (key == undefined) { this.itemfilterList = []; return; }
    this.itemfilterList = [];
    if (key.trim() == '') {
      this.itemfilterList = [];
      ingredient.itemname = '';
    }
    else {
      this.invent.forEach((item, i) => {
        if (item.name.toUpperCase().includes(key.toUpperCase())) {
          this.itemfilterList.push({ index: i, dbid: item.dbid, name: item.name, amount: Number(item.amount), unit: item.unit, supplier: item.supplier, cat: item.cat, suppliercat:item.suppliercat });
        }
      });
    }

    if (this.itemfilterList.length < 1 && key.trim() != '') {
      ingredient.itemname = '';
      this.finditem = false;
      setTimeout(() => this.finditem = true, 5000);
    }
  }

  recipeFilter(ingredient: Ingredient, key: string) {
    if (key == undefined) return;
    this.recipeFilterList = [];

    if (key.trim() == '') {
      this.recipeFilterList = [];
      ingredient.recipename = '';
    }
    else {
      this.recipes.forEach((recipe, r) => {
        if (recipe.name.toUpperCase().includes(key.toUpperCase()) || recipe.somruid.toUpperCase().includes(key.toUpperCase())   ) {
          this.recipeFilterList.push({ index: r, dbid: recipe.dbid, recipename: recipe.name, recipeid: recipe.somruid });
        }
      });
    }

    if (this.recipeFilterList.length < 1) {
      ingredient.recipename = '';
      this.findrecipe = false;
      setTimeout(() => this.findrecipe = true, 5000);
    }

  }

  selectItem(select: { index: number, dbid: number, name: string, amount: string, unitsize: string, supplier: string, cat: string, suppliercat:string }, ingreIndex: number) {
    if (this.updatedsopingredients == undefined) return;
    if (ingreIndex == undefined || ingreIndex < 0 || ingreIndex >= this.updatedsopingredients.length) return;
    if (this.updatedsopingredients[ingreIndex] == undefined) return;

    this.updatedsopingredients[ingreIndex].itemname = select.name;
    this.updatedsopingredients[ingreIndex].unitquan = select.amount;
    this.updatedsopingredients[ingreIndex].unitmeasuretype = select.unitsize;
    this.updatedsopingredients[ingreIndex].vendor = select.supplier;
    this.updatedsopingredients[ingreIndex].cat = select.cat;
    this.updatedsopingredients[ingreIndex].suppliercat = select.suppliercat;
    this.itemfilterList = undefined;
  }

  selectRecipe(select: { index: number, dbid: number, recipename: string, recipeid:string }, ingreIndex: number) {
    if (this.updatedsopingredients == undefined) return;
    if (ingreIndex == undefined || ingreIndex < 0 || ingreIndex >= this.updatedsopingredients.length) return;
    if (this.updatedsopingredients[ingreIndex] == undefined) return;
    this.updatedsopingredients[ingreIndex].recipename = select.recipename;
    this.updatedsopingredients[ingreIndex].recipesomruid = select.recipeid;
    this.recipeFilterList = undefined;
  }


  removeIngre(index: number) {
    if (index < 0) return;
    if (this.updatedsopingredients[index] == undefined) return;

    this.updatedsopingredients.splice(index, 1);
  }

  reset() {
    this.displayRecipe = Recipe.newRecipe(this.recipe);
    this.isNew ? '' : this.enableEdit = false;
  }

  validateuserinput() {
    let missinformation = false;
    this.idunique = true;
    this.nameunique = true;
    this.hidewarning = true;
    this.inputwarning = [];
    let validated = true;

    if (this.displayRecipe.somruid == undefined || !this.displayRecipe.somruid || this.displayRecipe.somruid.trim() == '') {
      this.inputwarning.push('Recipe Id can not be empty.');
      missinformation = true;
      this.hidewarning = false;
      validated = false;
    }
    if (this.displayRecipe.name == undefined || !this.displayRecipe.name || this.displayRecipe.name.trim() == '') {
      this.inputwarning.push('Recipe name can not be empty.');
      missinformation = true;
      this.hidewarning = false;
      validated = false;
    }

    if (this.displayRecipe.volumn == undefined || !this.displayRecipe.volumn || this.displayRecipe.volumn.trim() == '' || this.displayRecipe.volumn.trim() == '0') {
      this.inputwarning.push('Recipe volume can not be empty or 0.');
      missinformation = true;
      this.hidewarning = false;
      validated = false;
    }

    if (this.displayRecipe.unit == undefined || !this.displayRecipe.unit || this.displayRecipe.unit.trim() == '') {
      this.inputwarning.push('Recipe unit can not be empty.');
      missinformation = true;
      this.hidewarning = false;
      validated = false;
    }

    if (this.displayRecipe.direction == undefined || !this.displayRecipe.direction || this.displayRecipe.direction.trim() == '') {
      this.inputwarning.push('Recipe Direction can not be empty.');
      missinformation = true;
      this.hidewarning = false;
      validated = false;
    }

    if (this.updatedsopingredients.length < 1) {
      this.inputwarning.push('Recipe should have at least one ingredient.');
      missinformation = true;
      this.hidewarning = false;
      validated = false;
    }

    this.updatedsopingredients.forEach(ingredient => {
      if ((!ingredient.itemname || ingredient.itemname.trim() == '' || ingredient.itemname.trim() == 'N/A') && (!ingredient.recipename || ingredient.recipename.trim() == '' || ingredient.recipename.trim() == 'N/A')) {
        this.inputwarning.push('An ingredient must has either material or recipe.');
        missinformation = true;
        this.hidewarning = false;
        validated = false;
      }

      if (!ingredient.reqquan || ingredient.reqquan.trim() == '') {
        this.inputwarning.push('An ingredient must has amount.');
        missinformation = true;
        this.hidewarning = false;
        validated = false;
      }

      if (!ingredient.requnit || ingredient.requnit.trim() == '') {
        this.inputwarning.push('An ingredient must has unit.');
        missinformation = true;
        this.hidewarning = false;
        validated = false;
      }
    });

    this.recipes=[];
    this.recipeService.loadRecipe().subscribe(recipes=>{
      this.recipes=recipes.slice();
      if (this.recipes.find(x =>x.dbid !== this.displayRecipe.dbid && x.name == this.displayRecipe.name) !== undefined) {
        this.nameunique = false;
        validated = false;
      }
  
      if (this.recipes.find(x =>x.dbid !== this.displayRecipe.dbid && x.somruid == this.displayRecipe.somruid) !== undefined) {
        this.idunique = false;
        validated = false;
      }
  
      
      if (this.idunique == false) {
        setTimeout(() => this.idunique = true, 5000);
      }
  
      if (this.nameunique == false) {
        setTimeout(() => this.nameunique = true, 5000);
      }
  
      if (missinformation == true) {
        setTimeout(() => this.hidewarning = true, 5000);
      }

      if(validated==true){
        this.save();
      }
    })
  }

  checkrecipeunit(recipeunit: string) {
    if (recipeunit.toUpperCase().includes('PLEASE SELECT')) {
      this.displayRecipe.unit = '';
    }
    else {
      this.displayRecipe.unit = recipeunit;
    }
  }



  save() {
    this.displayRecipe.editstatus = 'Entered';
    this.displayRecipe.enteredby = this.currentusername;
    this.displayRecipe.reviewedby = '';
    this.displayRecipe.approvedby = '';
    this.displayRecipe.reviewcomment = '';
    this.displayRecipe.approvecomment = '';

    // if (this.validateuserinput()) {
      this.inputwarning = [];
      this.recipe = this.displayRecipe;
      this.enableEdit = false;
      this.updateRecipe.emit({ recipe: this.recipe, updatedsopingredients: this.updatedsopingredients });
      this.reset();
    // }
  }

  review() {
    this.displayRecipe.editstatus = 'Reviewed';
    this.displayRecipe.reviewedby = this.currentusername;
    this.recipe = this.displayRecipe;
    this.enableEdit = false;
    this.inputwarning = [];
    this.updateRecipe.emit({ recipe: this.recipe, updatedsopingredients: this.updatedsopingredients });
    this.reset();
  }
  reviewreject() {
    if (!this.displayRecipe.reviewcomment) {
      this.reviewjectcomment = false;
      setTimeout(() => this.reviewjectcomment = true, 5000);
    }
    else {
      this.displayRecipe.editstatus = 'Review Rejected';
      this.displayRecipe.reviewedby = this.currentusername;
      this.recipe = this.displayRecipe;
      this.enableEdit = false;
      this.inputwarning = [];
      this.updateRecipe.emit({ recipe: this.recipe, updatedsopingredients: this.updatedsopingredients });
      this.reset();
    }
  }
  approve() {
    this.displayRecipe.editstatus = 'Approved';
    this.displayRecipe.approvedby = this.currentusername;
    this.recipe = this.displayRecipe;
    this.enableEdit = false;
    this.inputwarning = [];
    this.updateRecipe.emit({ recipe: this.recipe, updatedsopingredients: this.updatedsopingredients });
    this.reset();
  }
  approvereject() {
    if (!this.displayRecipe.approvecomment) {
      this.approvejectcomment = false;
      setTimeout(() => this.approvejectcomment = true, 5000);
    }
    else {
      this.displayRecipe.editstatus = 'Approve Rejected';
      this.displayRecipe.approvedby = this.currentusername;
      this.recipe = this.displayRecipe;
      this.enableEdit = false;
      this.inputwarning = [];
      this.updateRecipe.emit({ recipe: this.recipe, updatedsopingredients: this.updatedsopingredients });
      this.reset();
    }
  }

  cancelrecipe() {
    this.cancel.emit();
  }
}
