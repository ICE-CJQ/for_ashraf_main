import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { item } from '../../shared/objects/item';
import { Kit, Kitcomponent } from 'app/shared/objects/Kit';
import { InventoryService } from 'app/shared/services/Inventory.Service';
import { RecipeService } from '../../shared/services/Recipe.Service';
import { KitcomponentService } from '../../shared/services/Kitcomponent.Service';
import { KitService } from '../../shared/services/Kit.Service';
import { SettingService } from '../../shared/services/Setting.Service';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { User } from 'app/shared/objects/User';
import { AuthenticationService } from '../../shared/services/Authenticate.Service'
import { ItemdetailService } from '../../shared/services/itemdetail.service';
import { Recipe } from '../../shared/objects/Recipe';

@Component({
  selector: 'app-kit-display',
  providers: [InventoryService, RecipeService, KitcomponentService, KitService, SettingService, ItemdetailService],
  templateUrl: './kit-display.component.html',
  styleUrls: ['./kit-display.component.css']
})
export class KitDisplayComponent implements OnInit {
  @Input() kit: Kit;
  @Input() enableEdit: boolean;
  @Input() isNew: boolean;

  @Output() updateKitandsequence = new EventEmitter();
  @Output() updateeditstatus = new EventEmitter();
  @Output() cancel = new EventEmitter();
  @Output() changeTab = new EventEmitter();
  @Output() selectrecipe = new EventEmitter();

  //cat config
  useExistCat: boolean = false;
  type: ConfigValue[] //= [{ id: 6, name: 'ADA' }, { id: 7, name: 'PK' }, { id: 0, name: 'Antibody' }, { id: 3, name: 'Cell Line' }, { id: 4, name: 'Protein' }, { id: 5, name: 'Misc' }, { id: 8, name: 'Method Transfer' }, { id: 9, name: 'Charecterization kit' }, { id: 10, name: 'Biomaker' }]
  cloneLevel: ConfigValue[] //= [{ id: 1, name: 'monoclonal' }, { id: 2, name: 'polyclonal' }];
  clients: ConfigValue[]// = [{ id: 5, name: 'Intas' }];
  species: ConfigValue[]
  selectType: { id: number, name: string };
  selectClone: { id: number, name: string }// = this.cloneLevel[0];
  isClientSpecific: boolean = false;
  selectClient: { id: number, name: string }// = this.clients[0];
  selectSpecies: { id: number, name: string }// = this.species[0];
  nextNum: Setting;
  catalog: string;
  islockCat: boolean;
  displayKit: Kit;
  kitMethod: ConfigValue[]
  kitStatus: ConfigValue[]
  vialdescription: ConfigValue[]
  unitList: ConfigValue[];
  volUnit: ConfigValue[];
  massUnit: ConfigValue[];
  otherUnit: ConfigValue[];
  kitsize: string[] = ['2 X 96', '5 X 96', '10 X 96', 'Custom'];
  customeKitMethod: string;
  customeKitStatus: string;
  kits: Kit[];
  inventList: item[];
  recipeList: Recipe[];
  kitlinks: Kitcomponent[];
  itemfilterList: { index: number, dbid: number, name: string, amount: number, unit: string, supplier: string, cat: string, suppliercat:string, type: string }[];
  recipeFilterList: { index: number, dbid: number, name: string, recipeid: string }[];
  updatedkitcomponents: Kitcomponent[] = [];
 

  onfocusIndex: number = -1;
  message = '';

  inputcat = true;
  inputwarning: string[] = [];
  hidewarning = true;
  finditem = true;
  catunique = true;
  nameunique = true;
  amountnegative = false;

  currentuser: User;
  currentusername: string = '';
  currentuserrole: string = '';

  reviewjectcomment: boolean = true;
  approvejectcomment: boolean = true;

  

  constructor(private inventoryService: InventoryService, private recipeService: RecipeService,
    private kitLinkService: KitcomponentService, private setting: SettingService, private kitservice: KitService,
    private authenticationservice: AuthenticationService, private idetailservice: ItemdetailService, ) {

  }

  ngOnInit() {
    this.loadConfig();
    let getcurrentuser = this.authenticationservice.getCurrentUser()
    if (getcurrentuser !== undefined) {
      getcurrentuser.then(_ => {
        this.currentuser = User.fromJson(_);
        this.currentusername = this.currentuser.name;
        this.currentuserrole = this.currentuser.role;
      });
    }
    this.kitservice.loadKit().subscribe(kits => { 
      this.kits = kits.slice(); 
      // kits.forEach(kit=>{
      //   if(!kit.size.includes('wells')){
      //     kit.size=kit.size+' wells';
      //     this.kitservice.updateKitInfo(kit);
      //   }
      // })
    });
    this.inventoryService.loadInventory().subscribe(invent => {this.inventList = invent});
    this.recipeService.loadRecipe().subscribe(recipes => this.recipeList = recipes);
    this.kitLinkService.loadKitLink().subscribe(kitlinks => this.kitlinks = kitlinks);
  }

  ngOnChanges(change: SimpleChanges) {
    if (change.kit && this.kit !== undefined) {
      this.reset();
      //this.updatedkitcomponents = [];
      //if this is not a new kit and has some kitlink to show
      if (this.kit.dbid != -1) {
        this.kitLinkService.loadKitLinkBykitid(this.kit.dbid).subscribe(kitlink => {
          if(kitlink == undefined) kitlink = [];
          this.updatedkitcomponents = kitlink.slice();

          if (this.updatedkitcomponents == undefined) return;

          this.updatedkitcomponents.sort(function (a, b) {
            let firstnumber = parseInt(a.componentid.slice(-1));
            let secondnumber = parseInt(b.componentid.slice(-1));
            if (firstnumber > secondnumber) {
              return 1;
            }
            else {
              return -1;
            }
          });
        });
      }
    }
    //load all the item table and recipe table data
  }

  loadConfig() {
    this.setting.getSettingByPage('catalog').subscribe(config => {
      this.nextNum = config.find(x => x.type == 'CatPointer');
      //this.type = ConfigValue.toConfigArray(config.find(x => x.type == 'catType').value);
      if (this.type == []) this.type = Setting.getKitType()
      this.cloneLevel = ConfigValue.toConfigArray(config.find(x => x.type == 'cloneType').value);
      if (this.cloneLevel == []) this.cloneLevel = Setting.getClone()
      this.clients = ConfigValue.toConfigArray(config.find(x => x.type == 'focusClient').value);
      if (this.clients == []) this.clients = Setting.getClientSpec()
      this.species = ConfigValue.toConfigArray(config.find(x => x.type == 'species').value);
      if (this.species == []) this.species = Setting.getSpecies()
    });

    this.setting.getSettingByPage('general').subscribe(config => {
      this.unitList = [];
      this.volUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'volUnit').value)
      if (this.volUnit == []) this.volUnit = Setting.getVolumnUnit()
      this.volUnit.forEach(v => { this.unitList.push(v) })
      this.massUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'massUnit').value)
      if (this.massUnit == []) this.massUnit = Setting.getMassUnit()
      this.massUnit.forEach(m => { this.unitList.push(m) })
      this.otherUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'otherUnit').value)
      if (this.otherUnit == []) this.otherUnit = Setting.getOtherUnit()
      this.otherUnit.forEach(o => { this.unitList.push(o) })
    });

    this.setting.getSettingByPage('kit').subscribe(config => {
      this.kitStatus = ConfigValue.toConfigArray(config.find(x => x.type == 'kitStatus').value);
      if (this.kitStatus == []) this.kitStatus = Setting.getKitStatus()
      this.type = ConfigValue.toConfigArray(config.find(x => x.type == 'kitMethod').value);
      if (this.type == []) this.type = Setting.getKitType()
      this.clients = ConfigValue.toConfigArray(config.find(x => x.type == 'clientSpec').value);
      if (this.clients == []) this.clients = Setting.getClientSpec()
      this.vialdescription = ConfigValue.toConfigArray(config.find(x => x.type == 'vialList').value);
      if (this.vialdescription == []) this.vialdescription = Setting.getVial()
      this.kitsize = ConfigValue.toStringArray(config.find(x => x.type == 'packSize').value);
      if (this.kitsize == []) Setting.getPackSize().forEach(v => { this.kitsize.push(v.name) })
    });

  }

  reset() {
    this.displayKit = Kit.newKit(this.kit);
    this.updatedkitcomponents = [];
    this.customeKitMethod = '';
    this.customeKitStatus = '';
    this.reviewjectcomment = true;
    this.approvejectcomment = true;
    this.isNew ? '' : this.enableEdit = false;
    if (this.isNew) {
      this.useExistCat = false;
    }

  }

  // validatepacksize(packsize) {
  //   console.log(packsize);
  //   if(packsize=='0'){
  //     return;
  //   }
  //   // packsize = Math.abs(Math.round(packsize));
  //   this.displayKit.size = packsize+' X 96';
  // }

  removekitcomponent(index: number) {
    if (index < this.updatedkitcomponents.length - 1) {
      this.updatedkitcomponents.forEach(itemlink => {
        let componentid = itemlink.componentid;
        let length = componentid.length;
        let firstpart = componentid.substr(0, length - 1);
        let lastnumber = parseInt(componentid.slice(-1));
        if (lastnumber - 1 > index) {
          itemlink.componentid = firstpart + (lastnumber - 1);
        }
      });
    }

    let removeLink = this.updatedkitcomponents[index];
    this.updatedkitcomponents.splice(index, 1);
  }

  //when user input a item or component name, this function look up the item that it's name include the input
  itemFilter(component: Kitcomponent, key: string) {
    if (key == undefined) return;
    this.finditem = true;
    this.itemfilterList = [];
    if (key.trim() == '') {
      this.itemfilterList = [];
      component.reagent = '';
    }
    else {
      this.inventList.forEach((item, i) => {
        if (item.name.toUpperCase().includes(key.toUpperCase())) {
          this.itemfilterList.push({ index: i, dbid: item.dbid, name: item.name, amount: Number(item.amount), unit: item.unit, supplier: item.supplier, cat: item.cat, suppliercat:item.suppliercat, type: item.type });
        }
        if (item.cat.toUpperCase().includes(key.toUpperCase())) {
          this.itemfilterList.push({ index: i, dbid: item.dbid, name: item.name, amount: Number(item.amount), unit: item.unit, supplier: item.supplier, cat: item.cat, suppliercat:item.suppliercat, type: item.type });
        }
        if (item.manufacturer.toUpperCase().includes(key.toUpperCase())) {
          this.itemfilterList.push({ index: i, dbid: item.dbid, name: item.name, amount: Number(item.amount), unit: item.unit, supplier: item.supplier, cat: item.cat, suppliercat:item.suppliercat, type: item.type });
        }
        if (item.supplier.toUpperCase().includes(key.toUpperCase())) {
          this.itemfilterList.push({ index: i, dbid: item.dbid, name: item.name, amount: Number(item.amount), unit: item.unit, supplier: item.supplier, cat: item.cat, suppliercat:item.suppliercat, type: item.type });
        }
      });
    }

    if (this.itemfilterList.length < 1 && key.trim() != '') {
      component.reagent = '';
      this.finditem = false;
      setTimeout(() => this.finditem = true, 5000);
    }
    else {
      this.finditem = true;
    }
  }

  recipeFilter(component: Kitcomponent, key: string) {
    if (key == undefined) return;
    this.recipeFilterList = [];
    if (key.trim()=='') {
      this.recipeFilterList = [];
      component.recipeid = '';
      component.recipename = '';
    }


    this.recipeList.forEach((recipe, r) => {
      if (recipe.name.toUpperCase().includes(key.trim().toUpperCase()) || recipe.somruid.toUpperCase().includes(key.trim().toUpperCase())  ) {
        this.recipeFilterList.push({ index: r, dbid: recipe.dbid, name: recipe.name, recipeid: recipe.somruid });
      }
    });

    if (this.recipeFilterList.length < 1) {
      component.recipeid = '';
      component.recipename = '';
    }
  }

  //select is filterList find the item
  selectItem(select: { index: number, dbid: number, name: string, amount: number, unitsize: string, supplier: string, cat: string, suppliercat: string, type: string }, index: number) {
    if (this.updatedkitcomponents == undefined) return;
    if (index == undefined || index < 0 || index >= this.updatedkitcomponents.length) return;
    if (this.updatedkitcomponents[index] == undefined) return;
    this.updatedkitcomponents[index].reagent = select.name + " " + select.supplier + " Cat#" + select.cat + "Supplier Cat#"+select.suppliercat;
    if (select.type == 'Kit') {
      this.updatedkitcomponents[index].iskit = 'Yes';
      this.updatedkitcomponents[index].partnumber = '';
    }
    else {
      this.updatedkitcomponents[index].iskit = 'No';
      this.updatedkitcomponents[index].partnumber = '';
    }

    this.itemfilterList = undefined;
  }

  selectRecipe(select: { index: number, dbid: number, name: string, recipeid: string }, index: number) {
    if (this.updatedkitcomponents == undefined) return;
    if (index == undefined || index < 0 || index >= this.updatedkitcomponents.length) return;
    if (this.updatedkitcomponents[index] == undefined) return;
    this.updatedkitcomponents[index].recipename = select.name;
    this.updatedkitcomponents[index].recipeid = select.recipeid;
    this.recipeFilterList = undefined;
  }

  addnewcomponent() {
    if (this.displayKit.cat) {
      let componentid = this.displayKit.cat + '-' + (this.updatedkitcomponents.length + 1);
      this.updatedkitcomponents.push(new Kitcomponent(-1, this.displayKit.dbid, '', componentid, '', '', '', '', this.unitList[0].name, '', '', '', '', '', this.vialdescription[0].name, this.currentuser.name, new Date()));
      this.enableEdit = true;
    }
    else {
      this.inputcat = false;
      setTimeout(() => this.inputcat = true, 5000);
    }
  }

  checkkitmethod(method:string){
    if(method.toUpperCase().includes('PLEASE')){
      this.displayKit.method='';
    }
    else{
      this.displayKit.method=method;
    }
  }

  checkkitclient(client:string){
    if(client.toUpperCase().includes('PLEASE')){
      this.displayKit.clientspecific='';
    }
    else{
      this.displayKit.clientspecific=client;
    }
  }
  checkkitstatus(status:string){
    if(status.toUpperCase().includes('PLEASE')){
      this.displayKit.status='';
    }
    else{
      this.displayKit.status=status;
    }
  }

  validateuserinput() {
    let missinformation = false;
    this.catunique = true;
    this.nameunique = true;
    this.hidewarning = true;
    this.inputwarning = [];
    let validated = true;

    // this.updatedkitcomponents.forEach(component => {
    //   if (component.reagent == '') {
    //     component.reagent = 'N/A';
    //   }
    //   if (component.recipeid == '') {
    //     component.recipeid = 'N/A';
    //   }
    //   if (component.recipename == '') {
    //     component.recipename = 'N/A';
    //   }
    //   if (component.recipename && component.recipename != 'N/A') {
    //     component.partnumber = '';
    //   }
    // });


    //test for miss informatin for catalog number, name, method, kitsize, kit staus, kit price and at least one kit component
    if (this.displayKit.cat == undefined || !this.displayKit.cat || this.displayKit.cat.trim() == '') {
      this.inputwarning.push('Catalog number can not be empty.');
      missinformation = true;
      this.hidewarning = false;
      validated = false;
    }
    if (this.displayKit.name == undefined || !this.displayKit.name || this.displayKit.name.trim() == '') {
      this.inputwarning.push('Kit name can not be empty.');
      missinformation = true;
      this.hidewarning = false;
      validated = false;
    }

    if (this.displayKit.method == undefined || !this.displayKit.method || this.displayKit.method.trim() == '') {
      this.inputwarning.push('Kit method can not be empty.');
      missinformation = true;
      this.hidewarning = false;
      validated = false;
    }

    if (this.displayKit.size == undefined || !this.displayKit.size || this.displayKit.size.trim() == '' || this.displayKit.size.trim() == '0 X 96') {
      this.inputwarning.push('Pack size can not be empty or 0.');
      missinformation = true;
      this.hidewarning = false;
      validated = false;
    }

    if (this.displayKit.status == undefined || !this.displayKit.status || this.displayKit.status.trim() == '') {
      this.inputwarning.push('Kit status can not be empty.');
      missinformation = true;
      this.hidewarning = false;
      validated = false;
    }

    if (this.displayKit.price == undefined || !this.displayKit.price || Number(this.displayKit.price) <= 0) {
      this.inputwarning.push('Kit price can not be empty or zero.');
      missinformation = true;
      this.hidewarning = false;
      validated = false;
    }

    // if (this.updatedkitcomponents.length < 1) {
    //   this.inputwarning.push('Kit should have at least one component.');
    //   missinformation = true;
    //   this.hidewarning = false;
    //   validated = false;
    // }

    // //test if each component has name, amount unit, reagent or recipe name
    // this.updatedkitcomponents.forEach(link => {
    //   //let checkreagent = link.reagent!==undefined&&link.reagent!==''?this.inventList.find(x=>x.name+" "+x.supplier+" Cat#"+x.cat == link.reagent):undefined;
    //   if (!link.component || !link.amount || !link.unit || link.component == '' || Number(link.amount) == 0 || link.unit == '') {
    //     this.inputwarning.push('Component, component amount and component unit can not be empty.');
    //     missinformation = true;
    //     this.hidewarning = false;
    //     validated = false;
    //   }

    //   if ((!link.reagent && !link.recipename) || (link.reagent == 'N/A' && link.recipename == 'N/A')) {
    //     this.inputwarning.push('A component must has either a reagent or a recipe name.');
    //     missinformation = true;
    //     this.hidewarning = false;
    //     validated = false;
    //   }
    // });

    //for new kit, the kit catalog number and name should be unique
    this.kits=[];
    this.kitservice.loadKit().subscribe(kits=>{
      this.kits=kits.slice();
      // if (this.kits.find(x =>x.dbid !== this.displayKit.dbid && x.name == this.displayKit.name) !== undefined) {
      //   this.nameunique = false;
      //   validated = false;
      // }
  
      if (this.kits.find(x =>x.dbid !== this.displayKit.dbid && x.cat == this.displayKit.cat) !== undefined) {
        this.catunique = false;
        validated = false;
      }
  
      if (this.catunique == false) {
        setTimeout(() => this.catunique = true, 5000);

      }
  
      // if (this.nameunique == false) {
      //   setTimeout(() => this.nameunique = true, 5000);

      // }
  
      //if there are miss iniformation, then show this message
      if (missinformation == true) {
        setTimeout(() => this.hidewarning = true, 5000);
        
      }

      if(validated==true){
        this.save();
      }
      
     })

    //return validated;
    
  }

  //save whole kit information include kit components
  save() {
    this.displayKit.editstatus = 'Entered';
    this.displayKit.enteredby = this.currentusername;
    this.displayKit.reviewedby = '';
    this.displayKit.approvedby = '';
    this.displayKit.reviewcomment = '';
    this.displayKit.approvecomment = '';
    this.displayKit.lastmodify = new Date();

    //search all the kits to see if the catalog number and name are unique
    // if (this.validateuserinput()==true) {
      this.updatedkitcomponents.forEach(component => {
        if (component.reagent == '') {
          component.reagent = 'N/A';
        }
        if (component.recipeid == '') {
          component.recipeid = 'N/A';
        }
        if (component.recipename == '') {
          component.recipename = 'N/A';
        }
      });

      this.kit = this.displayKit;
      this.enableEdit = false;
      //this.updateKitandsequence.emit({ kit: this.kit, newSeq: Number(this.nextNum.value), updatedkitcomponents: this.updatedkitcomponents });
      this.updateKitandsequence.emit({ kit: this.kit, updatedkitcomponents: this.updatedkitcomponents });
      this.reset();
    }
  // }

  selectrecipename(recipename, recipeid) {
    if (recipename != "N/A" || recipename != "NA") {
      this.selectrecipe.emit({name:recipename, somruid:recipeid});
    }
  }

  assignAndCheck(type: string, value: any) {
    if (value == undefined) return;
    switch (type) {
      case 'type':
        if (value.trim() == '') break;
        this.selectType = this.type.find(x => x.id == value);
        break;
      case 'clone':
        if (value.trim() == '') break;
        this.selectClone = this.cloneLevel.find(x => x.id == value);
        break;
      case 'isCP':
        this.isClientSpecific = value == 'Yes';
        break;
      case 'client':
        if (value.trim() == '') break;
        this.selectClient = this.clients.find(x => x.id == value);
        break;
      case 'species':
        if (value.trim() == '') break;
        this.selectSpecies = this.species.find(x => x.id == value);
        break;
    }
    this.generateCatalog();
  }

  isValid(): boolean {
    if (this.selectType == undefined || this.selectSpecies == undefined) {
      return false;
    }

    if (this.selectType.name == 'Antibody' && this.selectClone == undefined) {
      return false;
    }

    if (this.isClientSpecific && this.selectClient == undefined) {
      return false;
    }

    return true;
  }

  generateCatalog() {
    if (!this.isValid()) return;

    this.catalog = 'SB-0';

    if (this.isClientSpecific) {
      this.catalog += '' + this.selectClient.id;
    }

    if (this.selectType.name == 'Antibody') {
      this.catalog += '' + this.selectClone.id;
    }
    else {
      this.catalog += '' + this.selectType.id;
    }
    this.catalog += '-';
    this.catalog += '' + this.selectSpecies.id;

    this.catalog += '' + this.nextNum.value;
  }

  setCat() {
    this.displayKit.method = this.selectType.name;
    this.displayKit.cat = this.catalog;
    this.islockCat = true;
  }

  review() {
    this.displayKit.editstatus = 'Reviewed';
    this.displayKit.reviewedby = this.currentusername;
    this.displayKit.reviewtime = new Date();
    this.kit = this.displayKit;
    this.enableEdit = false;
    this.inputwarning = [];
    this.updateeditstatus.emit(this.kit);
    this.reset();
  }
  reviewreject() {
    if (!this.displayKit.reviewcomment) {
      this.reviewjectcomment = false;
      setTimeout(() => this.reviewjectcomment = true, 5000);
    }
    else {
      this.displayKit.editstatus = 'Review Rejected';
      this.displayKit.reviewedby = this.currentusername;
      this.displayKit.reviewtime = new Date();
      this.kit = this.displayKit;
      this.enableEdit = false;
      this.inputwarning = [];
      this.updateeditstatus.emit(this.kit);
      this.reset();
    }
  }
  approve() {
    this.displayKit.editstatus = 'Approved';
    this.displayKit.approvedby = this.currentusername;
    this.displayKit.approvetime = new Date();
    this.kit = this.displayKit;
    this.enableEdit = false;
    this.inputwarning = [];

    this.updateeditstatus.emit(this.kit);
    this.reset();
  }
  approvereject() {
    if (!this.displayKit.approvecomment) {
      this.approvejectcomment = false;
      setTimeout(() => this.approvejectcomment = true, 5000);
    }
    else {
      this.displayKit.editstatus = 'Approve Rejected';
      this.displayKit.approvedby = this.currentusername;
      this.displayKit.approvetime = new Date();
      this.kit = this.displayKit;
      this.enableEdit = false;
      this.inputwarning = [];
      this.updateeditstatus.emit(this.kit);
      this.reset();
    }
  }

  cancelkit(){
    this.cancel.emit();
  }
}
