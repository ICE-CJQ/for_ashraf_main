import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { item } from '../../shared/objects/item';
import { Product, Application } from 'app/shared/objects/Product';
import { ProductService } from '../../shared/services/product.service';
import { ApplicationService } from '../../shared/services/application.service';
import { User } from 'app/shared/objects/User';
import { AuthenticationService } from '../../shared/services/Authenticate.Service';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { SettingService } from '../../shared/services/Setting.Service';
import { image } from '../../shared/objects/Image';

@Component({
  providers: [ProductService, ApplicationService, SettingService],
  selector: 'app-product-display',
  templateUrl: './product-display.component.html',
  styleUrls: ['./product-display.component.css'],
  // host: {
  //   '(document:click)': 'onClick($event)',
  // }
})
export class ProductDisplayComponent implements OnInit {
  @Input() product: Product;
  @Input() enableEdit: boolean;
  @Input() isNew: boolean;

  @Output() updateProduct = new EventEmitter();
  @Output() updateStatus = new EventEmitter();
  @Output() cancel = new EventEmitter();


  unitList: string[];
  volumeunits:string[];
  displayProduct: Product;
  products: Product[];
  application: Application;
  applications: Application[]=[];
  onfocusIndex: number;
  updatedapplications: Application[] = [];

  inputwarning: string[] = [];
  hidewarning = true;
  finditem = true;
  findrecipe = true;
  idunique = true;
  nameunique = true;

  currentuser: User;
  currentusername: string = '';
  currentuserrole: string = '';

  reviewjectcomment: boolean = true;
  approvejectcomment: boolean = true;

  locationtype: ConfigValue[];
  unit: ConfigValue[];
  volUnit: ConfigValue[];
  massUnit: ConfigValue[];
  otherUnit: ConfigValue[];
  displayItem: item;
  customeItemType: string;
  customeUnitType: string;
  customeLocationType: string;

  clonalities:string[];
  hosts:string[];
  types:string[];

  constructor(private productService: ProductService, private applicationService: ApplicationService,
    private authenticationservice: AuthenticationService, private setting: SettingService) { }

    // onClick(event) {
    //   console.log($("#specialinput"));
    //   console.log($("#specialinput").html());
    // }

    ngOnInit() {
      let getcurrentuser = this.authenticationservice.getCurrentUser()
      if (getcurrentuser !== undefined) {
        getcurrentuser.then(_ => {
          this.currentuser = User.fromJson(_);
          this.currentusername = this.currentuser.name;
          this.currentuserrole = this.currentuser.role;
        });
      }
  
      this.loadSetting();
      // this.ingredientService.loadIngredient().subscribe(ingredients => {this.ingredients = ingredients;});
      // this.recipeService.loadRecipe().subscribe(recipes => { this.recipes = recipes; });
  
    }
  
    ngOnChanges(change: SimpleChanges) {
      if (change.product && this.product !== undefined) {
        this.reset()
        if(!this.isNew){
          this.applicationService.getApplicationByProductId(this.displayProduct.dbid).subscribe(applications => {
            this.applications = applications;
          });

        }
      }
    }

  
    loadSetting() {
      this.setting.getSettingByPage('general').subscribe(config => {
        this.unit = [];
        this.volumeunits = [];
        this.volUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'volUnit').value)
        this.volUnit.forEach(v => { this.unit.push(v) })
        this.volUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'volUnit').value)
        this.volUnit.forEach(v => { this.unit.push(v); this.volumeunits.push(v.name) })
        this.massUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'massUnit').value)
        this.massUnit.forEach(m => { this.unit.push(m) })
        this.otherUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'otherUnit').value)
        this.otherUnit.forEach(o => { this.unit.push(o) })
      });

      this.setting.getSettingByPage('invent').subscribe(config => {
        this.clonalities = []
        let cloneLevel = ConfigValue.toConfigArray(config.find(x => x.type == 'cloneType').value)
        if(cloneLevel == []) cloneLevel = Setting.getClone();
        cloneLevel.forEach(c => { this.clonalities.push(c.name) })
        this.hosts = [];
        let host = ConfigValue.toConfigArray(config.find(x => x.type == 'host').value)
        if(host == []) host = Setting.getHost();
        host.forEach(h => { this.hosts.push(h.name) })
      });

      this.setting.getSettingByPage('product').subscribe(config => {
        this.types = []
        let types= ConfigValue.toConfigArray(config.find(x => x.type == 'protype').value);
        types.forEach(t => { this.types.push(t.name) })
      });


    }
  
    addNewApplication() {
      this.applications.push(new Application(-1, this.displayProduct.dbid, '', '',  '', '', this.currentuser.name, new Date()));
      this.enableEdit = true;
    }
    
    removeApplication(index: number) {
      if (index < 0) return;
      if (this.applications[index] == undefined) return;
  
      this.applications.splice(index, 1);
    }
  
    reset() {
      this.displayProduct = Product.newProduct(this.product);
      this.isNew ? '' : this.enableEdit = false;
    }
  
    validateuserinput() {
      let missinformation = false;
      this.idunique = true;
      this.nameunique = true;
      this.hidewarning = true;
      this.inputwarning = [];
      let validated = true;
  
      if (this.displayProduct.name == undefined || !this.displayProduct.name || this.displayProduct.name.trim() == '') {
        this.inputwarning.push('Product name can not be empty.');
        missinformation = true;
        this.hidewarning = false;
        validated = false;
      }
  
      if (this.displayProduct.cat == undefined || !this.displayProduct.cat || this.displayProduct.cat.trim() == '') {
        this.inputwarning.push('Product cat can not be empty.');
        missinformation = true;
        this.hidewarning = false;
        validated = false;
      }

      if (this.displayProduct.unitsize == undefined || !this.displayProduct.unitsize || this.displayProduct.unitsize.trim() == '') {
        this.inputwarning.push('Product unit size can not be empty.');
        missinformation = true;
        this.hidewarning = false;
        validated = false;
      }
  
      if (this.displayProduct.unit == undefined || !this.displayProduct.unit || this.displayProduct.unit.trim() == '') {
        this.inputwarning.push('Product unit can not be empty.');
        missinformation = true;
        this.hidewarning = false;
        validated = false;
      }

      if (this.displayProduct.unitprice == undefined || !this.displayProduct.unitprice || this.displayProduct.unitprice.trim() == '') {
        this.inputwarning.push('Product unit price can not be empty.');
        missinformation = true;
        this.hidewarning = false;
        validated = false;
      }

      if (this.displayProduct.type == undefined || !this.displayProduct.type || this.displayProduct.type.trim() == '') {
        this.inputwarning.push('Product type can not be empty.');
        missinformation = true;
        this.hidewarning = false;
        validated = false;
      }
  
      // if (this.displayProduct.direction == undefined || !this.displayProduct.direction || this.displayProduct.direction.trim() == '') {
      //   this.inputwarning.push('Recipe Direction can not be empty.');
      //   missinformation = true;
      //   this.hidewarning = false;
      //   validated = false;
      // }
  
      // if (this.applications.length < 1) {
      //   this.inputwarning.push('Recipe should have at least one ingredient.');
      //   missinformation = true;
      //   this.hidewarning = false;
      //   validated = false;
      // }
  
      // this.applications.forEach(application => {
      //   if (!application.purpose || application.purpose.trim() == '') {
      //     this.inputwarning.push('An application must has purpose.');
      //     missinformation = true;
      //     this.hidewarning = false;
      //     validated = false;
      //   }
  
      //   if (!application.recommenconcentration || application.recommenconcentration.trim() == '') {
      //     this.inputwarning.push('An application must has recommendation.');
      //     missinformation = true;
      //     this.hidewarning = false;
      //     validated = false;
      //   }
  
      //   if (!application.comment || application.comment.trim() == '') {
      //     this.inputwarning.push('An application must has comment.');
      //     missinformation = true;
      //     this.hidewarning = false;
      //     validated = false;
      //   }
      // });
  
      this.products=[];
      this.productService.loadProduct().subscribe(products=>{
        this.products=products.slice();
        // if (this.products.find(x =>x.dbid !== this.displayProduct.dbid && x.name == this.displayProduct.name) !== undefined) {
        //   this.nameunique = false;
        //   validated = false;
        // }   
        
        if (this.idunique == false) {
          setTimeout(() => this.idunique = true, 5000);
        }
    
        // if (this.nameunique == false) {
        //   setTimeout(() => this.nameunique = true, 5000);
        // }
    
        if (missinformation == true) {
          setTimeout(() => this.hidewarning = true, 5000);
        }
  
        if(validated==true){
          this.setimmune();
        }
      })
    }

    setimmune(){
  // <span style="color: rgb(34, 34, 34); font-family: Arial, Helvetica, sans-serif; font-size: large;">dsfasdfsadds</span>
  // <span style="color: rgb(47, 47, 47); font-family: &quot;Segoe UI&quot;, &quot;Segoe UI Web&quot;, &quot;Segoe UI Symbol&quot;, wf_segoe-ui_normal, &quot;Helvetica Neue&quot;, &quot;BBAlpha Sans&quot;, &quot;S60 Sans&quot;, Arial, sans-serif; font-size: 16px;">X</span>
  // <sup class="ocpSup" style="color: rgb(47, 47, 47); font-family: &quot;Segoe UI&quot;, &quot;Segoe UI Web&quot;, &quot;Segoe UI Symbol&quot;, wf_segoe-ui_normal, &quot;Helvetica Neue&quot;, &quot;BBAlpha Sans&quot;, &quot;S60 Sans&quot;, Arial, sans-serif;">4</sup>
  // <font size="4" style="color: rgb(34, 34, 34); font-family: Arial, Helvetica, sans-serif;">dsfasdfsadds</font><br>

  // <span style="font-size:11.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-CA;mso-fareast-language:EN-US;mso-bidi-language:AR-SA">Full length human VEGF
  // <sub>189</sub>
  //  expressed in 
  // <i>E. coli</i>
  // </span><br>

  // <i><span style="font-size:11.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-CA;mso-fareast-language:EN-US;mso-bidi-language:AR-SA">S. frugiperda</span></i>
  // <span style="font-size:11.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-CA;mso-fareast-language:EN-US;mso-bidi-language:AR-SA"> insect ovarian cell line 
  // <i>Sf</i>
  //  21-derived recombinant human VEGF
  // <sub>165</sub>
  // </span><br>

      // let immunogen = $("#specialinput").html();
      let immunogen =  document.getElementById("specialinput").innerHTML;
      immunogen = immunogen.replace(/<br>/g,'').trim();

      immunogen = immunogen.replace(/<span([^>]*)>/g,'<span>');

      immunogen = immunogen.replace(/<i([^>]*)>/g,'<i>');

      immunogen = immunogen.replace(/<sub([^>]*)>/g,'<sub>');

      immunogen = immunogen.replace(/<span><\/span>/g,'')
      immunogen = immunogen.replace(/<i><\/i>/g,'')
      immunogen = immunogen.replace(/<sub><\/sub>/g,'')
      immunogen = immunogen.replace(/<span>/g,'');
      immunogen = immunogen.replace(/<\/span>/g,'');

      this.displayProduct.immunogen=immunogen;

      this.save();
    }
  
    save() {
      // if (this.validateuserinput()) {
        this.displayProduct.status='Entered';
        this.displayProduct.enteredby=this.currentusername;
        this.displayProduct.entertime=new Date();
        this.displayProduct.reviewedby='';
        this.displayProduct.reviewtime=null;
        this.displayProduct.approvedby='';
        this.displayProduct.approvetime=null;
        this.displayProduct.reviewcomment='';
        this.displayProduct.approvecomment='';
        this.inputwarning = [];
        this.product = this.displayProduct;
        this.enableEdit = false;
        this.updateProduct.emit({ product: this.displayProduct, updatedapplications: this.applications });
        this.reset();
      // }
    }

    review() {
      this.displayProduct.status = 'Reviewed';
      this.displayProduct.reviewedby = this.currentusername;
      this.product = this.displayProduct;
      this.enableEdit = false;
      this.inputwarning = [];
      this.updateStatus.emit(this.displayProduct);
      this.reset();
    }
    reviewreject() {
      if (!this.displayProduct.reviewcomment) {
        this.reviewjectcomment = false;
        setTimeout(() => this.reviewjectcomment = true, 5000);
      }
      else {
        this.displayProduct.status = 'Review Rejected';
        this.displayProduct.reviewedby = this.currentusername;
        this.product = this.displayProduct;
        this.enableEdit = false;
        this.inputwarning = [];
        this.updateStatus.emit(this.displayProduct);
        this.reset();
      }
    }
    approve() {
      this.displayProduct.status = 'Approved';
      this.displayProduct.approvedby = this.currentusername;
      this.product = this.displayProduct;
      this.enableEdit = false;
      this.inputwarning = [];
      this.updateStatus.emit(this.displayProduct);
      this.reset();
    }
    approvereject() {
      if (!this.displayProduct.approvecomment) {
        this.approvejectcomment = false;
        setTimeout(() => this.approvejectcomment = true, 5000);
      }
      else {
        this.displayProduct.status = 'Approve Rejected';
        this.displayProduct.approvedby = this.currentusername;
        this.product = this.displayProduct;
        this.enableEdit = false;
        this.inputwarning = [];
        this.updateStatus.emit(this.displayProduct);
        this.reset();
      }
    }
  
    cancelproduct() {
      this.cancel.emit();
    }
  }
  
