import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { item, Itemtype, itemdetail } from 'app/shared/objects/item';
import { Location, Sublocation, Concentrationunit } from 'app/shared/objects/SettingObjects';
import { InventoryService } from '../../shared/services/Inventory.Service';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { ItemdetailService } from '../../shared/services/itemdetail.service';
import { SettingService } from '../../shared/services/Setting.Service';
import { IMyDpOptions } from 'mydatepicker';
import { User } from 'app/shared/objects/User';
import { AuthenticationService } from '../../shared/services/Authenticate.Service'
import { Chicken, Injection, Egg, toggleEgg } from '../../shared/objects/Chicken';
import { ChickenService } from 'app/shared/services/chicken.Service';
import { UserhistoryService } from '../../shared/services/userhistory.service';
import { EggtransferService } from '../../shared/services/eggtransfer.service';
import { Userhistory } from '../../shared/objects/Userhistory';
import { Eggtransfer } from 'app/shared/objects/eggtransfer';
import { Barcode } from '../../shared/objects/Barcode';
import { BarcodeService } from '../../shared/services/barcode.service';
import * as fileSaver from 'file-saver';
//import { FILE } from 'dns';


@Component({
  providers: [InventoryService, SettingService,  ItemdetailService, ChickenService, UserhistoryService, EggtransferService, BarcodeService],
  selector: 'app-receiveitem-display',
  templateUrl: './receiveitem-display.component.html',
  styleUrls: ['./receiveitem-display.component.css']
})

export class ReceiveitemDisplayComponent implements OnInit, OnChanges {
  @Input() receiveitem: itemdetail;
  @Input() unreceiveamount: string;
  @Input() enableEdit: boolean;
  @Input() newlot: boolean;
  @Input() showinuse: boolean;
  @Input() showunit: boolean;
  @Input() eggtransfer: Eggtransfer;
  @Input() updateRf: boolean;
  @Input() updateCon: boolean;

  @ViewChild('uploadfile', {static: false}) uploadfile: ElementRef;

  @Output() savereceiveitem = new EventEmitter();
  @Output() exit = new EventEmitter();
  @Output() updateItemdetail = new EventEmitter();//emit to inventory
  @Output() updateReceivedetail = new EventEmitter();//emit to order item
  @Output() cancelItemdetail = new EventEmitter();
  @Output() cancelreference = new EventEmitter();
  @Output() cancelconjugate = new EventEmitter();
  @Output() rfconapprove = new EventEmitter();



  displayreceiveitem: itemdetail;
  receiveitems: itemdetail[];
  parentlotnumbers: string[];
  lotnumbers: string[];
  locationtype: ConfigValue[]=[];
  unit: ConfigValue[]=[];
  volUnit: ConfigValue[]=[];
  massUnit: ConfigValue[]=[];
  otherUnit: ConfigValue[]=[];
  itemcategories: ConfigValue[]=[];
  projectnumbers: ConfigValue[]=[];
  displayItem: item;
  itemdetails: itemdetail[] = [];
  allitemdetails: itemdetail[] = [];
  inputwarning: string[] = [];
  hidewarning = true;
  catunique = true;
  nameunique = true;
  missunitsize = false;
  cloneLevel: ConfigValue[]
  clonalities:string[];
  specialunits:string[];// = ['case(s)', 'pack(s)', 'carton(s)', 'bag(s)', 'box(es)', 'unit(s)', 'prep(s)', 'each', 'bottle(s)', 'flask(s)'];
  hosts:string[];// = ['Chicken', 'Rabbit', 'Mouse', 'Rat', 'Goat', 'Sheep', 'Hamster'];
  purifications:string[];
  volumeunits:string[];
  weightunits:string[];
  tspecies:string[]// = ['Human', 'Mouse', 'Rat', 'Mokey', 'Chicken', 'Donkey', 'Goat', 'Rabbit'];
  locations: ConfigValue[] = [];
  sublocations: ConfigValue[] = [];
  allsublocations: ConfigValue[] = [];
  conjugateRatioOptions:ConfigValue[]=[];
  conjugateChemistryOptions:ConfigValue[]=[];
  itemtypes: Itemtype[];
  concentrationunits: Concentrationunit[];

  expiredateoptions: IMyDpOptions = { dateFormat: 'mm/dd/yyyy' };
  retestdateoptions: IMyDpOptions = { dateFormat: 'mm/dd/yyyy' };
  receivedateoptions: IMyDpOptions = { dateFormat: 'mm/dd/yyyy' };
  conjugateprepdateoptions: IMyDpOptions = { dateFormat: 'mm/dd/yyyy' };
  expiredate: { date: { year: number, month: number, day: number } };
  retestdate: { date: { year: number, month: number, day: number } };
  receivedate: { date: { year: number, month: number, day: number } };
  apcolumnprepdate: { date: { year: number, month: number, day: number } };
  conjugateprepdate: { date: { year: number, month: number, day: number } };
  earlyexpiredatemessage: string;
  inputerror=false;

  oldlotnumber='';
  uniqueitemlot=true;
  sameitemlot=false;
  currentuser: User;

  unopenbarcodedisplay=[];
  inusebarcodedisplay=[];
  showBarcodes=false;

  itemCategory='';
  itemManufacturer='';
  itemSupplier='';
  itemType='';
  expireOrRetest='Expire';

  uploadfileModel:any;
  pdfSrc:any;

  selectCon:File;
  selectRf:File;
  filename='';
  showfile=false;
  allusers:User[]=[];

  constructor(private inventService: InventoryService, private setting: SettingService, private itemdetailservice: ItemdetailService, private authenticationservice: AuthenticationService, private chickenService: ChickenService, private userhistoryservice: UserhistoryService, 
    private modalService: NgbModal, private eggtransferservice: EggtransferService, private barcodeservice: BarcodeService) {}

  ngOnInit() {
    let getcurrentuser = this.authenticationservice.getCurrentUser()
    if (getcurrentuser !== undefined) {
      getcurrentuser.then(_ => {
        this.currentuser = User.fromJson(_);
      });
    }
    this.loadSetting();
    this.parentlotnumbers = [];
    this.lotnumbers = [];
    // this.locationservice.loadLocation().subscribe(locations => { this.locations = locations; });
    // this.itemtypeserivce.loadItemtype().subscribe(itemtypes => { this.itemtypes = itemtypes; });
    // this.concentrationunitservice.loadConcentrationunit().subscribe(concentrationunits => { this.concentrationunits = concentrationunits; });
    this.itemdetailservice.loadItemDetail().subscribe(itemdetails => {
      this.allitemdetails = itemdetails;
    });

    if (this.displayreceiveitem) {
      let r = new Date(this.displayreceiveitem.receivedate);
      this.receivedate = { date: { year: r.getFullYear(), month: r.getMonth() + 1, day: r.getDate() } };

      let e = new Date(this.displayreceiveitem.expiredate);
      this.expiredate = { date: { year: e.getFullYear(), month: e.getMonth() + 1, day: e.getDate() } };

      let rt = new Date(this.displayreceiveitem.retestdate);
      this.retestdate = { date: { year: rt.getFullYear(), month: rt.getMonth() + 1, day: rt.getDate() } };

      let p = new Date(this.displayreceiveitem.apcolumnprepdate);
      this.apcolumnprepdate = { date: { year: p.getFullYear(), month: p.getMonth() + 1, day: p.getDate() } };

      let c = new Date(this.displayreceiveitem.conjugateprepdate);
      this.conjugateprepdate = { date: { year: p.getFullYear(), month: p.getMonth() + 1, day: p.getDate() } };

      this.inventService.getItemWithId(this.receiveitem.itemdbid).subscribe(item=>{
        this.itemCategory=item.category;
        this.itemManufacturer=item.manufacturer;
        this.itemSupplier=item.supplier;
        this.itemType=item.type;       
      })

    }

  }

  ngOnChanges(change: SimpleChanges) {
    if (change.receiveitem && this.receiveitem !== undefined) {
      this.reset();
      this.inventService.getItemWithId(this.receiveitem.itemdbid).subscribe(item=>{
        this.itemCategory=item.category;
        this.itemManufacturer=item.manufacturer;
        this.itemSupplier=item.supplier;
        this.itemType=item.type;       
      })
      



      if(this.displayreceiveitem.expiredate && this.displayreceiveitem.retestdate){
        this.expireOrRetest='Expire';
      }
      else if(this.displayreceiveitem.retestdate && !this.displayreceiveitem.expiredate){
        this.expireOrRetest='Retest';
      }
      else{
        this.expireOrRetest='Expire';
      }

      //console.log(this.expireOrRetest);
      
      if(this.displayreceiveitem.location){
        let targetlocation=this.locations.find(x => x.name == this.displayreceiveitem.location);
        this.allsublocations.forEach(sl=>{
          if(sl.id == targetlocation.id){
            this.sublocations.push(sl);
          }
        });
      }

      if (this.displayreceiveitem) {
        this.oldlotnumber = this.displayreceiveitem.lotnumber;
        let r = new Date(this.displayreceiveitem.receivedate);
        this.receivedate = { date: { year: r.getFullYear(), month: r.getMonth() + 1, day: r.getDate() } };
  
        let e = new Date(this.displayreceiveitem.expiredate);
        this.expiredate = { date: { year: e.getFullYear(), month: e.getMonth() + 1, day: e.getDate() } };

        let rt = new Date(this.displayreceiveitem.retestdate);
        this.retestdate = { date: { year: rt.getFullYear(), month: rt.getMonth() + 1, day: rt.getDate() } };
  
        let p = new Date(this.displayreceiveitem.apcolumnprepdate);
        this.apcolumnprepdate = { date: { year: p.getFullYear(), month: p.getMonth() + 1, day: p.getDate() } };

        let c = new Date(this.displayreceiveitem.conjugateprepdate);
        this.conjugateprepdate = { date: { year: p.getFullYear(), month: p.getMonth() + 1, day: p.getDate() } };

        if(this.displayreceiveitem.confile){
          let array=this.displayreceiveitem.confile.split('\\');
          this.filename=array[array.length-1];
        }
        if(this.displayreceiveitem.rffile){
          let array=this.displayreceiveitem.rffile.split('\\');
          this.filename=array[array.length-1];
        }

        if(this.showinuse){
          this.unopenbarcodedisplay=[];
          this.inusebarcodedisplay=[];
          this.showBarcodes=false;
          this.barcodeservice.getBarcodesByItemdetailDbid(this.displayreceiveitem.dbid).subscribe(barcodes=>{
            function compare(a,b) {
              if (a.barcode < b.barcode)
                return -1;
              if (a.barcode > b.barcode)
                return 1;
              return 0;
            }

            barcodes.sort(compare);
            let allbarcodes=[];
            barcodes.forEach(barcode=>{
              if(!barcode.usestatus.includes("Complete")){
                allbarcodes.push(barcode)
              }
            })

            let unopenbarcodes=[];
            let inusebarcodes=[];
            allbarcodes.forEach(barcode=>{
              if(barcode.usestatus=='Un-Open'){
                unopenbarcodes.push(barcode)
              }
              if(barcode.usestatus=='In-Use'){
                inusebarcodes.push(barcode)
              }
            })

          this.unopenbarcodedisplay=[];
          let unopenonedarray=[];
          let a;
          for(a=0; a<unopenbarcodes.length;a++){
            if(a==0 || a%3!=0){
              unopenonedarray.push(unopenbarcodes[a].barcode);
            }
            else{
              this.unopenbarcodedisplay.push(unopenonedarray);
              unopenonedarray=[];
              unopenonedarray.push(unopenbarcodes[a].barcode);
            }
          }
          this.unopenbarcodedisplay.push(unopenonedarray);
          unopenonedarray=[];

          
          

          this.inusebarcodedisplay=[];
          let inuseonedarray=[];
          let b;
          for(b=0; b<inusebarcodes.length;b++){
            if(b==0 || b%3!=0){
              inuseonedarray.push(inusebarcodes[b].barcode);
            }
            else{
              this.inusebarcodedisplay.push(inuseonedarray);
              inuseonedarray=[];
              inuseonedarray.push(inusebarcodes[b].barcode);
            }
          }
          this.inusebarcodedisplay.push(inuseonedarray);
          inuseonedarray=[];
        });
        }

      }
      
    }
  }



  loadSetting(){
    this.authenticationservice.loadAllUsers().subscribe(allusers=>{
      this.allusers=allusers;
    })
    this.setting.getSettingByPage('general').subscribe(config => {
      this.locations = [];
      ConfigValue.toConfigArray(config.find(x => x.type == 'location').value).forEach(l => {
        this.locations.push(l);
      });
     
      this.allsublocations = [];
      ConfigValue.toConfigArray(config.find(x => x.type == 'subLocation').value).forEach(sl => {
        this.allsublocations.push(sl);
      });

      this.unit = [];
      this.volumeunits = [];
      this.weightunits = [];
      this.specialunits = [];

      this.volUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'volUnit').value)
      this.volUnit.forEach(v => { this.unit.push(v); this.volumeunits.push(v.name) })
      this.massUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'massUnit').value)
      this.massUnit.forEach(m => { this.unit.push(m); this.weightunits.push(m.name)})
      this.otherUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'otherUnit').value)
      this.otherUnit.forEach(o => { this.unit.push(o); this.specialunits.push(o.name) })
      this.concentrationunits = [];
      let conUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'conUnit').value)
      conUnit.forEach(c => { this.concentrationunits.push(new Concentrationunit(c.id, c.name)) });
    });

    // this.setting.getSettingByPage('general').subscribe(config => {
    //   this.unit = [];
    //   this.volUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'volUnit').value)
    //   this.volUnit.forEach(v => { this.unit.push(v) })
    //   this.massUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'massUnit').value)
    //   this.massUnit.forEach(m => { this.unit.push(m) })
    //   this.otherUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'otherUnit').value)
    //   this.otherUnit.forEach(o => { this.unit.push(o) });
    // });

    this.setting.getSettingByPage('invent').subscribe(config => {
      this.clonalities = []
      let cloneLevel = ConfigValue.toConfigArray(config.find(x => x.type == 'cloneType').value)
      if(cloneLevel == []) cloneLevel = Setting.getClone();
      cloneLevel.forEach(c => { this.clonalities.push(c.name) })
      this.purifications = [];
      let puri = ConfigValue.toConfigArray(config.find(x => x.type == 'purification').value)
      if(puri == []) puri = Setting.getPurification();
      puri.forEach(p => { this.purifications.push(p.name) })
      this.hosts = [];
      let host = ConfigValue.toConfigArray(config.find(x => x.type == 'host').value)
      if(host == []) host = Setting.getHost();
      host.forEach(h => { this.hosts.push(h.name) })
      this.tspecies = [];
      let species = ConfigValue.toConfigArray(config.find(x => x.type == 'species').value)
      if(species == []) species = Setting.getSpecies();
      species.forEach(s => { this.tspecies.push(s.name) })

      this.itemcategories = ConfigValue.toConfigArray(config.find(x => x.type == 'category').value)

      this.conjugateChemistryOptions = ConfigValue.toConfigArray(config.find(x => x.type == 'conjugatechemistry').value)
      this.conjugateRatioOptions = ConfigValue.toConfigArray(config.find(x => x.type == 'conjugateratio').value)

    });

    this.setting.getSettingByPage('orderItem').subscribe(config => {
      let proList = ConfigValue.toConfigArray(config.find(x => x.type == 'projectList').value);
      if (proList == []) proList = Setting.getProject()
      proList.forEach(p => {
        this.projectnumbers.push(p);
      });
    });
  }


  showsublocationoption(l: string){
    this.sublocations=[];
    let targetlocation=this.locations.find(x => x.name == l);
    if (targetlocation == undefined) return;

    this.allsublocations.forEach(sl=>{
      if(sl.id == targetlocation.id){
        this.sublocations.push(sl);
      }
    });

    if(this.sublocations.length>0){
      this.displayreceiveitem.sublocation=this.sublocations[0].name;
    }
    

  }


  validateinput(receive:itemdetail){
    this.inputerror=false;
    this.uniqueitemlot=true;
    this.itemdetailservice.loadItemDetailByItemDbid(receive.itemdbid).subscribe(itemdetails=>{

      if(receive.lotnumber==undefined || receive.lotnumber.trim()=='' ||
      receive.amount==undefined || receive.amount.trim()=='' ||
      receive.location==undefined || receive.location.trim()=='' ||
      receive.unit==undefined || receive.unit.trim()=='' || (receive.itemtype!='Conjugate' && receive.receivedate==undefined)
      ){
        this.inputerror=true;
      }
      else{
        this.inputerror=false;
      }

      if(receive.itemtype!='Conjugate' && receive.receiveperson!=undefined && receive.receiveperson.trim()!=''){
        this.uniqueitemlot==true
      }
      else{
        if(itemdetails.findIndex(x=>x.dbid!=receive.dbid && x.lotnumber==receive.lotnumber)!=-1 && 
        itemdetails.findIndex(x=>x.dbid!=receive.dbid && x.receivedate==receive.receivedate)!=-1 && 
        itemdetails.findIndex(x=>x.dbid!=receive.dbid && x.location==receive.location)!=-1 &&
        itemdetails.findIndex(x=>x.dbid!=receive.dbid && x.recon==receive.recon)!=-1 &&
        itemdetails.findIndex(x=>x.dbid!=receive.dbid && x.comment==receive.comment)!=-1
        ){
          this.uniqueitemlot=false;
        }
        else{
          this.uniqueitemlot=true;
        }
      }

      if(this.inputerror==false && this.uniqueitemlot==true){
        this.haschanges();
      }

    })
  }

  haschanges(){
    let haschange=false;
    if(this.displayreceiveitem.lotnumber!=this.receiveitem.lotnumber){haschange=true;}
    if(this.displayreceiveitem.receivedate!=this.receiveitem.receivedate){haschange=true;}
    if(this.displayreceiveitem.expiredate!=this.receiveitem.expiredate){haschange=true;}
    if(this.displayreceiveitem.retestdate!=this.receiveitem.retestdate){haschange=true;}
    if(this.displayreceiveitem.batchnumber!=this.receiveitem.batchnumber){haschange=true;}
    if(this.displayreceiveitem.storetemperature!=this.receiveitem.storetemperature){haschange=true;}
    if(this.displayreceiveitem.purity!=this.receiveitem.purity){haschange=true;}
    // if(this.displayreceiveitem.rffile!=this.receiveitem.rffile){haschange=true;}


    if(this.displayreceiveitem.conjugatechemistry!=this.receiveitem.conjugatechemistry){haschange=true;}
    if(this.displayreceiveitem.biotintobiomoleculeratio!=this.receiveitem.biotintobiomoleculeratio){haschange=true;}
    if(this.displayreceiveitem.descriptionpreparation!=this.receiveitem.descriptionpreparation){haschange=true;}
    if(this.displayreceiveitem.conjugateprepperson!=this.receiveitem.conjugateprepperson){haschange=true;}
    if(this.displayreceiveitem.conjugateprepdate!=this.receiveitem.conjugateprepdate){haschange=true;}
    if(this.displayreceiveitem.moleculeweight!=this.receiveitem.moleculeweight){haschange=true;}
    if(this.displayreceiveitem.bindingactivity!=this.receiveitem.bindingactivity){haschange=true;}

    if(this.displayreceiveitem.conjugateincorporationratio!=this.receiveitem.conjugateincorporationratio){haschange=true;}
    if(this.displayreceiveitem.confile!=this.receiveitem.confile){haschange=true;}
    if(this.displayreceiveitem.bca!=this.receiveitem.bca){haschange=true;}
    if(this.displayreceiveitem.runnumber!=this.receiveitem.runnumber){haschange=true;}
    if(this.displayreceiveitem.biomoleculeinfo!=this.receiveitem.biomoleculeinfo){haschange=true;}
    if(this.displayreceiveitem.conjugateinfo!=this.receiveitem.conjugateinfo){haschange=true;}
    if(this.displayreceiveitem.location!=this.receiveitem.location){haschange=true;}

    if(this.displayreceiveitem.sublocation!=this.receiveitem.sublocation){haschange=true;}
    if(this.displayreceiveitem.amount!=this.receiveitem.amount){haschange=true;}
    if(this.displayreceiveitem.inuse!=this.receiveitem.inuse){haschange=true;}
    if(this.displayreceiveitem.concentration!=this.receiveitem.concentration){haschange=true;}
    if(this.displayreceiveitem.concentrationunit!=this.receiveitem.concentrationunit){haschange=true;}
    if(this.displayreceiveitem.species!=this.receiveitem.species){haschange=true;}
    if(this.displayreceiveitem.clonality!=this.receiveitem.clonality){haschange=true;}

    if(this.displayreceiveitem.host!=this.receiveitem.host){haschange=true;}
    if(this.displayreceiveitem.conjugate!=this.receiveitem.conjugate){haschange=true;}
    if(this.displayreceiveitem.iggdepletion!=this.receiveitem.iggdepletion){haschange=true;}
    if(this.displayreceiveitem.purification!=this.receiveitem.purification){haschange=true;}
    if(this.displayreceiveitem.volume!=this.receiveitem.volume){haschange=true;}
    if(this.displayreceiveitem.volumeunit!=this.receiveitem.volumeunit){haschange=true;}
    if(this.displayreceiveitem.weight!=this.receiveitem.weight){haschange=true;}

    if(this.displayreceiveitem.weightunit!=this.receiveitem.weightunit){haschange=true;}
    if(this.displayreceiveitem.apcolumnprepdate!=this.receiveitem.apcolumnprepdate){haschange=true;}
    if(this.displayreceiveitem.usagecolumn!=this.receiveitem.usagecolumn){haschange=true;}
    if(this.displayreceiveitem.columnnumber!=this.receiveitem.columnnumber){haschange=true;}
    if(this.displayreceiveitem.comment!=this.receiveitem.comment){haschange=true;}
    if(this.displayreceiveitem.reserve!=this.receiveitem.reserve){haschange=true;}
    if(this.displayreceiveitem.projectnumber!=this.receiveitem.projectnumber){haschange=true;}

    if(this.displayreceiveitem.unit!=this.receiveitem.unit){haschange=true;}
    if(this.displayreceiveitem.unitsize!=this.receiveitem.unitsize){haschange=true;}
    if(this.displayreceiveitem.recon!=this.receiveitem.recon){haschange=true;}



    this.enableEdit=false;

    if(haschange==true){this.save();}
  }

    save() {   
      this.enableEdit = false;
      this.unreceiveamount = (Number(this.unreceiveamount) - Number(this.displayreceiveitem.amount)).toString();
      this.displayreceiveitem.modifydate=new Date();
      this.displayreceiveitem.modifyperson=this.currentuser.name;
      
      // if(this.expiredateoptions=='Retest'){
      //   this.displayreceiveitem.expiredate=null;
      // }
      // else{
      //   this.displayreceiveitem.retestdate=null;
      // }


      if (this.displayreceiveitem.dbid == -1) {
        this.itemdetailservice.addItemDetail(this.displayreceiveitem).then(_ => {
          this.displayreceiveitem.dbid = _['dbid'];
          this.receiveitem = this.displayreceiveitem;
          if(this.eggtransfer){
            this.eggtransfer.destinationdbid=this.displayreceiveitem.dbid.toString();
            this.eggtransferservice.addEggtransfer(this.eggtransfer);
          }

          this.inventService.getItemWithId(this.displayreceiveitem.itemdbid).subscribe(item=>{
            if(this.showinuse){
              let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Inventory', 'Create New Itemdetail', 'Item name: '+item.name+', Item manufacturer cat#: '+item.cat+'\n'+'Itemdetail dbid: '+this.displayreceiveitem.dbid+', Itemdetail lot#: '+this.displayreceiveitem.lotnumber)
              this.userhistoryservice.addUserHistory(userhistory);
            }
            else{
              let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Order Item/Ordered', 'Receive New Item Lot', 'Item name: '+item.name+', Item manufacturer cat#: '+item.cat+'\n'+'Itemdetail dbid: '+this.displayreceiveitem.dbid+', Itemdetail lot#: '+this.displayreceiveitem.lotnumber+', itemdetail receive date: '+this.displayreceiveitem.receivedate.toString()+', Receive Amount is: '+this.displayreceiveitem.amount)
              this.userhistoryservice.addUserHistory(userhistory)
            }
          })



          this.updateItemdetail.emit({ receiveitem: this.receiveitem, unreceiveamount: this.unreceiveamount });
        });
      }
      else {
        if(this.updateCon==true){
          this.itemdetailservice.uploadFile(this.selectCon, this.displayreceiveitem.dbid).then(_=>{
            this.displayreceiveitem.confile=_['confile'];
            this.displayreceiveitem.constatus='Entered';
            this.itemdetailservice.updateSingleItemDetail(this.displayreceiveitem).then(() => {
              this.receiveitem = this.displayreceiveitem
              this.inventService.getItemWithId(this.displayreceiveitem.itemdbid).subscribe(item=>{
                let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Inventory', 'Update Conjugate Item Lot', 'Item name: '+item.name+', Item manufacturer cat#: '+item.cat+'\n'+'Itemdetail dbid: '+this.displayreceiveitem.dbid+', Itemdetail lot#: '+this.displayreceiveitem.lotnumber)
                this.userhistoryservice.addUserHistory(userhistory);
              })
              this.updateItemdetail.emit({ receiveitem: this.receiveitem, unreceiveamount: this.unreceiveamount});
            });
          });
        }
        else if(this.updateRf==true){
          this.itemdetailservice.uploadRfFile(this.selectRf, this.displayreceiveitem.dbid).then(_=>{
            this.displayreceiveitem.rffile=_['rffile'];
            this.displayreceiveitem.rfstatus='Entered';
            this.itemdetailservice.updateSingleItemDetail(this.displayreceiveitem).then(() => {
              this.receiveitem = this.displayreceiveitem;
              this.inventService.getItemWithId(this.displayreceiveitem.itemdbid).subscribe(item=>{
                let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Inventory', 'Update Reference Material Item Lot', 'Item name: '+item.name+', Item manufacturer cat#: '+item.cat+'\n'+'Itemdetail dbid: '+this.displayreceiveitem.dbid+', Itemdetail lot#: '+this.displayreceiveitem.lotnumber+', itemdetail create date: '+this.displayreceiveitem.receivedate.toString())
                this.userhistoryservice.addUserHistory(userhistory);
              })
              this.updateItemdetail.emit({ receiveitem: this.receiveitem, unreceiveamount: this.unreceiveamount});
            });
          });
        }
        else{
          this.itemdetailservice.updateSingleItemDetail(this.displayreceiveitem).then(() => {
            this.receiveitem = this.displayreceiveitem
            this.inventService.getItemWithId(this.displayreceiveitem.itemdbid).subscribe(item=>{
              let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Inventory', 'Update Item Lot', 'Item name: '+item.name+', Item manufacturer cat#: '+item.cat+'\n'+'Itemdetail dbid: '+this.displayreceiveitem.dbid+', Itemdetail lot#: '+this.displayreceiveitem.lotnumber)
              this.userhistoryservice.addUserHistory(userhistory);
            })
            this.updateItemdetail.emit({ receiveitem: this.receiveitem, unreceiveamount: this.unreceiveamount});
          });
        }

      }
  }



  validrf(receive:itemdetail){
    let inputerror=false;

    if(!receive.batchnumber || receive.batchnumber==null || receive.batchnumber.trim()==''){
      inputerror=true;
    }

    if(!receive.storetemperature || receive.storetemperature==null || receive.storetemperature.trim()==''){
      inputerror=true;
    }

    if(!receive.purity || receive.purity==null || receive.purity.trim()=='' || isNaN(Number(receive.purity))){
      inputerror=true;
    }

    if(this.selectRf==undefined){
      inputerror=true;
    }

      if(inputerror==false){
        receive.rfstatus='Entered'
        this.save();
      }
  }

  cancelrf(){
    this.itemCategory='';
    // this.itemManufacturer='';
    // this.itemSupplier='';
    this.selectRf=undefined;
    this.cancelreference.emit();
  }

  approverf(){
    this.displayreceiveitem.rfstatus='Approved';
    this.itemdetailservice.updateSingleItemDetail(this.displayreceiveitem).then(()=>{
      this.rfconapprove.emit('rf');
    })
  }


  validcon(receive:itemdetail){
    let inputerror=false;

    if(!receive.concentration || receive.concentration==null || receive.concentration.trim()=='' || isNaN(Number(receive.concentration))){
      inputerror=true;
    }

    if(!receive.concentrationunit || receive.concentrationunit==null || receive.concentrationunit.trim()==''){
      inputerror=true;
    }

    if(!receive.moleculeweight || receive.moleculeweight==null || receive.moleculeweight.trim()=='' || isNaN(Number(receive.moleculeweight))){
      inputerror=true;
    }

    if(!receive.bindingactivity || receive.bindingactivity==null || receive.bindingactivity.trim()=='' || isNaN(Number(receive.bindingactivity))){
      inputerror=true;
    }

    if(!receive.purity || receive.purity==null || receive.purity.trim()=='' || isNaN(Number(receive.purity))){
      inputerror=true;
    }

    if(!receive.conjugateincorporationratio || receive.conjugateincorporationratio==null || receive.conjugateincorporationratio.trim()=='' || isNaN(Number(receive.conjugateincorporationratio))){
      inputerror=true;
    }

    if(this.selectCon==undefined){
      inputerror=true;
    }

      if(inputerror==false){
        receive.rfstatus='Entered'
        this.save();
      }
  }

  cancelcon(){
    this.itemType='';
    this.selectCon=undefined;
    this.cancelconjugate.emit();
  }

  approvecon(){
    this.displayreceiveitem.constatus='Approved';
    this.itemdetailservice.updateSingleItemDetail(this.displayreceiveitem).then(()=>{
      this.rfconapprove.emit('con');
    })
  }

  // uploadcon(){
  //   let img: any=document.querySelector("#file")
  //   console.log('img is')
  //   console.log(img)
  //   console.log('file read type is')
  //   console.log(typeof(FileReader))
  //   let reader=new FileReader();
  //   reader.onload=(e:any)=>{
  //     console.log('path is:');
  //     console.log(e.target.result);
  //     console.log('e target is:');
  //     console.log(e.target);
  //     this.pdfSrc=e.target.result;
  //   }

  //   reader.readAsArrayBuffer(img.files[0])
  // }

  uploadcon(event){
    this.selectCon = event.target.files[0];
  }

  uploadrf(event){
    this.selectRf = event.target.files[0];
  }


  // checksameitemdetail(){
  //   this.itemdetailservice.loadItemDetailByItemDbid(this.displayreceiveitem.itemdbid).subscribe(itemdetails=>{

  //     let index =itemdetails.findIndex(x=>x.dbid!=this.displayreceiveitem.dbid && 
  //       new Date(x.receivedate).getDate()==new Date(this.displayreceiveitem.receivedate).getDate() &&
  //       new Date(x.receivedate).getMonth()==new Date(this.displayreceiveitem.receivedate).getMonth() &&
  //       new Date(x.receivedate).getFullYear()==new Date(this.displayreceiveitem.receivedate).getFullYear()
  //     )
  //     let index =itemdetails.findIndex(x=>x.dbid!=this.displayreceiveitem.dbid && x.lotnumber==this.displayreceiveitem.lotnumber  && 
  //     x.receivedate==this.displayreceiveitem.receivedate && 
  //     x.location==this.displayreceiveitem.location &&
  //     x.recon==this.displayreceiveitem.recon &&
  //     x.comment==this.displayreceiveitem.comment &&
  //     x.itemname==this.displayreceiveitem.itemname &&
  //     x.itemtype==this.displayreceiveitem.itemtype &&
  //     x.supplier==this.displayreceiveitem.supplier &&
  //     x.parentlotnumber==this.displayreceiveitem.parentlotnumber &&
  //     x.expiredate==this.displayreceiveitem.expiredate &&    
  //     x.sublocation==this.displayreceiveitem.sublocation &&       
  //     x.concentration==this.displayreceiveitem.concentration &&    
  //     x.concentrationunit==this.displayreceiveitem.concentrationunit &&     
  //     x.species==this.displayreceiveitem.species &&     
  //     x.clonality==this.displayreceiveitem.clonality &&    
  //     x.host==this.displayreceiveitem.host &&     
  //     x.conjugate==this.displayreceiveitem.conjugate &&     
  //     x.iggdepletion==this.displayreceiveitem.iggdepletion &&    
  //     x.purification==this.displayreceiveitem.purification &&     
  //     x.volume==this.displayreceiveitem.volume &&     
  //     x.volumeunit==this.displayreceiveitem.volumeunit &&    
  //     x.weight==this.displayreceiveitem.weight &&     
  //     x.weightunit==this.displayreceiveitem.weightunit && 
  //     x.apcolumnprepdate==this.displayreceiveitem.apcolumnprepdate &&    
  //     x.usagecolumn==this.displayreceiveitem.usagecolumn &&     
  //     x.columnnumber==this.displayreceiveitem.columnnumber &&        
  //     x.reserve==this.displayreceiveitem.reserve &&     
  //     x.projectnumber==this.displayreceiveitem.projectnumber && 
  //     x.receiveperson==this.displayreceiveitem.receiveperson &&         
  //     x.unit==this.displayreceiveitem.unit &&     
  //     x.unitsize==this.displayreceiveitem.unitsize)
  //     if(index!=-1){
  //         this.sameitemlot=true;
  //         this.thesameitemlot=itemdetails[index];
  //         }
  //       else{
  //         this.sameitemlot=false;
  //       }

  //   })
  // }




  validateamount(receive: number) {
    if (isNaN(receive)) { this.displayreceiveitem.amount = '0'; return; }
    receive = Math.abs(Math.round(receive));
    if (receive > Number(this.unreceiveamount)) { receive = Number(this.unreceiveamount); }

    this.displayreceiveitem.amount = receive.toString();
  }

  reset() {
    this.displayreceiveitem = itemdetail.newitemdetail(this.receiveitem);
    this.inventService.getItemWithId(this.receiveitem.itemdbid).subscribe(item=>{
      this.itemCategory=item.category;
      this.itemManufacturer=item.manufacturer;
      this.itemSupplier=item.supplier;
      this.itemType=item.type;       
    })
    this.inputerror=false;
    this.showBarcodes=false;
    this.filename='';
    this.cancelcon();
    this.cancelrf();
  }

  changeexpiredateorrestest(event) {
    let d = event.formatted;
    if (d == undefined) return;
    if (new Date(d) < new Date()) {
      if(this.expireOrRetest=='Expire'){
        this.earlyexpiredatemessage = "Please make sure the expire date is not earlier than today";
      }
      else if(this.expireOrRetest=='Retest'){
        this.earlyexpiredatemessage = "Please make sure the retest date is not earlier than today";
      }
      
      setTimeout(_ => { this.earlyexpiredatemessage = undefined }, 5000);
    }
    else {
      if(this.expireOrRetest=='Expire'){
        this.displayreceiveitem.expiredate = new Date(d);
        this.displayreceiveitem.retestdate = null;
      }
      else if(this.expireOrRetest=='Retest'){
        this.displayreceiveitem.retestdate = new Date(d);
        this.displayreceiveitem.expiredate = null;
      }     
    }
  }

  changereceivedate(event) {
    let d = event.formatted;
    if (d == undefined) return;
    this.displayreceiveitem.receivedate = new Date(d);
  }

  changecolumeprepdate(event){
    let d = event.formatted;
    if (d == undefined) return;
    this.displayreceiveitem.apcolumnprepdate = new Date(d);
  }

  changeconjugateprepdate(event){
    let d = event.formatted;
    if (d == undefined) return;
    this.displayreceiveitem.conjugateprepdate = new Date(d);
  }




  showparentlotnumber(parentlotnumber) {
    if (parentlotnumber == undefined || parentlotnumber.trim() == '') {
      this.parentlotnumbers = [];
      this.itemdetailservice.loadItemDetailByItemDbid(this.receiveitem.itemdbid).subscribe((itemdetails) => {
        itemdetails.forEach((itemdetail) => {
          if (this.parentlotnumbers.findIndex(x => x == itemdetail.parentlotnumber) == -1) {
            this.parentlotnumbers.push(itemdetail.parentlotnumber);
          }
        });
      });
    }
    else {
      this.displayreceiveitem.parentlotnumber = parentlotnumber;
      this.parentlotnumbers = [];
    }
  }

  selectparentlotnumber(parentlotnumber, index) {
    this.displayreceiveitem.parentlotnumber = parentlotnumber;
  }

  showlotnumber(lotnumber) {
    if (lotnumber == undefined || lotnumber.trim() == '') {
      this.lotnumbers = [];
      this.itemdetailservice.loadItemDetailByItemDbid(this.receiveitem.itemdbid).subscribe((itemdetails) => {
        itemdetails.forEach((itemdetail) => {
          if (this.lotnumbers.findIndex(x => x == itemdetail.lotnumber) == -1) {
            this.lotnumbers.push(itemdetail.lotnumber);
          }
        });
      });
    }
    else {
      this.displayreceiveitem.lotnumber = lotnumber;
      this.lotnumbers = [];
    }
  }

  selectlotnumber(lotnumber, index) {
    this.displayreceiveitem.lotnumber = lotnumber;
  }

  changereservestatus(){
    if(this.displayreceiveitem.reserve!='Yes'){
      this.displayreceiveitem.reserve='Yes';
    }
    else{
      this.displayreceiveitem.reserve='No';
    }
    
  }

  cancelitemdetail(){
    this.cancelrf();
    this.cancelcon();
    this.reset();
    this.cancelItemdetail.emit();
  }

  changeiggdepletion() {
    if(this.displayreceiveitem.iggdepletion == 'Yes'){
      this.displayreceiveitem.iggdepletion = 'No';
    }
    else{
      this.displayreceiveitem.iggdepletion = 'Yes';
    }
  }

  changeconjugate(){
    if(this.displayreceiveitem.conjugate == 'Conjugated'){
      this.displayreceiveitem.conjugate = 'Unconjugated';
    }
    else{
      this.displayreceiveitem.conjugate = 'Conjugated';
    }
  }

  openuploadfilemodel(){
    this.uploadfileModel=this.modalService.open(this.uploadfile, { backdrop: "static", size: 'lg' })
  }

  getfilelocation(event){


  }

  savefilelocation(){

  }

  closefilemodel(){
    this.uploadfileModel.close();
  } 

  openfile(type:string){
    if(type=='coa'){
      this.showfile=true;
      let path=this.displayreceiveitem.confile.replace(/ /g,'space')
      let array=this.displayreceiveitem.confile.split('\\')
      let filename=array[array.length-1]
    this.itemdetailservice.getFilepath(this.displayreceiveitem.dbid).subscribe(file=>{
      let blob: Blob = file.blob();
      fileSaver.saveAs(blob, filename);
    })
    }

    if(type=='rf'){
      this.showfile=true;
      let path=this.displayreceiveitem.rffile.replace(/ /g,'space')
      let array=this.displayreceiveitem.rffile.split('\\')
      let filename=array[array.length-1]
    this.itemdetailservice.getrfFilepath(this.displayreceiveitem.dbid).subscribe(file=>{
      let blob: Blob = file.blob();
      fileSaver.saveAs(blob, filename);
    })
    }


  }



}
