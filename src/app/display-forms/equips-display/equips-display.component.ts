import { Component, OnInit, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { Equipment } from 'app/shared/objects/Equipment';

@Component({
  selector: 'app-equips-display',
  templateUrl: './equips-display.component.html',
  styleUrls: ['./equips-display.component.css']
})
export class EquipsDisplayComponent implements OnInit, OnChanges {
  @Input() equipment: Equipment
  @Input() enableEdit: boolean;
  @Input() isNew:boolean;

  @Output() updateEquip = new EventEmitter();
  
  displayItem:Equipment;
  constructor() { }

  ngOnInit() {
   
  }

  ngOnChanges(change: SimpleChanges) {
    if(change.equipment && this.equipment !== undefined){
      this.reset();
    }
  }

  save(){
    this.equipment = Equipment.newEquipment(this.displayItem);
    this.enableEdit = false;
    this.reset();
    this.updateEquip.emit(this.equipment);
  }

  reset(){
    this.displayItem = Equipment.newEquipment(this.equipment);
    this.isNew?'':this.enableEdit = false;
  }
}
