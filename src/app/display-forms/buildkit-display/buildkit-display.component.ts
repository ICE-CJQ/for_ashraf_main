import { orderitem } from "../../shared/objects/OrderItem";
import { Kit, Kitcomponent } from "../../shared/objects/Kit";
import { item, itemdetail } from "../../shared/objects/item";
import { Recipe, Ingredient } from "../../shared/objects/Recipe";
import { Kittree, recipenode } from "../../shared/objects/Kittree";
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
  ElementRef
} from "@angular/core";
import { KitService } from "app/shared/services/Kit.Service";
import { RecipeService } from "app/shared/services/Recipe.Service";
import { IngredientService } from "app/shared/services/Ingredient.Service";
import { InventoryService } from "app/shared/services/Inventory.Service";
import { OrderitemService } from "app/shared/services/Orderitem.Service";
import { KitcomponentService } from "../../shared/services/Kitcomponent.Service";
import { ItemdetailService } from "../../shared/services/itemdetail.service";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { Setting } from "../../shared/objects/Setting";
import { SettingService } from "../../shared/services/Setting.Service";
import { Observable } from "rxjs";
import { IfObservable } from "rxjs/observable/IfObservable";

@Component({
  selector: "app-buildkit-display",
  providers: [
    KitService,
    KitcomponentService,
    RecipeService,
    IngredientService,
    OrderitemService,
    SettingService,
    InventoryService,
    ItemdetailService
  ],
  templateUrl: "./buildkit-display.component.html",
  styleUrls: ["./buildkit-display.component.css"]
})
export class BuildkitDisplayComponent implements OnInit, OnChanges {
  @ViewChild("kitDisplay", { static: false }) kitDisplay: ElementRef;
  @ViewChild("confirmation", { static: false }) confirmation: ElementRef;

  @Input() recipelist: recipenode[];
  @Input() tree: Kittree;
  @Input() selectlist: recipenode[];

  @Output() sendselectlist;

  kitmodal: any;
  message: string;

  buildkit: Kit;
  buildkitcomponents: Kitcomponent[];
  buildkititems: {
    item: item;
    supplier: string;
    cat: string;
    requireamount: number;
    requireunit: string;
    stockamount: number;
    stockunit: string;
    selected: boolean;
  }[] = [];
  unitnotsame: string[] = [];
  notsameunit: boolean;

  notfindindatabase: string[] = [];
  notindatabase: boolean;

  wronginput: boolean;
  wronginputinformaiton = [];
  databaseerror: boolean;
  databaseerrorinformation = [];

  kitlinks: Kitcomponent[] = [];

  displaycomponents = false;
  cat: string;
  kitpacknumber: number;
  totalneed: number;

  kittree = new Kittree(null);

  constructor(
    private modalService: NgbModal,
    private kitService: KitService,
    private settingservice: SettingService,
    private kitcomponentService: KitcomponentService,
    private recipeService: RecipeService,
    private ingreService: IngredientService,
    private orderitemservce: OrderitemService,
    private inventservice: InventoryService,
    private itemdetailservice: ItemdetailService
  ) {}

  ngOnInit() {}

  ngOnChanges(change: SimpleChanges) {}

  ngAfterViewInit() {
    setTimeout(_ => {
      if (this.tree !== undefined && this.tree) {
      }
    });
  }

  select(node: recipenode) {
    if (node.selected == "No") {
      node.selected = "Yes";
      // if(this.selectlist.find(x=>x.itemname==node.itemname && x.itemcat==node.itemcat && x.supplier==node.supplier)==undefined){
      //   this.selectlist.push(node);
      // }
    } else {
      node.selected = "No";
      let index = this.selectlist.findIndex(
        x =>
          x.itemname == node.itemname &&
          x.itemcat == node.itemcat &&
          x.supplier == node.supplier
      );
      // if(index!=-1){
      //   this.selectlist.splice(index,1);
      // }
    }
    // this.sendselectlist.emit(this.selectlist);
  }
}
