import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { item, Itemtype, itemdetail } from '../../shared/objects/item';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { IMyDpOptions } from 'mydatepicker';
import { Projectnumber } from '../../shared/objects/SettingObjects';
import { SettingService } from '../../shared/services/Setting.Service';
import { InventoryService } from '../../shared/services/Inventory.Service';
import { orderitem } from '../../shared/objects/OrderItem';


@Component({
  providers: [InventoryService, SettingService],
  selector: 'app-orderitem-display',
  templateUrl: './orderitem-display.component.html',
  styleUrls: ['./orderitem-display.component.css'],
})
export class OrderitemDisplayComponent implements OnInit, OnChanges {
  @Input() orderitem: orderitem[];
  @Input() isAdd: boolean;
  @Input() enableEdit: boolean;
  @Input() newrequest: boolean;

  @Output() addOrders = new EventEmitter();
  @Output() updateorderItem = new EventEmitter();
  @Output() closeorder = new EventEmitter();

  

  unit: ConfigValue[]=[];
  volUnit: ConfigValue[]=[];
  massUnit: ConfigValue[]=[];
  otherUnit: ConfigValue[]=[];
  projectnumbers: ConfigValue[]=[];
  itemtypes: ConfigValue[]=[];
  itemcategories: ConfigValue[]=[];

  warningmessage=[];

  showitemlist=false;
  showcatlist=false;
  receivedateoptions: IMyDpOptions = { dateFormat: 'mm/dd/yyyy' };
  etaoptions: IMyDpOptions = { dateFormat: 'mm/dd/yyyy' };
  receivedate: { date: { year: number, month: number, day: number } };
  eta: { date: { year: number, month: number, day: number } };
  invent: item[];
  
  blockinput:boolean;
  orderitemfilterList: { name: string, cat: string, category:string, suppliercat: string, supplier: string, manufacturer: string, unit: string, unitsize: string, unitprice:string, type:string }[];
  catfilterList: { name: string, cat: string, category:string, suppliercat: string,  supplier: string, manufacturer: string, unit: string, unitsize: string, unitprice:string, type:string }[];
  

  
  constructor( private inventservice: InventoryService, private settingservice: SettingService, private setting: SettingService) { }


  ngOnInit() {
    this.inventservice.loadInventory().subscribe(invent => { this.invent = invent;});
    this.loadConfig()
    this.loadeta();
    this.blockinput=false;
  }

  ngOnChanges(change: SimpleChanges) {
    this.loadeta();
    this.blockinput=false;
  }

  loadeta(){
    if (this.orderitem) {
      this.orderitem.forEach(orderitem=>{
        let r = new Date(orderitem.eta);
        this.eta = { date: { year: r.getFullYear(), month: r.getMonth() + 1, day: r.getDate() } };
      })

    }
  }

  loadConfig() {
    this.setting.getSettingByPage('general').subscribe(config => {
      this.unit = [];
      this.volUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'volUnit').value)
      if (this.volUnit == []) this.volUnit = Setting.getVolumnUnit()
      this.volUnit.forEach(v => { this.unit.push(v) })
      this.massUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'massUnit').value)
      if (this.massUnit == []) this.massUnit = Setting.getMassUnit()
      this.massUnit.forEach(m => { this.unit.push(m) })
      this.otherUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'otherUnit').value)
      if (this.otherUnit == []) this.otherUnit = Setting.getOtherUnit()
      this.otherUnit.forEach(o => { this.unit.push(o) })
    });

    this.setting.getSettingByPage('orderItem').subscribe(config => {
      let proList = ConfigValue.toConfigArray(config.find(x => x.type == 'projectList').value);
      if (proList == []) proList = Setting.getProject()
      proList.forEach(p => {
        this.projectnumbers.push(p);
      });
    });


    this.setting.getSettingByPage('invent').subscribe(config => {
      this.itemtypes = [];
      let itemt = ConfigValue.toConfigArray(config.find(x => x.type == 'itemType').value)
      if (itemt == []) itemt = Setting.getItemType();
      itemt.forEach(l => {
        this.itemtypes.push(l);
      });
      this.itemcategories = ConfigValue.toConfigArray(config.find(x => x.type == 'category').value)
    });
  }

  check(orderitem:orderitem){
    let check=true;
    this.warningmessage=[];
    if(orderitem.name==undefined || orderitem.name.trim()==''){
      check=false;
    }
    if(!orderitem.cat || orderitem.cat.trim()==''){
      check=false;
    }
    if(!orderitem.type || orderitem.type.trim()==''){
      check=false;
    }
    if(   (!orderitem.supplier || orderitem.supplier.trim()=='')  &&  (!orderitem.manufacturer || orderitem.manufacturer.trim()=='') ){
      check=false;
    }

    if(!orderitem.amount || orderitem.amount.trim()=='' || orderitem.amount.trim()=='0' || /^[0-9]+$/.test(orderitem.amount)==false || parseInt(orderitem.amount)<=0){
      check=false;
    }
    if(!orderitem.unit || orderitem.unit.trim()==''){
      check=false;
    }
    if(!orderitem.unitsize || orderitem.unitsize.trim()=='' || parseFloat(orderitem.unitsize)==NaN ||  /^[0-9,.]*$/.test(orderitem.unitsize)==false){
      check=false;
    }
    if(orderitem.unitprice && orderitem.unitprice.trim()!=''){
      if( parseFloat(orderitem.unitprice)==NaN ||  /^[0-9,.]*$/.test(orderitem.unitprice)==false){
        check=false;
      }
    }
    if(!orderitem.grantid || orderitem.grantid.trim()==''){
      check=false;
    }
    return check;
  }
  save() {
    let i;
    for(i=0;i<this.orderitem.length;i++){
      if(this.check(this.orderitem[i])==false){
        return;
      }
      else{
        this.orderitem[i].unreceiveamount=this.orderitem[i].amount;
      }      
    }
    // this.orderitem.forEach(orderitem=>{
    //   orderitem.unreceiveamount=orderitem.amount;
    // })
    this.addOrders.emit(this.orderitem);
    this.enableEdit = false;
  }

  onClickedOutside(e:Event){
    this.showitemlist=false;
    this.showcatlist=false;
  }

  update() {
      if(this.check(this.orderitem[0])==false){
        return;
      }
      else{
          this.updateorderItem.emit(this.orderitem[0]);
          this.enableEdit = false;
      }      
    
    // this.orderitem.forEach(orderitem=>{
    //   orderitem.unreceiveamount=orderitem.amount;
    // })
          // this.updateorderItem.emit(this.orderitem[0]);
          // this.enableEdit = false;
  }

  changeurgentstatus(index: number) {
    if (index == undefined || index < 0 || this.orderitem[index] == undefined)return;
      this.orderitem[index].urgent = !this.orderitem[index].urgent;
  }

  changereservestatus(index: number) {
    if (index == undefined || index < 0 || this.orderitem[index] == undefined)return;
    if(this.orderitem[index].reserve=='Yes'){
      this.orderitem[index].reserve='No';
    }
    else{
      this.orderitem[index].reserve='Yes'
    }
      
  }

  addneworderitem() {
    this.orderitem.push(new orderitem(-1, '', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', new Date(), null, null, null, null, null, false,'','', 'No'));
  }

  removeorderitem(index) {
    if (index == undefined || index == -1) return;
    let removeorderitem = this.orderitem[index];
    this.orderitem.splice(index, 1);
  }

  getFilter(key: string, orderitem: orderitem) {
    if (key == undefined) return;
    this.orderitemfilterList = [];
    if(key.trim()==''){
      this.orderitemfilterList = [];
    }
    else{
      this.invent.forEach((item, i) => {
        if (item.name.toUpperCase().includes(key.toUpperCase()) && !item.supplier.toUpperCase().includes('SOMRU')   ) {
          this.orderitemfilterList.push({ name: item.name, category: item.category, cat: item.cat, suppliercat: item.suppliercat, supplier: item.supplier, manufacturer: item.manufacturer, unit: item.unit, unitsize: item.unitsize, unitprice:item.unitprice, type:item.type});
        }
      });
    }

    // if (this.orderitemfilterList.length == 0) {
    //   orderitem.name = key;
    // }
  }

  filtername(event:any, order:orderitem){
    let key=event.clipboardData.getData('text/plain');
    this.getFilter(key, order)
  }

  selectorderitem(select: { name: string, cat: string, category: string,suppliercat:string, supplier: string, manufacturer: string, unit: string, unitsize: string, unitprice:string, type:string }, index: number) {
    if (this.orderitem == undefined) return;
    if (index == undefined || index < 0 || index >= this.orderitem.length) return;
    if (this.orderitem[index] == undefined) return;

    this.orderitem[index].name = select.name;
    this.orderitem[index].category = select.category;
    this.orderitem[index].cat = select.cat;
    this.orderitem[index].suppliercat = select.suppliercat;
    this.orderitem[index].supplier = select.supplier;
    this.orderitem[index].manufacturer = select.manufacturer;
    this.orderitem[index].unit = select.unit;
    this.orderitem[index].unitsize = select.unitsize;
    this.orderitem[index].unitprice = select.unitprice;
    this.orderitem[index].type = select.type;
    this.orderitem[index].exist = 'Yes';
    this.orderitemfilterList = undefined;
    this.blockinput=true;
  }

  getFilterbycat(key: string, orderitem: orderitem) {
    if (key == undefined) return;
    this.catfilterList = [];
    if(key.trim()==''){
      this.catfilterList = [];
    }
    else{
      this.invent.forEach((item, i) => {
        if (item.cat.toUpperCase().includes(key.toUpperCase()) && !item.supplier.toUpperCase().includes('SOMRU') ) {
          this.catfilterList.push({ name: item.name, category: item.category, cat: item.cat, suppliercat:item.suppliercat, supplier: item.supplier, manufacturer: item.manufacturer, unit: item.unit, unitsize: item.unitsize, unitprice:item.unitprice, type:item.type});
        }
      });
    }

    // if (this.catfilterList.length == 0) {
    //   orderitem.cat = key;
    // }
  }

  filtercat(event:any, order:orderitem){
    let key=event.clipboardData.getData('text/plain');
    this.getFilterbycat(key, order)
  }


  selectorderitembycat(select: { name: string, cat: string, suppliercat:string, supplier: string, manufacturer: string, unit: string, unitsize: string, unitprice:string, type:string }, index: number) {
    if (this.orderitem == undefined) return;
    if (index == undefined || index < 0 || index >= this.orderitem.length) return;
    if (this.orderitem[index] == undefined) return;

    this.orderitem[index].name = select.name;
    this.orderitem[index].cat = select.cat;
    this.orderitem[index].suppliercat = select.suppliercat;
    this.orderitem[index].supplier = select.supplier;
    this.orderitem[index].manufacturer = select.manufacturer;
    this.orderitem[index].unit = select.unit;
    this.orderitem[index].unitsize = select.unitsize;
    this.orderitem[index].unitprice = select.unitprice;
    this.orderitem[index].type = select.type;
    this.orderitem[index].exist = 'Yes';
    this.orderitemfilterList = undefined;
    this.blockinput=true;

  }

  cancelorder(){
    this.closeorder.emit();
  }

  changeeta(event, orderitem:orderitem) {
    let d = event.formatted;
    if (d == undefined) return;
    orderitem.eta = new Date(d);
  }



}