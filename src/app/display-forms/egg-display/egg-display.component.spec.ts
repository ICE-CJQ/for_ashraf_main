import { async, inject, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import {EggDisplayComponent} from './egg-display.component'
import { Chicken, Injection, Egg } from '../../shared/objects/Chicken';
import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter,  ViewChild, ElementRef, SimpleChange  } from '@angular/core';
import { InventoryService } from 'app/shared/services/Inventory.Service';
import { ChickenService } from '../../shared/services/chicken.Service';
import { SettingService } from '../../shared/services/Setting.Service';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { User } from 'app/shared/objects/User';
import { AuthenticationService } from '../../shared/services/Authenticate.Service'
import { InjectionService } from '../../shared/services/injection.service';
import { EggService } from '../../shared/services/egg.service';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { HttpModule, Http } from "@angular/http";
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/of';
import 'zone.js/dist/async-test';
import 'zone.js/dist/fake-async-test';


xdescribe('Egg-Display Component Integration Test',()=>{
    //Arrange
    let eggcomponent: EggDisplayComponent;
    let fixture: ComponentFixture<EggDisplayComponent>;
    let inventoryService: InventoryService;
    let chickenService: ChickenService;
    let injectionService: InjectionService; 
    let setting: SettingService ;
    let eggService: EggService;
    let authenticationservice: AuthenticationService;
    

    beforeAll(()=>{
        TestBed.configureTestingModule({
            imports: [ FormsModule, MyDatePickerModule, HttpClientModule, HttpClientTestingModule, HttpModule, NgbModule.forRoot()],
            declarations:[EggDisplayComponent],
            providers:[ChickenService, EggService, InjectionService, InventoryService, SettingService, AuthenticationService]
        });

        fixture =  TestBed.createComponent(EggDisplayComponent);
        eggcomponent=fixture.componentInstance;
        injectionService = TestBed.get(InjectionService); 
        inventoryService = TestBed.get(InventoryService);
        chickenService = TestBed.get(ChickenService);
        eggService = TestBed.get(EggService);
        setting = TestBed.get(SettingService);
        authenticationservice = TestBed.get(AuthenticationService);
    });


    it('Calculate days after immunization for new eggs for chicken 11-6', (done)=>{
        chickenService.getChickenByChickenId('11-6').subscribe(chicken=>{
            injectionService.loadInjectionBychickenid(chicken.chickenid).subscribe(injections=>{
                eggcomponent.displayEgg=new Egg(-1, chicken.chickenid, chicken.immunogen, 'Whole Egg', new Date('Feb/23/2019'), new Date(injections[0].injectdate), '', new Date('Feb/22/2019'), new Date('Feb/23/2019'), '2', '', '', null, '', '', 'Chen Fei', new Date());
                eggcomponent.caldaysafterimmune();
                expect(eggcomponent.displayEgg.daysafterimmune).toBe('0-1');
                done();
            })
        })   
    })

    // it('Save new eggs to chicken 11-6', ()=>{
    //     eggcomponent.updateEgg.subscribe(()=>{
    //         eggService.loadEggBychickenid()
    //     });

    //     })













    



})