import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Chicken, Injection, Egg } from 'app/shared/objects/Chicken';
import { item } from 'app/shared/objects/item';
import { InventoryService } from 'app/shared/services/Inventory.Service';
import { ChickenService } from '../../shared/services/chicken.Service';
import { SettingService } from '../../shared/services/Setting.Service';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { User } from 'app/shared/objects/User';
import { AuthenticationService } from '../../shared/services/Authenticate.Service'
import { InjectionService } from '../../shared/services/injection.service';
import { EggService } from '../../shared/services/egg.service';
import { IMyDpOptions } from 'mydatepicker';
import { Location, Sublocation, Concentrationunit } from 'app/shared/objects/SettingObjects';
import { UserhistoryService } from '../../shared/services/userhistory.service';
import { Userhistory } from '../../shared/objects/Userhistory';

@Component({
  selector: 'app-egg-display',
  templateUrl: './egg-display.component.html',
  providers: [InventoryService, SettingService, ChickenService, EggService, UserhistoryService],
  styleUrls: ['./egg-display.component.css']
})
export class EggDisplayComponent implements OnInit {

  @Input() egg: Egg;
  @Input() enableEdit: boolean;
  @Input() isNew: boolean;

  @Output() updateEgg = new EventEmitter();
  @Output() updateeditstatus = new EventEmitter();
  @Output() cancel = new EventEmitter();
  @Output() changeTab = new EventEmitter();
  @Output() selectrecipe = new EventEmitter();

  //cat config

  currentuser: User;
  displayEgg:Egg;
  injections:Injection[]=[];
  eggs:Egg[]=[];
  updatedInjections:Injection[]=[];
  updatedEggs:Egg[]=[];
  eggparts=['Whole Egg','Yolk'];
  finditem:boolean;
  itemfilterList: {name: string, supplier: string, cat: string}[];
  adjuvantfilterList: {name: string, supplier: string, cat: string}[];
  inventList: item[];
  onfocusIndex: number = -1;
  volumeunits:string[];

  

  firstdaysafterimmune='';
  lastdaysafterimmune='';


  collectiondateoptions: IMyDpOptions = { dateFormat: 'mm/dd/yyyy' };
  firstlayeggdateoptions: IMyDpOptions = { dateFormat: 'mm/dd/yyyy' };
  lastlayeggdateoptions: IMyDpOptions = { dateFormat: 'mm/dd/yyyy' };
  frozendateoptions: IMyDpOptions = { dateFormat: 'mm/dd/yyyy' };
  collectiondate: { date: { year: number, month: number, day: number } };
  firstlayeggdate: { date: { year: number, month: number, day: number } };
  lastlayeggdate: { date: { year: number, month: number, day: number } };
  frozendate: { date: { year: number, month: number, day: number } };

  type: ConfigValue[] //= [{ id: 6, name: 'ADA' }, { id: 7, name: 'PK' }, { id: 0, name: 'Antibody' }, { id: 3, name: 'Cell Line' }, { id: 4, name: 'Protein' }, { id: 5, name: 'Misc' }, { id: 8, name: 'Method Transfer' }, { id: 9, name: 'Charecterization kit' }, { id: 10, name: 'Biomaker' }]
  cloneLevel: ConfigValue[] //= [{ id: 1, name: 'monoclonal' }, { id: 2, name: 'polyclonal' }];
  clients: ConfigValue[]// = [{ id: 5, name: 'Intas' }];
  species: ConfigValue[]
  selectType: { id: number, name: string };
  selectClone: { id: number, name: string }// = this.cloneLevel[0];
  isClientSpecific: boolean = false;
  selectClient: { id: number, name: string }// = this.clients[0];
  selectSpecies: { id: number, name: string }// = this.species[0];
  kitMethod: ConfigValue[]
  kitStatus: ConfigValue[]
  vialdescription: ConfigValue[]
  unitList: ConfigValue[];
  volUnit: ConfigValue[];
  massUnit: ConfigValue[];
  otherUnit: ConfigValue[];
  customeKitMethod: string;
  customeKitStatus: string;
  kitsize=[];

  originaleggamount=0;
  yolktiterlist:ConfigValue[]=[];
  locations: ConfigValue[] = [];
  sublocations: ConfigValue[] = [];
  
  constructor(private inventoryService: InventoryService, private chickenService: ChickenService,
    private injectionService: InjectionService, private setting: SettingService, private eggService: EggService, 
    private authenticationservice: AuthenticationService, private userhistoryservice: UserhistoryService) {

  }

  ngOnInit() {   
    let getcurrentuser = this.authenticationservice.getCurrentUser()
    if (getcurrentuser !== undefined) {
      getcurrentuser.then(_ => {
        this.currentuser = User.fromJson(_);
      });
    }
    this.loadSetting();
    this.inventoryService.loadInventory().subscribe(invent => {this.inventList = invent});  
    
  }

  ngOnChanges(change: SimpleChanges) {
    if (change.egg && this.egg !== undefined) {
      this.originaleggamount=Number(this.egg.amount);

      if(this.egg.sublocation){
        this.setting.getSettingByPage('general').subscribe(config => {
          this.locations = [];
          this.locations=ConfigValue.toConfigArray(config.find(x => x.type == 'location').value)
          let target = this.locations.find(x => x.name == this.egg.location);
          this.setting.getSettingByPageAndType('general', 'subLocation').subscribe(subLoc => {
            let subList = ConfigValue.toConfigArray(subLoc.value);
            subList.forEach(sl => {
              if (sl.id == target.id) {
                this.sublocations.push(sl);
              }
            })
          })
        });


      }


        if(this.egg.collectiondate){
          let c = new Date(this.egg.collectiondate)
          this.collectiondate = { date: { year: c.getFullYear(), month: c.getMonth() + 1, day:c.getDate() } };
        }
        if(this.egg.firstlayeggdate){
          let f = new Date(this.egg.firstlayeggdate)
          this.firstlayeggdate = { date: { year: f.getFullYear(), month: f.getMonth() + 1, day:f.getDate() } };
        }
        if(this.egg.lastlayeggdate){
          let l = new Date(this.egg.lastlayeggdate)
          this.lastlayeggdate = { date: { year: l.getFullYear(), month: l.getMonth() + 1, day:l.getDate() } };
        }
        if(this.egg.frozendate){
          let fr = new Date(this.egg.frozendate)
          this.frozendate = { date: { year: fr.getFullYear(), month: fr.getMonth() + 1, day:fr.getDate() } };
        }
        



      this.reset();


      }

  }

  loadSetting(){
    this.setting.getSettingByPage('general').subscribe(config => {
      this.locations = [];
      this.locations=ConfigValue.toConfigArray(config.find(x => x.type == 'location').value)
    });



    this.setting.getSettingByPage('egginventory').subscribe(config => {
      this.yolktiterlist = [];
      this.yolktiterlist = ConfigValue.toConfigArray(config.find(x => x.type == 'titer').value);
    });
  }


  showsublocationoption(l: string) {
    this.sublocations = [];
    let target = this.locations.find(x => x.name == l);
    if (target == undefined) return;
    this.setting.getSettingByPageAndType('general', 'subLocation').subscribe(subLoc => {
      let subList = ConfigValue.toConfigArray(subLoc.value);
      subList.forEach(sl => {
        if (sl.id == target.id) {
          this.sublocations.push(sl);
        }
      })
    })
  }
  
  reset() {
    this.displayEgg = Egg.newEgg(this.egg);
    this.isNew ? '' : this.enableEdit = false;
  }



  caldaysafterimmune(){
    this.firstdaysafterimmune=Math.ceil((( new Date(this.displayEgg.firstlayeggdate).getTime()   - new Date(this.displayEgg.firstinjectdate).getTime() )/(1000*60*60*24)  )).toString() ;
    this.lastdaysafterimmune=Math.ceil(((new Date(this.displayEgg.lastlayeggdate).getTime()   - new Date(this.displayEgg.firstinjectdate).getTime() )/(1000*60*60*24)  )).toString() ;

    if(this.firstdaysafterimmune == this.lastdaysafterimmune){
      this.displayEgg.daysafterimmune=this.firstdaysafterimmune;
    }
    else{
      this.displayEgg.daysafterimmune=this.firstdaysafterimmune+'-'+this.lastdaysafterimmune;
    }


  }

  validateuserinput(): boolean {
    if(!this.displayEgg.amount || this.displayEgg.amount.trim()=='' || this.displayEgg.amount.trim()=='0'){return false;}
    
    if(!this.displayEgg.firstlayeggdate){return false;}
    if(!this.displayEgg.lastlayeggdate){return false;}
    if(!this.displayEgg.location){return false;}
    if(this.displayEgg.eggpart && this.displayEgg.eggpart.trim()=='Whole Egg'){
      if(!this.displayEgg.collectiondate){return false;}
    }
    else if(this.displayEgg.eggpart && this.displayEgg.eggpart.trim()=='Yolk'){
      if(!this.displayEgg.frozendate){return false;}
      // if(!this.displayEgg.addsucrose || this.displayEgg.addsucrose.trim()==''){return false;}
      // if(!this.displayEgg.titer || this.displayEgg.titer.trim()==''){return false;}

    }

    return true;
  }

  //save whole kit information include kit components
  save() {
    //search all the kits to see if the catalog number and name are unique
    if (this.validateuserinput()) {
      this.injectionService.loadInjectionBychickenid(this.displayEgg.chickenid).subscribe(injections=>{
        if(!this.displayEgg.firstinjectdate){
          this.displayEgg.firstinjectdate=injections[0].injectdate;
        }

        this.caldaysafterimmune();
        this.egg = this.displayEgg;
        this.enableEdit = false;
        this.isNew=false;

        this.chickenService.getChickenByChickenId(this.egg.chickenid).subscribe(chicken=>{
          if(this.displayEgg.dbid==-1){
            this.eggService.addEgg(this.egg).then(_=>{
              let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Egg Inventory', 'Add Eggs to a Chicken', 'Chicken Id: '+chicken.chickenid+', Immnogen: '+chicken.immunogen+'\n'+', Egg Part: '+this.egg.eggpart+', Egg Collection Date: '+this.egg.collectiondate.toDateString())
              this.userhistoryservice.addUserHistory(userhistory)
              this.egg.dbid=_['dbid'];
              chicken.totalegg= (Number(chicken.totalegg)+Number(this.egg.amount)).toString();
              this.chickenService.updateChickenInfo(chicken).then(result=>{
                this.updateEgg.emit({egg:this.egg,chicken:chicken} );
              })
              
            })
          }
          else{
            this.eggService.updateSingleEgg(this.egg).then(()=>{
              // let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Egg Inventory', 'Update an Egg of a Chicken', 'Chicken Id: '+chicken.chickenid+', Immnogen: '+chicken.immunogen+'\n'+'Egg Part: '+this.egg.eggpart+', Egg Collection Date: '+this.egg.collectiondate.toDateString())
              // this.userhistoryservice.addUserHistory(userhistory)
              chicken.totalegg= (Number(chicken.totalegg)-this.originaleggamount+Number(this.egg.amount)).toString();
              this.chickenService.updateChickenInfo(chicken).then(result=>{
                this.updateEgg.emit({egg:this.egg,chicken:chicken} );
              })
            })
          }
    
          })

      })




      
    }
  }

  cancelEdit(){
    this.reset();
    this.cancel.emit();
  }


  changecollectiondate(event) {
    let d = event.formatted;
    if (d == undefined) return;
    this.displayEgg.collectiondate = new Date(d);
  }

  changefirstlayeggdate(event) {
    let d = event.formatted;
    if (d == undefined) return;
    this.displayEgg.firstlayeggdate = new Date(d);

    // if(this.displayEgg.lastlayeggdate){}
  }

  changelastlayeggdate(event) {
    let d = event.formatted;
    if (d == undefined) return;
    this.displayEgg.lastlayeggdate = new Date(d);
    // if(this.displayEgg.firstlayeggdate){}
  }

  changefrozendate(event){
    let d = event.formatted;
    if (d == undefined) return;
    this.displayEgg.frozendate = new Date(d);
  }

  checklocation(location:string){
    if(location.toUpperCase().includes('LOCAION')){
      this.displayEgg.location='';
    }
    else{
      this.displayEgg.location=location;
    }
  }

  checksublocation(sublocation:string){
    if(sublocation.toUpperCase().includes('SUBLOCAION')){
      this.displayEgg.sublocation='';
    }
    else{
      this.displayEgg.sublocation=sublocation;
    }
  }

}
