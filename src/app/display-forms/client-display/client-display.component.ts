import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { ClientCompany, ClientContact, CompanyWContacts } from '../../shared/objects/Client';
import { ClientService } from '../../shared/services/Client.Service';
import { User } from 'app/shared/objects/User';
import { AuthenticationService } from '../../shared/services/Authenticate.Service'

@Component({
  providers: [ClientService],
  selector: 'app-client-display',
  templateUrl: './client-display.component.html',
  styleUrls: ['./client-display.component.css']
})
export class ClientDisplayComponent implements OnInit, OnChanges {
  @Input() selectClient: CompanyWContacts;
  @Input() isNew: boolean = false;
  @Output() cancel = new EventEmitter();
  @Output() updateClient = new EventEmitter();

  enableEdit: boolean = false;
  validEmail: boolean = true;
  validPhone: boolean = true;
  validInput: boolean = true;
  displayClient: ClientCompany;
  isSameAsShip: boolean = false;
  contact: ClientContact[] = [];
  clients: ClientCompany[];
  currentuser: User;

  constructor(private clientService: ClientService,private authenticationservice: AuthenticationService) {
    this.clientService.loadClient().subscribe(c => {
      this.clients = c;
    })
  }

  ngOnInit() {
        let getcurrentuser = this.authenticationservice.getCurrentUser()
    if (getcurrentuser !== undefined) {
      getcurrentuser.then(_ => {
        this.currentuser = User.fromJson(_);
      });
    }
  }

  ngOnChanges(change: SimpleChanges) {
    if (change.selectClient && this.selectClient !== undefined) {
      this.reset();
    }
  }

  newContact() {
    if (this.contact == undefined) this.contact = [];
    this.contact.push(new ClientContact(-1, this.displayClient.dbid, "", "", "", "", "", "", this.currentuser.name, new Date()));
  }

  removeContact(index: number) {
    if (index == undefined || index < 0 || index > this.contact.length) return;
    if (this.contact[index] == undefined) return;
    this.contact.splice(index, 1);
  }

  validateEmail(index: number, email: string) {
    if (index == undefined || index < 0 || index > this.contact.length) return;
    if (this.contact[index] == undefined) return;

    if (email == undefined || email == '') {
      this.validEmail = false;
      return;
    }

    // if (email.match(/[a-zA-Z0-9]+[\_\-\.]*[a-zA-Z0-9]+@\w+\.\w+/) == undefined) {
    //   this.validEmail = false;
    //   return;
    // }

    
    if (email.match(/[a-zA-Z0-9]+[\_\-\.]*[a-zA-Z0-9]+@\D\S+\.\w+/) == undefined) {
      this.validEmail = false;
      return;
    }

    this.contact[index].email = email;
    this.validEmail = true;
  }

  validatePhone(index: number, phone: string) {
    if (index == undefined || index < 0 || index > this.contact.length) return;
    if (this.contact[index] == undefined) return;
    if (phone == undefined || phone == '') {
      this.validPhone = false;
      return;
    }

    this.contact[index].phone = phone;
    this.validPhone = true;
  }

  save() {
    this.validateInput()
    if (!this.validInput) {
      return;
    }

    this.enableEdit = false;
    this.updateClient.emit({ com: this.displayClient, contact: this.contact });
  }

  validateInput() {
    if (this.displayClient.company == undefined || this.displayClient.company.trim() == '') {
      this.validInput = false;
      return;
    }
    if (this.clients.find(x => x.company == this.displayClient.company && x.dbid !== this.displayClient.dbid) !== undefined) {
      this.validInput = false;
      return;
    }

    if (this.displayClient.address == undefined || this.displayClient.address.trim() == '') {
      this.validInput = false;
      return;
    }
    if (this.displayClient.city == undefined || this.displayClient.city.trim() == '') {
      this.validInput = false;
      return;
    }
    if (this.displayClient.country == undefined || this.displayClient.country.trim() == '') {
      this.validInput = false;
      return;
    }
    if (this.displayClient.baddress == undefined || this.displayClient.baddress.trim() == '') {
      this.validInput = false;
      return;
    }
    if (this.displayClient.bcity == undefined || this.displayClient.bcity.trim() == '') {
      this.validInput = false;
      return;
    }
    if (this.displayClient.bcountry == undefined || this.displayClient.bcountry.trim() == '') {
      this.validInput = false;
      return;
    }

    this.contact.forEach(c => {
      if (c.name == undefined || c.name.trim() == '') {
        this.validInput = false;
        return;
      }
    })
    this.validInput = true;
  }

  setBillAddress() {
    this.isSameAsShip = !this.isSameAsShip;
    if (this.isSameAsShip) {
      this.displayClient.baddress = this.displayClient.address;
      this.displayClient.baddress2 = this.displayClient.address2;
      this.displayClient.bcity = this.displayClient.city;
      this.displayClient.bprovince = this.displayClient.province;
      this.displayClient.bcountry = this.displayClient.country;
      this.displayClient.bpostalCode = this.displayClient.postalCode;
    }
  }

  reset() {
    this.displayClient = ClientCompany.newCompany(this.selectClient.company);
    this.contact = [];
    if (this.selectClient.contacts !== undefined)
      this.selectClient.contacts.forEach(c => {
        if (c !== undefined) this.contact.push(c);
      });

    if (this.displayClient.address == this.displayClient.baddress && !this.isNew) {
      this.isSameAsShip == true;
    }
    else {
      this.isSameAsShip = false;
    }

    if (this.isNew) {
      this.enableEdit = true;
      this.newContact();
    }
  }

  cancelEdit() {
    this.enableEdit = false;
    if (this.isNew)
      this.cancel.emit();
  }
}
