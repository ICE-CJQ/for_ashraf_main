 import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { InventoryService } from '../../shared/services/Inventory.Service';
import { ClientService } from '../../shared/services/Client.Service';
import { KitService } from '../../shared/services/Kit.Service';
import { Quote, SaleLink } from '../../shared/objects/Quote';
import { ClientCompany, ClientContact } from '../../shared/objects/Client';
import { item } from '../../shared/objects/item';
import { Kit } from '../../shared/objects/Kit';
import { SaleLinkService } from '../../shared/services/Sale.Link.Service';
import { IMyDpOptions } from 'mydatepicker';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { image } from '../../shared/objects/Image';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { SettingService } from '../../shared/services/Setting.Service';
import { AuthenticationService } from '../../shared/services/Authenticate.Service';
import { User } from '../../shared/objects/User';
import { Product } from 'app/shared/objects/Product';
import { ProductService } from '../../shared/services/product.service';

declare var jsPDF: any

@Component({
  providers: [InventoryService, ClientService, KitService, SettingService, ProductService],
  selector: 'app-quote-display',
  templateUrl: './quote-display.component.html',
  styleUrls: ['./quote-display.component.css']
})
export class QuoteDisplayComponent implements OnInit, OnChanges {
  @Input() quote: Quote;
  @Input() enableEdit: boolean;
  @Input() isnew: boolean;
  @Output() updateQuote = new EventEmitter();
  @Output() cancel = new EventEmitter();
  @Output() completeQuote = new EventEmitter();

  displayQuote: Quote;
  myDatePickerOptions: IMyDpOptions = { dateFormat: 'mm/dd/yyyy' };
  cDate: { date: { year: number, month: number, day: number } };
  eDate: { date: { year: number, month: number, day: number } };
  clientList: ClientContact[] = [];
  showClientFilter: boolean;
  companies: ClientCompany[] = [];
  representative: ClientContact[] = [];
  kits: Kit[] = [];
  item: item[] = [];
  paymentType: ConfigValue[];
  selectClient: ClientCompany;
  selectSaleRep: ClientContact;
  selectclientcompany='';
  selectLink: SaleLink[];
  quoteLinks: SaleLink[];
  orderitem: { cat: string, type: string, id: number, name: string, price: number, size: string }[];
  filterOrderItem: { cat: string, type: string, id: number, name: string, price: number, size: string }[]
  showfilter: number;
  message: string;
  termAndCond: string;
  isInnoProgram: boolean;
  shipDate: string;
  note: string;
  isExpire: boolean;
  isLoading: boolean;
  cur: User;
  taxRate: ConfigValue[];
  includefeeandtax:boolean;
  selectRate: string;
  rate: number;
  isTax: boolean;
  currencyOptions=['USD', 'CAD', 'EURO'];
  currencySigns=['$', '$', '€'];
  constructor(private kitservice: KitService, private itemservice: InventoryService,
    private clientservice: ClientService, private salelinkservice: SaleLinkService,
    private modalService: NgbModal, private setting: SettingService,
    private auth: AuthenticationService, private productservice:ProductService) { }

  ngOnInit() {

    this.auth.getCurrentUser().then(_ => {
      this.cur = _;
    })

    this.loadSetting();
    this.loadAllClient();
    this.loadAllOrderItem();
    
  }

  ngOnChanges(change: SimpleChanges) {
    if (change.quote && this.quote !== undefined) {
      //this.isLoading = true;
      this.reset();
      if (this.isnew) {
        this.quoteLinks = [];
        this.selectLink = [];
        this.enableEdit = true;
        this.isLoading = false;
        this.clientservice.getClientById(1).subscribe(client=>{
          this.selectClient=client;
          this.clientservice.getContactByCompany(1).subscribe(salerep=>{
            this.representative=salerep.slice();
            this.selectSaleRep=this.representative[0];
          })
        })
      }
      else{
        if(this.displayQuote.shippingfee!=0 || this.displayQuote.handlefee!=0 || this.displayQuote.handlefee!=0){
          this.includefeeandtax=true;
        }
        this.clientservice.getClientById(this.displayQuote.clientid).subscribe(client=>{
          this.selectClient=client;
          this.clientservice.getContactByCompany(this.displayQuote.clientid).subscribe(contacts=>{
            this.representative=contacts.slice();
            this.clientservice.getContactById(this.displayQuote.saleRepid).subscribe(salerep=>{
              this.selectSaleRep=salerep;
            })
          })

        })
      }

      this.selectLink = [];
      this.quoteLinks = [];
      if (this.quote.dbid !== -1) {
        this.salelinkservice.loadAllLinkById(this.quote.dbid, 'quote').subscribe(l => {
          l.forEach(link => {
            if (link.linkType == 'quote') {
              this.selectLink.push(SaleLink.newLink(link));
              this.quoteLinks.push(SaleLink.newLink(link));
            }
          });
        });
      }
    }
  }

  // ngDoCheck() {
  //   console.log('b')
  //   if (this.quote.dbid !== -1 && this.isLoading) {
  //     if (this.selectClient == undefined && this.companies!=undefined) {
  //       console.log('undefiend')
  //       this.setClient(this.quote.clientid);
  //       console.log(this.selectClient)

  //     }
  //     if (this.selectSaleRep == undefined) {
  //       this.setSaleRep(this.quote.saleRepid);

  //     }
  //     if (this.selectClient !== undefined && (!this.isnew && this.selectClient.dbid == this.displayQuote.clientid && this.selectClient.dbid == this.selectSaleRep.companyId) && this.selectLink !== undefined && this.selectLink.length > 0) {
  //       this.isLoading = false;
  //     }

  //     this.isLoading = false;
  //   }
  // }

  loadSetting(){
    this.setting.getSettingByPage("sale").subscribe(config => {
      this.paymentType = ConfigValue.toConfigArray(config.find(x => x.type == 'paymentTerms').value)
      if (this.paymentType == []) this.paymentType = Setting.getPaymentTerm();
      this.taxRate = ConfigValue.toConfigArray(config.find(x => x.type == 'tax').value);
      if(this.taxRate == []) this.taxRate = Setting.getTaxRate();
    })



  }

  loadAllClient() {
    this.clientservice.loadContact().subscribe(contact => {
      this.clientList = contact;
    });
    this.clientservice.loadClient().subscribe(clients => {
      this.companies = [];
      clients.forEach(c => {
        if (this.companies.findIndex(x => x.company == c.company) == -1) {
          this.companies.push(ClientCompany.newCompany(c));
        }
      });
      //this.getSaleRepresentative(this.companies[0].dbid);
    });

  }

  loadAllOrderItem() {
    this.orderitem = [];
    this.kitservice.loadKit().subscribe(kits => {
      kits.forEach(k => {
        if (k.cat !== undefined && k.cat.trim() !== '' &&
          k.dbid !== -1 &&
          k.name !== undefined && k.name.trim() !== '' &&
          k.price !== undefined &&
          k.size !== undefined && k.size.trim() !== '') {
          this.orderitem.push({ cat: k.cat, type: 'kit', id: k.dbid, name: k.name, price: Number(k.price), size: k.size });
        }
      });
      this.filterOrderItem = this.orderitem.slice();
    });
    // this.itemservice.loadInventory().subscribe(invent => {
    //   invent.forEach(i => {
    //     if (i.cat !== undefined && i.cat.trim() !== '' &&
    //       i.dbid !== -1 &&
    //       i.name !== undefined && i.name.trim() !== '' &&
    //       i.unitprice !== undefined &&
    //       !isNaN(Number(i.unitsize)) && Number(i.unitsize) >= 0 &&
    //       i.unit !== undefined &&
    //       i.supplier == 'Somru BioScience Inc.') {
    //       this.orderitem.push({ cat: i.cat, type: 'inventory', id: i.dbid, name: i.name, price: Number(i.unitprice), size: i.unitsize + '' + i.unit });
    //     }
    //   });
    //   this.filterOrderItem = this.orderitem.slice();
    // });

    this.productservice.loadProduct().subscribe(products=>{
      products.forEach(product=>{
        if(product.cat !== undefined && product.cat.trim() !== '' &&
        product.dbid !== -1 &&
        product.name !== undefined && product.name.trim() !== ''){
          this.orderitem.push({ cat: product.cat, type: 'product', id: product.dbid, name: product.name, price: Number(product.unitprice), size: product.unitsize + ' ' + product.unit });
        }
      });
      this.filterOrderItem = this.orderitem.slice();
    });




  }

  open(content, modalName: string) {
    if (modalName == 'pdf') {
      let tac = new TermsAndCondition(this.displayQuote.paymentType);
      this.termAndCond = tac.tac;
      this.isInnoProgram = false;
      this.shipDate = '3-5';
    }

    this.modalService.open(content).result.then((result) => {
    }, (reason) => {
    });
  }

  reset() {
    this.isExpire = false;
    this.displayQuote = Quote.newQuote(this.quote);
    let c = new Date(this.quote.createDate);
    let e = new Date(this.quote.expireDate);
    this.cDate = { date: { year: c.getFullYear(), month: c.getMonth() + 1, day: c.getDate() } };
    this.eDate = { date: { year: e.getFullYear(), month: e.getMonth() + 1, day: e.getDate() } };
    this.isExpired();
    this.selectClient = undefined;
    this.selectSaleRep = undefined;
    this.selectLink = [];
  }

  // setClient(companyId: number) {
  //   if (companyId == undefined) return;
  //   let c = this.companies.find(x => x.dbid == companyId)
  //   if (c !== undefined) {
  //     this.selectClient = c;
  //     this.displayQuote.clientid = companyId;
  //     //this.getSaleRepresentative(companyId);
  //     if(this.isnew) {
  //       this.setSaleRep(this.representative[0].id);}
  //   }

  // }




  filterByCat(key: string) {
    if (key == undefined || key == '') this.filterOrderItem = this.orderitem.slice();
    this.filterOrderItem = [];
    this.orderitem.forEach(i => {
      if (i.cat.includes(key)) {
        this.filterOrderItem.push(i);
      }
    });
  }

  removeLink(index: number) {
    if (this.selectLink == undefined || this.selectLink.length < 1) return;
    if (isNaN(index) || index < 0 || index >= this.selectLink.length) return;
    this.selectLink.splice(index, 1);

  }

  addQuoteLink() {
    if (this.selectLink == undefined) this.selectLink = [];
    this.selectLink.push(new SaleLink(-1, this.quote.dbid, 'quote', '', -1, '', '', 0, '', 0, 1, '', this.cur.name, new Date()));
  }

  setSaleLinkItem(linkindex: number, item: { cat: string, type: string, id: number, name: string, price: number, size: string }) {
    if (this.selectLink == undefined || this.selectLink.length < 1 || this.selectLink.length <= linkindex) return;
    if (item == undefined) return;
    this.selectLink[linkindex].cat = item.cat;
    this.selectLink[linkindex].itemId = item.id;
    this.selectLink[linkindex].name = item.name;
    this.selectLink[linkindex].itemType = item.type;
    this.selectLink[linkindex].price = item.price;
    this.selectLink[linkindex].size = item.size;
    this.showfilter = undefined;
  }

  setDiscount(linkindex: number, discount: number) {
    if (this.selectLink == undefined || this.selectLink.length < 1 || this.selectLink.length <= linkindex) return;
    if (isNaN(discount)) discount = 0;
    if (discount < 0) discount = Math.abs(discount);
    this.selectLink[linkindex].itemDiscount = discount;
  }

  setQuantity(linkindex: number, amount: number) {
    if (this.selectLink == undefined || this.selectLink.length < 1 || this.selectLink.length <= linkindex) return;
    if (isNaN(amount)) amount = 1;
    if (amount < 1) amount = 1;
    this.selectLink[linkindex].itemQuan = amount;
  }

  setComplete() {
    this.displayQuote.complete = true;
    this.completeQuote.emit(this.displayQuote);
  }

  hideFilter() {
    setTimeout(_ => { this.showfilter = undefined; }, 200)
  }

  checkFootnote(note: string, linkindex: number) {
    if (linkindex == undefined || linkindex < 0) return;
    if (this.selectLink[linkindex] == undefined) return;
    if (note == undefined) note = '';
    if (note.length > 500) note = note.substr(0, 500);
    this.selectLink[linkindex].footnote = note;
  }

  checkTAC(tac: string) {
    if (tac == undefined || tac.trim() == '') {
      return;
    }

    this.termAndCond = tac;
  }

  isExpired() {
    if (this.displayQuote == undefined) { this.isExpire = true; return; };
    let today = new Date();
    if (today >= this.displayQuote.expireDate && !this.displayQuote.complete) { this.isExpire = true; return; }
    this.isExpire = false;
    return;
  }

  onDateChanged(type: string, event) {
    let d = event.formatted;
    if (d == undefined) return;

    if (type == 'cd') {
      if (new Date(d) >= this.displayQuote.expireDate) {
        this.message = "Please make sure the create date is earlier than the expiry date";
        setTimeout(_ => { this.message = undefined }, 3000);
        return;
      }
      this.displayQuote.createDate = new Date(d);
    }
    else if (type == 'ed') {
      if (new Date(d) <= this.displayQuote.expireDate) {
        this.message = "Please make sure the create date is earlier than the expiry date";
        setTimeout(_ => { this.message = undefined }, 3000);
        return;
      }
      this.displayQuote.expireDate = new Date(d);
    }
  }

  checkForChanges(): boolean {
    console.log(this.displayQuote.currency);
    console.log(this.quote.currency);
    if (this.displayQuote.expireDate !== this.quote.expireDate) return true;
    if (this.displayQuote.createDate !== this.quote.createDate) return true;
    if (this.displayQuote.paymentType !== this.quote.paymentType) return true;
    if (this.displayQuote.clientid !== this.quote.clientid) return true;
    if (this.displayQuote.saleRepid !== this.quote.saleRepid) return true;
    if (this.displayQuote.quoteNum !== this.quote.quoteNum) return true;
    if (this.displayQuote.complete !== this.quote.complete) return true;
    if (this.displayQuote.note !== this.quote.note) return true;
    if (this.displayQuote.currency !== this.quote.currency) return true;
    if (this.displayQuote.currencyrate !== this.quote.currencyrate) return true;

    if (this.selectLink.length !== this.quoteLinks.length) return true;

    if (this.displayQuote.handlefee !== this.quote.handlefee) return true;
    if (this.displayQuote.shippingfee !== this.quote.shippingfee) return true;
    if (this.displayQuote.taxrate !== this.quote.taxrate) return true;

    let hc = false;

    for (let i = 0; i < this.selectLink.length; i++) {
      let link = SaleLink.newLink(this.selectLink[i]);
      let original = this.quoteLinks.find(x => x.dbid == link.dbid);
      if (original == undefined) { hc = true; return true; };
      if (original.itemId !== link.itemId) { hc = true; return true; };
      if (original.itemDiscount !== link.itemDiscount) { hc = true; return true; };
      if (original.itemType !== link.itemType) { hc = true; return true; };
      if (original.itemQuan !== link.itemQuan) { hc = true; return true; };
      if (original.footnote !== link.footnote) { hc = true; return true; };
    }

    return false;
  }

  validate(): boolean {
    if (this.displayQuote.clientid == -1 || this.displayQuote.saleRepid == -1) {
      this.message = 'Please make sure you select a Company and Requisitioner';
      setTimeout(_ => { this.message = undefined }, 3000);
      return false;
    }

    if (this.displayQuote.createDate >= this.displayQuote.expireDate) {
      this.message = 'Please make sure you have the expiry date set later than create date';
      setTimeout(_ => { this.message = undefined }, 3000);
      return false;
    }

    if (this.selectLink == undefined || this.selectLink.length < 1) {
      this.message = 'Please add at least an item to the quote';
      setTimeout(_ => { this.message = undefined }, 3000);
      return false;
    }

    if(this.includefeeandtax==undefined){
      this.message = 'Please select include shipping, handling and tax?';
      setTimeout(_ => { this.message = undefined }, 3000);
      return false;
    }

    return true;
  }

  save() {
    console.log(this.displayQuote.currencyrate);
    if (!this.validate()) return;
    if (!this.checkForChanges()) {
      this.enableEdit = false;
      return;
    }
    this.updateQuote.emit({ q: this.displayQuote, link: this.selectLink });

    this.isnew = false;
    this.enableEdit = false;
  }

  cancelEdit() {
    this.reset();
    this.enableEdit = false;
    if (this.isnew) this.cancel.emit();
  }




  // getSaleRepresentative(company) {
  //   // if (isNaN(companyid)) return;
  //   if (company=undefined) return;
  //   this.selectClient = this.companies.find(x => x.company == company);
  //   this.representative = [];
  //   if (this.selectClient == undefined) return;
  //   this.displayQuote.clientid = this.selectClient.dbid;
  //   this.displayQuote.saleRepid = -1;
  //   this.clientList.forEach(c => {
  //     if (this.selectClient.dbid == c.companyId) {
  //       this.representative.push(c);
  //     }
  //   });
  //   console.log(this.representative )
  //   if (this.representative.length > 0)
  //     this.selectSaleRep=this.representative[0];
  // }

  //   setSaleRep(salename) {
  //   if (salename == undefined) return;
  //   let c = this.representative.find(x => x.name == salename)
  //   if (c !== undefined) {
  //     this.selectSaleRep = c;
  //     this.displayQuote.saleRepid = c.dbid; 
  //   }
  // }


  getSaleRepresentative(companyid: number) {
    if (isNaN(companyid)) return;
    this.clientservice.getClientById(companyid).subscribe(company=>{
      this.selectClient=company;
      this.representative = [];
      this.displayQuote.clientid = this.selectClient.dbid;

      this.clientservice.getContactByCompany(companyid).subscribe(contacts=>{
        this.representative = contacts.slice();
        this.selectSaleRep = this.representative[0];
        this.displayQuote.saleRepid = this.selectSaleRep.dbid;
      });
    })
  }

  setSaleRep(saleId: number) {
    if (saleId == undefined) return;
    let c = this.representative.find(x => x.dbid == saleId)
    if (c !== undefined) {
      this.selectSaleRep = c;
      this.displayQuote.saleRepid = saleId; 
    }
  }

  // setTaxRate(rate: string) {
  //   if (rate == undefined) rate = 'No Tax';
  //   this.isTax = false;
  //   this.rate = 0;
  //   this.selectRate = rate;
  //   if (rate !== 'No Tax') {
  //     this.isTax = true;
  //     this.rate = Number(rate.split('-')[1].trim().replace('%', '')) / 100;
  //   }
  // }

  setTax() {
    if(this.displayQuote.taxrate!=undefined && this.displayQuote.taxrate!='No Tax'){
      this.rate = Number(this.displayQuote.taxrate.split('-')[1].trim().replace('%', '')) / 100;
      
      let total = 0;
      this.selectLink.forEach(link => {
        let rate = 1 - (link.itemDiscount / 100);
        total += ((link.price * link.itemQuan) * rate);
      });

      this.displayQuote.tax = (Number(total) + Number(this.displayQuote.handlefee)+Number(this.displayQuote.shippingfee)) * this.rate;
    }
    else{
      this.displayQuote.tax = 0;
    }


  }

  splitstring(information):string[]{
    let wordsarray=information.split(' ');
    let eachline='';
    let companyarray=[];
    wordsarray.forEach( (word, index)=>{
      if(index%4!=0 || index==0){
        eachline=eachline+word+' '
      }
      else if(index%3==0 && index!=0){
        eachline=eachline+word
      }
      else{
        companyarray.push(eachline);
        eachline='';
        eachline=eachline+word+' '
      }
    }  );

    companyarray.push(eachline);
    return companyarray;
  }

  getQuotePDF() {
    var doc = new jsPDF();
    let imgData = image.logo;
    let width = doc.internal.pageSize.getWidth();
    let height = doc.internal.pageSize.getHeight();
    let margin = 5;
    let tablewidth=Math.floor((width - (2 * margin)) / 3);
    let emptyHead = [{ title: '', dataKey: 'header' }, { title: '', dataKey: 'content' }];
    // doc.lines([[width - (2*margin),0]] , margin, 13);
    doc.addImage(imgData, 'JPEG', margin, 10, 60, 23);
    let headmargin = (((width - (margin * 2)) / 3) * 2) + margin;
    let ed = new Date(this.displayQuote.expireDate);
    let today = new Date();
    let month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let headData = [
      { header: 'QUOTE #', content: this.displayQuote.quoteNum },
      { header: 'Version #', content: this.displayQuote.revision },
      { header: 'EXPIRY DATE: ', content: ed.getDate() + ' ' + month[ed.getMonth()] + ' ' + ed.getFullYear() },
      { header: 'QUOTE STATUS: ', content: this.displayQuote.complete ? 'Completed' : today > ed ? 'Expired' : 'Active' }
    ];
    doc.autoTable(emptyHead, headData, {
      theme: 'plain',
      styles: {
        cellPadding: 0,
        fontSize: 9,
        textColor: [0, 132, 183]
      },
      headerStyles: {
        fontStyle: 'normal',
        fontSize: 0
      },
      columnStyles: {
        header: {
          fontStyle: 'bold'
        }
      },
      startY: 13, // false (indicates margin top value) or a number
      pageBreak: 'auto', // 'auto', 'avoid' or 'always'
      tableWidth: (width - (2 * margin)) / 3, // 'auto', 'wrap' or a number, 
      tableLineColor: 200, // number, array (see color section below)
      tableLineWidth: 0,
      showHeader: 'never',
      margin: headmargin,
    });

    doc.setDrawColor(151, 202, 65);
    doc.lines([[width - (2 * margin), 0]], margin, 35);
    let linepointer = 36;
    let nextpointer = 36;
    let col1Margin = margin;
    let col2Margin = margin + (width - (2 * margin)) / 3;
    let col3Margin = margin + 2 * ((width - (2 * margin)) / 3);
    let salerep = 'Susan Holland';
    if (this.cur !== undefined) {
      salerep = this.cur.name;
    }
    if (salerep == undefined) salerep = 'Susan Holland'
    let clientAddress = '';
    let clientTable = [[this.selectSaleRep.name]];
    if (this.selectSaleRep.role !== undefined && this.selectSaleRep.role.trim() !== '' && this.selectSaleRep.role.trim() !== '-'){
      // if(this.selectSaleRep.role.length>4){
      //   let inforarray=this.splitstring(this.selectSaleRep.role);
      //   inforarray.forEach(line=>{
      //     clientTable.push([line])
      //   });
      // }
      // else{
      //   clientTable.push([this.selectSaleRep.role])
      // }

      clientTable.push([this.selectSaleRep.role])

      
    }
      
    clientAddress = this.selectClient.address.trim();
    //clientAddress = clientAddress.replace(/, /g, ',\n').trim();

    if (this.selectClient.address2 !== undefined && this.selectClient.address2.trim() != '' && this.selectClient.address2.trim() != '-'){
      let address2=this.selectClient.address2.trim();
      //address2 = address2.replace(/, /g, ',\n').trim();
      clientAddress += '\n' + address2.trim();
    }

    clientAddress += '\n' + this.selectClient.city.trim();
    if (this.selectClient.province !== undefined && this.selectClient.province.trim() != '' && this.selectClient.province.trim() != '-'){
      clientAddress += ', ' + this.selectClient.province.trim();
    }

    clientAddress += '\n' + this.selectClient.country.trim();
    if (this.selectClient.postalCode !== undefined && this.selectClient.postalCode.trim() != '' && this.selectClient.postalCode.trim() != '-'){
      clientAddress += '   ' + this.selectClient.postalCode.trim();
    }

    // if(this.selectClient.company.length>4){
    //   let inforarray=this.splitstring(this.selectClient.company);
    //   inforarray.forEach(line=>{
    //     clientTable.push([line])
    //   });
    // }
    // else{
    //   clientTable.push([this.selectClient.company])
    // }


    clientTable.push([this.selectClient.company])
    clientTable.push([clientAddress])
    if (this.selectSaleRep.phone !== undefined && this.selectSaleRep.phone.trim() !== '')
      clientTable.push(['Phone: ' + this.selectSaleRep.phone])
    if (this.selectSaleRep.email !== undefined && this.selectSaleRep.email.trim() !== '')
      clientTable.push(['Email: ' + this.selectSaleRep.email])
    doc.autoTable(['Requisitioner Information:'], clientTable, {
      theme: 'plain',
      styles: {
        fontSize: 9,
        textColor: 0,
      },
      headerStyles: {
        cellPadding: [2, 0],
        fontStyle: 'bold'
      },
      bodyStyles: {
        cellPadding: 0
      },
      startY: linepointer, // false (indicates margin top value) or a number
      pageBreak: 'auto', // 'auto', 'avoid' or 'always'
      tableWidth: Math.floor((width - (2 * margin)) / 3)+10, // 'auto', 'wrap' or a number, 
      tableLineColor: 200, // number, array (see color section below)
      tableLineWidth: 0,
      margin: col1Margin,
      drawRow: function (row, data) {
        nextpointer = Math.max(nextpointer, row.y + (row.height / 2));
      }
    });

    let cd = new Date(this.displayQuote.createDate);
    let rvd = new Date(this.displayQuote.revisionDate);
    doc.autoTable(['Quote Details:'], [
      ['Quote Date: ' + cd.getDate() + ' ' + month[cd.getMonth()] + ' ' + cd.getFullYear()],
      ['Revision Date: ' + rvd.getDate() + ' ' + month[rvd.getMonth()] + ' ' + rvd.getFullYear()],
      ['Issue By: ' + salerep]
    ], {
        theme: 'plain',
        styles: {
          fontSize: 9,
          textColor: 0,
        },
        headerStyles: {
          cellPadding: [2, 0],
          fontStyle: 'bold'
        },
        bodyStyles: {
          cellPadding: 0
        },
        startY: linepointer, // false (indicates margin top value) or a number
        pageBreak: 'auto', // 'auto', 'avoid' or 'always'
        tableWidth: Math.floor((width - (2 * margin)) / 3)-10, // 'auto', 'wrap' or a number, 
        tableLineColor: 200, // number, array (see color section below)
        tableLineWidth: 0,
        margin: col2Margin+10,
        drawRow: function (row, data) {
          nextpointer = Math.max(nextpointer, row.y + (row.height / 2));
        }
      });

    doc.autoTable(['Contact Info:'], [
      ['Sale Rep.: ' + salerep],
      ['Phone: 1 (902) 367-4322'],
      ['Email: customer@somrubioscience.com']
    ], {
        theme: 'plain',
        styles: {
          fontSize: 9,
          textColor: 0,
        },
        headerStyles: {
          cellPadding: [2, 0],
          fontStyle: 'bold'
        },
        bodyStyles: {
          cellPadding: 0
        },
        startY: linepointer, // false (indicates margin top value) or a number
        pageBreak: 'auto', // 'auto', 'avoid' or 'always'
        tableWidth: Math.floor((width - (2 * margin)) / 3), // 'auto', 'wrap' or a number, 
        tableLineColor: 200, // number, array (see color section below)
        tableLineWidth: 0,
        margin: col3Margin,
        drawRow: function (row, data) {
          linepointer = row.y + (row.height / 2);
        }
      });
    linepointer += 2;
    doc.autoTable(['Somru BioScience Info:'], [
      ['19 Innovation Way'],
      ['Charlottetown, PE'],
      ['C1E 0B7  Canada'],
      ['Phone: 1 (902) 367-4322'],
      ['Email: customer@somrubioscience.com']
    ], {
        theme: 'plain',
        styles: {
          fontSize: 9,
          textColor: 0,
        },
        headerStyles: {
          cellPadding: [2, 0],
          fontStyle: 'bold'
        },
        bodyStyles: {
          cellPadding: 0
        },
        startY: linepointer, // false (indicates margin top value) or a number
        pageBreak: 'auto', // 'auto', 'avoid' or 'always'
        tableWidth: Math.floor((width - (2 * margin)) / 3), // 'auto', 'wrap' or a number, 
        tableLineColor: 200, // number, array (see color section below)
        tableLineWidth: 0,
        margin: col3Margin,
        drawRow: function (row, data) {
          nextpointer = Math.max(nextpointer, row.y + (row.height / 2));
        }
      });

    linepointer = nextpointer + 10;

    let quotedata = this.getDataFromLink();
    let header = quotedata.conclude.hasDisc ? [{ title: 'Catalog #', dataKey: 'cat' },
    { title: 'Name', dataKey: 'des' },
    { title: 'Qty', dataKey: 'quan' },
    { title: 'List Price', dataKey: 'lprice' },
    { title: 'Disc.', dataKey: 'discount' },
    // { title: 'Your Price', dataKey: 'aprice' },
    { title: 'Price', dataKey: 'aprice' },
    { title: 'Extended', dataKey: 'tprice' }] : [
        { title: 'Catalog #', dataKey: 'cat' },
        { title: 'Name', dataKey: 'des' },
        { title: 'Qty', dataKey: 'quan' },
        { title: 'List Price', dataKey: 'lprice' },
        { title: 'Extended', dataKey: 'tprice' }
      ]
    doc.autoTable(header, quotedata.data, {
      theme: 'grid', // 'striped', 'grid' or 'plain'
      styles: {
        halign: 'center',
        valign: 'middle',
        font: 'times',
        fontSize: 9,
        textColor: 0,
        overflow: 'linebreak',
        lineColor: [151, 202, 65],
        fillColor: 255
      },
      // rowStyles:{
      //   rowHeight:'auto',

      // },
      columnStyles: {
        // quan: { columnWidth: 'auto', halign: 'center' },
        // des: { columnWidth: 'auto', halign: 'left', fillColor: false },
        // discount: { columnWidth: 'auto', halign: 'center' },
        // cat: { columnWidth: 'auto', halign: 'center' },
        // lprice: { columnWidth: 'auto', halign: 'center' },
        // tprice: { columnWidth: 'auto', halign: 'center' },
        // aprice: { columnWidth: 'auto', halign: 'center' },

        // quan: { columnWidth: 8, halign: 'center' },
        // des: { columnWidth: 'auto', halign: 'center', fillColor: false },
        // discount: { columnWidth: 10, halign: 'center' },
        // cat: { columnWidth: 20, halign: 'center' },
        // lprice: { columnWidth: 15, halign: 'center' },
        // tprice: { columnWidth: 20, halign: 'center' },
        // aprice: { columnWidth: 15, halign: 'center' },

        quan: { columnWidth: 10, halign: 'center' },
        des: { columnWidth: 'auto', halign: 'center', fillColor: false },
        discount: { columnWidth: 10, halign: 'center' },
        cat: { columnWidth: 35, halign: 'center' },
        lprice: { columnWidth: 20, halign: 'center' },
        tprice: { columnWidth: 20, halign: 'center' },
        aprice: { columnWidth: 20, halign: 'center' },
      },
      headerStyles: {
        fillColor: [151, 202, 65]
      },
      startY: linepointer, // false (indicates margin top value) or a number
      pageBreak: 'auto', // 'auto', 'avoid' or 'always'
      tableWidth: width - (2 * margin), // 'auto', 'wrap' or a number, 
      tableLineColor: [151, 202, 65], // number, array (see color section below)
      tableLineWidth: 0,
      margin: margin,
      drawHeaderRow: function (row, data) {
        row.cells.quan.text = 'Qty';
        row.cells.quan.styles.halign = 'center';
        row.cells.lprice.styles.halign = 'center';
        row.cells.tprice.styles.halign = 'center';
        row.cells.des.styles.halign = 'left';
        row.cells.cat.styles.halign = 'center';
        if (quotedata.conclude.hasDisc) {
          row.cells.discount.styles.halign = 'center';
          row.cells.aprice.styles.halign = 'center';
        }
      },
      drawRow: function (row, data) {
        linepointer = row.y + row.height;
        
        if (row.cells.aprice !== undefined) {
          row.cells.aprice.styles.columnWidth = 'wrap';
          row.cells.aprice.styles.halign = 'center';
        }

        if (row.cells.discount !== undefined) {
          row.cells.discount.styles.columnWidth = 'wrap';
          row.cells.discount.styles.halign = 'center';
        }
      },

      drawCell: function (cell, data) {
        if (cell.text.findIndex(x => x.includes('Name: ')) == 0 && cell.text.findIndex(x => x.includes('Size: ')) != -1) {
          let nameArray=[];
          if(cell.text[1]!=undefined && cell.text[1]!=''){
            nameArray.push(cell.text[0].replace('Name: ', ''));
            nameArray.push(cell.text[1]);
          }
          else{
            nameArray.push(cell.text[0].replace('Name: ', ''));
          }
          doc.setDrawColor(151, 202, 65);
          doc.setFillColor(255, 255, 255);
          doc.rect(cell.x, cell.y, cell.width, cell.height, 'DF');
          let note=[];
          let lp;
          let text = cell.raw.trim().split('\n');
          cell.text = "";

          if(text.length>3){
            let i;
            for(i=3;i<text.length;i++){
              let splitnote = doc.splitTextToSize(text[i].replace('Note: ', ''), cell.width-15);
              splitnote.forEach(line=>{
                note.push(line)
              })
            }

            // if(text.length>4){
            //   //lp = (cell.height / (text.length+note.length-2.5));
            //   console.log('text length is: '+text.length)
            //   console.log('note length is: '+note.length)
            //   lp = (cell.height / (4+note.length-1));
            // }
            // else{
            //   //lp = (cell.height / (text.length+note.length-1));
            //   console.log('text length is: '+text.length)
            //   console.log('note length is: '+note.length)
            //   lp = (cell.height / (4+note.length-1));
            // }
            
            lp = (cell.height / (4+note.length-1));

          }
          else{
            lp = (cell.height / text.length);
          }
          doc.setFontStyle('bold')
          doc.text('Name: ', cell.x + cell.styles.cellPadding, cell.y + lp)
          doc.setFontStyle('normal')
          if(nameArray.length==0){
            doc.text(text[0].replace('Name: ', ''), cell.x + cell.styles.cellPadding + 10, cell.y + lp);
            doc.setFontStyle('bold')
            doc.text('Size: ', cell.x + cell.styles.cellPadding, 2 + cell.y + 2 * lp)
            doc.setFontStyle('normal')
            doc.text(text[2].replace('Size: ', ''), cell.x + cell.styles.cellPadding + 10, 2 + cell.y + 2 * lp)
            if (text.length>3) {
              doc.setFontStyle('bold')
              doc.text('Note: ', cell.x + cell.styles.cellPadding, 6 + cell.y + 2 * lp)
              doc.setFontStyle('normal')
              doc.text(note, cell.x + cell.styles.cellPadding + 10, 6 + cell.y + 2 * lp)
            }

          }
          else{
            doc.text(nameArray, cell.x + cell.styles.cellPadding + 10, cell.y + lp);
            let l=nameArray.length;
            let height=(l+0.5) * lp;
            doc.setFontStyle('bold')
            doc.text('Size: ', cell.x + cell.styles.cellPadding, 2 + cell.y + height)
            doc.setFontStyle('normal')
            doc.text(text[2].replace('Size: ', ''), cell.x + cell.styles.cellPadding + 10, 2 + cell.y + height)
            if (text.length>3) {
              doc.setFontStyle('bold')
              doc.text('Note: ', cell.x + cell.styles.cellPadding, 6 + cell.y + height)
              doc.setFontStyle('normal')
              doc.text(note, cell.x + cell.styles.cellPadding + 10, 6 + cell.y + height)
            }


          }


        }
      }
    });

    let totalhead = [{ title: '', dataKey: 'text' }, { title: '', dataKey: 'amount' }]
    let totaldata = []
    let rowcount = 0;

    if (quotedata.conclude.hasDisc) {
      totaldata.push({ text: 'List Total Price:', amount: '$' +   this.formatMoney(quotedata.conclude.totalLP) });
      totaldata.push({ text: 'Discount Total:', amount: '$' + this.formatMoney(quotedata.conclude.totalD) });
      rowcount += 2;
    }
    totaldata.push({ text: 'Subtotal:', amount: '$' + this.formatMoney(quotedata.conclude.totalC) });

      if(this.displayQuote.handlefee!=null && this.displayQuote.handlefee!=0 && this.displayQuote.shippingfee!=null && this.displayQuote.shippingfee!=0){
        totaldata.push({ text: 'Estimated Shipping and Handling Fee:', amount: '$' + this.formatMoney(this.displayQuote.handlefee+this.displayQuote.shippingfee) });
        rowcount += 1;
      }
      else if(this.displayQuote.handlefee!=null && this.displayQuote.handlefee!=0 && (this.displayQuote.shippingfee==null || this.displayQuote.shippingfee==0)   ){
        totaldata.push({ text: 'Estimated Handling Fee:', amount: '$' + this.formatMoney(this.displayQuote.handlefee) });
        rowcount += 1;
      }
      else if(this.displayQuote.shippingfee!=null && this.displayQuote.shippingfee!=0 && (this.displayQuote.handlefee==null || this.displayQuote.handlefee==0)   ){
        totaldata.push({ text: 'Estimated Shipping Fee:', amount: '$' + this.formatMoney(this.displayQuote.shippingfee) });
        rowcount += 1;
      }
      else{
        totaldata.push({ text: 'Estimated Shipping and Handling Fee:', amount: 'TBD' });
        rowcount += 1;
      }
      


    if (this.displayQuote.tax!=undefined && this.displayQuote.tax!=null && this.displayQuote.tax !== 0) {
      totaldata.push({ text: 'Tax:', amount: '$' + this.formatMoney(this.displayQuote.tax) });
      totaldata.push({ text: 'Total (USD):', amount: '$' + this.formatMoney(quotedata.conclude.totalC + this.displayQuote.handlefee + this.displayQuote.shippingfee  + this.displayQuote.tax) });
      rowcount += 2;

      if(this.displayQuote.currency && this.displayQuote.currency!='USD' && this.displayQuote.currencyrate){
        let currencyIndex = this.currencyOptions.indexOf(this.displayQuote.currency);
        totaldata.push({ text: 'Currency Rate ('+this.displayQuote.currency+'):', amount: this.formatMoney(this.displayQuote.currencyrate) });
        rowcount += 2;
        totaldata.push({ text: 'Total ('+this.displayQuote.currency+'):', amount: this.currencySigns[currencyIndex]+this.formatMoney(   (quotedata.conclude.totalC + this.displayQuote.handlefee + this.displayQuote.shippingfee  + this.displayQuote.tax)*this.displayQuote.currencyrate         ) });
        rowcount += 2;
      }


    }
    else {
      totaldata.push({ text: 'Total (USD):', amount: '$' + this.formatMoney(quotedata.conclude.totalC + this.displayQuote.handlefee + this.displayQuote.shippingfee) });
      rowcount += 1;
      if(this.displayQuote.currency && this.displayQuote.currency!='USD' && this.displayQuote.currencyrate){
        let currencyIndex = this.currencyOptions.indexOf(this.displayQuote.currency);
        console.log(this.displayQuote.currency)
        console.log(currencyIndex)
        totaldata.push({ text: 'Currency Rate ('+this.displayQuote.currency+'):', amount: this.formatMoney(this.displayQuote.currencyrate) });
        rowcount += 1;
        totaldata.push({ text: 'Total ('+this.displayQuote.currency+'):', amount: this.currencySigns[currencyIndex]+this.formatMoney((quotedata.conclude.totalC + this.displayQuote.handlefee + this.displayQuote.shippingfee)*this.displayQuote.currencyrate         ) });
        rowcount += 1;
      }
    }



    doc.autoTable(totalhead, totaldata, {
      theme: 'plain',
      styles: {
        halign: 'right',
        valign: 'middle',
        font: 'times',
        fontSize: 9,
        textColor: 0,
        overflow: 'linebreak',
      },
      columnStyles: {
        text: {
          columnWidth: 2 * ((width - (2 * margin)) / 3),
          cellPadding: 0
        },
        amount: {
          columnWidth: ((width - (2 * margin)) / 3)
        }
      },
      // Properties
      startY: linepointer, // false (indicates margin top value) or a number
      pageBreak: 'auto', // 'auto', 'avoid' or 'always'
      tableWidth: width - (2 * margin), // 'auto', 'wrap' or a number, 
      tableLineColor: [151, 202, 65], // number, array (see color section below)
      tableLineWidth: 0.1,
      showHeader: 'never',
      margin: margin,
      drawRow: function (row, data) {
        linepointer = row.y + row.height;
        // if (row.index == rowcount) {
        //   row.cells.text.styles.fontStyle = 'bold';
        //   row.cells.amount.styles.fontStyle = 'bold';
        // }
        if (row.index % 2 == 0) {
          row.cells.text.styles.fillColor = [224, 234, 207];
          row.cells.amount.styles.fillColor = [224, 234, 207];
        }
      }
    });



    // linepointer += 4;
    // doc.setFontSize(7);
    // doc.setTextColor(100);
    // doc.setFontStyle('normal')
    // doc.text('* In US Dollars (USD)', margin, linepointer);

    doc.setFontSize(9);
    linepointer += 8;
    if (this.displayQuote.note !== null && this.displayQuote.note.trim() !== '') {
      doc.setTextColor(0, 132, 183);
      doc.setFontStyle('bold');
      doc.text('Please Note: ', margin, linepointer);
      doc.setFontSize(8);
      doc.setFontStyle('italic');
      let n = doc.splitTextToSize(this.displayQuote.note, 150);
      doc.text(n, margin + 30, linepointer);
      linepointer += (5 * n.length) - 1;
      doc.setTextColor(255);
      doc.text('a ', margin, linepointer);
      linepointer += 3;
    }

    if(   (height - linepointer)  < 50  ){
      doc.addPage();
      linepointer=15;
    }

    doc.setTextColor(100);
    doc.setFontStyle('bold');
    doc.text('Payment Terms: ', margin, linepointer);

    doc.setFontSize(8);
    doc.setFontStyle('normal');
    doc.text(this.displayQuote.paymentType, margin + 30, linepointer);

    linepointer += 5;

    doc.setFontSize(9);
    doc.setFontStyle('bold');
    doc.text('Terms and Conditions: ', margin, linepointer);
    linepointer += 5;

    doc.setFontSize(8);
    doc.setFontStyle('normal');
    if (this.isInnoProgram) {
      doc.text('- Innovator program data sharing discount.', margin, linepointer);
      linepointer += 4;
    }

    let lineTAC = this.termAndCond.split('\n');
    lineTAC.forEach(line => {
      let lt = doc.splitTextToSize(line, width - (2 * margin));
      doc.text(lt, margin, linepointer);
      linepointer += (4 * lt.length) - (1 * (lt.length - 1));
    });

    let footerHead = ['If you have any questions concerning this price quote, please contact our sales team. sales@somrubioscience.com']
    let footerData = [['THANK YOU FOR YOUR BUSINESS!']];
    doc.autoTable(footerHead, footerData, {
      theme: 'plain', // 'striped', 'grid' or 'plain'
      styles: {
        halign: 'center',
        valign: 'middle',
        font: 'times',
        fontSize: 9,
        overflow: 'linebreak',
        fillColor: false,
        textColor: 0
      },
      headerStyles: {
        fontStyle: 'italic',
      },
      bodyStyles: {
        fontStyle: 'bold',
        fontSize: 10
      },
      // Properties
      startY: linepointer, // false (indicates margin top value) or a number
      pageBreak: 'avoid', // 'auto', 'avoid' or 'always'
      tableWidth: width - (2 * margin), // 'auto', 'wrap' or a number, 
      tableLineColor: 200, // number, array (see color section below)
      tableLineWidth: 0,
      margin: margin
    });
	this.assignPageNum(doc);
    doc.save('Somru Quote ' + this.displayQuote.quoteNum + '.pdf');
  }

		assignPageNum(doc: any) {
    let totalPages = doc.internal.getNumberOfPages();
    let timestamp = new Date().toString();
    for (let i = 1; i <= totalPages; i++) {
      doc.setPage(i);
      doc.setFontStyle('bold');
      doc.text(200,295,i + ' of ' + totalPages);
    }
  }

  getDataFromLink(): { data: any[], conclude: { totalD: number, totalC: number, totalLP: number, hasDisc: boolean } } {
    let data = [];
    if (this.selectLink == undefined) return;
    let td = 0;
    let tc = 0;
    let tlp = 0;
    let hasDiscCount = 0;
    this.selectLink.forEach(link => {
      if (link.itemDiscount > 0) hasDiscCount++;
      let discountamount = link.price * (link.itemDiscount / 100);
      td += discountamount * link.itemQuan;
      let adjustprice = link.price * (1 - (link.itemDiscount / 100));
      let total = (link.price * link.itemQuan) * (1 - (link.itemDiscount / 100));
      tlp += link.price * link.itemQuan
      tc += total; 
      let name =
        data.push({ cat: link.cat, des: 'Name: ' + link.name + '\n\nSize: ' + link.size + (link.footnote == undefined || link.footnote.trim() == '' ? '' : '\nNote: ' + link.footnote), quan: link.itemQuan, lprice: '$' + this.formatMoney(link.price), discount: link.itemDiscount + '%', aprice: '$' + this.formatMoney(adjustprice), tprice: '$' + this.formatMoney(total) });
    });

    return { data: data, conclude: { totalD: td, totalC: tc, totalLP: tlp, hasDisc: hasDiscCount != 0 } }
  }


  formatMoney(val: number): string {
    let format = val.toFixed(2);
    format = format.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return format;
  }
}

export class TermsAndCondition {
  public tac: string;

  constructor(paymentType: string) {
    if(paymentType=='Prepayment'){
      this.tac = 
     `      - Product will be shipped in 5-7 business day(s) after receiving prepayment.
      - The listed discount(s) and/or prices will be applied when the quote number is provided at time of order.
      - All products must be ordered collectively under one purchase order number for the discount to apply.
      - Applicable taxes, import duties and delivery charges are additional unless stated otherwise. 
      - Shipping via FedEx unless otherwise requested. All goods ship via Incoterms EXW - Exworks.
      - Please see the following link for the full details of our sales agreement. http://somrubioscience.com/page/standard-terms-and-conditions-of-sale`
    }
    else{
      this.tac = 
     `      - Product will be shipped in 5-7 business day(s) after receiving PO.
      - The listed discount(s) and/or prices will be applied when the quote number is provided at time of order.
      - All products must be ordered collectively under one purchase order number for the discount to apply.
      - Applicable taxes, import duties and delivery charges are additional unless stated otherwise. 
      - Shipping via FedEx unless otherwise requested. All goods ship via Incoterms EXW - Exworks.
      - Please see the following link for the full details of our sales agreement. http://somrubioscience.com/page/standard-terms-and-conditions-of-sale`
    }



  }
}
