import { Component, OnInit, Input, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';
import { Invoice } from '../../shared/objects/Invoice';
import { ClientCompany, ClientContact } from '../../shared/objects/Client';
import { Quote, SaleLink } from '../../shared/objects/Quote';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SaleLinkService } from '../../shared/services/Sale.Link.Service';
import { ClientService } from '../../shared/services/Client.Service';
import { InventoryService } from '../../shared/services/Inventory.Service';
import { KitService } from '../../shared/services/Kit.Service';
import { QuoteService } from '../../shared/services/Quote.Service';
import { InvoiceService } from '../../shared/services/Invoice.Service';
import { image } from '../../shared/objects/Image';
import { SettingService } from '../../shared/services/Setting.Service';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { AuthenticationService } from '../../shared/services/Authenticate.Service';
import { User } from '../../shared/objects/User';
import { Product } from 'app/shared/objects/Product';
import { ProductService } from '../../shared/services/product.service';

declare var jsPDF: any
@Component({
  providers: [ClientService, InventoryService, KitService, SaleLinkService, InvoiceService, QuoteService, SettingService, ProductService],
  selector: 'app-invoice-display',
  templateUrl: './invoice-display.component.html',
  styleUrls: ['./invoice-display.component.css']
})
export class InvoiceDisplayComponent implements OnInit, OnChanges {
  @Input() invoice: Invoice;
  @Input() isWithQuote: boolean;
  @Input() enableEdit: boolean;
  @Input() isnew: boolean;

  @Output() update = new EventEmitter();
  @Output() cancel = new EventEmitter();

  invoiceList: Invoice[];
  companies: ClientCompany[] = [];
  clientList: ClientContact[] = [];
  completeQuotes: Quote[];
  selectLink: SaleLink[] = [];
  originalLinks: SaleLink[] = [];
  selectQuote: Quote;
  orderitem: { cat: string, type: string, id: number, name: string, price: number, size: string }[];
  filterOrderItem: { cat: string, type: string, id: number, name: string, price: number, size: string }[]
  showfilter: number;
  showClientFilter: string;
  isSameAsShip: boolean;
  messincoTermage: string;
  paymentTerms: ConfigValue[]
  incoTerm: ConfigValue[]
  taxRate: ConfigValue[]
  selectRate: string;
  rate: number;
  isTax: boolean;
  message: string;
  note: string;
  paymentInfo: string;
  filterCLients: ClientContact[];
  isLoading: boolean;
  cur: User;
  displayInvoice: Invoice;
  nextQuote: string;
  nextInvoice: string;
  invoiceoption=[];
  selectClient:ClientCompany;
  selectContacts:ClientContact[]=[];
  selectContact:ClientContact;
  selectbillAttn:ClientContact;
  selectshipAttn:ClientContact;
  currencyOptions=['USD', 'CAD', 'EURO'];
  currencySigns=['$', '$', '€'];

  constructor(private modalService: NgbModal, private salelinkservice: SaleLinkService,
    private clientservice: ClientService, private inventservice: InventoryService,
    private kitservice: KitService, private quoteservice: QuoteService,
    private invoiceservice: InvoiceService, private setting: SettingService,
    private auth: AuthenticationService, private productservice:ProductService) { }

  ngOnInit() {
    this.auth.getCurrentUser().then(_ => {
      this.cur = _;
    })

    this.loadConfig();
    this.loadAllClient();
    this.loadAllOrderItem();
    this.loadCompleteQuote();
    this.invoiceList = [];
    this.invoiceservice.loadInvoice().subscribe(i => {
      this.invoiceList = i;
    })
  }

  ngOnChanges(change: SimpleChanges) {
    if (change.invoice && this.invoice !== undefined) {
      this.isLoading = true;
      if(!this.isnew && this.invoice.quoteId && this.invoice.quoteId!=0){
        this.quoteservice.getQuoteById(this.invoice.quoteId).subscribe(quote=>{
          this.selectQuote=quote;
        })
      } 
      
      if(!this.isnew && this.invoice.clientId && this.invoice.clientId!=0){
        this.clientservice.getClientById(this.invoice.clientId).subscribe(client=>{
          this.selectClient = client;
          this.clientservice.getContactByCompany(this.selectClient.dbid).subscribe(contacts=>{
            this.selectContacts=contacts;

            if(!this.isnew && this.invoice.requisitioner && this.invoice.requisitioner!=''){
              this.selectContact = this.selectContacts.find(x=>x.name.trim()==this.invoice.requisitioner.trim())
            }
      
            if(!this.isnew && this.invoice.billAttn && this.invoice.billAttn!=''){
              this.selectbillAttn = this.selectContacts.find(x=>x.name.trim()==this.invoice.billAttn.trim())
            }
      
            if(!this.isnew && this.invoice.shipAttn && this.invoice.shipAttn!=''){
              this.selectshipAttn = this.selectContacts.find(x=>x.name.trim()==this.invoice.shipAttn.trim())
            }


          })
        })
      }

      // if(!this.isnew && this.invoice.requisitioner && this.invoice.requisitioner!=''){
      //   this.selectContact=this.invoice.requisitioner
      // }




      this.reset();
    }
  }

  ngDoCheck() {
    if (this.isLoading && this.invoice.dbid !== -1) {
      if (this.selectLink !== undefined && this.selectLink.length > 0) {
        this.isLoading = false;
      }
    }
  }

  loadConfig() {
    this.setting.getSettingByPage("sale").subscribe(config => {
      this.paymentTerms = ConfigValue.toConfigArray(config.find(x => x.type == 'paymentTerms').value);
      if(this.paymentTerms == []) this.paymentTerms = Setting.getPaymentTerm();
      this.incoTerm = ConfigValue.toConfigArray(config.find(x => x.type == 'incoTerm').value);
      if(this.incoTerm == []) this.paymentTerms = Setting.getIncoTerm();
      this.taxRate = ConfigValue.toConfigArray(config.find(x => x.type == 'tax').value);
      if(this.taxRate == []) this.paymentTerms = Setting.getTaxRate();
    });

    this.setting.getSettingByPage("invoice").subscribe(
      config => {
        this.invoiceoption = ConfigValue.toConfigArray(config.find(x => x.type == 'invoiceoption').value);
      });
  }

  loadAllClient() {
    this.clientservice.loadContact().subscribe(contact => { this.clientList = contact; });
    this.clientservice.loadClient().subscribe(clients => {
      this.companies = [];
      clients.forEach(c => {
        if (this.companies.findIndex(x => x.company == c.company) == -1) {
          this.companies.push(ClientCompany.newCompany(c));
        }
      });
    });
  }

  loadAllOrderItem() {
    this.orderitem = [];
    this.kitservice.loadKit().subscribe(kits => {
      kits.forEach(k => {
        if (k.cat !== undefined && k.cat.trim() !== '' &&
          k.dbid !== -1 &&
          k.name !== undefined && k.name.trim() !== '' &&
          k.price !== undefined &&
          k.size !== undefined && k.size.trim() !== '') {
          this.orderitem.push({ cat: k.cat, type: 'kit', id: k.dbid, name: k.name, price: Number(k.price), size: k.size });
        }
      });
      this.filterOrderItem = this.orderitem.slice();
    });
    // this.inventservice.loadInventory().subscribe(invent => {
    //   invent.forEach(i => {
    //     if (i.cat !== undefined && i.cat.trim() !== '' &&
    //       i.dbid !== -1 &&
    //       i.name !== undefined && i.name.trim() !== '' &&
    //       i.unitprice !== undefined &&
    //       !isNaN(Number(i.unitsize)) && Number(i.unitsize) >= 0 &&
    //       i.unit !== undefined &&
    //       i.supplier == 'Somru BioScience Inc.') {
    //       this.orderitem.push({ cat: i.cat, type: 'inventory', id: i.dbid, name: i.name, price: Number(i.unitprice), size: i.unitsize + '' + i.unit });
    //     }
    //   });
    //   this.filterOrderItem = this.orderitem.slice();
    // });

    this.productservice.loadProduct().subscribe(products=>{
      products.forEach(product=>{
        if(product.cat !== undefined && product.cat.trim() !== '' &&
        product.dbid !== -1 &&
        product.name !== undefined && product.name.trim() !== ''){
          if(product.packsize){
            this.orderitem.push({ cat: product.cat, type: 'product', id: product.dbid, name: product.name, price: Number(product.unitprice), size: product.packsize+'x'+product.unitsize + '' + product.unit });
          }
          else{
            this.orderitem.push({ cat: product.cat, type: 'product', id: product.dbid, name: product.name, price: Number(product.unitprice), size: product.unitsize + ' ' + product.unit });
          }
          
        }
      });
      this.filterOrderItem = this.orderitem.slice();
    });
  }

  loadCompleteQuote() {
    this.completeQuotes = [];
    this.quoteservice.loadCompletedQuote().subscribe(q => {
      this.completeQuotes = q;
    });

  }

  changeproform(){
    if(this.displayInvoice.proform=='Yes'){
      this.displayInvoice.proform='No'
    }
    else{
      this.displayInvoice.proform='Yes'
    }
  }
 
  open(content, modalName: string) {
    if (modalName == 'pdf') {
      this.paymentInfo = `Somru BioScience Inc. -- Payment must be made via wire transfer. 
Bank details for wire transfer:
Bank Name: Scotia Bank
Bank Address: 135 St. Peters Road, Charlottetown, PE, C1A 5P3
Routing Number: 026002532
Account Number: 510030035211
Swift code: NOSCCATT`;
      this.note = '';
    }

    this.modalService.open(content).result.then((result) => {
    }, (reason) => {
    });
  }

  removeLink(index: number) {
    if (this.selectLink == undefined || this.selectLink.length < 1) return;
    if (isNaN(index) || index < 0 || index >= this.selectLink.length) return;
    this.selectLink.splice(index, 1);
  }

  addQuoteLink() {
    if (this.selectLink == undefined) this.selectLink = [];
    this.selectLink.push(new SaleLink(-1, this.invoice.invoiceNum, 'invoice', '', -1, '', '', 0, '', 0, 1, '', this.cur.name, new Date() ));
  }

  addServiceLink() {
    if (this.selectLink == undefined) this.selectLink = [];
    this.selectLink.push(new SaleLink(-1, this.invoice.invoiceNum, 'invoice', 'Service', 0, 'no', '', 0, 'no', 0, 1, '', this.cur.name, new Date()));
  }


  loadQuoteInfo(quoteId: number) {
    if (isNaN(quoteId)) return;
    if (this.completeQuotes == undefined) return;
    this.selectQuote = this.completeQuotes.find(x => x.dbid == quoteId);
    if (this.selectQuote == undefined || this.selectQuote.dbid == undefined) return;

    this.displayInvoice.quoteId = this.selectQuote.dbid;
    this.displayInvoice.paymentTerm = this.selectQuote.paymentType;
    this.displayInvoice.shippingfee = this.selectQuote.shippingfee;
    this.displayInvoice.handleFee = this.selectQuote.handlefee;
    this.displayInvoice.tax = this.selectQuote.tax;
    this.selectRate=this.selectQuote.taxrate;
    this.selectLink = [];
    this.originalLinks=[];
    this.salelinkservice.loadAllLinkById(this.selectQuote.dbid, 'quote').subscribe(links => {
      links.forEach(link => {
        if (link.linkType == 'quote') {
          link.dbid = -1;
          link.linkType = 'invoice';
          this.selectLink.push(SaleLink.newLink(link));
          this.originalLinks.push(SaleLink.newLink(link));
        }
      });
    })

    this.selectClient = this.companies.find(x => x.dbid == this.selectQuote.clientid);
    this.displayInvoice.clientId = this.selectClient.dbid;
    this.clientservice.getContactByCompany(this.selectQuote.clientid).subscribe(contacts=>{
      this.selectContacts=contacts;
      this.selectContact = this.selectContacts.find(x => x.dbid == this.selectQuote.saleRepid);
      if (this.selectClient !== undefined && this.selectContact !== undefined) {
        this.selectbillAttn = this.selectContact;
        this.selectshipAttn = this.selectContact;
        let address = this.getFormatAddress(this.selectClient);
        this.displayInvoice.requisitioner = this.selectContact.name;
        
        this.displayInvoice.billAttn = this.selectContact.name;

        this.displayInvoice.shipAttn = this.selectContact.name;
        this.displayInvoice.shipAddress = address.ship;
  
        if (this.selectClient.baddress == undefined || this.selectClient.baddress.trim() == '') {
          this.isSameAsShip = true;
          this.displayInvoice.billAddress = this.displayInvoice.shipAddress;
        }
        else {
          this.isSameAsShip = false;
          this.displayInvoice.billAddress = address.bill;
        }
  
      }



    })






  }

  setClient(companyid: number) {
    if (isNaN(companyid)) return;
    this.clientservice.getClientById(companyid).subscribe(company=>{
      this.selectClient=company;
      this.selectContacts = [];
      this.displayInvoice.clientId = this.selectClient.dbid;
      this.clientservice.getContactByCompany(companyid).subscribe(contacts=>{
        this.selectContacts = contacts.slice();
        this.selectContact = this.selectContacts[0];
        this.displayInvoice.requisitioner = this.selectContact.name;
        this.displayInvoice.billAttn = this.selectContact.name;
        this.displayInvoice.shipAttn = this.selectContact.name;
        this.displayInvoice.billAddress = this.getFormatAddress(this.selectClient).bill;
        this.displayInvoice.shipAddress = this.getFormatAddress(this.selectClient).ship;        
      });
    })
  }

  setRequisitioner(ReId: number) {
    if (ReId == undefined) return;
    let c = this.selectContacts.find(x => x.dbid == ReId)
    if (c !== undefined) {
      this.selectContact = c;
      this.displayInvoice.requisitioner = c.name; 
    }
  }

  setbillAttn(billId: number){
    if (billId == undefined) return;
    let c = this.selectContacts.find(x => x.dbid == billId)
    if (c !== undefined) {
      this.selectbillAttn = c;
      this.displayInvoice.billAttn = c.name; 
    }
  }

  setshipAttn(shipId: number){
    if (shipId == undefined) return;
    let c = this.selectContacts.find(x => x.dbid == shipId)
    if (c !== undefined) {
      this.selectshipAttn = c;
      this.displayInvoice.shipAttn = c.name; 
    }
  }

  setTaxRate(rate: string) {
    this.isTax = false;
    this.rate = 0;
    if (rate == undefined) {
      rate = 'No Tax';
    }
    else if(rate == 'No Tax'){
    }
    else{
      this.isTax = true;
      this.rate = Number(rate.split('-')[1].trim().replace('%', '')) / 100;
      
    }
    this.selectRate = rate;
  }

  setDiscount(linkindex: number, discount: number) {
    if (this.selectLink == undefined || this.selectLink.length < 1 || this.selectLink.length <= linkindex) return;
    if (isNaN(discount)) discount = 0;
    if (discount < 0) discount = Math.abs(discount);
    this.selectLink[linkindex].itemDiscount = discount;
    this.setTax();
  }

  setQuoteLinkItem(linkindex: number, item: { cat: string, type: string, id: number, name: string, price: number, size: string }) {
    if (this.selectLink == undefined || this.selectLink.length < 1 || this.selectLink.length <= linkindex) return;
    if (item == undefined) return;
    this.selectLink[linkindex].cat = item.cat;
    this.selectLink[linkindex].itemId = item.id;
    this.selectLink[linkindex].name = item.name;
    this.selectLink[linkindex].itemType = item.type;
    this.selectLink[linkindex].price = item.price;
    this.selectLink[linkindex].size = item.size;
    this.showfilter = undefined;
    this.setTax();
  }

  setHandleFee(fee: number) {
    if (isNaN(fee)) {
      this.displayInvoice.handleFee = 0;
      return;
    }

    this.displayInvoice.handleFee = Number(fee);
    // this.setTax();
  }

  setShippingFee(fee: number) {
    if (isNaN(fee)) {
      this.displayInvoice.shippingfee = 0;
      return;
    }

    this.displayInvoice.shippingfee = Number(fee);
    // this.setTax();
  }

  setQuantity(linkindex: number, amount: number) {
    if (this.selectLink == undefined || this.selectLink.length < 1 || this.selectLink.length <= linkindex) return;
    if (isNaN(amount)) amount = 1;
    if (amount < 1) amount = 1;
    this.selectLink[linkindex].itemQuan = amount;
    this.setTax();
  }

  setShipping() {
    this.isSameAsShip = !this.isSameAsShip;

    if (this.isSameAsShip) {
      this.displayInvoice.shipAttn = this.displayInvoice.billAttn;
      this.displayInvoice.shipAddress = this.displayInvoice.billAddress;
    }
    else{
      this.displayInvoice.shipAttn=this.selectContacts[0].name
      this.displayInvoice.shipAddress=this.getFormatAddress(this.selectClient).ship;
    }
  }

  setClientInfo(c: ClientContact) {
    if (this.showClientFilter == 'bill') {
      this.displayInvoice.billAttn = c.name;
      let com = this.companies.find(x => x.dbid == c.companyId);
      if (com == undefined) return;
      this.displayInvoice.billAddress = this.getFormatAddress(com).bill;
    }
    else if (this.showClientFilter == 'ship') {
      this.displayInvoice.shipAttn = c.name;
      let com = this.companies.find(x => x.dbid == c.companyId);
      if (com == undefined) return;
      this.displayInvoice.shipAddress = this.getFormatAddress(com).ship;
    }

    setTimeout(_ => { this.showClientFilter = undefined }, 100);
  }

  setTax() {
    if (this.rate!=undefined && this.rate!=0) {
      let total = 0;
      this.selectLink.forEach(link => {
        let rate = 1 - (link.itemDiscount / 100);
        total += ((link.price * link.itemQuan) * rate);
      });

      this.displayInvoice.tax = (total + this.displayInvoice.handleFee+this.displayInvoice.shippingfee) * this.rate;
    }
    else {
      this.displayInvoice.tax = 0;
    }
  }

  lookupClientName(val: string, type: string) {
    if (val == undefined || val.trim() == '') {
      this.filterCLients = [];
      return;
    }

    this.filterCLients = [];
    this.clientList.forEach(c => {
      let name = c.name.toLocaleLowerCase();
      let x = val.toLocaleLowerCase();
      if (name.includes(x)) this.filterCLients.push(c);
    });

    if (this.filterCLients.length > 0)
      this.showClientFilter = type;
    else {
      this.showClientFilter = '';
    }
  }

  checkPAY(pay: string) {
    if (pay == undefined || pay.trim() == '') {
      return;
    }

    this.paymentInfo = pay;
  }

  checkFootnote(note: string, linkindex: number) {
    if (linkindex == undefined || linkindex < 0) return;
    if (this.selectLink[linkindex] == undefined) return;
    if (note == undefined) note = '';
    if (note.length > 255) note = note.substr(0, 255);
    this.selectLink[linkindex].footnote = note;
  }

  validateString(val: string): boolean {
    if (val == undefined || val.trim() == '') {
      return false;
    }

    return true;
  }

  validate() {
    if (isNaN(this.displayInvoice.invoiceNum)) {return false;}
    let i = this.invoiceList.find(x => x.invoiceNum == this.displayInvoice.invoiceNum);
    if (i !== undefined && this.isnew) {return false;}
    if (this.isWithQuote && isNaN(this.displayInvoice.quoteId)) {return false;}
    if (!this.validateString(this.displayInvoice.billAttn)) {return false;}
    if (!this.validateString(this.displayInvoice.shipAttn)) {return false;}
    if (!this.validateString(this.displayInvoice.billAddress)) {return false;}
    if (!this.validateString(this.displayInvoice.shipAddress)) {return false;}
    if (!this.validateString(this.displayInvoice.poNum)) {return false;}
    if (!this.validateString(this.displayInvoice.requisitioner)) {return false;}
    if (!this.validateString(this.displayInvoice.courier)) {return false;}
    if (!this.validateString(this.displayInvoice.paymentTerm)) {return false;}
    if (!this.validateString(this.displayInvoice.shippingTerm)) {return false;}
    if (this.selectLink == undefined || this.selectLink.length < 1) {return false;}
    if (isNaN(this.displayInvoice.handleFee)) {return false;}
    return true;
  }

  hideFilter() {
    setTimeout(_ => { this.showfilter = undefined; }, 200)
  }

  reset() {
    this.displayInvoice = Invoice.newInvoice(this.invoice);
    this.isSameAsShip = false;
    this.selectLink = [];
    this.originalLinks = [];
    this.selectQuote = undefined;
    if (!this.isnew) {
      if (isNaN(this.displayInvoice.tax)) {
        this.isTax = false;
      }
      else {
        this.isTax = true;
      }

      if (this.invoice !== undefined && this.invoice.dbid !== -1) {
        this.salelinkservice.loadAllLinkById(this.invoice.dbid, 'invoice').subscribe(links => {
          if (links !== undefined) {
            links.forEach(l => {
              if (l.linkType == 'invoice') {
                this.selectLink.push(SaleLink.newLink(l))
                this.originalLinks.push(SaleLink.newLink(l))
              }
            });
          }
        });
      }
    }
    else {
      //this.isLoading = false;
      this.selectRate = 'No Tax';
    }
    this.isLoading = false;
  }

  hasChanges(): boolean {
    if (this.displayInvoice.billAddress !== this.invoice.billAddress) return true;
    if (this.displayInvoice.shipAddress !== this.invoice.shipAddress) return true;
    if (this.displayInvoice.billAttn !== this.invoice.billAttn) return true;
    if (this.displayInvoice.shipAttn !== this.invoice.shipAttn) return true;
    if (this.displayInvoice.poNum !== this.invoice.poNum) return true;
    if (this.displayInvoice.requisitioner !== this.invoice.requisitioner) return true;
    if (this.displayInvoice.courier !== this.invoice.courier) return true;
    if (this.displayInvoice.paymentTerm !== this.invoice.paymentTerm) return true;
    if (this.displayInvoice.quoteId !== this.invoice.quoteId) return true;
    if (this.displayInvoice.invoiceNum !== this.invoice.invoiceNum) return true;

    if (this.displayInvoice.currency !== this.invoice.currency) return true;
    if (this.displayInvoice.currencyrate !== this.invoice.currencyrate) return true;

    if (this.displayInvoice.handleFee !== this.invoice.handleFee) return true;
    if (this.displayInvoice.shippingfee !== this.invoice.shippingfee) return true;
    if (this.displayInvoice.proform !== this.invoice.proform) return true;
    if (this.displayInvoice.shippingTerm !== this.invoice.shippingTerm) return true;
    if (this.displayInvoice.tax !== this.invoice.tax) return true;
    if (this.displayInvoice.note !== this.invoice.note) return true;
    if (this.selectLink.length !== this.originalLinks.length) return true;
    let hc = false;
    for (let i = 0; i < this.selectLink.length; i++) {
      let link = SaleLink.newLink(this.selectLink[i]);
      let original = this.originalLinks.find(x => x.dbid == link.dbid);
      if (original == undefined) { hc = true; return true; };
      if (original.itemId !== link.itemId) { hc = true; return true; };
      if (original.itemDiscount !== link.itemDiscount) { hc = true; return true; };
      if (original.itemType !== link.itemType) { hc = true; return true; };
      if (original.itemQuan !== link.itemQuan) { hc = true; return true; };
      if (original.footnote !== link.footnote) { hc = true; return true; };
    }

    return false;
  }

  save() {
    //validate
    this.setTax();
    if (!this.validate()) {
      this.message = "Please make sure all field are input correctly";
      setTimeout(_ => { this.message = undefined }, 5000);
      return;
    }

    if (!this.hasChanges()) {
      this.enableEdit = false;
      return;
    }

    this.update.emit({ i: this.displayInvoice, link: this.selectLink });

    this.isWithQuote = false;
    this.enableEdit = false;
    this.isnew = false;
    //this.displayInvoice = undefined;
  }

  cancelEdit() {
    this.reset();
    this.enableEdit = false;
    if (this.isnew) {
      this.cancel.emit();
      this.displayInvoice = undefined;
    }
  }

  getFormatAddress(c: ClientCompany): { bill: string, ship: string } {
    let bill = '';
    let ship = '';

    ship = c.company.trim() + '\n' + c.address.trim() + '\n';
    if (c.address2 !== undefined && c.address2.trim() !== '')
      ship += c.address2.trim() + '\n';
    ship += c.city.trim() + '';
    if (c.province !== undefined && c.province.trim() !== '')
      ship += ',' + c.province.trim();
    ship += '\n' + c.country.trim();
    if (c.postalCode !== undefined && c.postalCode.trim() !== '')
      ship += '  ' + c.postalCode.trim() + '\n';

    bill = c.company.trim() + '\n' + c.baddress.trim() + '\n';
    if (c.baddress2 !== undefined && c.baddress2.trim() !== '')
      bill += c.baddress2.trim() + '\n';
    bill += c.bcity.trim() + '';
    if (c.bprovince !== undefined && c.bprovince.trim() !== '')
      bill += ',' + c.bprovince.trim();
    bill += '\n' + c.bcountry.trim();
    if (c.bpostalCode !== undefined && c.bpostalCode.trim() !== '')
      bill += '  ' + c.bpostalCode.trim() + '\n';
    return { bill: bill, ship: ship };
  }

  filterByCat(key: string) {
    if (key == undefined || key == '') this.filterOrderItem = this.orderitem.slice();
    this.filterOrderItem = [];
    this.orderitem.forEach(i => {
      if (i.cat.includes(key)) {
        this.filterOrderItem.push(i);
      }
    });
  }

  getPDF() {
    var doc = new jsPDF();
    let imgData = image.logo;
    let width = doc.internal.pageSize.getWidth();
    let height = doc.internal.pageSize.getHeight();
    let margin = 5;
    let emptyHead = [{ title: '', dataKey: 'header' }, { title: '', dataKey: 'content' }];
    // doc.lines([[width - (2*margin),0]] , margin, 13);
    doc.addImage(imgData, 'JPEG', margin, 10, 60, 23);
    let headmargin = (((width - (margin * 2)) / 3) * 2) + margin;
    let cd = new Date(this.displayInvoice.dateCreate);
    let rvd = new Date(this.displayInvoice.revisionDate);
    let month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let headData = []
    if(this.displayInvoice.proform=='Yes'){
      headData.push({ header: 'PROFORMA', content: '' })
    }
    
    headData.push({ header: 'INVOICE #', content: this.displayInvoice.invoiceNum })
    headData.push({ header: 'Version #', content: this.displayInvoice.revision })
    if (this.displayInvoice.revision > 1) {
      headData.push({ header: 'Modify Date:', content: rvd.getDate() + ' ' + month[rvd.getMonth()] + ' ' + rvd.getFullYear() })
    }
    headData.push({ header: 'Date Created', content: cd.getDate() + ' ' + month[cd.getMonth()] + ' ' + cd.getFullYear() })

    doc.autoTable(emptyHead, headData, {
      theme: 'plain',
      styles: {
        cellPadding: 0,
        fontSize: 9,
        textColor: [0, 132, 183]
      },
      headerStyles: {
        fontStyle: 'normal',
        fontSize: 0
      },
      columnStyles: {
        header: {
          fontStyle: 'bold'
        }
      },
      startY: 13, // false (indicates margin top value) or a number
      pageBreak: 'avoid', // 'auto', 'avoid' or 'always'
      tableWidth: (width - (2 * margin)) / 3, // 'auto', 'wrap' or a number, 
      tableLineColor: 200, // number, array (see color section below)
      tableLineWidth: 0,
      showHeader: 'never',
      margin: headmargin,
    });

    doc.setDrawColor(151, 202, 65);
    doc.lines([[width - (2 * margin), 0]], margin, 35);
    let linepointer = 36;
    let nextpointer = 36;

    let col1Margin = margin;
    let col2Margin = margin + (width - (2 * margin)) / 3;
    let col3Margin = margin + 2 * ((width - (2 * margin)) / 3)
    // let cbadd = ''
    // this.displayInvoice.shipAddress.split('\\n').forEach(l => {
    //   cbadd += l + '\n';
    // })

    // this.displayInvoice.billAddress = cbadd
    doc.autoTable(['Bill To:'], [
      [this.displayInvoice.billAddress], ['ATTN: ' + this.displayInvoice.billAttn]
    ], {
        theme: 'plain',
        styles: {
          fontSize: 9,
          textColor: 0,
        },
        headerStyles: {
          cellPadding: [2, 0],
          fontStyle: 'bold'
        },
        bodyStyles: {
          cellPadding: 0
        },
        startY: linepointer, // false (indicates margin top value) or a number
        pageBreak: 'avoid', // 'auto', 'avoid' or 'always'
        tableWidth: Math.floor((width - (2 * margin)) / 3), // 'auto', 'wrap' or a number, 
        tableLineColor: 200, // number, array (see color section below)
        tableLineWidth: 0,
        margin: col1Margin,
        drawRow: function (row, data) {
          nextpointer = Math.max(nextpointer, row.y + (row.height / 2));
        }
      });

    // let cadd = ''
    // this.displayInvoice.shipAddress.split('\\n').forEach(l => {
    //   cadd += l + '\n';
    // })
    // this.displayInvoice.shipAddress = cadd;
    doc.autoTable(['Ship To:'], [
      [this.displayInvoice.shipAddress], ['ATTN: ' + this.displayInvoice.shipAttn]
    ], {
        theme: 'plain',
        styles: {
          fontSize: 9,
          textColor: 0,
        },
        headerStyles: {
          cellPadding: [2, 0],
          fontStyle: 'bold'
        },
        bodyStyles: {
          cellPadding: 0
        },
        startY: linepointer, // false (indicates margin top value) or a number
        pageBreak: 'avoid', // 'auto', 'avoid' or 'always'
        tableWidth: Math.floor((width - (2 * margin)) / 3), // 'auto', 'wrap' or a number, 
        tableLineColor: 200, // number, array (see color section below)
        tableLineWidth: 0,
        margin: col2Margin,
        drawRow: function (row, data) {
          nextpointer = Math.max(nextpointer, row.y + (row.height / 2));
        }
      });

    doc.autoTable(['Somru BioScience Info:'], [
      ['19 Innovation Way'],
      ['Charlottetown, PE'],
      ['C1E 0B7  Canada'],
      ['Phone: 1 (902) 367-4322'],
      ['Email: customer@somrubioscience.com']
    ], {
        theme: 'plain',
        styles: {
          fontSize: 9,
          textColor: 0,
        },
        headerStyles: {
          cellPadding: [2, 0],
          fontStyle: 'bold'
        },
        bodyStyles: {
          cellPadding: 0
        },
        startY: linepointer, // false (indicates margin top value) or a number
        pageBreak: 'avoid', // 'auto', 'avoid' or 'always'
        tableWidth: Math.floor((width - (2 * margin)) / 3), // 'auto', 'wrap' or a number, 
        tableLineColor: 200, // number, array (see color section below)
        tableLineWidth: 0,
        margin: col3Margin,
        drawRow: function (row, data) {
          nextpointer = Math.max(nextpointer, row.y + (row.height / 2));
        }
      });

    let salerep = 'Susan Holland';

    if (this.cur !== undefined) {
      salerep = this.cur.name;
    }

    linepointer = nextpointer + 10;
    let head = ['SALESPERSON', 'P.O. NUMBER', 'REQUISITIONER', 'SHIP VIA', 'PAYMENT TERMS', 'SHIPPING TERMS']
    let data = [
      [salerep, this.displayInvoice.poNum, this.displayInvoice.requisitioner, this.displayInvoice.courier, this.displayInvoice.paymentTerm, this.displayInvoice.shippingTerm]
    ]

    doc.autoTable(head, data, {
      theme: 'plain',
      styles: {
        halign: 'left',
        valign: 'middle',
        font: 'times',
        fontSize: 9,
        textColor: 0,
        overflow: 'linebreak',
      },
      headerStyles: {
        fillColor: [224, 234, 207]
      },
      // Properties
      startY: linepointer, // false (indicates margin top value) or a number
      pageBreak: 'auto', // 'auto', 'avoid' or 'always'
      tableWidth: width - (2 * margin), // 'auto', 'wrap' or a number, 
      tableLineColor: [151, 202, 65], // number, array (see color section below)
      tableLineWidth: 0.1,
      margin: margin,
      drawRow: function (row, data) {
        linepointer = row.y + row.height;
      }
    });

    let quotedata = this.getDataFromLink();
    let header;
    if(this.displayInvoice.type=='Product invoice'){
      header = quotedata.conclude.hasDisc ? [
      { title: 'Catalog #', dataKey: 'cat' },
      { title: 'Name', dataKey: 'des' },
      { title: 'Qty', dataKey: 'quan' },
      { title: 'List Price', dataKey: 'lprice' },
      { title: 'Disc.', dataKey: 'discount' },
      { title: 'Your Price', dataKey: 'aprice' },
      { title: 'Extended', dataKey: 'tprice' }] : [
          { title: 'Catalog #', dataKey: 'cat' },
          { title: 'Name', dataKey: 'des' },
          { title: 'Qty', dataKey: 'quan' },
          { title: 'List Price', dataKey: 'lprice' },
          { title: 'Extended', dataKey: 'tprice' }
        ]
    }
    else if(this.displayInvoice.type=='Service invoice'){
      header = quotedata.conclude.hasDisc ? [
        { title: 'Description', dataKey: 'des' },
        { title: 'Qty', dataKey: 'quan' },
        { title: 'List Price', dataKey: 'lprice' },
        { title: 'Disc.', dataKey: 'discount' },
        { title: 'Price', dataKey: 'aprice' },
        { title: 'Extended', dataKey: 'tprice' }] : [
            { title: 'Description', dataKey: 'des' },
            { title: 'Qty', dataKey: 'quan' },
            { title: 'List Price', dataKey: 'lprice' },
            { title: 'Extended', dataKey: 'tprice' }
          ]
    }

    doc.autoTable(header, quotedata.data, {
      theme: 'grid', // 'striped', 'grid' or 'plain'
      styles: {
        halign: 'center',
        valign: 'middle',
        font: 'times',
        fontSize: 9,
        textColor: 0,
        overflow: 'linebreak',
        lineColor: [151, 202, 65],
        fillColor: 255
      },
      columnStyles: {
        // quan: { columnWidth: 'wrap' },
        // des: { columnWidth: 'auto', halign: 'left', fillColor: false},
        // cat: { columnWidth: 'wrap' },
        // lprice: { columnWidth: 'wrap' },
        // tprice: { columnWidth: 'wrap', halign: 'right' }

        quan: { columnWidth: 10, halign: 'center' },
        des: { columnWidth: 'auto', halign: 'center', fillColor: false },
        discount: { columnWidth: 12, halign: 'center' },
        cat: { columnWidth: 30, halign: 'center' },
        lprice: { columnWidth: 20, halign: 'center' },
        tprice: { columnWidth: 20, halign: 'center' },
        aprice: { columnWidth: 20, halign: 'center' },
      },
      headerStyles: {
        fillColor: [151, 202, 65]
      },
      startY: linepointer, // false (indicates margin top value) or a number
      pageBreak: 'auto', // 'auto', 'avoid' or 'always'
      tableWidth: width - (2 * margin), // 'auto', 'wrap' or a number, 
      tableLineColor: [151, 202, 65], // number, array (see color section below)
      tableLineWidth: 0,
      margin: margin,
      drawHeaderRow: function (row, data) {
        // row.cells.quan.text = 'Qty';
        // row.cells.quan.styles.halign = 'right';
        // row.cells.lprice.styles.halign = 'right';
        // row.cells.tprice.styles.halign = 'right';
        // row.cells.des.styles.halign = 'left';

        // if(row.cells.cat!=undefined){
        //   row.cells.cat.styles.halign = 'left';
        // }
        // if (quotedata.conclude.hasDisc) {
        //   row.cells.discount.styles.halign = 'right';
        //   row.cells.aprice.styles.halign = 'right';
        // }

        row.cells.quan.text = 'Qty';
        row.cells.quan.styles.halign = 'center';
        row.cells.lprice.styles.halign = 'center';
        row.cells.tprice.styles.halign = 'center';
        row.cells.des.styles.halign = 'left';

        if(row.cells.cat!=undefined){
          row.cells.cat.styles.halign = 'center';
        }
        if (quotedata.conclude.hasDisc) {
          row.cells.discount.styles.halign = 'center';
          row.cells.aprice.styles.halign = 'center';
        }


      },
      drawRow: function (row, data) {
        linepointer = row.y + row.height;

        let product = row.cells.des.raw;
        let productName = product[0];
        let size = product[2];
        let note = product[3];

        if (row.cells.aprice !== undefined) {
          row.cells.aprice.styles.columnWidth = 'wrap';
          row.cells.aprice.styles.halign = 'right';
        }

        if (row.cells.discount !== undefined) {
          row.cells.discount.styles.columnWidth = 'wrap';
          row.cells.discount.styles.halign = 'right';
        }
      },
      // drawCell: function (cell, data) {
      //   if (cell.text.findIndex(x => x.includes('Name: ')) == 0 && cell.text.findIndex(x => x.includes('Size: ')) == 2) {
      //     doc.setDrawColor(151, 202, 65);
      //     doc.setFillColor(255, 255, 255);
      //     doc.rect(cell.x, cell.y, cell.width, cell.height, 'DF');
      //     let text = cell.raw.split('\n');
      //     cell.text = "";
      //     let lp = (cell.height / text.length);
      //     doc.setFontStyle('bold')
      //     doc.text('Name: ', cell.x + cell.styles.cellPadding, cell.y + lp)
      //     doc.setFontStyle('normal')
      //     doc.text(text[0].replace('Name: ', ''), cell.x + cell.styles.cellPadding + 10, cell.y + lp)
      //     doc.setFontStyle('bold')
      //     doc.text('Size: ', cell.x + cell.styles.cellPadding, 2 + cell.y + 2 * lp)
      //     doc.setFontStyle('normal')
      //     doc.text(text[2].replace('Size: ', ''), cell.x + cell.styles.cellPadding + 10, 2 + cell.y + 2 * lp)
      //     if (text[3]) {
      //       doc.setFontStyle('bold')
      //       doc.text('Note: ', cell.x + cell.styles.cellPadding, 2 + cell.y + 2.7 * lp)
      //       doc.setFontStyle('normal')
      //       let note = doc.splitTextToSize(text[3].replace('Note: ', ''), cell.width-20);
      //       doc.text(note, cell.x + cell.styles.cellPadding + 10, 2 + cell.y + 2.7 * lp)
      //     }
      //   }
      // }

      drawCell: function (cell, data) {
        if (cell.text.findIndex(x => x.includes('Name: ')) == 0 && cell.text.findIndex(x => x.includes('Size: ')) !=-1) {
          
          let nameArray=[];
          if(cell.text[1]!=undefined && cell.text[1]!=''){
            nameArray.push(cell.text[0].replace('Name: ', ''));
            nameArray.push(cell.text[1]);
          }
          else{
            nameArray.push(cell.text[0].replace('Name: ', ''));
          }
          
          doc.setDrawColor(151, 202, 65);
          doc.setFillColor(255, 255, 255);
          doc.rect(cell.x, cell.y, cell.width, cell.height, 'DF');
          let note=[];
          let lp;
          let text = cell.raw.trim().split('\n');
          cell.text = "";


          if(text.length>3){
            let i;
            for(i=3;i<text.length;i++){
              let splitnote = doc.splitTextToSize(text[i].replace('Note: ', ''), cell.width-15);
              splitnote.forEach(line=>{
                note.push(line)
              })
            }

            // if(text.length>4){
            //   //lp = (cell.height / (text.length+note.length-2.5));
            //   console.log('text length is: '+text.length)
            //   console.log('note length is: '+note.length)
            //   lp = (cell.height / (4+note.length-1));
            // }
            // else{
            //   //lp = (cell.height / (text.length+note.length-1));
            //   console.log('text length is: '+text.length)
            //   console.log('note length is: '+note.length)
            //   lp = (cell.height / (4+note.length-1));
            // }
            
            lp = (cell.height / (4+note.length-1));

          }
          else{
            lp = (cell.height / text.length);
          }
          doc.setFontStyle('bold')
          doc.text('Name: ', cell.x + cell.styles.cellPadding, cell.y + lp)
          doc.setFontStyle('normal')
          if(nameArray.length==0){
            doc.text(text[0].replace('Name: ', ''), cell.x + cell.styles.cellPadding + 10, cell.y + lp);
            doc.setFontStyle('bold')
            doc.text('Size: ', cell.x + cell.styles.cellPadding, 2 + cell.y + 2 * lp)
            doc.setFontStyle('normal')
            doc.text(text[2].replace('Size: ', ''), cell.x + cell.styles.cellPadding + 10, 2 + cell.y + 2 * lp)
            if (text.length>3) {
              doc.setFontStyle('bold')
              doc.text('Note: ', cell.x + cell.styles.cellPadding, 6 + cell.y + 2 * lp)
              doc.setFontStyle('normal')
              doc.text(note, cell.x + cell.styles.cellPadding + 10, 6 + cell.y + 2 * lp)
            }

          }
          else{
            doc.text(nameArray, cell.x + cell.styles.cellPadding + 10, cell.y + lp);
            let l=nameArray.length;
            let height=(l+0.5) * lp;
            doc.setFontStyle('bold')
            doc.text('Size: ', cell.x + cell.styles.cellPadding, 2 + cell.y + height)
            doc.setFontStyle('normal')
            doc.text(text[2].replace('Size: ', ''), cell.x + cell.styles.cellPadding + 10, 2 + cell.y + height)
            if (text.length>3) {
              doc.setFontStyle('bold')
              doc.text('Note: ', cell.x + cell.styles.cellPadding, 6 + cell.y + height)
              doc.setFontStyle('normal')
              doc.text(note, cell.x + cell.styles.cellPadding + 10, 6 + cell.y + height)
            }


          }

        }
      }


  });

    isNaN(this.displayInvoice.tax) ? this.displayInvoice.tax = 0 : '';
    isNaN(this.displayInvoice.handleFee) ? this.displayInvoice.handleFee = 0 : '';

    let totalhead = [{ title: '', dataKey: 'text' }, { title: '', dataKey: 'amount' }]
    let totaldata = []
    let rowcount = 0;
    if (quotedata.conclude.hasDisc) {
      totaldata.push({ text: 'List Total Price:', amount: '$' + this.formatMoney(quotedata.conclude.totalLP) });
      totaldata.push({ text: 'Discount Total:', amount: '$' + this.formatMoney(quotedata.conclude.totalD) });
      rowcount += 2;
    }
    totaldata.push({ text: 'Subtotal:', amount: '$' + this.formatMoney(quotedata.conclude.totalC) });
    if(this.displayInvoice.proform=='Yes'){
      if(this.displayInvoice.handleFee!=null && this.displayInvoice.handleFee!=0 && this.displayInvoice.shippingfee!=null && this.displayInvoice.shippingfee!=0){
        totaldata.push({ text: 'Estimated Shipping and Handling Fee:', amount: '$' + this.formatMoney(this.displayInvoice.handleFee+this.displayInvoice.shippingfee) });
        rowcount += 1;
      }

 
      if(this.displayInvoice.handleFee!=null && this.displayInvoice.handleFee!=0 && (this.displayInvoice.shippingfee==null || this.displayInvoice.shippingfee==0)   ){
        totaldata.push({ text: 'Estimated Handling Fee:', amount: '$' + this.formatMoney(this.displayInvoice.handleFee) });
        rowcount += 1;
      }
      
    }
    else{
      if(this.displayInvoice.handleFee!=null && this.displayInvoice.handleFee!=0 && this.displayInvoice.shippingfee!=null && this.displayInvoice.shippingfee!=0){
        totaldata.push({ text: 'Shipping and Handling Fee:', amount: '$' + this.formatMoney(this.displayInvoice.handleFee+this.displayInvoice.shippingfee) });
        rowcount += 1;
      }

 
      if(this.displayInvoice.handleFee!=null && this.displayInvoice.handleFee!=0 && (this.displayInvoice.shippingfee==null || this.displayInvoice.shippingfee==0)   ){
        totaldata.push({ text: 'Handling Fee:', amount: '$' + this.formatMoney(this.displayInvoice.handleFee) });
        rowcount += 1;
      }
      
    }

    if (this.displayInvoice.tax !== 0) {
      totaldata.push({ text: 'Tax:', amount: '$' + this.formatMoney(this.displayInvoice.tax) });
      totaldata.push({ text: 'Total (USD):', amount: '$' + this.formatMoney(quotedata.conclude.totalC + this.displayInvoice.handleFee + this.displayInvoice.shippingfee  + this.displayInvoice.tax) });
      rowcount += 2
      if(this.displayInvoice.currency && this.displayInvoice.currency!='USD' && this.displayInvoice.currencyrate){
        let currencyIndex = this.currencyOptions.indexOf(this.displayInvoice.currency);
        totaldata.push({ text: 'Currency Rate ('+this.displayInvoice.currency+'):', amount: this.formatMoney(this.displayInvoice.currencyrate) });
        rowcount += 1;
        totaldata.push({ text: 'Total ('+this.displayInvoice.currency+'):', amount: this.currencySigns[currencyIndex]+this.formatMoney(   (quotedata.conclude.totalC + this.displayInvoice.handleFee + this.displayInvoice.shippingfee  + this.displayInvoice.tax)*this.displayInvoice.currencyrate         ) });
        rowcount += 1;
      }
    }
    else {
      totaldata.push({ text: 'Total (USD):', amount: '$' + this.formatMoney(quotedata.conclude.totalC + this.displayInvoice.handleFee + this.displayInvoice.shippingfee) });
      rowcount += 1;
      if(this.displayInvoice.currency && this.displayInvoice.currency!='USD' && this.displayInvoice.currencyrate){
        let currencyIndex = this.currencyOptions.indexOf(this.displayInvoice.currency);
        totaldata.push({ text: 'Currency Rate ('+this.displayInvoice.currency+'):', amount: this.formatMoney(this.displayInvoice.currencyrate) });
        rowcount += 1;
        totaldata.push({ text: 'Total ('+this.displayInvoice.currency+'):', amount: this.currencySigns[currencyIndex]+this.formatMoney((quotedata.conclude.totalC + this.displayInvoice.handleFee + this.displayInvoice.shippingfee)*this.displayInvoice.currencyrate         ) });
        rowcount += 1;
      }
    }

    doc.autoTable(totalhead, totaldata, {
      theme: 'plain',
      styles: {
        halign: 'right',
        valign: 'middle',
        font: 'times',
        fontSize: 9,
        textColor: 0,
        overflow: 'linebreak',
      },
      columnStyles: {
        text: {
          columnWidth: 2 * ((width - (2 * margin)) / 3),
          cellPadding: 0
        },
        amount: {
          columnWidth: ((width - (2 * margin)) / 3)
        }
      },
      // Properties
      startY: linepointer, // false (indicates margin top value) or a number
      pageBreak: 'auto', // 'auto', 'avoid' or 'always'
      tableWidth: width - (2 * margin), // 'auto', 'wrap' or a number, 
      tableLineColor: [151, 202, 65], // number, array (see color section below)
      tableLineWidth: 0.1,
      showHeader: 'never',
      margin: margin,
      drawRow: function (row, data) {
        linepointer = row.y + row.height;
        if (row.index == rowcount) {
          row.cells.text.styles.fontStyle = 'bold';
          row.cells.amount.styles.fontStyle = 'bold';
        }
        if (row.index % 2 == 0) {
          row.cells.text.styles.fillColor = [224, 234, 207];
          row.cells.amount.styles.fillColor = [224, 234, 207];
        }
      }
    });

    // linepointer += 4;
    // doc.setFontSize(7);
    // doc.setTextColor(100);
    // doc.setFontStyle('normal')
    // doc.text('* Currency: US Dollar (USD)', margin, linepointer);

    doc.setFontSize(8);
    linepointer += 8;
    if (this.displayInvoice.note !== null && this.displayInvoice.note.trim() !== '') {
      doc.setTextColor(0, 132, 183);
      doc.setFontStyle('bold');
      doc.text('Please Note: ', margin, linepointer);
      doc.setFontSize(8);
      doc.setFontStyle('italic');
      let n = doc.splitTextToSize(this.displayInvoice.note, 150);
      doc.text(n, margin + 30, linepointer);
      linepointer += (5 * n.length) - 1;
    }

    if(   (height - linepointer)  < 50  ){
      doc.addPage();
      linepointer=15;
    }
    doc.setTextColor(100);
    doc.setFontStyle('bold');
    doc.text('Payment Terms: ', margin, linepointer);

    doc.setFontSize(8);
    doc.setFontStyle('normal');
    doc.text(this.displayInvoice.paymentTerm, margin + 30, linepointer);

    linepointer += 5;
    doc.setFontSize(9);
    doc.setFontStyle('bold');
    doc.text('Make all payments to: ', margin, linepointer);
    linepointer += 4;
    doc.setFontSize(8);
    doc.setFontStyle('normal');
    let payline = this.paymentInfo.split('\n');
    payline.forEach(line => {
      let lt = doc.splitTextToSize(line, width - (2 * margin));
      doc.text(lt, margin, linepointer);
      linepointer += (4 * lt.length) - (1 * (lt.length - 1));
    });

    let footerHead = ['']
    let footerData = [
      ['Accounts not paid within 30 days of the date of the invoice are subject to a 5% monthly finance charge.\n If you have any questions concerning this invoice, please contact our sales team. sales@somrubioscience.com'],
      ['THANK YOU FOR YOUR BUSINESS!']
    ];
    doc.autoTable(footerHead, footerData, {
      theme: 'plain', // 'striped', 'grid' or 'plain'
      styles: {
        halign: 'center',
        valign: 'middle',
        font: 'times',
        fontSize: 9,
        overflow: 'linebreak',
        fillColor: false,
        textColor: 0,
        fontStyle: 'italic'
      },
      // Properties
      showHeader: 'never',
      startY: linepointer, // false (indicates margin top value) or a number
      pageBreak: 'avoid', // 'auto', 'avoid' or 'always'
      tableWidth: width - (4 * margin), // 'auto', 'wrap' or a number, 
      tableLineColor: 200, // number, array (see color section below)
      tableLineWidth: 0,
      margin: [0,margin],
      drawCell: function (cell, data) {
        var rows = data.table.rows;
        if (data.row.index == rows.length - 1) {
          doc.setFontStyle('bold');
          doc.setFontSize(10);
        }
      }
    });
	this.assignPageNum(doc);
    doc.save('Somru Invoice ' + this.displayInvoice.invoiceNum + '.pdf');
  }
	
		assignPageNum(doc: any) {
    let totalPages = doc.internal.getNumberOfPages();
    let timestamp = new Date().toString();
    for (let i = 1; i <= totalPages; i++) {
      doc.setPage(i);
      doc.setFontStyle('bold');
      doc.text(200,295,i + ' of ' + totalPages);
    }
  }
	
  getDataFromLink(): { data: any[], conclude: { totalD: number, totalC: number, totalLP: number, hasDisc: boolean } } {
    let data = [];
    if (this.selectLink == undefined) return;
    let td = 0;
    let tc = 0;
    let tlp = 0;
    let hasDiscCount = 0;
    this.selectLink.forEach(link => {
      // let row = [];
      if (link.itemDiscount > 0) hasDiscCount++;
      let discountamount = link.price * (link.itemDiscount / 100);
      td += discountamount * link.itemQuan;
      let adjustprice = link.price * (1 - (link.itemDiscount / 100));
      let total = (link.price * link.itemQuan) * (1 - (link.itemDiscount / 100));
      tlp += (link.price * link.itemQuan)
      tc += total;
      if(this.displayInvoice.type=='Product invoice'){
        let name =
        data.push({ cat: link.cat, des: 'Name: ' + link.name + '\n\nSize: ' + link.size + ( (link.footnote == undefined || link.footnote.trim()=='')  ? '' : '\nNote: ' + link.footnote), quan: link.itemQuan, lprice: '$' + this.formatMoney(link.price), discount: link.itemDiscount + '%', aprice: '$' + this.formatMoney(adjustprice), tprice: '$' + this.formatMoney(total) });
      }
      else{
        let name =
        data.push({ des: 'Name: ' + link.name + '\n\nSize: ' + link.size + ( (link.footnote == undefined || link.footnote.trim()=='')  ? '' : '\nNote: ' + link.footnote), quan: link.itemQuan, lprice: '$' + this.formatMoney(link.price), discount: link.itemDiscount + '%', aprice: '$' + this.formatMoney(adjustprice), tprice: '$' + this.formatMoney(total) });
      }
    });

    return { data: data, conclude: { totalD: td, totalC: tc, totalLP: tlp, hasDisc: hasDiscCount != 0 } }
  }



  formatMoney(val: number): string {
    let format = val.toFixed(2);
    format = format.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return format;
  }
}
