import { Component, OnInit } from '@angular/core';
import { SettingService } from '../../shared/services/Setting.Service';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { User } from '../../shared/objects/User';
import { AuthenticationService } from '../../shared/services/Authenticate.Service';

@Component({
  selector: 'app-config',
  providers: [SettingService],
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.css']
})


export class ConfigComponent implements OnInit {

  enableEdit: boolean;
  currentPage: string = 'general';
  pages: { key: string, name: string, isShow: boolean }[];
  displaySetting: { type: string, name: string }[];
  user: User;
  origin: Setting;
  targetView: { type: string, name: string, indexMatter: boolean, data: ConfigValue[], pending: ConfigValue[], del: ConfigValue[] }
  newConfig: ConfigValue[];
  delConfig: ConfigValue[];
  dependancy: ConfigValue[];
  isLoading: boolean;
  message: string;
  isValid: boolean;
  pendingChange: string;
  pendingTargetId: number;
  delChange: string;
  delTargetId: number;
  privilegeLevel: number;
  isAdmin: boolean;
  hasChange: boolean;

  pendingpagelist: string[] = [];
  pendingsettinglist: string[] = [];

  noadddelete = false;

  constructor(private settingservice: SettingService, private auth: AuthenticationService) { }

  ngOnInit() {
    this.isLoading = true;
    this.pendingpagelist = [];
    this.pendingsettinglist = [];

    this.auth.getCurrentUser().then(_ => {
      this.user = User.fromJson(_);
      if (this.user.role == 'Senior Manager') {
        this.privilegeLevel = 1;
      }
      else if (this.user.role == 'Administrator' || this.user.role == 'Manager') {
        this.privilegeLevel = 2;
        this.isAdmin = this.user.role == 'Administrator';
      }
      else {
        this.privilegeLevel = 3;
      }

      this.pages = [{ key: 'general', name: 'General', isShow: true },
      { key: 'invent', name: 'Inventory', isShow: true },
      { key: 'orderItem', name: 'Order Item', isShow: true },
      { key: 'kit', name: 'Kit Assembly', isShow: true },
      { key: 'catalog', name: 'Master Catalog', isShow: this.privilegeLevel == 1 || this.isAdmin },
      { key: 'sale', name: 'Sales', isShow: this.privilegeLevel == 1 || this.isAdmin },
      { key: 'product', name: 'Product', isShow: this.privilegeLevel == 1 || this.isAdmin },
      { key: 'application', name: 'Application', isShow: this.privilegeLevel == 1 || this.isAdmin },
      { key: 'egginventory', name: 'Egg Inventory', isShow: true }]

      this.pages.forEach(page => {
        this.settingservice.getSettingByPage(page.key).subscribe(config => {
          config.forEach(eachconfig => {
            if ((eachconfig.pending && eachconfig.pending.trim() != '') || (eachconfig.requestDel && eachconfig.requestDel.trim() != '')) {
              this.pendingpagelist.push(page.name);
              this.pendingsettinglist.push(eachconfig.type)
            }
          })
        })
      });

      this.changePage(this.pages.find(x => x.isShow).key);
    })
  }

  loadPage() {
    this.hasChange = false;
    this.isLoading = true;
    this.displaySetting = [];
    this.newConfig = [];
    this.settingservice.getSettingByPage(this.currentPage).subscribe(config => {
      if (config == undefined) config = Setting.getSetting(this.currentPage);
      config.forEach(c => {
        if (c !== undefined) {
          let data = Setting.getSettingByType(c.type);
          if (c.value !== undefined) data = ConfigValue.toConfigArray(c.value)
          let pending = [];
          if (c.pending !== undefined) pending = ConfigValue.toConfigArray(c.pending);
          let del = [];
          if (c.requestDel !== undefined) del = ConfigValue.toConfigArray(c.requestDel);
          this.displaySetting.push({ type: c.type, name: Setting.getNameOfType(c.type) });
        }
      });

      this.displaySetting.sort((a, b) => {
        if (a.name < b.name) { return -1; }
        if (a.name > b.name) { return 1; }
        return 0;
      })
      if (this.displaySetting !== undefined && this.displaySetting.length > 0)
        this.setTargetView(this.displaySetting[0].type)
      this.isLoading = false;
    })
  }

  changePage(p: string) {
    this.isLoading = true;
    this.currentPage = p;
    this.targetView = undefined;
    this.loadPage();
  }

  setTargetView(t: string) {
    if (t == undefined || t.trim() == '') return;
    if (this.user == undefined) return;
    if ((this.privilegeLevel !== 1 && !this.isAdmin) && (this.currentPage == 'sale' || this.currentPage == 'catalog')) return
    let target = this.displaySetting.find(x => x.type == t);
    if (target == undefined) return;
    this.isLoading = true;
    this.settingservice.getSettingByPageAndType(this.currentPage, t).subscribe(c => {
      let data = Setting.getSettingByType(c.type);
      if (c.value !== undefined) data = ConfigValue.toConfigArray(c.value)
      let pending = [];
      if (c.pending !== undefined) pending = ConfigValue.toConfigArray(c.pending);
      let del = [];
      if (c.requestDel !== undefined) del = ConfigValue.toConfigArray(c.requestDel);
      this.targetView = { type: c.type, name: Setting.getNameOfType(c.type), indexMatter: Setting.isIndexMatter(c.type), data: data.slice(), pending: pending.slice(), del: del.slice() };
      this.origin = c;
      if (this.targetView.type == 'subLocation') {
        this.settingservice.getSettingByPageAndType(this.currentPage, 'location').subscribe(loc => {
          let location = Setting.getSettingByType('location');
          if (loc !== undefined) location = ConfigValue.toConfigArray(loc.value)
          this.dependancy = location;
        })
      }

      if (t == 'safety' || t == 'note') {
        this.noadddelete = true;
      }
      this.sorttargetview();
      this.isLoading = false;
    });
  }

  sorttargetview() {
    this.targetView.data.sort((a, b) => {
      if (a.id < b.id) { return -1; }
      if (a.id > b.id) { return 1; }
      return 0;
    })
  }

  enableEditing() {
    this.enableEdit = true;
    this.isValid = true;
    this.newConfig = [];
    this.delConfig = [];
  }

  addNewConfig() {
    if (this.user == undefined) return;
    if (this.targetView.data == undefined) this.targetView.data = [];
    let id = 0;

    if (this.newConfig == undefined) this.newConfig = [];
    if (this.targetView.type == 'subLocation') {
      id = this.dependancy.length - 1;
    }
    else {
      if (this.newConfig.length !== 0) {
        id = this.newConfig[this.newConfig.length - 1].id + 1;
      }
      else if (this.targetView.pending.length !== 0) {
        this.targetView.pending.sort((a, b) => { return a.id - b.id });
        id = this.targetView.pending[this.targetView.pending.length - 1].id + 1;
      }
      else if (this.targetView.data.length !== 0) {
        id = this.targetView.data[this.targetView.data.length - 1].id + 1;
      }
    }

    this.newConfig.push(new ConfigValue(id, ''));
    this.newConfig.sort((a, b) => { return a.id - b.id })
  }

  moveUp(index: number) {
    if (this.privilegeLevel !== 1) return;
    if (this.targetView == undefined) return;
    if (index < 0 || index >= this.targetView.data.length) return;
    if (index == 0) return;

    let target = this.targetView.data[index].name;
    let prev = this.targetView.data[index - 1].name;
    this.targetView.data[index].name = prev;
    this.targetView.data[index - 1].name = target;
  }

  moveDown(index: number) {
    if (this.privilegeLevel !== 1) return;
    if (this.targetView == undefined) return;
    if (index < 0 || index >= this.targetView.data.length) return;
    if (index == (this.targetView.data.length - 1)) return;

    let target = this.targetView.data[index].name;
    let next = this.targetView.data[index + 1].name;
    this.targetView.data[index].name = next;
    this.targetView.data[index + 1].name = target;
  }

  removeNew(index: number) {
    if (this.newConfig == undefined) this.newConfig = [];
    if (index < 0 || index >= this.newConfig.length) return;

    this.newConfig.splice(index, 1);
  }

  remove(index: number) {
    // if (this.privilegeLevel !== 1) return;
    if (this.targetView == undefined) return;
    if (index < 0 || index >= this.targetView.data.length) return;

    if (this.privilegeLevel == 1) {
      this.delConfig.push(this.targetView.data.splice(index, 1)[0]);
      this.delConfig.sort((a, b) => { return a.id - b.id })
    }
    else {
      this.targetView.del.push(this.targetView.data.splice(index, 1)[0]);
      this.targetView.del.sort((a, b) => { return a.id - b.id })
    }
  }

  isDelRes(index: number): boolean {
    if (this.targetView == undefined) return;
    if (index < 0 || index >= this.targetView.data.length) return;
    let target = this.targetView.data[index];
    if (this.targetView.del.findIndex(x => x.id == target.id && x.name == target.name) == -1)
      return false
    else
      return true;
  }

  reverseRemove(index: number) {
    if (this.targetView == undefined) return;
    if (index < 0 || index >= this.targetView.data.length) return;
    // if (this.privilegeLevel == 3) return;
    //let target = this.targetView.data[index];
    //let remvi = this.targetView.del.findIndex(x => x.id == target.id && x.name == target.name);
    //let reverseTarget = this.targetView.del.splice(remvi, 1)[0];
    let reverseTarget = this.targetView.del.splice(index, 1)[0];
    this.targetView.data.push(reverseTarget);
    this.targetView.data.sort((a, b) => { return a.id - b.id });

    // let i = this.delConfig.findIndex(x => x.id == reverseTarget.id && x.name == reverseTarget.name)
    // if (i !== -1) {
    //   this.delConfig.splice(i, 1);
    // }
  }

  validateIndex(id: number) {
    this.isValid = true;
    if (this.targetView == undefined) return;
    if (this.targetView.data == undefined) return;
    if (id == undefined) {
      this.message = 'Please assign an index to the config value';
      this.isValid = false;
      return;
    }

    this.message = undefined;
    this.isValid = true;
    id = Math.abs(id);
    let count = 0;
    if (this.targetView.type != 'subLocation') {
      this.targetView.data.forEach(v => {
        if (v.id == id) count++;
      })
      this.targetView.pending.forEach(v => {
        if (v.id == id) count++;
      })
      this.targetView.del.forEach(v => {
        if (v.id == id) count++;
      })
      this.newConfig.forEach(v => {
        if (v.id == id) count++;
      })


      if (count > 0 && this.targetView.type !== 'subLocation') {
        this.message = 'This index: ' + id + ' already exist in the record.'
        this.isValid = false;
      }

    }
  }

  validateVal(val: string, id?: number) {
    if (this.targetView == undefined) return;
    if (this.targetView.data == undefined) return;
    if (val == undefined || val.trim() == '') {
      this.message = 'Please assign a value for the new config value';
      this.isValid = false;
      return;
    }

    this.message = undefined;
    this.isValid = true;
    let count = 0;

    if (id) {
      this.targetView.data.forEach(v => {
        if (v.name.toLocaleLowerCase() == val.toLocaleLowerCase() && v.id == id) {
          count++;
        }
      })
      this.targetView.pending.forEach(v => {
        if (v.name.toLocaleLowerCase() == val.toLocaleLowerCase() && v.id == id) {
          count++;
        }
      })
      this.targetView.del.forEach(v => {
        if (v.name.toLocaleLowerCase() == val.toLocaleLowerCase() && v.id == id) {
          count++;
        }
      })
      this.newConfig.forEach(v => {
        if (v.name.toLocaleLowerCase() == val.toLocaleLowerCase() && v.id == id) {
          count++;
        }
      })
    }
    else {
      this.targetView.data.forEach(v => {
        if (v.name.toLocaleLowerCase() == val.toLocaleLowerCase()) { count++; }
      })
      this.targetView.pending.forEach(v => {
        if (v.name.toLocaleLowerCase() == val.toLocaleLowerCase()) { count++; }
      })
      this.targetView.del.forEach(v => {
        if (v.name.toLocaleLowerCase() == val.toLocaleLowerCase()) { count++; }
      })
      this.newConfig.forEach(v => {
        if (v.name.toLocaleLowerCase() == val.toLocaleLowerCase()) { count++; }
      })
    }

    if (count > 1) {
      this.message = 'This value is already exist in the record.'
      this.isValid = false;
    }
    else {
      this.isValid = true;
    }
  }

  acceptPending(index: number) {
    if (this.privilegeLevel >= 3) return;
    if (this.targetView == undefined) return;
    if (index < 0 || index >= this.targetView.pending.length) return;

    this.targetView.data.push(this.targetView.pending.splice(index, 1)[0]);
    this.targetView.data.sort((a, b) => { return a.id - b.id })
    this.updateSetting();
    this.pendingChange = undefined;
    this.pendingTargetId = undefined;
  }

  removePending(index: number) {
    if (this.privilegeLevel >= 3) return;
    if (this.targetView == undefined) return;
    if (index < 0 || index >= this.targetView.pending.length) return;

    this.targetView.pending.splice(index, 1);
    this.targetView.data.sort((a, b) => { return a.id - b.id })
    this.updateSetting();
    this.pendingChange = undefined;
    this.pendingTargetId = undefined;
  }

  acceptDel(index: number) {
    if (this.privilegeLevel >= 3) return;
    if (this.targetView == undefined) return;
    if (index < 0 || index >= this.targetView.del.length) return;
    let target = this.targetView.del.splice(index, 1)[0];
    let i = this.targetView.data.findIndex(x => x.id == target.id && x.name == target.name);
    if (i !== -1) this.targetView.data.splice(i, 1);
    this.updateSetting();
    this.delChange = undefined;
    this.delTargetId = undefined;
  }

  rejectDel(index: number) {
    if (this.privilegeLevel >= 3) return;
    if (this.targetView == undefined) return;
    if (index < 0 || index >= this.targetView.del.length) return;

    this.reverseRemove(index);
    this.updateSetting();
    this.delChange = undefined;
    this.delTargetId = undefined;
  }

  updateSetting() {
    this.targetView.data.forEach(val => {
      if (this.targetView.type == 'subLocation') {
        this.validateVal(val.name, val.id);
      }
      else {
        this.validateIndex(val.id);
        this.validateVal(val.name);
      }
    });
    this.enableEdit = false;
    this.isLoading = true;
    if (!this.isValid) return;
    if (this.currentPage == undefined) return;
    if (this.targetView == undefined) return;

    this.settingservice.getSettingByPageAndType(this.currentPage, this.targetView.type).subscribe(config => {
      if (config !== undefined) {
        if (JSON.stringify(this.origin) !== JSON.stringify(config)) {
          this.hasChange = true;
          this.isLoading = false;
          return;
        }
        else {
          let value = this.targetView.data;
          let pending = this.targetView.pending;
          let del = this.targetView.del;

          //check if user wants to delete
          if (this.delConfig !== undefined && this.delConfig.length > 0) {
            this.delConfig.forEach(con => {
              let i = value.findIndex(x => x.id == con.id && x.name == con.name);
              if (this.privilegeLevel == 1 && i !== -1) {
                value.slice(i, 1);
              }

              // if (this.privilegeLevel !== 1) {
              //   let i = del.findIndex(x => x.id == con.id && x.name == con.name);
              //   if (i == -1) {
              //     del.push(con);
              //   }
              // }
            });
          }

          if (this.newConfig !== undefined && this.newConfig.length > 0) {
            let id = 0;
            if (this.privilegeLevel == 1) {
              if (this.targetView.indexMatter) {
                this.newConfig.forEach((con, index) => {
                  value.push(con);
                });
              }
              else {
                id = value.length > 0 ? value[value.length - 1].id + 1 : 0;
                this.newConfig.forEach((con, index) => {
                  con.id = id + index;
                  value.push(con);
                });
              }
            }
            else {
              if (this.targetView.indexMatter) {
                this.newConfig.forEach((con, index) => {
                  pending.push(con);
                });
              }
              else {
                id = pending.length > 0 ? pending[pending.length - 1].id + 1 : 0;
                this.newConfig.forEach((con, index) => {
                  con.id = id + index;
                  pending.push(con);
                });
              }
            }
          }
          config.value = ConfigValue.toDBValueLine(value);
          config.pending = ConfigValue.toDBValueLine(pending);
          config.requestDel = ConfigValue.toDBValueLine(del);
          this.settingservice.updateSetting(config).then(_ => {
            this.settingservice.getSettingByPageAndType(this.currentPage, this.targetView.type).subscribe(config => { this.origin = config })
            //this.loadPage() 
          });
        }
      }
      // this.targetView.del.forEach(con => {
      //   let vi = value.findIndex(x => x.id == con.id && x.name == con.name);
      //   let di = del.findIndex(x => x.id == con.id && x.name == con.name)
      //   if ((vi !== -1 && di == -1) || (vi == -1 && di == -1)) {
      //     del.push(con);
      //     vi !== -1 ? value.splice(vi, 1) : '';
      //   }
      //   else if (vi !== -1 && di !== -1) {
      //     value.splice(vi, 1);
      //   }
      // });

      // this.targetView.data.forEach(con => {
      //   if (value.findIndex(x => x.id == con.id && x.name == con.name))
      // })
    })
  }




}
