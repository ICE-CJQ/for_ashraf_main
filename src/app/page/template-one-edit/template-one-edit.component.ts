import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Worksheet} from '../../shared/objects/Worksheet';
import {TimerComponent} from '../timer/timer.component';
import {WorksheetService} from '../../shared/services/worksheet.service';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateFormat} from '../template-one/template-one.component';
import {ActivatedRoute} from '@angular/router';




@Component({
  providers: [WorksheetService, { provide: MAT_DATE_LOCALE, useValue: 'en_CA' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: DateFormat }],
  selector: 'app-template-one-edit',
  templateUrl: './template-one-edit.component.html',
  styleUrls: ['./template-one-edit.component.css']
})
export class TemplateOneEditComponent implements OnInit {

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;
  worksheetTemplate: Worksheet;
  timer = new TimerComponent(4);
  // time spans for the four sections in template one
  counterSec = [0, 0, 0, 0];
  worksheetData: any;
  worksheetID: number;
  worksheet: Worksheet[] = [];

  constructor(private _formBuilder: FormBuilder, private worksheetService: WorksheetService, private route: ActivatedRoute) {
    this.worksheetService.getAllWorksheet().subscribe(list => {
      this.worksheet = list;
    });
  }

  ngOnInit() {
    this.worksheetID = this.route.snapshot.params.worksheetID;
    this.worksheetService.getWorksheetById(this.worksheetID).subscribe(data => {
      this.worksheetData = data;
    });

    this.firstFormGroup = this._formBuilder.group({
      assayNumber: ['', Validators.required],
      scientist: ['', Validators.required],
      date: ['', Validators.required]
    });

    this.secondFormGroup = this._formBuilder.group({
      coatingBuffer: ['', Validators.required],
      coatingBufferLotNumber: ['', Validators.required],
      coatingMaterial1: ['', Validators.required],
      coatingMaterial1LotNumber: ['', Validators.required],
      ulCoatingMaterial: ['', Validators.required],
      ulCoatingBuffer: ['', Validators.required],
      ulCoatingMaterial_ulCoatingBuffer: ['', Validators.required]

    });
    this.thirdFormGroup = this._formBuilder.group({
      lockingLotNum: ['', Validators.required]
    });
    this.fourthFormGroup = this._formBuilder.group({
      washingLotNum: ['', Validators.required]
    });
    this.fifthFormGroup = this._formBuilder.group({
      superblockLotNum: ['', Validators.required],
      ulClone1: ['', Validators.required],
      ulClone2: ['', Validators.required],
      ulDiluent: ['', Validators.required],
    });
    this.sixthFormGroup = this._formBuilder.group({
      diluentLotNumber: ['', Validators.required],
      innovatorLotNumber: ['', Validators.required],
      chemicalInput1: ['', Validators.required],
      chemicalInput2: ['', Validators.required],
      chemicalInput3: ['', Validators.required],
      chemicalInput4: ['', Validators.required],
      chemicalInput5: ['', Validators.required],
      chemicalInput6: ['', Validators.required],
      chemicalInput7: ['', Validators.required],
      concentrationLotNumber: ['', Validators.required],
      ulBiosimilar: ['', Validators.required],
      ulBiosimilar2: ['', Validators.required],
      ulSuperblock: ['', Validators.required],
      ulBiosimilar3: ['', Validators.required],
      ulBioSimilar4: ['', Validators.required],
      ulBioSimilar5: ['', Validators.required],
      ulBioSimilar6: ['', Validators.required],
      chemicalInput8: ['', Validators.required],
      chemicalInput9: ['', Validators.required],
      chemicalInput10: ['', Validators.required],
      chemicalInput11: ['', Validators.required],
      chemicalInput12: ['', Validators.required],
      chemicalInput13: ['', Validators.required],
      chemicalInput14: ['', Validators.required],
      chemicalInput15: ['', Validators.required],
    });
  }

  // tslint:disable-next-line:max-line-length
  formData(
    assayNumber, scientist, date, coatingBuffer,
    coatingBufferLotNumber, coatingMaterial1, coatingMaterial1LotNumber,
    ulCoatingMaterial, ulCoatingBuffer, ulCoatingMaterial_ulCoatingBuffer,
    lockingLotNum, washingLotNUm, superblockLotNum, ulCLone1, ulCLone2,
    ulDiluent, diluentLotNumber, innovatorLotNumber, chemicalInput1,
    chemicalInput2, chemicalInput3, chemicalInput4, chemicalInput5,
    chemicalInput6, chemicalInput7, concentrationLotNumber,
    ulBiosimilar, ulBiosimilar2, ulSuperblock, ulBiosimilar3,
    ulBioSimilar4, ulBioSimilar5, ulBioSimilar6, chemicalInput8, chemicalInput9,
    chemicalInput10, chemicalInput11, chemicalInput12, chemicalInput13,
    chemicalInput14, chemicalInput15) {

    // tslint:disable-next-line:max-line-length
    const worksheet = new Worksheet(
      assayNumber, scientist, date, coatingBuffer, coatingBufferLotNumber,
      coatingMaterial1, coatingMaterial1LotNumber, ulCoatingMaterial,
      ulCoatingBuffer, ulCoatingMaterial_ulCoatingBuffer, lockingLotNum,
      washingLotNUm, superblockLotNum, ulCLone1, ulCLone2, ulDiluent,
      diluentLotNumber, innovatorLotNumber, chemicalInput1, chemicalInput2,
      chemicalInput3, chemicalInput4, chemicalInput5, chemicalInput6, chemicalInput7,
      concentrationLotNumber, ulBiosimilar, ulBiosimilar2, ulSuperblock, ulBiosimilar3,
      ulBioSimilar4, ulBioSimilar5, ulBioSimilar6, chemicalInput8, chemicalInput9, chemicalInput10,
      chemicalInput11, chemicalInput12, chemicalInput13, chemicalInput14, chemicalInput15);

    console.log(assayNumber, scientist, date, coatingBuffer, coatingBufferLotNumber,
      coatingMaterial1, coatingMaterial1LotNumber, ulCoatingMaterial,
      ulCoatingBuffer, ulCoatingMaterial_ulCoatingBuffer, lockingLotNum,
      washingLotNUm, superblockLotNum, ulCLone1, ulCLone2, ulDiluent,
      diluentLotNumber, innovatorLotNumber, chemicalInput1, chemicalInput2,
      chemicalInput3, chemicalInput4, chemicalInput5, chemicalInput6, chemicalInput7,
      concentrationLotNumber, ulBiosimilar, ulBiosimilar2, ulSuperblock, ulBiosimilar3,
      ulBioSimilar4, ulBioSimilar5, ulBioSimilar6, chemicalInput8, chemicalInput9, chemicalInput10,
      chemicalInput11, chemicalInput12, chemicalInput13, chemicalInput14, chemicalInput15);
    this.worksheetService.addWorksheet(worksheet);
  }

  // save the time span for count down timer
  saveTime(param: number, section: number) {

    this.counterSec[section] = param;
  }

  // getter for time span
  getTime(arrIndex: number) {
    return this.counterSec[arrIndex];
  }

  // return the number of total timers used in this  template
  getTimerNumber() {
    return this.counterSec.length;
  }

}
