import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateOneEditComponent } from './template-one-edit.component';

describe('TemplateOneEditComponent', () => {
  let component: TemplateOneEditComponent;
  let fixture: ComponentFixture<TemplateOneEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplateOneEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateOneEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
