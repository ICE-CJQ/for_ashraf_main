import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { TimerComponent } from "../timer/timer.component";
import { Worksheet } from "app/shared/objects/Worksheet";
import { WorksheetService } from "app/shared/services/worksheet.service";
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE
} from "@angular/material/core";
import { MomentDateAdapter } from "@angular/material-moment-adapter";
import { throwError } from "rxjs";
import { AutoCompleteService } from "app/shared/services/auto-complete.service";

export const DateFormat = {
  parse: {
    dateInput: "input"
  },
  display: {
    dateInput: "YYYY-MM-DD",
    monthYearLabel: "MMMM YYYY",
    dateA11yLabel: "YYYY-MM-DD",
    monthYearA11yLabel: "MMMM YYYY"
  }
};

@Component({
  providers: [
    WorksheetService,
    { provide: MAT_DATE_LOCALE, useValue: "en_CA" },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    { provide: MAT_DATE_FORMATS, useValue: DateFormat }
  ],
  selector: "app-template-one",
  templateUrl: "./template-one.component.html",
  styleUrls: ["./template-one.component.css"]
})
export class TemplateOneComponent implements OnInit {
  searchListArr;
  autoInputDataArr;
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;

  seventhFormGroup: FormGroup;
  eighthFormGroup: FormGroup;
  ninthFormGroup: FormGroup;
  tenthFormGroup: FormGroup;
  eleventhFormGroup: FormGroup;
  twelfthFormGroup: FormGroup;
  thirteenthFormGroup: FormGroup;
  fourteenthFormGroup: FormGroup;

  worksheetTemplate: Worksheet;
  timer = new TimerComponent(4);
  // time spans for the four sections in template one
  counterSec = [0, 0, 0, 0];

  constructor(
    private _formBuilder: FormBuilder,
    private worksheetService: WorksheetService,
    private autoComplete: AutoCompleteService
  ) {}

  ngOnInit() {
    this.autoInputDataArr = this.autoComplete.iniInputDataArr(3);
    this.searchListArr = this.autoComplete.initSearchListArr(3);

    this.firstFormGroup = this._formBuilder.group({
      assayNumber: ["", Validators.required],
      scientist: ["", Validators.required],
      date: ["", Validators.required]
    });

    this.secondFormGroup = this._formBuilder.group({
      coatingBuffer: ["", Validators.required],
      coatingBufferLotNumber: ["", Validators.required],
      coatingMaterial1: ["", Validators.required],
      coatingMaterial1LotNumber: ["", Validators.required],
      ulCoatingMaterial: ["", Validators.required],
      ulCoatingBuffer: ["", Validators.required],
      ulCoatingMaterial_ulCoatingBuffer: ["", Validators.required],
      // control for supplier inputs
      step2_supplier_1: ["", Validators.required],
      step2_supplier_2: ["", Validators.required],
      //

      //control for concentration inputs
      step2_concentration_1: ["", Validators.required]
      //
    });
    this.thirdFormGroup = this._formBuilder.group({
      lockingLotNum: ["", Validators.required],
      // control for supplier inputs
      step3_supplier_1: ["", Validators.required],
      //

      // control for item name inputs
      step3_item_1: ["", Validators.required]
      //
    });
    this.fourthFormGroup = this._formBuilder.group({
      washingLotNum: ["", Validators.required],
      // control for supplier inputs
      step4_supplier_1: ["", Validators.required],
      //

      // control for item name inputs
      step4_item_1: ["", Validators.required]
      //
    });
    this.fifthFormGroup = this._formBuilder.group({
      superblockLotNum: ["", Validators.required],
      ulClone1: ["", Validators.required],
      ulClone2: ["", Validators.required],
      ulDiluent: ["", Validators.required],
      // control for supplier inputs
      step5_supplier_1: ["", Validators.required],
      //

      // control for item name inputs
      step5_item_1: ["", Validators.required],
      //

      // control for clone ID
      step5_cloneId_1: ["", Validators.required]
      //
    });
    this.sixthFormGroup = this._formBuilder.group({
      diluentLotNumber: ["", Validators.required],
      innovatorLotNumber: ["", Validators.required],
      chemicalInput1: ["", Validators.required],
      chemicalInput2: ["", Validators.required],
      chemicalInput3: ["", Validators.required],
      chemicalInput4: ["", Validators.required],
      chemicalInput5: ["", Validators.required],
      chemicalInput6: ["", Validators.required],
      chemicalInput7: ["", Validators.required],
      concentrationLotNumber: ["", Validators.required],
      ulBiosimilar: ["", Validators.required],
      ulBiosimilar2: ["", Validators.required],
      ulSuperblock: ["", Validators.required],
      ulBiosimilar3: ["", Validators.required],
      ulBioSimilar4: ["", Validators.required],
      ulBioSimilar5: ["", Validators.required],
      ulBioSimilar6: ["", Validators.required],
      chemicalInput8: ["", Validators.required],
      chemicalInput9: ["", Validators.required],
      chemicalInput10: ["", Validators.required],
      chemicalInput11: ["", Validators.required],
      chemicalInput12: ["", Validators.required],
      chemicalInput13: ["", Validators.required],
      chemicalInput14: ["", Validators.required],
      chemicalInput15: ["", Validators.required],
      // control for supplier inputs
      step6_supplier_1: ["", Validators.required],
      step6_supplier_2: ["", Validators.required],
      step6_supplier_3: ["", Validators.required],
      step6_supplier_4: ["", Validators.required],
      //

      // control for item name inputs
      step6_item_1: ["", Validators.required],
      step6_item_2: ["", Validators.required],
      step6_item_3: ["", Validators.required],
      step6_item_4: ["", Validators.required],
      //

      // control for concentration inputs
      step6_concentration_1: ["", Validators.required],
      step6_concentration_2: ["", Validators.required],
      step6_concentration_3: ["", Validators.required]
      //
    });

    //data from template two
    ///
    ///
    this.seventhFormGroup = this._formBuilder.group({
      seventhCtrl: ["", Validators.required]
    });
    this.eighthFormGroup = this._formBuilder.group({
      eigthCtrl: ["", Validators.required],
      //control for item inputs
      step8_item_1: ["", Validators.required],
      //

      //control for supplier inputs
      step8_supplier_1: ["", Validators.required]
      //
    });
    this.ninthFormGroup = this._formBuilder.group({
      ninthCtrl: ["", Validators.required],
      //control for item inputs
      step9_item_1: ["", Validators.required],
      step9_item_2: ["", Validators.required],
      //

      //control for supplier inputs
      step9_supplier_1: ["", Validators.required],
      step9_supplier_2: ["", Validators.required]
      //
    });
    this.tenthFormGroup = this._formBuilder.group({
      tenthCtrl: ["", Validators.required],
      //control for item inputs
      step10_item_1: ["", Validators.required],
      //

      //control for supplier inputs
      step10_supplier_1: ["", Validators.required]
      //
    });
    this.eleventhFormGroup = this._formBuilder.group({
      eleventhCtrl: ["", Validators.required],
      //control for item inputs
      step11_item_1: ["", Validators.required],
      step11_item_2: ["", Validators.required],
      //

      //control for supplier inputs
      step11_supplier_1: ["", Validators.required],
      step11_supplier_2: ["", Validators.required]
      //
    });
    this.twelfthFormGroup = this._formBuilder.group({
      twelfthCtrl: ["", Validators.required],
      //control for item inputs
      step12_item_1: ["", Validators.required],
      //

      //control for supplier inputs
      step12_supplier_1: ["", Validators.required]
      //
    });
    this.thirteenthFormGroup = this._formBuilder.group({
      thirteenthCtrl: ["", Validators.required],
      //control for item inputs
      step13_item_1: ["", Validators.required],
      //

      //control for supplier inputs
      step13_supplier_1: ["", Validators.required]
      //
    });
    this.fourteenthFormGroup = this._formBuilder.group({
      fourteenthCtrl: ["", Validators.required],
      //control for item inputs
      step14_item_1: ["", Validators.required],
      //

      //control for supplier inputs
      step14_supplier_1: ["", Validators.required]
      //
    });
  }

  // tslint:disable-next-line:max-line-length
  formData(
    assayNumber,
    scientist,
    date,
    coatingBuffer,
    coatingBufferLotNumber,
    coatingMaterial1,
    coatingMaterial1LotNumber,
    ulCoatingMaterial,
    ulCoatingBuffer,
    ulCoatingMaterial_ulCoatingBuffer,
    lockingLotNum,
    washingLotNUm,
    superblockLotNum,
    ulCLone1,
    ulCLone2,
    ulDiluent,
    diluentLotNumber,
    innovatorLotNumber,
    chemicalInput1,
    chemicalInput2,
    chemicalInput3,
    chemicalInput4,
    chemicalInput5,
    chemicalInput6,
    chemicalInput7,
    concentrationLotNumber,
    ulBiosimilar,
    ulBiosimilar2,
    ulSuperblock,
    ulBiosimilar3,
    ulBioSimilar4,
    ulBioSimilar5,
    ulBioSimilar6,
    chemicalInput8,
    chemicalInput9,
    chemicalInput10,
    chemicalInput11,
    chemicalInput12,
    chemicalInput13,
    chemicalInput14,
    chemicalInput15
  ) {
    // tslint:disable-next-line:max-line-length
    const worksheet = new Worksheet(
      assayNumber,
      scientist,
      date,
      coatingBuffer,
      coatingBufferLotNumber,
      coatingMaterial1,
      coatingMaterial1LotNumber,
      ulCoatingMaterial,
      ulCoatingBuffer,
      ulCoatingMaterial_ulCoatingBuffer,
      lockingLotNum,
      washingLotNUm,
      superblockLotNum,
      ulCLone1,
      ulCLone2,
      ulDiluent,
      diluentLotNumber,
      innovatorLotNumber,
      chemicalInput1,
      chemicalInput2,
      chemicalInput3,
      chemicalInput4,
      chemicalInput5,
      chemicalInput6,
      chemicalInput7,
      concentrationLotNumber,
      ulBiosimilar,
      ulBiosimilar2,
      ulSuperblock,
      ulBiosimilar3,
      ulBioSimilar4,
      ulBioSimilar5,
      ulBioSimilar6,
      chemicalInput8,
      chemicalInput9,
      chemicalInput10,
      chemicalInput11,
      chemicalInput12,
      chemicalInput13,
      chemicalInput14,
      chemicalInput15
    );

    this.worksheetService.addWorksheet(worksheet);
  }

  // save the time span for count down timer
  saveTime(param: number, section: number) {
    this.counterSec[section] = param;
  }

  // getter for time span
  getTime(arrIndex: number) {
    return this.counterSec[arrIndex];
  }

  // return the number of total timers used in this  template
  getTimerNumber() {
    return this.counterSec.length;
  }
}
