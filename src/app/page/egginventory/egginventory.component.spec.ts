import { async, inject, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import {EgginventoryComponent} from './egginventory.component'
import { Chicken, Injection } from '../../shared/objects/Chicken';
import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter,  ViewChild, ElementRef, SimpleChange  } from '@angular/core';
import { InventoryService } from 'app/shared/services/Inventory.Service';
import { ChickenService } from '../../shared/services/chicken.Service';
import { SettingService } from '../../shared/services/Setting.Service';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { User } from 'app/shared/objects/User';
import { AuthenticationService } from '../../shared/services/Authenticate.Service'
import { InjectionService } from '../../shared/services/injection.service';
import { EggService } from '../../shared/services/egg.service';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { HttpModule, Http } from "@angular/http";
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/of';
import 'zone.js/dist/async-test';
import 'zone.js/dist/fake-async-test';

xdescribe('Chicken-Display Component Integration Test',()=>{
    //Arrange
    let egginventoryComponent: EgginventoryComponent;
    let fixture: ComponentFixture<EgginventoryComponent>;
    let inventoryService: InventoryService;
    let chickenService: ChickenService;
    let injectionService: InjectionService; 
    let setting: SettingService ;
    let eggService: EggService;
    let authenticationservice: AuthenticationService;


    // beforeAll(()=>{
    //     TestBed.configureTestingModule({
    //         imports: [ FormsModule, MyDatePickerModule, HttpClientModule, HttpClientTestingModule, HttpModule, NgbModule.forRoot()],
    //         declarations:[EgginventoryComponent],
    //         providers:[ChickenService, EggService, InjectionService, InventoryService, SettingService, AuthenticationService]
    //     });

    //     fixture =  TestBed.createComponent(EgginventoryComponent);
    //     egginventoryComponent=fixture.componentInstance;
    //     injectionService = TestBed.get(InjectionService); 
    //     inventoryService = TestBed.get(InventoryService);
    //     chickenService = TestBed.get(ChickenService);
    //     eggService = TestBed.get(EggService);
    //     setting = TestBed.get(SettingService);
    //     authenticationservice = TestBed.get(AuthenticationService);

    //     egginventoryComponent.currentuser={dbid:1, name:'Chen Fei', role:'Senior Manager', email:'chen.fei@somrubioscience.com', password:'123456'}

    // });






})