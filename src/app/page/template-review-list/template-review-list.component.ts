import { Component, OnInit } from '@angular/core';
import {Worksheet} from '../../shared/objects/Worksheet';
import {NgModel} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {WorksheetService} from '../../shared/services/worksheet.service';

@Component({
  selector: 'app-template-review-list',
  templateUrl: './template-review-list.component.html',
  providers: [WorksheetService],
  styleUrls: ['./template-review-list.component.css']
})
export class TemplateReviewListComponent implements OnInit {

  worksheet: Worksheet[] = [];
  displayedColumns: string[] = ['position', 'name', 'author', 'nav'];
  constructor(private modalService: NgbModal, private worksheetService: WorksheetService) { }

  ngOnInit() {
    this.worksheetService.getAllWorksheet().subscribe(list => {
      this.worksheet = list;
    });
  }

}
