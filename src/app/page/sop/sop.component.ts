
import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild, Output, EventEmitter } from '@angular/core';
import { Recipe, Ingredient } from '../../shared/objects/Recipe';
import { Kit } from '../../shared/objects/Kit';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { KitService } from 'app/shared/services/Kit.Service';
import { RecipeService } from 'app/shared/services/Recipe.Service';
import { IngredientService } from 'app/shared/services/Ingredient.Service';
import { KitcomponentService } from '../../shared/services/Kitcomponent.Service';
import { User } from 'app/shared/objects/User';
import { AuthenticationService } from '../../shared/services/Authenticate.Service';
import { UserhistoryService } from '../../shared/services/userhistory.service';
import { Userhistory } from '../../shared/objects/Userhistory';

@Component({
  selector: 'app-sop',
  providers: [RecipeService, IngredientService, UserhistoryService],
  templateUrl: './sop.component.html',
  styleUrls: ['./sop.component.css'],
  host: {
    '(document:click)': 'onClick($event)',
  }
})
export class SopComponent implements OnInit {
  @Input() isShowRecipe: boolean;
  @Input() displayRecipe: Recipe;
  @Output() resetView = new EventEmitter();

  @ViewChild('recipeDisplay', {static: false}) recipeDisplay: ElementRef;
  @ViewChild('confirmation', {static: false}) confirmation: ElementRef;

  //modalref: any;
  ngactivemodal: any;
  message: string;
  recipedirection: string;
  recipes: Recipe[] = [];
  isNewRecipe: boolean;
  enableEditRecipe: boolean;
  displayRecipes: Recipe[] = [];
  rightingredients = [];
  newRecipe: Recipe;
  viewColumn: boolean[];
  columnList: string[];
  page: number;
  searchKey: string;
  searchArea:string;
  confirmationmessage: string = '';
  toconfirm: string = '';
  sopModal: any;
  warnModal: any;
  isLoading:boolean;
  count:number;
  currentuser: User;

  constructor(private modalService: NgbModal, private recipeService: RecipeService,
    private ingredientService: IngredientService, 
    private authenticationservice: AuthenticationService, private userhistoryservice: UserhistoryService) { }

  onClick(event) {
    if (!this.isNewRecipe && event !== undefined && event.path !== undefined && event.path.find(x => x.className !== undefined && x.className.includes('recipename')) !== undefined) {
      let itempath = event.path.find(x => x.className.includes('recipename'));
      if(itempath == undefined || itempath.id == undefined || itempath.innerText == undefined) return;
      this.showrecipe(this.displayRecipes.find(x=>x.somruid == itempath.id && x.name == itempath.innerText.trim()))
      //this.showrecipe(this.displayRecipes.find(x=> x.name == itempath.innerText.trim()))
    }
    else if (event.path !== undefined && event.path.find(x => x.className !== undefined && x.className == 'slide') !== undefined)
      return;
    else if (event.path !== undefined && event.path.find(x => x.innerText !== undefined && x.innerText.trim() == 'New SOP') !== undefined) {
      this.newSOP()
      return;
    }
    else {
      if (!this.isNewRecipe) {
        this.reset();
      }
    }
  }

  ngOnInit() {
    // this.reset();
    let getcurrentuser = this.authenticationservice.getCurrentUser()
    if (getcurrentuser !== undefined) {
      getcurrentuser.then(_ => {
        this.currentuser = User.fromJson(_);
      });
    }
    this.viewColumn = [true, true, true, true, true, true, false, false, false, false, true, false];
    this.columnList = ['Recipe Id', 'Name', 'Volume','Unit', 'Description', 'Entered Person', 'Reviewd Person', 'Approved Person',
      'Review Comment', 'Approve Comment', 'Edit Status', 'Direction'];
    this.page = 1;
    this.searchKey = '';
    this.searchArea = ''
    this.isLoading = true;
    this.recipeService.getCount().subscribe(c=>{
      this.count = c;
      this.loadPage();
      this.isLoading = false;
    })

  }

  ngOnChanges(change: SimpleChanges) {
    // this.reset();
  }

  toggleViewCol(index: number) {
    this.viewColumn[index] = !this.viewColumn[index];
  }

  loadPage() {
    this.isLoading = true;
    this.recipeService.loadRecipeByPage(this.page - 1).subscribe(recipes => {
      this.recipes = recipes;
      this.displayRecipes = this.recipes.slice();
      this.isLoading = false;
    });
  }

  showrecipe(recipe: Recipe) {
    this.displayRecipe = recipe;
    this.isShowRecipe = true;
  }

  open(content, type?: string) { // pass modal type
    //default can not edit
    this.enableEditRecipe = false;
    this.isShowRecipe = true;

    //tell it is a new recipe
    if (type == 'nr') {
      //if a new recipe, then create a new recipe and assign to displayRecipe
      this.isNewRecipe = true;
      this.newSOP();
      this.displayRecipe = this.newRecipe;
    }

    this.sopModal = this.modalService.open(content, { backdrop: "static", size: 'lg' });
    this.sopModal.result.then((result) => {
      this.enableEditRecipe = false;
      this.isNewRecipe = false;
      this.reset();
    }, () => {
      //close with X
      this.reset();
    });
  }

  newSOP() {
    this.newRecipe = new Recipe(-1, '', '', '', '', '', '', '', '', '', new Date(), null, null, '', '', '');
    this.enableEditRecipe = true;
    this.isShowRecipe = true;
    this.displayRecipe = this.newRecipe;
    this.isNewRecipe = true;
  }

  reset() {
    this.enableEditRecipe = false;
    this.isShowRecipe = false;
    this.displayRecipe = undefined;
    this.resetView.emit();
    this.isNewRecipe = false;
    this.page = 1;
    this.searchKey = '';
  }

  searchRecipe(key: string) {
    this.searchKey = key;
    if (this.searchKey == undefined || this.searchKey == '') {
      this.isLoading = true;
      this.recipeService.getCount().subscribe(c => {
        this.count = c;
        this.page = 1;
        this.isLoading = true;
        this.loadPage();
        this.isLoading = false;
      })
    }
    else{
      this.displayRecipes = [];
      this.recipes = [];
      this.recipeService.searchRecipe(this.searchKey, this.page - 1, this.searchArea).subscribe(result => {
        this.displayRecipes = result.slice();
        this.recipeService.searchRecipeCount(this.searchKey, this.searchArea).subscribe(count=>{
          this.count=count;
        })
        this.isLoading = false;
      })
    }


  }

  //search recipe by id or name
  filterDisplay(key: string) {
    if (key == undefined) return;
    if (this.displayRecipes == undefined) return;

    if (key.trim() == '') {
      this.displayRecipes = this.recipes.slice();
      return;
    }
    this.recipeService.loadRecipe().subscribe(recipes => {
      this.recipes = recipes;
      this.displayRecipes = [];

      this.recipes.forEach((recipe, index) => {
        if (recipe.name !== undefined && recipe.name.toUpperCase().includes(key.toUpperCase()) || recipe.somruid !== undefined && recipe.somruid.toString().includes(key.toUpperCase())) {
          this.displayRecipes.push(recipe);
        }
      });
    });
  }

  saveRecipe(r: { recipe: Recipe, updatedsopingredients: Ingredient[] }) {
    if (r.recipe == undefined) return;
    if (r.updatedsopingredients == undefined) return;

    //new recipe
    if (r.recipe.dbid == -1) {
      this.recipeService.addRecipe(r.recipe).then(_ => {
        let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'SOP', 'Add a New Recipe', 'Recipe name: '+r.recipe.name+', Recipe Id: '+r.recipe.somruid)
        this.userhistoryservice.addUserHistory(userhistory);

        let dbid = _['dbid'];
        r.recipe.dbid = dbid;
        r.updatedsopingredients.forEach((ingredient, index) => {
          if (ingredient.dbid == -1) {
            ingredient.recipeid = r.recipe.dbid;
            this.ingredientService.addIngredient(ingredient).then(_ => {
              let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'SOP', 'Add a New Ingredient for a Recipe', 'Recipe name: '+ingredient.recipename+', Recipe Id: '+ingredient.recipesomruid+', Ingredient Name: '+ingredient.itemname+', Ingredient Cat#'+ingredient.cat)
              this.userhistoryservice.addUserHistory(userhistory);

              ingredient.dbid = _['dbid'];
              if (index == r.updatedsopingredients.length - 1) {
                this.isShowRecipe = false;
                this.resetrecipedisplay(r.recipe);
                this.page=1;
                this.loadPage();
              }
            });
          }
        })
      });
    }
    else {
      //an existing recipe
      let allpromises = [];
      this.recipeService.updateRecipe(r.recipe).then(_ => {
        let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'SOP', 'Update a Recipe', 'Recipe name: '+r.recipe.name+', Recipe Id: '+r.recipe.somruid)
        this.userhistoryservice.addUserHistory(userhistory);

        let index = this.displayRecipes.findIndex(find => find.dbid == r.recipe.dbid);
        if (index != -1) {
          this.displayRecipes[index] = r.recipe;
        }

        this.ingredientService.getIngredientWithId(r.recipe.dbid).subscribe(ingredients => {
          let originalsopingredients = ingredients;

          //if user remove recipe ingredients
          originalsopingredients.forEach(ingredient => {
            let index = r.updatedsopingredients.findIndex(updatedingredient => updatedingredient.dbid == ingredient.dbid);
            if (index == -1) {
              allpromises.push(this.ingredientService.deleteIngredient(ingredient).then(
                ()=>{
                  let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'SOP', 'Delete an Ingredient of a Recipe', 'Recipe name: '+ingredient.recipename+', Recipe Id: '+ingredient.recipesomruid+', Ingredient Name: '+ingredient.itemname+', Ingredient Cat#: '+ingredient.cat)
                  this.userhistoryservice.addUserHistory(userhistory);
                }
              ));
            }
          });

          //if user edit the existing sop ingredients or add new sop ingredient
          r.updatedsopingredients.forEach(ingredient => {
            let index = originalsopingredients.findIndex(x => x.dbid == ingredient.dbid);
            //if user add new ingredients for this recipe
            if (index == -1) {
              allpromises.push(this.ingredientService.addIngredient(ingredient));
            }//if user updated the recipe ingredients
            else {
              let index = originalsopingredients.findIndex(originalcomponent => originalcomponent.dbid == ingredient.dbid);
              if (JSON.stringify(ingredient) != JSON.stringify(originalsopingredients[index])) {
                allpromises.push(this.ingredientService.updateIngredient(ingredient));
              }
            }
          });
          Promise.all(allpromises).then(_ => {
            this.isShowRecipe = false;
            this.resetrecipedisplay(r.recipe);
            this.loadPage();
          })
        });
      });
    }
  }

  resetrecipedisplay(r: Recipe) {
    this.displayRecipe = r;
    this.enableEditRecipe = false;
    this.isNewRecipe = false;
  }

  openconfirmation(message) {
    if (message == 'delete') {
      this.toconfirm = 'delete';
      this.confirmationmessage = 'Are you sure you want to delete it?';
      this.modalService.open(this.confirmation, { backdrop: "static" }).result.then(result => { });
    }
  }

  action() {
    if (this.toconfirm == 'delete') {
      this.deleteRecipe();
      //this.sopModal.close() ;
    }
  }

  deleteRecipe() {
    if (this.displayRecipe == undefined || this.displayRecipe.dbid == -1) { return; }
    let tempsopingredients;
    let id = this.displayRecipe.dbid

    //if this recipe exist in displayRecipes
    this.recipeService.deleteRecipe(this.displayRecipe)
    .then((result) => {
      let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'SOP', 'Delete a Recipe', 'Recipe name: '+this.displayRecipe.name+', Recipe Id: '+this.displayRecipe.somruid)
      this.userhistoryservice.addUserHistory(userhistory);

      //delete all recipe ingredients
      this.ingredientService.getIngredientWithId(id).subscribe(ingredient => {
        tempsopingredients = ingredient.slice();
        tempsopingredients.forEach((ingredient,index) => { 
          if(index==tempsopingredients.length-1){
            this.ingredientService.deleteIngredient(ingredient).then(result=>{
              this.displayRecipe = undefined;
              this.isShowRecipe = false;
              this.loadPage();
            })
          }
          else{
            this.ingredientService.deleteIngredient(ingredient)
          }
        });
      });
    }).catch(err => { })
  }
}

