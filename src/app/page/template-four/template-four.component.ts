import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { Query } from "@syncfusion/ej2-data";
import { EmitType } from "@syncfusion/ej2-base";
import { FilteringEventArgs } from "@syncfusion/ej2-dropdowns";
import { TimerComponent } from "../timer/timer.component";
export interface Checmicals {
  value: string;
  viewValue: string;
}

@Component({
  selector: "app-template-four",
  templateUrl: "./template-four.component.html",
  styleUrls: ["./template-four.component.css"]
})
export class TemplateFourComponent implements OnInit {
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;
  timer = new TimerComponent(4);
  seventhFormGroup: FormGroup;
  eigthFormGroup: FormGroup;
  // time spans for the four sections in template one
  counterSec = [0, 0, 0, 0];

  chemicals: Checmicals[] = [
    { value: "chemical-1", viewValue: "Somru1" },
    { value: "chemical-2", viewValue: "Somru2" },
    { value: "chemical-3", viewValue: "Somru3" }
  ];

  constructor(private _formBuilder: FormBuilder) {}

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ["", Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ["", Validators.required],

      // control for item inputs
      step2_item_1: ["", Validators.required],
      step2_item_2: ["", Validators.required],
      step2_item_3: ["", Validators.required],
      step2_item_4: ["", Validators.required],
      step2_item_5: ["", Validators.required],
      step2_item_6: ["", Validators.required],
      step2_item_7: ["", Validators.required],
      //

      // control for supplier inputs
      step2_supplier_1: ["", Validators.required],
      step2_supplier_2: ["", Validators.required],
      step2_supplier_3: ["", Validators.required],
      step2_supplier_4: ["", Validators.required],
      step2_supplier_5: ["", Validators.required],
      step2_supplier_6: ["", Validators.required],
      step2_supplier_7: ["", Validators.required],
      //

      // control for concentration inputs
      step2_concentration_1: ["", Validators.required],
      step2_concentration_2: ["", Validators.required],
      step2_concentration_3: ["", Validators.required],
      step2_concentration_4: ["", Validators.required],
      step2_concentration_5: ["", Validators.required],
      step2_concentration_6: ["", Validators.required]
      //
    });
    this.thirdFormGroup = this._formBuilder.group({
      thirdCtrl: ["", Validators.required],
      // control for item inputs
      step3_item_1: ["", Validators.required],
      step3_item_2: ["", Validators.required],
      step3_item_3: ["", Validators.required],
      //

      // control for supplier inputs
      step3_supplier_1: ["", Validators.required],
      step3_supplier_2: ["", Validators.required],
      step3_supplier_3: ["", Validators.required]
      //
    });
    this.fourthFormGroup = this._formBuilder.group({
      fourthCtrl: ["", Validators.required],
      // control for item inputs
      step4_item_1: ["", Validators.required],
      step4_item_2: ["", Validators.required],
      //

      // control for supplier inputs
      step4_supplier_1: ["", Validators.required],
      step4_supplier_2: ["", Validators.required],
      //

      // control for concentration inputs
      step4_concentration_1: ["", Validators.required]
      //
    });
    this.fifthFormGroup = this._formBuilder.group({
      fifthCtrl: ["", Validators.required],

      // control for item inputs
      step5_item_1: ["", Validators.required],
      step5_item_2: ["", Validators.required],
      //

      // control for supplier inputs
      step5_supplier_1: ["", Validators.required],
      step5_supplier_2: ["", Validators.required],
      //

      // control for concentration inputs
      step5_concentration_1: ["", Validators.required]
      //
    });
    this.sixthFormGroup = this._formBuilder.group({
      sixthCtrl: ["", Validators.required],

      // control for item inputs
      step6_item_1: ["", Validators.required],
      //

      // control for supplier inputs
      step6_supplier_1: ["", Validators.required]
      //
    });
    this.seventhFormGroup = this._formBuilder.group({
      seventhCtrl: ["", Validators.required]
    });
    this.eigthFormGroup = this._formBuilder.group({
      eigthCtrl: ["", Validators.required]
    });
  }

  // save the time span for count down timer
  saveTime(param: number, section: number) {
    this.counterSec[section] = param;
  }

  //getter for time span
  getTime(arrIndex: number) {
    return this.counterSec[arrIndex];
  }

  //return the number of total timers used in this  template
  getTimerNumber() {
    return this.counterSec.length;
  }
}
