import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { item, itemdetail } from '../../shared/objects/item';
import { orderitem } from '../../shared/objects/OrderItem';
import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild, Output, EventEmitter } from '@angular/core';
import { InventoryService } from '../../shared/services/Inventory.Service';
import { ArchiveService } from '../../shared/services/archive.service';
import * as XLSX from 'xlsx'//'ts-xlsx';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { SettingService } from '../../shared/services/Setting.Service';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { ItemdetailService } from '../../shared/services/itemdetail.service';
import { TransactionService } from '../../shared/services/Transaction.Service';
import { User } from '../../shared/objects/User';
import { AuthenticationService } from '../../shared/services/Authenticate.Service';
import { UserhistoryService } from '../../shared/services/userhistory.service';
import { Userhistory } from '../../shared/objects/Userhistory';
import { Eggtransfer } from '../../shared/objects/eggtransfer';
import { Itemarchive, ItemChange, Itemdetailarchive, ItemdetailChange } from '../../shared/objects/archives';
import { BarcodeService } from '../../shared/services/barcode.service';
import { IMyDpOptions } from 'mydatepicker';
import { Barcode } from '../../shared/objects/Barcode';
import { IfObservable } from 'rxjs/observable/IfObservable';


export class toggleItem {
  constructor(
    public masterId: number,
    public detailId: number,
    public itemCat: string,
    public itemName: string,
    public itemLot: string,
    public usize: string,
    public unit: string,
    public itemQuan: string,
    public nonopencheckoutamount: string,
    public inuseQuan: string,
    public inusecheckoutamount: string,
    public userforrecon:string,
    public reserved:string
  ) { }
}

export class compareChange {
  constructor(
    public field: string,
    public oldValue: string,
    public newValue: string
  ) { }
}

export class selectmolecule {
  constructor(
    public name: string, 
    public cat: string, 
    public supplier: string, 
    public stockamount:number, 
    public stockunit:string, 
    public requireamount:string, 
    public requireunit:string,
    public itemdetail:itemdetail,
    public itemdetails:itemdetail[]
  ) { }
}

export class newConjugate {
  constructor(
    public biomoleculename: string, 
    public biomolecule: string, 
    public checkoutbiomolecule: string,
    public biodepleteunopened: string, 
    public biodepleteinuse: string, 
    public biotransfer: string, 
    public bioamount: string, 
    public biounit: string, 
    public biorequireamount: string, 
    public conjugatename:string, 
    public conjugate:string, 
    public checkoutconjugate: string,
    public condepleteunopened: string, 
    public condepleteinuse: string, 
    public contransfer: string, 
    public conamount:string, 
    public conunit:string, 
    public conrequireamount: string, 
    public name:string,
    public projectnumber:string,
    public reserve:string,
    public lot:string,
    public bca:string,
    public runnumber:string,
    public biomoleculeinfo: string,
    public conjugateinfo: string,
    public amount: string, 
    public unitsize: string, 
    public unit: string, 
    public concentration:string, 
    public concentrationunit:string, 
    public location:string, 
    public sublocation:string,
    public chemistry:string,
    public ratio: string, 
    // public description: string, 
    public comment: string, 
    public prepdate: Date, 
    public prepperson:string
  ) { }
}



@Component({
  selector: 'app-inventory',
  providers: [InventoryService, SettingService, ItemdetailService, TransactionService, UserhistoryService, BarcodeService, ArchiveService],
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css'],
  host: {
    '(document:click)': 'onClick($event)',
  }
})
export class InventoryComponent implements OnInit, OnChanges {
  //input get from parent
  @Input() isShowItem: boolean;
  @Input() isShowItemDetail: boolean;
  @Input() item: item;
  @Input() displayitemdetail: itemdetail;
  @Input() eggtransfer: Eggtransfer;
  @Input() enabledEditItem: boolean;
  @Input() edititemdetail: boolean;
  @Input() isShowItemLog: boolean;



  //create a reference of input of #displayitem in inventory.component.html
  @ViewChild('displayitem', {static: false}) displayitem: ElementRef;
  @ViewChild('confirmation', {static: false}) confirmation: ElementRef;
  @ViewChild('barcode', {static: false}) barcode: ElementRef;
  @ViewChild('checkItem', {static: false}) checkItem: ElementRef;
  @ViewChild('ItemLog', {static: false}) ItemLog: ElementRef;
  @ViewChild('ItemdetailLog', {static: false}) ItemdetailLog: ElementRef;
  @ViewChild('openconjugate', {static: false}) openconjugate: ElementRef;
  @ViewChild('compare', {static: false}) compare: ElementRef;
  
  //output an event to parent
  @Output() cleanView = new EventEmitter();
  @Output() resetView = new EventEmitter();
  @Output() sendorder = new EventEmitter();

  invenLength: number;
  addnewitemdetail: itemdetail;
  showunreceive: boolean;
  currentuser: User;
  // selectItemName = '';
  page: number;
  partinvent: item[] = [];
  multipleorderarray;
  viewColumn: boolean[];
  columnList: string[];
  closeResult: string;
  isNewItem: boolean;
  isNewItemDetail: boolean;
  invent: item[] = [];
  displayInvent: { master: item, show: boolean, detail: itemdetail[] }[] = [];
  newitem: item;
  selectItems: toggleItem[];
  message: string;
  checkeditems: itemdetail[] = [];
  importfile;
  partdisplayInvent: item[] = [];
  inusealert: boolean;
  amountalert: boolean;
  nextCat: Setting;
  confirmationmessage: string = '';
  toconfirm: string = '';
  whattodelete: string = '';

  itemmodal: any;
  warningmodal: any;
  barcodemodal:any;
  subcolspan: number;
  inventories: { master: item, show: boolean, detail: itemdetail[] }[];
  showitemindex: number;
  showitemdetailindex: number;

  searchkey = '';
  searchKeyword: string;
  isLoading: boolean;

  newlot = true;
  showinuse = true;
  showunit = true;

  PrintSerials=[];
  twodarray = [];
  // PrintSerials = [{
  //   SerialId: 12345,
  //   Name: 'A'
  // },
  // {
  //   SerialId: 585885,
  //   Name: 'A'
  // }];
  // }
  barcodeValue;
  twoDBarcodeValues;

  selectAction='Complete';
  submitAction='';
  checkOutModal:any;
  barcodeNumber='';
  validBarcode=true;
  inuseBarcode=false;
  alreadyComplete=false;
  completeSubmit=false;
  notFoundBarcode=false;
  itemdetailreserve=false;
  barcodeInformMissing=false;
  missingInform=[];
  showitemlotdetail=false;
  checkoutBarcode:Barcode;
  checkoutItemdetail:itemdetail;
  checkoutItem:item;

  // barcodeString='0000000000000001';
  promisearray=[];

  itemLogModal:any;
  itemdetailLogModal:any;
  
  itemLogObject:{action:string,changes:compareChange[],time:Date,user:string}[]=[];

  itemdetailLogObject:{action:string,changes:ItemdetailChange,time:Date,user:string}[]=[]

  updaterf=false;
  updatecon=false;
  itemcategory=''
  conjugateitemtype='';

  //conjugate variable
  conjugatePrepdateDisplay: { date: { year: number, month: number, day: number } };
  conjugatePrepdate:Date=new Date();
  prepareDateOptions: IMyDpOptions = { dateFormat: 'mm/dd/yyyy' };
  allusers:User[]=[];
  conjugateChemistryOptions:ConfigValue[]=[];
  conjugateRatioOptions:ConfigValue[]=[];
  conjugateModal:any;

  newConjugate=new newConjugate('','','', '','','','','','','','','','','','','','','','','','','','','','','', '','','','','','', '','','','',new Date(),'');

  moleculeFilterList: { dbid:number, name: string, cat: string, supplier: string, unitsize:string, unit:string}[];
  conjugateFilterList: { dbid:number, name: string, cat: string, supplier: string, unitsize:string, unit:string}[];
  showMoleculeList:boolean;
  showConjugateList:boolean;
  selectedMolecule= new selectmolecule('','','',0,'','','',null, null);
  selectedConjugate= new selectmolecule('','','',0,'','','', null, null);

  hideconjugateformrow=[true,true,true];
 

  unitList: ConfigValue[];
  volUnit: ConfigValue[];
  massUnit: ConfigValue[];
  otherUnit: ConfigValue[];
  conUnit: ConfigValue[];
  locations: ConfigValue[] = [];
  sublocations: ConfigValue[] = [];
  allsublocations: ConfigValue[] = [];
  projectnumberoptions: ConfigValue[] = [];
  conjugateCompareModel:any;
  compareConjugateName='';
  compareConjugateList: { dbid:number, name: string, cat: string, supplier: string}[];
  showCompareConjugateList=true;
  compareConjugateItemLots:itemdetail[]=[];
  itemlotDynamicList:string[]=[];
  selectConjugateItemLots: { index:number, itemdetail:itemdetail}[]=[];
  showComparisonResult=false;
  showList=[true,true];
  conjugteCompareColspan:number;
  differentrows=[false, false, false, false, false, false, false, false,false, false, false,false, false]
 
  allItems=[];

  

  constructor(private modalService: NgbModal, private inventService: InventoryService, config: NgbPaginationConfig,
    private settingservice: SettingService, private itemdetailservice: ItemdetailService, private transService: TransactionService, 
    private authenticationservice: AuthenticationService, private userhistoryservice: UserhistoryService, private barcodeservice: BarcodeService,
    private archiveservice: ArchiveService) { }

  onClick(event) {
    if (!this.isNewItem && event !== undefined && event.path !== undefined && event.path.find(x => x.className !== undefined && x.className.toString().includes('itemname')) !== undefined) {
      // this.reset();
      let itempath = event.path.find(x => x.className.toString().includes('itemname'));
      let itemname = itempath.innerText.trim();
      if (itempath.className.toString().includes('master')) {
        //this.showitem(this.displayInvent.find(x => x.master.name.trim() == itemname.trim()).master);
        let masterindex=itempath.id;
        this.showitem(this.displayInvent[masterindex].master);
      }
      else if (itempath.className.toString().includes('detail')) {
        //let masterindex = Number(itempath.id);
        let totalindex = itempath.id.split(' ');
        let masterindex = totalindex[0];
        let detailindex = totalindex[1];
        if (masterindex == -1 || masterindex >= this.displayInvent.length || this.displayInvent[masterindex] == undefined || this.displayInvent[masterindex].master == undefined) return;
        let lot = itemname;
        itemname = this.displayInvent[masterindex].master.name;
        this.showitemdetail(this.displayInvent[masterindex].detail[detailindex]);
      }
    }
    else if (event.path !== undefined && event.path.find(x => x.innerText !== undefined && x.innerText.trim() == 'New Item') !== undefined) {
      this.newItem()
      return;
    }
    else if (event.path !== undefined && event.path.find(x => x.className !== undefined && x.className.toString() == 'slide') !== undefined ||
      event.path !== undefined && event.path.find(x => x.className !== undefined && x.className.toString().includes('checkout-checkin')) !== undefined ||
      event.path !== undefined && event.path.find(x => x.className !== undefined && x.className.toString().includes('btn btn-default')) !== undefined)
      return;
    else {
      if (!this.isNewItem) {
        this.reset();
      }
    }
  }

  ngOnInit() {
    this.inventService.loadInventory().subscribe(items=>{
      this.allItems=items.slice();
    })


    let getcurrentuser = this.authenticationservice.getCurrentUser()
    if (getcurrentuser !== undefined) {
      getcurrentuser.then(_ => {
        this.currentuser = User.fromJson(_);
      });
    }

    this.authenticationservice.loadAllUsers().subscribe(allusers=>{
      this.allusers=allusers;
    })

    this.settingservice.getSettingByPage('catalog').subscribe(config => {
      this.nextCat = config.find(x => x.type == 'CatPointer');
    });

    this.settingservice.getSettingByPage('invent').subscribe(config => {
      this.conjugateChemistryOptions = ConfigValue.toConfigArray(config.find(x => x.type == 'conjugatechemistry').value)
      this.conjugateRatioOptions = ConfigValue.toConfigArray(config.find(x => x.type == 'conjugateratio').value)
    });

    this.settingservice.getSettingByPage('orderItem').subscribe(config => {
      this.projectnumberoptions = ConfigValue.toConfigArray(config.find(x => x.type == 'projectList').value)
    });


    this.settingservice.getSettingByPage('general').subscribe(config => {
      this.unitList = [];
      this.volUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'volUnit').value)
      if (this.volUnit == []) this.volUnit = Setting.getVolumnUnit()
      this.volUnit.forEach(v => { this.unitList.push(v) })
      this.massUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'massUnit').value)
      if (this.massUnit == []) this.massUnit = Setting.getMassUnit()
      this.massUnit.forEach(m => { this.unitList.push(m) })
      this.otherUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'otherUnit').value)
      if (this.otherUnit == []) this.otherUnit = Setting.getOtherUnit()
      this.otherUnit.forEach(o => { this.unitList.push(o) });

      this.conUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'conUnit').value)
      this.locations = ConfigValue.toConfigArray(config.find(x => x.type == 'location').value)
      this.allsublocations = ConfigValue.toConfigArray(config.find(x => x.type == 'subLocation').value)
    });

    this.page = 1;
    // this.loadInvent()
    this.searchKeyword = '';
    this.isLoading = true;
    this.inventService.getCount().subscribe(c => {
      this.invenLength = c;
      this.loadPage();
    })
    this.viewColumn = [true, true, true, true, false, true, true, true, true, false, true, false, false];
    this.columnList = ['Item Status', 'Supplier Catalog #', 'Item Type',
      'Supplier Name', 'Manufacturer',
      'Amount/Quantity', 'Unit', 'Unit Size',
      'In Use', 'Unit Price (USD)', 'Quantity Threshold',
      'Last Modify', 'Comment(s)'];
    this.subcolspan = 2;
    this.viewColumn.forEach(v => {
      if (v) this.subcolspan++;
    })


    let r = new Date();
    this.conjugatePrepdateDisplay = { date: { year: r.getFullYear(), month: r.getMonth() + 1, day: r.getDate() } };

    // this.itemdetailservice.loadItemDetail().subscribe(itemdetails=>{
    //   itemdetails.forEach(itemdetail=>{
    //     this.inventService.getItemWithId(itemdetail.itemdbid).subscribe(result=>{},
    //       error=>{
    //         if(error.status=='404'){
    //           console.log('did not find item');
    //           this.itemdetailservice.deleteItemDetail(itemdetail);
    //         }
    //       })
    //   })
    // })

    // this.itemdetailservice.loadItemDetail().subscribe(itemdetails=>{
    //   itemdetails.forEach(itemdetail=>{
    //     if(!itemdetail.inuse){
    //       itemdetail.inuse='';
    //       this.itemdetailservice.updateSingleItemDetail(itemdetail);
    //     }
    //   })
 
    // })
    



    // this.inventService.loadInventory().subscribe(items=>{
    //   items.forEach(item=>{
    //     this.itemdetailservice.loadItemDetailByItemDbid(item.dbid).subscribe(itemdetails=>{
    //       itemdetails.forEach(itemdetail=>{
    //         itemdetail.itemdbid = item.dbid;
    //         itemdetail.itemname = item.name.trim();
    //         if(item.type!=undefined){
    //           itemdetail.itemtype=item.type.trim();
    //         }
    //         if(item.supplier!=undefined){
    //           itemdetail.supplier=item.supplier.trim();
    //         }
    //         this.itemdetailservice.updateSingleItemDetail(itemdetail);
    //       })
    //     })

    //   })
    // })


  //   let barcodeString='0000000000000001';
  //   let allpromises=[];
  //   let allBarcodesList:Barcode[]=[];
  
  //   this.itemdetailservice.loadItemDetail().subscribe(itemdetails=>{
  //     itemdetails.forEach(itemdetail=>{
  //       let amount=Number(itemdetail.amount);
  //       let inuse=Number(itemdetail.inuse);

  //       if( !isNaN(amount) && amount!=undefined && amount!=null && amount>0){
  //         let i;
  //         for(i=0;i<amount;i++){
  //           let newbarcode=new Barcode(-1, itemdetail.dbid, barcodeString, 'Un-Open', this.currentuser.name, new Date());
  //           //this.barcodeservice.addBarcode(newbarcode);
  //           allBarcodesList.push(newbarcode);
  //           barcodeString=this.increaseBarcode(barcodeString);
  //         }
          
  //       }
  //       if(inuse!=undefined && inuse!=null && inuse>0){
  //         let i;
  //         for(i=0;i<inuse;i++){
  //           let newbarcode=new Barcode(-1, itemdetail.dbid, barcodeString, 'In-Use', this.currentuser.name, new Date());
  //           //this.barcodeservice.addBarcode(newbarcode);
  //           allBarcodesList.push(newbarcode);
  //           barcodeString=this.increaseBarcode(barcodeString);
  //         }
  //       }

  //     })

  //     let count=0;
  //     let barcodeListLength=allBarcodesList.length;
  //     this.addBarcodesToDatabase(count, barcodeListLength, allBarcodesList);




  //   })


  // }

  // addBarcodesToDatabase(count, barcodeListLength, allBarcodesList){
  //   if(count<barcodeListLength){
  //     this.barcodeservice.addBarcode(allBarcodesList[count]).then(()=>{
  //       count=count+1;
  //       this.addBarcodesToDatabase(count, barcodeListLength, allBarcodesList);
  //     })
  //   }
  //   else{
  //     return;
  //   }


  
  }


  increaseBarcode(barcodeString):string{
    let barcodeNumber=Number(barcodeString);
    let nextBarcodeNumber=barcodeNumber+1;
    let nextBarcodeString=nextBarcodeNumber.toString();
    
    let missZeros=16-nextBarcodeString.length;
    for(let i=0; i<missZeros;i++){
      nextBarcodeString='0'+nextBarcodeString;
    }
    return nextBarcodeString;
  }

  ngOnChanges(change: SimpleChanges) {
    //this.enabledEditItem = false;
  }

  ngAfterViewInit() {

    
}

onValueChanges(result){
    this.barcodeValue = result.codeResult.code;
}

  showitem(item: item) {
    this.isShowItemDetail = false;
    this.item = item;
    this.isShowItem = true;
    this.isNewItemDetail = false;
    this.enabledEditItem = false;
    //this.displayitemdetail = undefined;
  }

  open(content, x?: item) {
    if (x) {
      this.item = x;
      this.isShowItem = true;
    }

    this.modalService.open(content, { backdrop: "static", size: 'lg' }).result
      .then((result) => {
        if (this.item) this.reset();
      }, () => {
        if (this.item) this.reset();
      });
  }

  loadPage() {
    this.isLoading = true;
    this.inventories = [];
    this.displayInvent = [];
    this.inventService.loadInventoryWDetails(this.page - 1).subscribe(iwd => {

      iwd.forEach(pair => {
        pair.item.amount = 0;
        pair.item.inuse = 0;
        pair.details.forEach(d => {
          if (!isNaN(Number(d.amount))) {
            pair.item.amount += Number(d.amount);
          }
          if (!isNaN(Number(d.inuse))) {
            pair.item.inuse += Number(d.inuse);
          }
        })

        this.inventories.push({ master: pair.item, show: false, detail: pair.details });
      })
      this.displayInvent = this.inventories.slice();
      this.amountalert = false;
      this.inusealert = false;
      this.isLoading = false;
    });
  }

  reset() {
    this.enabledEditItem = false;
    this.edititemdetail = false;
    this.isShowItem = false;
    this.isShowItemDetail = false;
    this.item = undefined;
    this.isNewItem = false;
    this.isNewItemDetail = false;
    //this.selectItems = [];
    this.showitemindex = undefined;
    this.showitemdetailindex = undefined;
    this.showunreceive = undefined;
    this.addnewitemdetail = undefined;
    this.updaterf=false;
    this.updatecon=false;
    this.cleanView.emit();

  }

  collapseAll() {
    if (this.inventories == undefined) return;
    this.inventories.forEach(i => {
      if (i !== undefined)
        i.show = false;
    })
  }

  //updateItem(i: { titem: item, itemdetail: itemdetail[]}) {
  updateItem(event) {
    if (event == undefined) return;

    let itemindex = this. displayInvent.findIndex(x=>x.master.dbid==event.titem.dbid);
    if(itemindex!=-1){
      this.displayInvent[itemindex].master=event.titem;
      if(event.itemdetail!=undefined){
        this.displayInvent[itemindex].detail=event.itemdetail;
      }
      this.reset();
    }
    else{
        this.isNewItem = false;
        this.enabledEditItem = false;
        this.reset();
        this.page = 1;
        this.isLoading=true;
        this.loadPage();
        this.isLoading=false;
    }

  }

  // generateBarcode(itemdetail:itemdetail){
  //   console.log('itemdetail is ');
  //   console.log(itemdetail);
  //   this.PrintSerials=[];
  //   this.twoDBarcodeValues=[];

  //   let total=Number(itemdetail.amount)+Number(itemdetail.inuse);
  //   console.log('total is ');
  //   console.log(total);
  //   let i;
  //   for(i=0;i<total;i++){
  //     this.PrintSerials.push(itemdetail.dbid);
  //     this.twoDBarcodeValues.push(itemdetail.dbid.toString());
  //   }

  //   console.log('printserials are: ');
  //   console.log(this.PrintSerials);

  //   this.twodarray=[];
  //   let onedarray=[];
  //   let a;
  //   for(a=0; a<this.PrintSerials.length;a++){
  //     if(a==0 || a%3!=0){
  //       console.log('a is: '+a)
  //       console.log('one line serial number is: '+this.PrintSerials[a]);
  //       onedarray.push(this.PrintSerials[a]);
  //     }
  //     else{
  //       console.log('next line serial number is: '+this.PrintSerials[a]);
  //       this.twodarray.push(onedarray);
  //       onedarray=[];
  //       onedarray.push(this.PrintSerials[a]);
  //     }
  //   }

  //   this.twodarray.push(onedarray);
  //   onedarray=[];

  //   console.log('twodarray are: ');
  //   console.log(this.twodarray);
    
  //   this.barcodemodal=this.modalService.open(this.barcode, { backdrop: "static", size: 'lg' });
  //   this.isShowItemDetail=false;
  // }

  // generate2DBarcode(itemdetail:itemdetail){
  //   this.twoDBarcodeValues=[];
  // }

  // cancelBarcode(){
  //   this.PrintSerials=[];
  //   this.barcodemodal.close();
  // }



  openCheckOut(){
    this.checkOutModal=this.modalService.open(this.checkItem, { backdrop: "static", size: 'lg' });
  }

  checkInput(event){
    let input=event.clipboardData.getData('text/plain')
    let inputString=input.toString();
    if(isNaN(input) || inputString.length!=16){
      this.barcodeNumber='';
      this.validBarcode=false;
      event.preventDefault();
      setTimeout(()=>{ this.validBarcode=true;}, 3000);
    }
  }

  displayItemlot(){
    this.selectAction='Complete';
    this.submitAction='';
    this.validBarcode=true;
    this.inuseBarcode=false;
    this.completeSubmit=false;
    this.notFoundBarcode=false;
    this.barcodeInformMissing=false;
    this.itemdetailreserve=false;
    this.alreadyComplete=false;
    this.missingInform=[];
    this.showitemlotdetail=false;
    this.checkoutBarcode=undefined;
    this.checkoutItemdetail=undefined;
    this.checkoutItem=undefined;

    this.missingInform=[];
    this.barcodeservice.getBarcodesByBarcode(this.barcodeNumber).subscribe(barcode=>{
      this.checkoutBarcode=barcode;
      this.itemdetailservice.getItemdetailWithId(this.checkoutBarcode.itemdetaildbid).subscribe(itemdetail=>{
        this.checkoutItemdetail=itemdetail;

        if(this.checkoutItemdetail.reserve=='Yes'){
          this.itemdetailreserve=true;
        }

        this.inventService.getItemWithId(this.checkoutItemdetail.itemdbid).subscribe(item=>{
          this.checkoutItem=item;

          if(this.checkoutItem.category=='Reference Material'){
            this.missingInform.push('Item category is Reference Material.')
            if(this.checkoutItemdetail.rfstatus!='Approved'){
              this.barcodeInformMissing=true;
              this.missingInform.push('Reference Material is not Approved to Use!')
            }

            if(!this.checkoutItem.manufacturer || this.checkoutItem.manufacturer.trim()==''){
              this.barcodeInformMissing=true;
              this.missingInform.push('Item manufacturer is missing!')
            }
            if(!this.checkoutItem.supplier || this.checkoutItem.supplier.trim()==''){
              this.barcodeInformMissing=true;
              this.missingInform.push('Item supplier is missing!')
            }
  
            if(!this.checkoutItemdetail.lotnumber){
              this.barcodeInformMissing=true;
              this.missingInform.push('Lot# is missing!')
            }
  
            if(!this.checkoutItemdetail.batchnumber){
              this.barcodeInformMissing=true;
              this.missingInform.push('Batch# is missing!')
            }
  
  
            if(!this.checkoutItemdetail.receivedate){
              this.barcodeInformMissing=true;
              this.missingInform.push('Receive Date is missing!')
            }
  
            if(!this.checkoutItemdetail.expiredate){
              this.barcodeInformMissing=true;
              this.missingInform.push('Expiry Date is missing!')
            }
  
            if(!this.checkoutItemdetail.storetemperature){
              this.barcodeInformMissing=true;
              this.missingInform.push('Store Temperature is missing!')
            }
  
            if(!this.checkoutItemdetail.purity){
              this.barcodeInformMissing=true;
              this.missingInform.push('Purity is missing!')
            }

            if(this.barcodeInformMissing==true){
              this.missingInform.push('Please go to Inventory to Update Item and Item Lot Information!')
              return;
            }


          }
          if(this.checkoutItem.type=='Conjugate'){
            this.missingInform.push('Item type is Conjugate.')
            this.missingInform.push('')
  
            if(this.checkoutItemdetail.constatus!='Approved'){
              this.barcodeInformMissing=true;
              this.missingInform.push('Conjugate is not Approved to Use!')
            }

            if(!this.checkoutItemdetail.conjugatechemistry){
              this.barcodeInformMissing=true;
              this.missingInform.push('Conjugate Chemistry # is missing!')
            }
  
            if(!this.checkoutItemdetail.biotintobiomoleculeratio){
              this.barcodeInformMissing=true;
              this.missingInform.push('Biotion to Biomolecule Ratio is missing!')
            }

  
            if(!this.checkoutItemdetail.conjugateprepperson){
              this.barcodeInformMissing=true;
              this.missingInform.push('Prepatration Person is missing!')
            }
  
            if(!this.checkoutItemdetail.conjugateprepdate){
              this.barcodeInformMissing=true;
              this.missingInform.push('Preparation Date is missing!')
            }

            if(!this.checkoutItemdetail.moleculeweight){
              this.barcodeInformMissing=true;
              this.missingInform.push('Molecule Weight is missing!')
            }
  
            if(!this.checkoutItemdetail.bindingactivity){
              this.barcodeInformMissing=true;
              this.missingInform.push('Binding Activity is missing!')
            }
  
            if(!this.checkoutItemdetail.conjugateincorporationratio){
              this.barcodeInformMissing=true;
              this.missingInform.push('Conjugte Incorporation Ratio is missing!')
            }

            if(this.barcodeInformMissing==true){
              this.missingInform.push('')
              this.missingInform.push('Please go to inventory to update item and item lot information!')
              return;
            }
          }
        })
      })
    },
    error=>{
      this.notFoundBarcode=true;
      this.barcodeNumber='';
      setTimeout(()=>{ this.notFoundBarcode=false; }, 3000);
    }
    
    )
    
  }


  submitBarcode(){
    this.inuseBarcode=false;
    this.alreadyComplete=false;
    this.completeSubmit=false;
    if(this.barcodeInformMissing==true || this.notFoundBarcode==true){
      return;
    }

    if(this.selectAction=='Complete'){
      this.submitAction='Complete'
      if(this.checkoutBarcode.usestatus.includes('Complete')){
        this.alreadyComplete=true;
      }
      else if(this.checkoutBarcode.usestatus=='Un-Open'){
        this.checkoutBarcode.usestatus='Un-Open Complete';
        this.barcodeservice.updateBarcode(this.checkoutBarcode).then(()=>{
          this.checkoutItemdetail.amount=(Number(this.checkoutItemdetail.amount)-1).toString();
          this.itemdetailservice.updateSingleItemDetail(this.checkoutItemdetail).then(()=>{

            let index = this.displayInvent.findIndex(x=>x.master.dbid==this.checkoutItemdetail.itemdbid);
            if(index!=-1){
              let dindex = this.displayInvent[index].detail.findIndex(x=>x.dbid==this.checkoutItemdetail.dbid)
                if(dindex!=-1){
                  this.displayInvent[index].detail[dindex]=this.checkoutItemdetail;
                  this.displayInvent[index].show=true;
                }

            }
            this.completeSubmit=true;
            this.inventService.getItemWithId(this.checkoutItemdetail.itemdbid).subscribe(item=>{
              let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Inventory', 'Check-Out Un-Open to Complete', 'Item Name: '+item.name+', '+'Item Cat#: '+item.cat+', '+'Item Lot dbid: '+this.checkoutItemdetail.dbid+', Lot#: '+this.checkoutItemdetail.lotnumber)
              this.userhistoryservice.addUserHistory(userhistory);
            })

          });
        })
      }
      else{
        this.checkoutBarcode.usestatus='In-Use Complete';
        this.barcodeservice.updateBarcode(this.checkoutBarcode).then(()=>{
          this.checkoutItemdetail.inuse=(Number(this.checkoutItemdetail.inuse)-1).toString();
          this.itemdetailservice.updateSingleItemDetail(this.checkoutItemdetail).then(()=>{
            let index = this.displayInvent.findIndex(x=>x.master.dbid==this.checkoutItemdetail.itemdbid);
            if(index!=-1){
              let dindex = this.displayInvent[index].detail.findIndex(x=>x.dbid==this.checkoutItemdetail.dbid)
                if(dindex!=-1){
                  this.displayInvent[index].detail[dindex]=this.checkoutItemdetail;
                  this.displayInvent[index].show=true;
                }

            }
            this.completeSubmit=true;
            this.inventService.getItemWithId(this.checkoutItemdetail.itemdbid).subscribe(item=>{
              let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Inventory', 'Check-Out In-Use to Complete', 'Item Name: '+item.name+', '+'Item Cat#: '+item.cat+', '+'Item Lot dbid: '+this.checkoutItemdetail.dbid+', Lot#: '+this.checkoutItemdetail.lotnumber)
              this.userhistoryservice.addUserHistory(userhistory);
            })
          });
        })
      }
    }
    else{
      this.submitAction='Transtoinuse';
      if(this.checkoutBarcode.usestatus=='Un-Open'){
        this.checkoutBarcode.usestatus='In-Use';
        this.barcodeservice.updateBarcode(this.checkoutBarcode).then(()=>{
          this.checkoutItemdetail.amount=(Number(this.checkoutItemdetail.amount)-1).toString();
          this.checkoutItemdetail.inuse=(Number(this.checkoutItemdetail.inuse)+1).toString();
          this.itemdetailservice.updateSingleItemDetail(this.checkoutItemdetail).then(()=>{
            let index = this.displayInvent.findIndex(x=>x.master.dbid==this.checkoutItemdetail.itemdbid);
            if(index!=-1){
              let dindex = this.displayInvent[index].detail.findIndex(x=>x.dbid==this.checkoutItemdetail.dbid)
                if(dindex!=-1){
                  this.displayInvent[index].detail[dindex]=this.checkoutItemdetail;
                  this.displayInvent[index].show=true;
                }
            }
            this.completeSubmit=true;
            this.inventService.getItemWithId(this.checkoutItemdetail.itemdbid).subscribe(item=>{
              let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Inventory', 'Check-Out to Transfer from Un-Open to In-Use', 'Item Name: '+item.name+', '+'Item Cat#: '+item.cat+', '+'Item Lot dbid: '+this.checkoutItemdetail.dbid+', Lot#: '+this.checkoutItemdetail.lotnumber)
              this.userhistoryservice.addUserHistory(userhistory);
            })
          });
        })
      }
      else if(this.checkoutBarcode.usestatus.includes('Complete')){
        this.alreadyComplete=true;
      }
      else{
        this.inuseBarcode=true;
        return;
      }
    }
    
  }

  cancelCheckOut(){
    this.checkOutModal.close();
    this.selectAction='Complete';
    this.submitAction='';
    this.barcodeNumber='';
    this.validBarcode=true;
    this.inuseBarcode=false;
    this.completeSubmit=false;
    this.notFoundBarcode=false;
    this.barcodeInformMissing=false;
    this.itemdetailreserve=false;
    this.alreadyComplete=false;
    this.missingInform=[];
    this.showitemlotdetail=false;
    this.checkoutBarcode=undefined;
    this.checkoutItemdetail=undefined;
    this.checkoutItem=undefined;
  }

  updateItemdetail(event) {
    let itemdetail=event.receiveitem;
    let index = this.displayInvent.findIndex(x=>x.master.dbid==itemdetail.itemdbid);
    if(index!=-1){
      let dindex = this.displayInvent[index].detail.findIndex(x=>x.dbid==itemdetail.dbid)
        if(dindex!=-1){
          this.displayInvent[index].detail[dindex]=itemdetail;
          this.displayInvent[index].show=true;
        }
        else{
          this.displayInvent[index].detail.push(itemdetail);
          this.displayInvent[index].show=true;
        }
    }

    this.edititemdetail = false;
    this.updaterf=false;
    this.reset();

    // this.page = 1;
    // this.isLoading = true;
    // this.loadPage();
    // this.isLoading = false;

  }

  changepage(searchkey) {
    if (searchkey.trim() == '') {
      this.loadPage();
    }
    else {
      this.searchInven(searchkey);
    }
  }



  searchInven(key: string) {
    if (key == undefined || key == '') {
      this.searchKeyword = '';
      this.inventService.getCount().subscribe(c => {
        this.invenLength = c;
        this.page = 1;
        this.isLoading = true;
        this.loadPage();
        this.isLoading = false;
      })
    }
    else{
      this.searchKeyword = key;
      //this.inventService.searchInventory(this.searchKeyword, this.page - 1, this.searchkey).subscribe(iwd => {
      this.inventService.searchInventory(key, this.page - 1, this.searchkey).subscribe(iwd => {
        this.inventories = [];
        iwd.forEach(pair => {
          pair.item.amount = 0;
          pair.item.inuse = 0;
          pair.details.forEach(d => {
            if (!isNaN(Number(d.amount))) {
              pair.item.amount += Number(d.amount);
            }
            if (!isNaN(Number(d.inuse))) {
              pair.item.inuse += Number(d.inuse);
            }
          })
          this.inventories.push({ master: pair.item, show: false, detail: pair.details });
        })
        this.displayInvent = this.inventories.slice();
        
        //this.inventService.getSearchCount(this.searchKeyword, this.searchkey).subscribe(c => {
        this.inventService.getSearchCount(key, this.searchkey).subscribe(c => {
          this.invenLength = c;
        })
      });
    }
  }

  newItem() {
    this.item = new item(-1, '', '', '', '', '', true, '', '', '', '', '', '', '', '', '', new Date(), this.currentuser.name,'');
    this.enabledEditItem = true;
    this.isNewItem = true;
    this.isShowItem = true;
  }

  openconfirmation(message1, message2) {
    if (message1 == 'delete') {
      this.toconfirm = 'delete';
      this.whattodelete = message2;
      this.confirmationmessage = 'Are you sure you want to delete it?';
      this.modalService.open(this.confirmation, { backdrop: "static" }).result.then(result => { });
    }
  }

  action() {
    if (this.toconfirm == 'delete' && this.whattodelete == 'item') {
      this.deleteItem();

      //this.sopModal.close() ;
    }
    else if (this.toconfirm == 'delete' && this.whattodelete == 'displayitemdetail') {
      this.deleteItemdetail();
      //this.loadInvent();
    }
  }

  deleteItem() {
    let itemdbid = this.item.dbid;
    let itemname= this.item.name;
    let itemmanufacturercat=this.item.cat;
    let deleteitem = item.newItem(this.item);
    this.inventService.deleteItem(deleteitem).then(_=>{
          let itemarchive= new Itemarchive(-1, 'Delete', new Date(), this.currentuser.name, deleteitem.dbid,deleteitem.category,
          deleteitem.cat, deleteitem.suppliercat,  deleteitem.name, deleteitem.type, deleteitem.active,
          deleteitem.clientspecific, deleteitem.supplier,  deleteitem.manufacturer, deleteitem.unit, deleteitem.unitprice,
          deleteitem.unitsize, deleteitem.quantitythreshold,  deleteitem.supplylink, deleteitem.manufacturerlink, deleteitem.lastmodify, deleteitem.modifyperson,deleteitem.comment);

          this.archiveservice.addItemarchive(itemarchive);
          this.item = undefined;
          this.isShowItem = false;
          let index = this.displayInvent.findIndex(x=>x.master.dbid==itemdbid);
          if(index!=-1){
            this.displayInvent.splice(index,1);
          }
          this.itemdetailservice.loadItemDetailByItemDbid(itemdbid).subscribe(itemdetails=>{
            itemdetails.forEach(itemdetail=>{
              this.itemdetailservice.deleteItemDetail(itemdetail);
              // itemdetail.modifydate=lastmodify;
              // itemdetail.modifyperson=this.currentuser.name;
              // this.itemdetailservice.updateSingleItemDetail(itemdetail).then(()=>{});
            });
          });
  
          let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Inventory', 'Delete Item', 'Item dbid: '+itemdbid+', Item name: '+itemname+', Item manufacturer cat#: '+itemmanufacturercat)
          this.userhistoryservice.addUserHistory(userhistory);
        })


  }

  deleteItemdetail() {
    let itemdetaildbid=this.displayitemdetail.dbid;
    let itemdbid=this.displayitemdetail.itemdbid;
    let itemdetaillotnumber= this.displayitemdetail.lotnumber;
    this.itemdetailservice.deleteItemDetail(this.displayitemdetail).then(() => {
      let itemindex = this.displayInvent.findIndex(x => x.master.dbid == this.displayitemdetail.itemdbid);
      if (itemindex != -1) {
        let index = this.displayInvent[itemindex].detail.findIndex(x => x.dbid == this.displayitemdetail.dbid);
        if(index!=-1){
          this.displayInvent[itemindex].detail.splice(index, 1);
        }
        
      }

      this.displayitemdetail = undefined;
      this.isShowItemDetail = false;
      this.selectItems=[];

      this.inventService.getItemWithId(itemdbid).subscribe(item=>{
        let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Inventory', 'Delete Item Lot', 'Item name: '+item.name+', Item manufacturer cat#: '+item.cat+'\n'+'Itemdetail dbid: '+itemdetaildbid+', Itemdetail lot#: '+itemdetaillotnumber)
        this.userhistoryservice.addUserHistory(userhistory);
      })
    });
  }

  toggleViewCol(index: number) {
    this.viewColumn[index] = !this.viewColumn[index];
    this.subcolspan = 2;
    this.viewColumn.forEach(v => {
      if (v) this.subcolspan++;
    })
  }

  checkItemSelect(i: itemdetail): boolean {
    if (i == undefined) return false;

    if (this.selectItems == undefined || this.selectItems.length < 1) return false;

    if (this.selectItems.find(x => x.detailId == i.dbid) == undefined) return false;

    return true;
  }

  toggleSelectItem(item: item, itemdetail: itemdetail) {    
    if (item == undefined || item.dbid == undefined || item.dbid < 0) return;
    if (itemdetail == undefined || itemdetail.dbid == undefined || itemdetail.dbid < 0) return;
    if (this.selectItems == undefined || this.selectItems.length < 1) {
      this.selectItems = [];
    }
    let index = this.selectItems.findIndex(x => x.detailId == itemdetail.dbid)
    if (index == -1) {
      this.selectItems.push(
        new toggleItem(item.dbid, itemdetail.dbid,
          item.cat, item.name, itemdetail.lotnumber, itemdetail.unitsize, itemdetail.unit,
          itemdetail.amount, '', itemdetail.inuse, '', 
          'No', itemdetail.reserve));
    }
    else {
      this.selectItems.splice(index, 1);
    }
  }


  updateItemAmount(o: {
    selectItems: toggleItem[],
    recon: { m: item, d: itemdetail, o: string, e: { date: { year: number, month: number, day: number } } }[]}) {
    
    if (o !== undefined && o.selectItems == undefined || o.selectItems.length < 1) return;

    o.selectItems.forEach((selItem, index) => {
      this.itemdetailservice.getItemdetailWithId(selItem.detailId).subscribe(itemdetail => {
        itemdetail.amount=selItem.itemQuan;
        itemdetail.inuse=selItem.inuseQuan;
          this.itemdetailservice.updateSingleItemDetail(itemdetail).then(_ => { 
            this.inventService.getItemWithId(itemdetail.itemdbid).subscribe(item=>{
              let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Inventory', 'Check-Out Item Lot', 'Item name: '+item.name+', Item manufacturer cat#: '+item.cat+'\n'+'Itemdetail dbid: '+itemdetail.dbid+', Itemdetail lot#: '+itemdetail.lotnumber)
              this.userhistoryservice.addUserHistory(userhistory);
            })

            let index = this.displayInvent.findIndex(x=>x.master.dbid==selItem.masterId);
            if(index!=-1){
              let dindex = this.displayInvent[index].detail.findIndex(x=>x.dbid==selItem.detailId)
              if(dindex!=-1){
                this.displayInvent[index].detail[dindex]=itemdetail;
                this.displayInvent[index].show=true;
              }
            }
          });
      });
    });

    if (o.recon !== undefined && o.recon.length > 0) {
      this.addReconstitute(o.recon);
    }

    this.selectItems = [];
  }

  addReconstitute(reconstitute: { m: item, d: itemdetail, o: string, e: { date: { year: number, month: number, day: number } } }[]) {
    if (reconstitute !== undefined && reconstitute.length > 0) {
      this.page = 1;
      reconstitute.forEach(recon => {
        let newitem = recon.m;
        // newitem.dbid = -1;
        let newdetail = recon.d;
        // newdetail.dbid = -1;

        //if (this.inventories.find(x => x.master.cat == newitem.cat && x.master.name == newitem.name) == undefined) {
        // this.inventService.addItem(newitem).then(i => {
        //let id = i.json().dbid;
        //newdetail.itemdbid = id;
        this.itemdetailservice.addItemDetail(newdetail).then(_ => {
          this.inventService.getItemWithId(newdetail.itemdbid).subscribe(item=>{
            let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Inventory', 'Reconstitute Item Lot', 'Item name: '+item.name+', Item manufacturer cat#: '+item.cat+'\n'+'Itemdetail dbid: '+newdetail.dbid+', Itemdetail lot#: '+newdetail.lotnumber)
            this.userhistoryservice.addUserHistory(userhistory);
          })
          recon.d.dbid= _['dbid'];
          let index = this.displayInvent.findIndex(x=>x.master.dbid==recon.m.dbid);
          if(index!=-1){
              this.displayInvent[index].detail.push(recon.d);
              this.displayInvent[index].show=true;
          }

          let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Inventory', 'Reconstitute Item Lot', 'Item name: '+recon.m.name+', Item manufacturer cat#: '+recon.m.cat+'\n'+'Itemdetail lot#: '+newdetail.lotnumber);
          this.userhistoryservice.addUserHistory(userhistory);
          // else{
          //   this.loadPage();
          // }
      
      
          // this.page = 1;
          // this.isLoading = true;
          // this.loadPage();
          // this.isLoading = false;
          //this.loadPage();
          // newdetail.dbid = d.json().dbid;
          // this.inventories.push({ master: newitem, show: false, detail: [newdetail] })
        });
        // });
        //}
      });
    }
  }

  //this method is not used
  validitemdetail(idetail: itemdetail) {
    if (idetail == undefined) return false;

    let today = new Date();
    if (idetail.receivedate == undefined || idetail.expiredate == undefined) return false;

    if (new Date(idetail.expiredate) == today) return false;

    if (new Date(idetail.receivedate) >= new Date(idetail.expiredate)) return false;

    if (idetail.lotnumber == undefined || idetail.lotnumber.trim() == '') return false;

    if (idetail.location == undefined || idetail.location.trim() == '') return false;

    return true;
  }

  sendsinglerequest(item) {
    let singleorderarray = [];
    let neworderitem = new orderitem(-1, '', '', '', '', '', '', '', '', '', '', '', ' ', '', '', '', '', '', '', new Date(), null, null, null, null, null,false, '', '','No');
    neworderitem.cat = item.cat;
    neworderitem.name = item.name;
    neworderitem.unit = item.unit;
    neworderitem.supplier = item.vendor;
    singleorderarray.push(neworderitem);
    this.sendorder.emit(['Order Item', singleorderarray]);
  }

  getfile(event) {
    if (event.target.files && event.target.files[0]) {
      this.importfile = event.target.files[0];
    }
  }

  showitemdetail(itemdetail: itemdetail) {
    this.inventService.getItemWithId(itemdetail.itemdbid).subscribe(item=>{
      this.isShowItem = false;
      this.item = undefined;
      this.enabledEditItem = false;
      this.isShowItemDetail = true;
      this.displayitemdetail = itemdetail;
      this.itemcategory=item.category;
      this.conjugateitemtype=item.type;
    })

  }

  rfconapprove(event){
    if(event=='rf'){
      this.displayitemdetail.rfstatus='Approved';
      this.isShowItemDetail=false;
    }

    if(event=='con'){
      this.displayitemdetail.constatus='Approved';
      this.isShowItemDetail=false;
    }
  }

  showItemLog(showitem: item){
    this.archiveservice.getItemarchiveByItemdbid(showitem.dbid).subscribe(archives=>{
      let originalItemArchive:Itemarchive[]=[];
      this.inventService.getItemWithId(showitem.dbid).subscribe(item=>{

        let lastedItemarchive=new Itemarchive(null,null,null,null, item.dbid,item.category,item.cat,item.suppliercat,item.name,item.type,item.active,item.clientspecific,
            item.supplier,item.manufacturer,item.unit,item.unitprice,item.unitsize,item.quantitythreshold,item.supplylink,item.manufacturerlink,
            item.lastmodify,item.modifyperson,item.comment);
            originalItemArchive.push(lastedItemarchive);

        let i;
        for(i=archives.length-1;i>=0;i--){
          originalItemArchive.push(archives[i]);
        }

        let itemAndArchives=originalItemArchive.slice()

        
        while(itemAndArchives.length>0){
          if(itemAndArchives.length>1){
            

            if(itemAndArchives[1].action!='Create'){
              let allItemChanges:compareChange[]=[];
              if(itemAndArchives[0].category!=itemAndArchives[1].category){allItemChanges.push({field:'Category',oldValue: itemAndArchives[1].category, newValue:itemAndArchives[0].category});}
              if(itemAndArchives[0].cat!=itemAndArchives[1].cat){allItemChanges.push({field:'Manufacturer Cat#',oldValue: itemAndArchives[1].cat, newValue:itemAndArchives[0].cat});}
              if(itemAndArchives[0].suppliercat!=itemAndArchives[1].suppliercat){allItemChanges.push({field:'Supplier Cat#',oldValue: itemAndArchives[1].suppliercat, newValue:itemAndArchives[0].suppliercat});}
              if(itemAndArchives[0].name!=itemAndArchives[1].name){allItemChanges.push({field:'Name',oldValue: itemAndArchives[1].name, newValue:itemAndArchives[0].name});}
              if(itemAndArchives[0].type!=itemAndArchives[1].type){allItemChanges.push({field:'Type',oldValue: itemAndArchives[1].type, newValue:itemAndArchives[0].type});}
              
              if(itemAndArchives[0].active!=itemAndArchives[1].active){allItemChanges.push({field:'Active',oldValue: itemAndArchives[1].active.toString(), newValue:itemAndArchives[0].active.toString()});}
              
              if(itemAndArchives[0].clientspecific!=itemAndArchives[1].clientspecific){allItemChanges.push({field:'Clientspecific',oldValue: itemAndArchives[1].clientspecific, newValue:itemAndArchives[0].clientspecific});}
              if(itemAndArchives[0].supplier!=itemAndArchives[1].supplier){allItemChanges.push({field:'Supplier',oldValue: itemAndArchives[1].supplier, newValue:itemAndArchives[0].supplier});}
              if(itemAndArchives[0].manufacturer!=itemAndArchives[1].manufacturer){allItemChanges.push({field:'Manufacturer',oldValue: itemAndArchives[1].manufacturer, newValue:itemAndArchives[0].manufacturer});}
              if(itemAndArchives[0].unit!=itemAndArchives[1].unit){allItemChanges.push({field:'Unit',oldValue: itemAndArchives[1].unit, newValue:itemAndArchives[0].unit});}
              if(itemAndArchives[0].unitprice!=itemAndArchives[1].unitprice){allItemChanges.push({field:'Unit Price',oldValue: itemAndArchives[1].unitprice, newValue:itemAndArchives[0].unitprice});}
              if(itemAndArchives[0].unitsize!=itemAndArchives[1].unitsize){allItemChanges.push({field:'Unit Size',oldValue: itemAndArchives[1].unitsize, newValue:itemAndArchives[0].unitsize});}
              if(itemAndArchives[0].quantitythreshold!=itemAndArchives[1].quantitythreshold){allItemChanges.push({field:'Quantitythreshold',oldValue: itemAndArchives[1].quantitythreshold, newValue:itemAndArchives[0].quantitythreshold});}
              if(itemAndArchives[0].supplylink!=itemAndArchives[1].supplylink){allItemChanges.push({field:'Supplylink',oldValue: itemAndArchives[1].supplylink, newValue:itemAndArchives[0].supplylink});}
              if(itemAndArchives[0].manufacturerlink!=itemAndArchives[1].manufacturerlink){allItemChanges.push({field:'Manufacturerlink',oldValue: itemAndArchives[1].manufacturerlink, newValue:itemAndArchives[0].manufacturerlink});}
              if(itemAndArchives[0].comment!=itemAndArchives[1].comment){allItemChanges.push({field:'Comment',oldValue: itemAndArchives[1].comment, newValue:itemAndArchives[0].comment});}
  
              this.itemLogObject.push({action:itemAndArchives[1].action, changes:allItemChanges, time:itemAndArchives[1].createtime, user:itemAndArchives[1].createperson}); 
              itemAndArchives.shift();
            }
            else{
              itemAndArchives.shift();
            }
          }
          else{
            if(itemAndArchives[0].action=='Create'){
              let allItemChanges:compareChange[]=[];
              if(itemAndArchives[0].category){allItemChanges.push({field:'Category',oldValue: '', newValue:itemAndArchives[0].category});}
              if(itemAndArchives[0].cat){allItemChanges.push({field:'Manufacturer Cat#',oldValue: '', newValue:itemAndArchives[0].cat});}
              if(itemAndArchives[0].suppliercat){allItemChanges.push({field:'Supplier Cat#',oldValue: '', newValue:itemAndArchives[0].suppliercat});}
              if(itemAndArchives[0].name){allItemChanges.push({field:'Name',oldValue: '', newValue:itemAndArchives[0].name});}
              if(itemAndArchives[0].type){allItemChanges.push({field:'Type',oldValue: '', newValue:itemAndArchives[0].type});}
              
              if(itemAndArchives[0].active){
                allItemChanges.push({field:'Active',oldValue: '', newValue:itemAndArchives[0].active.toString()});
              }
              
              if(itemAndArchives[0].clientspecific){allItemChanges.push({field:'Clientspecific',oldValue: '', newValue:itemAndArchives[0].clientspecific});}
              if(itemAndArchives[0].supplier){allItemChanges.push({field:'Supplier',oldValue: '', newValue:itemAndArchives[0].supplier});}
              if(itemAndArchives[0].manufacturer){allItemChanges.push({field:'Manufacturer',oldValue: '', newValue:itemAndArchives[0].manufacturer});}
              if(itemAndArchives[0].unit){allItemChanges.push({field:'Unit',oldValue: '', newValue:itemAndArchives[0].unit});}
              if(itemAndArchives[0].unitprice){allItemChanges.push({field:'Unit Price',oldValue: '', newValue:itemAndArchives[0].unitprice});}
              if(itemAndArchives[0].unitsize){allItemChanges.push({field:'Unit Size',oldValue: '', newValue:itemAndArchives[0].unitsize});}
              if(itemAndArchives[0].quantitythreshold){allItemChanges.push({field:'Quantitythreshold',oldValue: '', newValue:itemAndArchives[0].quantitythreshold});}
              if(itemAndArchives[0].supplylink){allItemChanges.push({field:'Supplylink',oldValue: '', newValue:itemAndArchives[0].supplylink});}
              if(itemAndArchives[0].manufacturerlink){allItemChanges.push({field:'Manufacturerlink',oldValue: '', newValue:itemAndArchives[0].manufacturerlink});}
              if(itemAndArchives[0].comment){allItemChanges.push({field:'Comment',oldValue: '', newValue:itemAndArchives[0].comment});}
              this.itemLogObject.push({action:itemAndArchives[0].action, changes:allItemChanges, time:itemAndArchives[0].createtime, user:itemAndArchives[0].createperson}); 
              itemAndArchives=[];
            }
            else{
              itemAndArchives=[];
            }





          }





        }

        this.isShowItem=false;
        this.isShowItemLog=true;
      })
    });
    
  }

  showItemdetailLog(showitemdetail: itemdetail){
    this.archiveservice.getItemarchiveByItemdbid(showitemdetail.dbid).subscribe(archives=>{
      let originalItemArchive:Itemarchive[]=[];
      this.inventService.getItemWithId(showitemdetail.dbid).subscribe(item=>{

        let lastedItemarchive=new Itemarchive(null,null,null,null, item.dbid,item.category,item.cat,item.suppliercat,item.name,item.type,item.active,item.clientspecific,
            item.supplier,item.manufacturer,item.unit,item.unitprice,item.unitsize,item.quantitythreshold,item.supplylink,item.manufacturerlink,
            item.lastmodify,item.modifyperson,item.comment);
            originalItemArchive.push(lastedItemarchive);

        let i;
        for(i=archives.length-1;i>=0;i--){
          originalItemArchive.push(archives[i]);
        }

        let itemAndArchives=originalItemArchive.slice()

        
        while(itemAndArchives.length>0){
          if(itemAndArchives.length>1){
            if(itemAndArchives[1].action!='Create'){
              let allItemChanges:compareChange[]=[];
              if(itemAndArchives[0].cat!=itemAndArchives[1].cat){allItemChanges.push({field:'Manufacturer Cat#',oldValue: itemAndArchives[1].cat, newValue:itemAndArchives[0].cat});}
              if(itemAndArchives[0].suppliercat!=itemAndArchives[1].suppliercat){allItemChanges.push({field:'Supplier Cat#',oldValue: itemAndArchives[1].suppliercat, newValue:itemAndArchives[0].suppliercat});}
              if(itemAndArchives[0].name!=itemAndArchives[1].name){allItemChanges.push({field:'Name',oldValue: itemAndArchives[1].name, newValue:itemAndArchives[0].name});}
              if(itemAndArchives[0].type!=itemAndArchives[1].type){allItemChanges.push({field:'Type',oldValue: itemAndArchives[1].type, newValue:itemAndArchives[0].type});}
              if(itemAndArchives[0].active!=itemAndArchives[1].active){allItemChanges.push({field:'Active',oldValue: itemAndArchives[1].active.toString(), newValue:itemAndArchives[0].active.toString()});}
              if(itemAndArchives[0].clientspecific!=itemAndArchives[1].clientspecific){allItemChanges.push({field:'Clientspecific',oldValue: itemAndArchives[1].clientspecific, newValue:itemAndArchives[0].clientspecific});}
              if(itemAndArchives[0].supplier!=itemAndArchives[1].supplier){allItemChanges.push({field:'Supplier',oldValue: itemAndArchives[1].supplier, newValue:itemAndArchives[0].supplier});}
              if(itemAndArchives[0].manufacturer!=itemAndArchives[1].manufacturer){allItemChanges.push({field:'Manufacturer',oldValue: itemAndArchives[1].manufacturer, newValue:itemAndArchives[0].manufacturer});}
              if(itemAndArchives[0].unit!=itemAndArchives[1].unit){allItemChanges.push({field:'Unit',oldValue: itemAndArchives[1].unit, newValue:itemAndArchives[0].unit});}
              if(itemAndArchives[0].unitprice!=itemAndArchives[1].unitprice){allItemChanges.push({field:'Unit Price',oldValue: itemAndArchives[1].unitprice, newValue:itemAndArchives[0].unitprice});}
              if(itemAndArchives[0].unitsize!=itemAndArchives[1].unitsize){allItemChanges.push({field:'Unit Size',oldValue: itemAndArchives[1].unitsize, newValue:itemAndArchives[0].unitsize});}
              if(itemAndArchives[0].quantitythreshold!=itemAndArchives[1].quantitythreshold){allItemChanges.push({field:'Quantitythreshold',oldValue: itemAndArchives[1].quantitythreshold, newValue:itemAndArchives[0].quantitythreshold});}
              if(itemAndArchives[0].supplylink!=itemAndArchives[1].supplylink){allItemChanges.push({field:'Supplylink',oldValue: itemAndArchives[1].supplylink, newValue:itemAndArchives[0].supplylink});}
              if(itemAndArchives[0].manufacturerlink!=itemAndArchives[1].manufacturerlink){allItemChanges.push({field:'Manufacturerlink',oldValue: itemAndArchives[1].manufacturerlink, newValue:itemAndArchives[0].manufacturerlink});}
              if(itemAndArchives[0].comment!=itemAndArchives[1].comment){allItemChanges.push({field:'Comment',oldValue: itemAndArchives[1].comment, newValue:itemAndArchives[0].comment});}
  
              this.itemLogObject.push({action:itemAndArchives[1].action, changes:allItemChanges, time:itemAndArchives[1].createtime, user:itemAndArchives[1].createperson}); 
              itemAndArchives.shift();
            }
            else{
              itemAndArchives.shift();
            }
          }
          else{
            if(itemAndArchives[0].action=='Create'){
              let allItemChanges:compareChange[]=[];
              if(itemAndArchives[0].cat){allItemChanges.push({field:'Manufacturer Cat#',oldValue: '', newValue:itemAndArchives[0].cat});}
              if(itemAndArchives[0].suppliercat){allItemChanges.push({field:'Supplier Cat#',oldValue: '', newValue:itemAndArchives[0].suppliercat});}
              if(itemAndArchives[0].name){allItemChanges.push({field:'Name',oldValue: '', newValue:itemAndArchives[0].name});}
              if(itemAndArchives[0].type){allItemChanges.push({field:'Type',oldValue: '', newValue:itemAndArchives[0].type});}
              
              if(itemAndArchives[0].active){
                allItemChanges.push({field:'Active',oldValue: '', newValue:itemAndArchives[0].active.toString()});
              }
              
              if(itemAndArchives[0].clientspecific){allItemChanges.push({field:'Clientspecific',oldValue: '', newValue:itemAndArchives[0].clientspecific});}
              if(itemAndArchives[0].supplier){allItemChanges.push({field:'Supplier',oldValue: '', newValue:itemAndArchives[0].supplier});}
              if(itemAndArchives[0].manufacturer){allItemChanges.push({field:'Manufacturer',oldValue: '', newValue:itemAndArchives[0].manufacturer});}
              if(itemAndArchives[0].unit){allItemChanges.push({field:'Unit',oldValue: '', newValue:itemAndArchives[0].unit});}
              if(itemAndArchives[0].unitprice){allItemChanges.push({field:'Unit Price',oldValue: '', newValue:itemAndArchives[0].unitprice});}
              if(itemAndArchives[0].unitsize){allItemChanges.push({field:'Unit Size',oldValue: '', newValue:itemAndArchives[0].unitsize});}
              if(itemAndArchives[0].quantitythreshold){allItemChanges.push({field:'Quantitythreshold',oldValue: '', newValue:itemAndArchives[0].quantitythreshold});}
              if(itemAndArchives[0].supplylink){allItemChanges.push({field:'Supplylink',oldValue: '', newValue:itemAndArchives[0].supplylink});}
              if(itemAndArchives[0].manufacturerlink){allItemChanges.push({field:'Manufacturerlink',oldValue: '', newValue:itemAndArchives[0].manufacturerlink});}
              if(itemAndArchives[0].comment){allItemChanges.push({field:'Comment',oldValue: '', newValue:itemAndArchives[0].comment});}
              this.itemLogObject.push({action:itemAndArchives[0].action, changes:allItemChanges, time:itemAndArchives[0].createtime, user:itemAndArchives[0].createperson}); 
              itemAndArchives=[];
            }
            else{
              itemAndArchives=[];
            }
          }
        }
        this.itemLogModal=this.modalService.open(this.ItemLog, { backdrop: "static", size: 'sm' });
      })
    });
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // this.archiveservice.getItemdetailarchiveByItemdetaildbid(showitemdetail.dbid).subscribe(archives=>{
    //   console.log('itemdetail archives are');
    //   console.log(archives)
    //   let itemdetailAndArchives:Itemdetailarchive[]=[];
    //   this.itemdetailservice.getItemdetailWithId(showitemdetail.dbid).subscribe(itemdetail=>{
    //     let lastedItemdetailarchive= new Itemdetailarchive(null,null,null,null,itemdetail.dbid,
    //       itemdetail.itemdbid,itemdetail.itemname,itemdetail.itemtype,itemdetail.supplier,itemdetail.parentlotnumber,itemdetail.lotnumber,
    //       itemdetail.receivedate,itemdetail.expiredate,itemdetail.location,itemdetail.sublocation,
    //       itemdetail.amount,itemdetail.inuse,itemdetail.concentration,itemdetail.concentrationunit,
    //       itemdetail.species,itemdetail.clonality,itemdetail.host,itemdetail.conjugate,
    //       itemdetail.iggdepletion,itemdetail.purification,itemdetail.volume,itemdetail.volumeunit,
    //       itemdetail.weight, itemdetail.weightunit,itemdetail.apcolumnprepdate, itemdetail.usagecolumn,
    //       itemdetail.columnnumber, itemdetail.comment,itemdetail.reserve, itemdetail.projectnumber,
    //       itemdetail.receiveperson, itemdetail.unit,itemdetail.unitsize,  itemdetail.recon,
    //       itemdetail.modifydate,  itemdetail.modifyperson);

    //       itemdetailAndArchives.push(lastedItemdetailarchive);
          
    //       let i;
    //       for(i=archives.length-1;i>=0;i--){
    //         itemdetailAndArchives.push(archives[i]);
    //       }
    //       console.log('all itemdetailAndArchives are');
    //       console.log(itemdetailAndArchives)

    //       this.itemdetailLogModal=this.modalService.open(this.ItemdetailLog, { backdrop: "static", size: 'sm' });
    //   })
    // })
    
  }

  cancelItemLog(){
    this.isShowItemLog=false
    this.itemLogObject=[];
  }

  cancelItemdetailLog(){
    this.itemdetailLogModal.close();
  }

  cancelrf(){
    this.updaterf=false;
  }

  cancelcon(){
    this.updatecon=false;

  }

  additemdetail(additem: item) {
    if (additem == undefined) return;
    this.itemdetailservice.searchItemdetailsByItemdbid(additem.dbid).subscribe(itemdetails => {
      if (itemdetails.length < 1) {
        this.displayitemdetail = new itemdetail(-1, additem.dbid, additem.category, additem.name, additem.type, additem.supplier, '', '', new Date(), new Date(), new Date(), '', '', '', '','',     '','','','',null,'','','','','','', '','','',         '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', new Date(), '', '', '', '', '', '', '', '1', 'No', new Date(), this.currentuser.name);
      }
      else {
        let lastest = itemdetails[itemdetails.length - 1];
        this.displayitemdetail = itemdetail.newitemdetail(lastest);
        this.displayitemdetail.dbid = -1;
        this.displayitemdetail.itemdbid = additem.dbid;
        this.displayitemdetail.itemname = additem.name;
        this.displayitemdetail.itemcategory = additem.category;
        this.displayitemdetail.itemtype = additem.type;
        this.displayitemdetail.supplier = additem.supplier;
        this.displayitemdetail.recon = 'No';
        this.displayitemdetail.reserve = 'No';
        this.displayitemdetail.projectnumber = '';
        this.displayitemdetail.receiveperson = '';
        this.displayitemdetail.inuse = '0';
        this.displayitemdetail.unitsize = '1';
        this.displayitemdetail.comment = '';
        this.displayitemdetail.rfstatus = '';
        this.displayitemdetail.rffile = '';
        this.displayitemdetail.confile = '';
        this.displayitemdetail.constatus = '';
        this.displayitemdetail.modifyperson=this.currentuser.name;
        this.displayitemdetail.modifydate=new Date();
        this.displayitemdetail.expiredate=new Date();
        this.displayitemdetail.retestdate=null;

      }
      this.isShowItemDetail = true;
      this.edititemdetail = true;
      this.isNewItemDetail = true;
    })
  }

  rftrue(){
    if(this.updaterf!=true){
      this.updaterf=true;
    }

  }

  contrue(){
    if(this.updatecon!=true){
      this.updatecon=true;
    }

  }

  conjugate(){
    this.hideconjugateformrow=[true,true,true];
    this.newConjugate=new newConjugate('','', '', '','','', '','','','','','','','','','','','','','','','','','','','','','','','', '','','', '', '','',new Date(),'');
    let r = this.newConjugate.prepdate;
    this.conjugatePrepdateDisplay = { date: { year: r.getFullYear(), month: r.getMonth() + 1, day: r.getDate() } };
    this.conjugateModal=this.modalService.open(this.openconjugate, { backdrop: "static", size: 'lg' })
  }

  searchMolecule(key){
    if (key == undefined) return;
    this.moleculeFilterList = [];
    if (key.trim() == '') {
      this.moleculeFilterList = [];
      this.newConjugate.biomolecule='';
    }
    else{
        this.allItems.forEach(item=>{
          if (item.name.toUpperCase().includes(key.toUpperCase())  && (item.type=='Antibody' || item.type=='Protein and Enzyme' ||  item.type=='Drug') ) {
            this.moleculeFilterList.push({ dbid:item.dbid, name: item.name,  cat: item.cat, supplier: item.supplier, unitsize:item.unitsize, unit:item.unit });
          }
        })
    }
  }

  pasteMolecule(event:any){
    let key=event.clipboardData.getData('text/plain');
    this.searchMolecule(key)
  }

  pasteConjugate(event:any){
    let key=event.clipboardData.getData('text/plain');
    this.searchConjugate(key)
  }

  searchConjugate(key){
    if (key == undefined) return;
    this.conjugateFilterList = [];
    if (key.trim() == '') {
    this.conjugateFilterList = [];
    }
    else{
        this.allItems.forEach(item=>{
          if (item.name.toUpperCase().includes(key.toUpperCase()) && (item.type=='Conjugate Molecule' || item.type=='Kit')  ) {
            this.conjugateFilterList.push({ dbid:item.dbid, name: item.name,  cat: item.cat, supplier: item.supplier, unitsize:item.unitsize, unit:item.unit  });
          }
        })

    }
  }


  selectMolecule(select: { dbid:number, name: string, cat: string,  supplier: string, unitsize:string, unit:string}){
    this.newConjugate.biomoleculename=select.name;
    this.newConjugate.biomolecule=select.name+', Cat#: '+select.cat+', Supplier: '+select.supplier;
    this.selectedMolecule.name=select.name;
    this.selectedMolecule.cat=select.cat;
    this.selectedMolecule.supplier=select.supplier;
    this.newConjugate.biomoleculeinfo='Name: '+select.name+'\n'+'Cat#: '+select.cat+'\n';
    
    this.itemdetailservice.loadItemDetailByItemDbid(select.dbid).subscribe(itemdetails=>{
      if(itemdetails.length>0){
        itemdetails.forEach((itemdetail, index)=>{
          if(itemdetail.amount=='0' && itemdetail.inuse=='0'){
            itemdetails.splice(index, 1);
          }
        })

        this.selectedMolecule.itemdetail=new itemdetail(-1, null, null, null, null, null, null, null,null,
          null, null, null, null, null, null,null, null,null,null,null,null,null,null,null,null, null, null, null, null, null,    
          null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        this.selectedMolecule.itemdetails=itemdetails.slice();

      }




      // let hasAmount=false;
      // let i;
      // for(i=0;i<itemdetails.length;i++){
      //   if(itemdetails[i].amount && itemdetails[i].amount.trim()!='' && itemdetails[i].amount.trim()!='0'){
      //     hasAmount=true;
          
      //     break;
      //   }
      // }

      // if(itemdetails.length!=0 && hasAmount==true){
      //   if(itemdetails[i].unitsize && itemdetails[i].unitsize.trim()!='' && itemdetails[i].unitsize.trim()!='0'){
      //     this.selectedMolecule.stockamount=  (Number(itemdetails[i].amount) * Number(itemdetails[i].unitsize) ).toString()
      //     this.selectedMolecule.stockunit=  itemdetails[i].unit;
      //   }
      //   else{
      //     this.selectedMolecule.stockamount=  itemdetails[i].amount
      //     this.selectedMolecule.stockunit=  itemdetails[i].unit
      //   }
      //   
      // }
      // else{
      //   this.selectedMolecule.stockamount= 'None in Stock';
      // }
      
    })
    this.showMoleculeList=false;

    if(this.newConjugate.biomoleculename.trim()!='' && this.newConjugate.conjugatename.trim()!=''){
      if(this.newConjugate.conjugate.toLowerCase().includes('biotin')){
        this.newConjugate.name='Biotinylated '+this.newConjugate.biomoleculename
      }
      if(this.newConjugate.conjugatename.toLowerCase().includes('digoxigenin')){
        this.newConjugate.name='Digoxigenated '+this.newConjugate.biomoleculename
      }
      if(this.newConjugate.conjugatename.toLowerCase().includes('hrp')){
        this.newConjugate.name=this.newConjugate.biomoleculename+' - HRP'
      }
      //this.newConjugate.name=this.newConjugate.conjugate+'_'+this.newConjugate.biomolecule
    }


  }

  getbiomodeluceitemlot(event){
    let index=event.target.selectedIndex-1;
    this.selectedMolecule.itemdetail=this.selectedMolecule.itemdetails[event.target.selectedIndex-1];
    this.newConjugate.biomoleculeinfo=this.newConjugate.biomoleculeinfo+'Lot#: '+this.selectedMolecule.itemdetail.lotnumber;

    let unitsize, amount, inuse, totalamount;
    if(!this.selectedMolecule.itemdetail.amount){
      amount=0;
    }
    else{
      amount=parseFloat(this.selectedMolecule.itemdetail.amount);
    }

    if(!this.selectedMolecule.itemdetail.inuse){
      inuse=0;
    }
    else{
      inuse=parseFloat(this.selectedMolecule.itemdetail.inuse);
    }

    if(!this.selectedMolecule.itemdetail.unitsize){
      unitsize=1;
    }
    else{
      unitsize=parseFloat(this.selectedMolecule.itemdetail.unitsize);
    }

    this.selectedMolecule.stockamount=(amount+inuse)*unitsize;

    this.newConjugate.biounit=this.selectedMolecule.itemdetail.unit;


  }

  getBioRequireAmount(amount:number){
    if(amount>Number(this.selectedMolecule.stockamount) ){
      this.newConjugate.biorequireamount=''
    }
  }
  pasteBioRequireAmount(event:any){
    let key=event.clipboardData.getData('text/plain');
    this.getBioRequireAmount(key)
  }

  getConRequireAmount(amount:number){
    if(amount>Number(this.selectedConjugate.stockamount) ){
      this.newConjugate.conrequireamount=''
    }
  }
  pasteConRequireAmount(event:any){
    let key=event.clipboardData.getData('text/plain');
    this.getConRequireAmount(key)
  }

  checkoutbioyes(){
    if(this.newConjugate.checkoutbiomolecule!='Yes'){
      this.newConjugate.checkoutbiomolecule='Yes';
    }
    else{
      this.newConjugate.checkoutbiomolecule='';
    }
  }

  checkoutbiono(){
    if(this.newConjugate.checkoutbiomolecule!='No'){
      this.newConjugate.checkoutbiomolecule='No';
    }
    else{
      this.newConjugate.checkoutbiomolecule='';
    }
  }

  checkoutconyes(){
    if(this.newConjugate.checkoutconjugate!='Yes'){
      this.newConjugate.checkoutconjugate='Yes';
    }
    else{
      this.newConjugate.checkoutconjugate='';
    }
  }

  checkoutconno(){
    if(this.newConjugate.checkoutconjugate!='No'){
      this.newConjugate.checkoutconjugate='No';
    }
    else{
      this.newConjugate.checkoutconjugate='';
    }
  }

  checkBioUnopened(amount:number){
    if(amount>Number(this.selectedMolecule.itemdetail.amount) ){
      this.newConjugate.biodepleteunopened=''
    }
  }

  checkBioInuse(amount:number){
    if(amount>Number(this.selectedMolecule.itemdetail.inuse) ){
      this.newConjugate.biodepleteinuse=''
    }
  }

  pasteBioUnopened(event:any){
    let key=event.clipboardData.getData('text/plain');
    this.checkBioUnopened(key)
  }

  pasteBioInuse(event:any){
    let key=event.clipboardData.getData('text/plain');
    this.checkBioInuse(key)
  }


  checkConUnopened(amount:number){
    if(amount>Number(this.selectedConjugate.itemdetail.amount) ){
      this.newConjugate.condepleteunopened=''
    }
  }

  checkConInuse(amount:number){
    if(amount>Number(this.selectedConjugate.itemdetail.inuse) ){
      this.newConjugate.condepleteinuse=''
    }
  }

  pasteConUnopened(event:any){
    let key=event.clipboardData.getData('text/plain');
    this.checkConUnopened(key)
  }

  pasteConInuse(event:any){
    let key=event.clipboardData.getData('text/plain');
    this.checkConInuse(key)
  }

  showsublocationoption(location:string){
    this.sublocations=[];
    let targetlocation=this.locations.find(x => x.name == location);
    if (targetlocation == undefined) return;

    
    this.allsublocations.forEach(sl=>{
      if(sl.id == targetlocation.id){
        this.sublocations.push(sl);
      }
    });
    this.newConjugate.sublocation=this.sublocations[0].name;
  }

  selectConjugate(select: { dbid:number, name: string, cat: string,  supplier: string, unitsize:string, unit:string}){
    this.newConjugate.conjugatename=select.name;
    this.newConjugate.conjugate=select.name+', Cat#: '+select.cat+', Supplier: '+select.supplier;
    this.selectedConjugate.name=select.name;
    this.selectedConjugate.cat=select.cat;
    this.selectedConjugate.supplier=select.supplier;
    this.newConjugate.conjugateinfo='Name: '+select.name+'\n'+'Cat#: '+select.cat+'\n';

    this.itemdetailservice.loadItemDetailByItemDbid(select.dbid).subscribe(itemdetails=>{
      if(itemdetails.length>0){
        itemdetails.forEach((itemdetail, index)=>{
          if(itemdetail.amount=='0' && itemdetail.inuse=='0'){
            itemdetails.splice(index, 1);
          }
        })
  
        this.selectedConjugate.itemdetail=new itemdetail(-1, null, null, null, null, null, null, null,null,
          null, null, null, null, null, null,null, null,null,null,null,null,null,null,null,null, null, null, null, null, null,    
          null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        this.selectedConjugate.itemdetails=itemdetails.slice();
      }



      // let hasAmount=false;
      // let i;
      // for(i=0;i<itemdetails.length;i++){
      //   if(itemdetails[i].amount && itemdetails[i].amount.trim()!='' && itemdetails[i].amount.trim()!='0'){
      //     hasAmount=true;
      //     this.selectedConjugate.itemdetail=itemdetails[i]
      //     break;
      //   }
      // }

      // if(itemdetails.length!=0 && hasAmount==true){
      //   if(itemdetails[i].unitsize && itemdetails[i].unitsize.trim()!='' && itemdetails[0].unitsize.trim()!='0'){
      //     this.selectedConjugate.stockamount=  (Number(itemdetails[i].amount) * Number(itemdetails[i].unitsize) ).toString()
      //     this.selectedConjugate.stockunit=  itemdetails[i].unit
      //   }
      //   else{
      //     this.selectedConjugate.stockamount=  itemdetails[i].amount
      //     this.selectedConjugate.stockunit=  itemdetails[i].unit
      //   }
      //   this.newConjugate.conjugateinfo='Name: '+select.name+'\n'+'Cat#: '+select.cat+'\n'+'Lot#: '+itemdetails[i].lotnumber;
      // }
      // else{
      //   this.selectedConjugate.stockamount= 'None in Stock';
      //   //this.selectedConjugate.stockamount= '0 '+select.unit;
      //   //this.selectedConjugate.stockunit= select.unit;
      //   //this.newConjugate.conjugateinfo='Name: '+select.name+'\n'+' Cat#: '+select.cat+'\n'+' Lot#: ';
      // }
      
    })
    this.showConjugateList=false;
    if(this.newConjugate.biomoleculename.trim()!='' && this.newConjugate.conjugatename.trim()!=''){
      if(this.newConjugate.conjugatename.toLowerCase().includes('biotin')){
        this.newConjugate.name='Biotinylated '+this.newConjugate.biomoleculename
      }
      if(this.newConjugate.conjugatename.toLowerCase().includes('digoxigenin')){
        this.newConjugate.name='Digoxigenated '+this.newConjugate.biomoleculename
      }
      if(this.newConjugate.conjugatename.toLowerCase().includes('hrp')){
        this.newConjugate.name=this.newConjugate.biomoleculename+' - HRP'
      }
      //this.newConjugate.name=this.newConjugate.conjugate+'_'+this.newConjugate.biomolecule;
    }
  }

  getconjugateitemlot(event){
    let index=event.target.selectedIndex-1;
    this.selectedConjugate.itemdetail=this.selectedConjugate.itemdetails[event.target.selectedIndex-1];
    this.newConjugate.conjugateinfo=this.newConjugate.conjugateinfo+'Lot#: '+this.selectedConjugate.itemdetail.lotnumber;
    let unitsize, amount, inuse, totalamount;
    if(!this.selectedConjugate.itemdetail.amount){
      amount=0;
    }
    else{
      amount=parseFloat(this.selectedConjugate.itemdetail.amount);
    }

    if(!this.selectedConjugate.itemdetail.inuse){
      inuse=0;
    }
    else{
      inuse=parseFloat(this.selectedConjugate.itemdetail.inuse);
    }

    if(!this.selectedConjugate.itemdetail.unitsize){
      unitsize=1;
    }
    else{
      unitsize=parseFloat(this.selectedConjugate.itemdetail.unitsize);
    }

    this.selectedConjugate.stockamount=(amount+inuse)*unitsize;
    this.newConjugate.conunit=this.selectedConjugate.itemdetail.unit;
  }

  changeprepdate(event) {
    let d = event.formatted;
    if (d == undefined) return;
    this.newConjugate.prepdate=new Date(d);
  }

  checkDescription(description: string) {
    if (description == undefined) description = '';
    if (description.length > 255) description = description.substr(0, 255);
  }

  checkConjugateInput(){
    if( this.newConjugate.biomolecule.trim()=='' ||
        this.newConjugate.conjugate.trim()==''  ||
        this.newConjugate.name.trim()=='' ||
        this.newConjugate.runnumber.trim()=='' ||
        this.newConjugate.lot.trim()=='' ||
        this.newConjugate.amount.trim()==''  ||
        this.newConjugate.unitsize.trim()==''  ||
        this.newConjugate.unit.trim()==''  ||
        this.newConjugate.concentration.trim()=='' ||
        this.newConjugate.concentrationunit.trim()==''  ||
        this.newConjugate.location.trim()=='' ||
        this.newConjugate.sublocation.trim()=='' ||
        this.newConjugate.chemistry.trim()=='' ||
        this.newConjugate.ratio.trim()=='' ||
        this.newConjugate.prepperson.trim()=='' ||
        this.newConjugate.bca.trim()==''||
        this.newConjugate.checkoutconjugate.trim()==''||
        this.newConjugate.checkoutbiomolecule.trim()==''||
        (this.newConjugate.checkoutconjugate.trim()=='Yes' && this.newConjugate.condepleteunopened.trim()=='' && this.newConjugate.condepleteinuse.trim()=='' &&  this.newConjugate.contransfer.trim()=='' ) ||
        (this.newConjugate.checkoutbiomolecule.trim()=='Yes' && this.newConjugate.biodepleteunopened.trim()=='' && this.newConjugate.biodepleteinuse.trim()=='' &&  this.newConjugate.biotransfer.trim()=='' )

      ){
        return;
    }
    else{
      this.saveConjugate();
    }
  }

  changeconjugatereserve() {
    if (this.newConjugate == undefined)return;
    if(this.newConjugate.reserve=='Yes'){
      this.newConjugate.reserve='No';
    }
    else{
      this.newConjugate.reserve='Yes'
    }
      
  }


  saveConjugate(){
    this.inventService.loadInventory().subscribe(items=>{
      let conjugateIndex=items.findIndex(x=>x.name==this.newConjugate.name);
      if(conjugateIndex!=-1){
        let item=items[conjugateIndex];
        let newitemdetail=new itemdetail(-1, item.dbid, item.category, item.name, item.type, item.supplier, '', this.newConjugate.lot,
        null, this.selectedMolecule.itemdetail.expiredate, null, '', '', '', '','',     this.newConjugate.chemistry,this.newConjugate.ratio,'',this.newConjugate.prepperson,this.newConjugate.prepdate,'','','','', '', this.newConjugate.bca,  this.newConjugate.runnumber,  this.newConjugate.biomoleculeinfo,  this.newConjugate.conjugateinfo,    
        this.newConjugate.location, this.newConjugate.sublocation, this.newConjugate.amount, '', this.newConjugate.concentration, this.newConjugate.concentrationunit, '', '', '', '', '', '', '', '', '', '', new Date(), '', '', this.newConjugate.comment, this.newConjugate.reserve, this.newConjugate.projectnumber, '', this.newConjugate.unit, this.newConjugate.unitsize, 'No', new Date(), this.currentuser.name);

        this.itemdetailservice.addItemDetail(newitemdetail).then(()=>{
          if(this.newConjugate.biodepleteunopened!=''){
            this.selectedMolecule.itemdetail.amount= (Number(this.selectedMolecule.itemdetail.amount)-Number(this.newConjugate.biodepleteunopened)).toString();}
          if(this.newConjugate.biodepleteinuse!=''){
            this.selectedMolecule.itemdetail.inuse= (Number(this.selectedMolecule.itemdetail.inuse)-Number(this.newConjugate.biodepleteinuse)).toString();}
          if(this.newConjugate.biotransfer!=''){
            this.selectedMolecule.itemdetail.amount= (Number(this.selectedMolecule.itemdetail.amount)-Number(this.newConjugate.biotransfer)).toString();
            this.selectedMolecule.itemdetail.inuse= (Number(this.selectedMolecule.itemdetail.inuse)+Number(this.newConjugate.biotransfer)).toString();
          }

          if(this.newConjugate.condepleteunopened!=''){
            this.selectedConjugate.itemdetail.amount= (Number(this.selectedConjugate.itemdetail.amount)-Number(this.newConjugate.condepleteunopened)).toString();}
          if(this.newConjugate.condepleteinuse!=''){
            this.selectedConjugate.itemdetail.inuse= (Number(this.selectedConjugate.itemdetail.inuse)-Number(this.newConjugate.condepleteinuse)).toString();}
          if(this.newConjugate.contransfer!=''){
            this.selectedConjugate.itemdetail.amount= (Number(this.selectedConjugate.itemdetail.amount)-Number(this.newConjugate.contransfer)).toString();
            this.selectedConjugate.itemdetail.inuse= (Number(this.selectedConjugate.itemdetail.inuse)+Number(this.newConjugate.contransfer)).toString();
          }

          this.itemdetailservice.updateSingleItemDetail(this.selectedMolecule.itemdetail);
          this.itemdetailservice.updateSingleItemDetail(this.selectedConjugate.itemdetail);
          this.page = 1;
          this.isLoading=true;
          this.loadPage();
          this.isLoading=false;
          this.cancelConjugate();
        });
      }
      else{
        let conjugateCats=[];
        items.forEach(item=>{
          if(item.cat.startsWith('SD')){
            conjugateCats.push(item.cat);
          }
        });

        let nextcat='SD456';
        if(conjugateCats.length!=0){
          let catnumberList=[];
          conjugateCats.forEach(cat=>{
            catnumberList.push(parseInt(cat.replace( /^\D+/g, ''),10))
          })
          catnumberList.sort();
          nextcat='SD'+(catnumberList[catnumberList.length-1]+1);
        }

        // this.settingservice.getSettingByPage('invent').subscribe(config => {
          //let conjugateCat = 'SD'+ConfigValue.toConfigArray(config.find(x => x.type == 'conjugatecat').value);
          //console.log(conjugateCat)
          let newItem=new item(-1, 'Critical Reagent', nextcat, '', this.newConjugate.name, 'Conjugate', true, 
          '', 'Somru BioScience Inc.', 'Somru BioScience Inc.', this.newConjugate.unit, this.newConjugate.unitsize, '', '', '', '', new Date(), this.currentuser.name,'');
          this.inventService.addItem(newItem).then(_=>{
            let itemdbid=_['dbid'];
            let newItemdetail=new itemdetail(-1, itemdbid, 'Critical Reagent', this.newConjugate.name, 'Conjugate', 'Somru BioScience Inc.', '', this.newConjugate.lot, 
            null, this.selectedMolecule.itemdetail.expiredate, null, '', '', '', '','',         this.newConjugate.chemistry,this.newConjugate.ratio,'',this.newConjugate.prepperson,this.newConjugate.prepdate,'','','','','',this.newConjugate.bca,  this.newConjugate.runnumber,   this.newConjugate.biomoleculeinfo,  this.newConjugate.conjugateinfo,    
            this.newConjugate.location, this.newConjugate.sublocation, this.newConjugate.amount, '', this.newConjugate.concentration, this.newConjugate.concentrationunit, '', '', '', '', '', '', '', '', '', '', new Date(), '', '', this.newConjugate.comment, this.newConjugate.reserve, this.newConjugate.projectnumber, '', this.newConjugate.unit,  this.newConjugate.unitsize, 'No', new Date(), this.currentuser.name);
            this.itemdetailservice.addItemDetail(newItemdetail).then(()=>{
              if(this.newConjugate.biodepleteunopened!=''){
                this.selectedMolecule.itemdetail.amount= (Number(this.selectedMolecule.itemdetail.amount)-Number(this.newConjugate.biodepleteunopened)).toString();}
              if(this.newConjugate.biodepleteinuse!=''){
                this.selectedMolecule.itemdetail.inuse= (Number(this.selectedMolecule.itemdetail.inuse)-Number(this.newConjugate.biodepleteinuse)).toString();}
              if(this.newConjugate.biotransfer!=''){
                this.selectedMolecule.itemdetail.amount= (Number(this.selectedMolecule.itemdetail.amount)-Number(this.newConjugate.biotransfer)).toString();
                this.selectedMolecule.itemdetail.inuse= (Number(this.selectedMolecule.itemdetail.inuse)+Number(this.newConjugate.biotransfer)).toString();
              }
    
              if(this.newConjugate.condepleteunopened!=''){
                this.selectedConjugate.itemdetail.amount= (Number(this.selectedConjugate.itemdetail.amount)-Number(this.newConjugate.condepleteunopened)).toString();}
              if(this.newConjugate.condepleteinuse!=''){
                this.selectedConjugate.itemdetail.inuse= (Number(this.selectedConjugate.itemdetail.inuse)-Number(this.newConjugate.condepleteinuse)).toString();}
              if(this.newConjugate.contransfer!=''){
                this.selectedConjugate.itemdetail.amount= (Number(this.selectedConjugate.itemdetail.amount)-Number(this.newConjugate.contransfer)).toString();
                this.selectedConjugate.itemdetail.inuse= (Number(this.selectedConjugate.itemdetail.inuse)+Number(this.newConjugate.contransfer)).toString();
              }
              this.itemdetailservice.updateSingleItemDetail(this.selectedMolecule.itemdetail);
              this.itemdetailservice.updateSingleItemDetail(this.selectedConjugate.itemdetail);
              //let nextCat= (Number(conjugateCat)+1).toString();
              //this.settingservice.updateSetting(new Setting(-1, 'invent', 'conjugatecat', nextCat, '',''));
              this.page = 1;
              this.isLoading=true;
              this.loadPage();
              this.isLoading=false;
              this.cancelConjugate();
            });
          })

        // });




        
      }
    })


  }

  cancelConjugate(){
    this.conjugateModal.close();
    this.selectedMolecule= new selectmolecule('','','',0,'','','',null, null);
    this.selectedConjugate= new selectmolecule('','','',0,'','','', null, null);
    this.newConjugate=new newConjugate('','', '','','','', '','','','','','','','','','','','','','','','','','','','','','','','', '','','', '', '','',new Date(),'');
    this.moleculeFilterList=[];
    this.conjugateFilterList=[];
    this.showMoleculeList=false;
    this.showConjugateList=false;
  }

  openCompareModel(){
    this.conjugateCompareModel=this.modalService.open(this.compare, { backdrop: "static", size: 'sm' })
  }

  closeCompareModel(){
    this.conjugateCompareModel.close();
    this.compareConjugateName='';
    this.compareConjugateList=[];
    this.showCompareConjugateList=false;
    this.compareConjugateItemLots=[];
    this.itemlotDynamicList=[];
    this.selectConjugateItemLots=[];
    this.showComparisonResult=false;
    this.showList=[true,true];
    this.conjugteCompareColspan=0;
    this.differentrows=[false, false, false, false, false, false, false, false,false, false, false,false, false]
  }


  searchCompareConjugate(key){
    if (key == undefined) return;
    this.compareConjugateList = [];
    if (key.trim() == '') {
    this.compareConjugateList = [];
    this.compareConjugateName = '';
    }
    else{
        this.allItems.forEach(item=>{
          if (item.name.toUpperCase().includes(key.toUpperCase()) && item.category=='Critical Reagent' && item.type=='Conjugate') {
            this.compareConjugateList.push({ dbid:item.dbid, name: item.name,  cat: item.cat, supplier: item.supplier });
          }
        })

    }
  }

  getName(event:any){
    let key=event.clipboardData.getData('text/plain');
    this.searchCompareConjugate(key)
  }




  selectCompareConjugate(select: { dbid:number, name: string, cat: string,  supplier: string}){
    this.compareConjugateName=select.name;
    this.compareConjugateList=[];
    this.showCompareConjugateList=false;
    this.selectConjugateItemLots=[];
    this.compareConjugateItemLots=[];
    this.showComparisonResult=false;
    this.itemlotDynamicList=[];
    this.showList=[true,true];
    this.conjugteCompareColspan=0;
    this.differentrows=[false, false, false, false, false, false, false, false,false, false, false,false, false]

    this.itemdetailservice.loadItemDetailByItemDbid(select.dbid).subscribe(itemdetails=>{

      this.compareConjugateItemLots=itemdetails.slice();
      itemdetails.forEach(itemlot=>{
        this.itemlotDynamicList.push(itemlot.lotnumber);
      })

      // let newitem1=new itemdetail(null,null,null,null,null,null,null,null,null,null,
      //   null,null,null,null,null,null,null,null,null,null,null,
      //   null,null,null,null,null,null,null,null,null,null,null,null,null,
      //   null,null,null,null,null,null,null,null,null,null,null,null,
      //   null,null,null,null,null,null,null,null,null,null,
      //   null,null)
      // let newitem2=new itemdetail(null,null,null,null,null,null,null,null,null,null,
      //   null,null,null,null,null,null,null,null,null,null,null,
      //   null,null,null,null,null,null,null,null,null,null,null,null,null,
      //   null,null,null,null,null,null,null,null,null,null,null,null,
      //   null,null,null,null,null,null,null,null,null,null,
      //   null,null)

      // this.selectConjugateItemLots.push({index:0, itemdetail:newitem1});
      // this.selectConjugateItemLots.push({index:1, itemdetail:newitem2});
      this.selectConjugateItemLots.push({index:0, itemdetail:null});
      this.selectConjugateItemLots.push({index:1, itemdetail:null});
    })
  }

  addlotcompare(){
    // let newitem=new itemdetail(null,null,null,null,null,null,null,null,null,null,
    //   null,null,null,null,null,null,null,null,null,null,null,
    //   null,null,null,null,null,null,null,null,null,null,null,null,null,
    //   null,null,null,null,null,null,null,null,null,null,null,null,
    //   null,null,null,null,null,null,null,null,null,null,
    //   null,null)
    //this.selectConjugateItemLots.push({index:this.selectConjugateItemLots.length, itemdetail:newitem});
    this.selectConjugateItemLots.push({index:this.selectConjugateItemLots.length, itemdetail:null});
  }

  setItemlot(i, lotnumber){
    let lotIndex=this.compareConjugateItemLots.findIndex(x=>x.lotnumber==lotnumber);
    if(lotIndex!=-1){
      this.selectConjugateItemLots[i].itemdetail=this.compareConjugateItemLots[lotIndex];
    }
    
    // let lotnumberIndex=this.itemlotDynamicList.findIndex(x=>x==lotnumber);
    // if(lotnumberIndex!=-1){
    //   this.itemlotDynamicList.splice(lotnumberIndex,1);
    // }
    
  }

  highlightrow(){
    let i;
    for(i=0;i<this.selectConjugateItemLots.length-1;i++){
      let j;

      
      for(j=i+1;j<this.selectConjugateItemLots.length;j++){
        if(this.selectConjugateItemLots[i].itemdetail.biotintobiomoleculeratio!=this.selectConjugateItemLots[j].itemdetail.biotintobiomoleculeratio){
          this.differentrows[0]=true;
          
        }
        if(this.selectConjugateItemLots[i].itemdetail.concentration!=this.selectConjugateItemLots[j].itemdetail.concentration){
          this.differentrows[1]=true;
          
        }
        if(this.selectConjugateItemLots[i].itemdetail.concentrationunit!=this.selectConjugateItemLots[j].itemdetail.concentrationunit){
          this.differentrows[1]=true;
          
        }
        if(this.selectConjugateItemLots[i].itemdetail.moleculeweight!=this.selectConjugateItemLots[j].itemdetail.moleculeweight){
          this.differentrows[2]=true;
          
        }
        if(this.selectConjugateItemLots[i].itemdetail.bindingactivity!=this.selectConjugateItemLots[j].itemdetail.bindingactivity){
          this.differentrows[3]=true;
          
        }
        if(this.selectConjugateItemLots[i].itemdetail.purity!=this.selectConjugateItemLots[j].itemdetail.purity){
          this.differentrows[4]=true;
          
        }
        if(this.selectConjugateItemLots[i].itemdetail.conjugateincorporationratio!=this.selectConjugateItemLots[j].itemdetail.conjugateincorporationratio){
          this.differentrows[5]=true;
          
        }



        if(this.selectConjugateItemLots[i].itemdetail.biomoleculeinfo!=this.selectConjugateItemLots[j].itemdetail.biomoleculeinfo){
          this.differentrows[6]=true;
         
        }
        if(this.selectConjugateItemLots[i].itemdetail.conjugateinfo!=this.selectConjugateItemLots[j].itemdetail.conjugateinfo){
          this.differentrows[7]=true;
          
        }
        if(this.selectConjugateItemLots[i].itemdetail.runnumber!=this.selectConjugateItemLots[j].itemdetail.runnumber){
          this.differentrows[8]=true;
        }

        if(this.selectConjugateItemLots[i].itemdetail.bca!=this.selectConjugateItemLots[j].itemdetail.bca){
          this.differentrows[9]=true;
         
        }
        if(this.selectConjugateItemLots[i].itemdetail.conjugatechemistry!=this.selectConjugateItemLots[j].itemdetail.conjugatechemistry){
          this.differentrows[10]=true;
  
        }

        let amount1= (Number(this.selectConjugateItemLots[i].itemdetail.amount) * Number(this.selectConjugateItemLots[i].itemdetail.unitsize)).toString()+' '+this.selectConjugateItemLots[i].itemdetail.unit;
        let amount2= (Number(this.selectConjugateItemLots[j].itemdetail.amount) * Number(this.selectConjugateItemLots[j].itemdetail.unitsize)).toString()+' '+this.selectConjugateItemLots[j].itemdetail.unit;


        if(amount1!=amount2){
          this.differentrows[11]=true;
         
        }
        if(this.selectConjugateItemLots[i].itemdetail.conjugateprepperson!=this.selectConjugateItemLots[j].itemdetail.conjugateprepperson){
          this.differentrows[12]=true;
        }

        let day1= new Date(this.selectConjugateItemLots[i].itemdetail.conjugateprepdate);
        let day2= new Date(this.selectConjugateItemLots[j].itemdetail.conjugateprepdate);

        if(day1.getFullYear()!=day2.getFullYear() || day1.getMonth()!=day2.getMonth() || day1.getDate()!=day2.getDate()){
          this.differentrows[13]=true;
        
        }

      }
    }

  }

  ShowComparison(){
    //this.columnWidth=100 * (1/(this.selectConjugateItemLots.length+1) )/100;
    let i;
    for(i=0;i<this.differentrows.length;i++){
      this.differentrows[i]=false;
    }
    // this.differentrows.forEach(row=>{
    //   row=false;
    // })
    this.highlightrow();
    this.conjugteCompareColspan=this.selectConjugateItemLots.length*2+2;
    this.showComparisonResult=true;
  }

  hidethisrow(index){
    this.showList[index]=false;
    
  }

  showthisrow(index){
    this.showList[index]=true;
  }

  import() {
    this.isLoading = true;
    var reader = new FileReader();
    var filedata;
    var workbook;
    var XL_row_object;
    var self = this;
    let allpromises = [];

    reader.onload = function (event) {
      var data = reader.result;
      workbook = XLSX.read(data, { type: 'binary' });
      let sheetnumber = workbook.SheetNames.length;
      workbook.SheetNames.forEach((sheetName, sindex) => {
        XL_row_object = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName]);
        let sheetname = sheetName;
        let index = 0;
        let condition = true;
        let i = 0;
        self.readeachlineoffile(i, XL_row_object, sheetname, sindex, sheetnumber);

      });

    }

    //reader.readAsArrayBuffer(this.importfile);
    reader.readAsBinaryString(this.importfile);
    //reader.readAsText(this.importfile);

    // self.page = 1;
    // self.loadPage();
    // self.isLoading = false;
  }

  readeachlineoffile(index: number, XL_row_object: any, sheetname: string, sindex: number, sheetnumber: number) {
    let rownumber = XL_row_object.length;
    if (index < rownumber) {
      let itemindex, newitemdetail;
      let excelitem = new item(-1, '', '', '', '', '', true, '', '', '', '', '', '', '', '', '', new Date(), this.currentuser.name,'');
      excelitem.type = sheetname;
      if (XL_row_object[index]['Catalog #']) { excelitem.cat = XL_row_object[index]['Catalog #'] };
      if (XL_row_object[index]['Item Name *']) { excelitem.name = XL_row_object[index]['Item Name *'] };
      if (XL_row_object[index]['Vendor']) { excelitem.supplier = XL_row_object[index]['Vendor'] }
      if (XL_row_object[index]['Price']) { excelitem.unitprice = XL_row_object[index]['Price'].replace(',', ''); }
      if (XL_row_object[index]['Unit Size']) {
        let unitsize = parseFloat(XL_row_object[index]['Unit Size']).toString();
        if (unitsize) {
          excelitem.unitsize = unitsize.toString();
        }
        let unit = XL_row_object[index]['Unit Size'].replace(/[0-9]/g, '');
        unit = unit.replace('.', '')
        //remove all special characters
        unit = unit.replace(/[^\w\s]/gi, '');
        unit = unit.replace(/ul/gi, 'µL');
        unit = unit.replace(/uL/gi, 'µL');
        unit = unit.replace(/ug/gi, 'µg');
        excelitem.unit = unit.trim();
      }
      if (XL_row_object[index]['URL']) {
        excelitem.supplylink = XL_row_object[index]['URL'];
        if (excelitem.supplylink.length > 255) {
          excelitem.supplylink = '';
        };
      }
      if (XL_row_object[index]['Amount in Stock'] !== undefined && XL_row_object[index]['Amount in Stock'].trim() != '' && XL_row_object[index]['Amount in Stock'].trim() != '0') {
        let inuse = '0';
        let amount = 0;
        if (XL_row_object[index]['Amount in Stock'].replace(' ', '').toUpperCase().trim() == 'INUSE') {
          inuse = '1';
        }
        else {
          amount = parseFloat(XL_row_object[index]['Amount in Stock'].replace(',', ''));
        }
        if (amount > 0) {
          newitemdetail = new itemdetail(-1, -1, excelitem.category, excelitem.name, excelitem.type, excelitem.supplier, '', '', null, null, null, '', '', '', '','',     '','','','',null,'','','','','','','','','',      '', '', amount.toString(), inuse, '', '', '', '', '', '', '', '', '', '', '', '', null, '', '', '', '', '', '', '', '', 'No', new Date(), this.currentuser.name);
          if (XL_row_object[index]['Location']) { newitemdetail.location = XL_row_object[index]['Location'] }
          if (XL_row_object[index]['Sub-location']) { newitemdetail.sublocation = XL_row_object[index]['Sub-location'] }
          if (XL_row_object[index]['Clonality']) { newitemdetail.clonality = XL_row_object[index]['Clonality'] }
          if (XL_row_object[index]['Lot Number']) { newitemdetail.lotnumber = XL_row_object[index]['Lot Number'] }
          if (XL_row_object[index]['Expiration Date']) { newitemdetail.expiredate = new Date(XL_row_object[index]['Expiration Date']).getTime() }
          if (XL_row_object[index]['Purified Date']) { newitemdetail.receivedate = new Date(XL_row_object[index]['Purified Date']).getTime() }
          if (XL_row_object[index]['Host']) { newitemdetail.host = XL_row_object[index]['Host']; newitemdetail.species = XL_row_object[index]['Host']; }
          if (XL_row_object[index]['IgG Depletion']) { newitemdetail.iggdepletion = XL_row_object[index]['IgG Depletion'] }
          if (XL_row_object[index]['Affinity Purification'] && XL_row_object[index]['Affinity Purification'] == 'Yes') { newitemdetail.purification = 'Affinity Purification' }
          if (XL_row_object[index]['Melon gel Purification'] && XL_row_object[index]['Melon gel Purification'] == 'Yes') { newitemdetail.purification = 'Melon gel Purification' }
          if (XL_row_object[index]['Concentration'] !== undefined && XL_row_object[index]['Concentration'].trim() != '') {
            newitemdetail.concentration = parseFloat(XL_row_object[index]['Concentration']).toString();
            let cunit = XL_row_object[index]['Concentration'].replace(/[0-9]/g, '');
            cunit = cunit.replace('.', '').trim();
            newitemdetail.concentrationunit = cunit.trim();
          }
          if (XL_row_object[index]['Volume']) {
            newitemdetail.volume = parseFloat(XL_row_object[index]['Volume']).toString();
            let vunit = XL_row_object[index]['Volume'].replace(/[0-9]/g, '');
            vunit = vunit.replace('.', '').trim();
            newitemdetail.volumeunit = vunit.trim();
          }
          if (XL_row_object[index]['Weight']) {
            newitemdetail.weight = parseFloat(XL_row_object[index]['Weight']).toString();
            let wunit = XL_row_object[index]['Weight'].replace(/[0-9]/g, '');
            wunit = wunit.replace('.', '').trim();
            newitemdetail.weightunit = wunit.trim();
          }
          if (XL_row_object[index]['AP Column Prep Date']) { newitemdetail.apcolumnprepdate = new Date(XL_row_object[index]['AP Column Prep Date']).getTime() }
          if (XL_row_object[index]['Usage of Column']) { newitemdetail.usagecolumn = XL_row_object[index]['Usage of Column'] }
          if (XL_row_object[index]['Comments']) { newitemdetail.comment = XL_row_object[index]['Comments'] }
        }
      }

      this.inventService.searchSingleInventoryByName(excelitem.name).subscribe(
        item => {
          excelitem.dbid = item.dbid;
          if (newitemdetail != undefined) {
            newitemdetail.itemdbid = item.dbid;
          }
          this.inventService.updateItem(excelitem).then(result => {
            let dindex;
            if (newitemdetail != undefined) {
              this.itemdetailservice.loadItemDetailByItemDbid(item.dbid).subscribe(itemdetaillist => {
                dindex = itemdetaillist.findIndex(x =>
                  x.itemdbid == newitemdetail.itemdbid &&
                  x.parentlotnumber == newitemdetail.parentlotnumber &&
                  x.lotnumber == newitemdetail.lotnumber &&
                  x.amount == newitemdetail.amount &&
                  x.itemtype == newitemdetail.itemtype &&
                  x.itemname == newitemdetail.itemname &&
                  x.supplier == newitemdetail.supplier &&
                  x.concentration == newitemdetail.concentration &&
                  x.concentrationunit == newitemdetail.concentrationunit &&
                  x.expiredate == newitemdetail.expiredate &&
                  x.receivedate == newitemdetail.receivedate &&
                  x.inuse == newitemdetail.inuse &&
                  x.location == newitemdetail.location &&
                  x.sublocation == newitemdetail.sublocation &&
                  x.species == newitemdetail.species &&
                  x.clonality == newitemdetail.clonality &&
                  x.host == newitemdetail.host &&
                  x.conjugate == newitemdetail.conjugate &&
                  x.iggdepletion == newitemdetail.iggdepletion &&
                  x.purification == newitemdetail.purification &&
                  x.volume == newitemdetail.volume &&
                  x.volumeunit == newitemdetail.volumeunit &&
                  x.weight == newitemdetail.weight &&
                  x.weightunit == newitemdetail.weightunit &&
                  x.apcolumnprepdate == newitemdetail.apcolumnprepdate &&
                  x.usagecolumn == newitemdetail.usagecolumn
                )
                //it is a new itemdetail
                if (dindex == -1) {
                  this.itemdetailservice.addItemDetail(newitemdetail).then(result => {
                    if (index < rownumber && sindex < sheetnumber) {
                      if (index == rownumber - 1 && sindex == sheetnumber - 1) {
                        this.page = 1;
                        this.loadPage();
                        return;
                      }
                      else {
                        this.readeachlineoffile(index + 1, XL_row_object, sheetname, sindex, sheetnumber);
                      }
                    }

                  })
                }//it is an existing itemdetail
                else {
                  // this.itemdetailservice.updateSingleItemDetail(newitemdetail).then(result=>{
                  //   console.log("update this itemdetail");
                  if (index < rownumber && sindex < sheetnumber) {
                    if (index == rownumber - 1 && sindex == sheetnumber - 1) {
                      this.page = 1;
                      this.loadPage();
                      return;
                    }
                    else {
                      this.readeachlineoffile(index + 1, XL_row_object, sheetname, sindex, sheetnumber);
                    }
                  }
                  // })
                }
              });
            }
            else {
              if (index < rownumber && sindex < sheetnumber) {
                if (index == rownumber - 1 && sindex == sheetnumber - 1) {
                  this.page = 1;
                  this.loadPage();

                  return;
                }
                else {
                  this.readeachlineoffile(index + 1, XL_row_object, sheetname, sindex, sheetnumber);
                }
              }
            }
          })

        },
        notfinditem => {
          let error = notfinditem.toString().trim();
          if (error.includes("SyntaxError: Unexpected end of JSON input")) {
            this.inventService.addItem(excelitem).then(_ => {
              if (newitemdetail != undefined) {
                newitemdetail.itemdbid = _['dbid'];
                this.itemdetailservice.addItemDetail(newitemdetail).then(result => {
                  if (index < rownumber && sindex < sheetnumber) {
                    if (index == rownumber - 1 && sindex == sheetnumber - 1) {
                      this.page = 1;
                      this.loadPage();
                      return;
                    }
                    else {
                      this.readeachlineoffile(index + 1, XL_row_object, sheetname, sindex, sheetnumber);
                    }
                  }
                })
              }
              else {
                if (index < rownumber && sindex < sheetnumber) {
                  if (index == rownumber - 1 && sindex == sheetnumber - 1) {
                    this.page = 1;
                    this.loadPage();
                    return;
                  }
                  else {
                    this.readeachlineoffile(index + 1, XL_row_object, sheetname, sindex, sheetnumber);
                  }
                }
              }
            });
          }
        }
      );
    }
  }




}


