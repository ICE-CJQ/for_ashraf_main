import { Component, OnInit } from '@angular/core';
import { formatDate } from 'ngx-bootstrap/chronos';
@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {
  startTimerArray;
  stopTimerArray;
  timeSpanArray;
  isClicked = false;

  constructor(param: number) {
    this.startTimerArray = new Array(param);
    this.stopTimerArray = new Array(param);
    this.timeSpanArray = new Array(param);
  }

  ngOnInit() {
  }

  startTimer(indexStart: number) {
    if (this.isClicked == false) {
      this.startTimerArray[indexStart] = new Date();
      window.alert("Timer starts at : " + formatDate(this.startTimerArray[indexStart], 'h:mm:ss a', 'en-CA', true));
      this.isClicked = true;
    }
    else {
      this.startTimerArray[indexStart] = new Date();
      window.alert("Timer starts at : " + formatDate(this.startTimerArray[indexStart], 'h:mm:ss a', 'en-CA', true));
      this.stopTimerArray[indexStart] = null;
    }

  }

  displayStartTime(index: number) {
    if (this.startTimerArray[index]) {
      return " (" + formatDate(this.startTimerArray[index], 'h:mm:ss a', 'en-CA', true) + ')';
    }
    return '';


  }

  stopTimer(indexStop: number) {
    this.stopTimerArray[indexStop] = new Date();
    this.timeSpanArray[indexStop] = this.stopTimerArray[indexStop] - this.startTimerArray[indexStop];
    window.alert("Timer stops at : " + formatDate(this.stopTimerArray[indexStop], 'h:mm:ss a', 'en-CA', true) +
      "\n\nTime spent on the current section is : " + this.displayTimeSpan(indexStop) + " minutes.");
  }

  displayStopTime(index: number) {
    if (this.stopTimerArray[index]) {
      return " (" + formatDate(this.stopTimerArray[index], 'h:mm:ss a', 'en-CA', true) + ')';
    }
    return '';

  }

  displayTimeSpan(index: number) {
    return (Math.round(this.timeSpanArray[index] * 10 / 1000 / 60) / 10).toFixed(1);
    //window.alert("Time span is: " + (Math.round(this.timeSpanArray[index] * 10 / 1000 / 60) / 10).toFixed(1));
  }

}