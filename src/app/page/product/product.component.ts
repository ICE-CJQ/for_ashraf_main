import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild, Output, EventEmitter } from '@angular/core';
import { Product, Application } from '../../shared/objects/Product';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProductService } from 'app/shared/services/product.service';
import { ApplicationService } from 'app/shared/services/application.service';
import * as XLSX from 'xlsx'//'ts-xlsx';
import { image } from '../../shared/objects/Image';
import { AuthenticationService } from '../../shared/services/Authenticate.Service';
import { User } from 'app/shared/objects/User';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { SettingService } from '../../shared/services/Setting.Service';
declare var jsPDF: any
import { UserhistoryService } from '../../shared/services/userhistory.service';
import { Userhistory } from '../../shared/objects/Userhistory';
//import * as jsPDF from 'jspdf';
//import 'jspdf-autotable';
//import jsPDF from 'jspdf';
//import 'jspdf-autotable';
// import * as jspdf from 'jspdf';  
// import * as html2canvas from "html2canvas";

@Component({
  selector: 'app-product',
  providers: [ProductService, ApplicationService, SettingService, UserhistoryService],
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  host: {
    '(document:click)': 'onClick($event)',
  }
})
export class ProductComponent implements OnInit {
  @Input() isShowProduct: boolean;
  @Input() displayProduct: Product;
  @Output() resetView = new EventEmitter();

  @ViewChild('productDisplay', {static: false}) productDisplay: ElementRef;
  @ViewChild('confirmation', {static: false}) confirmation: ElementRef;
  @ViewChild('productpdftable', {static: false}) productpdftable: ElementRef;
  displayProducts: Product[] = [];
  products: Product[] = [];
  allcount: number;
  isNewProduct: boolean;
  enableEditProduct: boolean;
  newProduct: Product;
  searchKey: string;
  isLoading: boolean;
  page: number;
  currentuser: User;

  //modalref: any;
  ngactivemodal: any;
  message: string;
  recipedirection: string;
  rightingredients = [];

  viewColumn: boolean[];
  columnList: string[];

  // searchArea:string;
  confirmationmessage: string = '';
  toconfirm: string = '';
  sopModal: any;
  warnModal: any;

  importfile;

  productsafety: ConfigValue[] = [];
  applicationnote: ConfigValue[] = [];

  productapplicationsforpdf: Application[] = [];

  constructor(private modalService: NgbModal, private productService: ProductService,
    private applicationService: ApplicationService,
    private authenticationservice: AuthenticationService, private setting: SettingService, private userhistoryservice: UserhistoryService) { }

  onClick(event) {
    if (!this.isNewProduct && event !== undefined && event.path !== undefined && event.path.find(x => x.className !== undefined && x.className.includes('recipename')) !== undefined) {
      let itempath = event.path.find(x => x.className.includes('recipename'));
      if (itempath == undefined || itempath.id == undefined || itempath.innerText == undefined) return;
      //let index = this.displayProducts.findIndex(x=>x.dbid == itempath.id && x.name == itempath.innerText.trim())
      let index = this.displayProducts.findIndex(x => x.dbid == itempath.id)
      this.showproduct(this.displayProducts[index])
      //this.showrecipe(this.displayRecipes.find(x=> x.name == itempath.innerText.trim()))
    }
    else if (event.path !== undefined && event.path.find(x => x.className !== undefined && x.className == 'slide') !== undefined)
      return;
    else if (event.path !== undefined && event.path.find(x => x.innerText !== undefined && x.innerText.trim() == 'New Product') !== undefined) {
      this.anewProduct()
      return;
    }
    else {
      if (!this.isNewProduct) {
        this.reset();
      }
    }
  }

  ngOnInit() {
    // this.reset();
    this.viewColumn = [true, true, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
    this.columnList = ['Cat#', 'Name', 'Type', 'Size', 'Price(USD)', 'Edit Status', 'Host', 'Clonality', 'Isotype', 'Immunogen', 'Description', 'Purification', 'Buffer', 'Old Cat', 'Specificity', 'Storage', 'Safety', 'Comment', 'Pack Size'];
    this.page = 1;
    this.searchKey = '';
    // this.searchArea = ''
    if(this.displayProduct != undefined){
      this.changeDisplayproduct();
    }
    else{
      this.isLoading = true;
      this.productService.getCount().subscribe(c => {
        this.allcount = c;
        this.loadPage();
        this.isLoading = false;
      })
    }

    // this.loadPage();
    let getcurrentuser = this.authenticationservice.getCurrentUser()
    if (getcurrentuser !== undefined) {
      getcurrentuser.then(_ => {
        this.currentuser = User.fromJson(_);
      });
    }

    this.setting.getSettingByPage('product').subscribe(config => {
      // console.log("here");
      this.productsafety = ConfigValue.toConfigArray(config.find(x => x.type == 'safety').value)
      // console.log("productsafety is");
      // console.log(this.productsafety);
    })

    this.setting.getSettingByPage('application').subscribe(config => {
      this.applicationnote = ConfigValue.toConfigArray(config.find(x => x.type == 'note').value)
      // console.log("applicationnote is");
      // console.log(this.applicationnote);
    })

    // let lasttwo=[];
    // let products=[];
    // this.productService.loadProduct().subscribe(allproducts=>{
    //   products = allproducts.slice();
    //   products.forEach((product,index)=>{
    //     console.log('catalog: '+product.cat);
    //     let part=product.cat.split('-');
    //     console.log('part is');
    //     console.log(part)
    //     console.log('part [2] is');
    //     console.log(part[2])
    //     let onumber= part[2].replace(/\D*/g,'')
    //     console.log('onumber is: '+onumber);
    //     let twolast = onumber.slice(-2)
    //     console.log('twolast is');
    //     console.log(twolast)
    //     let twodigit=parseInt(twolast)
    //     console.log('twolast digit is');
    //     console.log(twodigit)
    //     lasttwo.push(twodigit);
    //     if(index==products.length-1){
    //       console.log(lasttwo);
    //       lasttwo.sort((a, b) => b - a);
    //       console.log(lasttwo);
    //     }
    //   });
    // })



  }

  ngOnChanges(change: SimpleChanges) {
    // this.reset();
    console.log(this.displayProduct);
    console.log('in ngOnChanges')
    if (change.displayProduct && this.displayProduct && this.displayProduct !== undefined) {
      if (!change.displayProduct.isFirstChange()) {
        console.log('not first change')
        this.changeDisplayproduct();
        this.allcount=1;
      }
    }

  }

  changeDisplayproduct(){
    this.displayProducts=[];
    this.displayProducts.push(this.displayProduct);
    this.isLoading = false;
  }

  toggleViewCol(index: number) {
    this.viewColumn[index] = !this.viewColumn[index];
  }

  loadPage() {
    // this.isLoading = true;
    // this.productService.getCount().subscribe(c=>{
    //   this.allcount = c;
    //   this.productService.loadProductByPage(this.page - 1).subscribe(products => {
    //     this.products = products;
    //     this.displayProducts = this.products.slice();
    //     this.isLoading = false;
    //   });
    // })
    this.isLoading = true;
    this.productService.loadProductByPage(this.page - 1).subscribe(products => {
      this.products = products;
      this.displayProducts = this.products.slice();
      this.isLoading = false;
    });
  }

  showproduct(product: Product) {
    this.displayProduct = product;
    this.productapplicationsforpdf = [];
    this.applicationService.getApplicationByProductId(product.dbid).subscribe(applications => {
      this.productapplicationsforpdf = applications.slice();
    })

    this.isShowProduct = true;
  }

  open(content, type?: string) { // pass modal type
    //default can not edit
    this.enableEditProduct = false;
    this.isShowProduct = true;

    //tell it is a new recipe
    if (type == 'nr') {
      //if a new recipe, then create a new recipe and assign to displayRecipe
      this.isNewProduct = true;
      this.anewProduct();
      this.displayProduct = this.newProduct;
    }

    this.sopModal = this.modalService.open(content, { backdrop: "static", size: 'lg' });
    this.sopModal.result.then((result) => {
      this.enableEditProduct = false;
      this.isNewProduct = false;
      this.reset();
    }, () => {
      //close with X
      this.reset();
    });
  }

  anewProduct() {
    this.newProduct = new Product(-1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, null, null, '', '');
    this.enableEditProduct = true;
    this.isShowProduct = true;
    this.displayProduct = this.newProduct;
    this.isNewProduct = true;
  }

  reset() {
    this.enableEditProduct = false;
    this.isShowProduct = false;
    this.displayProduct = undefined;
    this.resetView.emit();
    this.isNewProduct = false;
    this.page = 1;
    this.searchKey = '';
  }

  searchProduct(key: string) {
    this.isLoading = true;
    if (key == undefined || key == '') {
      this.productService.getCount().subscribe(c => {
        this.allcount = c;
        this.page = 1;

        this.loadPage();
        this.isLoading = false;
      })
    }
    else {
      this.productService.searchProduct(key, this.page - 1).subscribe(result => {
        this.displayProducts = [];
        this.products = [];
        this.displayProducts = result.slice();
        this.productService.searchProductCount(key).subscribe(count => {
          this.allcount = count;
        })
        this.isLoading = false;
      })
    }


  }

  checknextnumber(product: Product) {
    let catalognumber = product.cat.split('-');
    let part2 = catalognumber[2]

    let onlynumber = part2.replace(/\D*/g, '');

    let lasttwo = onlynumber.slice(-2);

    this.setting.getSettingByPage('catalog').subscribe(config => {
      let nextProductNum = config.find(x => x.type == 'productPointer');

      let nextnumber = nextProductNum.value;
      if (parseInt(lasttwo) >= parseInt(nextnumber)) {
        nextProductNum.value = (parseInt(lasttwo) + 1).toString();
        this.setting.updateSetting(nextProductNum);
      }

    });
  }

  saveProduct(p: { product: Product, updatedapplications: Application[] }) {

    if (p.product == undefined || p.updatedapplications == undefined) return;

    //new product    
    if (p.product.dbid == -1) {

      let addproduct = this.productService.addProduct(p.product)
      let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Product', 'Add a New Product', 'Product name: '+p.product.name+', Product cat#: '+p.product.cat)
      this.userhistoryservice.addUserHistory(userhistory)

      addproduct.then(_ => {
        let dbid = _['dbid'];
        p.product.dbid = dbid;

        this.checknextnumber(p.product);

        if (p.updatedapplications.length > 0) {
          p.updatedapplications.forEach((application, index) => {
            if (application.dbid == -1) {
              application.productdbid = dbid;
              this.applicationService.addApplication(application).then(_ => {
                application.dbid = _['dbid'];
                if (index == p.updatedapplications.length - 1) {
                  this.isShowProduct = false;
                  this.resetrecipedisplay(p.product);
                  this.page = 1;
                  this.loadPage();
                }
              });
            }
          })
        }
        else {
          this.isShowProduct = false;
          this.resetrecipedisplay(p.product);
          this.page = 1;
          this.loadPage();
        }




      });
    }
    else {
      //an existing product
      //console.log("this is an existing product");
      let allpromises = [];
      this.productService.updateProduct(p.product).then(_ => {
        let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Product', 'Updated a Product', 'Product name: '+p.product.name+', Product cat#: '+p.product.cat)
        this.userhistoryservice.addUserHistory(userhistory)
        //if change the product in the current page
        let index = this.displayProducts.findIndex(find => find.dbid == p.product.dbid);
        if (index != -1) {
          this.displayProducts[index] = p.product;
        }

        this.checknextnumber(p.product);

        this.applicationService.getApplicationByProductId(p.product.dbid).subscribe(applications => {
          let originalapplications = applications;

          //if user remove a product application
          originalapplications.forEach(originalapplication => {
            let index = p.updatedapplications.findIndex(updatedapplication => updatedapplication.dbid == originalapplication.dbid);
            if (index == -1) {
              allpromises.push(this.applicationService.deleteApplication(originalapplication));
            }
          });

          //if user edit the existing sop ingredients or add new sop ingredient
          p.updatedapplications.forEach(updatedapplication => {
            let index = originalapplications.findIndex(x => x.dbid == updatedapplication.dbid);
            //if user add new ingredients for this recipe
            if (index == -1) {
              allpromises.push(this.applicationService.addApplication(updatedapplication));
            }//if user updated the recipe ingredients
            else {
              let index = originalapplications.findIndex(originalcomponent => originalcomponent.dbid == updatedapplication.dbid);
              if (JSON.stringify(updatedapplication) != JSON.stringify(originalapplications[index])) {
                allpromises.push(this.applicationService.updateApplication(updatedapplication));
              }
            }
          });
          Promise.all(allpromises).then(_ => {
            this.isShowProduct = false;
            this.resetrecipedisplay(p.product);
            this.loadPage();
          })
        });
      });
    }
  }

  updatestatus(updatedproduct: Product) {
    this.productService.updateProduct(updatedproduct).then(_ => {
      let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Product', 'Updated a Product', 'Product name: '+updatedproduct.name+', Product cat#: '+updatedproduct.cat)
      this.userhistoryservice.addUserHistory(userhistory)
      this.isShowProduct = false;
      this.page = 1;
      this.loadPage();
    })
  }

  resetrecipedisplay(p: Product) {
    this.displayProduct = p;
    this.enableEditProduct = false;
    this.isNewProduct = false;
  }

  openconfirmation(message) {
    if (message == 'delete') {
      this.toconfirm = 'delete';
      this.confirmationmessage = 'Are you sure you want to delete it?';
      this.modalService.open(this.confirmation, { backdrop: "static" }).result.then(result => { });
    }
  }

  action() {
    if (this.toconfirm == 'delete') {
      this.deleteProduct();
      //this.sopModal.close() ;
    }
  }

  deleteProduct() {
    if (this.displayProduct == undefined || this.displayProduct.dbid == -1) { return; }
    let tempapplications;
    let id = this.displayProduct.dbid
    //if this recipe exist in displayRecipes
    this.productService.deleteProduct(this.displayProduct).then((result) => {
      let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Product', 'Deleted a Product', 'Product name: '+this.displayProduct.name+', Product cat#: '+this.displayProduct.cat)
      this.userhistoryservice.addUserHistory(userhistory);

      let pindex = this.displayProducts.findIndex(x => x.dbid == id);
      if (pindex != -1) {
        this.displayProducts.splice(pindex, 1);
      }
      //delete all recipe ingredients
      this.applicationService.getApplicationByProductId(id).subscribe(applications => {
        tempapplications = applications.slice();
        tempapplications.forEach((application, index) => {
          if (index == tempapplications.length - 1) {
            this.applicationService.deleteApplication(application).then(result => {
              this.displayProduct = undefined;
              this.isShowProduct = false;
              this.loadPage();
            })
          }
          else {
            this.applicationService.deleteApplication(application)
          }
        });
      });
    }).catch(err => { })
  }

  getfile(event) {
    if (event.target.files && event.target.files[0]) {
      this.importfile = event.target.files[0];
    }
  }

  import() {
    this.isLoading = true;
    var reader = new FileReader();
    var filedata;
    var workbook;
    var XL_row_object;
    var self = this;
    let allpromises = [];

    reader.onload = function (event) {
      var data = reader.result;
      workbook = XLSX.read(data, { type: 'binary' });
      let sheetnumber = workbook.SheetNames.length;

      self.productService.loadProduct().subscribe(products => {
        workbook.SheetNames.forEach((sheetName, sindex) => {
          XL_row_object = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName]);
          let sheetname = sheetName;
          let index = 0;
          let condition = true;
          let i = 0;
          self.readeachlineoffile(i, XL_row_object, sheetname, sindex, sheetnumber, products);
        });

      });
    }

    //reader.readAsArrayBuffer(this.importfile);
    reader.readAsBinaryString(this.importfile);
    //reader.readAsText(this.importfile);

    // self.page = 1;
    // self.loadPage();
    // self.isLoading = false;
  }

  readeachlineoffile(index: number, XL_row_object: any, sheetname: string, sindex: number, sheetnumber: number, products: Product[]) {
    let rownumber = XL_row_object.length;
    if (index < rownumber) {
      let newproduct = new Product(-1, '', sheetname, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Entered', this.currentuser.name, '', '', new Date(), null, null, '', '')

      if (XL_row_object[index]['name']) { newproduct.name = XL_row_object[index]['name'].trim() };
      if (XL_row_object[index]['clonality']) { newproduct.clonality = XL_row_object[index]['clonality'].trim() };
      if (XL_row_object[index]['host']) { newproduct.host = XL_row_object[index]['host'].trim() }
      if (XL_row_object[index]['cat']) { newproduct.cat = XL_row_object[index]['cat'].trim() }
      if (XL_row_object[index]['oldcat']) { newproduct.oldcat = XL_row_object[index]['oldcat'].trim() }
      if (XL_row_object[index]['packsize']) { newproduct.packsize = XL_row_object[index]['packsize'].trim() }
      if (XL_row_object[index]['comment']) { newproduct.comment = XL_row_object[index]['comment'].trim() }
      if (XL_row_object[index]['description']) { newproduct.description = XL_row_object[index]['description'].trim() }
      if (XL_row_object[index]['quantity']) {
        let quantity = XL_row_object[index]['quantity'];
        let unit = quantity.replace(/[0-9]/g, '');
        let unit1 = unit.replace('.', '').trim()
        let unitsize = quantity.replace(unit1, '').trim();
        newproduct.unitsize = unitsize;
        newproduct.unit = unit1;
      };
      if (XL_row_object[index]['price']) {
        let price = XL_row_object[index]['price'];
        let pricenumber1 = price.replace('$', '').trim();
        let pricenumber2 = pricenumber1.replace(',', '').trim();
        newproduct.unitprice = pricenumber2;
      };

      let pindex = products.findIndex(x => x.name.trim() == newproduct.name.trim());
      if (pindex == -1) {
        this.productService.addProduct(newproduct).then(_ => {
          if (index < rownumber && sindex < sheetnumber) {
            if (index == rownumber - 1 && sindex == sheetnumber - 1) {
              this.page = 1;
              this.productService.getCount().subscribe(c => {
                this.allcount = c;
                this.loadPage();
              })
              // this.loadPage();
              return;
            }
            else {
              this.readeachlineoffile(index + 1, XL_row_object, sheetname, sindex, sheetnumber, products);
            }
          }
        })
      }
      else {
        newproduct.dbid = products[pindex].dbid;
        this.productService.updateProduct(newproduct).then(result => {
          if (index < rownumber && sindex < sheetnumber) {
            if (index == rownumber - 1 && sindex == sheetnumber - 1) {
              this.page = 1;
              this.page = 1;
              this.productService.getCount().subscribe(c => {
                this.allcount = c;
                this.loadPage();
              })
              // this.loadPage();
              return;
            }
            else {
              this.readeachlineoffile(index + 1, XL_row_object, sheetname, sindex, sheetnumber, products);
            }
          }

        });
      }
    }
  }

  //generate as an image
  // generatePDF(product: Product){
  //   var data = document.getElementById('productpdftable');  
  //   console.log(data)
  //   html2canvas(data).then(canvas => {  
  //     // Few necessary setting options  
  //     var imgWidth = 208;   
  //     var pageHeight = 295;    
  //     var imgHeight = canvas.height * imgWidth / canvas.width;  
  //     var heightLeft = imgHeight;  

  //     const contentDataURL = canvas.toDataURL('image/jpeg') 
  //     var doc = new jsPDF('p', 'mm', 'a4');

  //     let imgData = image.logo;
  //     let width = doc.internal.pageSize.getWidth();
  //     let height = doc.internal.pageSize.getHeight();
  //     let pagemargin = {
  //       left: 25,
  //       right: 25,
  //       top: 20,
  //       bottom: 10
  //     }

  //     doc.addImage(imgData, 'JPEG', 30, 15, 50, 18);
  //       let applicationnote = this.applicationnote;
  //       //doc.setDrawColor(70,130,180);
  //       //       #0285B9
  //       // (2,133,185)
  //       doc.setDrawColor(2, 133, 186);
  //       doc.setLineWidth(0.8);
  //       doc.line(85, 10, 85, 40);
  //       doc.setFontSize(8);
  //       doc.setTextColor(2, 133, 186);
  //       doc.text('19 Innovation Way, Charlottetown, PE', 88, 18);
  //       doc.text('Canada C1E 0B7', 88, 22);
  //       doc.text('Tel: 902-367-4322 Fax: 902-367-4323', 88, 26);
  //       doc.text('www.somrubioscience.com', 88, 30);

  //       doc.setFontSize(18);
  //       doc.setTextColor(0, 0, 0);
  //       doc.setFont('times', 'bold');
  //       doc.text(this.displayProduct.name + ', ' + this.displayProduct.unitsize + ' ' + this.displayProduct.unit, width - pagemargin.right, 55, { align: 'right' });

  //       doc.setFontSize(12);
  //       doc.setTextColor(0, 0, 0);
  //       doc.setFont('times', 'bold');
  //       let cattext = 'Catalogue Number: ' + this.displayProduct.cat
  //       doc.text(cattext, width - pagemargin.right, 61, { align: 'right' });

  //       doc.setFontSize(19);
  //       doc.setTextColor(0, 0, 0);
  //       doc.setFont('times', 'bold');
  //       doc.text('Product Specifications Data Sheet', pagemargin.left, 75, { align: 'left' });

  //       // var res = doc.autoTableHtmlToJson(document.getElementById("productpdftable"),false);
  //       // doc.autoTable(res.columns, res.data, {startY: 80});
  //       //doc.autoTable({html: '#productpdftable', startY: 100});
  //       // let specialElementHandlers = {'#editor': function(element, renderer){
  //       //   return true;
  //       // }}
  //       // let content=this.productpdftable.nativeElement;
  //       // doc.fromHTML(content.innerHTML,15, 15,{
  //       //   'width':180,
  //       //   'elementHandlers':specialElementHandlers
  //       // }) 

  //     doc.addImage(contentDataURL, 'JPEG', 25, 80, width-2*pagemargin.left, 150)   

  //     doc.setFontSize(12);
  //     doc.setTextColor(0, 0, 0);
  //     doc.setFont('times', 'bold');
  //     doc.text('Precautions: Use normal precautions for handing of blood derived products.', width / 2, height - pagemargin.bottom - 8, { align: 'center', valign: 'top' });

  //     doc.setFontSize(18);
  //     doc.setTextColor(0, 0, 0);
  //     doc.setFont('times', 'bold');
  //     doc.text('FOR RESEARCH USE ONLY', width / 2, height - pagemargin.bottom, { align: 'center', valign: 'top' });


  //     doc.save(product.name + '.pdf');


  //   });  

  // }

  //generate as an image
  // generatePDF(product: Product) {
  //   var data = document.getElementById('productpdftable');

  //   var doc = new jsPDF();

  //   let imgData = image.logo;
  //   let width = doc.internal.pageSize.getWidth();
  //   let height = doc.internal.pageSize.getHeight();
  //   let pagemargin = {
  //     left: 25,
  //     right: 25,
  //     top: 20,
  //     bottom: 10
  //   }

  //   doc.addImage(imgData, 'JPEG', 30, 15, 50, 18);
  //   let applicationnote = this.applicationnote;
  //   //doc.setDrawColor(70,130,180);
  //   //       #0285B9
  //   // (2,133,185)
  //   doc.setDrawColor(2, 133, 186);
  //   doc.setLineWidth(0.8);
  //   doc.line(85, 10, 85, 40);
  //   doc.setFontSize(8);
  //   doc.setTextColor(2, 133, 186);
  //   doc.text('19 Innovation Way, Charlottetown, PE', 88, 18);
  //   doc.text('Canada C1E 0B7', 88, 22);
  //   doc.text('Tel: 902-367-4322 Fax: 902-367-4323', 88, 26);
  //   doc.text('www.somrubioscience.com', 88, 30);

  //   doc.setFontSize(18);
  //   doc.setTextColor(0, 0, 0);
  //   doc.setFont('times', 'bold');
  //   doc.text(this.displayProduct.name + ', ' + this.displayProduct.unitsize + ' ' + this.displayProduct.unit, width - pagemargin.right, 55, { align: 'right' });

  //   doc.setFontSize(12);
  //   doc.setTextColor(0, 0, 0);
  //   doc.setFont('times', 'bold');
  //   let cattext = 'Catalogue Number: ' + this.displayProduct.cat
  //   doc.text(cattext, width - pagemargin.right, 61, { align: 'right' });

  //   doc.setFontSize(19);
  //   doc.setTextColor(0, 0, 0);
  //   doc.setFont('times', 'bold');
  //   doc.text('Product Specifications Data Sheet', pagemargin.left, 75, { align: 'left' });

  //   // var res = doc.autoTableHtmlToJson(document.getElementById("productpdftable"),true);
  //   // doc.autoTable(res.columns, res.data, {startY: 80});
  //   // console.log(document.getElementById("productpdftable"))

  //   // doc.autoTable({
  //   //   theme: 'grid',
  //   //   showHeader: 'false',

  //   //   columnStyles: {
  //   //     0: { cellWidth: Math.floor(width - pagemargin.left - pagemargin.right) / 4 },
  //   //     1: { cellWidth: (Math.floor(width - pagemargin.left - pagemargin.right) / 4) * 3 }
  //   //   },
  //   //   html: '#productpdftable',
  //   //   startY: 80,
  //   //   margin: { left: pagemargin.left, right: pagemargin.right },
  //   //   pageBreak: 'avoid', // 'auto', 'avoid' or 'always'
  //   //   tableWidth: Math.floor(width - pagemargin.left - pagemargin.right)
  //   // })

 

  //         let specialElementHandlers = {
  //           // element with id of "bypass" - jQuery style selector
  //           '#editor': function (element, renderer) {
  //               // true = "handled elsewhere, bypass text extraction"
  //               return true
  //           }
  //       };

  //       let content=this.productpdftable.nativeElement;
  //       console.log(content.innerHTML)
  //       doc.fromHTML(content.innerHTML, 25, 80, {
  //         'width': width-50,'elementHandlers': specialElementHandlers
  //     });
  //     var margins = {
  //       top: 80,
  //       bottom: 20,
  //       left: 25,
  //       width: 150
  //   };

  //   //replace with autotable
  //     doc.fromHTML($('#productpdftable').get(0), 25, 80, {
  //       'width': width-50,'elementHandlers': specialElementHandlers
  //   }, margins);
    

  //   doc.setFontSize(12);
  //   doc.setTextColor(0, 0, 0);
  //   doc.setFont('times', 'bold');
  //   doc.text('Precautions: Use normal precautions for handing of blood derived products.', width / 2, height - pagemargin.bottom - 8, { align: 'center', valign: 'top' });

  //   doc.setFontSize(18);
  //   doc.setTextColor(0, 0, 0);
  //   doc.setFont('times', 'bold');
  //   doc.text('FOR RESEARCH USE ONLY', width / 2, height - pagemargin.bottom, { align: 'center', valign: 'top' });


  //   doc.save(product.name + '.pdf');




  // }

  //wrote in autotable change style
  generatePDF(product: Product) {
    //console.log("test applicationnote");
    //console.log(this.applicationnote);
    var doc = new jsPDF();
    let imgData = image.logo;
    let width = doc.internal.pageSize.getWidth();
    let height = doc.internal.pageSize.getHeight();
    let pagemargin = {
      left: 25,
      right: 25,
      top: 20,
      bottom: 10
    }

    this.applicationService.getApplicationByProductId(this.displayProduct.dbid).subscribe(applications => {
      //console.log(applications);
      doc.addImage(imgData, 'JPEG', 30, 15, 50, 18);
      let applicationnote = this.applicationnote;
      //doc.setDrawColor(70,130,180);
      //       #0285B9
      // (2,133,185)
      doc.setDrawColor(2, 133, 186);
      doc.setLineWidth(0.8);
      doc.line(85, 10, 85, 40);
      doc.setFontSize(8);
      doc.setTextColor(2, 133, 186);
      doc.text('19 Innovation Way, Charlottetown, PE', 88, 18);
      doc.text('Canada C1E 0B7', 88, 22);
      doc.text('Tel: 902-367-4322 Fax: 902-367-4323', 88, 26);
      doc.text('www.somrubioscience.com', 88, 30);

      doc.setFontSize(18);
      doc.setTextColor(0, 0, 0);
      doc.setFont('times', 'bold');
      doc.text(this.displayProduct.name + ', ' + this.displayProduct.unitsize + ' ' + this.displayProduct.unit, width - pagemargin.right, 55, { align: 'right' });

      doc.setFontSize(12);
      doc.setTextColor(0, 0, 0);
      doc.setFont('times', 'bold');
      let cattext = 'Catalogue Number: ' + this.displayProduct.cat
      doc.text(cattext, width - pagemargin.right, 61, { align: 'right' });

      doc.setFontSize(19);
      doc.setTextColor(0, 0, 0);
      doc.setFont('times', 'bold');
      doc.text('Product Specifications Data Sheet', pagemargin.left, 75, { align: 'left' });

      let datalist = [];
      let header = ['',''];
  //[{'Description', colspan:1, rowspan:1}], [{this.displayProduct.description, colspan:3, rowspan:1}]
      if (this.displayProduct.description && this.displayProduct.description.trim() != '') {
        let datarow = [];
        datarow.push('Description', this.displayProduct.description);
        datalist.push(datarow);
        //header=['Description', this.displayProduct.description ];
      }
      // else{
      //   header=['',''];
      // }

      if (this.displayProduct.unitsize && this.displayProduct.unitsize.trim() != ''
        && this.displayProduct.unit && this.displayProduct.unit.trim() != '') {
        let datarow = [];
        datarow.push('Quantity', this.displayProduct.unitsize + ' ' + this.displayProduct.unit);
        datalist.push(datarow);
      }

      if (this.displayProduct.host && this.displayProduct.host.trim() != '' ||
        this.displayProduct.clonality && this.displayProduct.clonality.trim() != '' ||
        this.displayProduct.isotype && this.displayProduct.isotype.trim() != '') {
        let datarow = [];
        datarow.push('Host/Isotype', this.displayProduct.host + ' ' + this.displayProduct.clonality + ' ' + this.displayProduct.isotype);
        datalist.push(datarow);
      }

      if (this.displayProduct.immunogen && this.displayProduct.immunogen.trim() != '') {
        //console.log("immunogen")
        //console.log(this.displayProduct.immunogen)
        let datarow = [];
        datarow.push('Immunogen', this.displayProduct.immunogen);
        datalist.push(datarow);
      }

      if (this.displayProduct.purification && this.displayProduct.purification.trim() != '') {
        let datarow = [];
        datarow.push('Purification', this.displayProduct.purification);
        datalist.push(datarow);
      }

      if (this.displayProduct.buffer && this.displayProduct.buffer.trim() != '') {
        let datarow = [];
        datarow.push('Buffer', this.displayProduct.buffer);
        datalist.push(datarow);
      }

      if (this.displayProduct.specificity && this.displayProduct.specificity.trim() != '') {
        let datarow = [];
        datarow.push('Specificity', this.displayProduct.specificity);
        datalist.push(datarow);
      }

      if (this.displayProduct.reconstitution && this.displayProduct.reconstitution.trim() != '') {
        let datarow = [];
        datarow.push('Reconstitution', this.displayProduct.reconstitution);
        datalist.push(datarow);
      }

      if (this.displayProduct.storage && this.displayProduct.storage.trim() != '') {
        let datarow = [];
        datarow.push('Storage', this.displayProduct.storage);
        datalist.push(datarow);
      }

      let nextpointer = 80;

      if (applications.length > 0) {
        let datarow = [];
        let applicationlist = [];

        datarow.push('Applications', []);
        datalist.push(datarow);
      }

      //let safety:string;
      //safety='This product should be handled by trained personnel only. Please contact our customer service at customer@somrubioscience.com for more details.';
      let safety = this.productsafety[0].name;
      //console.log("safety is: "+safety);
      let datarow = [];
      datarow.push('Safety/' + '\n' + 'Precautions', safety);
      datalist.push(datarow);


      let rowheight, contentpadding, leftcolumnwidth, rightcolumnwidth, wordlineheight, ypoint, xpoint;
      let applicationwidth, apwholewidth, apcellwidth, apcellheight;

      doc.autoTable(header, datalist, {
        theme: 'grid',
        showHeader: 'false',
        styles: {
          fillColor: [255, 255, 255],
          fontSize: 12,
          font: 'times',
          textColor: [0, 0, 0],
          lineColor: [173, 216, 230],
          overflow: 'linebreak'
        },
        headerStyles: {
          lineWidth: 0.05,
          fillColor: [255, 255, 255],
          fontSize: 12,
          font: 'times',
          fontStyle: 'bold',
          textColor: [0, 0, 0],
          lineColor: [173, 216, 230],
          overflow: 'linebreak'
        },
        columnStyles: {
          0: {
            fontStyle: 'bold',
            halign: 'left',
            columnWidth: 'wrap',
            fillColor: [255, 255, 255],
            valign: 'top'
          },
          1: {
            halign: 'left',
            columnWidth: 'auto',
            fillColor: [255, 255, 255],
            valign: 'top'
          }

        },
        margin: { left: pagemargin.left, right: pagemargin.right },
        startY: nextpointer, // false (indicates margin top value) or a number
        pageBreak: 'avoid', // 'auto', 'avoid' or 'always'
        tableWidth: Math.floor(width - pagemargin.left - pagemargin.right), // 'auto', 'wrap' or a number, 
        tableLineColor: [173, 216, 230], // number, array (see color section below)
        tableLineWidth: 0.05,
        // showHeader: 'never',
        drawRow: function (row, data) {
          if (row.index == '0') {
            rowheight = row.height;
            leftcolumnwidth = row.cells[0].width;
            rightcolumnwidth = row.cells[1].width;
            contentpadding = row.cells[1].styles.cellPadding
            wordlineheight = rowheight - contentpadding * 2;
            xpoint = row.cells[1].textPos.x;
          }
          nextpointer = nextpointer + rowheight;

          if (row.raw[0] == 'Applications') {
            row.height = 11 + 11 * applications.length + 11;
          }

          if (row.raw[0] == 'Immunogen' && (row.raw[1].includes('<i>') || row.raw[1].includes('<sub>'))) {
            let cellpadding=row.cells[1].styles.cellPadding;
            //let wordlineheight=row.height-2*cellpadding;
            let secondcellwidth=row.cells['1'].width;
            let secondcelltext='';
            row.cells[1].text.forEach(line=>{
              secondcelltext=secondcelltext+line.trim();
            });
            secondcelltext = secondcelltext.replace(/<i>/g, "");
            secondcelltext = secondcelltext.replace(/<\/i>/g, "");
            secondcelltext = secondcelltext.replace(/<sub>/g, "");
            secondcelltext = secondcelltext.replace(/<\/sub>/g, "");
            secondcelltext = secondcelltext.replace(/\n|\r/g, "");
            secondcelltext = secondcelltext.trim();
            let textarray=doc.splitTextToSize(secondcelltext, 133.69702882407407 - 2 * cellpadding);
            let cellheight= (textarray.length + 1) * contentpadding + textarray.length * wordlineheight;
            row.height = cellheight;

          }

        },
        drawCell: function (cell, data) {
          if (data.row.raw[0] && data.row.raw[0] == 'Description' && cell.text != 'Description') {
            doc.setFontStyle('bold');
          }
          if (data.row.raw[0] && data.row.raw[0] == 'Applications' && cell.text == 'Applications') {
            applicationwidth = cell.width;
          }

          if (data.row.raw[0] && data.row.raw[0] == 'Immunogen' && cell.text != 'Immunogen' && (cell.raw.includes('<i>') || cell.raw.includes('<sub>'))) {
            doc.setDrawColor(173, 216, 230);
            doc.setFillColor(255, 255, 255)
            let celltext = '';
            cell.text.forEach(line => {
              celltext = celltext + ' ' + line;
            })

            let celltexttest = cell.text.toString();
            let iwords = [];
            let subwords = [];
            while (celltexttest.includes('<i>')) {
              let index1 = celltexttest.indexOf('<i>');
              let index2 = celltexttest.indexOf('</i>');
              let iword = celltexttest.substring(index1 + 3, index2)
              iwords.push(iword);
              celltexttest = celltexttest.substring(0, index1) + celltexttest.substring(index2 + 4, celltexttest.length)
            }

            while (celltexttest.includes('<sub>')) {
              let index1 = celltexttest.indexOf('<sub>');
              let index2 = celltexttest.indexOf('</sub>');
              let subword = celltexttest.substring(index1 + 5, index2)
              subwords.push(subword);
              celltexttest = celltexttest.substring(0, index1) + celltexttest.substring(index2 + 6, celltexttest.length)
            }

            celltext = celltext.replace(/<i>/g, "");
            celltext = celltext.replace(/<\/i>/g, "");
            celltext = celltext.replace(/<sub>/g, "");
            celltext = celltext.replace(/<\/sub>/g, "");
            let textarray = doc.splitTextToSize(celltext, cell.width - 2 * contentpadding)

            doc.setFontSize(12);
            doc.setTextColor(0, 0, 0);
            doc.setFont('times', 'normal');

            textarray.forEach((line, index) => {
              let xpointer, ypointer;
              if (index == 0) {
                xpointer = cell.x + contentpadding;
                ypointer = cell.y + (index + 1) * 3 * contentpadding + index * wordlineheight
              }
              else if (index > 0) {
                xpointer = cell.x + contentpadding;
                ypointer = cell.y + (index + 1) * 2 * contentpadding + index * wordlineheight
              }

              if (line.includes('E. coli')) {
                line = line.replace('E. coli', 'E.coli')
              }
              if (line.includes('S. frugiperda')) {
                line = line.replace('S. frugiperda', 'S.frugiperda')
              }
              let words = line.trim().split(' ')
              // console.log('words are')
              // console.log(words)
              words.forEach(word => {
                if (word.includes('E.coli')) {
                  word = word.replace('E.coli', 'E. coli');
                }
                if (word.includes('S.frugiperda')) {
                  word = word.replace('S.frugiperda', 'S. frugiperda');
                }
                if (iwords.findIndex(x => x == word) != -1) {
                  doc.setFontStyle('italic');
                  doc.text(word, xpointer, ypointer)
                  xpointer = xpointer + doc.getTextWidth(word) + contentpadding
                }
                // else if(subwords.findIndex(x=>word.includes(x))!=-1){
                //   let subword=subwords.find(x=>word.includes(x))
                //   let index=word.indexOf(subword);
                //   doc.setFontSize(12);
                //   let firstpart=word.substring(0,index)
                //   doc.text(firstpart,xpointer, ypointer)
                //   xpointer=xpointer+doc.getTextWidth(firstpart)
                //   let secondpart=word.substring(index, word.length)
                //   doc.setFontSize(6);
                //   doc.text(secondpart,xpointer, ypointer)
                //   xpointer=xpointer+doc.getTextWidth(secondpart)+contentpadding
                // }
                else if (word.includes('VEGF')) {
                  let vindex = word.indexOf('VEGF')
                  doc.setFontSize(12);
                  let firstpart = word.substring(0, index + 4)
                  doc.text(firstpart, xpointer, ypointer)
                  xpointer = xpointer + doc.getTextWidth(firstpart)
                  let secondpart = word.substring(index + 4, index + 7)
                  doc.setFontSize(6);
                  doc.text(secondpart, xpointer, ypointer)
                  xpointer = xpointer + doc.getTextWidth(secondpart) + contentpadding


                }
                else {
                  doc.setFontSize(12);
                  doc.setFontStyle('normal');
                  doc.text(word, xpointer, ypointer)
                  xpointer = xpointer + doc.getTextWidth(word) + contentpadding
                }
                // doc.text(word,xpointer, ypointer)
                // xpointer=xpointer+doc.getTextWidth(word)+contentpadding
              })

            })

            return false;

          }

          if (data.row.raw[0] && data.row.raw[0] == 'Applications' && cell.text != 'Applications') {
            apwholewidth = width - pagemargin.left - pagemargin.right - applicationwidth;
            // console.log("width is: "+width);
            // console.log("pagemargin.left is: "+pagemargin.left);
            // console.log("pagemargin.right is: "+pagemargin.right);
            // console.log("applicationwidth is: "+applicationwidth);
            // console.log("apwholewidth is: "+apwholewidth);
            cell.width = apwholewidth;
            apcellwidth = apwholewidth / 3;
            apcellheight = data.row.height / (applications.length + 2);

            // console.log("ap cell width is: "+apcellwidth);
            // console.log("ap cell height is: "+apcellheight);            

            doc.setDrawColor(173, 216, 230);
            doc.setFillColor(255, 255, 255)
            doc.rect(cell.x, cell.y, apcellwidth - 2, apcellheight, 'FD');
            doc.setFontSize(12);
            doc.setTextColor(0, 0, 0);
            doc.setFont('times', 'bold');
            doc.text('Purpose', cell.x + contentpadding, cell.y + 5, { align: 'left', valign: 'top' })

            doc.setDrawColor(173, 216, 230);
            doc.setFillColor(255, 255, 255)
            doc.rect(cell.x + apcellwidth - 2, cell.y, apcellwidth - 2, apcellheight, 'FD');
            doc.setFontSize(12);
            doc.setTextColor(0, 0, 0);
            doc.setFont('times', 'bold');
            doc.text('Recommended' + '\n' + 'Concentration', cell.x + contentpadding + apcellwidth - 2, cell.y + 5, { align: 'left', valign: 'top' })

            doc.setDrawColor(173, 216, 230);
            doc.setFillColor(255, 255, 255)
            doc.rect(cell.x + apcellwidth + apcellwidth - 4, cell.y, apcellwidth + 4, apcellheight, 'FD');
            doc.setFontSize(12);
            doc.setTextColor(0, 0, 0);
            doc.setFont('times', 'bold');
            doc.text('Comment', cell.x + contentpadding + apcellwidth + apcellwidth - 4, cell.y + 5, { align: 'left', valign: 'top' })

            let lastlinepoint;
            applications.forEach((application, index) => {
              doc.setDrawColor(173, 216, 230);
              doc.setFillColor(255, 255, 255)
              doc.rect(cell.x, cell.y + apcellheight * (index + 1), apcellwidth - 2, apcellheight, 'FD');
              doc.setFontSize(11);
              doc.setTextColor(0, 0, 0);
              doc.setFont('times', 'normal');
              doc.text(application.purpose, cell.x + contentpadding, cell.y + 5 + apcellheight * (index + 1), { align: 'left', valign: 'top' })

              doc.setDrawColor(173, 216, 230);
              doc.setFillColor(255, 255, 255)
              doc.rect(cell.x + apcellwidth - 2, cell.y + apcellheight * (index + 1), apcellwidth - 2, apcellheight, 'FD');
              doc.setFontSize(11);
              doc.setTextColor(0, 0, 0);
              doc.setFont('times', 'normal');
              doc.text(application.recommenconcentration, cell.x + contentpadding + apcellwidth - 2, cell.y + 5 + apcellheight * (index + 1), { align: 'left', valign: 'top' })

              doc.setDrawColor(173, 216, 230);
              doc.setFillColor(255, 255, 255)
              doc.rect(cell.x + apcellwidth + apcellwidth - 4, cell.y + apcellheight * (index + 1), apcellwidth + 4, apcellheight, 'FD');
              doc.setFontSize(11);
              doc.setTextColor(0, 0, 0);
              doc.setFont('times', 'normal');
              doc.text(application.comment, cell.x + contentpadding + apcellwidth + apcellwidth - 4, cell.y + 5 + apcellheight * (index + 1), { align: 'left', valign: 'top' })
              lastlinepoint = cell.y + apcellheight * (index + 1);
            });

            //let note='Please Note: Optimal dilutions should be determined by each laboratory for each application.';

            let note = applicationnote[0].name;
            let note2 = doc.splitTextToSize(note, apcellwidth * 3 - 10);
            doc.setDrawColor(173, 216, 230);
            doc.setFillColor(255, 255, 255)
            doc.rect(cell.x, lastlinepoint + apcellheight, apcellwidth * 3, apcellheight, 'FD');
            doc.setFontSize(12);
            doc.setTextColor(0, 0, 0);
            doc.setFont('times', 'italic');
            doc.text(note2, cell.x + contentpadding, lastlinepoint + apcellheight + 5, { align: 'left', valign: 'top' })

            return false;

          }

          if (data.row.raw[0] && data.row.raw[0] == 'Specificity' && cell.text != 'Specificity') {
            let xpointer = cell.x + contentpadding;
            let ypointer = cell.y + 3 * contentpadding;
            cell.text.forEach((line, lindex) => {
              // if(lindex==0){
              // xpointer=cell.x+contentpadding;
              // ypointer=ypointer+lindex*wordlineheight;
              // }
              // else if(lindex==1){
              // xpointer=cell.x+contentpadding;
              // ypointer=cell.y+(lindex+1)*1.5*contentpadding+lindex*wordlineheight;
              // }
              // else{
              // xpointer=cell.x+contentpadding;
              // ypointer=cell.y+(lindex+1)*contentpadding+lindex*wordlineheight-0.5;
              // }
              if (lindex > 0) {
                ypointer = ypointer + 0.3 * contentpadding + wordlineheight
                xpointer = cell.x + contentpadding;
              }
              else {
                xpointer = cell.x + contentpadding;
                ypointer = ypointer + lindex * wordlineheight;
              }


              let eachline = line;
              let words = eachline.split(' ');
              words.forEach(word => {
                if (word.includes('VEGF')) {
                  let index = word.indexOf('VEGF');
                  if (word.trim() != 'VEGF' && word.trim() != 'VEGF,' && word.trim() != 'VEGF.') {
                    doc.setFontSize(12);
                    doc.setFontStyle('normal');
                    let firstpart = word.substring(0, index + 4)
                    doc.text(firstpart, xpointer, ypointer)
                    xpointer = xpointer + doc.getTextWidth(firstpart)
                    doc.setFontSize(6);
                    let secondpart = word.substring(index + 4, index + 7)
                    doc.text(secondpart, xpointer, ypointer);
                    let thirdpart = word.substring(index + 7, word.length)
                    xpointer = xpointer + doc.getTextWidth(firstpart)
                    doc.setFontSize(12);
                    doc.text(thirdpart, xpointer + 0.5 * contentpadding, ypointer);
                  }
                  else {
                    doc.setFontSize(12);
                    doc.setFontStyle('normal');
                    doc.text(word, xpointer, ypointer)
                    xpointer = xpointer + doc.getTextWidth(word) + 0.5 * contentpadding
                  }
                }
                else {
                  doc.setFontSize(12);
                  doc.setFontStyle('normal');
                  doc.text(word, xpointer, ypointer)
                  xpointer = xpointer + doc.getTextWidth(word) + 0.5 * contentpadding
                }
              }

              )
            })
            return false;
          }


        }
      })

      doc.setFontSize(12);
      doc.setTextColor(0, 0, 0);
      doc.setFont('times', 'bold');
      doc.text('Precautions: Use normal precautions for handing of blood derived products.', width / 2, height - pagemargin.bottom - 8, { align: 'center', valign: 'top' });

      doc.setFontSize(18);
      doc.setTextColor(0, 0, 0);
      doc.setFont('times', 'bold');
      doc.text('FOR RESEARCH USE ONLY', width / 2, height - pagemargin.bottom, { align: 'center', valign: 'top' });


      doc.save(product.name + '.pdf');




    })
  }




}


