import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Equipment } from 'app/shared/objects/Equipment';
import { EquipmentService } from 'app/shared/services/Equipment.Service';

@Component({
  selector: 'app-equipment',
  providers: [EquipmentService],
  templateUrl: './equipment.component.html',
  styleUrls: ['./equipment.component.css']
})
export class EquipmentComponent implements OnInit, OnChanges {
  @ViewChild('displayequipment', {static: false}) displayequipment: ElementRef;

  viewColumn: boolean[];
  columnList: string[];
  isShowequipment: boolean;
  equipment: Equipment;
  enabledEditequipment: boolean;
  isNewequipment: boolean;
  equips: Equipment[] = [];
  displayEquips: Equipment[] = [];

  constructor(private modalService: NgbModal, private equipService: EquipmentService) { }

  ngOnInit() {
    this.loadEquip();
    this.viewColumn = [true, true, true, true, true, true, true];
    this.columnList = ['ID', 'Serial Number', 'Equipment Name', 'Manufactor', 'Model', 'Current Localtion', 'Comment'];
  }

  ngOnChanges(change: SimpleChanges) {

  }

  loadEquip() {
    this.equipService.loadEquipment().subscribe(equips => {
      this.equips = equips.sort((a, b) => { return a.id.localeCompare(b.id, undefined, { numeric: true, sensitivity: 'base' }) })
      this.displayEquips = this.equips.slice();
    });
  }

  reset() {
    this.enabledEditequipment = false;
    this.isShowequipment = false;
    this.equipment = undefined;
    this.isNewequipment = false;
  }

  open(content, size, modelClass: string) {
    this.modalService.open(content, { backdrop: "static", size: size, windowClass: modelClass }).result.then((result) => {
    });
  }

  openEquip(content, equipment?: Equipment) {
    this.equipment = equipment;
    this.isShowequipment = true;
    this.modalService.open(content, { backdrop: "static", size: 'lg' }).result.then((result) => {
      if (result !== 'deletewarn')
        this.reset();
    });
  }

  updateequipment(e: Equipment) {
    if (e == undefined) return;
    let index = this.equips.findIndex(x => x.dbid == this.equipment.dbid);
    if (index > -1) {
      this.equipService.updateEquipment(e).then(_ => {
        // console.log('success');
      }, err => {
        // console.log(err);
      });
    }
    else if (this.isNewequipment) {
      e.lastModify = (new Date()).toString();
      this.equips.push(e);
      this.isNewequipment = false;
    }
  }

  filterDisplay(key: string) {
    if (key == undefined) return;
    if (this.displayEquips == undefined) return;

    if (key.trim() == '') {
      this.displayEquips = this.equips.slice();
      return;
    }

    this.displayEquips = [];
    this.equips.forEach((equip, index) => {
      if ((equip.name !== undefined && equip.name.includes(key)) ||
        (equip.comment !== undefined && equip.comment.includes(key)) ||
        (equip.lastModify !== undefined && equip.lastModify.includes(key)) ||
        (equip.manufactor !== undefined && equip.manufactor.includes(key)) ||
        (equip.serial !== undefined && equip.serial.includes(key)) ||
        (equip.model !== undefined && equip.model.includes(key)) ||
        (equip.currentLocation !== undefined && equip.currentLocation.includes(key))) {
        this.displayEquips.push(equip);
      }
    })
  }

  newequipment() {
    this.equipment = new Equipment(-1, '', '', '', '', '', '', '', (new Date).toString());
    this.enabledEditequipment = true;
    this.isNewequipment = true;
    this.openEquip(this.displayequipment, this.equipment);
  }

  deleteequipment() {
    if (this.equipment == undefined) return;

    let index = this.equips.findIndex(x => x == this.equipment);
    if (index > -1) {
      this.equips.splice(index, 1);
      this.equipment = undefined;
      this.isShowequipment = false;
    }
  }

  toggleViewCol(index: number) {
    this.viewColumn[index] = !this.viewColumn[index];
  }

  convertDate(val: number) {
    return new Date(val).toString();
  }
}
