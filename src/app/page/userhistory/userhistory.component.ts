import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { item, itemdetail } from '../../shared/objects/item';
import { orderitem } from '../../shared/objects/OrderItem';
import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild, Output, EventEmitter, AfterViewInit } from '@angular/core';
// import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { OrderitemService } from 'app/shared/services/Orderitem.Service';
import { InventoryService } from 'app/shared/services/Inventory.Service';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { ItemdetailService } from '../../shared/services/itemdetail.service';
import { SettingService } from '../../shared/services/Setting.Service';
import { Projectnumber } from 'app/shared/objects/SettingObjects';
import { IMyDpOptions } from 'mydatepicker';
import { AuthenticationService } from '../../shared/services/Authenticate.Service';
import { User } from 'app/shared/objects/User';
import { UserhistoryService } from '../../shared/services/userhistory.service';
import { Userhistory } from '../../shared/objects/Userhistory';

@Component({
  selector: 'app-userhistory',
  providers: [OrderitemService, InventoryService, SettingService,  ItemdetailService, UserhistoryService],
  templateUrl: './userhistory.component.html',
  styleUrls: ['./userhistory.component.css']
})

export class UserhistoryComponent implements OnInit, OnChanges, AfterViewInit {

  //input get from parent
  //open single request modal
  @Input() isShoworderItem: boolean;
  @Input() orderitem: orderitem;

  //+ isnewrequests to open multiple requests modal
  @Input() multipleorders: orderitem[];
  @Input() editmultipleorderitems: boolean;


  //create a reference of input of #displayitem in inventory.component.html
  @ViewChild('displayorderitem', {static: false}) displayorderitem: ElementRef;
  @ViewChild('multiplerequestes', {static: false}) multiplerequestes: ElementRef;
  @ViewChild('displayrecieveitem', {static: false}) displayrecieveitem: ElementRef;

  //output an event to parent
  @Output() resetView = new EventEmitter();

  displayUserHistories: Userhistory[]=[];
  unreceiveamount: string;
  showunreceive: boolean;
  openreveitemmodal: any;
  editreceiveitem = false;
  //enable to edit the single order item in order item display
  enabledEditorderItem: boolean;//it will be passed to order item display component
  //if a new multiplerequests created
  isnewrequests = false;
  orderitems: orderitem[];
  viewColumn: boolean[];
  columnList: string[];
  //item invent
  invent: item[];
  orderitemfilterList: { name: string, cat: string, type: string, supplier: string, manufacturer: string, unit: string, unitsize: string }[];
  showUrgent: boolean = false;
  showrequesturgent = false;
  showapproveurgent = false;
  showorderurgent = false;

  //get the invent data for requests, orders and receives
  
  // requestiteminvent: orderitem[] = [];
  // approveiteminvent: orderitem[] = [];
  // orderiteminvent: orderitem[] = [];
  page: number
  // requestpage;
  // approvepage;
  // orderpage;
  // requestspages;
  // approvespages;
  // orderspages;
  selectOrders: orderitem[];
  selectrequestorderitems: orderitem[] = [];
  selectapprovedorderitems: orderitem[] = [];
  // selectorderorderitems: orderitem[] = [];
  receiveorderitem: orderitem;
  requestapprove = false;
  requestmark = false;
  approverequestitemnames = '';
  markrequestitemnames = '';
  receiveitem: itemdetail;
  unit: ConfigValue[];
  volUnit: ConfigValue[];
  massUnit: ConfigValue[];
  otherUnit: ConfigValue[];
  catalog: string;
  islockCat: boolean;
  locationtype: ConfigValue[];
  currentuser: User;
  receivedateoptions: IMyDpOptions = { dateFormat: 'mm/dd/yyyy' };
  receivedate: { date: { year: number, month: number, day: number } };
  projectnumbers: Projectnumber[];
  tabs: string[] = ['1 Week', '1 Month', '6 Months'];
  currentTab: string = '1 Week';
  isLoading: boolean = false;
  periodCount: number;
  searchKey: string;

  newlot=false;
  showinuse=false;
  showunit=false;
  dateformat: { date: { year: number, month: number, day: number } };
  showorderitemslide:boolean;

  multiplerequestsmodel:any;
  orderitemmodal:any;
  receiveitemmodal:any;

  constructor(private modalService: NgbModal, private orderitemService: OrderitemService, private inventService: InventoryService,
    private setting: SettingService, private itemdetailservice: ItemdetailService,
    private authenticationservice: AuthenticationService, private userhistoryservice: UserhistoryService) { }

  ngOnInit() {
    let getcurrentuser = this.authenticationservice.getCurrentUser()
    if (getcurrentuser !== undefined) {
      getcurrentuser.then(_ => {
        this.currentuser = User.fromJson(_);
      });
    }

    this.page = 1;
    this.currentTab = '1 Week';
    this.searchKey = '';
    this.selectOrders = [];
    // this.orderCount = 0;
    this.isLoading = true;

    this.userhistoryservice.getPeriodCount(this.currentTab).subscribe(c => {
      this.periodCount = c;
      this.loadPage();
    })
    this.viewColumn = [true, true, true, true, true];
    this.columnList = ['User Name',  'Component', 'Action', 'Time', 'Description'];

  }
  ngOnChanges(){}

  ngAfterViewInit() {
    // setTimeout(_ => {
    //   if (this.orderitem !== undefined && this.isShoworderItem) {
    //     this.open(this.displayorderitem, this.orderitem);
    //   }
    // });

    // setTimeout(_ => {
    //   if (this.multipleorders !== undefined && this.editmultipleorderitems) {
    //     this.isnewrequests = true;
    //     this.modalService.open(this.multiplerequestes, { backdrop: "static", size: 'lg' }).result
    //       .then((result) => { });
    //   }
    // });
  }

  toggleViewCol(index: number) {
    this.viewColumn[index] = !this.viewColumn[index];
  }

  changeTab(tab: string) {
    this.page = 1;
    this.displayUserHistories = [];
    this.currentTab = tab;
    this.searchKey = '';

    this.userhistoryservice.getPeriodCount(this.currentTab).subscribe(c => {
      this.periodCount = c;
      this.loadPage();
    })
  }

  loadPage() {
    this.displayUserHistories=[];
    this.isLoading = true;
    if(this.searchKey.trim() == ''){
      this.userhistoryservice.loadUserHistoryByPeriod(this.currentTab, this.page - 1).subscribe(userhistories => {
        this.displayUserHistories = userhistories;
        this.isLoading = false;
      })
    }
    else{
      // this.userhistoryservice.getNameAndPeriodCount(name, this.currentTab).subscribe(nameAndPeriodCount=>{
      //   this.periodCount=nameAndPeriodCount;
      //   if(nameAndPeriodCount>0){
          this.userhistoryservice.searchByNameAndPeriod(this.searchKey,this.currentTab, this.page-1).subscribe(userhistoryList=>{
            this.displayUserHistories=userhistoryList.slice();
            this.isLoading = false;
          })
      //   }
      //   else{
      //     this.isLoading = false;
      //   }
      // })
    }



  }

  searchByName(name: string) {
    if(this.searchKey == name){
      return;
    }
    else{
      if(name == undefined || name.trim() == ''){
        this.searchKey = '';
        this.page = 1;
        this.userhistoryservice.getPeriodCount(this.currentTab).subscribe(c => {
          this.periodCount = c;
          this.loadPage();
        }) 
      }
      else{
        this.searchKey = name;
        this.isLoading = true;
        this.displayUserHistories=[];
        this.userhistoryservice.getNameAndPeriodCount(this.searchKey, this.currentTab).subscribe(nameAndPeriodCount=>{
          this.periodCount=nameAndPeriodCount;
          if(nameAndPeriodCount>0){
            this.userhistoryservice.searchByNameAndPeriod(name,this.currentTab, this.page-1).subscribe(userhistoryList=>{
              this.displayUserHistories=userhistoryList.slice();
              this.isLoading = false;
            })
          }
          else{
            this.isLoading = false;
          }
        })
      }
    }


    // if(this.searchKey == undefined || this.searchKey == ''){
    //   this.page = 1;
    //   this.userhistoryservice.getPeriodCount(this.currentTab).subscribe(c => {
    //     this.periodCount = c;
    //     this.loadPage();
    //   }) 
    // }
    // else{
    //   this.orderitemService.searchOrder(this.currentTab, this.searchKey, this.page - 1).subscribe(result=>{
    //     this.displayOrders = result.slice();
    //     this.orderitemService.searchOrderCount(this.currentTab, this.searchKey,).subscribe(count=>{
    //       this.orderCount=count;
    //     })
    //     this.isLoading = false;
    //   })
    // }
  }
}
