import { orderitem } from '../../shared/objects/OrderItem';
import { Kit, Kitcomponent } from '../../shared/objects/Kit';
import { item, itemdetail, Itemtype } from '../../shared/objects/item';
import { Recipe, Ingredient, } from '../../shared/objects/Recipe';
import { Kittree,recipenode } from '../../shared/objects/Kittree';
import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import { KitService } from 'app/shared/services/Kit.Service';
import { RecipeService } from 'app/shared/services/Recipe.Service';
import { IngredientService } from 'app/shared/services/Ingredient.Service';
import { InventoryService } from 'app/shared/services/Inventory.Service';
import { OrderitemService } from 'app/shared/services/Orderitem.Service';
import { KitcomponentService } from '../../shared/services/Kitcomponent.Service';
import { ItemdetailService } from '../../shared/services/itemdetail.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Setting } from '../../shared/objects/Setting';
import { SettingService } from '../../shared/services/Setting.Service';
import { Observable } from 'rxjs';
import { IfObservable } from 'rxjs/observable/IfObservable';
import { find } from 'cfb/types';
import { UserhistoryService } from '../../shared/services/userhistory.service';
import { Userhistory } from '../../shared/objects/Userhistory';
import { User } from 'app/shared/objects/User';
import { AuthenticationService } from '../../shared/services/Authenticate.Service'

@Component({
  selector: 'app-kit',
  providers: [KitService, KitcomponentService, RecipeService, IngredientService, OrderitemService, SettingService, InventoryService, ItemdetailService, UserhistoryService],
  templateUrl: './kit.component.html',
  styleUrls: ['./kit.component.css'],
  host: {
    '(document:click)': 'onClick($event)',
  }
})

export class KitComponent implements OnInit, OnChanges {
  @ViewChild('kitDisplay', {static: false}) kitDisplay: ElementRef;
  @ViewChild('confirmation', {static: false}) confirmation: ElementRef;

  @Input() isShowKit: boolean;
  @Input() showkit: Kit;
  @Input() recipelist: recipenode[];

  @Output() cleanView = new EventEmitter();
  @Output() changeTab = new EventEmitter();
  @Output() sendout: EventEmitter<any> = new EventEmitter();
  @Output() sendorder = new EventEmitter();

  kitmodal: any;
  message: string;
  kits: Kit[] = [];
  isNewKit: boolean;
  enableEditKit: boolean;
  displayKit: Kit;
  displayKits: Kit[] = [];
  newkit: Kit;
  requestarray: orderitem[] = [];

  buildkit: Kit;
  buildkitcomponents: Kitcomponent[];
  buildkititems: { item: item, supplier: string, cat: string, requireamount: number, requireunit: string, stockamount: number, stockunit: string, selected: boolean }[] = [];
  unitnotsame: string[] = [];
  notsameunit: boolean;

  notfindindatabase: string[] = [];
  notindatabase: boolean;

  wronginput: boolean;
  wronginputinformaiton = [];
  databaseerror: boolean;
  databaseerrorinformation = [];

  kitlinks: Kitcomponent[] = [];
  rightkitlinks = [];

  recipes: Recipe[] = [];
  isNewRecipe: boolean;
  enableEditRecipe: boolean;
  displayRecipe: Recipe;
  displayRecipes: Recipe[];

  viewColumn: boolean[];
  columnList: string[];

  displaycomponents = false;
  cat: string;
  kitpacknumber: number;
  totalneed: number;

  ingredients: Ingredient[];
  ingredient: Ingredient;
  recipeout: Recipe;
  orderitemsinvent: orderitem[];

  page: number;
  searchKey: string;
  searchArea: string;
  count: number;
  nextCat: Setting;
  colCount: number;
  findcatalog: boolean;
  confirmationmessage: string = '';
  toconfirm: string = '';
  isLoading: boolean;
  //isDoneUpdate: boolean;

  kittree:Kittree;
  itemnodelist:recipenode[]=[];
  noselect:boolean;
  buildkitmodal:any;

  currentuser: User;
  

  constructor(private modalService: NgbModal, private kitService: KitService, private settingservice: SettingService,
    private kitcomponentService: KitcomponentService, private recipeService: RecipeService,private authenticationservice: AuthenticationService,
    private ingreService: IngredientService, private orderitemservce: OrderitemService, private inventservice: InventoryService, private itemdetailservice: ItemdetailService, private userhistoryservice: UserhistoryService) {
    this.settingservice.getSettingByPage('catalog').subscribe(config => {
      this.nextCat = config.find(x => x.type == 'CatPointer');
    });
  }

  onClick(event) {
    if (!this.isNewKit && event !== undefined && event.path !== undefined && event.path.find(x => x.className !== undefined && x.className.includes('kitname')) !== undefined) {
      let itempath = event.path.find(x => x.className.includes('kitname'));
      if(itempath == undefined || itempath.id == undefined || itempath.innerText == undefined) return;
      //this.showkitfunction(this.displayKits.find(x=>x.name.trim() == itempath.innerText.trim()))
      this.showkitfunction(this.displayKits.find(x=>x.dbid == itempath.id))
    }
    else if (event.path !== undefined && event.path.find(x => x.className !== undefined && x.className == 'slide') !== undefined || 
    event.path !== undefined && event.path.find(x => x.className !== undefined && x.className == 'recipeid') !== undefined)
      return;
    else if (event.path !== undefined && event.path.find(x => x.innerText !== undefined && x.innerText.trim() == 'New Kit') !== undefined) {
      this.newKit()
      return;
    }
    else {
      if (!this.isNewKit) {
        this.reset();
      }
    }
  }
  
  ngOnInit() {
    let getcurrentuser = this.authenticationservice.getCurrentUser()
    if (getcurrentuser !== undefined) {
      getcurrentuser.then(_ => {
        this.currentuser = User.fromJson(_);
      });
    }
    this.settingservice.getSettingByPage('catalog').subscribe(config => {
      this.nextCat = config.find(x => x.type == 'CatPointer');
    });

    this.searchArea = 'All'
    this.isLoading = true;
    this.searchKey = '';
    this.page = 1;
    if(this.showkit != undefined){
      this.changeDisplaykit();
    }
    else{
      this.kitService.count().subscribe(c => {
        this.count = c;
        this.loadPage();
      })
    }


    // this.kitService.loadKit().subscribe(kits => {
    //   kits.forEach(kit=>{
    //     kit.size=kit.size.replace('X','x');
    //     console.log(kit.size)
    //     this.kitService.updateKitInfo(kit);
    //   })
    // });

    this.viewColumn = [true, true, true, true, true, false, false, false, false,
      false, false, false, false, false, false, false, false, false];
    this.columnList = [
    'Cat #', 
    'Name',
    'Type',
    'Size',
    'Price(USD)',
    'Edit Status',
    'Description', 
    'Client Specific', 
    'Molecule (Innovator)', 
    'Biosimilar',  
    'Status', 
    'Entered By', 
    'Reviewed By', 
    'Review Comment', 
    'Approved By', 
    'Approve Comment', 
    'last Modify', 
    'Comment'];
  }

  ngOnChanges(change: SimpleChanges) {
    if (change.isShowKit && this.isShowKit && this.showkit !== undefined) {
      if (!change.showkit.isFirstChange()) {
        this.changeDisplaykit();
        this.count=1;
      }
    }
  }

  changeDisplaykit(){
    this.displayKits=[];
    this.displayKit = this.showkit;
    this.displayKits.push(this.displayKit);
    this.isLoading = false;
  }

  ngAfterViewInit() {
    setTimeout(_ => {
      if (this.showkit !== undefined && this.isShowKit) {
        this.displayKit = this.showkit;
        this.displayKits = this.kits.slice();
      }
    });
  }

  loadPage() {
    this.isLoading = true;
    this.searchKey='';
    this.searchArea='';
    this.kitService.loadKitWPage(this.page - 1).subscribe(kits => {
      this.kits = kits;
      this.displayKits = kits;
      this.isLoading = false;
    });
  }

  open(content, type?: string) {
    //first close enableeditkit first
    this.enableEditKit = false;

    //tell it is a new kit
    if (type == 'nk') {
      this.isNewKit = true;
      this.newKit();
      this.displayKit = this.newkit;
    }

    this.kitmodal = this.modalService.open(content, { backdrop: "static", size: 'lg' })
    this.kitmodal.result.then((result) => {
      if (result == 'closedel') {
        this.isShowKit = false;
      }

      if (this.isShowKit) {
        this.cleanView.emit()
      }
      this.enableEditKit = false;
      this.isNewKit = false;
      //this.displayKit = undefined;

      this.reset();
    }, () => {
      //close with X
      if (this.isShowKit) {
        this.cleanView.emit()
      }
      this.reset();
    });
  }

  openbuildkitmodal(modal){
    this.changeback();
    this.buildkitmodal=this.modalService.open(modal, { backdrop: "static", size: 'lg' });
  }

  showkitfunction(kit: Kit) {
    this.displayKit = kit;
    this.isShowKit = true;
  }

  searchKit(key: string) {
    this.searchKey = key;
    if (this.searchKey == undefined || this.searchKey == '') {
      this.kitService.count().subscribe(c => {
      this.count = c;
      this.page = 1;
      this.isLoading = true;
      this.loadPage();
      this.isLoading = false;
    })
    }
    else{
      this.kitService.search(this.searchKey, this.page - 1, this.searchArea).subscribe(result => {
        this.displayKits = result.slice();
        this.kitService.getSearchCount(this.searchKey, this.searchArea).subscribe(count=>{
          this.count=count;
        })
        //this.kits = result;
        this.isLoading = false;
      })
    }


  }

  filterDisplay(key: string) {
    if (key == undefined) return;
    if (this.displayKits == undefined) return;

    if (key.trim() == '') {
      this.displayKits = this.kits.slice();
      return;
    }

    this.kitService.loadKit().subscribe(kits => {
      this.kits = kits
      this.displayKits = [];

      this.kits.forEach((kit, index) => {
        if ((kit.cat !== undefined && kit.cat.toUpperCase().includes(key.toUpperCase())) ||
          (kit.name !== undefined && kit.name.toUpperCase().includes(key.toUpperCase()))) {
          this.displayKits.push(kit);
        }
      })
    });
  }

  reset() {
    this.enableEditRecipe = false;
    this.enableEditKit = false;
    this.displayKit = undefined;
    this.cleanView.emit();
    this.isNewKit = false;
    this.isShowKit = false;
  }

  newKit() {
    this.newkit = new Kit(-1, '', '', '', '', '', '', 'ADA', '2 X 96', 'Qualified with innovator', '', '', '', '', '', '', new Date(), null, null, '', '');
    this.enableEditKit = true;
    this.displayKit = this.newkit;
    this.isShowKit = true;
    this.isNewKit = true;
  }

  checknextnumber(kit:Kit){
    let catalognumber = kit.cat.split('-');
    let part2=catalognumber[2]
    let onlynumber = part2.replace(/\D*/g,'');
    let lasttwo = onlynumber.slice(-2);

    this.settingservice.getSettingByPage('catalog').subscribe(config => {
      let nextProductNum =config.find(x => x.type == 'kitPointer');
      let nextnumber = nextProductNum.value;
      if(parseInt(lasttwo)>=parseInt(nextnumber)){
        nextProductNum.value = (parseInt(lasttwo)+1).toString();
        this.settingservice.updateSetting(nextProductNum);
      }

    });
  }

  saveKitandsquence(k: { kit: Kit, updatedkitcomponents: Kitcomponent[] }) {
    if (k.kit == undefined) return;

    if (k.kit.dbid == -1) {
      //new kit
      this.kitService.addKit(k.kit).then(_ => {
        // let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Kit', 'Add a New Kit', 'Kit name: '+k.kit.name+', Kit cat#: '+k.kit.cat);
        // this.userhistoryservice.addUserHistory(userhistory);
        let id = _['dbid'];
        k.kit.dbid = id;
        this.checknextnumber(k.kit)
        // this.filterDisplay('');
        // this.nextCat.value = (k.newSeq + 1) + '';
        // this.settingservice.updateSetting(this.nextCat);
        if (k.updatedkitcomponents.length > 0) {
          k.updatedkitcomponents.forEach((component, index) => {
            if (component.dbid == -1) {
              component.kitid = id;
              this.kitcomponentService.addKitLink(component).then(_ => {
                let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Kit', 'Add a New Kitcomponent', 'Kit name: '+k.kit.name+', Kit cat#: '+k.kit.cat+', Kit Component Name: '+component.component);
                this.userhistoryservice.addUserHistory(userhistory);
                component.dbid = _['dbid'];
                if (index == k.updatedkitcomponents.length - 1) {
                  this.isShowKit = false;
                  this.resetdisplay(k.kit);
                  this.page=1;
                  this.loadPage();
                }
              });
            }
          })
        }
        else{
          this.isShowKit = false;
          this.resetdisplay(k.kit);
          this.page=1;
          this.loadPage();
        }
      })
    }
    else {
      //update kit
      let allpromises = [];
      this.kitService.updateKitInfo(k.kit).then(_ => {
        // let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Kit', 'Updated a Kit Information', 'Kit name: '+k.kit.name+', Kit cat#: '+k.kit.cat);
        // this.userhistoryservice.addUserHistory(userhistory);
        //after updated kit in database, then update in displaykits
        let index = this.displayKits.findIndex(x => x.dbid == k.kit.dbid);
        //if this kit exist in diplaykits
        if (index !== -1) {
          this.displayKits[index] = k.kit;
        }
        this.checknextnumber(k.kit)
        //find all the kit components in database
        this.kitcomponentService.loadKitLinkBykitid(k.kit.dbid).subscribe(kitlink => {
          let originalkitcomponents = kitlink;
          //compare original components with updated components to find any component is deleted
          originalkitcomponents.forEach(component => {
            if (k.updatedkitcomponents.findIndex(x => x.dbid == component.dbid) == -1) {
              allpromises.push(this.kitcomponentService.deleteKitLink(component));
            }
          });
          //compare updatedcomponents with originalcomponents
          k.updatedkitcomponents.forEach((component) => {
            let index = originalkitcomponents.findIndex(x => x.dbid == component.dbid);
            //find out the added 
            if (index == -1) {
              allpromises.push(this.kitcomponentService.addKitLink(component));
            }
            //if the component is in original components, but the informaiton is updated
            else if (JSON.stringify(component) != JSON.stringify(originalkitcomponents[index])) {
              allpromises.push(this.kitcomponentService.updateSingleKitLink(component));
            }
          });

          Promise.all(allpromises).then(_ => {
            this.isShowKit = false;
            this.resetdisplay(k.kit);
          })
        });
      });
    }
  }

  resetdisplay(k: Kit) {
    this.displayKit = k;
    this.enableEditKit = false;
    this.isNewKit = false;
  }

  saveeditstatus(k: Kit) {
    if (k == undefined) return;
    //update kit editstatus
    this.kitService.updateKitInfo(k).then(_ => {
      let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Kit', 'Updated a Kit Information', 'Kit name: '+k.name+', Kit cat#: '+k.cat);
      this.userhistoryservice.addUserHistory(userhistory);

      let index = this.displayKits.findIndex(x => x.dbid == k.dbid);
      if (index !== -1) {
        this.displayKits[index] = k;
      }
      this.displayKit = k;
      this.enableEditKit = false;
      this.isNewKit = false;
    });
  }

  convertDate(val: number) {
    return new Date(val).toString();
  }

  toggleViewCol(index: number) {
    this.viewColumn[index] = !this.viewColumn[index];
    this.colCount = 0;
    this.viewColumn.forEach(c => {
      if (c) this.colCount++;
    })
  }

  openconfirmation(message) {
    if (message == 'delete') {
      this.toconfirm = 'delete';
      this.confirmationmessage = 'Are you sure you want to delete it?';
      this.modalService.open(this.confirmation, { backdrop: "static" }).result.then(result => { });
    }
  }

  action() {
    if (this.toconfirm == 'delete') {
      this.deleteKit();
      this.isShowKit = false;
      //this.sopModal.close() ;
    }
  }

  deleteKit() {
    if (this.displayKit == undefined || this.displayKit.dbid == -1) { return; }
    let tempcomponents;
    let id = this.displayKit.dbid;

    let index = this.displayKits.findIndex(x => x == this.displayKit);
    let deleteKit = Kit.newKit(this.displayKit);
    if (index > -1) {
      //   //if the kit exist
      this.kitService.deleteKit(this.displayKit)
        .then(_ => {
          let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Kit', 'Delete a Kit', 'Kit name: '+deleteKit.name+', Kit cat#: '+deleteKit.cat);
          this.userhistoryservice.addUserHistory(userhistory);
          this.displayKits.splice(index, 1);
          this.kitcomponentService.loadKitLinkBykitid(id).subscribe(kitlink => {
            tempcomponents = kitlink;
            if (tempcomponents.length > 0) {
              let i;
              for (i = 0; i < tempcomponents.length; i++) {
                this.kitcomponentService.deleteKitLink(tempcomponents[i]);
              }
            }
            this.displayKit = undefined;
          });

        }).catch(err => {
          console.log(err);
        })
    }
  }

  // submit(cat:string, packnumber:number){
  //   this.changeback();
  //   if(cat==undefined || packnumber == undefined || packnumber==0){
  //     this.wronginput=true;
  //     this.wronginputinformaiton.push("You must enter a catalog # and a non-zero pack size!");
  //     setTimeout(() => this.wronginput = false, 5000);
  //     return;
  //   }
  
  //   cat = cat.toUpperCase();
  //   //get kit information through querying database by kit catalog #
  //   this.kitService.searchSingleKitByCat(cat).subscribe(
  //     kit=>{
  //     console.log(kit);
  //     console.log("User requires: "+packnumber+" plates")
  //     console.log("Kit size is: "+kit.size+" plates")
  //     this.findcatalog=true;
  //     let kitsize = kit.size.toUpperCase().split("X")[0].trim();
  //     console.log("kitsize is: "+kitsize);
  //     let kitratio=Number(packnumber)/Number(kitsize);
  //     console.log("The kit ratio is: "+kitratio);
  //     //find the kitcomponets of this kit through querying database by kit dbid
  //     this.kitcomponentService.searchKitComponentsByKitid(kit.dbid).subscribe(kitcomponents=>{
  //       if(kitcomponents.length<1){
  //         this.notindatabase=true;
  //         this.notfindindatabase.push("Kit "+cat+" does not have kitcomponents!");
  //         return;
  //       }
  //       this.buildkitcomponents=kitcomponents.slice();
  //       this.rightkitlinks = kitcomponents.slice();
  //       //check each kitcomponent
  //       let n=0;
  //       for(;n<this.buildkitcomponents.length;n++){
  //         //convert the unit to the smallest unit
  //         console.log("This component amount and unit are: "+ this.buildkitcomponents[n].amount+", "+this.buildkitcomponents[n].unit);
  //         let componentresult = this.convertunit(Number(this.buildkitcomponents[n].amount),this.buildkitcomponents[n].unit);
  //         console.log("Convert results are: "+ componentresult.amount+", "+componentresult.unit);
  //         //check if the kitcomponent is an item or a recipe,
  //         if(!this.buildkitcomponents[n].reagent||this.buildkitcomponents[n].reagent.trim()=='' ||this.buildkitcomponents[n].reagent.trim()=='N/A'){
  //           console.log("This is a recipe "+this.buildkitcomponents[n].recipename);
  
  //           let recipename = this.buildkitcomponents[n].recipename;
  //           let recipeid = this.buildkitcomponents[n].recipeid;
  //           //get this recipe
  //           console.log("get recipe "+recipeid);
  //           console.log("get recipe "+recipename);
  //           this.recipeService.searchSingleRecipeBySId(recipeid).subscribe(recipe=>{
  //           //this.recipeService.searchSingleRecipeByName(recipename).subscribe(recipe=>{
  //             console.log(recipe);
  //             console.log("Recipe "+recipename+" volumn and unit are: "+recipe.volumn+", "+recipe.unit);
  //             let reciperesult = this.convertunit(Number(recipe.volumn), recipe.unit);
  //             console.log("After convert, Recipe "+recipename+"  volumn and unit are: "+reciperesult.amount+", "+reciperesult.unit);
  //             let amountratio;
  //             //amountratio= kitratio * (componentresult.amount/reciperesult.amount);
  //             if(componentresult.unit.toUpperCase() == reciperesult.unit.toUpperCase()){
  //               amountratio= kitratio * (componentresult.amount/reciperesult.amount);
  //             }
  //             else{
  //               let notsameunit="Kit "+kit.name+" component "+ this.buildkitcomponents[n].component+" unit-"+componentresult.unit+" is different to the recipe "+ recipename+" unit-"+reciperesult.unit;
  //               this.unitnotsame.push(notsameunit);
  //               this.notsameunit=true;
  //             }
  //             console.log("Recipe ratio is: "+amountratio)
  //             this.ingreService.searchIngredientsByRecipeid(Number(recipe.dbid)).subscribe(ingredients=>{
  //               console.log(ingredients);
  //               if(ingredients.length<1){
  //                 this.notindatabase=true;
  //                 this.notfindindatabase.push("Recipe "+recipename+" does not have ingredients!");
  //               }
  //               ingredients.forEach(x=>{this.getitems(x, amountratio)});
  //             });
  //           },
  //           err => {
  //             console.log(err);
  //             let error = err.toString().trim();
  //             if(error.includes("SyntaxError: Unexpected end of JSON input")){
  //               this.notindatabase=true; 
                
  //               this.notfindindatabase.push("Recipe "+recipename+" does not exist in database!")
  //             }
  //             else{
  //               this.databaseerror=true;
  //               this.databaseerrorinformation.push("Database Error: you may have more than one recipe: "+recipename+" with the same name!");
  //             }
  //             return;
  //           })
  //         }
  //         else{
  //           //find this item, convert unit to the smallest unit, and convert the amount according to the unit and amount ratio, push into buildkititems lists
  //           console.log("This is an item"+this.buildkitcomponents[n].reagent);
  //           // if(componentresult.unit=='plate'){
  //           //   componentresult.unit='plate(s)';
  //           // }
  
  //           let itemwholename = this.buildkitcomponents[n].reagent;
  //           let itemcat=itemwholename.split("Cat#")[1].toString().trim();
  //           this.inventservice.searchSingleInventoryByCat(itemcat).subscribe(
  //             item=>{
  //             console.log(item);
  //             // if(item.type=='Kit'){
  //             //   componentresult.unit='box(es)'
  //             // }
  //             let stockunit='';
  //             if(item.unit){
  //               stockunit= item.unit;
  //             }
  //             let unitsize=1;
  //             if(item.unitsize && item.unitsize.trim()!='' && item.unitsize.trim()!='0'){
  //               unitsize = parseFloat(item.unitsize.trim());
  //             }
  //             this.itemdetailservice.searchItemdetailsByItemdbid(item.dbid).subscribe(itemdetails=>{
  //               let stockamount=0;
  //               itemdetails.forEach(itemdetail=>{
  //                 if(itemdetail.amount && !isNaN(Number(itemdetail.amount))){
  //                     stockamount = stockamount + Number(itemdetail.amount) * unitsize;
  //                 }
  //               });
                
  //               console.log("Item stock amount and unit are: "+stockamount+", "+stockunit);
  //               let stockresult = this.convertunit(stockamount, stockunit);
  //               console.log("After convert, Item amount and unit are: "+stockresult.amount+", "+stockresult.unit);
  //               if(componentresult.unit.toUpperCase() != stockresult.unit.toUpperCase()){
  //                 let notsameunit="Kit component "+item.name+" require unit-"+componentresult.unit+" is different to the item stock unit-"+ stockresult.unit;
  //                 this.unitnotsame.push(notsameunit);
  //                 this.notsameunit=true;
  //               }
  //               let itemindex = this.buildkititems.findIndex(x=>x.item.cat == item.cat);
  //               //detect catalog # for avoiding adding same kit item again
  //               if(itemindex == -1){
  //                 console.log("add list " +itemwholename);
  //                 console.log("amount is " +componentresult.amount);
  //                 console.log("unit is " +componentresult.unit);
  //                 console.log("amountratio is " +kitratio);
  //                 this.buildkititems.push({item:item, supplier:item.supplier, cat:item.cat, requireamount:componentresult.amount*kitratio, requireunit:componentresult.unit, stockamount:stockresult.amount, stockunit:stockresult.unit, selected:false} );
  //               }
  //             });
  //           },          
  //           err => {
  //             let error = err.toString().trim();
          
  //             if(error.includes("SyntaxError: Unexpected end of JSON input")){
  //               this.notindatabase=true; 
  //               this.notfindindatabase.push("Item: "+itemwholename+" does not exist in database!")
  //             }
  //             else{
  //               this.databaseerror=true;
  //               this.databaseerrorinformation.push("Database Error: you may have more than one item:"+ itemwholename+ " with the same name!");
  //             }
  //             return;
  //           });
  //         }
  //       }
  //     });
  //   },
  //   err => {
  //     let error = err.toString().trim();
  
  //     if(error.includes("SyntaxError: Unexpected end of JSON input")){
  //       this.notindatabase=true; 
  //       this.notfindindatabase.push("The kit catalog #: "+ cat+" does not exist in database!")
  //     }
  //     else{
  //       this.databaseerror=true;
  //       this.databaseerrorinformation.push("Database Error: you may have more than one kit with the same catalog #!");
  //     }
  //     return;
  //   });


  //     console.log("I am waiting for all the observable completed.");
  //     if(this.notindatabase==true){
  //       setTimeout(() => this.notindatabase = false, 5000);
  //     }
  //     if(this.databaseerror==true){
  //       setTimeout(() => this.databaseerror = false, 5000);
  //     }
  //     if(this.notindatabase==true){
  //       setTimeout(() => this.notindatabase = false, 5000);
  //     }
  //     if(this.notsameunit==true){
  //       setTimeout(() => this.notsameunit = false, 5000);
  //     }

  // }
  
    // getitems(ingredient:Ingredient,ratio:number){
    //   console.log("Ingredient amount and unit are: "+ingredient.reqquan+", "+ingredient.requnit);
    //   let ingredientresult = this.convertunit(Number(ingredient.reqquan), ingredient.requnit);
    //   console.log("After convert, Ingredient amount and unit are: "+ingredientresult.amount+", "+ingredientresult.unit);
    //   //if this ingredient is an recipe
    //   if(!ingredient.itemname){
    //     console.log("This ingredient"+ingredient.recipename+" is a recipe");
    //     let recipename = ingredient.recipename;
    //     let recipeid = ingredient.recipeid;
    //     console.log("ingredient recipe dbid is: "+recipeid);
    //     //get this recipe
    //     console.log("get recipe "+recipename);
    //     //this.recipeService.searchSingleRecipeByDId(recipeid).subscribe(recipe=>{
    //     this.recipeService.searchSingleRecipeByName(recipename).subscribe(recipe=>{
    //       console.log(recipe);
    //       console.log("in getitems, Recipe volumn and unit are: "+recipe.volumn+", "+recipe.unit);
    //       let recipeid = recipe.dbid;
    //       let reciperesult = this.convertunit(Number(recipe.volumn), recipe.unit);
    //       console.log("in getitems, after convert, Recipe volumn and unit are: "+reciperesult.amount+", "+reciperesult.unit);
    //       //ratio=ratio*(ingredientresult.amount/reciperesult.amount);
    //       if(ingredientresult.unit.toUpperCase() != reciperesult.unit.toUpperCase()){
    //         let notsameunit="Ingredient "+recipe.name+" unit-"+ingredientresult.unit+" is different to the recipe unit-"+reciperesult.unit;
    //         this.unitnotsame.push(notsameunit);
    //         this.notsameunit=true;
    //       }
    //       else{
    //         ratio=ratio*(ingredientresult.amount/reciperesult.amount);
    //         console.log("Ratio is: "+ratio);
    //         this.ingreService.searchIngredientsByRecipeid(Number(recipeid)).subscribe(ingredients=>{
    //           if(ingredients.length<1){
    //             this.notindatabase=true;
    //             this.notfindindatabase.push("Recipe "+recipename+" does not have ingredients!");
    //           }
    //           ingredients.forEach(x=>{this.getitems(x, ratio)});
    //         });
    //       }
    //     },          
    //     err => {
    //       let error = err.toString().trim();
    //       if(error.includes("SyntaxError: Unexpected end of JSON input")){
    //         this.notindatabase=true; 
    //         this.notfindindatabase.push("Recipe "+recipename+" does not exist in database!")
    //       }
    //       else{
    //         this.databaseerror=true;
    //         this.databaseerrorinformation.push("Database Error: you may have more than one recipe: "+recipename+" with the same name!");
    //       }
    //       return;
    //     });
    //   }
    //   else{
    //     //this ingredient is an item
    //     if(ingredientresult.unit=='plate'){
    //       ingredientresult.unit='plate(s)';
    //     }
    //     let itemname = ingredient.itemname.trim();
    //     console.log("in get items, This ingredient"+itemname+" is an item");
    //     this.inventservice.searchSingleInventoryByName(itemname).subscribe(item=>{
    //       console.log(item);
    //       let stockunit;
    //       if(item.unit){
    //         stockunit= item.unit;
    //       }
    //       let unitsize=1;
    //       if(item.unitsize && item.unitsize.trim()!='' && item.unitsize.trim()!='0'){
    //         unitsize = parseFloat(item.unitsize.trim());
    //       }
    //       this.itemdetailservice.searchItemdetailsByItemdbid(item.dbid).subscribe(itemdetails=>{
    //         let stockamount=0;
    //         itemdetails.forEach(itemdetail=>{
    //           if(itemdetail.amount && !isNaN(Number(itemdetail.amount))){
    //               stockamount = stockamount + Number(itemdetail.amount) * unitsize;
    //           }
    //         });
    //         console.log("in getitmes, Item stock amount and unit are: "+stockamount+", "+stockunit);
    //         let stockresult = this.convertunit(stockamount, stockunit);
    //         console.log("After convert, in getitmes,Item amount and unit are: "+stockresult.amount+", "+stockresult.unit);
    //         if(ingredientresult.unit.toUpperCase() != stockresult.unit.toUpperCase()){
    //           let notsameunit="Ingredient "+item.name+" require unit-"+ingredientresult.unit+" is different to the item stock unit-"+ stockresult.unit;
    //           this.unitnotsame.push(notsameunit);
    //           this.notsameunit=true;
    //         }
    //         let itemindex = this.buildkititems.findIndex(x=>x.item.name == item.name);
    //         if(itemindex == -1){
    //           this.buildkititems.push({item:item, supplier:item.supplier, cat:item.cat, requireamount:ingredientresult.amount*ratio, requireunit:ingredientresult.unit, stockamount:stockresult.amount, stockunit:stockresult.unit, selected:false});
    //         }
    //         else{
    //           this.buildkititems[itemindex].requireamount+=ingredientresult.amount*ratio;
    //         }
    //       });
    //     },          
    //     err => {
    //       let error = err.toString().trim();
    //       if(error.includes("SyntaxError: Unexpected end of JSON input")){
    //         this.notindatabase=true; 
    //         this.notfindindatabase.push("Item: "+itemname+" does not exist in database!")
    //       }
    //       else{
    //         this.databaseerror=true;
    //         this.databaseerrorinformation.push("Database Error: you may have more than one item: "+ itemname+" with the same name!");
    //       }
    //       return;
    //     });
    //   }
    // }

  convertunit(amount:number, unit:string){
    unit=unit.trim();
    if(unit=='mL'){
      amount = Math.pow(10,3)*amount;
      unit = 'μL';
    }
    if(unit=='L'){
      amount = Math.pow(10,6)*amount;
      unit = 'μL';
    }

    if(unit=='pg'){
      amount = Math.pow(10,-6)*amount;
      unit = 'μg';
    }
    if(unit=='ng'){
      amount = Math.pow(10,-3)*amount;
      unit = 'μg';
    }
    if(unit=='mg'){
      amount = Math.pow(10,3)*amount;
      unit = 'μg';
    }
    if(unit=='g'){
      amount = Math.pow(10,6)*amount;
      unit = 'μg';
    }
    if(unit=='kg'){
      amount = Math.pow(10,9)*amount;
      unit = 'ug';
    }

    return {amount:amount, unit:unit};
  }

  changeback() {
    this.itemnodelist=[];
    this.findcatalog = undefined;
    this.buildkitcomponents=[];
    this.buildkititems=[];   
    this.displaycomponents = false;
    this.rightkitlinks = [];
    this.unitnotsame=[];
    this.notsameunit=false;
    this.notfindindatabase=[];
    this.notindatabase=false;
    this.wronginput=false;
    this.wronginputinformaiton=[];
    this.databaseerror=false;
    this.databaseerrorinformation=[];
    this.kittree=undefined;
    this.requestarray=[];
    this.noselect=false;
    this.kitpacknumber=undefined;
  }

  
  buildkittree(cat:string, packnumber:number){
    //this.changeback();
    if(cat==undefined || packnumber == undefined || packnumber==0){
      this.wronginput=true;
      this.wronginputinformaiton.push("You must enter a catalog # and a non-zero pack size!");
      setTimeout(() => this.wronginput = false, 5000);
      return;
    }

    this.kittree=new Kittree(null);

    let root = new recipenode(cat,null, null,null,null,null,null, packnumber,null,null,null,'Yes', 'No', []);
    this.kittree.root =root;

    cat = cat.toUpperCase();
    //get kit information through querying database by kit catalog #
    this.kitService.searchSingleKitByCat(cat).subscribe(
      kit=>{
      this.findcatalog=true;
      let kitsize = kit.size.toUpperCase().split("X")[0].trim();
      let kitratio=Number(packnumber)/Number(kitsize);
      //find the kitcomponets of this kit through querying database by kit dbid
      this.kitcomponentService.searchKitComponentsByKitid(kit.dbid).subscribe(kitcomponents=>{
        if(kitcomponents.length<1){
          this.notindatabase=true;
          this.notfindindatabase.push("Kit "+cat+" does not have kitcomponents!");
          return;
        }
        this.buildkitcomponents=kitcomponents.slice();
        // this.rightkitlinks = kitcomponents.slice();
        //check each kitcomponent
        this.buildkitcomponents.sort((a,b)=>{return Number(a.componentid[a.componentid.length-1])-Number(b.componentid[a.componentid.length-1])     })
        let n=0;
        for(;n<this.buildkitcomponents.length;n++){
          let componentnode=new recipenode(null, this.buildkitcomponents[n].component,null, null, null,null,null, null,null,null,null, 'Yes', 'No',  []);
          if(this.buildkitcomponents[n].partnumber && this.buildkitcomponents[n].partnumber.trim()!=''){
            componentnode.partnumber=this.buildkitcomponents[n].partnumber;
          }
          root.children.push(componentnode);
          //convert the unit to the smallest unit
          let componentresult = this.convertunit(Number(this.buildkitcomponents[n].amount) * kitratio, this.buildkitcomponents[n].unit);
          // if(kitratio<1 && componentresult.amount%(1/kitratio)!=0){
          //   componentnode.requireamount=parseFloat(componentresult.amount.toFixed(2));
          // }
          // else{
          //   componentnode.requireamount=componentresult.amount;
          // }
          componentnode.requireamount=componentresult.amount;
          componentnode.requireunit=componentresult.unit;
          //check if the kitcomponent is an item or a recipe,
          if(!this.buildkitcomponents[n].reagent||this.buildkitcomponents[n].reagent.trim()=='' ||this.buildkitcomponents[n].reagent.trim()=='N/A'){
            let recipename = this.buildkitcomponents[n].recipename;
            let recipeid = this.buildkitcomponents[n].recipeid;
            componentnode.recipename=recipename;
            //get this recipe
            this.recipeService.searchSingleRecipeBySId(recipeid).subscribe(recipe=>{
            //this.recipeService.searchSingleRecipeByName(recipename).subscribe(recipe=>{
              let reciperesult = this.convertunit(Number(recipe.volumn), recipe.unit);
              let amountratio;
              //amountratio= kitratio * (componentresult.amount/reciperesult.amount);
              if(componentresult.unit.toUpperCase() == reciperesult.unit.toUpperCase()){
                amountratio= componentnode.requireamount/reciperesult.amount;
              }
              else{
                let notsameunit="Kit "+kit.name+" component "+ this.buildkitcomponents[n].component+" unit-"+componentresult.unit+" is different to the recipe "+ recipename+" unit-"+reciperesult.unit;
                this.unitnotsame.push(notsameunit);
                this.notsameunit=true;
              }
              this.ingreService.searchIngredientsByRecipeid(Number(recipe.dbid)).subscribe(ingredients=>{
                if(ingredients.length<1){
                  this.notindatabase=true;
                  this.notfindindatabase.push("Recipe "+recipename+" does not have ingredients!");
                }
                ingredients.forEach(ingredient=>{
                  let ingredientnode=new recipenode(null,null,null,null,null,null,null,null,null,null,null,'Yes', 'No', [])
                  let ingredientresult = this.convertunit(Number(ingredient.reqquan)*amountratio, ingredient.requnit);
                  ingredientnode.requireamount=ingredientresult.amount;
                  ingredientnode.requireunit=ingredientresult.unit;
                  componentnode.children.push(ingredientnode)
                  this.getitems(ingredient, ingredientnode)});
              });
            },
            err => {
              let error = err.toString().trim();
              if(error.includes("SyntaxError: Unexpected end of JSON input")){
                this.notindatabase=true; 
                
                this.notfindindatabase.push("Recipe "+recipename+" does not exist in database!")
              }
              else{
                this.databaseerror=true;
                this.databaseerrorinformation.push("Database Error: you may have more than one recipe: "+recipename+" with the same name!");
              }
              return;
            })
           }
          else{
            //find this item, convert unit to the smallest unit, and convert the amount according to the unit and amount ratio, push into buildkititems lists
            componentnode.enough='Yes'; 
            componentnode.selected='No'; 
            let itemwholename = this.buildkitcomponents[n].reagent;
            let itemcat=itemwholename.split("Cat#")[1].toString().trim();
            componentnode.itemcat=itemcat;
            if( componentnode.requireamount.toString().includes('.') && componentnode.requireamount.toString().split('.')[1].length>3 ){
              componentnode.requireamount=Math.ceil(componentnode.requireamount * 100) / 100
              //parseFloat((componentnode.requireamount).toFixed(2));
              
            }
            this.inventservice.searchSingleInventoryByCat(itemcat).subscribe(
              item=>{
              componentnode.itemname=item.name;
              componentnode.supplier=item.supplier;
              let stockunit='';
              if(item.unit){
                stockunit= item.unit;
              }
              let unitsize=1;
              if(item.unitsize && item.unitsize.trim()!='' && item.unitsize.trim()!='0'){
                unitsize = parseFloat(item.unitsize.trim());
              }
              this.itemdetailservice.searchItemdetailsByItemdbid(item.dbid).subscribe(itemdetails=>{
                let stockamount=0;
                itemdetails.forEach(itemdetail=>{
                  if(itemdetail.amount && !isNaN(Number(itemdetail.amount))){
                      stockamount = stockamount + Number(itemdetail.amount) * unitsize;
                  }
                });
                
                let stockresult = this.convertunit(stockamount, stockunit);
                componentnode.stockamount=stockresult.amount;
                componentnode.stockunit=stockresult.unit;
                if(componentresult.unit.toUpperCase() != stockresult.unit.toUpperCase()){
                  let notsameunit="Kit component "+item.name+" require unit-"+componentresult.unit+" is different to the item stock unit-"+ stockresult.unit;
                  this.unitnotsame.push(notsameunit);
                  this.notsameunit=true;
                }

                if(componentnode.requireamount>componentnode.stockamount){
                  componentnode.enough='No';
                }
                else{
                  componentnode.enough='Yes';
                }

                this.itemnodelist.push(componentnode);
                // let itemindex = this.itemnodelist.findIndex(x=>x.itemname == componentnode.itemname && x.itemcat == componentnode.itemcat && x.supplier == componentnode.supplier);
                // //detect catalog # for avoiding adding same kit item again
                // if(itemindex == -1){
                //   console.log("add list " +itemwholename);
                //   console.log("amount is " +componentresult.amount);
                //   console.log("unit is " +componentresult.unit);
                //   console.log("amountratio is " +kitratio);
                //   this.itemnodelist.push(componentnode);
                // }
              });
            },          
            err => {
              let error = err.toString().trim();
          
              if(error.includes("SyntaxError: Unexpected end of JSON input")){
                this.notindatabase=true; 
                this.notfindindatabase.push("Item: "+itemwholename+" does not exist in database!")
              }
              else{
                this.databaseerror=true;
                this.databaseerrorinformation.push("Database Error: you may have more than one item:"+ itemwholename+ " with the same name!");
              }
              return;
            });
          }
        }
      });
    },
    err => {
      let error = err.toString().trim();
  
      if(error.includes("SyntaxError: Unexpected end of JSON input")){
        this.notindatabase=true; 
        this.notfindindatabase.push("The kit catalog #: "+ cat+" does not exist in database!")
      }
      else{
        this.databaseerror=true;
        this.databaseerrorinformation.push("Database Error: you may have more than one kit with the same catalog #!");
      }
      return;
    });


      if(this.notindatabase==true){
        setTimeout(() => this.notindatabase = false, 5000);
      }
      if(this.databaseerror==true){
        setTimeout(() => this.databaseerror = false, 5000);
      }
      if(this.notindatabase==true){
        setTimeout(() => this.notindatabase = false, 5000);
      }
      if(this.notsameunit==true){
        setTimeout(() => this.notsameunit = false, 5000);
      }

  }


      getitems(ingredient:Ingredient,ingredientnode:recipenode){

      //if this ingredient is an recipe
      if(!ingredient.itemname){
        let recipename = ingredient.recipename;
        ingredientnode.recipename=recipename;
        let recipeid = ingredient.recipeid;
        //get this recipe
        //this.recipeService.searchSingleRecipeByDId(recipeid).subscribe(recipe=>{
        this.recipeService.searchSingleRecipeByName(recipename).subscribe(recipe=>{
          let recipeid = recipe.dbid;
          let reciperesult = this.convertunit(Number(recipe.volumn), recipe.unit);

          if(ingredientnode.requireunit.toUpperCase() != reciperesult.unit.toUpperCase()){
            let notsameunit="Ingredient "+recipe.name+" unit-"+ingredientnode.requireunit+" is different to the recipe unit-"+reciperesult.unit;
            this.unitnotsame.push(notsameunit);
            this.notsameunit=true;
          }
          else{
            let ratio=ingredientnode.requireamount/reciperesult.amount;
            this.ingreService.searchIngredientsByRecipeid(Number(recipeid)).subscribe(ingredients=>{
              if(ingredients.length<1){
                this.notindatabase=true;
                this.notfindindatabase.push("Recipe "+recipename+" does not have ingredients!");
              }
              else{
                ingredients.forEach(cingredient=>{
                  let cingredientnode=new recipenode(null,null,null,null,null,null,null, null,null,null,null,'Yes', 'No',  [])
                  //let cingredientresult = this.convertunit(Number(cingredient.reqquan)*ratio, cingredient.requnit);
                  let cingredientresult = this.convertunit(Number(cingredient.reqquan)*ratio, cingredient.requnit);
                  //cingredientnode.requireamount=parseFloat(cingredientresult.amount.toFixed(2));
                  cingredientnode.requireamount=cingredientresult.amount;
                  cingredientnode.requireunit=cingredientresult.unit;
                  ingredientnode.children.push(cingredientnode)
                  this.getitems(cingredient, cingredientnode)
                });
              }

            });
          }
        },          
        err => {
          let error = err.toString().trim();
          if(error.includes("SyntaxError: Unexpected end of JSON input")){
            this.notindatabase=true; 
            this.notfindindatabase.push("Recipe "+recipename+" does not exist in database!")
          }
          else{
            this.databaseerror=true;
            this.databaseerrorinformation.push("Database Error: you may have more than one recipe: "+recipename+" with the same name!");
          }
          return;
        });
      }
      else{
        //this ingredient is an item
        ingredientnode.enough='Yes';
        ingredientnode.selected='No';
        let itemname = ingredient.itemname.trim();
        ingredientnode.itemname=itemname;
        ingredientnode.supplier=ingredient.vendor;
        ingredientnode.itemcat=ingredient.cat;
        if( ingredientnode.requireamount.toString().includes('.') && ingredientnode.requireamount.toString().split('.')[1].length>3 ){
          ingredientnode.requireamount=Math.ceil(ingredientnode.requireamount * 100) / 100
          //ingredientnode.requireamount=parseFloat((ingredientnode.requireamount).toFixed(2));
        }

        this.inventservice.searchSingleInventoryByName(itemname).subscribe(item=>{
          let stockunit;
          if(item.unit){
            stockunit= item.unit;
          }
          let unitsize=1;
          if(item.unitsize && item.unitsize.trim()!='' && item.unitsize.trim()!='0'){
            unitsize = parseFloat(item.unitsize.trim());
          }
          this.itemdetailservice.searchItemdetailsByItemdbid(item.dbid).subscribe(itemdetails=>{

            let stockamount=0;
            itemdetails.forEach(itemdetail=>{
              if(itemdetail.amount && !isNaN(Number(itemdetail.amount))){
                  stockamount = stockamount + Number(itemdetail.amount) * unitsize;
              }
            });
            let stockresult = this.convertunit(stockamount, stockunit);
            ingredientnode.stockamount=stockresult.amount;
            ingredientnode.stockunit=stockresult.unit;
            if(ingredientnode.requireunit.toUpperCase() != stockresult.unit.toUpperCase()){
              let notsameunit="Ingredient "+item.name+" require unit-"+ingredientnode.requireunit+" is different to the item stock unit-"+ stockresult.unit;
              this.unitnotsame.push(notsameunit);
              this.notsameunit=true;
            }

            if(ingredientnode.requireamount>ingredientnode.stockamount){
              ingredientnode.enough='No';
            }
            else{
              ingredientnode.enough='Yes';
            }

            this.itemnodelist.push(ingredientnode);
            let a;
            let amount=ingredientnode.requireamount;
            let instockamount=ingredientnode.stockamount;
            let itemindexlist:number[]=[];
            itemindexlist.push(this.itemnodelist.length-1)
            for(a=0;a<this.itemnodelist.length-1;a++){
              if(ingredientnode.itemname==this.itemnodelist[a].itemname && ingredientnode.itemcat==this.itemnodelist[a].itemcat && ingredientnode.supplier==this.itemnodelist[a].supplier && ingredientnode.partnumber==null && this.itemnodelist[a].partnumber==null){
                amount=amount+this.itemnodelist[a].requireamount;
                itemindexlist.push(a);
              }
            }
            if(amount>instockamount && itemindexlist.length>1){
              itemindexlist.forEach(index=>{
                this.itemnodelist[index].enough='No';
              })
            }

            // let itemindex = this.buildkititems.findIndex(x=>x.item.name == item.name);
            // if(itemindex == -1){
            //   this.buildkititems.push({item:item, supplier:item.supplier, cat:item.cat, requireamount:ingredientresult.amount*ratio, requireunit:ingredientresult.unit, stockamount:stockresult.amount, stockunit:stockresult.unit, selected:false});
            // }
            // else{
            //   this.buildkititems[itemindex].requireamount+=ingredientresult.amount*ratio;
            // }
          });
        },          
        err => {
          let error = err.toString().trim();
          if(error.includes("SyntaxError: Unexpected end of JSON input")){
            this.notindatabase=true; 
            this.notfindindatabase.push("Item: "+itemname+" does not exist in database!")
          }
          else{
            this.databaseerror=true;
            this.databaseerrorinformation.push("Database Error: you may have more than one item: "+ itemname+" with the same name!");
          }
          return;
        });
      }
    }

    select(node:recipenode){
      if(node.selected=='No'){
        node.selected='Yes';
        // if(this.nodeselectlist.find(x=>x.itemname==node.itemname && x.itemcat==node.itemcat && x.supplier==node.supplier)==undefined){
        //   this.nodeselectlist.push(node);
        // }
      }
      else{
        node.selected='No';
        // let index=this.nodeselectlist.findIndex(x=>x.itemname==node.itemname && x.itemcat==node.itemcat && x.supplier==node.supplier);
        // if(index!=-1){
        //   this.nodeselectlist.splice(index,1);
        // }
      }
    }

  sendrecipe(recipeinfo) {
    let recipename=recipeinfo.name;
    let recipesomruid=recipeinfo.somruid;
    this.recipeService.searchSingleRecipeBySId(recipesomruid).subscribe(recipe=>{
      this.sendout.emit(['SOP', recipe]);
    },
    err=>{console.log(err);
    })
  }

  // selectorderitem(i) {
  //   if (this.buildkititems[i].selected == false) {
  //     this.buildkititems[i].selected = true;
  //     let neworderitem = new orderitem(-1, this.buildkititems[i].item.cat, this.buildkititems[i].item.name, this.buildkititems[i].item.type, this.buildkititems[i].item.supplier, this.buildkititems[i].item.manufacturer, '', '', this.buildkititems[i].item.unit, this.buildkititems[i].item.unitsize, this.buildkititems[i].item.unitprice, '', '', '', '', '', '', '', '', false, '')
  //     this.requestarray.push(neworderitem);
      
  //   }
  //   else {
  //     this.buildkititems[i].selected = false;
  //     let requestindex = this.requestarray.findIndex(x=>x.cat == this.buildkititems[i].item.cat);
  //     this.requestarray.splice(requestindex, 1)
  //   }
  // }

  // selectallorderitem() {
  //   let i;
  //   for (i = 0; i < this.buildkititems.length; i++) {
  //     if (!this.buildkititems[i].selected) {
  //       this.buildkititems[i].selected = true;
  //       let neworderitem = new orderitem(-1, this.buildkititems[i].item.cat, this.buildkititems[i].item.name, this.buildkititems[i].item.type, this.buildkititems[i].item.supplier, this.buildkititems[i].item.manufacturer, '', '', this.buildkititems[i].item.unit, this.buildkititems[i].item.unitsize, this.buildkititems[i].item.unitprice, '', '', '', '', '', '', '', '', false, '')
  //       this.requestarray.push(neworderitem);
  //     }
  //   }
  // }

  request() {
    this.requestarray=[];
    let selectnumber=0;
    let nodeselectlist:recipenode[]=[];
    this.itemnodelist.forEach(node=>{
      if(node.selected=='Yes' && nodeselectlist.find(x=>x.itemname==node.itemname && x.itemname==node.itemname && x.supplier==node.supplier)==undefined){
        nodeselectlist.push(node);
      }
    })

    if(nodeselectlist.length<1){
      this.noselect=true;
      return;
    }
    else{
      this.noselect=false;
      let index=0;
      for(;index<nodeselectlist.length;index++){
        this.inventservice.searchSingleInventoryByCat(nodeselectlist[index].itemcat).subscribe(item=>{
          let neworderitem = new orderitem(-1,item.cat,item.category,item.supplier, item.name,item.type,item.supplier, item.manufacturer,'',
            '',item.unit,item.unitsize,item.unitprice,'','','', '', '','',new Date(), null, null, null, null, null, false,'','','No')
          this.requestarray.push(neworderitem);
          if(index==nodeselectlist.length){
            this.buildkitmodal.close();
            this.sendorder.emit(this.requestarray);
          }
        });
      }
    }
  }

  openrecipe(recipeid) {
    let i;
    for (i = 0; i < this.recipes.length; i++) {
      if (this.recipes[i].somruid == recipeid) {
        this.recipeout = this.recipes[i];
        break;
      }
    }
    this.kitmodal.close();
    this.sendout.emit(['SOP', this.recipeout]);
  }
}

