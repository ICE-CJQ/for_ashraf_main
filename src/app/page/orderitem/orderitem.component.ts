import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { item, itemdetail } from '../../shared/objects/item';
import { orderitem, emailDetail} from '../../shared/objects/OrderItem';
import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild, Output, EventEmitter,AfterViewInit } from '@angular/core';
// import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { OrderitemService } from 'app/shared/services/Orderitem.Service';
import { InventoryService } from 'app/shared/services/Inventory.Service';
import { ConfigValue, Setting } from '../../shared/objects/Setting';
import { ItemdetailService } from '../../shared/services/itemdetail.service';
import { SettingService } from '../../shared/services/Setting.Service';
import { Projectnumber } from 'app/shared/objects/SettingObjects';
import { IMyDpOptions } from 'mydatepicker';
import { AuthenticationService } from '../../shared/services/Authenticate.Service';
import { User } from 'app/shared/objects/User';
import { UserhistoryService } from '../../shared/services/userhistory.service';
import { Barcode } from '../../shared/objects/Barcode';
import { BarcodeService } from '../../shared/services/barcode.service';
import { Userhistory } from '../../shared/objects/Userhistory';

@Component({
  selector: 'app-orderitem',
  providers: [OrderitemService, InventoryService, SettingService,  ItemdetailService, UserhistoryService, BarcodeService],
  templateUrl: './orderitem.component.html',
  styleUrls: ['./orderitem.component.css']
})
export class OrderitemComponent implements OnInit, OnChanges, AfterViewInit {

  //input get from parent
  //open single request modal
  @Input() isShoworderItem: boolean;
  @Input() orderitem: orderitem;

  //+ isnewrequests to open multiple requests modal
  @Input() multipleorders: orderitem[];
  @Input() editmultipleorderitems: boolean;


  //create a reference of input of #displayitem in inventory.component.html
  @ViewChild('displayorderitem', {static: false}) displayorderitem: ElementRef;
  @ViewChild('multiplerequestes', {static: false}) multiplerequestes: ElementRef;
  @ViewChild('displayrecieveitem', {static: false}) displayrecieveitem: ElementRef;
  @ViewChild('barcode', {static: false}) barcode: ElementRef;
  @ViewChild('rejectapproveditem', {static: false}) rejectapproveditem: ElementRef;
  @ViewChild('eta', {static: false}) eta: ElementRef;
  rejectapproveditemModal:any;
  //output an event to parent
  @Output() resetView = new EventEmitter();

  unreceiveamount: string;
  showunreceive: boolean;
  openreveitemmodal: any;
  editreceiveitem = false;
  //enable to edit the single order item in order item display
  enabledEditorderItem: boolean;//it will be passed to order item display component
  //if a new multiplerequests created
  isnewrequests = false;
  orderitems: orderitem[];
  viewColumn: boolean[];
  columnList: string[];
  //item invent
  invent: item[];
  orderitemfilterList: { name: string, cat: string, type: string, supplier: string, manufacturer: string, unit: string, unitsize: string }[];
  showUrgent: boolean = false;
  showrequesturgent = false;
  showapproveurgent = false;
  showorderurgent = false;
  orderswitheta: orderitem[]=[];

  //get the invent data for requests, orders and receives
  displayOrders: orderitem[];
  // requestiteminvent: orderitem[] = [];
  // approveiteminvent: orderitem[] = [];
  // orderiteminvent: orderitem[] = [];
  page: number
  // requestpage;
  // approvepage;
  // orderpage;
  // requestspages;
  // approvespages;
  // orderspages;
  selectOrders: orderitem[];
  selectrequestorderitems: orderitem[] = [];
  selectapprovedorderitems: orderitem[] = [];
  // selectorderorderitems: orderitem[] = [];
  receiveorderitem: orderitem;
  requestapprove = false;
  requestmark = false;
  approverequestitemnames = '';
  markrequestitemnames = '';
  receiveitem: itemdetail;
  unit: ConfigValue[];
  volUnit: ConfigValue[];
  massUnit: ConfigValue[];
  otherUnit: ConfigValue[];
  catalog: string;
  islockCat: boolean;
  locationtype: ConfigValue[];
  currentuser: User;
  receivedateoptions: IMyDpOptions = { dateFormat: 'mm/dd/yyyy' };
  // etaoption: IMyDpOptions = { dateFormat: 'dd mmm yyyy' };
  today = new Date();
  etaoption: IMyDpOptions = { 
    todayBtnTxt: 'Today',
    dateFormat: 'dd mmm yyyy',
    firstDayOfWeek: 'mo',
    sunHighlight: true,
    inline: false,
    disableUntil: {year: this.today.getFullYear(), month: this.today.getMonth()+1, day: this.today.getDate()-1}
   };
  
  // etadateoptions: IMyDpOptions = { dateFormat: 'dd mmm yyyy' };
  receivedate: { date: { year: number, month: number, day: number } };
  projectnumbers: Projectnumber[];
  tabs: string[] = ['Requested', 'Approved', 'Ordered', 'Received'];
  currentTab: string = 'Requested';
  isLoading: boolean = false;
  orderCount: number;
  searchKey: string;

  newlot=false;
  showinuse=false;
  showunit=false;
  dateformat: { date: { year: number, month: number, day: number } };
  showorderitemslide:boolean;

  multiplerequestsmodel:any;
  orderitemmodal:any;
  receiveitemmodal:any;
  etamodal:any;

  PrintSerials=[];
  twodarray1 = [];
  twodarray2 = [];
  twodarray3 = [];
  barcodeValue;
  barcodemodal:any;
  selectBarcodeType='Linear Barcode';
  selectBarcodeSize='Small';
  barcodeItemName='';
  barcodeItemCat='';
  rejectComment='';

  constructor(private modalService: NgbModal, private orderitemService: OrderitemService, private inventService: InventoryService,
    private setting: SettingService, private itemdetailservice: ItemdetailService,
    private authenticationservice: AuthenticationService, private userhistoryservice: UserhistoryService, private barcodeservice: BarcodeService) { }

  ngOnInit() {
    // let today = new Date();
    // console.log(today.getDate());
    // console.log(today.getMonth());
    // console.log(today.getFullYear());
    // this.etaoption.disableUntil={year: today.getFullYear(), month: today.getMonth()+1, day: today.getDate()};
    let getcurrentuser = this.authenticationservice.getCurrentUser()
    if (getcurrentuser !== undefined) {
      getcurrentuser.then(_ => {
        this.currentuser = User.fromJson(_);
      });
    }

    this.page = 1;
    this.currentTab = 'Requested';
    this.searchKey = '';
    this.selectOrders = [];
    // this.orderCount = 0;
    this.isLoading = true;
    this.orderitemService.getCount(this.currentTab).subscribe(c => {
      this.orderCount = c;
      this.loadPage();
    })
    this.viewColumn = [true, true, true, true, false, false, false, false, false, true, true, false, true, true, false];
    this.columnList = ['Name',  'Cat', 'Amount/Quantity', 'Unreceive Amount', 'Unit', 'Unit Size', 'Supplier', 'Manufacturer',
    'Project Number',  'Request Person', 'Status',  'Receive Person',
      this.currentTab+' Time',  'Urgent', 'Comment(s)'];
    this.inventService.loadInventory().subscribe(invent => {this.invent = invent;});

    // this.orderitemService.loadOrderitem().subscribe(orderitems=>{
    //   orderitems.forEach(orderitem=>{
    //     if(orderitem.unreceiveamount.trim()=='0' && orderitem.status.trim()=='Ordered'){
    //       orderitem.status='Received';
    //       this.orderitemService.updateOrderitem(orderitem);
    //     }
    //   })
    // });

  }


  //this one will run everytime the @input change
  ngOnChanges(change: SimpleChanges) {
    this.enabledEditorderItem = false;
  }

  ngAfterViewInit() {
    setTimeout(_ => {
      if (this.orderitem !== undefined && this.isShoworderItem) {
        this.open(this.displayorderitem, this.orderitem);
      }
    });

    setTimeout(_ => {
      if (this.multipleorders !== undefined && this.editmultipleorderitems) {
        this.isnewrequests = true;
        this.modalService.open(this.multiplerequestes, { backdrop: "static", size: 'lg' }).result
          .then((result) => { });
      }
    });
  }

  loadSetting(){
    this.setting.getSettingByPage('general').subscribe(config => {
      this.locationtype = ConfigValue.toConfigArray(config.find(x => x.type == 'location').value);
      if(this.locationtype == []) this.locationtype = Setting.getLocation();
      this.unit = [];
      this.volUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'volUnit').value)
      this.volUnit.forEach(v => { this.unit.push(v);})
      this.massUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'massUnit').value)
      this.massUnit.forEach(m => { this.unit.push(m);})
      this.otherUnit = ConfigValue.toConfigArray(config.find(x => x.type == 'otherUnit').value)
      this.otherUnit.forEach(o => { this.unit.push(o); })
    });

    this.setting.getSettingByPage('orderItem').subscribe(config => {
      this.projectnumbers = [];
      let proList = ConfigValue.toConfigArray(config.find(x => x.type == 'projectList').value);
      if (proList == []) proList = Setting.getProject()
      proList.forEach(p => {
        this.projectnumbers.push(new Projectnumber(p.id, p.name));
      });
    });
  }

  toggleViewCol(index: number) {
    this.viewColumn[index] = !this.viewColumn[index];
  }

  loadPage() {
    this.displayOrders=[];
    this.isLoading = true;
    this.showUrgent=false;
    // if (this.showUrgent){
    //   this.orderitemService.loadUrgentOrderByStatus(this.currentTab, this.page - 1).subscribe(result => {
    //     let temporders = result.slice();
    //     let index=temporders.length-1;
    //     for(index; index>=0; index--){
    //       if(temporders[index].urgent!=true){
    //         temporders.splice(index, 1);
    //       }
    //     }
    //     this.displayOrders = temporders.slice();
    //     this.isLoading = false;
    //   });
    // }
    // else{
      this.orderitemService.loadOrderByStatus(this.currentTab, this.page - 1).subscribe(result => {
        this.displayOrders = result;
        this.isLoading = false;
      })
    // }

  }

  searchOrder(key: string) {
    this.searchKey = key;
    this.isLoading = true;
    //this.showUrgent=false;
    if(this.searchKey == undefined || this.searchKey == ''){
      this.orderitemService.getCount(this.currentTab).subscribe(c => {
        this.orderCount = c;
        this.page = 1;
        this.showUrgent = false;
        this.isLoading = true;
        this.loadPage();
        this.isLoading = false;
      })    
    }
    else{
      this.orderitemService.searchOrder(this.currentTab, this.searchKey, this.page - 1).subscribe(result=>{
        this.displayOrders = result.slice();
        this.orderitemService.searchOrderCount(this.currentTab, this.searchKey,).subscribe(count=>{
          this.orderCount=count;
        })
        this.isLoading = false;
      })
    }



  }

  // seperateordertable(orderitems) {
  //   this.requestiteminvent = [];
  //   this.orderiteminvent = [];
  //   this.approveiteminvent = [];
  //   orderitems.forEach(orderitem => {
  //     if (orderitem.status == 'Requested') {
  //       this.requestiteminvent.push(orderitem);
  //     }
  //     if (orderitem.status == 'Approved') {
  //       this.approveiteminvent.push(orderitem);
  //     }
  //     if (orderitem.status == 'Ordered' && Number(orderitem.unreceiveamount) > 0) {
  //       this.orderiteminvent.push(orderitem);
  //     }
  //   });
  // }

  openorderitem(displayorderitem, o){
    this.orderitem = o;
    this.orderitem.urgent = Number(this.orderitem.urgent) == 1 ? true : false;
    this.orderitemmodal=this.modalService.open(displayorderitem, { backdrop: "static", size: 'sm' })
  }

  open(content, x?: orderitem) {
    if (x) {
      this.orderitem = x;
      //temp solution
      this.orderitem.urgent = Number(this.orderitem.urgent) == 1 ? true : false;
    }
    //this.modalService.open(content, { backdrop: "static", size: 'lg' }) that is to open the modal
    //property has result promise
    this.orderitemmodal=this.modalService.open(content, { backdrop: "static", size: 'lg' }).result
      .then(
        (result) => {
          if (this.orderitem) { this.reset(); }
        },
        () => {
          //print error, fall back, roll back
          if (!this.orderitem) { this.reset(); }
        });
  }

  showorderitem(orderitem:orderitem){
    this.reset();
    this.orderitem=orderitem;
    this.showorderitemslide=true;
  }

  reset() {
    this.showorderitemslide=false;
    this.enabledEditorderItem = false;
    this.isShoworderItem = false;
    this.orderitem = undefined;
    this.receiveitem = undefined;
    this.editmultipleorderitems = false;
    this.isnewrequests = false;
    this.multipleorders = undefined;
    this.showunreceive = undefined;
    this.resetView.emit();
    this.page = 1;
    // this.currentTab = 'Requested';
    this.searchKey = '';
    this.selectOrders = [];
  }

  //user only can update requests
  updateorderItem(orderitem: orderitem) {
    if (orderitem == undefined) return;
    //if this orderitem is exist in database
    //if user change order amount

      orderitem.unreceiveamount = orderitem.amount;
      this.orderitemService.updateOrderitem(orderitem).then((result) => {
        let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Order Item/Requested', 'Update a New Request', 'Requested name: '+orderitem.name+', Requested manufacturer cat#: '+orderitem.cat)
        this.userhistoryservice.addUserHistory(userhistory);

        this.enabledEditorderItem = false;
        let index = this.displayOrders.findIndex(x=>x.dbid==orderitem.dbid);
        if(index!=-1){
          this.displayOrders[index]=orderitem;
          this.orderitemmodal.close();
        }
        else{
          this.page=1;
          this.isLoading = true;
          this.loadPage();
          this.isLoading = false;
        }
        
      });




    

    
  }

  //only delete requests
  deleteorderItem() {
    //check if the orderitem database include this orderitem, update orderitems first
    let deleletresult = this.orderitemService.deleteOrderitem(this.orderitem);
    deleletresult
      .then((result) => {
        let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Order Item/Requested', 'Delete a Request', 'Requested name: '+this.orderitem.name+', Requested manufacturer cat#: '+this.orderitem.cat)
        this.userhistoryservice.addUserHistory(userhistory);
        this.page = 1;
        this.isLoading = true;
        this.loadPage();
        this.isLoading = false;;
        // this.orderitems.splice(index, 1);
        // // this.seperateordertable(this.orderitems);
        // this.orderitem = undefined;
        // this.isShoworderItem = false;
      })
      .catch(err => { });
  }

  //this method change an item from ordered to received, first update orderitem inventory, then update item amount in item inventory
  approve(orderitem: orderitem) {
    orderitem.status = 'Approved';
    orderitem.approvetime = new Date();
    orderitem.approveperson = this.currentuser.name;
    this.orderitemService.updateOrderitem(orderitem).then(_ => {

      let i;
      let newitem;
      for (i = 0; i < this.invent.length; i++) {
        if (orderitem.cat == this.invent[i].cat && orderitem.name == this.invent[i].name && orderitem.supplier == this.invent[i].supplier) {
          newitem = this.invent[i];
          newitem.amount = newitem.amount + orderitem.amount;
          break;
        }
      }
      this.inventService.updateItem(newitem)
    })
  }

  //create multipleoreder array, isnewrequests, then open multiplerequestes modle
  newrequests(modal) {
    this.multipleorders = [];
    this.isnewrequests = true;
    this.multipleorders.push(new orderitem(-1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', new Date(), null, null, null, null, null, false, '','','No'));
    this.multiplerequestsmodel=this.modalService.open(modal, { backdrop: "static", size: 'sm' });
  }

  addneworderitem() {
    this.multipleorders.push(new orderitem(-1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', new Date(), null, null, null, null, null, false, '','','No'));
  }

  removeorderitem(index) {
    let removeorderitem = this.multipleorders[index];
    //remove this itemLink from displayKit
    this.multipleorders.splice(index, 1);
  }

  //this method save the multiple orders
  savemultipleorders() {
    //this.enabledEditorderItem=false;
    let multiplerequesttime = new Date();
    //the multipleorders is an array include empty orderitems include: dbid, cat, name, vendor, amount, unit, grantid, 
    this.multipleorders.forEach(multipleorderitem => {
      multipleorderitem.requesttime = multiplerequesttime;
      multipleorderitem.requestperson = this.currentuser.name;
      multipleorderitem.unreceiveamount = multipleorderitem.amount;
      multipleorderitem.status = 'Requested';
      // this.invent.forEach(item => {
      //   if (item.name == multipleorderitem.name) {
      //     multipleorderitem.cat = item.cat;
      //     if (!multipleorderitem.supplier) {
      //       multipleorderitem.supplier = item.supplier;
      //     }
      //   }
      // });
      this.orderitemService.addOrderitem(multipleorderitem).subscribe(orderitem => {
        let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Order Item/Requested', 'Create New Requests', 'Requested name: '+orderitem.name+', Requested manufacturer cat#: '+orderitem.cat)
        this.userhistoryservice.addUserHistory(userhistory);

        this.multiplerequestsmodel.close();
        this.page = 1;
        this.isLoading = true;
        this.loadPage();
        this.isLoading = false;
      });

    });
    this.isnewrequests = false;
  }

  changeurgent(order, index) {
    order.urgent = !order.urgent;
  }

  //for multiple orders to select an item name to order
  //get user input and search the item name in inventory include the input
  getFilter(key: string, orderitem: orderitem) {
    if (key == undefined) return;
    this.orderitemfilterList = [];
    this.invent.forEach((item, i) => {
      if (item.name.toUpperCase().includes(key.toUpperCase())) {
        ;
        this.orderitemfilterList.push({ name: item.name, cat: item.cat, type: item.supplier, supplier: item.supplier, manufacturer: item.manufacturer, unit: item.unit, unitsize: item.unitsize });
      }
    });
    if (this.orderitemfilterList.length == 0) {
      orderitem.name = key;
    }
  }

  //select an item name that is filtered by getFilter method then put into multipleorders array
  selectorderitem(select: { name: string, cat: string, type: string, supplier: string, manufacturer: string, unit: string, unitsize: string }, index: number) {
    if (this.multipleorders == undefined) return;
    if (index == undefined || index < 0 || index >= this.multipleorders.length) return;
    if (this.multipleorders[index] == undefined) return;

    this.multipleorders[index].name = select.name;
    this.multipleorders[index].cat = select.cat;
    this.multipleorders[index].type = select.type;
    this.multipleorders[index].supplier = select.supplier;
    this.multipleorders[index].manufacturer = select.manufacturer;
    this.multipleorders[index].unit = select.unit;
    this.multipleorders[index].unitsize = select.unitsize;
    this.orderitemfilterList = undefined;
  }

  setUrgent() {
    this.page = 1;
    this.showUrgent = !this.showUrgent;
    this.displayOrders=[];
    //this.showUrgent=false;
    if (this.showUrgent){
      this.orderitemService.loadUrgentOrderByStatus(this.currentTab, this.page - 1).subscribe(result => {
        let temporders = result.slice();
        let index=temporders.length-1;
        for(index; index>=0; index--){
          if(temporders[index].urgent!=true){
            temporders.splice(index, 1);
          }
        }
        this.displayOrders = temporders.slice();
        this.isLoading = false;
        this.orderitemService.loadUrgentOrderByStatusCount(this.currentTab).subscribe(count=>{
          this.orderCount=count;
        })
      });
    }
    else{
      this.orderitemService.loadOrderByStatus(this.currentTab, this.page - 1).subscribe(result => {
        this.displayOrders = result;
        this.isLoading = false;
        this.orderitemService.getCount(this.currentTab).subscribe(count=>{
          this.orderCount=count;
        })
      })
    }
  }

  changeTab(tab: string) {
    this.page = 1;
    this.displayOrders = [];
    this.showUrgent = false;
    this.currentTab = tab;
    this.columnList[11]=this.currentTab+' Time';
    this.searchKey = '';
    this.orderitemService.getCount(this.currentTab).subscribe(c => {
      this.orderCount = c;
      this.loadPage();
    })
  }

  checkSelected(orderitem: orderitem): boolean {
    if (orderitem == undefined) { return false };
    // if (this.displayOrders == undefined || this.displayOrders.length < 1) { return false };
    let oi = this.selectOrders.find(x => x.dbid == orderitem.dbid);
    if (oi !== undefined) { return true };
    if (!oi) { return false };
  }

  // checkapprove(orderitem: orderitem): boolean {
  //   if (orderitem == undefined) { return false };
  //   if (this.selectapprovedorderitems == undefined || this.selectapprovedorderitems.length < 1) { return false };
  //   if (this.selectapprovedorderitems.find(x => x.dbid == orderitem.dbid)) { return true };
  //   if (!this.selectapprovedorderitems.find(x => x.dbid == orderitem.dbid)) { return false };
  // }

  // checkorder(orderitem: orderitem): boolean {
  //   if (orderitem == undefined) { return false };
  //   if (this.selectorderorderitems == undefined || this.selectorderorderitems.length < 1) { return false };
  //   if (this.selectorderorderitems.find(x => x.dbid == orderitem.dbid)) { return true };
  //   if (!this.selectorderorderitems.find(x => x.dbid == orderitem.dbid)) { return false };
  // }

  selectOrderItem(orderitem: orderitem) {
    if (orderitem.status !== this.currentTab) return;
    let checked = !this.checkSelected(orderitem);
    if (checked == false) {
      let i = this.selectOrders.indexOf(orderitem);
      this.selectOrders.splice(i, 1);
    }
    else {
      this.selectOrders.push(orderitem);
    }
  }

  // selectapproveorderitem(orderitem: orderitem) {
  //   let checked = !this.checkapprove(orderitem);
  //   if (checked == false) {
  //     let i = this.selectapprovedorderitems.indexOf(orderitem);
  //     this.selectapprovedorderitems.splice(i, 1);
  //   }
  //   else {
  //     this.selectapprovedorderitems.push(orderitem);
  //   }
  // }

  approvemultiplerequests() {
    let requestindex = 0;
    let promises=[];
    this.approverequestitemnames = '';
    let length = this.selectOrders.length;
    this.selectOrders.forEach((orderitem, index) => {
      if (orderitem.status == 'Requested') {
        orderitem.status = 'Approved';
        orderitem.approvetime = new Date();
        orderitem.approveperson = this.currentuser.name;
        promises.push(this.orderitemService.updateOrderitem(orderitem));
        let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Order Item/Requested', 'Approve Requests', 'Requested name: '+orderitem.name+', Requested manufacturer cat#: '+orderitem.cat)
        this.userhistoryservice.addUserHistory(userhistory);

        if (index < length - 1) {
          this.approverequestitemnames = this.approverequestitemnames + orderitem.name + " , and ";
        }
        else {
          this.approverequestitemnames = this.approverequestitemnames + orderitem.name + " ";
        }
      }
    });

    Promise.all(promises).then(result=>{

      this.requestapprove = true;
      setTimeout(() => this.requestapprove = false, 3000);
      this.loadPage();
      this.selectOrders = [];
    })
  }

  showETA(){
    this.orderswitheta =[];
    this.selectOrders.forEach(order=>{
      let selectorderitem:orderitem = orderitem.neworderitem(order);
      this.orderswitheta.push(selectorderitem);
    })
    this.etamodal=this.modalService.open(this.eta, { backdrop: "static", size: 'lg' });
  }

  changeeta(order:orderitem, event) {
    let d = event.formatted;
    if (d == undefined) return;
    order.eta = new Date(d);
  }

  markmultipleorder(action:string) {
    this.markrequestitemnames = '';
    let promises=[];
    let correctOrderitemList=[];
    let length;
    if(action=='update'){
      correctOrderitemList = this.orderswitheta;
      length = this.orderswitheta.length;
    }
    else{
      correctOrderitemList = this.selectOrders;
      length = this.selectOrders.length;
    }

    correctOrderitemList.forEach((orderitem, index) => {
        if (orderitem.status == 'Approved') {
          orderitem.status = 'Ordered';
          orderitem.ordertime = new Date();
          orderitem.orderperson = this.currentuser.name;
          promises.push(this.orderitemService.updateOrderitem(orderitem))
          let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Order Item/Approved', 'Order Requests', 'Requested name: '+orderitem.name+', Requested manufacturer cat#: '+orderitem.cat)
          this.userhistoryservice.addUserHistory(userhistory);
        }
        if (index < length - 1) {
          this.markrequestitemnames = this.markrequestitemnames + orderitem.name + " , and ";
        }
        else {
          this.markrequestitemnames = this.markrequestitemnames + orderitem.name + " ";
        }
      });
  
    Promise.all(promises).then(result=>{
      this.requestmark = true;
      setTimeout(() => this.requestmark = false, 3000);
      this.loadPage();
      this.selectOrders = [];
      this.orderswitheta=[];
    });
  }

  addreceiveitems(i: orderitem) {
    this.receiveorderitem = i;
    this.unreceiveamount = this.receiveorderitem.unreceiveamount;
    this.inventService.loadInventory().subscribe(invent => {
      this.invent = invent;
      let index = this.invent.findIndex(x => x.name == i.name && x.cat == i.cat);
      //if order item dose not exist in database
      if (index == -1) {
        let newitem = new item(-1, '', '', '', '', '', true, '', '', '', '', '', '', '', '', '', new Date(), this.currentuser.name,'');
        newitem.name = i.name;
        newitem.category = i.category;
        newitem.cat = i.cat;
        newitem.type = i.type;
        newitem.unit = i.unit;
        newitem.unitsize = i.unitsize;
        newitem.supplier = i.supplier;
        newitem.manufacturer = i.manufacturer;
        newitem.unitprice = i.unitprice;
        this.inventService.addItem(newitem).then(_ => {
          // let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Order Item/Ordered', 'Add a New Item to Inventory', 'Item name: '+newitem.name+', Item manufacturer cat#: '+newitem.cat)
          // this.userhistoryservice.addUserHistory(userhistory);
          this.receiveitem = new itemdetail(-1, _['dbid'], newitem.category, newitem.name,  newitem.type, newitem.supplier, '', '', new Date(), new Date(),null, '', '', '', '','',    '','','','',null,'','','','', '','', '','','',       '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', new Date(), '', '', i.comment,i.reserve,i.grantid,this.currentuser.name,i.unit,i.unitsize,'No', new Date(), this.currentuser.name);
          if(this.receiveitem.unitsize==''){this.receiveitem.unitsize='1'}
          if(newitem.category=='Reference Material'){this.receiveitem.rfstatus='Entered'}
          if(newitem.type=='Conjugate'){this.receiveitem.constatus='Entered'}
          this.openreveitemmodal = this.modalService.open(this.displayrecieveitem, { backdrop: "static", size: 'lg' })
          this.editreceiveitem = true;
        })
      }
      else {//find exiting item in inventory with recieved order item
        this.receiveitem = new itemdetail(-1, this.invent[index].dbid, this.invent[index].category, this.invent[index].name, this.invent[index].type, this.invent[index].supplier, '', '', new Date(), new Date(), null, '', '', '', '','',     '','','','',null,'','','','','', '','','','',      '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', new Date(), '', '',i.comment,i.reserve,i.grantid,this.currentuser.name,i.unit,i.unitsize,'No', new Date(), this.currentuser.name);
        if(this.invent[index].category=='Reference Material'){this.receiveitem.rfstatus='Entered'}
        if(this.invent[index].type=='Conjugate'){this.receiveitem.constatus='Entered'}
        if(this.receiveitem.unitsize==''){this.receiveitem.unitsize='1'}
        this.openreveitemmodal = this.modalService.open(this.displayrecieveitem, { backdrop: "static", size: 'lg' })
        this.openreveitemmodal.result.then(result => { });
        this.editreceiveitem = true;
      }

    });

  }

  updateItemdetail(e: { receiveitem: itemdetail, unreceiveamount: string }) {
    this.receiveorderitem.unreceiveamount = e.unreceiveamount;
    this.receiveorderitem.receiveperson = this.currentuser.name;
    if(this.receiveorderitem.unreceiveamount.trim()=='0'){
      this.receiveorderitem.status='Received';
      this.receiveorderitem.receivetime=new Date();
      this.receiveorderitem.receiveperson=this.currentuser.name;
    }
    this.orderitemService.updateOrderitem(this.receiveorderitem).then(() => {
      // let index = this.orderiteminvent.findIndex(x => x.dbid == this.receiveorderitem.dbid);
      // if (index != -1) {
      //   if (Number(this.receiveorderitem.unreceiveamount) < 1) {
      //     this.orderiteminvent.splice(index, 1);
      //   }
      //   else {
      //     this.orderiteminvent[index] = this.receiveorderitem;
      //   }
      // }
      this.loadPage();
      this.receiveorderitem = undefined;
      this.openreveitemmodal.close();
      //this.generateBarcode(e.receiveitem);
    });
    //
  }

  generateBarcode(itemdetail:itemdetail){
    this.inventService.getItemWithId(Number(itemdetail.itemdbid)).subscribe(item=>{
      this.barcodeItemName=item.name;
      this.barcodeItemCat=item.cat;
    });
    this.PrintSerials=[];

    this.barcodeservice.getLatest().subscribe(barcode=>{
      let lastestBarcodeString=barcode.barcode;
      
      let i;
      for(i=0;i<Number(itemdetail.amount);i++){
        let latestNumber=Number(lastestBarcodeString);
        let nextBarcodeNumber=latestNumber+1;
        let nextBarcodeString=nextBarcodeNumber.toString();
        let missZeros=16-nextBarcodeString.length;
        for(let i=0; i<missZeros;i++){
          nextBarcodeString='0'+nextBarcodeString;
        }
        
        let newbarcode=new Barcode(-1, itemdetail.dbid, nextBarcodeString, 'Un-Open', this.currentuser.name, new Date());
        this.barcodeservice.addBarcode(newbarcode);
        lastestBarcodeString=nextBarcodeString;

        this.PrintSerials.push(nextBarcodeString);

      }

    this.changeBarcodeDisplay()

    //this.barcodemodal=this.modalService.open(this.barcode, { backdrop: "static", size: 'sm' });
    this.barcodemodal=this.modalService.open(this.barcode, { backdrop: "static", size: 'lg', windowClass: 'modal-xl' });


    });
    


  }

  changeBarcodeDisplay(){
    if(this.selectBarcodeSize=='Large'){
      this.twodarray1=[];
      let onedarray=[];
      let a;
      for(a=0; a<this.PrintSerials.length;a++){
        if(a==0 || a%2!=0){
          onedarray.push(this.PrintSerials[a].toString());
        }
        else{
          this.twodarray1.push(onedarray);
          onedarray=[];
          onedarray.push(this.PrintSerials[a].toString());
        }
      }

      this.twodarray1.push(onedarray);
      onedarray=[];

    }
    else if(this.selectBarcodeSize=='Middle'){
      this.twodarray2=[];
      let onedarray=[];
      let a;
      for(a=0; a<this.PrintSerials.length;a++){
        if(a==0 || a%3!=0){
          onedarray.push(this.PrintSerials[a].toString());
        }
        else{
          this.twodarray2.push(onedarray);
          onedarray=[];
          onedarray.push(this.PrintSerials[a].toString());
        }
      }

      this.twodarray2.push(onedarray);
      onedarray=[];

    }
    else{
      this.twodarray3=[];
      let onedarray=[];
      let a;
      for(a=0; a<this.PrintSerials.length;a++){
        if(a==0 || a%4!=0){
          onedarray.push(this.PrintSerials[a].toString());
        }
        else{
          this.twodarray3.push(onedarray);
          onedarray=[];
          onedarray.push(this.PrintSerials[a].toString());
        }
      }

      this.twodarray3.push(onedarray);
      onedarray=[];

    }
  }

  barcodePdf(barcodes, size){
    if(size=='Large'){

    }
    else if(size=='Middle'){

    }
    else{

    }

  }

  cancelBarcode(){
    this.PrintSerials=[];
    this.barcodemodal.close();
  }

  saveBarcode(){
    // this.barcodeservice.
  }

  printBarcode(printableArea){
    var printContents = document.getElementById(printableArea).innerHTML;
    var originalContents = document.body.innerHTML;


    document.body.innerHTML = printContents;

    window.print();

    //document.body.innerHTML = originalContents;
  }

  rejectorderItem(orderitem:orderitem){
    if(orderitem ==  undefined){return;}
    if(orderitem.status == 'Approved' ){
      orderitem.status = 'Requested';
      this.orderitemService.updateOrderitem(orderitem).then(_=>{
        if(this.currentTab == 'Approved'){
          let index = this.displayOrders.findIndex(x=>x.dbid == orderitem.dbid);
          if(index!=-1){
            this.displayOrders.splice(index, 1)
          }
        };

        //send email
        let emailList = [];
        let requestperson = orderitem.requestperson;
        let approveperson = orderitem.approveperson;

        this.authenticationservice.loadAllUsers().subscribe(allUsers=>{
          let requestIndex = allUsers.findIndex(x=>x.name == requestperson);
          let approveIndex = allUsers.findIndex(x=>x.name == approveperson);

          if(requestIndex != -1){
            emailList.push(allUsers[requestIndex].email);
          }
          if(approveIndex != -1){
            emailList.push(allUsers[approveIndex].email);
          }

          let subject = orderitem.name+'is rejected'
          let unApproveEmailContent = 'Hello,'+'\n'+orderitem.name+', '+orderitem.cat+', requested by '+orderitem.requestperson+' on '+new Date(orderitem.requesttime).toDateString()+' is rejected.\n'+'The reject reason is:\n'+this.rejectComment;
          this.rejectComment = '';
          
          let emaildetail = new emailDetail('not approve', emailList, subject, unApproveEmailContent);
          this.orderitemService.sendEmail(emaildetail)



        })


      })

      
    }
  }

}
