import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { TimerComponent } from "../timer/timer.component";
@Component({
  selector: "app-template-three",
  templateUrl: "./template-three.component.html",
  styleUrls: ["./template-three.component.css"]
})
export class TemplateThreeComponent implements OnInit {
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;
  seventhFormGroup: FormGroup;
  eighthFormGroup: FormGroup;
  ninthFormGroup: FormGroup;
  tenthFormGroup: FormGroup;
  eleventhFormGroup: FormGroup;
  timer = new TimerComponent(4);
  // time spans for the four sections in template one
  counterSec = [0, 0, 0, 0];

  constructor(private _formBuilder: FormBuilder) {}

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ["", Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ["", Validators.required],
      // control for item inputs
      step2_item_1: ["", Validators.required],
      step2_item_2: ["", Validators.required],
      step2_item_3: ["", Validators.required],
      step2_item_4: ["", Validators.required],
      step2_item_5: ["", Validators.required],
      step2_item_6: ["", Validators.required],
      step2_item_7: ["", Validators.required],
      step2_item_8: ["", Validators.required],

      //

      // control for supplier inputs
      step2_supplier_1: ["", Validators.required],
      step2_supplier_2: ["", Validators.required],
      step2_supplier_3: ["", Validators.required],
      step2_supplier_4: ["", Validators.required],
      step2_supplier_5: ["", Validators.required],
      step2_supplier_6: ["", Validators.required],
      step2_supplier_7: ["", Validators.required],
      step2_supplier_8: ["", Validators.required]
      //
    });
    this.thirdFormGroup = this._formBuilder.group({
      thirdCtrl: ["", Validators.required],
      // control for item inputs
      step3_item_1: ["", Validators.required],
      //

      // control for supplier inputs
      step3_supplier_1: ["", Validators.required]
      //
    });
    this.fourthFormGroup = this._formBuilder.group({
      fourthCtrl: ["", Validators.required],
      // control for item inputs
      step4_item_1: ["", Validators.required],
      //

      // control for supplier inputs
      step4_supplier_1: ["", Validators.required]
      //
    });
    this.fifthFormGroup = this._formBuilder.group({
      fifthCtrl: ["", Validators.required],
      // control for item inputs
      step5_item_1: ["", Validators.required],
      //

      // control for supplier inputs
      step5_supplier_1: ["", Validators.required]
      //
    });
    this.sixthFormGroup = this._formBuilder.group({
      sixthCtrl: ["", Validators.required],
      // control for item inputs
      step6_item_1: ["", Validators.required],
      step6_item_2: ["", Validators.required],
      step6_item_3: ["", Validators.required],
      step6_item_4: ["", Validators.required],
      //

      // control for supplier inputs
      step6_supplier_1: ["", Validators.required],
      step6_supplier_2: ["", Validators.required],
      step6_supplier_3: ["", Validators.required],
      step6_supplier_4: ["", Validators.required]
      //
    });
    this.seventhFormGroup = this._formBuilder.group({
      seventhCtrl: ["", Validators.required],
      // control for item inputs
      step7_item_1: ["", Validators.required],
      //

      // control for supplier inputs
      step7_supplier_1: ["", Validators.required]
      //
    });
    this.eighthFormGroup = this._formBuilder.group({
      eighthCtrl: ["", Validators.required],
      // control for item inputs
      step8_item_1: ["", Validators.required],
      step8_item_2: ["", Validators.required],
      //

      // control for supplier inputs
      step8_supplier_1: ["", Validators.required],
      step8_supplier_2: ["", Validators.required]
      //
    });
    this.ninthFormGroup = this._formBuilder.group({
      ninthCtrl: ["", Validators.required],
      // control for item inputs
      step9_item_1: ["", Validators.required],
      //

      // control for supplier inputs
      step9_supplier_1: ["", Validators.required]
      //
    });
    this.tenthFormGroup = this._formBuilder.group({
      tenthCtrl: ["", Validators.required]
    });
    this.eleventhFormGroup = this._formBuilder.group({
      eleventhCtrl: ["", Validators.required]
    });
  }

  // save the time span for count down timer
  saveTime(param: number, section: number) {
    this.counterSec[section] = param;
  }

  //getter for time span
  getTime(arrIndex: number) {
    return this.counterSec[arrIndex];
  }

  //return the number of total timers used in this  template
  getTimerNumber() {
    return this.counterSec.length;
  }
}
