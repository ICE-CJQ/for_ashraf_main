import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef
} from "@angular/core";

import { Observable, interval, Subscription } from "rxjs";
import { take } from "rxjs/operators";
import { map } from "rxjs/operators";

@Component({
  selector: "app-counter",
  templateUrl: "./counter.component.html",
  styleUrls: ["./counter.component.css"]
})
export class CounterComponent {
  pauseAt: number = 0;
  timeArr = ["00", "00", "00"];
  secValue = 0;
  hrValue = 0;
  minValue = 0;
  pauseValue = false;
  isStarted = false;
  firstStart = true;


  @Input()
  startAt = 0;

  @Input()
  showTimeRemaining = true;

  @Output()
  counterState = new EventEmitter();

  @Output()
  sendTime = new EventEmitter<number>();

  private currentValue = "";

  private currentSubscription: Subscription;

  constructor(private changeDetector: ChangeDetectorRef) { }

  public start() {
    this.currentValue = this.formatValue(this.startAt);
    this.changeDetector.detectChanges();
    if (this.firstStart) {
      this.sendTime.emit(this.startAt);
      this.firstStart = false;

    }


    const t: Observable<number> = interval(1000);
    this.currentSubscription = t
      .pipe(take(this.startAt))
      .pipe(map(v => this.startAt - (v + 1)))
      .subscribe(
        v => {
          this.currentValue = this.formatValue(v);
          this.changeDetector.detectChanges();
        },
        err => {
          this.counterState.error(err);
        },
        () => {

          this.stopTimer();
        }
      );
  }

  private formatValue(v) {
    this.pauseAt = v;
    const hours = Math.floor(Math.floor(v / 60) / 60);
    const formattedHours = "" + (hours > 9 ? hours : "0" + hours);
    const minutes = Math.floor(v / 60) % 60;
    const formattedMinutes = "" + (minutes > 9 ? minutes : "0" + minutes);
    const seconds = v % 60;
    const formattedSeconds = "" + (seconds > 9 ? seconds : "0" + seconds);
    this.timeArr = [formattedHours, formattedMinutes, formattedSeconds];

    return `${formattedHours}:${formattedMinutes}:${formattedSeconds}`;
  }

  public stop() {
    this.currentSubscription.unsubscribe();
    this.firstStart = true;
    // this.currentValue = "00:00:00";
    // this.counterState.emit("ABORTED");
    this.timeArr = ["00", "00", "00"];
  }

  public pause() {
    this.currentSubscription.unsubscribe();
    this.startAt = this.pauseAt;
  }

  // counterState = "counter is ticking";

  onKeySecond(event: any) {
    // without type info

    let temp = parseInt(event.target.value);
    this.secValue = temp;
  }

  onKeyMin(event: any) {
    // without type info

    let temp = parseInt(event.target.value);
    this.minValue = temp;
  }

  onKeyHr(event: any) {
    // without type info

    let temp = parseInt(event.target.value);
    this.hrValue = temp;
  }

  startTimer() {
    if (!this.isStarted) {
      console.log("second value: " + this.secValue);
      console.log("minute value: " + this.minValue);
      console.log("hour value: " + this.hrValue);

      if (isNaN(this.secValue)) {
        this.secValue = 0;
        console.log("changed sec: " + this.secValue);
      }

      if (isNaN(this.minValue)) {
        this.minValue = 0;
        console.log("changed min: " + this.minValue);
      }

      if (isNaN(this.hrValue)) {
        this.hrValue = 0;
        console.log("changed hr: " + this.hrValue);
      }

      if (this.pauseValue == false) {
        this.startAt = this.secValue + this.minValue * 60 + this.hrValue * 3600;
        console.log(this.startAt);
      }

      this.start();
      this.isStarted = true;
    }
  }

  pauseTimer() {
    this.pause();
    this.pauseValue = true;
    this.isStarted = false;
  }

  stopTimer() {
    this.stop();
    this.pauseValue = false;
    this.isStarted = false;
  }
}
