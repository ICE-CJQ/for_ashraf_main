import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { TimerComponent } from "../timer/timer.component";
@Component({
  selector: "app-template-two",
  templateUrl: "./template-two.component.html",
  styleUrls: ["./template-two.component.css"]
})
export class TemplateTwoComponent implements OnInit {
  isLinear = false;
  seventhFormGroup: FormGroup;
  eighthFormGroup: FormGroup;
  ninthFormGroup: FormGroup;
  tenthFormGroup: FormGroup;
  eleventhFormGroup: FormGroup;
  twelfthFormGroup: FormGroup;
  thirteenthFormGroup: FormGroup;
  fourteenthFormGroup: FormGroup;
  timer = new TimerComponent(3);
  // time spans for the three sections in template one
  counterSec = [0, 0, 0];

  constructor(private _formBuilder: FormBuilder) {}

  ngOnInit() {
    this.seventhFormGroup = this._formBuilder.group({
      seventhCtrl: ["", Validators.required]
    });
    this.eighthFormGroup = this._formBuilder.group({
      eigthCtrl: ["", Validators.required],
      //control for item inputs
      step8_item_1: ["", Validators.required],
      //

      //control for supplier inputs
      step8_supplier_1: ["", Validators.required]
      //
    });
    this.ninthFormGroup = this._formBuilder.group({
      ninthCtrl: ["", Validators.required],
      //control for item inputs
      step9_item_1: ["", Validators.required],
      step9_item_2: ["", Validators.required],
      //

      //control for supplier inputs
      step9_supplier_1: ["", Validators.required],
      step9_supplier_2: ["", Validators.required]
      //
    });
    this.tenthFormGroup = this._formBuilder.group({
      tenthCtrl: ["", Validators.required],
      //control for item inputs
      step10_item_1: ["", Validators.required],
      //

      //control for supplier inputs
      step10_supplier_1: ["", Validators.required]
      //
    });
    this.eleventhFormGroup = this._formBuilder.group({
      eleventhCtrl: ["", Validators.required],
      //control for item inputs
      step11_item_1: ["", Validators.required],
      step11_item_2: ["", Validators.required],
      //

      //control for supplier inputs
      step11_supplier_1: ["", Validators.required],
      step11_supplier_2: ["", Validators.required]
      //
    });
    this.twelfthFormGroup = this._formBuilder.group({
      twelfthCtrl: ["", Validators.required],
      //control for item inputs
      step12_item_1: ["", Validators.required],
      //

      //control for supplier inputs
      step12_supplier_1: ["", Validators.required]
      //
    });
    this.thirteenthFormGroup = this._formBuilder.group({
      thirteenthCtrl: ["", Validators.required],
      //control for item inputs
      step13_item_1: ["", Validators.required],
      //

      //control for supplier inputs
      step13_supplier_1: ["", Validators.required]
      //
    });
    this.fourteenthFormGroup = this._formBuilder.group({
      fourteenthCtrl: ["", Validators.required],
      //control for item inputs
      step14_item_1: ["", Validators.required],
      //

      //control for supplier inputs
      step14_supplier_1: ["", Validators.required]
      //
    });
  }

  // save the time span for count down timer
  saveTime(param: number, section: number) {
    this.counterSec[section] = param;
  }

  //getter for time span
  getTime(arrIndex: number) {
    return this.counterSec[arrIndex];
  }

  //return the number of total timers used in this  template
  getTimerNumber() {
    return this.counterSec.length;
  }
}
