import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Quote, SaleLink } from '../../shared/objects/Quote';
import { Invoice } from '../../shared/objects/Invoice';
import { QuoteService } from '../../shared/services/Quote.Service';
import { SaleLinkService } from '../../shared/services/Sale.Link.Service';
import { InvoiceService } from '../../shared/services/Invoice.Service';
import { User } from 'app/shared/objects/User';
import { AuthenticationService } from '../../shared/services/Authenticate.Service';
import { UserhistoryService } from '../../shared/services/userhistory.service';
import { Userhistory } from '../../shared/objects/Userhistory';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  providers: [SaleLinkService, QuoteService, InvoiceService, UserhistoryService],
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.css'],
  host: {
    '(document:click)': 'onClick($event)',
  }
})

export class SaleComponent implements OnInit {
  quotes: Quote[] = []
  quoteList: Quote[] = [];
  invoices: Invoice[];
  invoiceList: Invoice[] = [];
  displayObj: any[] = [];
  tabs: string[] = ['Quote', 'Invoice'];
  selectItem: any;
  enableEdit: boolean;
  currentTab: string = 'Quote';
  isnew: boolean;
  withQuote: boolean;
  page: number;
  isShow: boolean = false;
  searchKey: string;
  isLoading: boolean;
  count: number;
  currentuser: User;
  @ViewChild('changeVersionNumberOption', {static: false}) changeVersionNumberOption: ElementRef;
  changeVersionNumberModal:any;
  versionNumber=0;

  onClick(event) {
    if (!this.isnew && event.path !== undefined && event.path[0] !== undefined && event.path[0].classList !== undefined && event.path[0].classList[0] == 'hover-obj') {
      let num = event.path[0].innerText.trim();
      if (this.currentTab == 'Quote') {
        this.setSelectItem(this.displayObj.find(x => x.quoteNum == num))
      }
      else if (this.currentTab == 'Invoice') {
        this.setSelectItem(this.displayObj.find(x => x.invoiceNum == num))
      }
    }
    else if (event.path !== undefined && event.path.find(x => x.className == 'slide') !== undefined || this.isnew)
      return;
    else if (!this.isnew && event.path !== undefined && event.path.find(x => x.className !== undefined && x.className.includes('newquote')) !== undefined) {
      this.newItem();
    }
    // else if (!this.isnew && event.path !== undefined && event.path.find(x => x.className !== undefined && x.className.includes('newinvoice')) !== undefined) {
    //   this.withQuote = false;
    //   if (event.path.find(x => x.innerText !== undefined && x.innerText == 'With Existing Quote') !== undefined) {
    //     this.withQuote = true;
    //     this.newItem();
    //   }
    //   else{
    //     this.newItem();
    //   }
    // }
    else if (!this.isnew && event.path !== undefined && event.path.find(x => x.innerText !== undefined && x.innerText == 'With Existing Quote') !== undefined) {
        this.withQuote = true;
        this.newItem();
    }
    else if (!this.isnew && event.path !== undefined && event.path.find(x => x.innerText !== undefined && x.innerText == 'From Scratch') !== undefined) {
      this.withQuote = false;
      this.newItem();
    }
    else {
      this.isShow = false;
      this.isnew = false;
      this.enableEdit = false;
      //this.selectItem = undefined;
    }
  }

  constructor(private quoteservice: QuoteService,
    private salelinkservice: SaleLinkService,
    private invoiceservice: InvoiceService, 
    private authenticationservice: AuthenticationService, private userhistoryservice: UserhistoryService,
    private modalService: NgbModal) { }

  ngOnInit() {
    // this.quoteservice.getCurrencies().subscribe(data=>{
    //   if(data){
    //     console.log('get currency')
    //     console.log(data.rates.CAD);
    //     console.log(data.rates.EUR);
    //   }

    // })

    let getcurrentuser = this.authenticationservice.getCurrentUser()
    if (getcurrentuser !== undefined) {
      getcurrentuser.then(_ => {
        this.currentuser = User.fromJson(_);
      });
    }
    this.isLoading = true;
    this.page = 1;
    this.reset();
    if (this.currentTab == 'Quote'){
      this.quoteservice.count().subscribe(c => {
        this.count = c;
        this.loadQuotePage();
      })
    }

    else if (this.currentTab == 'Invoice'){
      this.invoiceservice.count().subscribe(c => {
        this.count = c;
        this.loadInvoicePage();
      })

    }

    // this.quoteservice.loadQuote().subscribe(quotes=>{
    //   quotes.forEach(quote=>{
    //     console.log(quote);
    //     let change = false;
    //     if(!quote.currency){quote.currency='USD';change=true;}
    //     if(quote.currencyrate==0){quote.currencyrate=1;change=true;}
    //     if(change){this.quoteservice.updateQuote(quote)}
    //   });
    // });

    // this.invoiceservice.loadInvoice().subscribe(invoices=>{
    //   invoices.forEach(invoice=>{
    //     console.log(invoice);
    //     let change = false;
    //     if(!invoice.currency){invoice.currency='USD';change=true;}
    //     if(invoice.currencyrate==0){invoice.currencyrate=1;change=true;}
    //     if(change){this.invoiceservice.updateInvoice(invoice)}
    //   });
    // });

    // this.quoteservice.loadQuote().subscribe(quotes => {
    //   this.quotes = quotes;
    //   this.sortAndAssignQuote();

    //   this.displayObj = this.quoteList;
    // });
    // this.invoiceservice.loadInvoice().subscribe(invoices => {
    //   this.invoices = invoices.sort((a, b) => { return new Date(a.dateCreate) >= new Date(b.dateCreate) ? -1 : 1 });
    //   // this.invoiceList = this.invoices.slice(0, 10);
    // })

  }

  loadQuotePage() {
    this.quoteservice.loadPage(this.page - 1).subscribe(q => {
      this.displayObj = q;
      this.isLoading = false;
    });
  }

  loadInvoicePage() {
    this.invoiceservice.loadPage(this.page - 1).subscribe(i => {
      this.displayObj = i;
      this.isLoading = false;
    })
  }

  reset() {
    this.currentTab = 'Quote';
    this.enableEdit = false;
    this.isnew = false;
    this.isShow = false;
    this.withQuote = false;
    this.displayObj = [];
    this.selectItem = undefined;
    this.searchKey = '';
  }

  changeTab(tab: string) {
    if(this.isnew) return;
    this.currentTab = tab;
    this.selectItem = undefined;
    this.enableEdit = false;
    this.isnew = false;
    this.isShow = false;
    this.page = 1;
    this.isLoading = true;
    if (this.currentTab == 'Quote'){
      this.quoteservice.count().subscribe(c => {
        this.count = c;
        this.loadQuotePage();
      })
    }

    else if (this.currentTab == 'Invoice'){
      this.invoiceservice.count().subscribe(c => {
        this.count = c;
        this.loadInvoicePage();
      })

      // this.invoiceservice.loadInvoice().subscribe(allinvoices=>{
      //   allinvoices.forEach(invoice=>{
      //     invoice.clientId=0;
      //     if(invoice.quoteId && invoice.quoteId!=0){
      //       console.log('invoice quoteid is: '+invoice.quoteId);
      //       this.quoteservice.getQuoteById(invoice.quoteId).subscribe(quote=>{
      //         invoice.clientId = quote.clientid;
      //         console.log('invoice clientid is '+invoice.clientId);

      //       })
      //     }
      //     this.invoiceservice.updateInvoice(invoice);
      //   })
      // })


    }

  }

  setSelectItem(i: any) {
    if (i == undefined) return;
    if (this.currentTab == 'Quote') {
      this.selectItem = Quote.newQuote(i);
      this.enableEdit = false;
      this.isnew = false;
      
    }
    else if (this.currentTab == 'Invoice') {
      this.selectItem = Invoice.newInvoice(i);
      this.enableEdit = false;
      this.isnew = false;
    }

    this.isShow = true;
    
  }

  sortAndAssignQuote() {
    this.quotes = this.quotes.sort((a, b) => {
      if (this.isExpired(a) && this.isExpired(b)) {
        return 0;
      }
      else if (this.isExpired(a)) {
        return 1;
      }
      else if (this.isExpired(b)) {
        return -1;
      }
      else {
        if (a.complete && b.complete) return 0;
        else if (a.complete) return 1;
        else if (b.complete) return -1;
        else return 1;
      }
    });

    this.quoteList = [];
    this.quotes.forEach(q => {
      if (!this.isExpired(q) && !q.complete) {
        this.quoteList.push(q);
      }
    });
  }

  newItem() {
    this.isLoading = true;
    if (this.currentTab == 'Quote') {
      let today = new Date();
      let today30dlater = today.getTime() + 60 * (24 * 60 * 60 * 1000);
      let id = 0;
      if (this.count < 1) {
        id = 7000;
        this.selectItem = new Quote(-1, id + '', -1, -1, today, new Date(today30dlater), 'Prepayment', 1, today, 0, 0, 0, 'No Tax', false, '', this.currentuser.name, new Date(), 'USD', 1);
        this.enableEdit = true;
        this.isnew = true;
        this.isShow = true;
        this.isLoading = false;
      }
      else {
        this.quoteservice.getLatest().subscribe(last => {
          id = Number(last.quoteNum) + 1;
          this.selectItem = new Quote(-1, id + '', -1, -1, today, new Date(today30dlater), 'Prepayment', 1, today, 0, 0, 0, 'No Tax', false,'', this.currentuser.name, new Date(), 'USD', 1);
          this.enableEdit = true;
          this.isnew = true;
          this.isShow = true;
          this.isLoading = false;
        })
      }
    }
    else if (this.currentTab == 'Invoice') {
      let today = new Date();
      let id = 0;
      if (this.count < 1) {
        id = 7000;
        this.selectItem = new Invoice(-1, id, today, undefined, -1, 1, today, '', '', '', '', '', '', 'FedEx', 'Prepayment', 'EXW',0, 0, 0,'No','Product invoice', '', this.currentuser.name, new Date(), 'USD', 1);
        this.enableEdit = true;
        this.isnew = true;
        this.isShow = true;
        this.isLoading = false;
      } // add to setting to keep track of the number sequence
      else {
        this.invoiceservice.getLatest().subscribe(last => {
          if (last !== undefined) {
            id = last.invoiceNum + 1;
            this.selectItem = new Invoice(-1, id, today, undefined, -1, 1, today, '', '', '', '', '', '', 'FedEx', 'Prepayment', 'EXW', 0, 0, 0,'No','Product invoice', '', this.currentuser.name, new Date(), 'USD', 1);
            this.enableEdit = true;
            this.isnew = true;
            this.isShow = true;
            this.isLoading = false;
          }
        })
      }
    }
  }

  searchObj(key: string) {
    if(this.isnew) return;
    this.isLoading = true;
    this.searchKey = key;
    if (this.searchKey == undefined || this.searchKey == '') {
      this.page = 1;
      this.currentTab == 'Quote' ? this.loadQuotePage() : this.currentTab == 'Invoice' ? this.loadInvoicePage() : '';
      return;
    }

    if (this.currentTab == 'Quote') {
      this.quoteservice.search(this.searchKey, this.page - 1).subscribe(result => {
        this.displayObj = result;
        this.isLoading = false;
      })
    }
    else if (this.currentTab == 'Invoice') {
      this.invoiceservice.search(this.searchKey, this.page - 1).subscribe(result => {
        this.displayObj = result;
        this.isLoading = false;
      })
    }
  }

  find(key: string) {
    if (this.currentTab == 'Quote') {
      this.quoteList = [];
      if (key == undefined || key.trim() == '') {
        this.sortAndAssignQuote();
        return;
      }

      this.quotes.forEach(q => {
        if ((q.quoteNum + '').includes(key)) {
          this.quoteList.push(q);
        }
      })
    }
    else if (this.currentTab == 'Invoice') {
      this.invoiceList = [];
      if (key == undefined || key.trim() == '') {
        this.invoiceList = this.invoices.slice(0, 10);
        return;
      }

      this.invoices.forEach(i => {
        if ((i.invoiceNum + '').includes(key)) {
          this.invoiceList.push(i);
        }
      })
    }
  }

  isExpired(quote: Quote): boolean {
    if (quote == undefined) return true;
    let today = new Date();
    if (today >= quote.expireDate) return true;
    return false
  }

  cancelAdd() {
    this.selectItem = undefined;
    this.isnew = false;
    this.isShow = false;
    this.withQuote = false;
    this.enableEdit = false;
  }

  updateInvoice(invoice: { i: Invoice, link: SaleLink[] }) {
    if (this.isnew) {
      let invnum = invoice.i.invoiceNum;
      while (this.invoiceList.find(x => x.invoiceNum == invnum) !== undefined) {
        invnum++;
      }
      invoice.i.invoiceNum = invnum;
      this.invoiceservice.addInvoice(invoice.i).then(_ => {
        let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Sale/Invoice', 'Add a New Invoice', 'Invoice Number: '+invoice.i.invoiceNum)
        this.userhistoryservice.addUserHistory(userhistory);

        let id = _['dbid'];
        // invoice.i.dbid = id;
        // this.invoices.push(invoice.i);
        // this.invoices = this.invoices.sort((a, b) => { return new Date(a.dateCreate) >= new Date(b.dateCreate) ? -1 : 1 })
        // this.invoiceList = this.invoices.slice(0, 10);
        if (invoice.link) {
          invoice.link.forEach(l => {l.linkId = id;});
          this.savesalelink(invoice.link,0);
        }
      });
    }
    else {
      //invoice.i.revision += 1;
      invoice.i.revisionDate = new Date();
      this.invoiceservice.updateInvoice(invoice.i).then(_ => {
        let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Sale/Invoice', 'Updated an Invoice', 'Invoice Number: '+invoice.i.invoiceNum)
        this.userhistoryservice.addUserHistory(userhistory);
        let id = _['dbid'];
        invoice.i.dbid = id;
        let i = this.displayObj.findIndex(x => x.dbid == id);
        this.displayObj[i] = invoice.i;
        this.displayObj = this.displayObj.sort((a, b) => { return new Date(a.dateCreate) >= new Date(b.dateCreate) ? -1 : 1 })
        this.invoiceList = this.displayObj.slice();
        
        if (invoice.link) {
          invoice.link.forEach(l => {l.linkId = id;});
          this.savesalelink(invoice.link,0);
          this.versionNumber = Number(invoice.i.revision);
          this.changeVersionNumberModal=this.modalService.open(this.changeVersionNumberOption, { backdrop: "static", size: 'lg' });
          this.salelinkservice.loadAllLinkById(invoice.i.dbid, 'invoice').subscribe(dblinks => {
            let deletelist = [];
            dblinks.forEach(link => {
              if (invoice.link.find(x => x.dbid == link.dbid) == undefined) {
                deletelist.push(link);
              }
            });

            deletelist.forEach(link => {
              this.salelinkservice.deleteSaleLink(link);
            });


            // let p = [];
            // Promise.all(p).then(_ => { 
            //   //this.setSelectItem(invoice.i); 
            //   this.isnew = false;
            //   this.isShow=false;
            //   this.count++;
            // });
          });
        }
      });
    }
  }

  savesalelink(link:SaleLink[],index:number){
    if(index<link.length-1){
      if(link[index].dbid==-1){
        this.salelinkservice.addSaleLink(link[index]).then(()=>{
          if(link[index].linkType=='invoice'){
            this.invoiceservice.getInvoiceById(link[index].linkId).subscribe(invoice=>{
              let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Sale/Invoice', 'Add an Item Link to an Invoice', 'Invoice Number: '+invoice.invoiceNum+' Link Name: '+link[index].name+' Link Cat#: '+link[index].cat)
              this.userhistoryservice.addUserHistory(userhistory);
            })
          }
          else{
            this.quoteservice.getQuoteById(link[index].linkId).subscribe(quote=>{
              let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Sale/Quote', 'Add an Item Link to an Quote', 'Quote Number: '+quote.quoteNum+', Link Name: '+link[index].name+', Link Cat#: '+link[index].cat)
              this.userhistoryservice.addUserHistory(userhistory);
            })
          }

          this.savesalelink(link,index+1)
        })
      }
      else{
        this.salelinkservice.updateSaleLink(link[index]).then(()=>{
          if(link[index].linkType=='invoice'){
            this.invoiceservice.getInvoiceById(link[index].linkId).subscribe(invoice=>{
              let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Sale/Invoice', 'Upate Invoice Item Link', 'Invoice Number: '+invoice.invoiceNum+', Link Name: '+link[index].name+', Link Cat#: '+link[index].cat)
              this.userhistoryservice.addUserHistory(userhistory);
            })
          }
          else{
            this.quoteservice.getQuoteById(link[index].linkId).subscribe(quote=>{
              let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Sale/Quote', 'Upate Quote Item Link', 'Quote Number: '+quote.quoteNum+', Link Name: '+link[index].name+', Link Cat#: '+link[index].cat)
              this.userhistoryservice.addUserHistory(userhistory);
            })
          }
          this.savesalelink(link,index+1)
        })
      }

    }
    else{
      if(link[index].dbid==-1){
          this.salelinkservice.addSaleLink(link[index]).then(()=>{
            if(link[index].linkType=='invoice'){
              this.invoiceservice.getInvoiceById(link[index].linkId).subscribe(invoice=>{
                let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Sale/Invoice', 'Add an Item Link to an Invoice', 'Invoice Number: '+invoice.invoiceNum+', Link Name: '+link[index].name+', Link Cat#: '+link[index].cat)
                this.userhistoryservice.addUserHistory(userhistory);
              })
            }
            else{
              this.quoteservice.getQuoteById(link[index].linkId).subscribe(quote=>{
                let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Sale/Quote', 'Add an Item Link to an Quote', 'Invoice Number: '+quote.quoteNum+', Link Name: '+link[index].name+', Link Cat#: '+link[index].cat)
                this.userhistoryservice.addUserHistory(userhistory);
              })
            }
            if(this.currentTab=='Quote'){
              this.loadQuotePage();
            }
            else{
              this.loadInvoicePage();
            }

            this.isShow=false;
            this.enableEdit = false;
            this.isnew = false;
            this.count++;
        })
      }
      else{
        this.salelinkservice.updateSaleLink(link[index]).then(()=>{
          if(link[index].linkType=='invoice'){
            this.invoiceservice.getInvoiceById(link[index].linkId).subscribe(invoice=>{
              let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Sale/Invoice', 'Upate Invoice Item Link', 'Quote Number: '+invoice.invoiceNum+', Link Name: '+link[index].name+', Link Cat#: '+link[index].cat)
              this.userhistoryservice.addUserHistory(userhistory);
            })
          }
          else{
            this.quoteservice.getQuoteById(link[index].linkId).subscribe(quote=>{
              let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Sale/Quote', 'Upate Quote Item Link', 'Quote Number: '+quote.quoteNum+', Link Name: '+link[index].name+', Link Cat#: '+link[index].cat)
              this.userhistoryservice.addUserHistory(userhistory);
            })
          }
          if(this.currentTab=='Quote'){
            this.loadQuotePage();
          }
          else{
            this.loadInvoicePage();
          }
          this.isShow=false;
          this.enableEdit = false;
          this.isnew = false;
          this.count++;
        })
      }

    }
  }

  complateQuote(quote:Quote){
    this.quoteservice.updateQuote(quote).then(result=>{
      let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Sale/Invoice', 'Mark Quote Complete', 'Quote Number: '+quote.quoteNum)
      this.userhistoryservice.addUserHistory(userhistory);

      let index = this.displayObj.findIndex(x=>x.dbid == quote.dbid);
      if(index!=-1){
        this.displayObj[index]=quote;
      }
      
    })

  }

  updateQuote(quote: { q: Quote, link: SaleLink[] }) {
    if (this.isnew) {
      let quotenum = Number(quote.q.quoteNum);
      while (this.quotes.find(x => Number(x.quoteNum) == quotenum) !== undefined) {
        quotenum++;
      }
      quote.q.quoteNum = quotenum + '';
      this.quoteservice.addQuote(quote.q).then(_ => {
        let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Sale/Quote', 'Add a New Quote', 'Quote Number: '+quote.q.quoteNum)
        this.userhistoryservice.addUserHistory(userhistory);
        let id = _['dbid'];
        quote.q.dbid = id;
        //this.quotes.push(quote.q);
       //this.sortAndAssignQuote();
        quote.link.forEach(l => {l.linkId = id;});
       this.savesalelink(quote.link,0);
        //let p = []
        // quote.link.forEach(l => {
        //   l.linkId = id;
        //   l.linkType = 'quote';
        //   p.push(this.salelinkservice.addSaleLink(l));
        // });

        // Promise.all(p).then(_ => { 
        //   //this.setSelectItem(quote.q); 
        //   this.loadQuotePage();
        //   this.isShow=false;
        //   this.enableEdit = false;
        //   this.isnew = false;
        //   this.count++;
        // });


      });
    }
    else {
      //quote.q.revision += 1;
      quote.q.revisionDate = new Date();
      this.quoteservice.updateQuote(quote.q).then(_ => {
        // let i = this.quotes.findIndex(x => x.dbid == _['dbid]);
        // this.quotes[i] = quote.q;
        let i = this.displayObj.findIndex(x => x.dbid == _['dbid']);
        this.displayObj[i] = quote.q;
        //this.sortAndAssignQuote();
        quote.link.forEach(l => {l.linkId = quote.q.dbid;});
        this.savesalelink(quote.link,0);
        this.versionNumber = Number(quote.q.revision);
        this.changeVersionNumberModal=this.modalService.open(this.changeVersionNumberOption, { backdrop: "static", size: 'lg' });
        this.salelinkservice.loadAllLinkById(quote.q.dbid, 'quote').subscribe(dblinks => {
          let deletelist = [];
          dblinks.forEach(link => {
            if (quote.link.find(x => x.dbid == link.dbid) == undefined) {
              deletelist.push(link);
            }
          });
          deletelist.forEach(link => {
            this.salelinkservice.deleteSaleLink(link);
          })







          // let p = [];
          // quote.link.forEach(l => {
          //   if (l.dbid !== -1) {
          //     p.push(this.salelinkservice.updateSaleLink(l));
          //   }
          //   else {
          //     l.linkId = _['dbid'];
          //     l.linkType = 'quote';
          //     p.push(this.salelinkservice.addSaleLink(l));
          //   }
          // });
          // deletelist.forEach(link => {
          //   p.push(this.salelinkservice.deleteSaleLink(link));
          // })

          // Promise.all(p).then(_ => {
          //   //this.setSelectItem(quote.q);
          //   this.isShow=false;
          //   this.enableEdit = false;
          //   this.count++;
          // })
        });
      });
    }
  }

  validateVersoin(){
    let button =  <HTMLInputElement>document.getElementById('updateversion');
    if(this.versionNumber==null || this.versionNumber<=this.selectItem.revision || !Number.isInteger(this.versionNumber)){
      this.versionNumber = this.selectItem.revision;
      button.disabled = true;
      return;
    }

    if(this.versionNumber > this.selectItem.revision){
      button.disabled = false;
     }

  }

  updateVersionNumber(currentTab:string, selectItem:any, versionNumber:number){
    selectItem.revision = versionNumber.toString();
    if(currentTab=='Quote'){
      this.quoteservice.updateQuote(selectItem).then(_=>{
        this.changeVersionNumberModal.close();
        let i = this.displayObj.findIndex(x => x.dbid == selectItem.dbid);
        if(i!=-1){
          this.displayObj[i] = selectItem;
        }
      })
    }
    else{
      this.invoiceservice.updateInvoice(selectItem).then(_=>{
        this.changeVersionNumberModal.close();
        let i = this.displayObj.findIndex(x => x.dbid == selectItem.dbid);
        if(i!=-1){
          this.displayObj[i] = selectItem;
        }
      })
    }
    
  }
}