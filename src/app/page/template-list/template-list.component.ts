import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
export interface PeriodicElement {
  name: string;
  position: number;
  author: string;
  nav: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: "Worksheet One", author: "Wayne", nav: "template-1" },
  // {
  //   position: 2,
  //   name: "Worksheet Two",
  //   author: "Wayne",
  //   nav: "template-2"
  // },
  {
    position: 3,
    name: "Worksheet Three",
    author: "Duo",
    nav: "template-3"
  },
  {
    position: 4,
    name: "Worksheet Four",
    author: "Drew",
    nav: "template-4"
  },
  {
    position: 5,
    name: "Worksheet Five",
    author: "Not Available",
    nav: "template-5"
  },
  {
    position: 6,
    name: "Worksheet Six",
    author: "Not Available",
    nav: "template-6"
  },
  {
    position: 7,
    name: "Worksheet Seven",
    author: "Not Available",
    nav: "template-7"
  },
  {
    position: 8,
    name: "Worksheet Eight",
    author: "Not Available",
    nav: "template-8"
  },
  {
    position: 9,
    name: "Worksheet Nine",
    author: "Not Available",
    nav: "template-9"
  }
];

@Component({
  selector: "app-template-list",
  templateUrl: "./template-list.component.html",
  styleUrls: ["./template-list.component.css"]
})
export class TemplateListComponent implements OnInit {
  displayedColumns: string[] = ["position", "name", "author", "nav"];
  dataSource = ELEMENT_DATA;

  constructor(private location: Location) {}

  ngOnInit() {}

  goBack(): void {
    this.location.back();
  }
}
