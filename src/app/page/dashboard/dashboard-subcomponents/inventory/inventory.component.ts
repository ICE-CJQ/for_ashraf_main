import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InventoryService } from 'app/shared/services/Inventory.Service';
import { ItemdetailService } from 'app/shared/services/itemdetail.service';
import { item, ItemWDetails, itemdetail } from 'app/shared/objects/item';
import { trigger, state, style, animate, transition, keyframes} from '@angular/animations';
import { Enum } from 'app/shared/objects/Enum';


@Component({
  selector: 'dashboard-inventory',
  providers: [InventoryService, ItemdetailService],
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css'],
  animations: [
    trigger('routeAnimation', [
      state('true', style({
        backgroundColor: 'lightblue'
      })),
      transition('false => true', [
        animate('700ms 300ms', keyframes ([
          style({ opacity: 0.1, offset: 0.07 }),
          style({ opacity: 0.6, offset: 0.14 }),
          style({ opacity: 1,   offset: 0.35 }),
          style({ opacity: 0.2, offset: 0.49 }),
          style({ opacity: 1, backgroundColor: 'white', offset: 0.7}),
          style({ backgroundColor: 'lightblue', offset: 1})
        ]))
      ])
    ])
  ]
})

export class InventoryComponent implements OnInit {
   constructor(private inventoryService: InventoryService, private itemdetailService: ItemdetailService) { }
   @Output() routeDash = new EventEmitter();
   @Input() timeNow: number;
   @Input() doLogError: boolean;

   // For use in html
   TABLE = Enum.getDashboardRouteDestinations();
   _array = Array;

   // Route variables
   animateTable = this.TABLE.none;
   animateRow = -1;
   
   /***************** Data Models *****************/
   inventory: ItemWDetails[] = []; // Contains all inventory data, other models are a subset of this one
   expiredMaterialModel: itemdetail[]; // All Items (incl. non-active) with a valid expire-date and are in-stock
   stockWithThresoldModel: item[]; // SKUs that are active and have a threshold > 0
   stockNoThresholdModel: item[]; // SKUs without a valid threshold

   /****************** UI Control *****************/
   /*   1) Data for the tables in the current view
   /*   2) Table titles tables
   /*   3) Collapse
   /***********************************************/
   expiredMaterialModelCurr: itemdetail[];
   stockStatusModelCurr: item[];

   expiredMaterialTitle: string = "Expired Materials";
   stockStatusTitle: string = "Stock Status";

   expiredMaterialCollapsed:boolean = true;
   stockStatusCollapsed: boolean = true;


   ngOnInit() {
      this.loadInventoryData().then(_ => {
         this.displayInventoryView();
      });
   }

   private displayInventoryView() {
     // default: show materials that are at or under the stock requirement
     this.createStockModels();
     this.showStockStatus(true, 100);

     // default: show currently expired materials
     this.createExpiredMaterialsModel();
     this.showMaterialsExpiring(0); 
   }

   // underPercentRequired = 0 will return Out of Stock materials (0 amount and inuse)
   private showStockStatus(withThreshold: boolean, underPercentRequired: number = 100) {
     this.stockStatusTitle = (!withThreshold)?"Stock Status (Missing Threshold)":
     (underPercentRequired <= 0)?"Out of Stock":
     (underPercentRequired == 100)?"Low in Stock":"Under Stock Requirement ("+underPercentRequired+"% Threshold)";

     let requirement = underPercentRequired/100; this.stockStatusModelCurr = []; let result = [];
     if (withThreshold) {
       this.stockWithThresoldModel.forEach(SKU => {
         let amount = SKU.amount/+SKU.quantitythreshold;
         if (underPercentRequired <= 0 && SKU.inuse != 0) { return; } // For Out of Stock view only
         if (amount > requirement && Math.abs(amount-requirement) > 0.0001) { return; }
         this.stockStatusModelCurr.push(SKU);
         result.push(SKU);
       });
     }
     else {
       result = this.stockNoThresholdModel;
     }
     this.stockStatusModelCurr = result;
   }

   // Update UI model and table name
   // Table Inventory, Subview Expired Materials
   private showMaterialsExpiring(daysFromToday: number) {
     this.expiredMaterialTitle = (daysFromToday == 0)?"Expired Materials":(daysFromToday > 0)?
     "Soon to Expire (" + daysFromToday +" days)":"Already Expired ("+ daysFromToday + ") days";
     
     // Update table data
     let futureDate = this.timeNow + daysFromToday*1000*60*60*24; let done = false;
     this.expiredMaterialModelCurr = []; let index = 0; let result = [];
     while(!done && +this.expiredMaterialModel[index].expiredate < futureDate) {
       this.expiredMaterialModelCurr.push(this.expiredMaterialModel[index]); // Update view instantly
       result.push(this.expiredMaterialModel[index]);
       index++;
       if (index >= this.expiredMaterialModel.length) { done = true; }
     }
     // Check up to 24 hours later
     let dayOfWeek = new Date(futureDate).getDay();
     while(!done && +this.expiredMaterialModel[index].expiredate <= futureDate+1000*60*60*24) {
       if (dayOfWeek == new Date(+this.expiredMaterialModel[index].expiredate).getDay()) {
         this.expiredMaterialModelCurr.push(this.expiredMaterialModel[index]);
         result.push(this.expiredMaterialModel[index]);
       }
       index++;
       if (index >= this.expiredMaterialModel.length) { done = true; }
     }
     // Prevent errors caused by rapid successive user inputs
     this.expiredMaterialModelCurr = result;
   }

   private input_showStockStatus(event: any) {
      if (event && event.target && event.target.value && !isNaN(+event.target.value)) { 
         this.showStockStatus(true, +event.target.value);
      }
   }

   private input_showMaterialsExpiring(event: any) {
      if (event && event.target && event.target.value && !isNaN(+event.target.value)) { 
         this.showMaterialsExpiring(+event.target.value);
      }
   }

  /************************************************/
  /****************** Data Models *****************/
  /************************************************/


  // Returns a list of all SKUs that are at or under the inventory threshold
  // Only unopened inventory is use for comparision with threshold
  // Ignore SKUs that are not active
  private createStockModels() {
    let model: item[] = [];
    let noThresholdModel: item[] = [];

    this.inventory.forEach(SKU => {
      if (SKU.item == null) { return; }
      if (SKU.item.active !== true) { return; }
      (+SKU.item.quantitythreshold > 0)?model.push(SKU.item):noThresholdModel.push(SKU.item);
    });
    // Most important: amount/threshold, second: inuse, third: threshold
    model.sort(function(a,b){
      if (+a.quantitythreshold == +b.quantitythreshold) {return 0;}
      return ((+a.quantitythreshold < +b.quantitythreshold)?1:-1);
    });
    model.sort(function(a,b){
      if (a.inuse == b.inuse) {return 0;}
      return ((a.inuse < b.inuse)?-1:1);
    });
    model.sort(function(a,b){
      let _a = a.amount/+a.quantitythreshold;let _b = b.amount/+b.quantitythreshold;
      if (Math.abs(_a - _b) < 0.0001) {return 0;}
      return ((_a < _b)?-1:1);
    });
    this.stockWithThresoldModel = model;
    this.stockNoThresholdModel = noThresholdModel;
  }

  // Returns a list of all Items that have a valid expiredate and in stock
  // Track non-active items too
  private createExpiredMaterialsModel() {
    let model: itemdetail[] = [];
    this.inventory.forEach(SKU => {
      SKU.details.forEach(item => {
        // Ignore items with invalid Date
        if (item.expiredate == null || isNaN(+item.expiredate) || +item.expiredate == 0) { return; }

        // Only include items that we have in stock (open and unopened)
        if (+item.amount > 0 || +item.inuse > 0) { 
        model.push(item);
        }  
      });
    });
    // Sort by importance
    model.sort(function(a,b){
    return ((+a.expiredate-+b.expiredate>0)?1:-1);
    }); 
    this.expiredMaterialModel = model;
  }

   // Retrives all SKUs and Items and set value for SKU.amount and SKU.inuse
   // this.inventory[x].item corresonds to the SKU with dbid x (or null).
   // this.inventory[x].details corresonds to all Items (array) for a given SKU.
   private loadInventoryData() {
     let p = new Promise((resolve, reject) => {
       let promise1 = this.inventoryService.loadInventory().toPromise(); // Fetch all SKUs (aka Item) sorted by dbid
       let promise2 = this.itemdetailService.loadItemDetail().toPromise(); // Fetch all items (aka ItemDetail)
       
       // Wait for fetching to be done, then put all data into this.inventory
       Promise.all([promise1, promise2]).then(_ => {
         let SKUs: item[] = _[0];
         let items: itemdetail[] = _[1];

         // Save SKUs into inventory[SKU.dbid] and set default value for amount and inuse
         for (let i = 0; i < SKUs.length; i++) {
           let SKU: item = SKUs[i];
           SKU.amount = 0; SKU.inuse = 0; // Must set it to 0 (or it will become NaN)

           // Add empty elements as needed
           while (SKU.dbid > this.inventory.length) {
             this.inventory.push(new ItemWDetails(null, []));
           }
           this.inventory.push(new ItemWDetails(SKU, []));
         }
         // Add items to SKU and update SKU.amount and SKU.inuse
         items.forEach(item => {
           let id = item.itemdbid;
           if (isNaN(id) || id < 1) {this.logError("ItemDetail has invalid itemdbid",item);}
           else if (this.inventory[id].item == null) {
             //this.logError("Cannot find corresponding Item from ItemDetail #"+item.dbid,item);
             this.inventory[id].details.push(item);
           }
           else if (this.inventory[id].item.dbid != id) {this.logError("Loading error, failed to save item_details",item);}
           else {
               if (+item.amount > 0) {this.inventory[id].item.amount += (+item.amount);}
               if (+item.inuse > 0) {this.inventory[id].item.inuse += (+item.inuse);}
               this.inventory[id].details.push(item);
           } 
         });
         // Done getting data
         resolve();
       });
     });
     return p;
   }

   // This following 2 functions are called by parent component
   cancelRoute() {
     this.animateTable = this.TABLE.none;
   }
   getData(table: number, row: number):any {
     switch(table){
       case this.TABLE.inv_expired:
         return this.expiredMaterialModelCurr[row];
       case this.TABLE.inv_stock:
         return this.stockStatusModelCurr[row];
       default:
         return null;
     }
   }

   // Animate row if user holds mouse down
   // If animation completes, route user to different view
   private onClickTableData(table: number, dataRow: number) {
     this.animateTable = table;
     this.animateRow = dataRow;
   }

   private onAnimationDone(event: any) {
     if (event.fromState != false || event.toState != true) { return; }
     if (this.animateTable == this.TABLE.none) { return; }

     // Animation fully played, route user to differnt view
     this.route(this.animateTable, this.animateRow);
   }

   // Let Dashboard component handle the routing
   private route(fromTable: number, row: number) {
     this.routeDash.emit({fromTable:fromTable, row: row});
   }

   private logError(errorMsg:string, obj1:any=null, obj2:any=null) {
     if (this.doLogError === false) { return; }
     errorMsg = "[Db-Inv] " + errorMsg;
     if (obj2) {console.log(errorMsg, obj1, obj2);} 
     else if (obj1) {console.log(errorMsg, obj1);} 
     else {console.log(errorMsg);}
   }
}
