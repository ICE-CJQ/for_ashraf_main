import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InjectionService } from 'app/shared/services/injection.service';
import { ChickenService } from 'app/shared/services/chicken.Service';
import { Chicken, Injection } from 'app/shared/objects/Chicken';
import { trigger, state, style, animate, transition, keyframes} from '@angular/animations';
import { Enum } from 'app/shared/objects/Enum';


@Component({
  selector: 'dashboard-immunization',
  providers: [ChickenService, InjectionService],
  templateUrl: './immunization.component.html',
  styleUrls: ['../inventory/inventory.component.css'],
  animations: [
    trigger('routeAnimation', [
      state('true', style({
        backgroundColor: 'lightblue'
      })),
      transition('false => true', [
        animate('700ms 300ms', keyframes ([
          style({ opacity: 0.1, offset: 0.07 }),
          style({ opacity: 0.6, offset: 0.14 }),
          style({ opacity: 1,   offset: 0.35 }),
          style({ opacity: 0.2, offset: 0.49 }),
          style({ opacity: 1, backgroundColor: 'white', offset: 0.7}),
          style({ backgroundColor: 'lightblue', offset: 1})
        ]))
      ])
    ])
  ]
})
export class ImmunizationComponent implements OnInit {
   constructor(private chickenService: ChickenService, private injectionService: InjectionService) { }
   @Output() routeDash = new EventEmitter();
   @Input() timeNow: number;
   @Input() doLogError: boolean;

   // For use in html
   TABLE = Enum.getDashboardRouteDestinations();
   _array = Array;

   // Route variables
   animateTable = this.TABLE.none;
   animateRow = -1;

   /***************** Data Models *****************/
   chickens: Chicken[] = []; // chickens[x] corresonds to the chicken with dbid x (or null).
   chickenStructure: [Chicken[]] = [[]]; // chickenStructure[9][2] will return Chicken with ID 9-2
   chickenInvalidID: Chicken[] = []; // unordered list of chickens that don't have ID with number-number
   injections: Injection[] = []; // unordered list of all injections
   // Sorted list of incomplete injections where the associated chicken is still alive
   injectionReminder: {injection:Injection, chicken:Chicken, displayMessage:string}[]; 

   // For UI: current view
   injectionReminderCurr: {injection:Injection, chicken:Chicken, displayMessage:string}[];
   injectionCollapsed: boolean = true;
   injectionTitle: string = "Immunization Reminder";


   ngOnInit() {
      this.loadImmunizingData().then(_ => {
         this.createImmunizingModel();
         this.showImmunizingReminder(7);
      });
   }

   /*************************/
   /*    Immunizing View    */
   /*************************/

   // Retrieve all Injection and Chicken data
   // this.injections is an unordered list
   // this.chickens[x] corresonds to the chicken with dbid x (or null).
   private loadImmunizingData() {
     let p = new Promise((resolve, reject) => {
       let promise1 = this.injectionService.loadInjection().toPromise();
       let promise2 = this.chickenService.loadChicken().toPromise();

       Promise.all([promise1, promise2]).then(_ => {
         let injections = _[0];
         let chickens = _[1];
         this.injections = injections;

         for (let i = 0; i < chickens.length; i++) {
           let chicken: Chicken = chickens[i];

           while (chicken.dbid > this.chickens.length) {
             this.chickens.push(null); // Add empty elements as needed
           }
           this.chickens.push(chicken);
         } 
         this.createChickenStructure();
         resolve();
       });
     });
     return p;
   }

   private createImmunizingModel() {  
     let dayOfTheMonthNow: number = new Date(this.timeNow).getDate();
     let dayOfTomorrow: number = new Date(this.timeNow+1000*60*60*24).getDate();
     let model: {injection:Injection, chicken:Chicken, displayMessage:string}[] = [];

     this.injections.forEach(injection => {
       let message: string = null;
       let dayOfInjection = new Date(injection.injectdate).getDate();
       
       if (injection.complete === "Completed") {} // Ignore injections that have been completed
       else if (injection.complete !== "Not Complete") {this.logError("Chicken Injection has invalid status", injection);}
       else {
         // Make sure the associated chicken is valid
         let chicken = this.getChickenFromInjection(injection);
         if (chicken == null) {return;} // Error is logged in getChickenFromInjection()

         // Make sure chicken is alive
         if (chicken.chickenstatus == "Sacrificed") {return;}
         else if (chicken.chickenstatus != "Active") {
           this.logError("Chicken neither Sacrificed nor Active", chicken);
           return;
         }
         // Add injection to list, and set a message to display
         if (this.timeNow > +injection.injectdate) {
           message = "Injection Overdue";

           // Make sure injection is really overdue, or if it can still be done today
           if (this.timeNow-1000*60*60*24 < +injection.injectdate) { 
             if (dayOfTheMonthNow == dayOfInjection){
               message = "Today";
             }
           }
         }
         else if (dayOfTheMonthNow == dayOfInjection) {
           message = "Today";
         } 
         else if (dayOfTomorrow == dayOfInjection){
           message = "Tomorrow";
         }
         model.push({injection: injection, chicken:chicken, displayMessage: message});
       }
     });
     // Sort by importance
     model.sort(function(a,b) {
       return (a.injection.injectdate<b.injection.injectdate)?-1:1;
     });
     this.injectionReminder = model;
   }

   private showImmunizingReminder(reminderThresholdInDays: number) {
     this.injectionTitle = "Immunization Reminder ("+reminderThresholdInDays+" days)";

     let reminderThreshold: number = reminderThresholdInDays*1000*60*60*24;
     this.injectionReminderCurr = []; let result = [];

     for (var i = 0; i < this.injectionReminder.length; i++) {
       let injection = this.injectionReminder[i].injection;
       let timeTillInjection = +injection.injectdate - this.timeNow;

       // Add to list until Injection is not within threshold
       if (timeTillInjection > reminderThreshold) {
         break;
       }
       this.injectionReminderCurr.push(this.injectionReminder[i]);
       result.push(this.injectionReminder[i]);
     }
     this.injectionReminderCurr = result;
   }

   private input_showImmunizingReminder(event) {
     if (event && event.target && event.target.value && !isNaN(+event.target.value)) { 
       this.showImmunizingReminder(+event.target.value);
     }
   }

   private createChickenStructure() {
     let chickens = this.chickens;
     let structure: [Chicken[]] = [[]];
     for (let i=0; i<chickens.length; i++) {

       // Validate ChickenID
       let chicken = chickens[i];
       if (chicken == null) {continue;}
       if (chicken.chickenid == null) {this.logError("Missing ChickenID",chicken);continue;}
       if (!this.isValidChickenID(chicken.chickenid)) {
         this.chickenInvalidID.push(chicken);
         continue;
       }
       
       // Create Structure
       let idArray = chicken.chickenid.split('-');
       let num1 = +idArray[0];
       let num2 = +idArray[1];
       while (num1 > structure.length) {
         structure.push([]);
       }
       if (num1 == structure.length) {
         let innerArray = [];
         while (num2 > innerArray.length) {
           innerArray.push(null);
         }
         innerArray.push(chicken);
         structure.push(innerArray);
       }
       else {
         let innerArray = structure[num1];
         while (num2 > innerArray.length) {
           innerArray.push(null);
         }
         if (num2 == innerArray.length) {
           innerArray.push(chicken);
         }
         else {
           innerArray[num2] = chicken;
         }
       }
     }
     // Complete
     this.chickenStructure = structure;
   }

   private getChickenFromInjection(injection:Injection): Chicken {
     let id = injection.chickenid;
     if (id == null) {this.logError("Missing ChickenID",injection);return null;}

     if (this.isValidChickenID(id)) {
       let idArray = id.split('-');
       if (this.chickenStructure.length > +idArray[0] && this.chickenStructure[+idArray[0]][+idArray[1]]) {
        return this.chickenStructure[+idArray[0]][+idArray[1]];
       }
     }
     else {
       let chicken = this.chickenInvalidID.find(chicken => chicken.chickenid === id);
       if (chicken != undefined) {
         return chicken;
       }
     }
     this.logError("Could not find Chicken with ChickenID:"+id,injection);
     return null;
   }

   private isValidChickenID(chickenID: string): boolean {
     let idArray = chickenID.split('-');
     if (idArray.length!==2||idArray[0]==''||idArray[1]==''||isNaN(+idArray[0])||isNaN(+idArray[1])) {
       return false;
     } else { return true; }
   }

   // This following 2 functions are called by parent component
   cancelRoute() {
     this.animateTable = this.TABLE.none;
   }
   getData(table: number, row: number):any {
     switch(table){
       case this.TABLE.imm_reminder:
         return this.injectionReminder[row].injection;
       default:
         return null;
     }
   }

   // Animate row if user holds mouse down
   // If animation completes, route user to different view
   private onClickTableData(table: number, dataRow: number) {
     this.animateTable = table;
     this.animateRow = dataRow;
   }

   private onAnimationDone(event: any) {
     if (event.fromState != false || event.toState != true) { return; }
     if (this.animateTable == this.TABLE.none) { return; }

     // Animation fully played, route user to differnt view
     this.route(this.animateTable, this.animateRow);
   }

   // Let Dashboard component handle the routing
   private route(fromTable: number, row: number) {
     this.routeDash.emit({fromTable:fromTable, row: row});
   }

   private logError(errorMsg:string, obj1:any=null, obj2:any=null) {
     if (this.doLogError === false) { return; }
     errorMsg = "[Db-Imm] " + errorMsg;
     if (obj2) {console.log(errorMsg, obj1, obj2);} 
     else if (obj1) {console.log(errorMsg, obj1);} 
     else {console.log(errorMsg);}
   }
}
