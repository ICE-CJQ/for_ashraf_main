import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { AuthenticationService } from 'app/shared/services/Authenticate.Service';
import { QuoteService } from 'app/shared/services/Quote.Service';
import { RecipeService } from 'app/shared/services/Recipe.Service';
import { KitService } from 'app/shared/services/Kit.Service';
import { Quote } from 'app/shared/objects/Quote';
import { Recipe } from 'app/shared/objects/Recipe';
import { Kit } from 'app/shared/objects/Kit';
import { User } from 'app/shared/objects/User';
import { trigger, state, style, animate, transition, keyframes} from '@angular/animations';
import { InventoryComponent } from './dashboard-subcomponents/inventory/inventory.component';
import { ImmunizationComponent } from './dashboard-subcomponents/immunization/immunization.component';
import { Enum } from 'app/shared/objects/Enum';

enum SOP { none, review, approve }
enum KIT { none, review, approve }

@Component({
  selector: 'app-dashboard',
  providers: [KitService, RecipeService, QuoteService],
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  animations: [
    trigger('routeAnimation', [
      state('true', style({
        backgroundColor: 'lightblue'
      })),
      transition('false => true', [
        animate('700ms 300ms', keyframes ([
          style({ opacity: 0.1, offset: 0.07 }),
          style({ opacity: 0.6, offset: 0.14 }),
          style({ opacity: 1,   offset: 0.35 }),
          style({ opacity: 0.2, offset: 0.49 }),
          style({ opacity: 1, backgroundColor: 'white', offset: 0.7}),
          style({ backgroundColor: 'lightblue', offset: 1})
        ]))
      ])
    ])
  ]
})
export class DashboardComponent implements OnInit {
   constructor(private kitService: KitService, private recipeService: RecipeService, private quoteService: QuoteService, private auth: AuthenticationService) { }
   @ViewChild(InventoryComponent, {static: false}) invComponent!:InventoryComponent;
   @ViewChild(ImmunizationComponent, {static: false}) immComponent!:ImmunizationComponent;
   @Output() routeMain = new EventEmitter();

   user: User;
   timeNow: number; 
   doLogError = true;

   // For use in html
   _array = Array;
   TABLE = Enum.getDashboardRouteDestinations();
   SOP = SOP;
   KIT = KIT;

   showInventory = false;
   showImmunizingSchedule = false;
   showSOP = {show: false, type: SOP.none};
   showKIT = {show: false, type: KIT.none};
   showQuotes = false;
   showInvoice = false;

   // Route variables
   animateTable = this.TABLE.none;
   animateRow = -1;

   // Quotes variables
   quotes: Quote[] = []; // unsorted list of all quotes
   quotesReminderModel: Quote[]; // sorted list of quotes: not complete and have valid expiry date
   quotesReminderTitle: string = "Expire Soon";
   quotesReminderCurr: Quote[]; // For UI
   quotesExpired: Quote[]; // For UI, sorted list of expired quotes
   quotesRCollapsed = true;
   quotesECollapsed = true;

   // SOP variables
   recipes: Recipe[] = [];
   recipesToReview: Recipe[]; // For UI
   recipesToApprove: Recipe[]; // For UI
   recipesRCollapsed = true;
   recipesACollapsed = true;

   // KIT variables
   kits: Kit[] = [];
   kitsToReview: Kit[]; // For UI
   kitsToApprove: Kit[]; // For UI
   kitsRCollapsed = true;
   kitsACollapsed = true;

   ngOnInit() {
     // Can change these threshold variables
     let quotesExpiryThresholdInWeeks: number = 2;

     this.timeNow = Date.now();

     // Check user role to determine which views are activated
     this.enableViews().then(_ => {

       // For each active view, load data then populate table/view
       if (this.showQuotes === true) {
         this.loadQuotesData().then(_ => {
           this.createQuotesModels();
           this.showQuotesReminder(14);
         });
       }
       if (this.showSOP.show === true) {
         this.loadSOPData().then(_ => {
           this.displaySOPView();
         });
       }
       if (this.showKIT.show === true) {
         this.loadKITData().then(_ => {
           this.displayKITView();
         });
       }
     });
   }

   // Get called when mouse-up happens or mouse leaves table row
   private cancelRoute() {
     this.animateTable = this.TABLE.none;
     this.invComponent.cancelRoute();
     this.immComponent.cancelRoute();
   }

   // Animate row if user holds mouse down
   // If animation completes, route user to different view
   private onClickTableData(table: number, dataRow: number) {
     this.animateTable = table;
     this.animateRow = dataRow;
   }

   private onAnimationDone(event: any) {
     if (event.fromState != false || event.toState != true) { return; }
     if (this.animateTable == this.TABLE.none) { return; }

     // Animation fully played, route user to differnt view
     this.route(this.animateTable, this.animateRow);
   }

   // This gets called by subcomponents
   subcomponentRoute(route:{fromTable:number, row:number}) {
     this.route(route.fromTable, route.row);
   }

   // Let Main component handle the routing
   private route(fromTable: number, row: number) {
     let object: any = null;
     switch (fromTable) {
       case this.TABLE.inv_expired:
       case this.TABLE.inv_stock:
         object = this.invComponent.getData(fromTable, row);
         break;
       case this.TABLE.imm_reminder:
         object = this.immComponent.getData(fromTable, row);
         break;
       case this.TABLE.quotes_expired:
         object = this.quotesExpired[row];
         break;
       case this.TABLE.quotes_expireSoon:
         object = this.quotesReminderCurr[row];
         break;
       case this.TABLE.SOP_toReview:
         object = this.recipesToReview[row];
         break;
       case this.TABLE.SOP_toApprove:
         object = this.recipesToApprove[row];
         break;
       case this.TABLE.KIT_toReview:
         object = this.kitsToReview[row];
         break;
       case this.TABLE.KIT_toApprove:
         object = this.kitsToApprove[row];
         break;
       default:
         // Leave object as null
         break;
     }
     if (object == null) {this.logError("unknown route"); return;}
     this.routeMain.emit({fromTable:fromTable, object:object});
   }

   /*************************/
   /*      Permissions      */
   /*************************/

   private enableViews() {
     let p = new Promise((resolve, reject) => {

       //Set default/lowest permission
       this.showInventory = true;
       this.showImmunizingSchedule = true;
       this.showSOP.show = false;
       this.showKIT.show = false;
       this.showQuotes = false;
       this.showInvoice = false;

       this.auth.getCurrentUser().then(_=>{
         this.user = User.fromJson(_);
         switch(this.user.role) {
         case 'Administrator':
           this.showInventory = true;
           this.showImmunizingSchedule = true;
           this.showSOP = {show: true, type: SOP.approve};
           this.showKIT = {show: true, type: KIT.approve};
           this.showQuotes = true;
           this.showInvoice = true;
           break;  
         case 'Senior Manager':
           // Senior Manager has same permission as Manager
         case 'Manager':
           this.showInventory = true;
           this.showImmunizingSchedule = true;
           this.showSOP = {show: true, type: SOP.review};
           this.showKIT = {show: true, type: KIT.review};
           this.showQuotes = false;
           this.showInvoice = false;
           break;
         case 'User':
           // User has lowest permission, same as default
           break;
         default:
           this.logError('Error: User with invalid role', this.user);
           //If invalid user.role, do not change default permissions 
         }
         resolve();
       });
     });
     return p;
   }


   /*************************/
   /*       KIT View        */
   /*************************/  

  private loadKITData() {
    let p = new Promise((resolve, reject) => {
      this.kitService.loadKit().toPromise().then(kits => {
        this.kits = kits;
        resolve();
      });
    });
    return p;
  }


  private displayKITView() {
    let reviewModel = [];
    let approveModel = [];

    this.kits.forEach(kit => {
      if (!kit.reviewedby || kit.reviewedby.trim().length == 0) { 
        if (kit.approvedby && kit.approvedby.trim().length != 0) { this.logError("Kit approved before review", kit); return;}
        // Save only Kits that have not been reviewed nor approved yet.
        reviewModel.push(kit);
      } 
      else if (!kit.approvedby || kit.approvedby.trim().length == 0){
        // Save Kits that have been reviewed but not approved.
        approveModel.push(kit);
      }
    });
    this.kitsToReview = reviewModel;
    this.kitsToApprove = approveModel;
  }


   /*************************/
   /*       SOP View        */
   /*************************/  

  private loadSOPData() {
    let p = new Promise((resolve, reject) => {
      this.recipeService.loadRecipe().toPromise().then(recipes => {
        this.recipes = recipes;
        resolve();
      });
    });
    return p;
  }

  private displaySOPView() {
    let reviewModel = [];
    let approveModel = [];

    this.recipes.forEach(recipe => {
      if (!recipe.reviewedby || recipe.reviewedby.trim().length == 0) { 
        if (recipe.approvedby && recipe.approvedby.trim().length != 0) { this.logError("Recipe approved before review", recipe); return;}
        // Save only recipes that have not been reviewed nor approved yet.
        reviewModel.push(recipe);
      } 
      else if (!recipe.approvedby || recipe.approvedby.trim().length == 0){
        // Save recipes that have been reviewed but not approved.
        approveModel.push(recipe);
      }
    });
    this.recipesToReview = reviewModel;
    this.recipesToApprove = approveModel;
  }


   /*************************/
   /*      Quotes View      */
   /*************************/

  private loadQuotesData() {
    let p = new Promise((resolve, reject) => {
      this.quoteService.loadQuote().toPromise().then(quotes => {
        this.quotes = quotes;
        resolve();
      });
    });
    return p;
  }

  // Creates 2 models
  // Ignores quotes that are completed
  // (1) Quotes that are active and have a valid expiry date
  // (2) Quotes that are expired
  private createQuotesModels() {
    let modelActive: Quote[] = []; let modelExpired: Quote[] = [];
    
    this.quotes.forEach(quote => {
     if (quote.complete === true) { return; }
     if (quote.expireDate == null || isNaN(+quote.expireDate) || +quote.expireDate == 0) { return; }
     
     let timeTillExpire = +quote.expireDate - this.timeNow;
       if (timeTillExpire > 0) {
         modelActive.push(quote);
       }
       else {
         modelExpired.push(quote);
       }
    });
    // Sort by importance
    modelActive.sort(function(a,b){
     return ((+a.expireDate-+b.expireDate>0)?1:-1);
    }); 
    modelExpired.sort(function(a,b){
     return ((+a.expireDate-+b.expireDate>0)?-1:1);
    });
    this.quotesReminderModel = modelActive;
    this.quotesExpired = modelExpired; // Displayed in UI
  }

  private showQuotesReminder(expireInDays: number) {
    if (expireInDays <= 0) { expireInDays = 14; }
    this.quotesReminderTitle = "Expire Soon ("+expireInDays+" days)";

    let expiryThreshold: number = expireInDays*1000*60*60*24;
    this.quotesReminderCurr = []; let result = [];

    for (var i = 0; i < this.quotesReminderModel.length; i++) {
      let quote = this.quotesReminderModel[i];
      let timeTillExpire = +quote.expireDate - this.timeNow;
      if (timeTillExpire > expiryThreshold) { break; } // Add to list until Quote is not within threshold
      this.quotesReminderCurr.push(quote); 
      result.push(quote);
    }
    this.quotesReminderCurr = result;
  }

   private input_showQuotesReminder(event) {
     if (event && event.target && event.target.value && !isNaN(+event.target.value)) { 
       this.showQuotesReminder(+event.target.value);
     }
   }

   private logError(errorMsg:string, obj1:any=null, obj2:any=null) {
     if (this.doLogError === false) { return; }
     errorMsg = "[Db] " + errorMsg;
     if (obj2) {console.log(errorMsg, obj1, obj2);} 
     else if (obj1) {console.log(errorMsg, obj1);} 
     else {console.log(errorMsg);}
   }
}
