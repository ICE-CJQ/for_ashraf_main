import { Component, OnInit} from '@angular/core';
import { ClientCompany, ClientContact, CompanyWContacts } from '../../shared/objects/Client';
import { ClientService } from '../../shared/services/Client.Service';
import { NgbModal } from '../../../../node_modules/@ng-bootstrap/ng-bootstrap';
import * as XLSX from 'xlsx';
import { UserhistoryService } from '../../shared/services/userhistory.service';
import { Userhistory } from '../../shared/objects/Userhistory';
import { User } from 'app/shared/objects/User';
import { AuthenticationService } from '../../shared/services/Authenticate.Service';

@Component({
  providers: [ClientService, UserhistoryService],
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css'],
  host: {
    '(document:click)': 'onClick($event)',
  }
})
export class ClientComponent implements OnInit {
  clients: ClientCompany[];
  saleRep: ClientContact[][] = [];
  filterclients: ClientCompany[];
  display: CompanyWContacts[] = [];
  viewColumn: boolean[];
  columnList: string[];
  selectClient: CompanyWContacts;
  isnew: boolean = false;
  showClient: boolean;
  hasHeader: boolean;
  page: number;
  search: string = '';
  searchArea: string = '';
  isLoading: boolean;
  count: number;
  currentuser: User;

  onClick(event) {
    if (!this.isnew && event.path !== undefined && event.path.find(x => x.id !== undefined && x.className.includes('companyRecord')) !== undefined) {
      let targetpath = event.path.find(x => x.className.includes('companyRecord'));
      if(targetpath == undefined) return;
      let com = targetpath.id;
      this.selectClient = this.display.find(x => x.company.dbid == com);
      this.showClient = true;
    }
    else if (event.path !== undefined && event.path.find(x => x.className !== undefined && x.className == 'slide') !== undefined){
      return;
    }
    else if(event.path !== undefined && event.path.find(x => x.innerText !==undefined && x.innerText.trim() == 'New Client') !== undefined){
      this.newClient();
      return;
    }
    else {
      if (!this.isnew) {
        this.showClient = false;
        this.selectClient = undefined;
      }
    }
  }

  constructor(private clientService: ClientService, private modalService: NgbModal, private userhistoryservice: UserhistoryService, private authenticationservice: AuthenticationService) { }

  ngOnInit() {
    let getcurrentuser = this.authenticationservice.getCurrentUser()
    if (getcurrentuser !== undefined) {
      getcurrentuser.then(_ => {
        this.currentuser = User.fromJson(_);
      });
    }
    this.viewColumn = [true, true, true, true]
    this.columnList = ['Company', 'Shipping Address', 'Billing Address', 'Contact'];
    this.saleRep = [];
    this.page = 1;
    this.search = '';
    this.searchArea = 'All';
    this.isLoading = true;
    this.showClient = false;
    this.clientService.count().subscribe(c => {
      this.count = c;
      this.loadPage();
    })
    // this.clientService.loadClient().subscribe(clientList => {
    //   clientList.forEach(c => {
    //     let na = ''
    //     c.address.split('\\n').forEach(l => {
    //       na += l + '\n';
    //     })
    //     c.address = na;
    //     na = ''
    //     c.baddress.split('\\n').forEach(l => {
    //       na += l + '\n';
    //     })
    //     c.baddress = na;
    //     this.getClientContact(c.dbid);
    //   });

    //   this.clients = clientList.slice();
    // });



    this.hasHeader = true;
  }

  loadPage() {
    this.isLoading = true;
    this.clientService.loadClientWContactByPage(this.page - 1).subscribe(cwcon => {
      this.display = cwcon;
      this.isLoading = false;
    });
  }

  searchWPage(key: string) {
    this.search = key;
    if (this.search == '') {
      this.page = 1;
      this.loadPage();
      return;
    }
    this.isLoading = true;
    this.clientService.searchByPage(key, this.page - 1, this.searchArea).subscribe(cwcon => {
      this.display = cwcon;
      this.isLoading = false;
    })
  }

  // getClientContact(id: number) {
  //   if (this.saleRep == undefined) this.saleRep = [];
  //   this.saleRep[id] = [];
  //   this.clientService.getContactByCompany(id).subscribe(contacts => {
  //     contacts.forEach(co => {
  //       this.saleRep[id].push(co);
  //     })
  //   })
  // }

  newClient() {
    this.isnew = true;
    this.selectClient = new CompanyWContacts(new ClientCompany(-1, '', '', '', '', '', '', '', '', '', '', '', '', '', this.currentuser.name, new Date()), []);
    this.showClient = true;
  }

  cancelAdd(){
    this.selectClient = undefined; 
    this.isnew = false;
    this.showClient = false;
    
  }

  open(content) {
    this.modalService.open(content, { backdrop: "static", size: 'lg' }).result.then((result) => {
      this.showClient = false;
      this.isnew = false;
      if (result = 'closedel') {
        this.selectClient = undefined
      }
    });
  }

  toggleViewCol(index: number) {
    this.viewColumn[index] = !this.viewColumn[index];
  }

  saveClient(client: { com: ClientCompany, contact: ClientContact[] }) {
    if (client == undefined || client.com == undefined) return;
    if (client.contact == undefined) client.contact = [];

    this.clientService.loadClient().subscribe(result=>{
      this.clients=result;
      if (client.com.dbid == -1 && this.clients.find(x => x.company == client.com.company) == undefined) {
        this.clientService.addClient(client.com).then(_ => {
          client.com.dbid = _['dbid'];

          let p = [];
          client.contact.forEach(c => {
            if (c !== undefined) {
              c.companyId = client.com.dbid;
              p.push(this.clientService.addContact(c));
            }
          });
          Promise.all(p).then(_ => { 
            let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Client', 'Add a New Client', 'Company name: '+client.com.company)
            this.userhistoryservice.addUserHistory(userhistory);
            this.selectClient = new CompanyWContacts(client.com, client.contact);
            // this.getClientContact(this.selectClient.dbid);
            this.isnew = false;
          });
        })
      }
      else {
        let i = this.clientService.updateClient(client.com).then(_ => {
          let i = this.display.findIndex(x => x.company.dbid == client.com.dbid)
          if (i !== -1) {
            this.display[i].company = client.com;
            // this.filter('');
          }
          let p = [];
          this.clientService.getContactByCompany(client.com.dbid).subscribe(original => {
            let del = [];
            if (original !== undefined)
              original.forEach(x => {
                if (client.contact.find(y => y.dbid == x.dbid) == undefined) {
                  del.push(x);
                }
              });
  
            client.contact.forEach(c => {
              c.companyId = client.com.dbid;
              if (c.dbid == -1) {
                p.push(this.clientService.addContact(c));
              }
              else if (c.dbid !== -1) {
                p.push(this.clientService.updateContact(c));
              }
            });
  
            del.forEach(c => {
              if (c !== undefined && c.dbid !== -1){
                p.push(this.clientService.deleteContact(c))
              }
            })
  
            Promise.all(p).then(_ => {
              let userhistory=new Userhistory(-1, new Date(), this.currentuser.name, 'Client', 'Update a Client', 'Company name: '+client.com.company)
              this.userhistoryservice.addUserHistory(userhistory);
              this.selectClient = new CompanyWContacts(client.com, client.contact);
              this.display[i].contacts = client.contact;
              this.isnew = false;
              // this.getClientContact(this.selectClient.dbid);
            });
          });
        })
      }
    })


    this.selectClient = undefined;
    this.isnew = false;
    this.showClient=false;
  }

  // removeClient() {
  //   if (this.selectClient == undefined) return;
  //   let i = this.filterclients.findIndex(x => x.dbid == this.selectClient.dbid);
  //   if (i !== -1)
  //     this.filterclients.splice(i, 1);
  //   i = this.clients.findIndex(x => x.dbid == this.selectClient.dbid);
  //   if (i !== -1)
  //     this.clients.splice(i, 1);
  //   this.clientService.deleteClient(this.selectClient).then(_ => {
  //     this.selectClient = undefined;
  //   });
  // }

  // filter(key: string) {
  //   this.search = key;
  //   if (key == undefined || key.trim() == '') {
  //     this.filterclients = this.clients.slice();
  //     return;
  //   }
  //   this.filterclients = [];
  //   key = key.toLocaleLowerCase()
  //   this.clients.forEach(c => {
  //     if ((c.company !== undefined && c.company.toLocaleLowerCase().includes(key)) ||
  //       (c.address !== undefined && c.address.toLocaleLowerCase().includes(key)) ||
  //       (c.address2 !== undefined && c.address2.toLocaleLowerCase().includes(key)) ||
  //       (c.city !== undefined && c.city.toLocaleLowerCase().includes(key)) ||
  //       (c.province !== undefined && c.province.toLocaleLowerCase().includes(key)) ||
  //       (c.country !== undefined && c.country.toLocaleLowerCase().includes(key)) ||
  //       (c.postalCode !== undefined && c.postalCode.toLocaleLowerCase().includes(key)) ||
  //       (c.baddress !== undefined && c.baddress.toLocaleLowerCase().includes(key)) ||
  //       (c.baddress2 !== undefined && c.baddress2.toLocaleLowerCase().includes(key)) ||
  //       (c.bcity !== undefined && c.bcity.toLocaleLowerCase().includes(key)) ||
  //       (c.bprovince !== undefined && c.bprovince.toLocaleLowerCase().includes(key)) ||
  //       (c.bcountry !== undefined && c.bcountry.toLocaleLowerCase().includes(key)) ||
  //       (c.bpostalCode !== undefined && c.bpostalCode.toLocaleLowerCase().includes(key))) {
  //       this.filterclients.push(c);
  //     }
  //   });
  // }

  // loadFile(event) {
  //   let filename: File = event.target.files[0];
  //   if (filename == undefined) return;
  //   let read = new FileReader();
  //   read.onload = file => {
  //     let text = read.result;
  //     let data = new Uint8Array(text);
  //     let arr = new Array();
  //     for (let i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
  //     let bstr = arr.join("");
  //     let workbook = XLSX.read(bstr, { type: "binary" });
  //     let companyInfo = this.getDataArrayFromXML(workbook.Sheets['Formated']);
  //     let exhead: { type: string, value: any }[] = [];
  //     let newClients: { com: ClientCompany, contact: ClientContact[] }[] = [];
  //     let c: ClientCompany[] = [];
  //     if (companyInfo !== undefined)
  //       companyInfo.forEach((row, index) => {
  //         if (index == 0) {
  //           exhead = row;
  //         }
  //         else {
  //           let nco = new ClientCompany(-1, '', '', '', '', '', '', '', '', '', '', '', '', '');
  //           row.forEach((col, i) => {
  //             if (exhead[i].value == 'Company') nco.company = col.value;
  //             if (exhead[i].value == 'Billing Street Address 1') nco.baddress = col.value;
  //             if (exhead[i].value == 'Billing Street Address 2') nco.baddress2 = col.value;
  //             if (exhead[i].value == 'Billing City') nco.bcity = col.value;
  //             if (exhead[i].value == 'Billing States/Province') nco.bprovince = col.value;
  //             if (exhead[i].value == 'Billing Country') nco.bcountry = col.value;
  //             if (exhead[i].value == 'Billing Postal Code') nco.bpostalCode = col.value;
  //             if (exhead[i].value == 'Shipping Street Address 1') nco.address = col.value;
  //             if (exhead[i].value == 'Shipping Street Address 2') nco.address2 = col.value;
  //             if (exhead[i].value == 'Shipping City') nco.city = col.value;
  //             if (exhead[i].value == 'Shipping States/Province') nco.province = col.value;
  //             if (exhead[i].value == 'Shipping Country') nco.country = col.value;
  //             if (exhead[i].value == 'Shipping Postal Code') nco.postalCode = col.value;
  //           });
  //           if (nco.company !== undefined && nco.company.trim() !== '' &&
  //             nco.address !== undefined && nco.address.trim() !== '' &&
  //             nco.city !== undefined && nco.city.trim() !== '' &&
  //             nco.country !== undefined && nco.country.trim() !== '' &&
  //             nco.baddress !== undefined && nco.baddress.trim() !== '' &&
  //             nco.bcity !== undefined && nco.bcity.trim() !== '' &&
  //             nco.bcountry !== undefined && nco.bcountry.trim() !== '') {
  //             c.push(nco);
  //           }
  //         }
  //       });

  //     c.forEach(comre => {
  //       if (comre.company !== undefined && comre.company.trim() !== '' &&
  //         this.clients.find(x => x.company.toLocaleLowerCase() == comre.company.toLocaleLowerCase()) == undefined) {
  //         newClients.push({ com: comre, contact: [] });
  //       }
  //     });

  //     let contactInfo = this.getDataArrayFromXML(workbook.Sheets['Contact_Email_Company']);
  //     let cnthead: { type: string, value: any }[] = [];
  //     if (contactInfo !== undefined)
  //       contactInfo.forEach((row, index) => {
  //         if (index == 0) {
  //           cnthead = row;
  //         }
  //         else {
  //           let ncnt = new ClientContact(-1, -1, '', '', '', '', '', '');
  //           let comIndex = -1;
  //           row.forEach((col, i) => {
  //             if (cnthead[i].value == 'Company') {
  //               comIndex = newClients.findIndex(x => x.com.company.toLocaleLowerCase() == col.value.toLocaleLowerCase());
  //             }
  //             if (cnthead[i].value == 'Contact') ncnt.name = col.value;
  //             if (cnthead[i].value == 'Role') ncnt.role = col.value;
  //             if (cnthead[i].value == 'Email') ncnt.email = col.value;
  //             if (cnthead[i].value == 'Phone') ncnt.phone = col.value;
  //             if (cnthead[i].value == 'Fax') ncnt.fax = col.value;
  //             if (cnthead[i].value == 'Ext') ncnt.ext = col.value;
  //           });

  //           if (comIndex !== -1 && ncnt.name !== undefined && ncnt.name.trim() !== '') {
  //             newClients[comIndex].contact.push(ncnt);
  //           }
  //         }
  //       });
  //     newClients.forEach(record => {
  //       this.saveClient(record)
  //     })
  //   };

  //   read.readAsArrayBuffer(filename);
  // }

  getDataArrayFromXML(worksheet: XLSX.WorkSheet): { type: string, value: any }[][] {
    if (worksheet == undefined) return;
    let result: { type: string, value: any }[][] = [];
    let range = XLSX.utils.decode_range(worksheet['!ref']);
    for (let rowNum = range.s.r; rowNum <= range.e.r; rowNum++) {
      let row = [];
      for (let colNum = range.s.c; colNum <= range.e.c; colNum++) {
        var nextCell = worksheet[XLSX.utils.encode_cell({ r: rowNum, c: colNum })];
        if (nextCell === undefined) {
          row.push({ type: 's', value: '' });
        }
        else {
          let val = nextCell.w;
          if (nextCell.t == 'd') {
            let date = new Date(nextCell.w);
            let dom = date.getDate();
            val = date.getMonth + '/' + (dom < 10 ? '0' + dom : dom) + '/' + date.getFullYear();
          }
          row.push({ type: nextCell.t, value: val });

        }
        // row.push(nextCell.w)
      }
      let isAllEmpty = true;
      row.forEach(val => { if (val.value !== '') isAllEmpty = false; })

      if (!isAllEmpty)
        result.push(row);
    }
    return result;
  }
}
