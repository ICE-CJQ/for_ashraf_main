import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { Route } from "@angular/router";
import { RouterModule, Routes } from '@angular/router';
import {AuthGuard} from './shared/services/AuthGuard'
/* not necessary
const indexRoute: Route =
{
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
};

const fallbackRoute: Route =
{
    path: '**',
    redirectTo: '/login',
    pathMatch: 'full'
};*/

export const routerConfig: Route[] = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'resetpassword',
        children: [
            {
              path: ':token',
              component: ResetpasswordComponent
            }
          ]
    },
    {
        path: 'main',
        component: MainComponent,
        canActivate: [AuthGuard]
    },
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
    },
    {
        path: '**',
        redirectTo: '/login',
        pathMatch: 'full'
    }
];