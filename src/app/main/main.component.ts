import { NgbTabset, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Kit } from '../shared/objects/Kit';
import { Recipe, Ingredient } from '../shared/objects/Recipe';
import { orderitem } from '../shared/objects/OrderItem';
import { Component, ElementRef, Input, OnChanges, OnInit, ViewChild, Directive, HostListener, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { RecipeService } from '../shared/services/Recipe.Service';
import { IngredientService } from '../shared/services/Ingredient.Service';
import { AuthenticationService } from '../shared/services/Authenticate.Service';
import { User } from '../shared/objects/User';
import { SettingService } from '../shared/services/Setting.Service';
import { Setting } from '../shared/objects/Setting';
import { item, itemdetail } from '../shared/objects/item';
import { UserhistoryService } from 'app/shared/services/userhistory.service';
import { Userhistory } from 'app/shared/objects/Userhistory';
import { Eggtransfer } from 'app/shared/objects/eggtransfer';
import { Chicken } from 'app/shared/objects/Chicken';
import { Enum } from 'app/shared/objects/Enum';
import { ResponsiveService } from 'app/shared/services/responsive.service';


@Component({
  selector: 'app-main',
  providers: [RecipeService, IngredientService, SettingService, UserhistoryService, ResponsiveService],
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, OnChanges {

  routeDestinations = Enum.getDashboardRouteDestinations();

  showmultiplerequests: boolean;
  isShoworderItem: boolean;
  orderitem: orderitem;

  showRecipe: boolean;
  displayRecipe: any;

  showKit: boolean;
  kit: Kit;
  showProduct: boolean;
  product: Kit;

  //inventories of recipe, ingredients and requests
  recipes: Recipe[] = [];
  ingredients: Ingredient[] = [];
  requests: orderitem[];

  //catalog window
  catalogViewMode: string = 'search';
  catFocus: boolean;
  enableEditCatSetting: boolean;

  //user
  user: User;
  needChangePass: boolean;
  userIsManager = false; // controls what pages the user has access to

  showuser: boolean = false;
  currentTab: string = 'dboard';
  showAll: boolean = false;
  lock: boolean = false;

  item: item;
  newitemdetail:itemdetail;
  showItem: boolean;
  showItemdetail:boolean;
  edititem:boolean;
  edititemdetail:boolean;
  eggtransfer:Eggtransfer;

  // Egg Inventory input
  showChicken = false;
  chicken: Chicken;
  public isTablet: Boolean;

  showMenu=false;
  constructor(private recipeService: RecipeService, private ingredientService: IngredientService,
    private auth: AuthenticationService, private router: Router, private modalService: NgbModal, 
    private settingservive:SettingService, private userhistoryservice: UserhistoryService, private responsiveService:ResponsiveService) {
  }

  ngOnInit() {
    // this.onResize();
    // this.responsiveService.checkWidth();
    if(window.innerWidth<=1000 && window.innerWidth>=768){
      this.isTablet=true;
    }

    let c = this.auth.getCurrentUser();
    c !== undefined ? c.then(_ => {
      this.user = User.fromJson(_);

      this.userIsManager = false;
      if (this.user && this.user.role) {
        switch(this.user.role.toLowerCase()) {
          case "administrator":
          case "senior manager":
          case "manager":
            this.userIsManager = true;
            break;
          case "user":
          default:
            // leave user as not manager
            break;
        }
      }
    }) : this.router.navigate(['/login']);
    
    this.recipeService.loadRecipe().subscribe(recipes => { this.recipes = recipes });
    this.ingredientService.loadIngredient().subscribe(ingredients => { this.ingredients = ingredients });
    this.settingservive.getSettingByPage('general').subscribe(config=>{
      if (config == undefined || config.length < 1) {
        config = Setting.getGeneralSetting();
        config.forEach(s => {
          this.settingservive.addSetting(s);
        });
      }
    });
    this.settingservive.getSettingByPage('invent').subscribe(config=>{
      if (config == undefined || config.length < 1) {
        config = Setting.getInventorySetting();
        config.forEach(s => {
          this.settingservive.addSetting(s);
        });
      }
    });
    this.settingservive.getSettingByPage('kit').subscribe(config=>{
      if (config == undefined || config.length < 1) {
        config = Setting.getKitSetting();
        config.forEach(s => {
          this.settingservive.addSetting(s);
        });
      }
    });
    this.settingservive.getSettingByPage('orderItem').subscribe(config=>{
      if (config == undefined || config.length < 1) {
        config = Setting.getOrderItemSetting();
        config.forEach(s => {
          this.settingservive.addSetting(s);
        });
      }
    });
    this.settingservive.getSettingByPage('catalog').subscribe(config=>{
      if (config == undefined || config.length < 1) {
        config = Setting.getCatSetting();
        config.forEach(s => {
          this.settingservive.addSetting(s);
        });
      }
    });
    this.settingservive.getSettingByPage('sale').subscribe(config=>{
      if (config == undefined || config.length < 1) {
        config = Setting.getSaleSetting();
        config.forEach(s => {
          this.settingservive.addSetting(s);
        });
      }
    });
  }

  // Routing from dashboard component to another component
  dashboardRoute(route:{fromTable:number, object:any}) {
    this.reset();
    switch (route.fromTable) {
      case this.routeDestinations.inv_expired:
        this.showItemdetail = true;
        this.newitemdetail = route.object;
        this.currentTab="inven";
        break;
      case this.routeDestinations.inv_stock:
        this.requests = [orderitem.fromItem(route.object)];
        this.showmultiplerequests = true;
        this.currentTab = "order";
        //this.showItem = true;
        //this.item = route.object;
        //this.currentTab="inven";
        break;
      case this.routeDestinations.imm_reminder:
        //this.showChicken = true;
        //this.chicken = route.object;
        this.currentTab="egg inventory";
        break;
      case this.routeDestinations.quotes_expired:
      case this.routeDestinations.quotes_expireSoon:
        this.currentTab="sale";
        break;
      case this.routeDestinations.SOP_toReview:
      case this.routeDestinations.SOP_toApprove:
        this.showRecipe = true;
        this.displayRecipe = route.object;
        this.currentTab="sop";
        break;
      case this.routeDestinations.KIT_toReview:
      case this.routeDestinations.KIT_toApprove:
        this.showKit = true;
        this.kit = route.object;
        this.currentTab="kit";
        break;
      default:
        console.log("Failed to route from Main", route);
        break;
    }
  }

  ngOnChanges(change: SimpleChanges) {
  }

  onResize() {
    this.responsiveService.getTabletStatus().subscribe(isTablet => {
      this.isTablet = isTablet;
    });
  }

  closeMenu() {
    setTimeout(_ => { this.lock ? '' : this.showAll = false }, 1000);
  }

  closeUserPanel() {
    setTimeout(_ => { this.showuser = false }, 500);
  }

  changeMenuStatus(){
    if(this.showMenu==true){
      this.showMenu=false;
      this.hidemenu();
    }
    else{
      this.showMenu=true;
      this.showmenu();
    }
  }

  showmenu(){
    document.getElementById("menu").style.width = '90px';
    document.getElementById("menu").style.transition = "linear 0.5s";
  }

  hidemenu(){
    document.getElementById("menu").style.width = '20px';
    document.getElementById("menu").style.transition = "linear 0.5s";
  }

  open(content) {
    this.modalService.open(content, { size: 'sm' }).result.then((result) => {
    }, (reason) => {
    });
  }

  //get emit from dashboard by clicking on item of inventory to open this item
  setProduct(param) {
    this.clearProduct();
    if (param == undefined) return;
    this.showProduct = true;
    this.product = param;
  }

  clearProduct() {
    this.showProduct = false;
    this.product = undefined;
  }

  setKit(param) {
    this.clearKit();
    if (param == undefined) return;
    this.showKit = true;
    this.kit = param;
  }

  clearKit() {
    this.showKit = false;
    this.kit = undefined;
  }

  openorderrequests(requests) {
    if (requests == undefined) return;
    this.showmultiplerequests = true;
    this.requests = requests;
  }

  transtoitem(event){
    this.eggtransfer=event.eggtransfer;
    this.item=event.item;
    this.showItem=true;
    this.edititem=true;
  }
  transtoitemdetail(event){
    this.eggtransfer=event.eggtransfer;
    this.newitemdetail=event.itemdetail;
    this.showItemdetail=true;
    this.edititemdetail=true;
  }

  changetorecipe(recipe) {
    this.showRecipe = true;
    this.displayRecipe = recipe;
  }

  reset() {
    this.showItem = false;
    this.item = undefined;
    this.showRecipe = false;
    this.displayRecipe = undefined;
    this.isShoworderItem = false;
    this.orderitem = undefined;
    this.showmultiplerequests = false;
    this.requests = undefined;
    this.showItemdetail = false;
    this.newitemdetail = undefined;
    this.eggtransfer = undefined;
    this.showKit = false;
    this.kit = undefined;
    this.edititem = false;
    this.edititemdetail = false;
    this.showChicken = false;
    this.chicken = undefined;
  }

  getrequests(requests) {
    this.requests = requests;
  }

  //get emit from dashboard by clicking on orderitem of orders to open this orderitem
  setsingleorder(orderitem) {
    this.isShoworderItem = true;
    this.orderitem = orderitem;
  }

  signout() {
    this.auth.logout()
        let userhistory=new Userhistory(-1, new Date(), this.user.name, 'Main', 'Log Out', 'User Log Out');
        localStorage.clear();
        sessionStorage.clear();
        this.userhistoryservice.addUserHistory(userhistory);
        this.router.navigate(['/login']);
  }

  // updatePass(p:string){
  //   this.user.password = p;
  //   this.auth.updatePass(this.user).then(_=>{
  //     this.router.navigate(['/main']);
  //   })
  // }

  
  // @HostListener('window:beforeunload', ['$event'])
  // setTimeout(() => {
  //   window.alert('are you sure to beofreunload?')
  // }, 2000);
    
  // }

  // @HostListener('window:unload', ['$event'])
  //   window.alert('are you sure to unload?')
  // }



}
