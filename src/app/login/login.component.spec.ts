
import { FormGroup, FormBuilder, Validators, NgForm, ReactiveFormsModule } from '@angular/forms';
import { Router, Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login.component';
import { RouterTestingModule } from '@angular/router/testing';
import { async, inject, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter,  ViewChild, ElementRef, SimpleChange  } from '@angular/core';
import { User } from 'app/shared/objects/User';
import { AuthenticationService } from '../shared/services/Authenticate.Service'
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { HttpModule, Http } from "@angular/http";
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/of';
import 'zone.js/dist/async-test';
import 'zone.js/dist/fake-async-test';

xdescribe('Login Component Integration Test',()=>{
    //Arrange
    let logincomponent: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    let authenticationservice: AuthenticationService;
    let loginSpyObject = jasmine.createSpyObj('authenticationservice', ['login']);

    beforeAll(()=>{
        TestBed.configureTestingModule({
            imports: [ FormsModule, MyDatePickerModule, RouterTestingModule, ReactiveFormsModule, HttpClientModule, HttpClientTestingModule, HttpModule, NgbModule.forRoot()],
            declarations:[LoginComponent],
            providers:[AuthenticationService, { provide: loginSpyObject, useValue: loginSpyObject }]
        });

        fixture =  TestBed.createComponent(LoginComponent);
        logincomponent=fixture.componentInstance;
        authenticationservice = TestBed.get(AuthenticationService);
        
    })

    afterEach(()=>{
        authenticationservice.logout();
    })

    it('Show wrong message if log in email is wrong, isR is true', async(()=>{
        logincomponent.user=new User(-1, '', '', '');
        logincomponent.user.email='email'
        logincomponent.isRem=true;
        //let loginSpy=loginSpyObject.login().and.callThrough();
        logincomponent.login()
        fixture.whenStable();
        expect(logincomponent.message).toBe('Fail to Login');
    }))

    // it('Should navigate to main page after user log in with correct email, isR is true', ()=>{
    //     logincomponent.user.email='chen.fei@somrubioscience.com'
    //     logincomponent.isRem=true;
    //     logincomponent.login()
    // })

    // it('Show wrong message if log in email is wrong, isR is false', ()=>{
    //     logincomponent.user.email='email'
    //     logincomponent.isRem=false;
    //     logincomponent.login()
    //     expect(logincomponent.message).toBe('Fail to Login');
    // })

    // it('Should navigate to main page after user log in with correct email, isR is false', ()=>{
    //     logincomponent.user.email='chen.fei@somrubioscience.com'
    //     logincomponent.isRem=false;
    //     logincomponent.login()
    // })







    



})