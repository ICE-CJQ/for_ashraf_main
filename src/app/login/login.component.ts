import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm, FormControl, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../shared/services/Authenticate.Service';
import { User } from '../shared/objects/User';
import { UserhistoryService } from 'app/shared/services/userhistory.service';
import { Userhistory } from 'app/shared/objects/Userhistory';
import { ResponsiveService } from 'app/shared/services/responsive.service';

@Component({
  providers: [UserhistoryService, ResponsiveService],
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  isRem: boolean;
  message: string;
  form: FormGroup;
  user: User;
  currentuser: User;
  emailnotvalid=true;
  public isTablet=false;
  windth;
  resetPassword = false;
  resetNotification = "You will receive an email to reset your password shortly."
  loginForm: FormGroup;
  email: FormControl;
  password: FormControl;
  isRemember: FormControl;
  requetResetForm: FormGroup;
  resetemail: FormControl;
  loginError=false;
  activeError=false;
  errorMessage='';

  constructor(private fb: FormBuilder, private router: Router, private auth: AuthenticationService, private userhistoryservice: UserhistoryService, private responsiveService:ResponsiveService) {
    if (this.auth.isLogin())
      this.router.navigate(['./main']);
  }

  ngOnInit() {
    this.resetError();
    this.loginForm= this.createLoginForm();
    this.requetResetForm = this.createRequestResetForm();
    this.resetPassword = false;
    this.isRem = false;
    if(window.innerWidth<=1000 && window.innerWidth>=768){
      this.isTablet=true;
    }
    // this.onResize();
    this.user = new User(-1, '', '', '','',0,true);
    // if (localStorage.getItem('isR') !== 'y') {
    //   this.isRem = false;
    // } 
    // else {
    //   this.isRem = true;
    //   this.user.email = localStorage.getItem('user.email');
    // }
  }

  createLoginForm(): FormGroup {
    return this.fb.group(
      {
        // email is required and must be a valid email email
        email: [null, Validators.compose([
           Validators.email,
           Validators.required,
           this.patternValidator(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/, { hasPattern: true })])
        ],
        password: [ null, Validators.compose([Validators.required])],
        isRemember: [null, Validators.compose([])
       ]
     });
  }

  createRequestResetForm(): FormGroup {
    return this.fb.group(
      {
        // email is required and must be a valid email email
        resetemail: [null, Validators.compose([
           Validators.email,
           Validators.required,
           this.patternValidator(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/, { hasPattern: true })])
        ]
     });
  }



  onResize() {
    this.responsiveService.getTabletStatus().subscribe(isTablet => {
      this.isTablet = isTablet;
      this.responsiveService.checkWidth();
    });
  }

  emailPattern(control:FormControl): {[s:string]:boolean}{
    let emailPattern=/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
    if(!control.value){
      return null;
    }
      return control.value.match(emailPattern)?null:{'forbiddenName': true} ;
  }

  patternValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        // if control is empty return no error
        return null;
      }
  
      // test the value of the control against the regexp supplied
      const valid = regex.test(control.value);
  
      // if true, return no error (no error), else return error passed in the second parameter
      return valid ? null : error;
    };
  }

  static passwordMatchValidator(control: AbstractControl) {
    const password: string = control.get('password').value; // get password from our password form control
    const confirmPassword: string = control.get('confirmPassword').value; // get password from our confirmPassword form control
    // compare is the password math
    if (password !== confirmPassword) {
      // if they don't match, set an error in our confirmPassword form control
      control.get('confirmPassword').setErrors({ NoPassswordMatch: true });
    }
  }

  // onCheckBox(){
  //   this.isRem = !this.isRem;
  //   this.loginForm.controls['isRemember'].setValue(this.isRem);
  //   if (this.isRem) {
  //     localStorage.setItem('isR', 'y')
  //   }
  //   else { 
  //     let stayLoggedIn = localStorage.getItem('stayLoggedIn');
  //     localStorage.clear();
  //     sessionStorage.clear();
  //     localStorage.setItem('stayLoggedIn', stayLoggedIn);
  //     localStorage.setItem('isR', 'n')
  //   }
  // }

  changeTable(tableName:string){
    if(tableName=='login'){
      this.resetPassword = false;
      this.requetResetForm.reset();
    }
    else{
      this.resetPassword = true;
      this.loginForm.reset();
    }
    this.resetError();
  }

  sendLink(){
    let email = this.requetResetForm.value.resetemail;
    this.auth.requestResetPassword(email).subscribe(result=>{
      if(!result){
        alert('This email address does not exist')
        return;
      }
      else{
        alert('You will receive an email to reset your password shortly.')
      }
    }, error=>{
      console.log('catch error')
      console.log(error)
    })

  }

  resetError(){
    this.loginError=false;
    this.activeError=false;
    if(this.loginForm){
      this.loginForm.enable();
    }
  }

  // login(form: NgForm) {
  login() {
    // Login shorthand for Somru Bioscience Staff
    this.resetError();
    let email = this.loginForm.value.email;
    let password = this.loginForm.value.password;


    let emailTemp = email.split("@");
    if (emailTemp.length == 2 && emailTemp[0].length > 0) {
      if (emailTemp[1].toLowerCase() == "s.com") {
        email = emailTemp[0] + "@somrubioscience.com";
      }
    }
    let loginUser: any = new User(-1, '--', '--', email, password, null, null);

    // this.auth.login(loginUser, this.isRem).then(_ => {
      this.auth.login(loginUser).then(_=> {
      let getcurrentuser = this.auth.getCurrentUser()
      if (getcurrentuser != undefined) {
          getcurrentuser.then(_ => {
          let userhistory=new Userhistory(-1, new Date(), User.fromJson(_).name, 'Main', 'Log In', 'User Log  In')
          this.userhistoryservice.addUserHistory(userhistory);
        });
      }
      this.router.navigate(['./main']);
    })
    .catch(error=>{
      console.log(error)
      this.errorMessage=error;
      if(error=='Invalid email or password'){
        this.loginError=true;
      }
      if(error=='Your account is inactive, please contact administrator'){
        this.activeError=true;
        this.loginForm.disable();
      }
    })

    // The following if/else statement will always return true...
    // Code above will replace this.
    /*
      this.auth.login(loginUser, this.isRem).then(_ => {
        if ((localStorage.getItem('isR') == undefined && sessionStorage.getItem('currentUser') !== undefined) ||
          (localStorage.getItem('isR') !== undefined && localStorage.getItem('currentUser') !== undefined)){

          let getcurrentuser = this.auth.getCurrentUser()
          if (getcurrentuser !== undefined) {
              getcurrentuser.then(_ => {
              let userhistory=new Userhistory(-1, new Date(), User.fromJson(_).name, 'Main', 'Log In', 'User Log  In')
              this.userhistoryservice.addUserHistory(userhistory);
            });
          }
          this.router.navigate(['./main']);
        }
        else {
          this.message = 'Fail to Login';
          setTimeout(_ => { this.message = undefined; }, 5000);
        }
      });
      */

  }
}
